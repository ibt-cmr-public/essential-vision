using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoBehaviour
{

    public enum Scene
    {
        XR,
        Desktop,
        Main
    }
    
    [SerializeField] private List<Scene> XRStartupScenes = new List<Scene>();
    [SerializeField] private List<Scene> DesktopStartupScenes = new List<Scene>();
    
    

    private static SceneLoader _instance;
    public static SceneLoader Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SceneLoader>();
            }
            return _instance;
        }
    }
    
    private void Awake()
    {
        
        // Ensure there's only one instance of the singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        // Set this instance as the singleton
        _instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
    

    // Start is called before the first frame update
    void Start()
    {
        
        if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
        {
            foreach (Scene scene in XRStartupScenes)
            {
                SceneManager.LoadScene(scene.ToString(), LoadSceneMode.Additive);
            }
        }
        else
        {
            foreach (Scene scene in DesktopStartupScenes)
            {
                SceneManager.LoadScene(scene.ToString(), LoadSceneMode.Additive);
            }
        }
    }
    
}

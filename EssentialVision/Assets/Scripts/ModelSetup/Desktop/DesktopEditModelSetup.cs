using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesktopEditModelSetup : DesktopModelSetup
{
    private CommandInvoker<Model3DView> commandInvoker = null;
    
    protected override void InitializeComponents()
    {
        base.InitializeComponents();
        commandInvoker = new CommandInvoker<Model3DView>(ModelView);
        ((DesktopEditModelUI)desktopUI).CommandInvoker = commandInvoker;
    }
}

using UnityEngine;

public class DesktopModelSetup : ModelSetup
{
    private Model3DView modelView;
    [SerializeField] public DesktopModelUI desktopUI;
    [SerializeField] private AttachToTransform cameraRigAttachment;
    public Camera camera;
    public FitCameraToUIBlock cameraFitter;
    
    public override Model3DView ModelView
    {
        get
        {
            return modelView;
        }
        set
        {
            if (ModelView != value)
            {
                modelView = value;
                if (modelView.CurrentModel != null)
                {
                    InitializeComponents();
                }
                modelView.OnModelSet += (model) =>
                {
                    InitializeComponents();
                };
                OnModelViewSet?.Invoke(modelView);
            }
        }
    }

    protected virtual void InitializeComponents()
    {
        cameraRigAttachment.targetTransform = modelView.transform;
        desktopUI.ModelView = modelView;
    }
    
    public override void DestroyModel()
    {
        modelView.DestroyModel();
        DestroyImmediate(desktopUI);
        DestroyImmediate(gameObject);
    }

    public override void Hide()
    {
        modelView.gameObject.SetActive(false);
        desktopUI.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
    
    public override void Show()
    {
        gameObject.SetActive(true);
        desktopUI.gameObject.SetActive(true);
        modelView.gameObject.SetActive(true);
    }
}

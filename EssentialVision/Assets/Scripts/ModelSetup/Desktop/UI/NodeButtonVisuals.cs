using Nova;
using NovaSamples.UIControls;
using UnityEngine;

public class NodeButtonVisuals : ItemVisuals
{
    //Styling
    [SerializeField] float indentationWidth = 20;

    //Components
    [SerializeField] private UIBlock2D indentationSpacer;
    [SerializeField] public Button isVisibleButton;
    [SerializeField] public Button isExpandedButton;
    [SerializeField] public Button isOpaqueButton;
    [SerializeField] public Button nodeButton;
    [SerializeField] public TextBlock nodeButtonLabel;
    
    //Icons
    [SerializeField] private Texture2D expandedIcon;
    [SerializeField] private Texture2D collapsedIcon;
    [SerializeField] private Texture2D visibleIcon;
    [SerializeField] private Texture2D invisibleIcon;
    [SerializeField] private Texture2D opaqueIcon;
    [SerializeField] private Texture2D transparentIcon;
    
    private bool isVisible;
    private bool isExpanded;
    private bool isOpaque;
    private bool isLeaf;
    private bool isRoot;
    private int indentationLevel;

    public bool IsExpanded
    {
        get
        {
            return isExpanded;
        }
        set
        {
            isExpanded = value;
            if (isExpanded)
            {
                isExpandedButton.GetComponent<UIBlock2D>().SetImage(expandedIcon);
            }
            else
            {
                isExpandedButton.GetComponent<UIBlock2D>().SetImage(collapsedIcon);
            }
        }
    }
    
    public bool IsVisible
    {
        get
        {
            return isVisible;
        }
        set
        {
            isVisible = value;
            if (isVisible)
            {
                isVisibleButton.GetComponent<UIBlock2D>().SetImage(visibleIcon);
            }
            else
            {
                isVisibleButton.GetComponent<UIBlock2D>().SetImage(invisibleIcon);
            }
        }
    }
    
    public bool IsOpaque
    {
        get
        {
            return isOpaque;
        }
        set
        {
            isOpaque = value;
            if (isOpaque)
            {
                isOpaqueButton.GetComponent<UIBlock2D>().SetImage(opaqueIcon);
            }
            else
            {
                isOpaqueButton.GetComponent<UIBlock2D>().SetImage(transparentIcon);
            }
        }
    }
    
    public bool IsLeaf
    {
        get
        {
            return isLeaf;
        }
        set
        {
            isLeaf = value;
            if (isLeaf)
            {
                isExpandedButton.gameObject.SetActive(false);
            }
            else
            {
                isExpandedButton.gameObject.SetActive(true);
            }
        }
    }

    public int IndentationLevel
    {
        get
        {
            return indentationLevel;
        }
        set
        {
            indentationLevel = value;
            indentationSpacer.Layout.Size.X = indentationLevel * indentationWidth;
        }
    }
}

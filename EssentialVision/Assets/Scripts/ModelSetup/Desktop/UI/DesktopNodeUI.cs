using Nova;
using UnityEngine;

public class DesktopNodeUI : NodeUI
{

    [SerializeField] private TextBlock nameText;
    [SerializeField] private TextBlock descriptionText;
    
    public override void SetNode(ModelNode node)
    {
        if (this.node != node)
        {
            this.node.OnNodeInfoChanged -= UpdateUI;
            this.node = node; 
            node.OnNodeInfoChanged += UpdateUI;
            UpdateUI();
        }
    }

    private void UpdateUI()
    {
        if (node == null)
        {
            nameText.Text = "";
            descriptionText.Text = "";
            return;
        }
        nameText.Text = node.Name;
        descriptionText.Text = node.Description;
    }
}

using System.Collections;
using System.Collections.Generic;
using NovaSamples.UIControls;
using UnityEngine;

public class DesktopEditNodeListUI : DesktopNodeListUI
{
    [SerializeField] private Button addNodeButton;
    [SerializeField] private Button deleteNodeButton;
    private CommandInvoker<Model3DView> commandInvoker;

    public CommandInvoker<Model3DView> CommandInvoker
    {
        get
        {
            return commandInvoker;
        }
        set
        {
            if (commandInvoker != value)
            {
                ((DesktopEditNodeUI)selectedNodeUI).CommandInvoker = value;
                commandInvoker = value;
            }
        }
    }
    
    private void OnClickAddNode()
    {
        //TODO: IMPLEMENT THIS
    }

    private void OnClickDeleteNode()
    {
        //TODO: IMPLEMENT THIS
    }
}

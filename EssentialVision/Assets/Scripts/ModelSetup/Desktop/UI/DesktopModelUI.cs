using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesktopModelUI : MonoBehaviour
{
    protected Model3DView modelView;
    [SerializeField] protected DesktopShortcutUI desktopShortcutUI;
    [SerializeField] protected DesktopNodeListUI desktopNodeListUI;
    [SerializeField] protected AnimationUI animationUI;
    
    public virtual Model3DView ModelView
    {
        get
        {
            return modelView;
        }
        set
        {
            if (ModelView != value)
            {
                modelView = value;
                if (modelView.CurrentModel != null)
                {
                    InitializeComponents();
                }
                modelView.OnModelSet += (model) =>
                {
                    InitializeComponents();
                };
            }
        }
    }
    
    public void InitializeComponents()
    {
        animationUI.Animator = modelView.animator;
        desktopNodeListUI.StateController = modelView.stateController;
        desktopNodeListUI.NodeController = modelView.nodeController;
        desktopShortcutUI.ModelView = modelView;
        animationUI.gameObject.SetActive(modelView.CurrentModel.IsAnimated);
    }
}

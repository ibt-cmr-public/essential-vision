using NovaSamples.UIControls;
using UnityEngine;

public class DesktopAnimationUI : AnimationUI
{
    
    [SerializeField] private Slider AnimationSlider;
    [SerializeField] private Button PlayPauseButton;
    [SerializeField] private Button StopButton;
    
    
    public void OnAnimationSliderUpdated(float value)
    {
        if (userInputUpdate)
        {
            OnAnimationTimeSet(value);
        }
    }

    public void Start()
    {
        AnimationSlider.OnValueChanged.AddListener(OnAnimationSliderUpdated);
    }

    protected override void UpdateUI()
    {
        PlayPauseButton.transform.Find("Icon_play").gameObject.SetActive(!animator.IsPlaying);
        PlayPauseButton.transform.Find("Icon_pause").gameObject.SetActive(animator.IsPlaying);
        AnimationSlider.Value = animator.CurrentTime;
    }
    
}

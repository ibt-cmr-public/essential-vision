using System;
using System.Collections.Generic;
using Nova;
using UnityEngine;
using ListView = Nova.ListView;

public class DesktopNodeListUI : MonoBehaviour
{
    //UI Elements
    [SerializeField] protected NodeUI selectedNodeUI;
    [SerializeField] private ListView nodeListView;
    
    //Styling
    [SerializeField] private bool showModelRoot = false;
    [SerializeField] private bool expandedByDefault = true;
    
    //Other components]
    private NodeVisibilityStateController stateController;
    private NodeController nodeController;
    
    //Logic
    private Dictionary<Guid, bool> nodesExpanded = new Dictionary<Guid, bool>();
    private Guid displayedLayerGuid;
    private bool isInitialized = false;
    private bool UIDirty = false;
    
    //Properties
    private ModelNode rootNode
    {
        get
        {
            return nodeController?.RootNode;
        }
    }

    public NodeController NodeController
    {
        get
        {
            return nodeController;
        }
        set
        {
            nodeController = value;
            nodesExpanded.Clear();
            if (rootNode != null)
            {
                foreach (ModelNode node in rootNode.Subnodes)
                {
                    nodesExpanded.Add(node.Guid, expandedByDefault);
                }
            }
            nodeController.NodeAdded += (node) =>
            {
                SetUIDirty();
            };
            
            nodeController.NodeRemoved += (node) =>
            {
                SetUIDirty();
            };
            
            nodeController.NodeSelected += (node) =>
            {
                SetUIDirty();
            };
            
            UIDirty = true;
        }
    }
    
    public NodeVisibilityStateController StateController
    {
        get
        {
            return stateController;
        }
        set
        {
            stateController = value;
            stateController.NodeVisibilityChanged += SetUIDirty;
            UIDirty = true;
        }
    }
    
    //Expanded tree handling
    public class NodeButtonData
    {
        public ModelNode Node;
        public bool Expanded;    
    }
    
    private IEnumerable<ModelNode> TraverseCollapsedTree(ModelNode node)
    {
        yield return node;
        if (nodesExpanded[node.Guid]) {
            foreach (ModelNode child in node.Children)
            {
                //Traverse subtree of each child
                foreach (ModelNode grandchild in TraverseCollapsedTree(child))
                {
                    yield return grandchild;
                }
            }
        }
    }
    
    public IEnumerable<ModelNode> TraverseCollapsedTree()
    {
        if (rootNode != null)
        {
            foreach (ModelNode node in TraverseCollapsedTree(rootNode))
            {
                yield return node;
            }
        }
    }
    
    private void UpdateExpandedDictionary()
    {
        foreach (ModelNode node in rootNode.Subnodes)
        {
            if (!nodesExpanded.ContainsKey(node.Guid))
            {
                nodesExpanded.Add(node.Guid, expandedByDefault);
            }
        }
    }
    
    public void SetNodeExpanded(Guid nodeID, bool expanded)
    {
        if (nodesExpanded.ContainsKey(nodeID))
        {
            nodesExpanded[nodeID] = expanded;
        }
        UpdateListView();
    }

    public void ToggleNodeExpanded(Guid nodeID)
    {
        SetNodeExpanded(nodeID, !nodesExpanded[nodeID]);
    }

    //UI Update
    private void SetUIDirty()
    {
        UIDirty = true;
    }
    
    public void UpdateListView()
    {
        List<NodeButtonData> dataSource = new List<NodeButtonData>();
        foreach (ModelNode node in TraverseCollapsedTree())
        {
            dataSource.Add(new NodeButtonData
            {
                Node = node,
                Expanded = nodesExpanded[node.Guid]
            });
        }
        nodeListView.SetDataSource(dataSource);
    }

    public void UpdateSelectedNodeUI()
    {
        if (selectedNodeUI!= null)
        {
            selectedNodeUI.SetNode(nodeController?.SelectedNode);
        }
    }

    private void Start()
    {
        Initialize();
    }

    private void Update()
    {
        if (UIDirty)
        {
            UpdateListView();
            UpdateSelectedNodeUI();
            UIDirty = false;
        }
    }

    private void Initialize()
    {
        if (!isInitialized)
        {
            nodeListView.AddDataBinder<NodeButtonData, NodeButtonVisuals>(NodeButtonDataBinder);
        }
        isInitialized = true;
    }
    
    private void NodeButtonDataBinder(Data.OnBind<NodeButtonData> nodeButtonData, NodeButtonVisuals nodeButtonVisuals, int index)
    {
        //Set Visuals
        nodeButtonVisuals.IsExpanded = nodeButtonData.UserData.Expanded;
        ModelNode node = nodeButtonData.UserData.Node;
        nodeButtonVisuals.nodeButtonLabel.Text = node.NodeInfo.Name;
        nodeButtonVisuals.IsLeaf = node.IsLeaf;
        nodeButtonVisuals.IsVisible = !node.IsHidden;
        nodeButtonVisuals.IsOpaque = !node.IsFaded;
        nodeButtonVisuals.IndentationLevel = node.Level;
        
        //Gesture handlers
        nodeButtonVisuals.isExpandedButton.OnClicked.RemoveAllListeners();
        nodeButtonVisuals.isVisibleButton.OnClicked.RemoveAllListeners();
        nodeButtonVisuals.isOpaqueButton.OnClicked.RemoveAllListeners();
        nodeButtonVisuals.nodeButton.OnClicked.RemoveAllListeners();
        nodeButtonVisuals.isExpandedButton.OnClicked.AddListener(() => {ToggleNodeExpanded(node.Guid);});
        nodeButtonVisuals.isVisibleButton.OnClicked.AddListener(() => {StateController.ToggleHideNode(node.Guid);});
        nodeButtonVisuals.isOpaqueButton.OnClicked.AddListener(() => {StateController.ToggleFadeNode(node.Guid);});
        nodeButtonVisuals.nodeButton.OnClicked.AddListener(() => {NodeController.SelectedNodeGuid = node.Guid;});
    }

    
    //Button event handlers
    
    public void OnClickHide()
    {
        StateController.ToggleHideNode(displayedLayerGuid);
    }

    public void OnClickHideOthers()
    {
        StateController.HideOtherLayers(displayedLayerGuid, !stateController.OtherLayersHidden(displayedLayerGuid));
    }
    
    public void OnClickFade()
    {
        StateController.ToggleFadeNode(displayedLayerGuid);
    }

    public void OnClickFadeOthers()
    {
        // Flags to track if other layers are hidden/faded
        StateController.FadeOtherLayers(displayedLayerGuid, !stateController.OtherLayersFaded(displayedLayerGuid));
    }

    public void OnClickShowAll()
    {
        StateController.ShowAll();
    }

    public void OnClickUnfadeAll()
    {
        StateController.UnfadeAll();
    }

}

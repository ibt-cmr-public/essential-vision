using System;
using System.Collections;
using System.Collections.Generic;
using NovaSamples.UIControls;
using UnityEditor.AddressableAssets.Build;
using UnityEngine;

public class DesktopEditModelUI : DesktopModelUI
{

    //UI Elements
    [SerializeField] private TextField nameText;
    [SerializeField] private TextField descriptionText;
    [SerializeField] private Button undoButton;
    [SerializeField] private Button redoButton;
    [SerializeField] private Button saveChangeButton;
    
    //Input variables
    public string Name => nameText.Text;
    public string Description => descriptionText.Text;
    

    //Logic
    bool userInput = true;
    private CommandInvoker<Model3DView> commandInvoker;
    public CommandInvoker<Model3DView> CommandInvoker
    {
        get => commandInvoker;
        set
        {
            if (commandInvoker != value)
            {
                if (commandInvoker != null)
                {
                    commandInvoker.OnUndo -= UpdateUI;
                    commandInvoker.OnRedo -= UpdateUI;
                    commandInvoker.OnClear -= UpdateUI;
                    commandInvoker.OnCommandAdded -= UpdateUI;
                }
                commandInvoker = value;
                ((DesktopEditNodeListUI)desktopNodeListUI).CommandInvoker = commandInvoker;
                if (commandInvoker != null)
                {
                    commandInvoker.OnUndo += UpdateUI;
                    commandInvoker.OnRedo += UpdateUI;
                    commandInvoker.OnClear += UpdateUI;
                    commandInvoker.OnCommandAdded += UpdateUI;
                    UpdateUI();
                }
            }
        }
    }
    
    public override Model3DView ModelView
    {
        set
        {
            if (modelView != null)
            {
                modelView.OnModelInfoChanged -= UpdateUI;
            }
            base.ModelView = value;
            if (modelView != null)
            {
                modelView.OnModelInfoChanged += UpdateUI;
            }
            UpdateUI();
        }
    }


    public void Start()
    {
        nameText.OnTextChanged += () =>
        {
            if (userInput) OnUserEnteredModelName(); 
            
        };
        descriptionText.OnTextChanged += () =>
        {
            if (userInput) OnUserEnteredModelDescription();
        };
        saveChangeButton.OnClicked.AddListener(OnClickSaveChanges);
        undoButton.OnClicked.AddListener(OnClickUndo);
        redoButton.OnClicked.AddListener(OnClickRedo);
    }

    public void UpdateUI()
    {
        userInput = false;
        undoButton.enabled = commandInvoker!=null? commandInvoker.CanUndo: false;
        redoButton.enabled = commandInvoker!=null? commandInvoker.CanRedo: false;
        nameText.Text = modelView!=null?modelView.Name:"";
        descriptionText.Text = modelView!=null?modelView.Description:"";
        userInput = true;
    }
    
    //Button event handlers
    private void OnClickUndo()
    {
        commandInvoker.Undo();
    }
    
    private void OnClickRedo()
    {
        commandInvoker.Redo();
    }
    
    private void OnClickSaveChanges()
    {
        //TODO: IMPLEMENT THIS SAVING FUNCTION
    }

    private void OnUserEnteredModelName()
    {
        string newName = nameText.Text;
        if (modelView == null || commandInvoker == null || newName == modelView.Name)
        {
            return;
        }
        ChangeModelPropertyCommand<string> changeNameCommand = new ChangeModelPropertyCommand<string>(model => model.Name, newName);
        commandInvoker.AddCommand(changeNameCommand);
    }
    
    private void OnUserEnteredModelDescription()
    {
        string newDescription = descriptionText.Text;
        if (modelView == null || commandInvoker == null || newDescription == modelView.Description)
        {
            return;
        }
        ChangeModelPropertyCommand<string> changeDescriptionCommand = new ChangeModelPropertyCommand<string>(model => model.Description, newDescription);
        commandInvoker.AddCommand(changeDescriptionCommand);
    }
    
}

using System;
using System.Collections;
using System.Collections.Generic;
using Nova;
using NovaSamples.UIControls;
using SimpleFileBrowser;
using UnityEngine;

public class DesktopEditNodeUI : NodeUI
{
    //TODO: Probably put all layer relaterd stuff in subclass DEsktopEditLayerUI
    [SerializeField] private TextField nameText;
    [SerializeField] private TextField descriptionText;
    [SerializeField] private Slider smoothnessSlider;
    [SerializeField] private Button loadVTKButton;
    
    
    public string Name => nameText.Text;
    public string Description => descriptionText.Text;
    public float Smoothness => smoothnessSlider.Value;
    private bool userInputUpdate = true;
    
    //Logic components
    CommandInvoker<Model3DView> commandInvoker;
    public CommandInvoker<Model3DView> CommandInvoker
    {
        get => commandInvoker;
        set
        {
            commandInvoker= value;
        }
    }
    
    public void Start() 
    {
        nameText.OnTextChanged += () =>
        {
            if (userInputUpdate) OnUserEnteredName(nameText.Text);
        };
        descriptionText.OnTextChanged += () => 
        {
            if(userInputUpdate) OnUserEnteredDescription(descriptionText.Text);
        };
        smoothnessSlider.OnValueChanged.AddListener((value) =>
        {
            if (userInputUpdate) OnUserSetSmoothness(value);
        });
        loadVTKButton.OnClicked.AddListener(OnPressLoadVTK);
    }
    
    public override void SetNode(ModelNode node)
    {
        if (this.node != node)
        {
            this.node.OnNodeInfoChanged -= UpdateUI;
            if(this.node is LayerInstance oldLayer)
                oldLayer.OnMaterialChanged -= UpdateUI;
            this.node = node; 
            this.node.OnNodeInfoChanged += UpdateUI;
            if(this.node is LayerInstance newLayer)
                newLayer.OnMaterialChanged += UpdateUI;
            UpdateUI();
        }
    }

    private void UpdateUI()
    {
        userInputUpdate = false;
        if (node == null)
        {
            nameText.Text = "";
            descriptionText.Text = "";
            smoothnessSlider.gameObject.SetActive(false);
            userInputUpdate = true;
            return;
        }
        nameText.Text = node.Name;
        descriptionText.Text = node.Description;
        
        if (node is LayerInstance layer && layer.NodeType == NodeType.Mesh)
        {
            smoothnessSlider.gameObject.SetActive(true);
            smoothnessSlider.Value = layer.Smoothness;
        }
        else
        {
            smoothnessSlider.gameObject.SetActive(false);
        }
        userInputUpdate = true;
    }

    private void OnPressLoadVTK()
    {
        FileBrowser.SetFilters( false, new FileBrowser.Filter( "VTK Files", "vtk" ));
        FileBrowser.SetDefaultFilter( ".vtk" );
        StartCoroutine( ShowLoadDialogCoroutine() );
    }
    
    IEnumerator ShowLoadDialogCoroutine()
    {
        yield return FileBrowser.WaitForLoadDialog( FileBrowser.PickMode.Files, false, null, null, "Select Files", "Load" );
        // Dialog is closed
        // Print whether the user has selected some files or cancelled the operation (FileBrowser.Success)
        Debug.Log( FileBrowser.Success );

        if( FileBrowser.Success )
            OnUserLoadVTKFile(FileBrowser.Result); // FileBrowser.Result is null, if FileBrowser.Success is false
    }

    private void OnUserEnteredName(string newName)
    {
        if (node == null || CommandInvoker == null) return;
        ChangeNodePropertyCommand<string> nodeCommand = new ChangeNodePropertyCommand<string>(node => node.Name, newName);
        ChangeModelNodePropertyCommand<string> modelCommand =
            new ChangeModelNodePropertyCommand<string>(nodeCommand, node.Guid);
        CommandInvoker?.AddCommand(modelCommand);
    }
    
    private void OnUserEnteredDescription(string newDescription)
    {
        if (node == null || CommandInvoker == null) return;
        ChangeNodePropertyCommand<string> nodeCommand = new ChangeNodePropertyCommand<string>(node => node.Description, newDescription);
        ChangeModelNodePropertyCommand<string> modelCommand =
            new ChangeModelNodePropertyCommand<string>(nodeCommand, node.Guid);
        CommandInvoker?.AddCommand(modelCommand);
    }
    
    private void OnUserSetSmoothness(float newSmoothness)
    {
        if (node == null || CommandInvoker == null) return;
        ChangeNodePropertyCommand<float> nodeCommand = new ChangeNodePropertyCommand<float>(node => ((LayerInstance)node).Smoothness, newSmoothness);
        ChangeModelNodePropertyCommand<float> modelCommand =
            new ChangeModelNodePropertyCommand<float>(nodeCommand, node.Guid);
        CommandInvoker?.AddCommand(modelCommand);
    }

    private void OnUserLoadVTKFile(string[] filePaths)
    {
        if (node == null || CommandInvoker == null) return;
        //TODO: IMPLEMENT THIS
        throw new NotImplementedException();
    }



}

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class CameraRigController : MonoBehaviour
{
    
    [SerializeField]
    private InputActionAsset inputActions;

    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Camera camera;
    private InputAction moveAction;
    private float movementSpeed = 0.01f;
    private InputAction rotateAction;
    private float rotateSpeed = 0.25f;
    private InputAction zoomAction;
    private float zoomSpeed = 0.001f;
    private float minDistance = 0.005f;
    private float maxDistance = 10f;
    private float distance = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        moveAction = inputActions.FindAction("MoveCameraRig");
        rotateAction = inputActions.FindAction("RotateCameraRig");
        zoomAction = inputActions.FindAction("Zoom");
        inputActions.Enable();
        distance = camera.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        Vector2 move = -moveAction.ReadValue<Vector2>()*movementSpeed;
        cameraTransform.localPosition += new Vector3(move.x, move.y, 0);
        Vector2 rotate = rotateAction.ReadValue<Vector2>()*rotateSpeed;
        transform.Rotate(-rotate.y, rotate.x, 0);
        float zoom = zoomAction.ReadValue<float>()*zoomSpeed;
        distance -= zoom * camera.orthographicSize;
        distance = Mathf.Clamp(distance, minDistance, maxDistance);
        camera.orthographicSize = distance;
    }
}

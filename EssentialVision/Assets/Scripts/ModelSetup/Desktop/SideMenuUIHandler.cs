using UnityEngine;

public class SideMenuUIHandler : MonoBehaviour
{

    public GameObject ModelMenu;
    public GameObject SharedExpereinceMenu;
    public GameObject MenuPanels;
    
    public void ToggleModelMenu() 
    {
        ModelMenu.SetActive(!ModelMenu.activeSelf);
        if (!ModelMenu.activeSelf & !SharedExpereinceMenu.activeSelf)
        {
            MenuPanels.SetActive(false);
        }
        else if (!MenuPanels.activeSelf)
        {
            MenuPanels.SetActive(true);
        }
    }

    public void ToggleSharedExpereinceMenu()
    {
        SharedExpereinceMenu.SetActive(!SharedExpereinceMenu.activeSelf);
        if (!ModelMenu.activeSelf & !SharedExpereinceMenu.activeSelf)
        {
            MenuPanels.SetActive(false);
        }
        else if (!MenuPanels.activeSelf)
        {
            MenuPanels.SetActive(true);
        }
    }
}

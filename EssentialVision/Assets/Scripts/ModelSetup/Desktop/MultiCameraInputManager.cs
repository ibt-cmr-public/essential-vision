using System.Collections;
using System.Collections.Generic;
using NovaSamples.UIControls;
using UnityEngine;

public class MultiCameraInputManager : InputManager
{
    
    public List<Camera> cameras;
    
    // Update is called once per frame
    void Update()
    {
        UpdateActiveCamera();
        base.Update();
    }
    
    void UpdateActiveCamera()
    {
        //Determine current mouse cursor position in screen space
        float currentDepth = float.MaxValue;
        Vector3 mousePosition = Input.mousePosition;
        foreach (var camera in cameras)
        {
            if (camera == null || !camera.gameObject.activeInHierarchy || !camera.enabled || camera.depth > currentDepth)
            {
                continue;
            }
            //Check if mouse cursor is within camera viewport
            if (camera.pixelRect.Contains(mousePosition)) {
                Cam = camera;
            }

        }
    }
    
}

using UnityEngine;


//TODO: Should we set up events here so buttons are correctly hooked up to animation state at all times?
//Class for animation menu that handles animation controls (in the case of an animated model)
public abstract class AnimationUI : MonoBehaviour
{
    protected ModelAnimator animator;
    protected bool userInputUpdate = true;
    public ModelAnimator Animator
    {
        get
        {
            return animator;
        }
        set
        {
            if (animator != value)
            {
                animator = value;
            }
        }    
    }
    
    // UI Input Handlers
    public void OnClickPausePlay()
    {
        Animator.TogglePlay();
    }

    public void OnClickStop()
    {
        Animator.Stop();
    }
    public void OnAnimationTimeSet(float time)
    {
        Animator.CurrentTime = time;
    }

    protected abstract void UpdateUI();

    public void Update()
    {
        userInputUpdate = false;
        UpdateUI();
        userInputUpdate = true;
    }

}

﻿using MixedReality.Toolkit.UX;
using UnityEngine;
using Image = UnityEngine.UI.Image;


public class XRAnimationUI : AnimationUI
{
    [SerializeField] private Sprite PlayIcon;
    [SerializeField] private Sprite PauseIcon;
    [SerializeField] private MixedReality.Toolkit.UX.Slider AnimationSlider;
    [SerializeField] private PressableButton PlayPauseButton;
    [SerializeField] private PressableButton StopButton;
    
    
    public void OnAnimationSliderUpdated(SliderEventData data)
    {
        if (userInputUpdate)
        {
            OnAnimationTimeSet(data.NewValue);
        }
    }

    protected override void UpdateUI()
    {
        PlayPauseButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon").GetComponent<Image>()
                    .sprite = animator.IsPlaying ? PauseIcon : PlayIcon;
        AnimationSlider.Value = animator.CurrentTime;
    }
    
}
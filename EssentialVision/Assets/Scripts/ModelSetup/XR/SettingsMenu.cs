using MixedReality.Toolkit.UX;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


//Class for clipping plane menu that handles controls for clipping planes (add clipping plane, change shape, etc.)

public class SettingsMenu : MonoBehaviour
{

    //Other controllers/high level components
    [SerializeField] public ClippingPlaneController clippingPlaneController;
    [SerializeField] public Model3DView modelView;
    [SerializeField] public ModelAnimator animator;
    [SerializeField] public NodeVisibilityStateController stateController;
    [SerializeField] public NodeController nodeController;
    [SerializeField] public AnnotationController annotationController;

    
    [SerializeField] private AudioManager audioManager;
    [SerializeField] private GameObject animationMenu;
    
    //UI components
    //Animation settings
    [SerializeField] private GameObject animationSettings;
    [SerializeField] private Slider animationSpeedSlider;
    [SerializeField] private PressableButton displayAnimationMenuButton;
    [SerializeField] private PressableButton rotateModelButton;
    [SerializeField] private Slider rotationSpeedSlider;
    
    //Clipping plane settings
    [SerializeField] private PressableButton activateClippingPlaneButton;
    [SerializeField] private TMP_Dropdown volumetricTextureDropdown;
    
    //Appearance settings
    [SerializeField] private ToggleCollection highlightLayersToggleCollection;
    [SerializeField] private ToggleCollection tooltipsToggleCollection;
    
    //Audio settings
    [SerializeField] private PressableButton readDescriptionOnSelectionButton;

    //Variable that tracks whether a UI update is performed by the user through input 
    //(i.e. by pressing a button or moving a slider) vs. because an internal variable 
    //has changed on the network. We need to distinguish in this way to avoid a feedback 
    //loop in which the client-side updates call a change of variable on the server which
    //causes another client-side update and so on
    private bool userInputUpdate = true;

    public void Awake()
    {
        clippingPlaneController.isActive.OnValueChanged += OnClippingPlaneIsActiveValueChanged;
        animator.animationSpeed.OnValueChanged += OnAnimationSpeedChanged;
        modelView.rotationSpeed.OnValueChanged += OnRotationSpeedChanged;
        modelView.autorotateModel.OnValueChanged += OnAutorotateModelValueChanged;
        clippingPlaneController.textureIdx.OnValueChanged += OnClippingPlaneTextureIdxValueChanged;
    }
    
    //Events for updating UI when internal values are updated
    private void OnClippingPlaneIsActiveValueChanged(bool oldValue, bool newValue)
    {
        //Note: This will not cause the event of the toggle button to fire (again)
        //because it has been set to fire on OnClick not OnToggle 
        activateClippingPlaneButton.ForceSetToggled(newValue, true);
    }

    private void OnClippingPlaneTextureIdxValueChanged(int oldValue, int newValue)
    {
        userInputUpdate = false;
        volumetricTextureDropdown.value = newValue;
        userInputUpdate = true;
    }
    
    private void OnAnimationSpeedChanged(float oldValue, float newValue)
    {
        userInputUpdate = false;
        animationSpeedSlider.Value = newValue;
        userInputUpdate = true;
    }

    private void OnRotationSpeedChanged(float oldValue, float newValue)
    {
        userInputUpdate = false;
        rotationSpeedSlider.Value = newValue;
        userInputUpdate = true;
    }

    private void OnAutorotateModelValueChanged(bool oldValue, bool newValue)
    {
        userInputUpdate = false;
        rotateModelButton.ForceSetToggled(newValue, true);
        userInputUpdate = true;
    }

    //Input methods to call when UI elements are triggered by user
    //Animation Settings
    public void OnChangeAnimationSpeed(SliderEventData data)
    {
        if (userInputUpdate)
        {
            animator.AnimationSpeed = data.NewValue;    
        }
    }

    public void OnToggleAnimationMenu()
    {
        animationMenu.SetActive(!animationMenu.activeSelf);
    }

    public void OnToggleModelRotation(bool value)
    {
        if (userInputUpdate)
        {
            modelView.AutorotateModel = value;
        }
    }

    public void OnChangeRotationSpeed(SliderEventData data)
    {
        if (userInputUpdate)
        {
            modelView.RotationSpeed = data.NewValue;
        }
    }
    
    //Visuals settings
    public void OnChangeTransparencyValue(SliderEventData data)
    {
        if (userInputUpdate)
        {
            stateController.TransparencyValue = data.NewValue;
        }
    }


    //Clipping Plane Settings
    public void OnToggleClippingPlane()
    {
        if (userInputUpdate)
        {
            clippingPlaneController.IsActive = !clippingPlaneController.IsActive;
        }
    }

    public void OnSelectVolumetricTexture(int i)
    {
        if (userInputUpdate)
        {
            clippingPlaneController.TextureIdx = i - 1;
        }
    }

    public void OnCheckSelectionMode(int value)
    {

    }
    
    //Annotation Settings
    public void OnToggleShowAnnotations()
    {
        annotationController.ShowAnnotations = !annotationController.ShowAnnotations;
    }


    //Audio Settings
    public void OnToggleReadDescriptions()
    {
        audioManager.PlayDescriptionOnSelection = !audioManager.PlayDescriptionOnSelection;
    }


    //Appearance/Control Settings
    //Highlight Layers
    public void OnToggleGroupedSelection()
    {
        nodeController.GroupedSelection = !nodeController.GroupedSelection;
    }
    
    public void OnToggleGazeInteractions()
    {

    }
    
    public void OnCheckHighlightSelectedLayers()
    {
        stateController.OutlineSelected = true;
    }

    public void OnUncheckHighlightSelectedLayers()
    {
        stateController.OutlineSelected = false;
    }

    public void OnCheckHighlightHoveredLayers()
    {
        stateController.OutlineHovered = true;
    }

    public void OnUncheckHighlightHoveredLayers()
    {
        stateController.OutlineHovered = false;
    }

    //Tooltips
    public void OnCheckTooltipSelectedLayers()
    {
        stateController.ShowTooltipSelected = true;
    }

    public void OnUncheckTooltipSelectedLayers()
    {
        stateController.ShowTooltipSelected = false;
    }

    public void OnCheckTooltipHoveredLayers()
    {
        stateController.ShowTooltipHovered = true;
    }

    public void OnUncheckTooltipHoveredLayers()
    {
        stateController.ShowTooltipHovered = false;
    }

    //Color Layers
    public void OnToggleColorLayers(int value)
    {
        //TODO: Implement color functionality
    }

    public void SetModel(ModelScriptableObject model)
    {
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
        options.Add(new TMP_Dropdown.OptionData("None"));
        foreach (var volumetricTexture in model.VolumetricTextures)
        {
            options.Add(new TMP_Dropdown.OptionData(volumetricTexture.caption));
        }
        volumetricTextureDropdown.options = options;
        animationSettings.SetActive(model.IsAnimated);
    }
    
}

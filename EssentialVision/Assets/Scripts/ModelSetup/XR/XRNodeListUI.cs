using System;
using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Class for layer menu that allows for layer selection and layer visibility controls
public class XRNodeListUI : NodeListUI
{

    [Header("Model and UI Components")] 
    [SerializeField] private PressableButton toggleHideButton;
    [SerializeField] private PressableButton toggleFadeButton;
    [SerializeField] private PressableButton hideOthersButton;
    [SerializeField] private PressableButton fadeOthersButton;
    [SerializeField] private PressableButton showAllButton;

    // Serialized Fields - Icons
    [Header("Icons")]
    [SerializeField] private Sprite visibleIcon;
    [SerializeField] private Sprite invisibleIcon;
    [SerializeField] private Sprite opaqueIcon; 
    [SerializeField] private Sprite transparentIcon;




    // Method to update the UI elements based on layer visibility and fade states
    protected override void UpdateUI()
    {
        if (NodeController == null | StateController == null)
        {
            return;
        }
        base.UpdateUI();
        Guid selectedNodeGuid = _nodeController.SelectedNodeGuid;

        //Update layer description and title
        if (selectedNodeGuid != Guid.Empty && NodeController.SelectedNode != null)
        {
            // Update button states based on layer visibility and fade states
            bool otherNodesHidden = stateController.OtherLayersHidden(selectedNodeGuid);
            bool otherNodesFaded = stateController.OtherLayersFaded(selectedNodeGuid);
            toggleHideButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon").GetComponent<Image>().sprite =
                _nodeController.SelectedNode.IsHidden ? invisibleIcon : visibleIcon;
            toggleHideButton.transform.Find("Frontplate/AnimatedContent/Text").GetComponent<TextMeshProUGUI>().text =
                _nodeController.SelectedNode.IsHidden ? "Show" : "Hide";

            toggleFadeButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon").GetComponent<Image>().sprite =
                _nodeController.SelectedNode.IsFaded ? transparentIcon : opaqueIcon;
            toggleFadeButton.transform.Find("Frontplate/AnimatedContent/Text").GetComponent<TextMeshProUGUI>().text =
                _nodeController.SelectedNode.IsFaded ? "Unfade" : "Fade";

            hideOthersButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon").GetComponent<Image>().sprite =
                otherNodesHidden ? invisibleIcon : visibleIcon;
            hideOthersButton.transform.Find("Frontplate/AnimatedContent/Text").GetComponent<TextMeshProUGUI>().text =
                otherNodesHidden ? "Show Others" : "Hide Others";

            fadeOthersButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon").GetComponent<Image>().sprite =
                otherNodesFaded ? transparentIcon : opaqueIcon;
            fadeOthersButton.transform.Find("Frontplate/AnimatedContent/Text").GetComponent<TextMeshProUGUI>().text =
                otherNodesFaded ? "Unfade Others" : "Fade Others";
        }
    }
    
}
using TMPro;
using UnityEngine;

public class XRNodeUI : NodeUI
{
    [SerializeField] private TextMeshProUGUI layerDescriptionText;
    [SerializeField] private TextMeshProUGUI layerTitleText;
    
    public override void SetNode(ModelNode node)
    {
        if (node == null)
        {
            layerTitleText.SetText("");
            layerDescriptionText.SetText("");
        }
        else
        {
            layerTitleText.SetText(node.Name);
            layerDescriptionText.SetText(node.Description);
        }
    }
}

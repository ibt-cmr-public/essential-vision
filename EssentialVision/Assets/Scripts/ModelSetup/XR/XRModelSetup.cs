using MixedReality.Toolkit.SpatialManipulation;
using UnityEngine;
using UnityEngine.Serialization;

public class XRModelSetup : ModelSetup
{

    public bool IsNetworked = false;
    private UIController UIController;
    
    [FormerlySerializedAs("xrShortcutMenu")]
    [FormerlySerializedAs("shortcutMenu")]
    [Header("UI Elements")]
    [SerializeField]
    private XRShortcutUI xrShortcutUI;
    [FormerlySerializedAs("xrLayerMenu")] [FormerlySerializedAs("LayerMenu")] [SerializeField]
    XRNodeListUI xrNodeListUI;
    [FormerlySerializedAs("AnimationMenu")] [SerializeField]
    AnimationUI animationUI;
    [SerializeField]
    SettingsMenu SettingsMenu;

    [SerializeField] private GameObject SideMenu;
    [SerializeField] private CourseMenu courseMenu;
    
    [Header("Logic Components")]
    [SerializeField]
    private AudioManager audioManager;
    private Model3DView modelView;

    public override Model3DView ModelView
    {
        get
        {
            return modelView;
        }
        set
        {
            if (ModelView != value)
            {
                modelView = value;
                InitializeComponents();
                OnModelViewSet?.Invoke(modelView);
            }
        }
    }

    private void InitializeComponents()
    {
        
        
        //Hook up UI components
        animationUI.Animator = modelView.animator;
        xrNodeListUI.StateController =modelView.stateController;
        xrNodeListUI.NodeController = modelView.nodeController; //TODO: Make faster
        
        xrShortcutUI.ModelView = modelView;
        
        SettingsMenu.animator = modelView.animator;
        SettingsMenu.clippingPlaneController = modelView.clippingPlaneController;
        SettingsMenu.stateController = modelView.stateController;
        SettingsMenu.nodeController = modelView.nodeController;
        SettingsMenu.modelView = modelView;
        SettingsMenu.annotationController = modelView.annotationController;
        
        audioManager.NodeController = modelView.nodeController;
        if (modelView.CurrentModel != null)
        {
            animationUI.gameObject.SetActive(modelView.CurrentModel.IsAnimated);
            SettingsMenu.SetModel(modelView.CurrentModel);
            courseMenu.LoadCourses(modelView.CurrentModel.name);
        }

        GetComponent<AttachToTransform>().targetTransform = modelView.nodeContainer.transform;
        SideMenu.GetComponent<FixedRelativeAngle>().anchor = modelView.nodeContainer.transform;
        SideMenu.GetComponent<FixedDistance>().target = modelView.nodeContainer.transform;
        animationUI.GetComponent<FixedRelativeAngle>().anchor = modelView.nodeContainer.transform;
        animationUI.GetComponent<FixedDistance>().target = modelView.nodeContainer.transform;
        animationUI.GetComponent<MoveWithScale>().target = modelView.nodeContainer.transform;
        SideMenu.GetComponent<MoveWithScale>().target = modelView.nodeContainer.transform;
        
        modelView.OnModelSet += (model) =>
        {
            animationUI.gameObject.SetActive(model.IsAnimated);
            SettingsMenu.SetModel(model);
            courseMenu.LoadCourses(model.name);
        };
    }


    public override void DestroyModel()
    {
        modelView.DestroyModel();
        Destroy(gameObject);
    }
    
    
    // private void SetLoadingState(bool isLoading, string loadingText = "Loading")
    // {
    //     shortcutMenu.SetActive(!isLoading);
    //     AnimationMenu.gameObject.SetActive(!isLoading && currentModel != null && currentModel.IsAnimated);
    // }

    public void ToggleBoundingBox()
    {
        BoundsControl boundsControl = modelView.nodeContainer.GetComponent<BoundsControl>();
        boundsControl.HandlesActive = !boundsControl.HandlesActive;
    }
    
    
    
}

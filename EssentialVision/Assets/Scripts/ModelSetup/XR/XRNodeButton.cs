using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class XRNodeButton : MonoBehaviour, INodeButton
{
    [Header("UI Components")]
    [SerializeField]
    private TextMeshProUGUI buttonLabel;
    [SerializeField]
    public PressableButton SelectionButton;
    [SerializeField]
    public PressableButton ToggleVisibilityButton;
    [SerializeField]
    public PressableButton ToggleOpaqueButton;
    [SerializeField] public PressableButton ToggleExpandButton;

    [Header("Button Icons")]
    [SerializeField]
    private Sprite VisibleIcon;
    [SerializeField]
    private Sprite InvisibleIcon;
    [SerializeField]
    private Sprite OpaqueIcon;
    [SerializeField]
    private Sprite TransparentIcon;

    private bool isVisible = true;
    private bool isOpaque = true;
    private bool isExpanded = true;
    private bool isLeaf = true;
    private int level = 0;

    [SerializeField] private int indent = 20;

    private UnityEvent _onToggleVisibilityButton = new UnityEvent();
    private UnityEvent _onToggleOpacityButton = new UnityEvent();
    private UnityEvent _onToggleExpandButton = new UnityEvent();
    private UnityEvent _onClickSelectButton = new UnityEvent();

    public UnityEvent OnToggleVisibilityButton
    {
        get { return _onToggleVisibilityButton; }
        set { _onToggleVisibilityButton = value; }
    }

    public UnityEvent OnToggleOpacityButton
    {
        get { return _onToggleOpacityButton; }
        set { _onToggleOpacityButton = value; }
    }

    public UnityEvent OnToggleExpandButton
    {
        get { return _onToggleExpandButton; }
        set { _onToggleExpandButton = value; }
    }

    public UnityEvent OnClickSelectButton
    {
        get { return _onClickSelectButton; }
        set { _onClickSelectButton = value; }
    }
    

    public void Start()
    {
        OnToggleVisibilityButton = ToggleVisibilityButton.OnClicked;
        OnToggleOpacityButton = ToggleOpaqueButton.OnClicked;
        OnToggleExpandButton = ToggleExpandButton.OnClicked;
        OnClickSelectButton = SelectionButton.OnClicked;
    }
    public bool IsLeaf
    {
        get
        {
            return isLeaf;
        }
        set
        {
            isLeaf = value;
            if (ToggleExpandButton != null)
            {
                ToggleExpandButton.gameObject.SetActive(!isLeaf);
            }
        }
    }
    public bool IsExpanded
    {
        get
        {
            return isExpanded;
        }
        set
        {
            isExpanded = value;
            if (ToggleExpandButton != null)
            {
                FontIconSelector iconSelector = ToggleExpandButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonFontIcon")?.GetComponent<FontIconSelector>();
                if (iconSelector != null)
                {
                    iconSelector.CurrentIconName = isExpanded ? "Icon 58" : "Icon 60";
                }
                else
                {
                    Debug.LogWarning("ToggleExpandButton is missing the required icon FontIconSelector component.");
                }
            }
        }
    }

    public int Level
    {
        get
        {
            return level;
        }
        set
        {
            level = value;
            HorizontalLayoutGroup layoutGroup = GetComponent<HorizontalLayoutGroup>();
            if (layoutGroup!= null)
            {
                layoutGroup.padding.left = indent * level;
            }
        }
    }

    // Property to get and set the visibility of the layer
    public bool IsVisible
    {
        get { return isVisible; }
        set
        {
            isVisible = value;
            // Add null check for ToggleVisibilityButton before accessing its child components
            if (ToggleVisibilityButton != null)
            {
                Image visibilityIcon = ToggleVisibilityButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon")?.GetComponent<Image>();
                if (visibilityIcon != null)
                {
                    visibilityIcon.sprite = value ? VisibleIcon : InvisibleIcon;
                }
                else
                {
                    Debug.LogWarning("ToggleVisibilityButton is missing the required icon Image component.");
                }
            }
        }
    }

    // Property to get and set the opacity of the layer
    public bool IsOpaque
    {
        get { return isOpaque; }
        set
        {
            isOpaque = value;
            // Add null check for ToggleOpaqueButton before accessing its child components
            if (ToggleOpaqueButton != null)
            {
                Image opaqueIcon = ToggleOpaqueButton.transform.Find("Frontplate/AnimatedContent/Icon/UIButtonSpriteIcon")?.GetComponent<Image>();
                if (opaqueIcon != null)
                {
                    opaqueIcon.sprite = value ? OpaqueIcon : TransparentIcon;
                }
                else
                {
                    Debug.LogWarning("ToggleOpaqueButton is missing the required icon Image component.");
                }
            }
        }
    }


    // Method to set the layer information for the button
    public void SetNode(ModelNodeScriptableObject nodeInfo)
    {
        if (nodeInfo != null)
        {
            // Add null check for buttonLabel before setting its text
            if (buttonLabel != null)
            {
                buttonLabel.text = nodeInfo.Name;
            }
            else
            {
                Debug.LogWarning("buttonLabel is missing. Unable to set layer name.");
            }
        }
    }

}

using System.Collections.Generic;
using UnityEngine;

public class FixedDistance : MonoBehaviour
{
    public Transform target;
    public float distance = 1.0f;
    public float distanceDelta = 0.1f;
    public List<Collider> colliders = new List<Collider>();
    public bool useTargetChildrenColliders = true;
    
    // Start is called before the first frame update
    void Start()
    {
        // if (target != null)
        // {
        //     distance = Vector3.Distance(target.position, transform.position);
        // }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            return;
        }


        //Get closest point out of all colliders and the current distance to it
        Vector3 closestPoint = target.position;
        float currentDistance = Vector3.Distance(target.position, transform.position);
        if (useTargetChildrenColliders)
        {
            foreach (Collider collider in target.GetComponentsInChildren<Collider>())
            {
                if (collider != null)
                {
                    // Get the collider's bounds
                    Bounds bounds = collider.bounds;

                    // Get the corner points of the bounds
                    Vector3[] corners = new Vector3[8];
                    corners[0] = bounds.center + new Vector3(bounds.extents.x, bounds.extents.y, bounds.extents.z);
                    corners[1] = bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y, bounds.extents.z);
                    corners[2] = bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y, bounds.extents.z);
                    corners[3] = bounds.center + new Vector3(-bounds.extents.x, -bounds.extents.y, bounds.extents.z);
                    corners[4] = bounds.center + new Vector3(bounds.extents.x, bounds.extents.y, -bounds.extents.z);
                    corners[5] = bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y, -bounds.extents.z);
                    corners[6] = bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y, -bounds.extents.z);
                    corners[7] = bounds.center + new Vector3(-bounds.extents.x, -bounds.extents.y, -bounds.extents.z);

                    // Find the closest corner point
                    foreach (Vector3 corner in corners)
                    {
                        float distanceToPoint = Vector3.Distance(corner, transform.position);
                        if (distanceToPoint < currentDistance)
                        {
                            closestPoint = corner;
                            currentDistance = distanceToPoint;
                        }
                    }
                }
            }
        }
        
        float minDistance = distance - distanceDelta;
        float maxDistance = distance + distanceDelta;
        
        Ray ray = new Ray(target.position, transform.position - target.position);
        Vector3 closestPointOnRay = target.position + ray.direction * Vector3.Dot(closestPoint - target.position, ray.direction);
        float distanceToRay = Vector3.Distance(closestPointOnRay, closestPoint);
        float closestPointOnRayDistance = Vector3.Distance(closestPointOnRay, target.position);
        
        float minDistanceOnRay = 0f;
        float maxDistanceOnRay = float.MaxValue;
        if (distanceToRay < minDistance)
        {
            minDistanceOnRay = closestPointOnRayDistance + Mathf.Sqrt(minDistance * minDistance - distanceToRay * distanceToRay);
        }
        if (distanceToRay < maxDistance)
        {
            maxDistanceOnRay = closestPointOnRayDistance + Mathf.Sqrt(maxDistance * maxDistance - distanceToRay * distanceToRay);
        }
        
        float newDistanceOnRay = Mathf.Clamp(Vector3.Distance(target.position,transform.position), minDistanceOnRay, maxDistanceOnRay);
        transform.position = target.position + newDistanceOnRay * ray.direction;

    }
}

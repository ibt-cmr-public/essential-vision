using UnityEngine;

public class AttachToTransform : MonoBehaviour
{

    [SerializeField] public Transform targetTransform;
    

    // Update is called once per frame
    void Update()
    {
        if (targetTransform != null)
        {
            transform.position = targetTransform.position;
        }
    }
}

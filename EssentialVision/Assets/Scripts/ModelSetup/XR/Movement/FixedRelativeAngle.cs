using UnityEngine;

//Keeps a fixed angle between this object, the anchor and the target transform
public class FixedRelativeAngle : MonoBehaviour
{

    public Transform anchor;
    public Transform target;
    public float azimuthalAngle = 0f;
    public float polarAngle = 90f;
    public float polarAngleDelta = 10f;
    public float azimuthalAngleDelta = 10f;
    public bool setAnglesAtStart = false;
    
    
    // Start is called before the first frame update
    void Start()
    {
        if (target == null && Camera.main != null)
        {
            target = Camera.main.transform;
        }
        //if achor or target null
        if (anchor == null  || target == null)
        {
            return;
        }

        if (setAnglesAtStart)
        {

            Vector3 targetToAnchor = anchor.position - target.position;
            Vector3 targetToAnchorFlattened = new Vector3(targetToAnchor.x, targetToAnchor.y, 0);
            Vector3 anchorToThis = transform.position - anchor.position;
            Vector3 anchorToThisFlattened = new Vector3(anchorToThis.x, anchorToThis.y, 0);
            azimuthalAngle = Vector3.SignedAngle(anchorToThisFlattened, anchorToThis, Vector3.right);
            polarAngle = Vector3.SignedAngle(targetToAnchorFlattened, anchorToThisFlattened, Vector3.up);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (anchor == null || target == null)
        {
            return;
        }
        
        Vector3 targetToAnchor =  anchor.position - target.position;
        Vector3 targetToAnchorFlattened = new Vector3(targetToAnchor.x, targetToAnchor.y, 0);
        Vector3 anchorToThis = transform.position - anchor.position;
        Vector3 anchorToThisFlattened = new Vector3(anchorToThis.x, anchorToThis.y, 0);
        float currentAzimuthalAngle = Vector3.SignedAngle(anchorToThisFlattened, anchorToThis, Vector3.right);
        float currentPolarAngle = Vector3.SignedAngle(targetToAnchorFlattened, anchorToThisFlattened, Vector3.up);
        float newAzimuthalAngle = Mathf.Clamp(currentAzimuthalAngle, azimuthalAngle - azimuthalAngleDelta, azimuthalAngle + azimuthalAngleDelta);
        float newPolarAngle = Mathf.Clamp(currentPolarAngle, polarAngle - polarAngleDelta, polarAngle + polarAngleDelta);

        Vector3 desiredDirectionFromAnchor =  targetToAnchorFlattened.normalized;
        desiredDirectionFromAnchor =   Quaternion.AngleAxis(newPolarAngle, Vector3.up) * desiredDirectionFromAnchor;
        desiredDirectionFromAnchor =   Quaternion.AngleAxis(newAzimuthalAngle, Vector3.Cross(Vector3.up, desiredDirectionFromAnchor)) * desiredDirectionFromAnchor;
        float distance = Vector3.Distance(anchor.position, transform.position);
        transform.position = anchor.position + distance * desiredDirectionFromAnchor;
    }
}

using MixedReality.Toolkit.UX;
using UnityEngine;

public class XRShortcutUI : ShortcutUI
{
    // Menu buttons
    [SerializeField] private PressableButton closeModelButton;
    [SerializeField] private PressableButton showMenuButton;
    [SerializeField] private PressableButton showBoundingBoxButton;
    [SerializeField] private PressableButton freeMovementButton;
    [SerializeField] private PressableButton explodeViewButton;
    [SerializeField] private PressableButton resetViewButton;

    // Controllers
    [SerializeField] private UIController uiController;

    private XRModelSetup XRModelSetup
    {
        get
        {
            return (XRModelSetup)modelSetup;
        }
    }

    public void OnClickShowMenu()
    {
        if (uiController != null)
        {
            uiController.ToggleSideMenu();
        }
    }
    
    public void OnClickShowBoundingBox()
    {
        if (modelSetup != null)
        {
            XRModelSetup.ToggleBoundingBox();
        }
    }
    
}
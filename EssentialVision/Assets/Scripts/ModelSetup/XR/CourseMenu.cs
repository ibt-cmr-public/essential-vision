using System.Collections.Generic;
using MixedReality.Toolkit.UX;
using MixedReality.Toolkit.UX.Experimental;
using TMPro;
using UnityEngine;

public class CourseMenu : MonoBehaviour
{
    // Serialized Fields - Model and UI Components
    [Header("UI Components")]
    [SerializeField] private TextMeshProUGUI courseDescriptionText;
    [SerializeField] private TextMeshProUGUI courseTitleText;
    [SerializeField] private VirtualizedScrollRectList courseScrollRectList;
    [SerializeField] private PressableButton startCourseButton;
    
    [Header("Other Managers")]
    [SerializeField] private ModelCheckpointManager checkpointManager;
    private string selectedCheckpointGraphName = null;


    public string SelectedCheckpointGraphName
    {
        get
        {
            return selectedCheckpointGraphName;
        }
        set
        {
            selectedCheckpointGraphName = value;
            if (value == null)
            {
                startCourseButton.gameObject.SetActive(false);
                courseDescriptionText.gameObject.SetActive(false);
                courseTitleText.gameObject.SetActive(false);
            }
            else
            {
                ModelCheckpointGraph selectedCourse = LibraryManager.Instance.GetGraph(selectedCheckpointGraphName);
                startCourseButton.gameObject.SetActive(true);
                courseDescriptionText.gameObject.SetActive(true);
                courseTitleText.gameObject.SetActive(true);
                courseDescriptionText.SetText(selectedCourse.Description);
                courseTitleText.SetText(selectedCourse.Name);
            }
        }
    }

    
    public void OnClickStartCourse()
    {
        checkpointManager.EnterCheckpointGraph(LibraryManager.Instance.GetGraph(selectedCheckpointGraphName));
    }


    public void LoadCourses(string modelName)
    {
        List<ModelCheckpointGraph> courses = LibraryManager.Instance.CheckpointGraphsForModel(modelName);
        int numCourses = courses.Count;
        courseScrollRectList.SetItemCount(numCourses);
        
        courseScrollRectList.OnVisible = (go, i) =>
        {
            PressableButton button = go.GetComponent<PressableButton>();
            ModelCheckpointGraph course = courses[i];
            button.GetComponentInChildren<TextMeshProUGUI>().SetText(course.Name);
            button.OnClicked.AddListener(() => { SelectedCheckpointGraphName = courses[i].Name; });
        };
        
        courseScrollRectList.OnInvisible = (go, i) =>
        {
            PressableButton button = go.GetComponent<PressableButton>();
            button.OnClicked.RemoveAllListeners();
        };
        
        for (int i = 0; i < courses.Count; i++)
        {
            string courseName = courses[i].Name;
            courseScrollRectList.TryGetVisible(i, out GameObject visibleObject);
            if (visibleObject != null)
            {
                CourseButton courseButton = visibleObject.GetComponent<CourseButton>();
                courseButton.Text = courseName;
            }
        }
    }
}

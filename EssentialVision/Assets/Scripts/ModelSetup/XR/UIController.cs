using System.Collections.Generic;
using MixedReality.Toolkit.UX;
using Unity.Netcode;
using UnityEngine;

public class UIController : NetworkBehaviour
{
    [SerializeField] private GameObject ExtendedSideMenu;
    [SerializeField] private bool sideMenuActive = true;
    [SerializeField] private GameObject AnimationMenu;
    
    //Main Menu buttons
    [SerializeField] private PressableButton closeModelButton;
    [SerializeField] private PressableButton sideMenuButton;
    [SerializeField] private PressableButton boundingBoxButton;
    [SerializeField] private PressableButton freeMoveButton;
    [SerializeField] private PressableButton explodeViewButton;
    [SerializeField] private PressableButton resetViewButton;
    
    //Side Menu tabs
    [SerializeField] private ToggleCollection sideMenuTabCollection;
    [SerializeField] private GameObject layersTab;
    [SerializeField] private GameObject settingsTab;
    [SerializeField] private GameObject modelInfoTab;

    
    private int CurrentTab
    {
        get
        {
            return sideMenuTabCollection.CurrentIndex;
        }
        set
        {
            sideMenuTabCollection.CurrentIndex = value;
        }
    }
    private bool SideMenuActive
    {
        get
        {
            return ExtendedSideMenu.activeSelf;
        }
        set
        {
            ExtendedSideMenu.SetActive(value);
        }
    }
    private bool AnimationMenuActive
    {
        get
        {
            return AnimationMenu.activeSelf;
        }
        set
        {
            AnimationMenu.SetActive(value);
        }
    }
    private List<bool> TabsActive
    {
        get
        {
            List<bool> tabsActive = new List<bool>();
            foreach (var toggle in sideMenuTabCollection.Toggles)
            {
                tabsActive.Add(toggle.gameObject.activeSelf);
            }

            return tabsActive;
        }
        set
        {
            // Ensure the value list has the same length as the number of toggles
            if (value.Count == sideMenuTabCollection.Toggles.Count)
            {
                // Iterate over both the value list and the toggles
                for (int i = 0; i < value.Count; i++)
                {
                    sideMenuTabCollection.Toggles[i].gameObject.SetActive(value[i]);
                }
            }
            else
            {
                Debug.LogError("Number of elements in the 'value' list does not match the number of toggles.");
            }
        }
    }
    public void ToggleSideMenu()
    {
        SideMenuActive = !SideMenuActive;
    }
    public void ToggleAnimationMenu()
    {
        AnimationMenuActive = !AnimationMenuActive;
    }

    //Set UI State
    public UIState state
    {
        get
        {
            UIState currentState = new UIState();
            return currentState;
        }
    }
    public void SetUIState(UIState state)
    {
        SetUIStateServerRpc(state);
    }

    [ServerRpc (RequireOwnership = false)]
    private void SetUIStateServerRpc(UIState state)
    {
        if (state == null) return;   
    }
}

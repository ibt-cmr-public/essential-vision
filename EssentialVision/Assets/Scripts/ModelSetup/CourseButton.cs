using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;

public class CourseButton : PressableButton
{
    [SerializeField] private TextMeshProUGUI buttonText;
    private string text;

    public string Text
    {
        get
        {
            return text;
        }
        set
        {
            text = value;
            buttonText.SetText(text);
        }
    }
}

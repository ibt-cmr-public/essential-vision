using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

// Class for layer menu that allows for layer selection and layer visibility controls
public class NodeListUI: MonoBehaviour
{
    // Serialized Fields - Model and UI Components
    [Header("Model and UI Components")] 
    [SerializeField] private NodeUI selectedNodeUI;
    [FormerlySerializedAs("layerScrollRectDropdown")] [SerializeField] private VirtualizedScrollDropdown layerScrollDropdown;

    [Header("Settings")] [SerializeField] 
    private bool showModelRoot = false;
    
    // Components to manage layer states and selection buttons
    private Guid displayedLayerGuid;
    protected NodeVisibilityStateController stateController;
    protected NodeController _nodeController;
    private bool UIDirty = false;

    
    private void SetUIDirty()
    {
        UIDirty = true;
    }

    //Dictionary mapping button indices to node IDs
    private Dictionary<int, Guid> IDMapping = new Dictionary<int, Guid>();
    private Guid buttonIDToNodeID(int buttonID)
    {
        if (IDMapping.ContainsKey(buttonID))
        {
            return IDMapping[buttonID];
        }
        else
        {
            return Guid.Empty; 
        }
    }
    
    private int nodeIDToButtonID(Guid nodeID)
    {
        // Assuming you want to find the first buttonID that matches the given nodeID
        foreach (var pair in IDMapping)
        {
            if (pair.Value == nodeID)
            {
                return pair.Key;
            }
        }
        return -1; 
    }
    
    public int NumButtons 
    {
        get => IDMapping.Count;
    }
    
    // Property to get/set the LayerStateController and subscribe to LayerVisibilityChanged event
    public NodeVisibilityStateController StateController
    {
        get => stateController;
        set
        {
            if (stateController == value)
            {
                return;
            }
            stateController = value;
            stateController.NodeVisibilityChanged += SetUIDirty;
            SetUIDirty();
        }
    }
    

    // Property to get/set the LayerController 
    public NodeController NodeController
    {
        get => _nodeController;
        set
        {
            if (_nodeController == value)
            {
                return;
            }
            _nodeController = value;
            IDMapping.Clear();
            foreach (ModelNode node in _nodeController.Nodes)
            {
                OnNodeAdded(node);
            }
            
            layerScrollDropdown.OnAssignedNode = (go, buttonNodeID) =>
            {
                INodeButton button = go.GetComponent<INodeButton>();
                Guid nodeID = buttonIDToNodeID(buttonNodeID);
                ModelNodeScriptableObject nodeInfo = _nodeController.Node(nodeID).NodeInfo;
                button.SetNode(nodeInfo);
                button.IsVisible = !StateController.IsHidden(nodeID);
                button.IsOpaque = !StateController.IsFaded(nodeID);
                button.OnClickSelectButton.AddListener(() => { NodeController.SelectedNodeGuid = nodeID; });
                button.OnToggleVisibilityButton.AddListener(() => { StateController.ToggleHideNode(nodeID); });
                button.OnToggleOpacityButton.AddListener(() => { StateController.ToggleFadeNode(nodeID); });
                button.OnToggleExpandButton.AddListener(() => { layerScrollDropdown.ToggleNodeExpanded(buttonNodeID); });
            };

            layerScrollDropdown.OnUnassignedNode = (go, i) =>
            {
                INodeButton button = go.GetComponent<INodeButton>();
                button.OnClickSelectButton.RemoveAllListeners();
                button.OnToggleVisibilityButton.RemoveAllListeners();
                button.OnToggleOpacityButton.RemoveAllListeners();
                button.OnToggleExpandButton.RemoveAllListeners();
            };

            _nodeController.NodeSelected += OnNodeSelected;
            _nodeController.NodeAdded += OnNodeAdded;
        }
    }

    public void OnNodeAdded(ModelNode node)
    {
        int newButtonID = -1;
        
        if (node.IsRoot && !showModelRoot)
        {
            return;
        }
        
        if (node.Parent == null || (node.Parent.IsRoot && !showModelRoot))
        {
            newButtonID = layerScrollDropdown.AddNode(-1);
        }
        else
        {
            int parentButtonID = nodeIDToButtonID(node.Parent.Guid);
            newButtonID = layerScrollDropdown.AddNode(parentButtonID);
        }
       
        IDMapping.Add(newButtonID, node.Guid);
    }
    
    public void OnNodeSelected(Guid nodeID)
    {
        SetUIDirty();
    }
    
    // Methods to handle button clicks
    public void OnClickHide()
    {
        StateController.ToggleHideNode(displayedLayerGuid);
    }

    public void OnClickHideOthers()
    {
        StateController.HideOtherLayers(displayedLayerGuid, !stateController.OtherLayersHidden(displayedLayerGuid));
    }


    public void OnClickFade()
    {
        StateController.ToggleFadeNode(displayedLayerGuid);
    }

    public void OnClickFadeOthers()
    {
        // Flags to track if other layers are hidden/faded
        StateController.FadeOtherLayers(displayedLayerGuid, !stateController.OtherLayersFaded(displayedLayerGuid));
    }

    public void OnClickShowAll()
    {
        StateController.ShowAll();
    }

    public void OnClickUnfadeAll()
    {
        StateController.UnfadeAll();
    }


    // Method to update the UI elements based on layer visibility and fade states
    protected virtual void UpdateUI()
    {
        
        if (NodeController == null | StateController == null)
        {
            return;
        }

        
        Guid selectedNodeGuid = _nodeController.SelectedNodeGuid;
        displayedLayerGuid = selectedNodeGuid;

        //Update selected layer view
        if (selectedNodeGuid == Guid.Empty || NodeController.SelectedNode == null)
        {
            selectedNodeUI.SetNode(null);
        }
        else
        {
            ModelNodeScriptableObject selectedNodeInfo = _nodeController.SelectedNode.NodeInfo;    
            // Set the description text based on the selected layer
            selectedNodeUI.SetNode(selectedNodeInfo);
        }
        
        
        // Update layer buttons 
        foreach (int buttonID in IDMapping.Keys) 
        {
            layerScrollDropdown.TryGetVisible(buttonID, out GameObject visibleObject);
            ModelNode node = _nodeController.Node(buttonIDToNodeID(buttonID));
            if (visibleObject != null)
            {
                XRNodeButton button = visibleObject.GetComponent<XRNodeButton>();
                button.SetNode(_nodeController.Node(node.Guid).NodeInfo);
                button.IsVisible = !node.IsHidden;
                button.IsOpaque = !node.IsFaded;
            }
        }
        
    }

    // Unsubscribe from LayerVisibilityChanged event when the object is destroyed
    private void OnDestroy()
    {
        if (StateController != null)
        {
            StateController.NodeVisibilityChanged -= SetUIDirty;
        }
        if (NodeController != null)
        {
            //Unsubscribe from layer controller events
        }
    }

    private void Update()
    {
        if (UIDirty)
        {
            UpdateUI();
            UIDirty = false;
        }
    }
}
using UnityEngine.Events;

public interface INodeButton: IComponent, IDropdownItem 
{
    public void SetNode(ModelNodeScriptableObject node);
    public bool IsVisible { get; set; }
    public bool IsOpaque { get; set; }

    public UnityEvent OnToggleExpandButton { get; set; }
    public UnityEvent OnToggleVisibilityButton { get; set; }
    public UnityEvent OnToggleOpacityButton { get; set; }
    public UnityEvent OnClickSelectButton { get; set; }

}

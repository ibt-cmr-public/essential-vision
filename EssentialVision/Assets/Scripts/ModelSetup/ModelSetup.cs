using System;
using UnityEngine;

public abstract class ModelSetup: MonoBehaviour
{
    
    private Guid instanceGuid = Guid.NewGuid();
    public Guid InstanceGuid
    {
        get { return instanceGuid; }
    }
    public Action<Model3DView> OnModelViewSet;
    public abstract Model3DView ModelView { get; set; }
    public abstract void DestroyModel();

    public virtual void Hide()
    {
        this.gameObject.SetActive(false);
    }
    
    public virtual void Show()
    {
        this.gameObject.SetActive(true);
    }
    
    public string ModelName
    {
        get
        {
            return ModelView?.CurrentModel?.Name??"";
        }
    }
}

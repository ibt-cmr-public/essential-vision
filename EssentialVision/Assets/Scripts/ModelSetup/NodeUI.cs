using UnityEngine;

public abstract class NodeUI : MonoBehaviour
{
    public abstract void SetNode(ModelNode node);
    protected ModelNode node;
}

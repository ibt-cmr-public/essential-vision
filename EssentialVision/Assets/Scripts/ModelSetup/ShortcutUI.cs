﻿using UnityEngine;
using UnityEngine.Serialization;

public class ShortcutUI: MonoBehaviour
{
    protected Model3DView modelView;
    [FormerlySerializedAs("modelUI")] [FormerlySerializedAs("modelInstance")] [SerializeField] protected ModelSetup modelSetup;
    
    public Model3DView ModelView
    {
        get
        {
            return modelView;
        }
        set
        {
            if (ModelView != value)
            {
                modelView = value;
            }
        }
    }
    
    public void OnClickCloseModel()
    {
        if (modelSetup != null)
        {
            modelSetup.DestroyModel();
        }
    }

    public void OnToggleMovementType()
    {
        if (modelView != null && modelView.inputController != null)
        {
            modelView.inputController.IndividualMovementActive = !modelView.inputController.IndividualMovementActive;
        }
    }

    public void OnClickExplodeView()
    {
        if (modelView != null && modelView.nodePositionController != null)
        {
            modelView.nodePositionController.SetExplodeTransforms();
        }
    }

    public void OnClickResetView()
    {
        if (modelView != null && modelView.nodePositionController != null)
        {
            modelView.nodePositionController.SetDefaultTransforms();
        }
    }
    
}
using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class ModelCheckpoint
{
    public CheckpointType checkpointType;
    public List<IModelCheckpointTransitionEffect> transitionEffects = new List<IModelCheckpointTransitionEffect>();
    public List<ModelCheckpointTransition> transitions = new List<ModelCheckpointTransition>();
    public bool keepModifiedState = false;
    public int dataIndex;
    public int positiveScore = 1;
    public int negativeScore = 0;
}

[Serializable]
public class ModelCheckpointTransition
{
    public int targetCheckpointIndex;
    [SerializeReference, SerializeReferenceButton]
    public List<IModelCheckpointTransitionCondition> conditions = new List<IModelCheckpointTransitionCondition>();
}

[Serializable]
[CreateAssetMenu(fileName = "New Model Checkpoint Graph", menuName = "EssentialVision/Model Checkpoint Graph")]
public class ModelCheckpointGraph : ScriptableObject
{
    public Guid guid = Guid.NewGuid();
    public string modelName;
    public string Name;
    public string Description;
    public List<ScriptableObject> checkpointData = new List<ScriptableObject>();
    public List<ModelCheckpoint> checkpoints = new List<ModelCheckpoint>();
    
    public int numCheckpoints
    {
        get
        {
            return checkpoints.Count;
        }
    }
    
    public int numData
    {
        get
        {
            return checkpointData.Count;
        }
    }
    
    public int maxScore
    {
        get
        {
            int score = 0;
            foreach (ModelCheckpoint checkpoint in checkpoints)
            {
                score += maxCheckpointScore(checkpoint);
            }

            return score;
        }
    }

    public int score(List<object> answers)
    {
        int currentScore = 0;
        if (answers.Count != checkpointData.Count)
        {
            Debug.LogError("Answers count does not match checkpoint data count");
            return currentScore;
        }
        foreach (ModelCheckpoint checkpoint in checkpoints)
        {
            currentScore += checkpointScore(checkpoint, answers[checkpoint.dataIndex]);
        }

        return currentScore;
    }
    
    public int checkpointScore(ModelCheckpoint checkpoint, object answer)
    {
        if (checkpoint.checkpointType == CheckpointType.MultipleChoiceQuestion)
        {
            MultipleChoiceCheckpointData question = (MultipleChoiceCheckpointData) checkpointData[checkpoint.dataIndex];
            if (answer != null && question.correctAnswerIndex == (int) answer)
            {
                return checkpoint.positiveScore;
            }
            else
            {
                return checkpoint.negativeScore;
            }
        }
        else
        {
            return 0;
        }
    }

    public int maxCheckpointScore(ModelCheckpoint checkpoint)
    {
        if (checkpoint.checkpointType == CheckpointType.MultipleChoiceQuestion)
        {
            return checkpoint.positiveScore;
        }
        else
        {
            return 0;
        }
    }
    
}

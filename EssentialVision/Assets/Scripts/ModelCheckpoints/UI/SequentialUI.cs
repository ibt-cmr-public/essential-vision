using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SequentialUI : ModelCheckpointUI
{
    [SerializeField] private TextMeshProUGUI titleText;
    [SerializeField] private TextMeshProUGUI descriptionText;
    [SerializeField] private Image image;
    public void InitializeUI(InformationCheckpointData sequentialData)
    {
        titleText.SetText(sequentialData.title);
        descriptionText.SetText(sequentialData.description);
        if (sequentialData.image != null)
        {
            image.sprite = sequentialData.image;
            image.enabled = true;
        }
        else
        {
            image.enabled = false;
        }
    }
    
}

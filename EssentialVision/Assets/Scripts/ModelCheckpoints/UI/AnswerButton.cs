using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour
{
    public enum FeedbackState
    {
        Correct,
        Neutral,
        Incorrect
    }
    
    [SerializeField] private TextMeshProUGUI answerText;
    [SerializeField] private PressableButton button;
    [SerializeField] private GameObject feedbackImage;
    [SerializeField] private FontIconSelector iconSelector;
    [SerializeField] private RawImage backgroundImage;
    [SerializeField] private Color correctColor;
    [SerializeField] private Color incorrectColor;
    [SerializeField] private Color neutralColor;

    public void SetFeedbackMode(bool feedbackMode, FeedbackState feedbackState = FeedbackState.Neutral)
    {
        if (feedbackMode)
        {
            button.enabled = false;
            switch (feedbackState)
            {
                case FeedbackState.Correct:
                    iconSelector.CurrentIconName = "Icon 57";
                    feedbackImage.SetActive(true);
                    backgroundImage.color = correctColor;
                    break;
                case FeedbackState.Incorrect:
                    iconSelector.CurrentIconName = "Icon 79";
                    feedbackImage.SetActive(true);
                    backgroundImage.color = incorrectColor;
                    break;
                case FeedbackState.Neutral:
                    feedbackImage.SetActive(false);
                    backgroundImage.color = neutralColor;
                    break;
            }
        }
        else
        {
            backgroundImage.color = neutralColor;
            feedbackImage.SetActive(false);
            button.enabled = true;
        }
    }
    public void SetAnswerText(string text)
    {
        answerText.SetText(text);
    }
    
    public float PreferredHeight()
    {
        return answerText.preferredHeight;
    }
}

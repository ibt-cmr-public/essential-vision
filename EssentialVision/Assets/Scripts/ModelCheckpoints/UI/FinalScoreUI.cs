using TMPro;
using UnityEngine;

public class FinalScoreUI : ModelCheckpointUI
{
    [SerializeField] private TextMeshProUGUI scoreText;
    public void InitializeUI(int score, int maxScore)
    {
        scoreText.SetText($"{score}/{maxScore}");
    }
    
    public void OnClickNext()
    {
        checkpointManager.TransitionToNext();
    }
    
    public void OnClickPrevious()
    {
        checkpointManager.TransitionToPrevious();
    }
    
}

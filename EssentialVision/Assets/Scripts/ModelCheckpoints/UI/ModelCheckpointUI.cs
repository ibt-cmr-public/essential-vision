using UnityEngine;


public enum CheckpointType
{
    Information,
    MultipleChoiceQuestion,
    MultipleChoiceFeedback,
    DragAndDropQuestion,
    DragAndDropFeedback,
    PlaceMarkerQuestion,
    PlaceMarkerFeedback,
    SelectLayerQuestion,
    SelectLayerFeedback,
    FreeformQuestion,
    FreeformFeedback,
    Score
}

public abstract class ModelCheckpointUI : MonoBehaviour
{
    [SerializeField] protected ModelCheckpointManager checkpointManager;
    
    public void OnClickNext()
    {
        checkpointManager.TransitionToNext();
    }
    
    public void OnClickPrevious()
    {
        checkpointManager.TransitionToPrevious();
    }
}

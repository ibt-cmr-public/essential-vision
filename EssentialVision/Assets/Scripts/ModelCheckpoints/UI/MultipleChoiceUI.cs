using System;
using System.Collections.Generic;
using System.Linq;
using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultipleChoiceUI : ModelCheckpointUI
{
    [SerializeField] private TextMeshProUGUI titleText;
    [SerializeField] private TextMeshProUGUI questionText;
    [SerializeField] private Image image;
    [SerializeField] private List<AnswerButton> answerButtons = new List<AnswerButton>();
    [SerializeField] private GridLayoutGroup answerButtonGrid;
    [SerializeField] private GameObject submitButton;
    [SerializeField] private GameObject previousButton;
    [SerializeField] private GameObject nextButton;
    private bool navigationEnabled = true;


    public void Start()
    {
        for (int i = 0; i < answerButtons.Count; i++)
        {
            var i1 = i;
            answerButtons[i].GetComponent<PressableButton>().OnClicked.AddListener(() => OnClickAnswer(i1));
        }
    }

    public void OnClickAnswer(int answerIdx)
    {
        for (int i = 0; i < answerButtons.Count; i++)
        {
            if (i != answerIdx) answerButtons[i].GetComponent<PressableButton>().ForceSetToggled(false);
        }
        checkpointManager.SetCurrentAnswer(answerIdx);
    }

    public void InitializeUI(MultipleChoiceCheckpointData multipleChoiceCheckpointData, int selectedAnswer = -1, bool feedbackMode = false, bool showNavigation = true)
    {
        //Set text
        titleText.SetText(multipleChoiceCheckpointData.title);
        questionText.SetText(multipleChoiceCheckpointData.question);
        if (multipleChoiceCheckpointData.image != null)
        {
            image.sprite = multipleChoiceCheckpointData.image;
            image.enabled = true;
        }
        else
        {
            image.enabled = false;
        }
        
        //Set answer buttons
        int numAnswers = multipleChoiceCheckpointData.answers.Count;
        for (int i = 0; i < answerButtons.Count; i++)
        {
            if (i >= numAnswers)
            {
                answerButtons[i].gameObject.SetActive(false);
                continue;
            }
            answerButtons[i].SetAnswerText(multipleChoiceCheckpointData.answers[i]);
            answerButtons[i].GetComponent<PressableButton>().ForceSetToggled(i == selectedAnswer);
            AnswerButton.FeedbackState feedbackState = AnswerButton.FeedbackState.Neutral;
            if (i == multipleChoiceCheckpointData.correctAnswerIndex)
            {
                feedbackState = AnswerButton.FeedbackState.Correct; 
            }
            else if (i == selectedAnswer)
            {
                feedbackState = AnswerButton.FeedbackState.Incorrect;
            }
            answerButtons[i].SetFeedbackMode(feedbackMode, feedbackState);
        }
        
        //Set grid size
        float maxAnswerHeight = answerButtons.Max(answerButton => answerButton.PreferredHeight()) + 20f;
        answerButtonGrid.cellSize = new Vector2(answerButtonGrid.cellSize.x, Math.Max(maxAnswerHeight, 40f));

        //Set navigation buttons
        submitButton.SetActive(!showNavigation);
        previousButton.SetActive(showNavigation);
        nextButton.SetActive(showNavigation);

        Canvas.ForceUpdateCanvases();
    }
    
    public new void OnClickNext()
    {
        checkpointManager.TransitionToNext();
    }
    
    
}

using System;
using System.Collections.Generic;

public interface IModelCheckpointTransitionEffect 
{
    public void Apply(Model3DView modelView);
}

public class FocusOnNodeTransitionEffect : IModelCheckpointTransitionEffect
{
    private Guid nodeID;
    private bool hideOthers = false;
    private bool resetNodePositions = true;
    private bool relativeToUser = true;
    
    public void Apply(Model3DView modelView)
    {
        modelView.FocusOnNode(nodeID, hideOthers, resetNodePositions, relativeToUser);
    }
}

public class CompareNodesTransitionEffect : IModelCheckpointTransitionEffect
{
    private List<Guid> nodeIDS;
    private bool radialPositioning = false;
    private bool relativeToUser = true;
    
    public void Apply(Model3DView modelView)
    {
        modelView.CompareNodes(nodeIDS, radialPositioning, relativeToUser);
    }
}

public class DragAndDropNodesTransitionEffect : IModelCheckpointTransitionEffect
{
    private List<Guid> nodeIDS;
    private bool relativeToUser = true;
    
    public void Apply(Model3DView modelView)
    {
        modelView.DragAndDropNodes(nodeIDS, relativeToUser);
    }
}

//Add addtional transition effects here later
//SetModelState
//All substates
//SetUIState
//SetNodeVisibility
//...



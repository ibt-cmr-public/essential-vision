using System;


public interface IModelCheckpointTransitionCondition
{
    void RegisterWithModelView(Model3DView modelView);
    void UnregisterWithModelView(Model3DView modelView);
}


[Serializable]
public class LayerSelectedTransitionCondition : IModelCheckpointTransitionCondition
{
    public int layerID;
    
    
    public void RegisterWithModelView(Model3DView modelView)
    {
    }
    
    public void UnregisterWithModelView(Model3DView modelView)
    {
    }
}

//Add addtional transition conditions here later
//Drag and drop completed
//Layer correctly positioned
//etc ...

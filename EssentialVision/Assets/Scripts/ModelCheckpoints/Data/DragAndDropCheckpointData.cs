using System;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDropCheckpointData: ScriptableObject
{
    public List<Guid> nodeIDS;
    public string title;
    public string description;
    public Sprite image = null;
}

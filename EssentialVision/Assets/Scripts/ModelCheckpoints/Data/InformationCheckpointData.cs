using UnityEngine;

[CreateAssetMenu(fileName = "Info", menuName = "EssentialVision/Model Checkpoints/Informational")]
public class InformationCheckpointData : ScriptableObject
{
    public string title;
    public string description;
    public Sprite image = null;
}

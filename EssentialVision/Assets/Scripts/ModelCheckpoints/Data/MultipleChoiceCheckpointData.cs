using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "MultipleChoiceQuestion", menuName = "EssentialVision/Model Checkpoints/Multiple Choice Question")]
public class MultipleChoiceCheckpointData : ScriptableObject
{
    public string title;
    public string question;
    public List<string> answers = new List<string>();
    public int correctAnswerIndex;
    public Sprite image = null;
}

using System.Collections.Generic;
using UnityEngine;

public class FreeformQuestionCheckpointData : ScriptableObject
{
    public string title;
    public string question;
    public List<string> correctAnswers = new List<string>();
    public Sprite image = null;
}
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class ModelCheckpointManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] private SequentialUI sequentialUI;
    [SerializeField] private MultipleChoiceUI multipleChoiceUI;
    [SerializeField] private FinalScoreUI finalScoreUI;
    [SerializeField] private GameObject checkpointUIContainer;
    [FormerlySerializedAs("checkpointWindow")] [SerializeField] private XRWindow checkpointXRWindow;
    
    [Header("Other components and managers")]
    [SerializeField] private Model3DView modelView;
    
    [Header("Checkpoint Logic")]
    private ModelCheckpointGraph checkpointGraph = null;
    private List<object> answers = null;
    private int currentCheckpointIndex = -1;
    
    private ModelCheckpoint currentCheckpoint
    {
        get
        {
            return currentCheckpointIndex == -1 ? null : checkpointGraph.checkpoints[currentCheckpointIndex];
        }
    }
    
    public void EnterCheckpointGraph(ModelCheckpointGraph _checkpointGraph, int startIdx = 0)
    {
        if (checkpointGraph != null)
        {
            ExitCheckpointGraph();
        }
        checkpointXRWindow.Open();
        checkpointGraph = _checkpointGraph;
        answers = Enumerable.Repeat((object)null, checkpointGraph.numData).ToList();
        TransitionToCheckpoint(startIdx);
    }
    
    public void ExitCheckpointGraph()
    {
        UnregisterTransitions(currentCheckpoint.transitions);
        checkpointGraph = null;
        answers = null;
        checkpointXRWindow.Close();
        currentCheckpointIndex = -1;
    }
    
    
    public void SetAnswer(int answerIdx, object answer)
    {
        answers[answerIdx] = answer;
    }

    public void SetCurrentAnswer(object answer)
    {
        answers[currentCheckpoint.dataIndex] = answer;
    }

    public void TransitionToNext()
    {
        if (currentCheckpointIndex + 1 < checkpointGraph.numCheckpoints)
        {
            TransitionToCheckpoint(currentCheckpointIndex + 1);
        }
    }

    public void TransitionToPrevious()
    {
        if (currentCheckpointIndex > 0)
        {
            TransitionToCheckpoint(currentCheckpointIndex - 1);
        }
    }
    
    public void ResetCurrentCheckpoint()
    {
        TransitionToCheckpoint(currentCheckpointIndex);
    }
    
    public void TransitionToCheckpoint(int targetCheckpointIndex)
    {
        //Exit graph if target checkoint index is -1
        if (targetCheckpointIndex == -1)
        {
            ExitCheckpointGraph();
            return;
        }

        //Exit last checkpoint
        if (currentCheckpoint != null)
        {
            if (currentCheckpoint.keepModifiedState)
            {
                //Fix keeping modified state somehow..
            }
            UnregisterTransitions(currentCheckpoint.transitions);    
        }
        
        //Enter target checkpoint
        ModelCheckpoint targetCheckpoint = checkpointGraph.checkpoints[targetCheckpointIndex];
        RegisterTransitions(targetCheckpoint.transitions);
        ApplyDefaultTransition(targetCheckpoint);
        foreach (IModelCheckpointTransitionEffect effect in targetCheckpoint.transitionEffects)
        {
            effect.Apply(modelView);
        }
        UpdateUI(targetCheckpoint);
        currentCheckpointIndex = targetCheckpointIndex;
    }

    private void ApplyDefaultTransition(ModelCheckpoint newCheckpoint = null)
    {
        if (newCheckpoint == null)
        {
            return;
        }
        switch (newCheckpoint.checkpointType)
        {
            case CheckpointType.DragAndDropQuestion:
            {
                DragAndDropCheckpointData checkpointData =
                    (DragAndDropCheckpointData)checkpointGraph.checkpointData[newCheckpoint.dataIndex];
                modelView.DragAndDropNodes(checkpointData.nodeIDS, true);
                break;
            }
            case CheckpointType.DragAndDropFeedback:
            {
                DragAndDropCheckpointData checkpointData =
                    (DragAndDropCheckpointData)checkpointGraph.checkpointData[newCheckpoint.dataIndex];
                modelView.DragAndDropNodes(checkpointData.nodeIDS, true);
                break;
            }
        }
    }

    private void UpdateUI(ModelCheckpoint newCheckpoint = null)
    {
        if (newCheckpoint == null)
        {
            return;
        }

        multipleChoiceUI.gameObject.SetActive(false);
        sequentialUI.gameObject.SetActive(false);
        finalScoreUI.gameObject.SetActive(false);
        switch (newCheckpoint.checkpointType)
        {
            case CheckpointType.Information:
            {
                sequentialUI.gameObject.SetActive(true);
                sequentialUI.InitializeUI((InformationCheckpointData)checkpointGraph.checkpointData[newCheckpoint.dataIndex]);
                break;
            }
            case CheckpointType.MultipleChoiceQuestion:
            {
                int dataIdx = newCheckpoint.dataIndex;
                multipleChoiceUI.gameObject.SetActive(true);
                int selectedAnswer = answers[dataIdx] == null
                    ? -1
                    : (int)answers[dataIdx];
                multipleChoiceUI.InitializeUI(
                    (MultipleChoiceCheckpointData)checkpointGraph.checkpointData[dataIdx], selectedAnswer, false);
                break;
            }
            case CheckpointType.MultipleChoiceFeedback:
            {
                int dataIdx = newCheckpoint.dataIndex;
                multipleChoiceUI.gameObject.SetActive(true);
                int selectedAnswer = answers[dataIdx] == null ? -1 : (int)answers[dataIdx];
                multipleChoiceUI.InitializeUI((MultipleChoiceCheckpointData)checkpointGraph.checkpointData[dataIdx], selectedAnswer, true);
                break;
            }
            case CheckpointType.Score:
            {
                finalScoreUI.gameObject.SetActive(true);
                finalScoreUI.InitializeUI(checkpointGraph.score(answers), checkpointGraph.maxScore);
                break;
            }
        }
    }

    private void RegisterTransitions(List<ModelCheckpointTransition> transitions)
    {
        foreach (ModelCheckpointTransition transition in transitions)
        {
            RegisterTransitionConditions(transition.conditions);
        }
    }
    
    private void UnregisterTransitions(List<ModelCheckpointTransition> transitions)
    {
        foreach (ModelCheckpointTransition transition in transitions)
        {
            UnregisterTransitionConditions(transition.conditions);
        }
    }

    private void RegisterTransitionConditions(List<IModelCheckpointTransitionCondition> conditions)
    {
        foreach (IModelCheckpointTransitionCondition condition in conditions)
        {
            condition.RegisterWithModelView(modelView);
        }
    }
    
    private void UnregisterTransitionConditions(List<IModelCheckpointTransitionCondition> conditions)
    {
        foreach (IModelCheckpointTransitionCondition condition in conditions)
        {
            condition.UnregisterWithModelView(modelView);
        }
    }

    
}

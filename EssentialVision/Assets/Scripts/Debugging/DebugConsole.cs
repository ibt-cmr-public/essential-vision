using TMPro;
using UnityEngine;

public class DebugConsole : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textMesh;

    // Maximum length for the text
    private int maxTextLength = 3000;

    // Use this for initialization
    void Start()
    {
        if (textMesh == null)
        {
            textMesh = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        }
    }

    void OnEnable()
    {
        Application.logMessageReceived += LogMessage;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= LogMessage;
    }

    public void LogMessage(string message, string stackTrace, LogType type)
    {
        // If the text is longer than the maximum length, remove characters from the beginning
        while (textMesh.text.Length + message.Length > maxTextLength)
        {
            int excessLength = (textMesh.text.Length + message.Length) - maxTextLength;
            int removeCount = Mathf.Min(excessLength, textMesh.text.Length);
            textMesh.text = textMesh.text.Remove(0, removeCount);
        }

        // Add the new message to the end
        textMesh.text += message + "\n";
    }
}
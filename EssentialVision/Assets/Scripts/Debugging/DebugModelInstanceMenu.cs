using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


public class DebugModelInstanceMenu : MonoBehaviour
{
    [FormerlySerializedAs("modelUI")] [FormerlySerializedAs("modelInstance")] [SerializeField] private ModelSetup modelSetup;
    [SerializeField] private DragAndDropBoxManager dragAndDropBoxManagerBoxManager;
    [SerializeField] private int focusNodeIdx = 0;

    [SerializeField] private List<int> comparisonNodeIDs = new List<int>();
    [SerializeField] private bool radialPositioning = false;
    
    // // Start is called before the first frame update
    // public void OnClickDebugButton1()
    // {
    //     modelInstance.FocusOnNode(focusNodeIdx);
    // }
    // public void OnClickDebugButton2()
    // {
    //     modelInstance.CompareNodes(comparisonNodeIDs, radialPositioning);
    // }
    // public void OnClickDebugButton3()
    // {
    //     modelInstance.DragAndDropNodes(comparisonNodeIDs);
    // }
}

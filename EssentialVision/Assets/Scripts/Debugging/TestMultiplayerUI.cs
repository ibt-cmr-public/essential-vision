using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class TestMultiplayerUI : MonoBehaviour
{

    [SerializeField] private Button HostButton;

    [SerializeField] private Button ClientButton;
    
    // Start is called before the first frame update
    private void Awake()
    {
        HostButton.onClick.AddListener(() =>
        {
            Debug.Log("Starting Host.");
            NetworkManager.Singleton.StartHost();
        });  
        
        ClientButton.onClick.AddListener(() =>
        {
            Debug.Log("Starting Client.");
            NetworkManager.Singleton.StartClient();
        });  
    }
}

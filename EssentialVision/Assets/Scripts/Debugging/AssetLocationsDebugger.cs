using System.Collections.Generic;
using UnityEngine;

public class AssetLocationsDebugger : MonoBehaviour
{
    // Singletion pattern
    public static AssetLocationsDebugger Instance { get; private set; }
    
    public List<AssetLocations> assetLocations = new List<AssetLocations>();
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
}

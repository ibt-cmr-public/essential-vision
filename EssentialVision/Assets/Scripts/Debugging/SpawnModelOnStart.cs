using Unity.Netcode;
using UnityEngine;

public class SpawnModelOnStart : MonoBehaviour
{
    
    public GameObject modelViewPrefab;
    public string modelGuid; 
    // Start is called before the first frame update
    void Start()
    {
        //Get Model
        NetworkManager.Singleton.StartHost();
        GameObject modelView = Instantiate(modelViewPrefab);
        modelView.GetComponent<NetworkObject>().Spawn();
        modelView.GetComponent<Model3DView>().SetModel(modelGuid);
    }
}

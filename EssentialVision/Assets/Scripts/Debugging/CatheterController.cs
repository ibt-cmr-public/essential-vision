using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CatheterController : MonoBehaviour
{
    [SerializeField]
    private string csv_filepath;
    public CatheterMovement catheterMovement;
    public bool useSecondaryPositions;
    List<Vector3> catheterPositions = new List<Vector3>();
    List<Vector3> secondaryPositions = new List<Vector3>();
    List<float> times = new List<float>();
    // Update is called once per frame
    // Time accumulator for tracking 100 ms intervals
    float timer = 0f;
    // Time interval in seconds (100 ms)
    float updateInterval = 0.1f;
    private int counter = 0;

    void Start()
    {
        ReadTabDelimitedText(csv_filepath);
    }
    
    public void ReadTabDelimitedText(string filePath)
    {
        // Open the text file for reading using a StreamReader
        using (StreamReader reader = new StreamReader(filePath))
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                // Skip processing if the line is empty
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                // Split the line into individual values using tab as the delimiter
                string[] values = line.Split('\t');

                // Parse the values into appropriate types and add them to respective lists
                float time = float.Parse(values[0]);
                Vector3 proximal = new Vector3(float.Parse(values[1])-260f, -float.Parse(values[3])+260f, float.Parse(values[2])-260f);
                Vector3 distal =  new Vector3(float.Parse(values[4])-260f, -float.Parse(values[6])+260f, float.Parse(values[5])-260f);

                times.Add(time);
                catheterPositions.Add(proximal);
                secondaryPositions.Add(distal);
            }
        }

        Debug.Log("Tab-delimited text file successfully read.");
    }
    
    void Update()
    {
        // Increment the timer based on elapsed time
        timer += Time.deltaTime;

        // Calculate the current index based on the elapsed time and wrap around if necessary
        int index = 0;
        if (times.Count > 0)
        {
            float totalTime = times[times.Count - 1]; // Total time in the list
            float elapsedTime = timer % totalTime; // Elapsed time considering wrap around

            // Find the index corresponding to the elapsed time
            for (int i = 0; i < times.Count; i++)
            {
                if (elapsedTime < times[i])
                {
                    index = i;
                    break;
                }
            }
        }

        // Update catheter positions based on the determined index
        if (useSecondaryPositions)
        {
            catheterMovement.UpdatePositions(catheterPositions[index], secondaryPositions[index]);
        }
        else
        {
            catheterMovement.UpdatePosition(catheterPositions[index]);
        }
    }

}

using System.Collections.Generic;
using System.Linq;
using UnityEngine;


//TODO: We could clean up this class by simply updating the active session using the activesessionmanager.activesessioninfo
//attribute and then subscribing to a single event in activesessionmanager when the active session info is updated 
//this would mean less events for the active session manager to invoke (right now we don;t need these events anyways) 


public class ActiveSessionUI : MonoBehaviour
{
    [SerializeField] private GameObject participantButtonList;
    [SerializeField] private GameObject participantButtonPrefab;
    protected Dictionary<string, IParticipantButton> participantButtons = new Dictionary<string, IParticipantButton>();
    private string selectedParticipantID = null;
    
    
    // Start is called before the first frame update
    protected ActiveSessionManager activeSessionManager
    {
        get { return ActiveSessionManager.Instance; }
    }
    
    
    public virtual string SelectedParticipantID
    {
        get
        {
            return selectedParticipantID;
        }
        set
        {
            selectedParticipantID = value;
        }
    }
    
    public void Start()
    {
        //Delete all children
        while (participantButtonList.transform.childCount > 0)
        {
            DestroyImmediate(participantButtonList.transform.GetChild(0).gameObject);
            participantButtons = new Dictionary<string, IParticipantButton>();
        }
        activeSessionManager.SessionJoined += UpdateUI;
        activeSessionManager.SessionStarted += UpdateUI;
        activeSessionManager.SessionUpdated += UpdateUI;
        UpdateUI();
    }
    
    public void OnEnable()
    {
        activeSessionManager.SessionJoined += UpdateUI;
        activeSessionManager.SessionStarted += UpdateUI;
        activeSessionManager.SessionUpdated += UpdateUI;
        UpdateUI();
    }
    
    public void OnDestroy()
    {
        activeSessionManager.SessionJoined -= UpdateUI;
        activeSessionManager.SessionStarted -= UpdateUI;
        activeSessionManager.SessionUpdated -= UpdateUI;
    }
    
    public void OnDisable()
    {
        ResetUI();
        activeSessionManager.SessionJoined -= UpdateUI;
        activeSessionManager.SessionStarted -= UpdateUI;
        activeSessionManager.SessionUpdated -= UpdateUI;
    }

    protected virtual void UpdateUI()
    {
        LobbyInfo lobbyInfo = activeSessionManager.activeLobbyInfo;
        if (lobbyInfo == null)
        {
            ResetUI();
            return;
        }

        foreach (PlayerInfo playerInfo in lobbyInfo.currentParticipants)
        {
            AddOrUpdateParticipantButton(playerInfo);
        }

        // Remove the participantButtons that are not in currentParticipants
        HashSet<string> playerIDsToRemove = new HashSet<string>(participantButtons.Keys);
        foreach (PlayerInfo playerInfo in lobbyInfo.currentParticipants)
        {
            playerIDsToRemove.Remove(playerInfo.playerId);
        }
        foreach (string playerID in playerIDsToRemove)
        {
            RemoveParticipantButton(playerID);
        }
    }
    
    protected virtual void ResetUI()
    {
        foreach (string playerID in participantButtons.Keys.ToList())
        {
            RemoveParticipantButton(playerID);
        }
    }
    
    //Button management
    protected virtual void RemoveParticipantButton(string playerID)
    {
        if (!participantButtons.ContainsKey(playerID)) return;
        DestroyImmediate(participantButtons[playerID].gameObject);
        participantButtons.Remove(playerID);
    }
    
    protected virtual void AddOrUpdateParticipantButton(PlayerInfo playerInfo)
    {
        if (participantButtons.ContainsKey(playerInfo.playerId))
        {
            participantButtons[playerInfo.playerId].SetPlayerInfo(playerInfo);
        }
        else
        {
            AddParticipantButton(playerInfo);
        }
    }
    
    protected virtual void AddParticipantButton(PlayerInfo playerInfo)
    {
        XRParticipantButton newButton = Instantiate(participantButtonPrefab, participantButtonList.transform).GetComponent<XRParticipantButton>();
        participantButtons.Add(playerInfo.playerId, newButton);
        newButton.SetPlayerInfo(playerInfo);
    }
    
    //UI event handlers
    public void OnClickExitSession()
    {
        activeSessionManager.ExitSession();
    }
    
    public void OnClickKickPlayer()
    {
        activeSessionManager.KickPlayer(selectedParticipantID);
    }


}

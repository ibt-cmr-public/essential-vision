using UnityEngine;

public class XRSharedExperienceUI : MonoBehaviour
{
    [SerializeField] private ActiveSessionManager activeSessionManager;
    [SerializeField] private XRWindow XRSharedExperienceWindow;

    private void Start()
    {
        activeSessionManager = ActiveSessionManager.Instance;
        activeSessionManager.SessionStarted += OnSessionStarted;
        activeSessionManager.SessionJoined += OnSessionStarted;
        activeSessionManager.SessionExited += OnSessionEnded;
    }

    private void OnDestroy()
    {
        if (activeSessionManager != null)
        {
            activeSessionManager.SessionStarted -= OnSessionStarted;
            activeSessionManager.SessionJoined -= OnSessionStarted;
            activeSessionManager.SessionExited -= OnSessionEnded;
        }
    }

    private void OnSessionStarted()
    {
        XRSharedExperienceWindow.CurrentViewIdx = 1;
    }
    
    private void OnSessionEnded()
    {
        XRSharedExperienceWindow.CurrentViewIdx = 0;
    }
    

}

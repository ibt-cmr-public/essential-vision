using TMPro;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public class XRLobbyButton : XRToggleButton, ILobbyButton
{
    [SerializeField]
    private TextMeshProUGUI buttonLabel;
    [SerializeField]
    private TextMeshProUGUI numParticipantsLabel;
    [SerializeField]
    private TextMeshProUGUI IPAddressLabel;
    
    public void SetLobbyData(LocalLobby lobby)
    {
        buttonLabel.text = lobby.name;
        numParticipantsLabel.text = lobby.participantCount + "/" + lobby.maxParticipantCount;
        IPAddressLabel.text = "IP: " + lobby.endpoint.Address.ToString(); 
    }

    public void SetLobbyData(Lobby lobby)
    {
        buttonLabel.text = lobby.Name;
        numParticipantsLabel.text = lobby.Players.Count + "/" + lobby.MaxPlayers;
    }
}

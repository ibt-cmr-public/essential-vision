using MixedReality.Toolkit;
using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;


public class XRActiveSessionUI : ActiveSessionUI
{
    //UI Elements
    [SerializeField] private TextMeshProUGUI sessionNameLabel;
    [SerializeField] private TextMeshProUGUI sessionTimerLabel;
    [SerializeField] private PressableButton removeParticipantButton;
    [SerializeField] private ToggleCollection participantToggleCollection;
    
    //Logic
    public override string SelectedParticipantID
    {
        set
        {
            base.SelectedParticipantID = value;
            removeParticipantButton.enabled = (value != null);
        }
    }

    protected override void AddParticipantButton(PlayerInfo playerInfo)
    {
        base.AddParticipantButton(playerInfo);
        StatefulInteractable buttonInteractable = participantButtons[playerInfo.playerId].gameObject.GetComponent<StatefulInteractable>();
        participantToggleCollection.Toggles.Add(buttonInteractable);
    }
    
    protected override void RemoveParticipantButton(string playerID)
    {
        if (!participantButtons.ContainsKey(playerID)) return;
        StatefulInteractable buttonInteractable = participantButtons[playerID].gameObject.GetComponent<StatefulInteractable>();
        participantToggleCollection.Toggles.Remove(buttonInteractable);
        base.RemoveParticipantButton(playerID);
    }

    protected override void ResetUI()
    {
        base.ResetUI();
        sessionNameLabel.SetText("");
        removeParticipantButton.gameObject.SetActive(false);
    }
    
    protected override void UpdateUI()
    {
        base.UpdateUI();
        removeParticipantButton.gameObject.SetActive(activeSessionManager.IsHost);
        sessionNameLabel.SetText(activeSessionManager.activeLobbyInfo.configuration.name);
    }
    
}

using TMPro;
using UnityEngine;

public class XRParticipantButton : XRToggleButton, IParticipantButton
{
    [SerializeField] private TextMeshProUGUI nameLabel;
    [SerializeField] private TextMeshProUGUI addressLabel;
    [SerializeField] private TextMeshProUGUI roleLabel;
    public void SetPlayerInfo(PlayerInfo playerInfo)
    {
        nameLabel.SetText(playerInfo.name);
        addressLabel.SetText(playerInfo.playerId);
    }
}

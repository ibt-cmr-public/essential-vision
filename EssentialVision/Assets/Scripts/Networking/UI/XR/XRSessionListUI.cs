using MixedReality.Toolkit.UX;
using UnityEngine;

public class XRSessionListUI : SessionListUI
{
    /// <summary>
    /// UI Components
    /// </summary>
    [SerializeField] private PressableButton joinSessionButton;
    [SerializeField] private PressableButton newSessionButton;
    [SerializeField] private ToggleCollection connectionToggleCollection;
    [SerializeField] private GameObject newSessionDialogPrefab;
    [SerializeField] protected PopupManager dialogManager;
    
    //Logic
    public override string SelectedRemoteSessionID
    {
        set
        {
            base.SelectedRemoteSessionID = value;
            if (!showLocalLobbies)
            {
                joinSessionButton.enabled = (value != null);
            }
        }
    }
    
    public override string SelectedLocalSessionID
    {
        set
        {
            base.SelectedLocalSessionID = value;
            if (showLocalLobbies)
            {
                joinSessionButton.enabled = (value != null);
            }
        }
    }

    //Unity methods
    protected override void Start()
    {
        base.Start();
        connectionToggleCollection.OnToggleSelected.AddListener(OnConnectionTypeToggleSelected);
    }


    public override void OnClickNewSession()
    {
        XRNewSessionDialog xrNewSessionDialog =
            (XRNewSessionDialog)dialogManager.SpawnPopup(PopupManager.Policy.DismissExisting,
                newSessionDialogPrefab);
        xrNewSessionDialog.positiveAction += () =>
        {
            activeSessionManager.StartSession(xrNewSessionDialog.EnteredLobbyConfiguration);
        };

    }

}


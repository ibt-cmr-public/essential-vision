using MixedReality.Toolkit.UX;
using UnityEngine;

public class XRNewSessionDialog : XRPopup
{

    [SerializeField]
    MRTKTMPInputField sessionNameInputField;
    [SerializeField]
    MRTKTMPInputField passcodeInputField;
    [SerializeField]
    MixedReality.Toolkit.UX.Slider maxParticipantsSlider;
    [SerializeField] private ToggleCollection anchorPlacementToggleCollection;
    [SerializeField] private PressableButton isRemoteButton;

    public bool passcodeActivated;

    public LobbyConfiguration EnteredLobbyConfiguration {
        get {
            LobbyConfiguration enteredConfiguration = new LobbyConfiguration();
            enteredConfiguration.name = sessionNameInputField.text != "" ? sessionNameInputField.text : "New Session";
            if (passcodeActivated)
            {
                enteredConfiguration.passcode = passcodeInputField.text;
            }
            else
            {
                enteredConfiguration.passcode = null;
            }

            enteredConfiguration.isRemote = isRemoteButton.IsToggled;
            enteredConfiguration.maxParticipantCount = (int)maxParticipantsSlider.Value;
            enteredConfiguration.localAnchorPlacement = anchorPlacementToggleCollection.CurrentIndex == 0;
            return enteredConfiguration;
        }
    }
}

using Unity.Services.Lobbies.Models;

public interface ILobbyButton: IToggleButton
{
    
    void SetLobbyData(Lobby lobby);
    void SetLobbyData(LocalLobby lobby);
    
}

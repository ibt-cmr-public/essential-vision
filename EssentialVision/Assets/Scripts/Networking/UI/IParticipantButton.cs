public interface IParticipantButton : IToggleButton
{
    public void SetPlayerInfo(PlayerInfo playerInfo);
}

using System.Collections.Generic;
using System.Linq;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public abstract class SessionListUI : MonoBehaviour
{
    //Managers
    [SerializeField] protected LocalLobbyManager localLobbyManager;
    [SerializeField] protected LobbyDiscovery remoteLobbyDiscovery;
    [SerializeField] protected ActiveSessionManager activeSessionManager;

    
    /// <summary>
    /// UI Prefabs
    /// </summary>
    [SerializeField] private GameObject sessionButtonPrefab;
    
    /// <summary>
    /// UI components
    /// </summary>
    [SerializeField] private GameObject sessionButtonList;
    private Dictionary<string, ILobbyButton> localLobbyButtons = new Dictionary<string, ILobbyButton>();
    private Dictionary<string, ILobbyButton> remoteLobbyButtons = new Dictionary<string, ILobbyButton>();
    
    //Logic
    [SerializeField] private bool searchOnEnable = true;
    private bool isInitialized = false;
    protected bool showLocalLobbies = true;
    private string selectedRemoteSessionID;
    private string selectedLocalSessionID;
    
    
    //Logic
    public bool ShowLocalLobbies
    {
        get { return showLocalLobbies; }
        set
        {
            if (showLocalLobbies != value)
            {
                showLocalLobbies = value;
                DestroyAllButtons();
                if (showLocalLobbies)
                {
                    UpdateLocalLobbyButtons();
                }
                else
                {
                    UpdateRemoteLobbyButtons();
                }
            }
        }
    }
    
    public virtual string SelectedRemoteSessionID { get; set; }
    public virtual string SelectedLocalSessionID { get; set;  }
    
    
    //Unity methods
    protected virtual void Start()
    {
        //Sanity checks
        if (remoteLobbyDiscovery == null) remoteLobbyDiscovery = FindObjectOfType<LobbyDiscovery>();
        if (remoteLobbyDiscovery == null)
        {
            Debug.LogWarning("The session list manager was unable to find a lobby discovery component.");
        }
        
        if (localLobbyManager== null) localLobbyManager = FindObjectOfType<LocalLobbyManager>();
        if (localLobbyManager == null)
        {
            Debug.LogWarning("The session list manager was unable to find a local lobby manager component.");
        }
        
        if (activeSessionManager == null) activeSessionManager = FindObjectOfType<ActiveSessionManager>();
        if (activeSessionManager == null)
        {
            Debug.LogWarning("The session list manager was unable to find an active session manager component.");
        }

        //Hook up events
        remoteLobbyDiscovery.LobbyListUpdated += UpdateRemoteLobbyButtons;
        localLobbyManager.LobbyListUpdated += UpdateLocalLobbyButtons;
        
        isInitialized = true;
        
        //Remove all scene-placed children 
        DestroyAllButtons();
        
        //Start search
        if (searchOnEnable)
        {
            StartLocalLobbySearch();
            StartRemoteLobbySearch();
        }
    }
    
    private void OnEnable()
    {
        if (searchOnEnable && isInitialized)
        {
            DestroyAllButtons();
            StartLocalLobbySearch();
            StartRemoteLobbySearch();
        }   
    }

    private void OnDisable()
    {
        if (searchOnEnable && isInitialized)
        {
            remoteLobbyDiscovery.enabled = false;
            localLobbyManager.StopListeningClient();
            localLobbyManager.StopSearchingClient();
        }
    }
    
    //Lobby search
    void StartLocalLobbySearch()
    {
        localLobbyManager.StartListeningClient();
        localLobbyManager.StartSearchingClient();
    }
        
    void StartRemoteLobbySearch()
    {
        remoteLobbyDiscovery.enabled = true;
    }

    //Button management
    private void DestroyAllButtons()
    {
        //Remove all scene-placed children 
        while (sessionButtonList.transform.childCount > 0)
        {
            DestroyImmediate(sessionButtonList.transform.GetChild(0).gameObject);
            localLobbyButtons = new Dictionary<string, ILobbyButton>();
            remoteLobbyButtons = new Dictionary<string, ILobbyButton>();
        }
    }

    private void UpdateLocalLobbyButtons()
    {
        // Compare the keys of the dictionaries
        var lobbyKeys = localLobbyManager.Lobbies.Keys.ToList();
        var buttonKeys = localLobbyButtons.Keys.ToList();

        // Add buttons for keys in Lobbies but not in lobbyButtons
        foreach (var lobbyKey in lobbyKeys.Except(buttonKeys))
        {
            AddLocalLobbyButton(localLobbyManager.Lobbies[lobbyKey]);
        }

        // Update buttons for keys that are in both dictionaries
        foreach (var lobbyKey in lobbyKeys.Intersect(buttonKeys))
        {
            UpdateLocalLobbyButton(localLobbyManager.Lobbies[lobbyKey]);
        }

        // Remove buttons for keys in lobbyButtons but not in Lobbies
        foreach (var lobbyKey in buttonKeys.Except(lobbyKeys))
        {
            RemoveLocalLobbyButton(lobbyKey);
        }
    }

    private void UpdateRemoteLobbyButtons()
    {
        // Compare the keys of the dictionaries
        var lobbyKeys = remoteLobbyDiscovery.RemoteLobbies.Keys.ToList();
        var buttonKeys = remoteLobbyButtons.Keys.ToList();

        // Add buttons for keys in Lobbies but not in lobbyButtons
        foreach (var lobbyKey in lobbyKeys.Except(buttonKeys))
        {
            AddRemoteLobbyButton(remoteLobbyDiscovery.RemoteLobbies[lobbyKey]);
        }

        // Update buttons for keys that are in both dictionaries
        foreach (var lobbyKey in lobbyKeys.Intersect(buttonKeys))
        {
            UpdateRemoteLobbyButton(remoteLobbyDiscovery.RemoteLobbies[lobbyKey]);
        }

        // Remove buttons for keys in remoteLobbyButtons but not in RemoteLobbies
        foreach (var lobbyKey in buttonKeys.Except(lobbyKeys))
        {
            RemoveRemoteLobbyButton(lobbyKey);
        }
    }

    void AddLocalLobbyButton(LocalLobby lobby)
    {
        if (!showLocalLobbies)
        {
            return;
        }

        ILobbyButton button = Instantiate(sessionButtonPrefab, sessionButtonList.transform).GetComponent<ILobbyButton>();
        localLobbyButtons.Add(lobby.guid.ToString(), button);
        button.SetLobbyData(lobby);
        button.OnButtonToggled += () => SelectedLocalSessionID = lobby.guid.ToString();
        button.OnButtonUntoggled += () => SelectedLocalSessionID = null;
    }

    void RemoveLocalLobbyButton(string lobbyId)
    {
        if (!showLocalLobbies)
        {
            return;
        }

        if (localLobbyButtons.ContainsKey(lobbyId))
        {
           DestroyImmediate(localLobbyButtons[lobbyId].gameObject);
            localLobbyButtons.Remove(lobbyId);
        }
    }

    void UpdateLocalLobbyButton(LocalLobby lobby)
    {
        if (!showLocalLobbies) return;
        string lobbyId = (lobby.guid.ToString());
        if (localLobbyButtons.ContainsKey(lobbyId))
        {
            localLobbyButtons[lobbyId].SetLobbyData(lobby);
        }
    }

    void AddRemoteLobbyButton(Lobby lobby)
    {
        if (showLocalLobbies)
        {
            return;
        }

        XRLobbyButton button = Instantiate(sessionButtonPrefab, sessionButtonList.transform).GetComponent<XRLobbyButton>();
        remoteLobbyButtons.Add(lobby.Id, button);
        button.SetLobbyData(lobby);
        button.OnButtonToggled += () => SelectedRemoteSessionID = lobby.Id;
        button.OnButtonUntoggled += () => SelectedRemoteSessionID = null;
    }

    void RemoveRemoteLobbyButton(string lobbyId)
    {
        if (showLocalLobbies) return;
        if (remoteLobbyButtons.ContainsKey(lobbyId))
        {
            DestroyImmediate(remoteLobbyButtons[lobbyId].gameObject);
            remoteLobbyButtons.Remove(lobbyId);
        }
    }

    void UpdateRemoteLobbyButton(Lobby lobby)
    {
        if (showLocalLobbies) return;
        string lobbyId = (lobby.Id);
        if (remoteLobbyButtons.ContainsKey(lobbyId))
        {
            remoteLobbyButtons[lobbyId].SetLobbyData(lobby);
        }
    }
    
    //Event handlers
    public void OnConnectionTypeToggleSelected(int i)
    {
        if (i == 0)
        {
            ShowLocalLobbies = true;
        }
        else
        {
            ShowLocalLobbies = false;
        }
    }

    public abstract void OnClickNewSession();

    public void OnClickJoinSession()
    {
        if (showLocalLobbies && SelectedLocalSessionID != null)
        {
            activeSessionManager.JoinLocalSession(localLobbyManager.Lobbies[SelectedLocalSessionID]);
        }
        else if (!showLocalLobbies && selectedRemoteSessionID != null)
        {
            activeSessionManager.JoinRemoteSession(remoteLobbyDiscovery.RemoteLobbies[SelectedRemoteSessionID]);
        }
        else
        {
            Debug.Log("No session selected.");
        }
    }
    
}

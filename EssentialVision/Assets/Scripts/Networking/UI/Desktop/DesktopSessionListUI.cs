using UnityEngine;

public class DesktopSessionListUI : SessionListUI
{
    // Start is called before the first frame update
    [SerializeField] private GameObject desktopNewSessionPopupPrefab;
    [SerializeField] private GameObject blockingOverlay;
    
    public override void OnClickNewSession()
    {
        blockingOverlay.SetActive(true);
        DesktopCreateSessionMenu desktopNewSessionMenu = Instantiate(desktopNewSessionPopupPrefab, blockingOverlay.transform).GetComponent<DesktopCreateSessionMenu>();
        desktopNewSessionMenu.OnClickCreate.AddListener(() =>
        {
            activeSessionManager.StartSession(desktopNewSessionMenu.EnteredLobbyConfiguration);
            blockingOverlay.SetActive(false);
            Destroy(desktopNewSessionMenu.gameObject);
        });
        
        desktopNewSessionMenu.OnClickCancel.AddListener(() =>
        {
            blockingOverlay.SetActive(false);
            Destroy(desktopNewSessionMenu.gameObject);
        });
    }
}

using UnityEngine;
using NovaSamples.UIControls;
using UnityEngine.Events;

public class DesktopCreateSessionMenu : MonoBehaviour
{

    [SerializeField] private NovaSamples.UIControls.Toggle Lan;
    [SerializeField] private NovaSamples.UIControls.Toggle Remote;
    [SerializeField] private NovaSamples.UIControls.TextField SessionName;


    [SerializeField] private Button CreateButton;
    [SerializeField] private Button CancelButton;

    public UnityEvent OnClickCreate
    {
        get
        {
            return CreateButton.OnClicked;
        }
    }

    public UnityEvent OnClickCancel
    {
        get
        {
            return CancelButton.OnClicked;
        }
    }
    private bool isRemote = false;
    
    
    public LobbyConfiguration EnteredLobbyConfiguration
    {
        get
        {
            LobbyConfiguration enteredConfiguration = new LobbyConfiguration();
            enteredConfiguration.name = SessionName.Text != "" ? SessionName.Text : "New Session";
            enteredConfiguration.isRemote = isRemote;
            enteredConfiguration.localAnchorPlacement = true;
            return enteredConfiguration;
        }
    }
    
}

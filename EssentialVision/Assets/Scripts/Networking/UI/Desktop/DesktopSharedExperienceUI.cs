using UnityEngine;

public class DesktopSharedExperienceUI: MonoBehaviour
{

    [SerializeField] private ActiveSessionManager activeSessionManager;
    [SerializeField] private GameObject activeSessionUI;
    [SerializeField] private GameObject sessionListUI;

    private void Start()
    {
        activeSessionManager = ActiveSessionManager.Instance;
        activeSessionManager.SessionStarted += OnSessionStarted;
        activeSessionManager.SessionJoined += OnSessionStarted;
        activeSessionManager.SessionExited += OnSessionEnded;
    }

    private void OnDestroy()
    {
        if (activeSessionManager != null)
        {
            activeSessionManager.SessionStarted -= OnSessionStarted;
            activeSessionManager.SessionJoined -= OnSessionStarted;
            activeSessionManager.SessionExited -= OnSessionEnded;
        }
    }

    private void OnSessionStarted()
    {
        activeSessionUI.SetActive(true);
        sessionListUI.SetActive(false);
    }
    
    private void OnSessionEnded()
    {
        activeSessionUI.SetActive(false);
        sessionListUI.SetActive(true);
    }

}

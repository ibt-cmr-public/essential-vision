using Nova;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public class DesktopLobbyButton : DesktopToggleButton, ILobbyButton
{
    [SerializeField] private TextBlock sessionNameTextBlock;
    [SerializeField] private TextBlock numParticipantsTextBlock;
    [SerializeField] private TextBlock ipAddressTextBlock;
    
    // Start is called before the first frame update
    public void SetLobbyData(Lobby lobby)
    {
        sessionNameTextBlock.Text = lobby.Name;
        numParticipantsTextBlock.Text = lobby.Players.Count + "/" + lobby.MaxPlayers;
    }
    
    public void SetLobbyData(LocalLobby lobby)
    {
        sessionNameTextBlock.Text = lobby.name;
        numParticipantsTextBlock.Text = lobby.participantCount + "/" + lobby.maxParticipantCount;
        ipAddressTextBlock.Text = "IP: " + lobby.endpoint.Address.ToString();
    }
}

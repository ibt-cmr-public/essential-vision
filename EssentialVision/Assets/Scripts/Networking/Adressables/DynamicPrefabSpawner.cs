using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Netcode;
using UnityEngine;

public class DynamicPrefabSpawner : NetworkBehaviour
{
    [SerializeField] NetworkManager networkManager;
    float synchronousSpawnTimeoutTimer;
    private Dictionary<int, int> synchronousSpawnAckCount = new Dictionary<int, int>();
    //int synchronousSpawnAckCount = 0;
    private float networkSpawnTimeOutSeconds = 30;

    // Step 1: Create a static instance variable
    private static DynamicPrefabSpawner instance;

    void Start()
    {
        // Step 4: Ensure the singleton instance is created only once
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        
        // Initialize the singleton instance
        instance = this;
        
        DynamicPrefabLoadingUtilities.Init(networkManager);
        //networkManager.NetworkConfig.ConnectionApproval = true;
        // Here, we keep ForceSamePrefabs disabled. This will allow us to dynamically add network prefabs to Netcode
        // for GameObject after establishing a connection.
        networkManager.NetworkConfig.ForceSamePrefabs = false;
    }


    // Step 2: Add a public static property to access the singleton instance
    public static DynamicPrefabSpawner Instance
    {
        get { return instance; }
    }
        
    public override void OnDestroy()
    {
        DynamicPrefabLoadingUtilities.UnloadAndReleaseAllDynamicPrefabs();
        base.OnDestroy();
    }

    /// <summary>
    /// This call attempts to spawn a prefab by it's addressable guid - it ensures that all the clients have loaded the prefab before spawning it,
    /// and if the clients fail to acknowledge that they've loaded a prefab - the spawn will fail.
    /// </summary>
    /// <param name="guid"></param>
    /// <returns></returns>
    public async Task<(bool Success, NetworkObject Obj)> TrySpawnDynamicPrefabSynchronously(string guid, Transform parentTransform, float timeOutSeconds = 30f)
    {
        if (IsServer)
        {
            var assetGuid = new AddressableGUID()
            {
                Value = guid
            };
            
            if (DynamicPrefabLoadingUtilities.IsPrefabLoadedOnAllClients(assetGuid))
            {
                Debug.Log("Prefab is already loaded by all peers, we can spawn it immediately");
                var obj = Spawn(assetGuid);
                return (true, obj);
            }
            
            synchronousSpawnAckCount[assetGuid.GetHashCode()] = 0;
            float synchronousSpawnTimeoutTimer = 0;
            
            Debug.Log("Loading dynamic prefab on the clients...");
            LoadAddressableClientRpc(assetGuid);
            
            //load the prefab on the server, so that any late-joiner will need to load that prefab also
            await DynamicPrefabLoadingUtilities.LoadDynamicPrefab(assetGuid);
            
            var requiredAcknowledgementsCount = IsHost ? networkManager.ConnectedClients.Count - 1 : 
                networkManager.ConnectedClients.Count;
            
            while (synchronousSpawnTimeoutTimer < timeOutSeconds)
            {
                if (synchronousSpawnAckCount[assetGuid.GetHashCode()] >= requiredAcknowledgementsCount)
                {
                    Debug.Log($"All clients have loaded the prefab in {synchronousSpawnTimeoutTimer} seconds, spawning the prefab on the server...");
                    var obj = Spawn(assetGuid);
                    return (true, obj);
                }
                
                synchronousSpawnTimeoutTimer += Time.deltaTime;
                await Task.Yield();
            }
            
            // left to the reader: you'll need to be reactive to clients failing to load -- you should either have
            // the offending client try again or disconnect it after a predetermined amount of failed attempts
            Debug.LogError("Failed to spawn dynamic prefab - timeout");
            return (false, null);
        }

        return (false, null);

        NetworkObject Spawn(AddressableGUID assetGuid)
        {
            if (!DynamicPrefabLoadingUtilities.TryGetLoadedGameObjectFromGuid(assetGuid, out var prefab))
            {
                Debug.LogWarning($"GUID {assetGuid} is not a GUID of a previously loaded prefab. Failed to spawn a prefab.");
                return null;
            }
            var obj = Instantiate(prefab.Result, parentTransform).GetComponent<NetworkObject>();
            obj.Spawn();
            obj.TrySetParent(parentTransform);
            Debug.Log("Spawned dynamic prefab");
            return obj;
        }
    }
    
    [ClientRpc]
    void LoadAddressableClientRpc(AddressableGUID guid, ClientRpcParams rpcParams = default)
    {
        if (!IsHost)
        {
            Load(guid);
        }

        async void Load(AddressableGUID assetGuid)
        {
            Debug.Log("Loading dynamic prefab on the client...");
            await DynamicPrefabLoadingUtilities.LoadDynamicPrefab(assetGuid);
            Debug.Log("Client loaded dynamic prefab");
            AcknowledgeSuccessfulPrefabLoadServerRpc(assetGuid.GetHashCode());
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    void AcknowledgeSuccessfulPrefabLoadServerRpc(int prefabHash, ServerRpcParams rpcParams = default)
    {
        synchronousSpawnAckCount[prefabHash] = synchronousSpawnAckCount[prefabHash] + 1;
        Debug.Log($"Client acknowledged successful prefab load with hash: {prefabHash}");
        DynamicPrefabLoadingUtilities.RecordThatClientHasLoadedAPrefab(prefabHash,
            rpcParams.Receive.SenderClientId);
    }
    
}

using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Netcode;
using UnityEngine;


/// <summary>
/// A dynamic prefab loading use-case where the server instructs all clients to load a single network prefab via a
/// ClientRpc, will spawn said prefab as soon as it is loaded on the server, and will mark it as network-visible
/// only to clients that have already loaded that same prefab. As soon as a client loads the prefab locally, it
/// sends an acknowledgement ServerRpc, and the server will mark that spawned NetworkObject as network-visible to
/// that client.
/// </summary>
public sealed class DynamicPrefabSpawnerOld: NetworkBehaviour
{
    [SerializeField] NetworkManager networkManager;
    //A storage where we keep association between prefab (hash of it's GUID) and the spawned network objects that use it
    Dictionary<int, HashSet<NetworkObject>> prefabHashToNetworkObjectId = new Dictionary<int, HashSet<NetworkObject>>();
    
    void Start()
    {
        DynamicPrefabLoadingUtilities.Init(networkManager);
        networkManager.NetworkConfig.ConnectionApproval = true;
        // Here, we keep ForceSamePrefabs disabled. This will allow us to dynamically add network prefabs to Netcode
        // for GameObject after establishing a connection.
        networkManager.NetworkConfig.ForceSamePrefabs = false;
    }
    
    public override void OnDestroy()
    {

        DynamicPrefabLoadingUtilities.UnloadAndReleaseAllDynamicPrefabs();
        base.OnDestroy();
    }

    /// <summary>
    /// This call spawns an addressable prefab by it's guid. It does not ensure that all the clients have loaded the
    /// prefab before spawning it. All spawned objects are network-invisible to clients that don't have the prefab
    /// loaded. The server tells the clients that lack the preloaded prefab to load it and acknowledge that they've
    /// loaded it, and then the server makes the object network-visible to that client.
    /// </summary>
    /// <param name="guid"></param>
    /// <returns></returns>
    public async Task<NetworkObject> SpawnDynamicPrefab(string guid, Transform parentTransform)
    {
        if (IsServer)
        {
            var assetGuid = new AddressableGUID()
            {
                Value = guid
            };
            
            return await Spawn(assetGuid);
        }

        return null;

        async Task<NetworkObject> Spawn(AddressableGUID assetGuid)
        {
            // server is starting to load a prefab
            var prefab = await DynamicPrefabLoadingUtilities.LoadDynamicPrefab(assetGuid);
            var obj = Instantiate(prefab, parentTransform).GetComponent<NetworkObject>();
            
            //Add network object ID to list of network object IDS for the asset GUID
            if (prefabHashToNetworkObjectId.TryGetValue(assetGuid.GetHashCode(), out var networkObjectIds))
            {
                networkObjectIds.Add(obj);
            }
            else
            {
                prefabHashToNetworkObjectId.Add(assetGuid.GetHashCode(), new HashSet<NetworkObject>() {obj});
            }

            // This gets called on spawn and makes sure clients currently syncing and receiving spawns have the
            // appropriate network visibility settings automatically. This can happen on late join, on spawn, on
            // scene switch, etc.
            obj.CheckObjectVisibility = (clientId) => 
            {
                if (clientId == NetworkManager.ServerClientId)
                {
                    // object is loaded on the server, no need to validate for visibility
                    return true;
                }
                
                //if the client has already loaded the prefab - we can make the object network-visible to them
                if (DynamicPrefabLoadingUtilities.HasClientLoadedPrefab(clientId, assetGuid.GetHashCode()))
                {
                    return true;
                }
                
                //otherwise the clients need to load the prefab, and after they ack - the ShowHiddenObjectsToClient 
                LoadAddressableClientRpc(assetGuid, new ClientRpcParams(){Send = new ClientRpcSendParams(){TargetClientIds = new ulong[]{clientId}}});
                return false;
            };
            
            
            
            obj.Spawn();

            return obj;
        }
    }
    
    void ShowHiddenObjectsToClient(int prefabHash, ulong clientId)
    {
        if (prefabHashToNetworkObjectId.TryGetValue(prefabHash, out var networkObjects))
        {
            foreach (var obj in networkObjects)
            {
                if (!obj.IsNetworkVisibleTo(clientId))
                {
                    obj.NetworkShow(clientId);
                }
            }
        }
    }
    
    [ClientRpc]
    void LoadAddressableClientRpc(AddressableGUID guid, ClientRpcParams rpcParams = default)
    {
        if (!IsHost)
        {
            Load(guid);
        }

        async void Load(AddressableGUID assetGuid)
        {
            
            Debug.Log("Loading dynamic prefab on the client...");
            await DynamicPrefabLoadingUtilities.LoadDynamicPrefab(assetGuid);
            Debug.Log("Client loaded dynamic prefab");
            AcknowledgeSuccessfulPrefabLoadServerRpc(assetGuid.GetHashCode());
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    void AcknowledgeSuccessfulPrefabLoadServerRpc(int prefabHash, ServerRpcParams rpcParams = default)
    {
        Debug.Log($"Client acknowledged successful prefab load with hash: {prefabHash}");
        DynamicPrefabLoadingUtilities.RecordThatClientHasLoadedAPrefab(prefabHash, 
            rpcParams.Receive.SenderClientId);
       
        //the server has all the objects network-visible, no need to do anything
        if (rpcParams.Receive.SenderClientId != networkManager.LocalClientId)
        {
            ShowHiddenObjectsToClient(prefabHash, rpcParams.Receive.SenderClientId);
        }
    }
}

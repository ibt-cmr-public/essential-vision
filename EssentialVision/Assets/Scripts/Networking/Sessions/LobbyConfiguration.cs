using System;
using System.Collections.Generic;
using Unity.Services.Lobbies.Models;

[Serializable]
public class LobbyConfiguration
{
    public string name;
    public int maxParticipantCount = 128;
    public string passcode = null;
    public bool isRemote = false;
    public bool localAnchorPlacement;
    
    // Default constructor.
    public LobbyConfiguration()
    {
    }
    
    // Constructor from lobby 
    public LobbyConfiguration(Lobby lobby)
    {
        maxParticipantCount = lobby.MaxPlayers;
        name = lobby.Name;
        isRemote = true;
        localAnchorPlacement = true;
    }
    
    //Constructor from local lobby
    public LobbyConfiguration(LocalLobby lobby)
    {
        maxParticipantCount = lobby.maxParticipantCount;
        name = lobby.name;
        isRemote = false;
        localAnchorPlacement = lobby.localAnchorPlacement;
    }

}

public class LobbyInfo
{
    public LobbyConfiguration configuration = null;
    public int currentParticipantCount
    {
        get
        {
            return currentParticipants.Count;
        }
    }
    public List<PlayerInfo> currentParticipants;
    
    public LobbyInfo(LocalLobby lobby)
    {
    
        configuration = new LobbyConfiguration(lobby);
        currentParticipants = new List<PlayerInfo>();
        foreach (LocalPlayer player in lobby.participants.Values)
        {
            currentParticipants.Add(new PlayerInfo(player));
        }
    }

    public LobbyInfo(Lobby lobby)
    {
        configuration = new LobbyConfiguration(lobby);
        currentParticipants = new List<PlayerInfo>();
        foreach (Player player in lobby.Players)
        {
            currentParticipants.Add(new PlayerInfo(player));
        }
    }
}
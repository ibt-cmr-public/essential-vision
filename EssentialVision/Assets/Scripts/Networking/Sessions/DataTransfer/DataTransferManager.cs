using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using TreeView;
using Unity.Netcode;
using UnityEngine;
using Random = System.Random;

public class DataTransferManager : NetworkBehaviour
{
    public static DataTransferManager Instance { get; private set; }

    [SerializeField] private ModelInstanceManager modelInstanceManager;
    
    private TcpListener server = null;
    private TcpClient client = null;
    private NetworkStream stream = null;


    public void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    //Request individual layers from library
    public async Task<ModelNodeScriptableObject> RequestNodeFileFromClient(string modelID, string nodeID, ulong clientID)
    {
        RpcParams rpcParams = RpcTarget.Single(clientID, RpcTargetUse.Temp);
        return await RequestNodeFileFromRPCTarget(modelID, nodeID, rpcParams);
    }
    
    public async Task<ModelNodeScriptableObject> RequestNodeFileFromServer(string modelID, string nodeID)
    {
        RpcParams rpcParams = RpcTarget.Server;
        return await RequestNodeFileFromRPCTarget(modelID, nodeID, rpcParams);
    }
    
    private async Task<ModelNodeScriptableObject> RequestNodeFileFromRPCTarget(string modelID, string nodeID, RpcParams rpcParams)
    {
        try
        {
            // Start TCP server
            int port = new Random().Next(10000, 20000);
            server = new TcpListener(IPAddress.Any, port);
            server.Start();

            // Set up RPC call to request node file from client
            RequestNodeFileClientRpc(modelID, nodeID, NetworkHelper.GetLocalIPAddress().ToString(), port, rpcParams);

            // Accept a TCP client asynchronously
            client = await server.AcceptTcpClientAsync();
            stream = client.GetStream();

            byte[] lengthBytes = new byte[sizeof(int)];
            int totalBytesRead = 0;
            while (totalBytesRead < lengthBytes.Length)
            {
                int bytesRead = await stream.ReadAsync(lengthBytes, totalBytesRead, lengthBytes.Length - totalBytesRead);
                if (bytesRead == 0)
                {
                    // Handle the case where the connection is closed before all data is received
                    throw new IOException("Connection closed prematurely.");
                }
                totalBytesRead += bytesRead;
            }

            // Determine the length of the incoming data
            int messageLength = BitConverter.ToInt32(lengthBytes, 0);

            // Read the actual data
            byte[] modelBytes = new byte[messageLength];
            totalBytesRead = 0;
            while (totalBytesRead < messageLength)
            {
                int bytesRead = await stream.ReadAsync(modelBytes, totalBytesRead, messageLength - totalBytesRead);
                if (bytesRead == 0)
                {
                    // Handle the case where the connection is closed before all data is received
                    throw new IOException("Connection closed prematurely.");
                }
                totalBytesRead += bytesRead;
            }

            // Process received node file (deserialize, etc.)
            ModelNodeScriptableObject receivedModel = ModelNodeScriptableObject.FromBinary(modelBytes);

            return receivedModel;
        }
        catch (Exception e)
        {
            Debug.LogError("Error in RequestNodeFileFromClient: " + e.Message);
            return null;
        }
        finally
        {
            // Stop TCP server
            if (server != null)
                server.Stop();
        }
    }
    
    [Rpc(SendTo.SpecifiedInParams)]
    private void RequestNodeFileClientRpc(string modelID, string nodeID, string targetIPAddress, int port, RpcParams rpcParams)
    {
        // Start TCP client and send file asynchronously
        Task.Run(async () => await SendNodeFileAsync(modelID, nodeID, targetIPAddress, port));
    }
    
    private async Task SendNodeFileAsync(string modelID, string nodeID, string targetIPAddress, int port)
    {
        try
        {
            // Start TCP client and connect to server
            client = new TcpClient();
            await client.ConnectAsync(targetIPAddress, port);
            stream = client.GetStream();

            // Convert node ID to bytes and send it
            ModelScriptableObject model = LibraryManager.Instance.GetModel(modelID);
            ModelNodeScriptableObject node = null;
            foreach (TreeNode<ModelNodeScriptableObject> modelNode in model.ModelTree)
            {
                if (modelNode.guid.ToString() == nodeID)
                {
                    node = modelNode.Data;
                    break;
                }
            }
            byte[] nodeBytes = await node.ToBinary();
            byte[] lengthBytes = BitConverter.GetBytes(nodeBytes.Length);
            await stream.WriteAsync(lengthBytes, 0, lengthBytes.Length);
            await stream.WriteAsync(nodeBytes, 0, nodeBytes.Length);
        }
        catch (Exception e)
        {
            Debug.LogError("Error sending node file: " + e.Message);
        }
        finally
        {
            // Close TCP client and stream
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
        }
    }
    
    //Request indivudal nodes from open model
    public async Task<ModelNodeScriptableObject> RequestInstanceNodeFileFromClient(string modelInstanceID, string nodeID, ulong clientID)
    {
        RpcParams rpcParams = RpcTarget.Single(clientID, RpcTargetUse.Temp);
        return await RequestInstanceNodeFileFromRPCTarget(modelInstanceID, nodeID, rpcParams);
    }
    
    public async Task<ModelNodeScriptableObject> RequestInstanceNodeFileFromServer(string modelInstanceID, string nodeID)
    {
        RpcParams rpcParams = RpcTarget.Server;
        return await RequestInstanceNodeFileFromRPCTarget(modelInstanceID, nodeID, rpcParams);
    }
    
    private async Task<ModelNodeScriptableObject> RequestInstanceNodeFileFromRPCTarget(string modelInstanceID, string nodeID, RpcParams rpcParams)
    {
        try
        {
            // Start TCP server
            int port = new Random().Next(10000, 20000);
            server = new TcpListener(IPAddress.Any, port);
            server.Start();

            // Set up RPC call to request node file from client
            RequestInstanceNodeFileClientRpc(modelInstanceID, nodeID, NetworkHelper.GetLocalIPAddress().ToString(), port, rpcParams);

            // Accept a TCP client asynchronously
            client = await server.AcceptTcpClientAsync();
            stream = client.GetStream();

            byte[] lengthBytes = new byte[sizeof(int)];
            int totalBytesRead = 0;
            while (totalBytesRead < lengthBytes.Length)
            {
                int bytesRead = await stream.ReadAsync(lengthBytes, totalBytesRead, lengthBytes.Length - totalBytesRead);
                if (bytesRead == 0)
                {
                    // Handle the case where the connection is closed before all data is received
                    throw new IOException("Connection closed prematurely.");
                }
                totalBytesRead += bytesRead;
            }

            // Determine the length of the incoming data
            int messageLength = BitConverter.ToInt32(lengthBytes, 0);

            // Read the actual data
            byte[] modelBytes = new byte[messageLength];
            totalBytesRead = 0;
            while (totalBytesRead < messageLength)
            {
                int bytesRead = await stream.ReadAsync(modelBytes, totalBytesRead, messageLength - totalBytesRead);
                if (bytesRead == 0)
                {
                    // Handle the case where the connection is closed before all data is received
                    throw new IOException("Connection closed prematurely.");
                }
                totalBytesRead += bytesRead;
            }

            // Process received node file (deserialize, etc.)
            ModelNodeScriptableObject receivedModel = ModelNodeScriptableObject.FromBinary(modelBytes);

            return receivedModel;
        }
        catch (Exception e)
        {
            Debug.LogError("Error in RequestInstanceNodeFileFromClient: " + e.Message);
            return null;
        }
        finally
        {
            // Stop TCP server
            if (server != null)
                server.Stop();
        }
    }
    
    [Rpc(SendTo.SpecifiedInParams)]
    private void RequestInstanceNodeFileClientRpc(string modelInstanceID, string nodeID, string targetIPAddress, int port, RpcParams rpcParams)
    {
        // Start TCP client and send file asynchronously
        Task.Run(async () => await SendInstanceNodeFileAsync(modelInstanceID, nodeID, targetIPAddress, port));
    }
    
    private async Task SendInstanceNodeFileAsync(string modelInstanceID, string nodeID, string targetIPAddress, int port)
    {
        try
        {
            // Start TCP client and connect to server
            client = new TcpClient();
            await client.ConnectAsync(targetIPAddress, port);
            stream = client.GetStream();

            // Convert node ID to bytes and send it
            ModelSetup modelInstance = modelInstanceManager.modelInstances[new Guid(modelInstanceID)];
            ModelNodeScriptableObject node = modelInstance.ModelView.nodeController.Node(new Guid(nodeID)).NodeInfo;
            byte[] nodeBytes = await node.ToBinary();
            byte[] lengthBytes = BitConverter.GetBytes(nodeBytes.Length);
            await stream.WriteAsync(lengthBytes, 0, lengthBytes.Length);
            await stream.WriteAsync(nodeBytes, 0, nodeBytes.Length);
        }
        catch (Exception e)
        {
            Debug.LogError("Error sending node file: " + e.Message);
        }
        finally
        {
            // Close TCP client and stream
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
        }
    }
    
    
    //Request full model files from library
    public async Task<ModelScriptableObject> RequestModelFileFromClient(string modelID, ulong clientID)
    {
        RpcParams rpcParams = RpcTarget.Single(clientID, RpcTargetUse.Temp);
        return await RequestModelFileFromRPCTarget(modelID, rpcParams);
    }
    
    public async Task<ModelScriptableObject> RequestModelFileFromServer(string modelID)
    {
        RpcParams rpcParams = RpcTarget.Server;
        return await RequestModelFileFromRPCTarget(modelID, rpcParams);
    }

    private async Task<ModelScriptableObject> RequestModelFileFromRPCTarget(string modelID, RpcParams rpcParams)
    {

        try
        {
            // Start TCP server
            int port = new Random().Next(10000, 20000);
            server = new TcpListener(IPAddress.Any, port);
            server.Start();

            // Set up RPC call to request model file from client
            RequestModelFileClientRpc(modelID, NetworkHelper.GetLocalIPAddress().ToString(), port, rpcParams);

            // Accept a TCP client asynchronously
            client = await server.AcceptTcpClientAsync();
            stream = client.GetStream();

            byte[] lengthBytes = new byte[sizeof(int)];
            int totalBytesRead = 0;
            while (totalBytesRead < lengthBytes.Length)
            {
                int bytesRead = await stream.ReadAsync(lengthBytes, totalBytesRead, lengthBytes.Length - totalBytesRead);
                if (bytesRead == 0)
                {
                    // Handle the case where the connection is closed before all data is received
                    throw new IOException("Connection closed prematurely.");
                }
                totalBytesRead += bytesRead;
            }

            // Determine the length of the incoming data
            int messageLength = BitConverter.ToInt32(lengthBytes, 0);

            // Read the actual data
            byte[] modelBytes = new byte[messageLength];
            totalBytesRead = 0;
            while (totalBytesRead < messageLength)
            {
                int bytesRead = await stream.ReadAsync(modelBytes, totalBytesRead, messageLength - totalBytesRead);
                if (bytesRead == 0)
                {
                    // Handle the case where the connection is closed before all data is received
                    throw new IOException("Connection closed prematurely.");
                }
                totalBytesRead += bytesRead;
            }


            File.WriteAllBytes("C:/Users/johan/OneDrive/Desktop/transferred_model.bin", modelBytes);

            // Process received model file (deserialize, etc.)
            ModelScriptableObject receivedModel = ModelScriptableObject.FromBinary(modelBytes);

            return receivedModel;
        }
        catch (Exception e)
        {
            Debug.LogError("Error in RequestModelFileFromClient: " + e.Message);
            return null;
        }
        finally
        {
            // Stop TCP server
            if (server != null)
                server.Stop();
        }
    }


    [Rpc(SendTo.SpecifiedInParams)]
    private void RequestModelFileClientRpc(string modelID, string targetIPAddress, int port, RpcParams rpcParams)
    {
        // Start TCP client and send file asynchronously
        Task.Run(async () => await SendModelFileAsync(modelID, targetIPAddress, port));
    }

    private async Task SendModelFileAsync(string modelID, string targetIPAddress, int port)
    {
        try
        {
            // Start TCP client and connect to server
            client = new TcpClient();
            await client.ConnectAsync(targetIPAddress, port);
            stream = client.GetStream();

            // Convert model ID to bytes and send it
            ModelScriptableObject model = LibraryManager.Instance.GetModel(modelID);
            byte[] modelBytes = await model.ToBinary(); 
            File.WriteAllBytes("C:/Users/johan/OneDrive/Desktop/right_before_transfer.bin", modelBytes);
            Debug.Log("Model bytes length: " + modelBytes.Length);
            byte[] lengthBytes = BitConverter.GetBytes(modelBytes.Length);
            await stream.WriteAsync(lengthBytes, 0, lengthBytes.Length);
            await stream.WriteAsync(modelBytes, 0, modelBytes.Length);
        }
        catch (Exception e)
        {
            Debug.LogError("Error sending model file: " + e.Message);
        }
        finally
        {
            // Close TCP client and stream
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
        }
    }
}
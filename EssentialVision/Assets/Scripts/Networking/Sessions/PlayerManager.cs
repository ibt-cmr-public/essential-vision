using System;
using System.Collections.Generic;
using Unity.Services.Authentication;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    [SerializeField] private string name;
    private Guid guid;

    public static PlayerManager Instance { get; private set; }

    public void Awake()
    {
        Instance = this;
    }

    public void Start()
    {
        guid = Guid.NewGuid();
        name = PlayerPrefs.GetString("PlayerName", "New User");
    }

    public LocalPlayer GetLocalPlayer()
    {
        LocalPlayer player = new LocalPlayer();
        player.name = name;
        player.guid = guid;
        return player;
    }
    
    public void SetPlayerName(string newName)
    {
        name = newName;
        PlayerPrefs.SetString("PlayerName", name);
    }
    
    public Player GetRemotePlayer()
    {
        return new Player(AuthenticationService.Instance.PlayerId, null, new Dictionary<string, PlayerDataObject> {
            { LobbyManager.KEY_PLAYER_NAME, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, name) }
        });
    }
}

﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;

/// <summary>
/// A component that advertises a session and searches for sessions in the LAN.
/// </summary>
public sealed class LocalLobbyDiscovery : MonoBehaviour {

	[SerializeField]
	[Tooltip("A string that differentiates your application/game from others. Must not be null, empty, or blank.")]
	private string secret;
	[SerializeField]
	[Tooltip("The port number used by this NetworkDiscovery component. Must be different from the one used by the Transport.")]
	private ushort port;
	[SerializeField]
	[Tooltip("How often this NetworkDiscovery component advertises a server or searches for servers.")]
	private float discoveryInterval = 0.1f;
	private float responseTimeout = 2f;
	
	[SerializeField]
	[Tooltip("How long available servers should be searched for.")]
	private float searchTimeout = 120f;
	[SerializeField]
	[Tooltip("Whether search for servers should be time restricted or run until stopped manually.")]
	private bool searchIndefinitely = true;
	//Session configuration to advertise
	public LobbyConfiguration advertisedLobbyConfiguration = null;
	
	private UdpClient _serverUdpClient;
	private UdpClient _clientUdpClient;
	public bool IsAdvertising => _serverUdpClient != null;
	public bool IsSearching => _clientUdpClient != null;
	
    public event Action <IPEndPoint, LobbyConfiguration> ServerFoundCallback;

    [SerializeField] private NetworkManager networkManager;
    

    private void OnDisable()
	{
		StopAdvertisingServer();
		StopSearchingForServers();
	}

	private void OnDestroy()
	{
		StopAdvertisingServer();
		StopSearchingForServers();
	}

	private void OnApplicationQuit()
	{
		StopAdvertisingServer();
		StopSearchingForServers();
	}

	#region Server


	public void StartAdvertisingServer()
	{

		if (IsAdvertising)
		{
			Debug.Log("Server is already being advertised.", this);

			return;
		}

		UnityTransport managerTransport = networkManager.NetworkConfig.NetworkTransport as UnityTransport;
		if (port == managerTransport.ConnectionData.Port)
		{
			Debug.LogWarning("Unable to start advertising server on the same port as the transport.", this);

			return;
		}

		_serverUdpClient = new UdpClient(port)
		{
			EnableBroadcast = true,
			MulticastLoopback = false,
		};

        Task.Run(() => AdvertiseServerAsync(advertisedLobbyConfiguration));

		Debug.Log("Started advertising server.", this);
	}
	
	public void StopAdvertisingServer()
	{
		if (!IsAdvertising) return;

		_serverUdpClient.Close();

		_serverUdpClient = null;

		Debug.Log("Stopped advertising server.", this);
	}

	private async void AdvertiseServerAsync(LobbyConfiguration lobbyConfiguration)
	{
		while (IsAdvertising)
		{
			await Task.Delay(TimeSpan.FromSeconds(discoveryInterval));

			UdpReceiveResult result = await _serverUdpClient.ReceiveAsync();
            Debug.Log("Received client call for server");

            string receivedSecret = Encoding.UTF8.GetString(result.Buffer);

			if (receivedSecret == secret)
			{

				byte[] sessionDataBytes = SerializationHelper.Serialize(lobbyConfiguration);

                await _serverUdpClient.SendAsync(sessionDataBytes, sessionDataBytes.Length, result.RemoteEndPoint);
                Debug.Log("Sent server response to client request");
            }
		}
	}

	#endregion

	#region Client
	
	public void StartSearchingForServers()
	{

		if (IsSearching)
		{
			Debug.Log("Already searching for servers.", this);

			return;
		}

		_clientUdpClient = new UdpClient()
		{
			EnableBroadcast = true,
			MulticastLoopback = false,
		};

		Task.Run(SearchForServersAsync);

		Debug.Log("Started searching for servers.", this);
	}
	public void StopSearchingForServers()
	{
		if (!IsSearching) return;

		_clientUdpClient.Close();

		_clientUdpClient = null;

		Debug.Log("Stopped searching for servers.", this);
	}
    private async void SearchForServersAsync()
    {
        byte[] secretBytes = Encoding.UTF8.GetBytes(secret);
        IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, port);
        DateTime startTime = DateTime.UtcNow;
        Task<UdpReceiveResult> receiveTask = _clientUdpClient.ReceiveAsync();

        while (IsSearching && (searchIndefinitely || (DateTime.UtcNow - startTime).TotalSeconds < searchTimeout))
        {
            await Task.Delay(TimeSpan.FromSeconds(discoveryInterval));
            await _clientUdpClient.SendAsync(secretBytes, secretBytes.Length, endPoint);
			Debug.Log("Sent searching message.");
            // Assuming there's a timeout for the response to avoid waiting indefinitely
            Task delayTask = Task.Delay(TimeSpan.FromSeconds(responseTimeout));
            await Task.WhenAny(receiveTask, delayTask);
            if (receiveTask.IsCompleted && !receiveTask.IsFaulted && !receiveTask.IsCanceled)
            {
                Debug.Log("Received response");
                UdpReceiveResult result = receiveTask.Result;
                LobbyConfiguration lobbyConfiguration = SerializationHelper.Deserialize<LobbyConfiguration>(result.Buffer);

                if (BitConverter.ToBoolean(result.Buffer, 0))
                {
					UnityMainThreadDispatcher.Instance().Enqueue(() => ServerFoundCallback?.Invoke(result.RemoteEndPoint, lobbyConfiguration));
                }

                //Reset receive task for other servers
                receiveTask = _clientUdpClient.ReceiveAsync();
            }

        }

        StopSearchingForServers();
    }

    #endregion
}


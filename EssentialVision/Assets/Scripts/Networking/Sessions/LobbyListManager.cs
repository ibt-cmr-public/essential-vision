using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Services.Lobbies.Models;
using UnityEngine;

//Manages list of found local and remote sessions and starts/stops search for sessions through LobbyManager and LobbyDiscovery components
public class LobbyListManager : MonoBehaviour
{
    [SerializeField] private LobbyDiscovery lobbyDiscovery;
    [SerializeField] private LocalLobbyManager localLobbyManager;
        
    //Whether search should start any time component is enabled and stop any time component is disabled
    [SerializeField] private bool searchOnEnable = true;
    private Dictionary<string, LocalLobby> localLobbies = new Dictionary<string, LocalLobby>();
    private Dictionary<string, Lobby> remoteLobbies = new Dictionary<string, Lobby>();
    public Dictionary<string, LocalLobby> LocalLobbies { get { return localLobbies; } }
    public Dictionary<string, Lobby> RemoteLobbies { get { return remoteLobbies; } }

    
    //Events fired when adding and removing lobbies
    public event Action<LocalLobby> LocalLobbyAdded;
    public event Action<LocalLobby> LocalLobbyRemoved;
    public event Action<LocalLobby> LocalLobbyUpdated;
    public event Action<Lobby> RemoteLobbyAdded;
    public event Action<Lobby> RemoteLobbyRemoved;
    public event Action<Lobby> RemoteLobbyUpdated;


    // Start is called before the first frame update
    
    void Start()
    {
        if (lobbyDiscovery == null) lobbyDiscovery = FindObjectOfType<LobbyDiscovery>();
        if (lobbyDiscovery == null)
        {
            Debug.LogWarning("The session list manager was unable to find a lobby discovery component.");
        }
        
        //localLobbyManager.OnLobbyFound += AddOrUpdateLocalLobby;
        //localLobbyManager.OnLobbyRemoved += RemoveLocalLobby;
        //lobbyDiscovery.OnLobbyListChanged += UpdateRemoteLobbies;
        
        if (searchOnEnable)
        {
            StartLocalLobbySearch();
            StartRemoteLobbySearch();
        }
    }

    void StartLocalLobbySearch()
    {
        localLobbyManager.StartListeningClient();
        localLobbyManager.StartSearchingClient();
    }
    
    void StartRemoteLobbySearch()
    {
        lobbyDiscovery.StartSearching();
    }

    private void OnEnable()
    {
        if (searchOnEnable)
        {
            RemoveAllLobbies();
            StartLocalLobbySearch();
            StartRemoteLobbySearch();
        }   
    }

    private void OnDisable()
    {
        if (searchOnEnable)
        {
            lobbyDiscovery.StopSearching();
            localLobbyManager.StopSearchingClient();
        }
    }

    private void RemoveAllLobbies()
    {
        UpdateRemoteLobbies(new List<Lobby>());
        List<string> previousLobbyIDs = LocalLobbies.Keys.ToList();
        foreach (string lobbyID in previousLobbyIDs)
        {
            RemoveLocalLobby(LocalLobbies[lobbyID]);
        }
    }
    
    private void UpdateRemoteLobbies(List<Lobby> lobbies)
    {
        List<string> previousLobbyIDs = remoteLobbies.Keys.ToList();
        foreach (Lobby lobby in lobbies)
        {
            if (remoteLobbies.ContainsKey(lobby.Id))
            {
                remoteLobbies[lobby.Id] = lobby;
                RemoteLobbyUpdated?.Invoke(lobby);
            }
            else
            {
                remoteLobbies.Add(lobby.Id, lobby);
                RemoteLobbyAdded?.Invoke(lobby);
            }

            previousLobbyIDs.Remove(lobby.Id);
        }

        foreach (string lobbyID in previousLobbyIDs)
        {
            Lobby lobby = remoteLobbies[lobbyID];
            remoteLobbies.Remove(lobbyID);
            RemoteLobbyRemoved?.Invoke(lobby);
        }
    }
    
    private void AddOrUpdateLocalLobby(LocalLobby lobby)
    {
        string lobbyId = lobby.guid.ToString();
        if (!localLobbies.ContainsKey(lobbyId))
        {
            localLobbies.Add(lobbyId, lobby);
            LocalLobbyAdded?.Invoke(lobby);
        }
        else
        {
            localLobbies[lobbyId] = lobby;
            LocalLobbyUpdated?.Invoke(lobby);
        }
    }

    private void RemoveLocalLobby(LocalLobby lobby)
    {
        string lobbyId = lobby.guid.ToString();
        if (!localLobbies.ContainsKey(lobbyId))
        {
            Debug.LogWarning($"Session list contains no session with the specified IP endpoint {lobbyId}. Session list remains unchanged.");
        }
        else
        {
            LocalLobby lobbyToRemove = localLobbies[lobbyId];
            localLobbies.Remove(lobbyId);
            LocalLobbyRemoved?.Invoke(lobbyToRemove);
        }
    }
    
    
}

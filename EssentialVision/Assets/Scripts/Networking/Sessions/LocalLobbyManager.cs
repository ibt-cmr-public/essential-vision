using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

//Manages the set of local lobbies and communication with joined lobby

public class LocalLobbyManager : MonoBehaviour
{
	public static LocalLobbyManager Instance { get; private set; }
	
	//Lobby list management
    private Dictionary<string, LocalLobby> lobbies = new Dictionary<string, LocalLobby>();
    public Dictionary<string, LocalLobby> Lobbies
    {
	    get
	    {
		    return lobbies;
	    }
    }
    private Dictionary<string, float> secondsSinceLobbyResponses = new Dictionary<string, float>();
    private LocalLobby joinedLobby = null;
    private float searchIntervalSeconds = 1f;
    private float lobbyAdvertisementTimeoutSeconds = 5f;
	public Action LobbyListUpdated;
    
    //UDP clients for communication
    //Clients periodically send out requests to look for available local lobbies and start a communication channel
    //When a lobby is started by a host, the server UdpClient is started to communicate with potential clients
    //A host acts as both a server and a client (serverUdpClient and clientUdpClient are active at the same time)
    //The server port is set application-wide whereas client ports are dynamically allocated
	[SerializeField] private ushort serverPort;
	public ushort ServerPort
	{
		get
		{
			return serverPort;
		}
	}
	public ushort ClientPort
	{
		get
		{
			if (clientUdpClient == null)
			{
				return 0;
			}
			else
			{
				return (ushort)((IPEndPoint)clientUdpClient.Client.LocalEndPoint).Port;
			}
		}
	}
	private UdpClient clientUdpClient;
	private UdpClient serverUdpClient;
	private CancellationTokenSource clientListenCancellationTokenSource = null;
	private CancellationTokenSource serverListenCancellationTokenSource = null;
	private CancellationTokenSource clientSearchCancellationTokenSource = null;

	//Joined lobby management
	private bool isHost = false;
	public bool IsHost
	{
		get
		{
			return isHost;
		}
	}
	public LocalLobby JoinedLobby
	{
		get
		{
			return joinedLobby;
		}
	}
	public Action<LocalLobby> OnJoinedLobby;
	public Action<LocalLobby> OnJoinedLobbyUpdate;
	public Action OnLeftLobby;
	public Action OnKickedFromLobby;
	
	private TaskCompletionSource<bool> joinRequestCompletionSource = null;
	private float joinRequestTimeoutSeconds = 10f;

	private float clientHeartbeatTimer = 0f;
	private Dictionary<string, float> secondsSinceLastClientHeartbeat;
	private float secondsSinceLastServerHeartbeat;
	private float heartbeatTimeoutIntervalSeconds = 1800f;
	private float clientHeartbeatIntervalSeconds = 1f;
	
	private LocalPlayer GetPlayer()
	{
		return PlayerManager.Instance.GetLocalPlayer();
	}
	
	//Unity methods
	private void Awake() {
		Instance = this;
	}
    private void OnDisable()
	{
		StopClient();
		StopServer();
	}
	private void OnDestroy()
	{
		StopClient();
		StopServer();
	}
	private void OnApplicationQuit()
	{
		StopClient();
		StopServer();
	}
	private void Update()
	{
		//Handle heartbeat
		HandleJoinedLobbyHeartbeats();
		//Handle lobby list update (remove lobbies that have not advertised in a while)
		HandleAdvertisedLobbyHeartbeats();
	}
	private void HandleAdvertisedLobbyHeartbeats()
	{
		foreach (string lobbyID in Lobbies.Keys.ToList())
		{
			secondsSinceLobbyResponses[lobbyID] += Time.deltaTime;
			if (secondsSinceLobbyResponses[lobbyID] > lobbyAdvertisementTimeoutSeconds)
			{
				LocalLobby lobbyToRemove = RemoveLobby(lobbyID);
				LobbyListUpdated?.Invoke();
			}
		}
	}
	private void HandleJoinedLobbyHeartbeats()
	{
		if (JoinedLobby != null)
		{
			clientHeartbeatTimer += Time.deltaTime;

			if (clientHeartbeatTimer > clientHeartbeatIntervalSeconds)
			{
				clientHeartbeatTimer = 0f;
				SendClientHeartbeat();
			}

			if (IsHost)
			{
				foreach (string playerID in JoinedLobby.participants.Keys.ToList())
				{
					if (!secondsSinceLastClientHeartbeat.ContainsKey(playerID))
					{
						secondsSinceLastClientHeartbeat.Add(playerID, 0f);
					}
					else
					{
						secondsSinceLastClientHeartbeat[playerID] += Time.deltaTime;
					}

					if (secondsSinceLastClientHeartbeat[playerID] > heartbeatTimeoutIntervalSeconds)
					{
						KickPlayer(playerID);
					}
				}
			}
			else
			{
				secondsSinceLastServerHeartbeat += Time.deltaTime;
				if (secondsSinceLastServerHeartbeat > heartbeatTimeoutIntervalSeconds)
				{
					LeaveLobby();
				}
			}
		}
	}
	
	//Udp client
	private void StartClient()
	{
		if (clientUdpClient != null)
		{
			Debug.Log("Client UdpClient already active.");
			return;
		}
		clientUdpClient = new UdpClient()
		{
			EnableBroadcast = true,
			MulticastLoopback = false,
		};
	}
	private void StopClient()
	{
		StopListeningClient();
		StopSearchingClient();
		if (clientUdpClient == null)
		{
			Debug.Log("No active client UdpClient.");
			return;
		}
		clientUdpClient.Close();
		clientUdpClient = null;
		clientListenCancellationTokenSource = null;
		clientSearchCancellationTokenSource = null;
	}
	private void StartServer()
	{
		if (serverUdpClient != null)
		{
			Debug.Log("Server UdpClient already active.");
			return;
		}
		serverUdpClient = new UdpClient(serverPort)
		{
			EnableBroadcast = true,
			MulticastLoopback = false,
		}; 
	}
	private void StopServer()
	{
		StopListeningServer();
		if (serverUdpClient == null)
		{
			Debug.Log("No active server UdpClient.");
			return;
		}
		serverUdpClient.Close();
		serverUdpClient = null;
		serverListenCancellationTokenSource = null;
	}
	public void StartListeningClient()
	{
		StartClient();
		Task.Run(StartListeningClientAsync);
	}
	private async Task StartListeningClientAsync()
	{
		if (clientUdpClient == null)
		{
			Debug.Log("Client Udp client not active. Could not listen to incoming messages.");
			return;
		}
		if (clientIsListening)
		{
			Debug.Log("Client already listening.");
			return;
		}
		clientListenCancellationTokenSource = new CancellationTokenSource();
		CancellationToken linkedToken = clientListenCancellationTokenSource.Token;
		
		try
		{
			Debug.Log("Listening for incoming messages.");
			while (!linkedToken.IsCancellationRequested && clientUdpClient != null)
			{
				var receiveTask = clientUdpClient.ReceiveAsync();
				var completedTask = await Task.WhenAny(receiveTask, Task.Delay(-1, linkedToken));

				if (completedTask == receiveTask)
				{
					// Handle the received message
					UdpReceiveResult result = receiveTask.Result;
					LocalMessage message = SerializationHelper.Deserialize<LocalMessage>(result.Buffer);
					IPEndPoint senderEndPoint = result.RemoteEndPoint;
					UnityMainThreadDispatcher.Instance().Enqueue(() => HandleMessage(message, senderEndPoint));
				}
				else if (linkedToken.IsCancellationRequested)
				{
					// Cancellation requested, stop listening
					break;
				}
			}
		}
		catch (OperationCanceledException)
		{
			// Cancellation requested, stop listening
		}
		catch (Exception ex)
		{
			Debug.LogError("Error while listening: " + ex.Message);
		}
		finally
		{
			clientListenCancellationTokenSource.Dispose();
			clientListenCancellationTokenSource = null;
		}
	}
	public void StopListeningClient()
	{
		if (clientIsListening)
		{
			clientListenCancellationTokenSource.Cancel();
		}
	}
	private async Task StartListeningServerAsync()
	{
		if (serverUdpClient == null)
		{
			Debug.Log("Server Udp client not active. Could not listen to incoming messages.");
			return;
		}
		
		if (serverIsListening)
		{
			Debug.Log("Udp server client already listening.");
			return;
		}
		
		serverListenCancellationTokenSource = new CancellationTokenSource();
		CancellationToken linkedToken = serverListenCancellationTokenSource.Token;
		
		try
		{
			Debug.Log("Listening for incoming messages.");
			while (!linkedToken.IsCancellationRequested && serverUdpClient != null)
			{
				var receiveTask = serverUdpClient.ReceiveAsync();
				var completedTask = await Task.WhenAny(receiveTask, Task.Delay(-1, linkedToken));

				if (completedTask == receiveTask)
				{
					// Handle the received message
					UdpReceiveResult result = receiveTask.Result;
					LocalMessage message = SerializationHelper.Deserialize<LocalMessage>(result.Buffer);
					IPEndPoint senderEndPoint = result.RemoteEndPoint;
					UnityMainThreadDispatcher.Instance().Enqueue(() => HandleMessage(message, senderEndPoint));
				}
				else if (linkedToken.IsCancellationRequested)
				{
					// Cancellation requested, stop listening
					break;
				}
			}
		}
		catch (OperationCanceledException)
		{
			// Cancellation requested, stop listening
		}
		catch (Exception ex)
		{
			Debug.LogError("Error while listening: " + ex.Message);
		}
		finally
		{
			serverListenCancellationTokenSource = null;
			serverListenCancellationTokenSource.Dispose();
		}
	}
	public void StopListeningServer()
	{
		if (serverIsListening)
		{
			serverListenCancellationTokenSource.Cancel();

		}
	}
	public void StartSearchingClient()
	{
		StartClient();
		Task.Run(StartSearchingClientAsync);
	}
	private async Task StartSearchingClientAsync()
	{
		if (clientUdpClient == null)
		{
			Debug.Log("Client Udp client not active. Could not start search for available servers.");
			return;
		}
		
		if (clientSearchCancellationTokenSource != null)
		{
			Debug.Log("Client already searching.");
			return;
		}
		
		clientSearchCancellationTokenSource = new CancellationTokenSource();
		CancellationToken linkedToken = clientSearchCancellationTokenSource.Token;

		try
		{
			Debug.Log("Searching for servers.");
			while (!linkedToken.IsCancellationRequested && clientUdpClient != null)
			{
				var sendRequestTask = SendLobbySearchRequest();
				var completedTask = await Task.WhenAny(sendRequestTask, Task.Delay(-1, linkedToken));

				if (completedTask == sendRequestTask)
				{
					// SendLobbySearchRequest completed, we wait some time before broadcasting another request
					await Task.Delay(TimeSpan.FromSeconds(searchIntervalSeconds));
				}
				else if (linkedToken.IsCancellationRequested)
				{
					// Cancellation requested, stop searching
					break;
				}
			}
		}
		catch (OperationCanceledException)
		{
			// Cancellation requested, stop searching
		}
		catch (Exception ex)
		{
			Debug.LogError("Error while searching: " + ex.Message);
		}
		finally
		{
			clientSearchCancellationTokenSource.Dispose();
			clientSearchCancellationTokenSource = null;
		}
	}
	public void StopSearchingClient()
	{
		RemoveAllLobbies();
		if (clientIsSearching)
		{
			clientSearchCancellationTokenSource.Cancel();
		}
	}
	private async Task SendClientMessageAsync(byte[] message, IPEndPoint endpoint)
	{
		await clientUdpClient.SendAsync(message, message.Length, endpoint);
	}
	private async Task SendServerMessageAsync(byte[] message, IPEndPoint endpoint)
	{
		await serverUdpClient.SendAsync(message, message.Length, endpoint);
	}
	private bool clientIsSearching
	{
		get { return (clientSearchCancellationTokenSource != null); }
	}
	private bool clientIsListening 
	{
		get {return (clientListenCancellationTokenSource != null);}
	}
	private bool serverIsListening
	{
		get { return (serverListenCancellationTokenSource != null);}
	}
	
	//Handle incoming messages
	private bool SenderIsHost(IPEndPoint senderEndPoint)
	{
		return joinedLobby != null && joinedLobby.endpoint.Equals(senderEndPoint);
	}
	private bool SenderIsMember(IPEndPoint senderEndPoint)
	{
		bool isMember = false;
		if (joinedLobby == null)
		{
			return isMember;
		}
		else
		{
			foreach (LocalPlayer participant in joinedLobby.participants.Values)
			{
				if (senderEndPoint.Equals(participant.endpoint))
				{
					isMember = true;
				}
			}
		}
		return isMember;
	}
	private void HandleMessage(LocalMessage message, IPEndPoint senderEndPoint)
	{
		Debug.Log($"Handling message of type {message.Type.ToString()}");
		switch (message.Type)
		{
			case MessageType.LobbySearchRequest:
				if (!IsHost) break;
				HandleLobbySearchRequest(senderEndPoint);
				break;
			case MessageType.LobbyAdvertisement:
				// Handle lobby advertisement
				LocalLobby advertisedLobby = SerializationHelper.Deserialize<LocalLobby>(message.data);
				HandleLobbyAdvertisement(advertisedLobby);
				break;
			case MessageType.LobbyUpdate:
				if (!SenderIsHost(senderEndPoint)) break;
				LocalLobby updatedLobby = SerializationHelper.Deserialize<LocalLobby>(message.data);
				HandleLobbyUpdate(updatedLobby);
				break;
			case MessageType.LobbyDestroyed:
				// Handle lobby destroyed
				if (!SenderIsHost(senderEndPoint)) break;
				LocalLobby destroyedLobby = SerializationHelper.Deserialize<LocalLobby>(message.data);
				HandleLobbyDestroyed(destroyedLobby);
				break;
			case MessageType.LobbyJoinRequest:
				if (!IsHost) break;
				LocalPlayer localPlayerToJoin = SerializationHelper.Deserialize<LocalPlayer>(message.data);
				HandleLobbyJoinRequest(localPlayerToJoin);
				break;
			case MessageType.LobbyJoinResponse:
				PlayerJoinResponseData responseData =
					SerializationHelper.Deserialize<PlayerJoinResponseData>(message.data);
				HandleLobbyJoinResponse(responseData);
				break;
			case MessageType.LobbyLeaveRequest:
				if (!SenderIsMember(senderEndPoint) || !IsHost) break;
				LocalPlayer localPlayerToLeave = SerializationHelper.Deserialize<LocalPlayer>(message.data);
				HandleLobbyLeaveRequest(localPlayerToLeave);
				break;
			case MessageType.PlayerKick:
				if (!SenderIsHost(senderEndPoint)) break;
				HandlePlayerKick();
				break;
			case MessageType.PlayerUpdate:
				if (!SenderIsMember(senderEndPoint) || !IsHost) break;
				LocalPlayer updatedLocalPlayer = SerializationHelper.Deserialize<LocalPlayer>(message.data);
				HandleLobbyLeaveRequest(updatedLocalPlayer);
				break;
			case MessageType.Heartbeat:
				if (SenderIsMember(senderEndPoint) && IsHost)
				{
					string playerID = SerializationHelper.Deserialize<string>(message.data);
					HandleClientHeartbeat(playerID, senderEndPoint);
				}
				else if (SenderIsHost(senderEndPoint))
				{
					HandleServerHeartbeat();
				}
				break;
		}
	}
	private void HandleClientHeartbeat(string playerID, IPEndPoint senderEndpoint)
	{
		if (secondsSinceLastClientHeartbeat.ContainsKey(playerID))
		{
			secondsSinceLastClientHeartbeat[playerID] = 0f;
		}
		//Send server heartbeat as response
		LocalMessage serverHeartbeat = new LocalMessage();
		serverHeartbeat.Type = MessageType.Heartbeat;
		SendServerMessageAsync(SerializationHelper.Serialize(serverHeartbeat), senderEndpoint);
	}
	private void HandleServerHeartbeat()
	{
		secondsSinceLastServerHeartbeat = 0f;
	}
	private void HandleLobbyAdvertisement(LocalLobby advertisedLobby)
	{
		if (advertisedLobby == null) return;
		AddLobby(advertisedLobby);
	}
	private async void HandleLobbySearchRequest(IPEndPoint senderEndpoint)
	{
		if (senderEndpoint == null) return;
		LocalMessage response = new LocalMessage();
		response.Type = MessageType.LobbyAdvertisement;
		response.data = SerializationHelper.Serialize(JoinedLobby);
		await SendServerMessageAsync(SerializationHelper.Serialize(response), senderEndpoint);
	}																																													
	private void HandleLobbyUpdate(LocalLobby updatedLobby)
	{
		if (updatedLobby == null) return;
		if (updatedLobby.guid != joinedLobby.guid)
		{
			Debug.LogError("Received lobby update for lobby that was not joined.");
		}
		else
		{
			joinedLobby = updatedLobby;
			OnJoinedLobbyUpdate?.Invoke(joinedLobby);
		}
	}
	private void HandleLobbyDestroyed(LocalLobby destroyedLobby)
	{
		if (destroyedLobby == null) return;
		if (joinedLobby.guid == destroyedLobby.guid)
		{
			joinedLobby = null;
			OnLeftLobby?.Invoke();
		}
		if (Lobbies.ContainsKey(destroyedLobby.guid.ToString()))
		{
			RemoveLobby(destroyedLobby.guid.ToString());
		}
	}
	private void HandleLobbyJoinRequest(LocalPlayer localPlayer)
	{
		if (localPlayer == null) return;
		if (joinedLobby.participants.Count < joinedLobby.maxParticipantCount)
		{
			//Send positive response to player
			LocalMessage response = new LocalMessage();
			PlayerJoinResponseData joinResponseData = new PlayerJoinResponseData(true);
			response.Type = MessageType.LobbyJoinResponse;
			response.data = SerializationHelper.Serialize(joinResponseData);
			SendServerMessageAsync(SerializationHelper.Serialize(response), localPlayer.endpoint);
			
			//Add player to lobby
			joinedLobby.participants.Add(localPlayer.guid.ToString(), localPlayer);
			secondsSinceLastClientHeartbeat.Add(localPlayer.guid.ToString(), 0f);
			SendLobbyUpdate();
		}
		else
		{
			//Send negative response to player
			LocalMessage response = new LocalMessage();
			PlayerJoinResponseData joinResponseData = new PlayerJoinResponseData(false);
			response.Type = MessageType.LobbyJoinResponse;
			response.data = SerializationHelper.Serialize(joinResponseData);
			SendServerMessageAsync(SerializationHelper.Serialize(response), localPlayer.endpoint);
		}
	}
	private void HandleLobbyJoinResponse(PlayerJoinResponseData responseData)
	{
		if (responseData == null) return;
		joinRequestCompletionSource.SetResult(responseData.response);
	}
	private void HandleLobbyLeaveRequest(LocalPlayer localPlayerToLeave)
	{
		if (localPlayerToLeave == null) return;
		joinedLobby.participants.Remove(localPlayerToLeave.guid.ToString());
		secondsSinceLastClientHeartbeat.Remove(localPlayerToLeave.guid.ToString());
		SendLobbyUpdate();
	}
	private void HandlePlayerUpdate(LocalPlayer localPlayerToUpdate)
	{
		if (localPlayerToUpdate == null) return;
		joinedLobby.participants[localPlayerToUpdate.guid.ToString()] = localPlayerToUpdate;
		SendLobbyUpdate();
	}
	private void HandlePlayerKick()
	{
		joinedLobby = null;
		OnKickedFromLobby?.Invoke();
	}
	
	//Sending messages
	private async void SendLobbyUpdate()
	{
		if (!IsHost) return;
		LocalMessage updateMessage = new LocalMessage();
		updateMessage.Type = MessageType.LobbyUpdate;
		updateMessage.data = SerializationHelper.Serialize(JoinedLobby);
		foreach (LocalPlayer participant in JoinedLobby.participants.Values)
		{
			await SendServerMessageAsync(SerializationHelper.Serialize(updateMessage), participant.endpoint);
		}
	}
	private async void SendPlayerUpdate()
	{
		if (joinedLobby == null) return;
		LocalMessage updateMessage = new LocalMessage();
		updateMessage.Type = MessageType.PlayerUpdate;
		updateMessage.data = SerializationHelper.Serialize(GetPlayer());
		await SendClientMessageAsync(SerializationHelper.Serialize(updateMessage), joinedLobby.endpoint);
		Debug.Log("Sent Player Update.");

	}
	private async Task SendLobbySearchRequest()
	{
		LocalMessage searchMessage = new LocalMessage();
		searchMessage.Type = MessageType.LobbySearchRequest;
		IPEndPoint broadcastEndpoint = new IPEndPoint(IPAddress.Broadcast, serverPort);
		await SendClientMessageAsync(SerializationHelper.Serialize(searchMessage), broadcastEndpoint);
		Debug.Log("Sent Lobby Search Request.");
	}
	private async Task SendClientHeartbeat()
	{
		LocalMessage heartbeat = new LocalMessage();
		heartbeat.Type = MessageType.Heartbeat;
		heartbeat.data = SerializationHelper.Serialize(GetPlayer().guid.ToString());
		await SendClientMessageAsync(SerializationHelper.Serialize(heartbeat), JoinedLobby.endpoint);
		Debug.Log("Sent Client Heartbeat.");
	}
	
    //Lobby management
    public async Task<LocalLobby> CreateLobby(LobbyConfiguration lobbyConfiguration) {
	    
	    //Create Lobby
	    LocalLobby lobby = new LocalLobby(lobbyConfiguration);
	    
	    //JoinLobby
	    joinedLobby = lobby;
	    joinedLobby.AddPlayer(GetPlayer());
	    isHost = true;
	    secondsSinceLastClientHeartbeat = new Dictionary<string, float>();
	    OnJoinedLobby?.Invoke(joinedLobby);
	    
	    //Start Server
	    StartServer();
	    Task.Run(async () => await StartListeningServerAsync());

	    Debug.Log("Created Lobby " + lobby.name);
	    return joinedLobby;
    }
    public async Task JoinLobbyByCode(string joinCode, IPEndPoint endpoint) {
	    //TODO: Implement this
    }
    public async Task JoinLobby(LocalLobby lobby) {
	    // Ensure any previous request is canceled and its TaskCompletionSource is reset.
	    if (joinRequestCompletionSource != null && !joinRequestCompletionSource.Task.IsCompleted) {
		    joinRequestCompletionSource.SetCanceled();
		    joinRequestCompletionSource = null;
	    }

	    // Create a new TaskCompletionSource to await the response
	    joinRequestCompletionSource = new TaskCompletionSource<bool>();

	    // Send join request to lobby host
	    IPEndPoint hostEndpoint = lobby.endpoint;
	    LocalMessage joinRequest = new LocalMessage();
	    joinRequest.Type = MessageType.LobbyJoinRequest;
	    joinRequest.data = SerializationHelper.Serialize(GetPlayer());
	    await SendClientMessageAsync(SerializationHelper.Serialize(joinRequest), hostEndpoint);

	    // Create a task for the timeout
	    var timeoutTask = Task.Delay(TimeSpan.FromSeconds(joinRequestTimeoutSeconds));

	    // Await the response or timeout
	    var completedTask = await Task.WhenAny(joinRequestCompletionSource.Task, timeoutTask);

	    // Handle the response or timeout
	    if (completedTask == joinRequestCompletionSource.Task) {
		    bool joinSuccessful = joinRequestCompletionSource.Task.Result;
		    if (joinSuccessful) {
			    Debug.Log("Successfully joined lobby");
			    joinedLobby = lobby;
			    secondsSinceLastServerHeartbeat = 0f;
			    OnJoinedLobby?.Invoke(lobby);
		    } else {
			    Debug.Log("Join request denied.");
			    joinRequestCompletionSource = null;
		    }
	    } else {
		    bool joinSuccessful = false;
		    Debug.Log("Join request failed - Timeout occurred.");
		    joinRequestCompletionSource = null;
	    }
    }
    public async void LeaveLobby()
    {
	    if (joinedLobby == null) return;
	    
	    //Send leave lobby request
	    LocalMessage leaveRequest = new LocalMessage();
	    leaveRequest.Type = MessageType.LobbyLeaveRequest;
	    leaveRequest.data = SerializationHelper.Serialize(GetPlayer());
	    await SendClientMessageAsync(SerializationHelper.Serialize(leaveRequest), joinedLobby.endpoint);
	    
	    //Leave lobby
	    joinedLobby = null;
	    OnLeftLobby?.Invoke();
    }
    public async void DestroyLobby()
    {
	    if (!isHost) return;
	    
	    //Broadcast LobbyDestroy message to all members
	    LocalMessage lobbyDestroyMessage = new LocalMessage();
	    lobbyDestroyMessage.Type = MessageType.LobbyDestroyed;
	    lobbyDestroyMessage.data = SerializationHelper.Serialize(joinedLobby); 
	    foreach (LocalPlayer participant in JoinedLobby.participants.Values)
	    {
		    await SendServerMessageAsync(SerializationHelper.Serialize(lobbyDestroyMessage), participant.endpoint);
	    }
	    
	    //Stop the server
	    StopServer();
	    
	    //Leave lobby
	    joinedLobby = null;
	    OnLeftLobby?.Invoke();
    }
    public async void KickPlayer(string playerId)
    {
	    if (!IsHost || !joinedLobby.participants.ContainsKey(playerId)) return;
	    LocalPlayer player = joinedLobby.participants[playerId];
		
	    //Remove player from lobby
	    joinedLobby.participants.Remove(playerId);
	    //Send kick message to player
	    LocalMessage kickRequest = new LocalMessage();
	    kickRequest.Type = MessageType.PlayerKick;
	    await SendServerMessageAsync(SerializationHelper.Serialize(kickRequest), player.endpoint);
	    //Send lobby update
	    SendLobbyUpdate();
    }
    public async void TransferLobbyHost(string newHostId)
    {
	    
    }
    
    //Lobby List Management
    private void AddLobby(LocalLobby lobby)
    {
	    if (lobby == null) return;
	    lobbies[lobby.guid.ToString()] = lobby;
	    secondsSinceLobbyResponses[lobby.guid.ToString()] = 0f;
	    LobbyListUpdated?.Invoke();
    }
    private LocalLobby RemoveLobby(string lobbyID)
    {
	    if (lobbies.ContainsKey(lobbyID))
	    {
		    LocalLobby lobbyToRemove = lobbies[lobbyID];
		    lobbies.Remove(lobbyID);
		    secondsSinceLobbyResponses.Remove(lobbyID);
		    return lobbyToRemove;
	    }
	    else
	    {
		    return null;
	    }
    }
    private void RemoveAllLobbies(){
	    foreach (string lobbyID in lobbies.Keys.ToList())
	    {
		    RemoveLobby(lobbyID);
	    }
    }
}

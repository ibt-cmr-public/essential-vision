using System;

[Serializable]
public enum MessageType
{
    LobbyAdvertisement,
    LobbySearchRequest,
    LobbyDestroyed,
    LobbyUpdate,
    LobbyJoinRequest,
    LobbyJoinResponse,
    LobbyLeaveRequest,
    PlayerKick,
    PlayerUpdate,
    HostTransfer,
    Heartbeat
}

[Serializable]
public class LocalMessage
{
    public MessageType Type;
    public byte[] data = null;
}


[Serializable]
public class PlayerJoinResponseData
{
    public bool response = false;
    
    public PlayerJoinResponseData(bool _response)
    {
        response = _response;
    }
   
}
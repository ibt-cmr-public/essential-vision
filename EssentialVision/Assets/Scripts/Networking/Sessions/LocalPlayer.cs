using System;
using System.Net;
using Newtonsoft.Json;
using Unity.Services.Lobbies.Models;

[Serializable]
public class LocalPlayer
{
    public Guid guid;
    public string name;
    public string address;
    public ushort port;

    [JsonIgnore]
    public IPEndPoint endpoint
    {
        get
        {
            return new IPEndPoint(IPAddress.Parse(address), port);
        }
    }

    public LocalPlayer(string playerName)
    {
        guid = Guid.NewGuid();
        name = playerName;
        port = LocalLobbyManager.Instance.ClientPort;
        address = NetworkHelper.GetLocalIPAddress().ToString();
    }
    
    public LocalPlayer()
    {
        guid = Guid.NewGuid();
        name = "";
        port = LocalLobbyManager.Instance.ClientPort;
        address = NetworkHelper.GetLocalIPAddress().ToString();   
    }
}

//Player info class that can be used to serialize player data for both local and remote players
public class PlayerInfo
{
    public string playerId;
    public string name;
    
    public PlayerInfo(LocalPlayer player)
    {
        playerId = player.guid.ToString();
        name = player.name;
    }
    
    public PlayerInfo(Player player)
    {
        playerId = player.Id;
        name = player.Id;
    }
    

}

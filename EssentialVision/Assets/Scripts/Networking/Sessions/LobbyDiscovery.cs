using System;
using System.Collections.Generic;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public class LobbyDiscovery : MonoBehaviour
{
    
    private Dictionary<string, Lobby> remoteLobbies = new Dictionary<string, Lobby>();
    public Dictionary<string, Lobby> RemoteLobbies
    {
        get
        {
            return remoteLobbies;
        }
    }
    public Action LobbyListUpdated;
    private bool isUpdating = false;
    private float secondsSinceLastUpdate = 0f;
    private float updateIntervalSeconds = 2f;

    private void Update() {
        if (isUpdating)
        {
            HandleRefreshLobbyList();
        }
    }

    private void HandleRefreshLobbyList() {
        if (UnityServices.State == ServicesInitializationState.Initialized && AuthenticationService.Instance.IsSignedIn)
        {
            secondsSinceLastUpdate += Time.deltaTime;
            if (secondsSinceLastUpdate > updateIntervalSeconds) {
                RefreshLobbyList();
                secondsSinceLastUpdate = 0f;
            }
        }
    }
    
    public async void RefreshLobbyList() {
        try {
            QueryLobbiesOptions options = new QueryLobbiesOptions();
            options.Count = 25;

            // Filter for open lobbies only
            options.Filters = new List<QueryFilter> {
                new QueryFilter(
                    field: QueryFilter.FieldOptions.AvailableSlots,
                    op: QueryFilter.OpOptions.GT,
                    value: "0")
            };

            // Order by newest lobbies first
            options.Order = new List<QueryOrder> {
                new QueryOrder(
                    asc: false,
                    field: QueryOrder.FieldOptions.Created)
            };

            QueryResponse lobbyListQueryResponse = await Lobbies.Instance.QueryLobbiesAsync();
            remoteLobbies = new Dictionary<string, Lobby>();
            foreach (Lobby lobby in lobbyListQueryResponse.Results)
            {
                remoteLobbies.Add(lobby.Id, lobby);
            }
            LobbyListUpdated?.Invoke();
        } catch (LobbyServiceException e) {
            Debug.Log(e);
        }
    }
    
    public async void StartSearching()
    {
        await NetworkHelper.InitializeRemoteServices();
        isUpdating = true;
    }
    
    public async void StopSearching()
    {
        remoteLobbies = new Dictionary<string, Lobby>();
        LobbyListUpdated?.Invoke();
        isUpdating = false;
    }
    
}

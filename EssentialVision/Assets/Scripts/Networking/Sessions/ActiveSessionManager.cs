using Unity.Netcode;
using Microsoft.Azure.SpatialAnchors;
using Microsoft.Azure.SpatialAnchors.Unity;
using MixedReality.Toolkit.SpatialManipulation;
using System;
using System.Threading.Tasks;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.SceneManagement;

//Manages the active shared session
//Can handle both remote and local sessions (but only one session is active at any point in time)
public class ActiveSessionManager : MonoBehaviour
{
    
    public static ActiveSessionManager Instance { get; private set; }
    
    //Other managers and components
    [SerializeField] private ushort networkManagerPort;
    
    //Anchors
    [SerializeField] private SpatialAnchorManager spatialAnchorManager; 
    [SerializeField] private GameObject remoteAnchorPrefab;
    [SerializeField] private GameObject localAnchorPrefab;
    [SerializeField] private string anchorID;
    [SerializeField] private GameObject networkedModelContainer;
    private GameObject anchorObject = null;
    private CloudSpatialAnchor currentSpatialAnchor = null;
    
    //Active session Info
    private Lobby activeRemoteLobby
    {
        get
        {
            return LobbyManager.Instance.JoinedLobby;
        }
    }
    private LocalLobby activeLocalLobby
    {
        get
        {
            return LocalLobbyManager.Instance.JoinedLobby;
        }
    }
    public bool SessionActive
    {
        get
        {
            return activeRemoteLobby != null || activeLocalLobby != null;
        }
    }
    private bool exitingSession = false;
    private bool joiningSession = false;
    private bool isHost = false;
    public bool IsHost
    {
        get { return isHost; }
    }
    public bool IsRemote
    {
        get
        {
            return activeLocalLobby == null && activeRemoteLobby != null;
        }
    }

    public LobbyInfo activeLobbyInfo
    {
        get
        {
            if (!SessionActive)
            {
                return null;
            }
            else
            {
                return IsRemote ? new LobbyInfo(activeRemoteLobby) : new LobbyInfo(activeLocalLobby);
            }
        }
    }
    
    //Client info
    public Action SessionStarted;
    public Action SessionJoined;
    public Action SessionUpdated;
    public Action SessionExited;
    
    #region Unity methods

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        //Initialize spatial anchor manager
        if (spatialAnchorManager == null) spatialAnchorManager  = GetComponent<SpatialAnchorManager>();
        if (spatialAnchorManager == null)
        {
            spatialAnchorManager = gameObject.AddComponent<SpatialAnchorManager>();
        }
        spatialAnchorManager.LogDebug += (sender, args) => Debug.Log($"Spatial Anchors - Debug: {args.Message}");
        spatialAnchorManager.Error += (sender, args) => Debug.LogError($"Spatial Anchors - Error: {args.ErrorMessage}");
        spatialAnchorManager.AnchorLocated += AnchorLocated;

        //Hook up events
        LocalLobbyManager.Instance.OnJoinedLobbyUpdate += OnLocalSessionUpdated;
        LobbyManager.Instance.OnJoinedLobbyUpdate += OnRemoteSessionUpdated;
        LocalLobbyManager.Instance.OnKickedFromLobby += ExitSession;
        LobbyManager.Instance.OnKickedFromLobby += ExitSession;
        LocalLobbyManager.Instance.OnLeftLobby += ExitSession;
        LobbyManager.Instance.OnLeftLobby += ExitSession;
        
        //Configure network manager for single player and start host
        NetworkManager.Singleton.GetComponent<UnityTransport>().SetConnectionData(
            "127.0.0.1",  // The IP address is a string
            (ushort)networkManagerPort, // The port number is an unsigned short
            "0.0.0.0" // The server listen address is a string.
        );
        NetworkManager.Singleton.StartHost();
    }

    
    #endregion
    
    #region Session Management
    
    //Start a session (as a host)
    public void StartSession(LobbyConfiguration lobbyConfiguration)
    {
        if (SessionActive)
        {
            Debug.Log("Already in active session. Exit session before starting a new one.");
            return;
        }

        if (lobbyConfiguration.isRemote)
        {
            try
            {
                StartRemoteSession(lobbyConfiguration);
            }
            catch (Exception e)
            {
                Debug.LogError("An error occurred while trying to start a remote session: " + e.Message);
                ExitSession();
            }
        }
        else
        {
            try {
                StartLocalSession(lobbyConfiguration);
            }
            catch (Exception e)
            {
                Debug.LogError("An error occurred while trying to start a local session: " + e.Message);
                ExitSession();
            }
        }
        
    }
    public async void StartLocalSession(LobbyConfiguration lobbyConfiguration)
    {
        //Anchor placement
        if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
        {
            if (lobbyConfiguration.localAnchorPlacement)
            {
                await PlaceLocalAnchor();
            }
            else
            {
                await PlaceRemoteAnchor();
            }
        }

        //Shutdown previous host
        await ShutdownNetworkManager();
        
        //Configure host for local multiplayer
        NetworkManager.Singleton.GetComponent<UnityTransport>().SetConnectionData(
            "127.0.0.1",  // The IP address is a string
            (ushort)networkManagerPort, // The port number is an unsigned short
            "0.0.0.0" // The server listen address is a string.
        );
        
        //Create lobby
        LocalLobby lobby = await LocalLobbyManager.Instance.CreateLobby(lobbyConfiguration);
        
        //Start host
        NetworkManager.Singleton.StartHost();
        NetworkManager.Singleton.SceneManager.SetClientSynchronizationMode(LoadSceneMode.Additive);
        
        //Set variables
        isHost = true;
        SessionStarted?.Invoke();
    }
    public async void StartRemoteSession(LobbyConfiguration lobbyConfiguration)
    {
        //Anchor placement
        if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
        {
            await PlaceLocalAnchor();
        }
        
        //Create relay (also configures host)
        string relayJoinCode = await CreateRelay(lobbyConfiguration.maxParticipantCount);
        NetworkManager.Singleton.SceneManager.SetClientSynchronizationMode(LoadSceneMode.Additive);
        
        //Create Lobby
        Lobby lobby = await LobbyManager.Instance.CreateLobby(lobbyConfiguration.name, relayJoinCode, lobbyConfiguration.maxParticipantCount, false);
        
        //Set variables
        isHost = true;
        SessionStarted?.Invoke();
    }
    
    //Join an existing session
    public async void JoinLocalSession(LocalLobby lobby)
    {
        if (SessionActive)
        {
            Debug.Log("Already in active session. Exit session before joining a new one.");
            return;
        }
        
        if (NetworkManager.Singleton == null)
        {
            Debug.LogWarning($"Could not join local session with lobby ID {lobby.guid}. NetworkManager is not set.");
            return;
        }

        try
        {
            //Start by locating session anchor.
            if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
            {
                if (lobby.localAnchorPlacement)
                {
                    await PlaceLocalAnchor();
                }
                else
                {
                    LocateAnchor(lobby.anchorID);
                }
            }

        //Join lobby
            await LocalLobbyManager.Instance.JoinLobby(lobby);

            //Restart network manager in local multiplayer 
            await ShutdownNetworkManager();
            NetworkManager.Singleton.GetComponent<UnityTransport>()
                .SetConnectionData(lobby.endpoint.Address.ToString(), networkManagerPort);
            NetworkManager.Singleton.StartClient();
            
            //Set variables
            isHost = false;
            SessionJoined?.Invoke();
        }
        catch (Exception e)
        {
            // Handle the exception here, you can log it or perform other error handling tasks.
            Debug.LogError("An error occurred while trying to join a local session: " + e.Message);
            ExitSession();
        }
    }
    public async void JoinRemoteSession(Lobby lobby)
    {
        try
        {
            if (SessionActive)
            {
                Debug.Log("Already in active session. Exit session before joining a new one.");
                return;
            }

            if (NetworkManager.Singleton == null)
            {
                Debug.LogWarning($"Could not join lobby with ID {lobby.Id}. NetworkManager is not set.");
                return;
            }

            //Anchor placement
            if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
            {
                await PlaceLocalAnchor();
            }

            //Join lobby
            await LobbyManager.Instance.JoinLobby(lobby);

            //Join relay 
            await JoinRelay(LobbyManager.Instance.JoinedLobby.Data["RelayJoinCode"].Value);

            //Set variables
            isHost = false;
            SessionJoined?.Invoke();
        }
        catch (Exception e)
        {
            // Handle the exception here, you can log it or perform other error handling tasks.
            Debug.LogError("An error occurred while trying to join a remote session: " + e.Message);
            ExitSession();
        }
    }

    
    //Exit a joined session 
    public async void ExitSession()
    {
        if (exitingSession)
        {
            Debug.Log("Currently exiting session.");
            return;
        }

        exitingSession = true;
        
        //Destroy anchor
        if (anchorObject != null)
        {
            Destroy(anchorObject);
        }

        //Transfer host
        bool transferHost = false;
        if (isHost && transferHost)
        {
            string hostId = null;
            TransferHost(hostId);
        }
        
        
        //Switch network manager to singleplayer
        try
        {
            await ShutdownNetworkManager();
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetConnectionData(
                "127.0.0.1", // The IP address is a string
                (ushort)networkManagerPort, // The port number is an unsigned short
                "0.0.0.0" // The server listen address is a string.
            );
            NetworkManager.Singleton.StartHost();
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to switch to single player mode: {e}");
        }

        //Leave or destroy joined lobby
        try
        {
            if (SessionActive)
            {
                if (isHost)
                {
                    if (IsRemote)
                    {
                        LobbyManager.Instance.DestroyLobby();
                    }
                    else
                    {
                        LocalLobbyManager.Instance.DestroyLobby();
                    }
                }
                else
                {
                    if (IsRemote)
                    {
                        LobbyManager.Instance.LeaveLobby();
                    }
                    else
                    {
                        LocalLobbyManager.Instance.LeaveLobby();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to leave Lobby: {e}");
        }
        
        //Reset anchors
        try
        {
            if (spatialAnchorManager.IsSessionStarted)
            {
                if (isHost)
                {
                    Task.Run(() => spatialAnchorManager.DeleteAnchorAsync(currentSpatialAnchor));
                }

                spatialAnchorManager.DestroySession();
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to reset anchor manager: {e}");
        }
        
        //Set variables
        isHost = false;
        exitingSession = false;
        SessionExited?.Invoke();
    }
    public async void TransferHost(string newHostID)
    {
        if (!isHost)
        {
            Debug.Log("Could not transfer host since current user is not host.");
        }
        else
        {
            //TODO: Transfer NetworkManager host - is that even possible?
            if (IsRemote)
            {
                LobbyManager.Instance.TransferLobbyHost(newHostID);
            }
            else
            {
                LocalLobbyManager.Instance.TransferLobbyHost(newHostID);
            }
        }
    }
    
    //Update session data 
    public void OnLocalSessionUpdated(LocalLobby lobby)
    {
        if (!IsRemote)
        {
            SessionUpdated?.Invoke();
        }
    }
    public void OnRemoteSessionUpdated(Lobby lobby)
    {
        if (IsRemote)
        {
            SessionUpdated?.Invoke();
        }
    }

    public void KickPlayer(string participantID)
    {
        if (!IsHost) return;
        NetworkManager.Singleton.DisconnectClient(0);
        if (IsRemote)
        {
            LobbyManager.Instance.KickPlayer(participantID);
        }
        else
        {
            LocalLobbyManager.Instance.KickPlayer(participantID);
        }
    }

    #endregion
    
    #region NetworkManager
    private async Task ShutdownNetworkManager()
    {
        //Check if network manager active
        if (NetworkManager.Singleton == null) return;
        if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer) return;
        
        // Wait for the shutdown process to complete or a timeout
        NetworkManager.Singleton.Shutdown();
        const int maxWaitTimeInSeconds = 10; // Adjust the timeout as needed
        float startTime = Time.time;
        while (NetworkManager.Singleton.ShutdownInProgress)
        {
            if (Time.time - startTime >= maxWaitTimeInSeconds)
            {
                Debug.LogError("Timeout: Shutdown exceeded timeout.");
                return;
            }
            await Task.Delay(100); // Check every 100 milliseconds, you can adjust the interval
        }
    }
    
    #endregion
    
    #region Anchors

    public Vector3 anchorPosition
    {
        get
        {
            if (anchorObject == null)
            {
                return new Vector3(0, 0, 0);
            }
            else
            {
                return anchorObject.transform.position;
            }
        }
    }
     private void LocateAnchor(string anchorID)
    {
        //Create watcher to look for all stored anchor IDs
        AnchorLocateCriteria anchorLocateCriteria = new AnchorLocateCriteria();
        anchorLocateCriteria.Identifiers = new string[] { anchorID };
        spatialAnchorManager.Session.CreateWatcher(anchorLocateCriteria);
        Debug.Log($"ASA - Watcher created!");
    }

    private void AnchorLocated(object sender, AnchorLocatedEventArgs args)
    {
        Debug.Log($"ASA - Anchor recognized as a possible anchor {args.Identifier} {args.Status}");

        if (args.Status == LocateAnchorStatus.Located)
        {
            //Creating and adjusting GameObjects have to run on the main thread. We are using the UnityDispatcher to make sure this happens.
            UnityDispatcher.InvokeOnAppThread(() =>
            {
                // Read out Cloud Anchor values
                CloudSpatialAnchor cloudSpatialAnchor = args.Anchor;

                //Create GameObject
                anchorObject = Instantiate(remoteAnchorPrefab);
                CloudNativeAnchor nativeAnchor = anchorObject.GetComponent<CloudNativeAnchor>();
                nativeAnchor.CloudToNative(cloudSpatialAnchor);
            });
        }
    }

    private async Task PlaceLocalAnchor()
    {
        //Let user place anchor
        Debug.Log("Letting user place anchor.");
        anchorObject = Instantiate(localAnchorPrefab);
        TapToPlace tapToPlace = anchorObject.GetComponent<TapToPlace>();
        if (tapToPlace == null)
        {
            tapToPlace = anchorObject.AddComponent<TapToPlace>();
        }
        TaskCompletionSource<bool> placingStoppedTask = new TaskCompletionSource<bool>();
        tapToPlace.OnPlacingStopped.AddListener(() =>
        {
            //Sync networked model instance container to the placed anchor
            networkedModelContainer.transform.position = anchorObject.transform.position;
            networkedModelContainer.transform.rotation = anchorObject.transform.rotation;
            anchorObject.transform.SetParent(networkedModelContainer.transform);
            ObjectManipulator objectManipulator = anchorObject.GetComponent<ObjectManipulator>();
            if (objectManipulator == null)
            {
                objectManipulator = anchorObject.AddComponent<ObjectManipulator>();
            }
            objectManipulator.HostTransform = networkedModelContainer.transform;
            
            BoundsControl boundsControl = anchorObject.GetComponent<BoundsControl>();
            if (boundsControl == null)
            {
                boundsControl = anchorObject.AddComponent<BoundsControl>();
            }
            boundsControl.Target = networkedModelContainer.transform;
            //Call placement finished
            placingStoppedTask.SetResult(true); // Set the task as completed when the event fires
        });

        await placingStoppedTask.Task;
    }

    private async Task<CloudSpatialAnchor> PlaceRemoteAnchor()
    {
        await spatialAnchorManager.StartSessionAsync(); 
        //Wait until anchor creation is available
        while (!spatialAnchorManager.IsReadyForCreate)
        {
            float createProgress = spatialAnchorManager.SessionStatus.RecommendedForCreateProgress;
            Debug.Log($"ASA - Move your device to capture more environment data: {createProgress:0%}");
            await Task.Delay(100);
        }

        Debug.Log("Started spatial anchor session.");

        //Let user place anchor
        Debug.Log("Letting user place anchor.");
        anchorObject = Instantiate(remoteAnchorPrefab);
        TapToPlace tapToPlace = anchorObject.GetComponent<TapToPlace>();
        if (tapToPlace == null)
        {
            tapToPlace = anchorObject.AddComponent<TapToPlace>();
        }
        TaskCompletionSource<bool> placingStoppedTask = new TaskCompletionSource<bool>();
        tapToPlace.OnPlacingStopped.AddListener(() =>
        {
            placingStoppedTask.SetResult(true); // Set the task as completed when the event fires
        });

        await placingStoppedTask.Task;


//#if UNITY_EDITOR
//        Debug.Log("Creating fake anchor for editor development.");
//        anchorID = "sampledID";
//        return null;
//#endif
        //Place anchor at specified 
        Debug.Log("Anchor placed by user.");
        CloudNativeAnchor cloudNativeAnchor = anchorObject.GetComponent<CloudNativeAnchor>();
        if (cloudNativeAnchor == null)
        {
            cloudNativeAnchor = anchorObject.AddComponent<CloudNativeAnchor>();
        }
        await cloudNativeAnchor.NativeToCloud();
        CloudSpatialAnchor cloudSpatialAnchor = cloudNativeAnchor.CloudAnchor;
        cloudSpatialAnchor.Expiration = DateTimeOffset.Now.AddDays(3);
        
        Debug.Log("Cloud anchor successfully prepared.");
        try
        {
            // Now that the cloud spatial anchor has been prepared, we can try the actual save here.
            await spatialAnchorManager.CreateAnchorAsync(cloudSpatialAnchor);

            bool saveSucceeded = cloudSpatialAnchor != null;
            if (!saveSucceeded)
            {
                Debug.LogError("ASA - Failed to save, but no exception was thrown.");
                return null;
            }

            Debug.Log($"ASA - Saved cloud anchor with ID: {cloudSpatialAnchor.Identifier}");
            anchorID = cloudSpatialAnchor.Identifier;
            anchorObject.GetComponent<MeshRenderer>().material.color = Color.green;
            return cloudSpatialAnchor;
        }
        catch (Exception exception)
        {
            Debug.Log("ASA - Failed to save anchor: " + exception.ToString());
            Debug.LogException(exception);
            return null;
        }

    }
    #endregion

    #region  Relays
    private async Task<string> CreateRelay(int maxConnections)
    {
        try
        {
            await NetworkHelper.InitializeRemoteServices();
            Allocation allocation =  await RelayService.Instance.CreateAllocationAsync(maxConnections);
            string joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

            //Configure and start network manager
            await ShutdownNetworkManager();
            RelayServerData relayServerData = new RelayServerData(allocation, "dtls");
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayServerData);
            NetworkManager.Singleton.StartHost();
           
            return joinCode;
        }
        catch (RelayServiceException e)
        {
            Debug.Log(e);
            return null;
        }
    }
    
    private async Task JoinRelay(string joinCode)
    {
        try
        {
            Debug.Log("Joining relay.");
            JoinAllocation joinAllocation = await RelayService.Instance.JoinAllocationAsync(joinCode);
            
            //Configure and start network manager
            await ShutdownNetworkManager();
            RelayServerData relayServerData = new RelayServerData(joinAllocation, "dtls");
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayServerData);
            NetworkManager.Singleton.StartClient();
        }
        catch (RelayServiceException e)
        {
            Debug.Log(e);
        }
    }

    #endregion

}

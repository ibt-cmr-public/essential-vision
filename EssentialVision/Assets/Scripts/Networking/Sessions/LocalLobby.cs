using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

public class LocalLobby
{
    public string name;
    public Guid guid;
    public int maxParticipantCount = 128;
    public int participantCount
    {
        get
        {
            return participants.Count;
        }
    }
    public bool localAnchorPlacement;
    public string anchorID;
    public string address;
    public ushort port;
    public Dictionary<string, LocalPlayer> participants = new Dictionary<string, LocalPlayer>();
    public string passcode = null;
    

    public LocalLobby(byte[] data)
    {
        try
        {
            // Deserialize the byte array into a SessionData object.
            string jsonData = Encoding.UTF8.GetString(data);
            JsonUtility.FromJsonOverwrite(jsonData, this);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to deserialize SessionData: {e}");
        }
    }

    // Default constructor.
    public LocalLobby()
    {
    }

    public LocalLobby(LobbyConfiguration configuration)
    {
        guid = Guid.NewGuid();
        name = configuration.name;
        maxParticipantCount = configuration.maxParticipantCount;
        localAnchorPlacement = configuration.localAnchorPlacement;
        port = LocalLobbyManager.Instance.ServerPort;
        address = NetworkHelper.GetLocalIPAddress().ToString();
        participants = new Dictionary<string, LocalPlayer>();
    }

    [JsonIgnore]
    public IPEndPoint endpoint
    {
        get
        {
            return new IPEndPoint(IPAddress.Parse(address),port);
        }
    }
    

    public bool AddPlayer(LocalPlayer player)
    {
        if (participants.Count >= maxParticipantCount)
        {
            return false;
        }
        string playerId = player.guid.ToString();
        participants[playerId] = player;
        return true;
    }
    
    
}

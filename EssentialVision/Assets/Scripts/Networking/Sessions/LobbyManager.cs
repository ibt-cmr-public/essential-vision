using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;




public class LobbyManager : MonoBehaviour {
    
    public static LobbyManager Instance { get; private set; }
    
    public const string KEY_PLAYER_NAME = "PlayerName";
    public const string KEY_JOIN_CODE = "RelayJoinCode";
    
    
    //Lobby Management
    public Action OnLeftLobby;
    public Action OnKickedFromLobby;
    public Action<Lobby> OnJoinedLobby;
    public Action<Lobby> OnJoinedLobbyUpdate;
    
    
    private float heartbeatTimer;
    private float lobbyPollTimer;
    private float refreshLobbyListTimer = 5f;
    private Lobby joinedLobby;


    private void Awake() {
        Instance = this;
    }
    
    
    //Update loop
    private void Update() {
        HandleLobbyHeartbeat();
        HandleLobbyPolling();
    }

    private async void HandleLobbyHeartbeat() {
        if (IsLobbyHost()) {
            heartbeatTimer -= Time.deltaTime;
            if (heartbeatTimer < 0f) {
                float heartbeatTimerMax = 15f;
                heartbeatTimer = heartbeatTimerMax;

                Debug.Log("Heartbeat");
                await LobbyService.Instance.SendHeartbeatPingAsync(joinedLobby.Id);
            }
        }
    }

    private async void HandleLobbyPolling() {
        if (joinedLobby != null) {
            lobbyPollTimer -= Time.deltaTime;
            if (lobbyPollTimer < 0f) {
                float lobbyPollTimerMax = 1.1f;
                lobbyPollTimer = lobbyPollTimerMax;

                joinedLobby = await LobbyService.Instance.GetLobbyAsync(joinedLobby.Id);

                OnJoinedLobbyUpdate?.Invoke(joinedLobby);

                if (!IsPlayerInLobby()) {
                    // Player was kicked out of this lobby
                    Debug.Log("Kicked from Lobby!");

                    OnKickedFromLobby?.Invoke();

                    joinedLobby = null;
                }
            }
        }
    }
    
    private bool IsPlayerInLobby() {
        if (joinedLobby != null && joinedLobby.Players != null) {
            foreach (Player player in joinedLobby.Players) {
                if (player.Id == AuthenticationService.Instance.PlayerId) {
                    // This player is in this lobby
                    return true;
                }
            }
        }
        return false;
    }

    public Lobby JoinedLobby {
        get
        {
            return joinedLobby;
        }
    }

    public bool IsLobbyHost() {
        return joinedLobby != null && joinedLobby.HostId == AuthenticationService.Instance.PlayerId;
    }


    private Player GetPlayer()
    {
        return PlayerManager.Instance.GetRemotePlayer();
    }
    

    //Lobby and player updates
    public async void UpdatePlayerName(string playerName) {
        if (joinedLobby != null) {
            try {
                UpdatePlayerOptions options = new UpdatePlayerOptions();

                options.Data = new Dictionary<string, PlayerDataObject>() {
                    {
                        KEY_PLAYER_NAME, new PlayerDataObject(
                            visibility: PlayerDataObject.VisibilityOptions.Public,
                            value: playerName)
                    }
                };

                string playerId = AuthenticationService.Instance.PlayerId;

                Lobby lobby = await LobbyService.Instance.UpdatePlayerAsync(joinedLobby.Id, playerId, options);
                joinedLobby = lobby;

                OnJoinedLobbyUpdate?.Invoke(joinedLobby);
            } catch (LobbyServiceException e) {
                Debug.Log(e);
            }
        }
    }
    
    // public async void UpdateLobbyData() {
    //     try {
    //         Lobby lobby = await Lobbies.Instance.UpdateLobbyAsync(joinedLobby.Id, new UpdateLobbyOptions {
    //             Data = new Dictionary<string, DataObject> {
    //                 //{ KEY_GAME_MODE, new DataObject(DataObject.VisibilityOptions.Public, gameMode.ToString()) }
    //             }
    //         });
    //
    //         joinedLobby = lobby;
    //
    //         OnLobbyGameModeChanged?.Invoke(this, new LobbyEventArgs { lobby = joinedLobby });
    //     } catch (LobbyServiceException e) {
    //         Debug.Log(e);
    //     }
    // }
    
    //Lobby management
    public async Task<Lobby> CreateLobby(string lobbyName, string relayJoinCode, int maxPlayers, bool isPrivate) {
        Player player = GetPlayer();

        CreateLobbyOptions options = new CreateLobbyOptions {
            Player = player,
            IsPrivate = isPrivate,
            Data = new Dictionary<string, DataObject> {
                { KEY_JOIN_CODE, new DataObject(DataObject.VisibilityOptions.Member, relayJoinCode) }
            }
        };

        Lobby lobby = await LobbyService.Instance.CreateLobbyAsync(lobbyName, maxPlayers, options);

        joinedLobby = lobby;

        OnJoinedLobby?.Invoke(lobby);
        
        Debug.Log("Created Lobby " + lobby.Name);

        return lobby;
    }
    
    public async Task JoinLobbyByCode(string lobbyCode) {
        Player player = GetPlayer();

        Lobby lobby = await LobbyService.Instance.JoinLobbyByCodeAsync(lobbyCode, new JoinLobbyByCodeOptions {
            Player = player
        });

        joinedLobby = lobby;

        OnJoinedLobby?.Invoke(lobby);
    }

    public async Task JoinLobby(Lobby lobby) {
        Player player = GetPlayer();

        joinedLobby = await LobbyService.Instance.JoinLobbyByIdAsync(lobby.Id, new JoinLobbyByIdOptions {
            Player = player
        });

        OnJoinedLobby?.Invoke(lobby);
    }
    
    public async void LeaveLobby() {
        if (joinedLobby != null) {
            try {
                await LobbyService.Instance.RemovePlayerAsync(joinedLobby.Id, AuthenticationService.Instance.PlayerId);
                Lobby lobbyToLeave = joinedLobby;
                joinedLobby = null;
                OnLeftLobby?.Invoke();
            } catch (LobbyServiceException e) {
                Debug.Log(e);
            }
        }
    }

    public async void KickPlayer(string playerId) {
        if (IsLobbyHost()) {
            try {
                await LobbyService.Instance.RemovePlayerAsync(joinedLobby.Id, playerId);
            } catch (LobbyServiceException e) {
                Debug.Log(e);
            }
        }
    }

    public async void DestroyLobby()
    {
        if (!IsLobbyHost()) return;
        try
        {
            await LobbyService.Instance.DeleteLobbyAsync(joinedLobby.Id);
            joinedLobby = null;
        }
        catch (LobbyServiceException e) {
            Debug.Log(e);
        }
    }
    
    public async void TransferLobbyHost(string newHostID)
    {
        if (IsLobbyHost()) {
            try
            {
                UpdateLobbyOptions updateLobbyOptions = new UpdateLobbyOptions()
                {
                    HostId = newHostID
                };
                Lobby lobby = await Lobbies.Instance.UpdateLobbyAsync(joinedLobby.Id, updateLobbyOptions);
                joinedLobby = lobby;
            } catch (LobbyServiceException e) {
                Debug.Log(e);
            }
        }
    }

    
}
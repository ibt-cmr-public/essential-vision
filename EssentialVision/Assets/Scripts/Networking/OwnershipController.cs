using MixedReality.Toolkit.SpatialManipulation;
using Unity.Netcode;
using UnityEngine.XR.Interaction.Toolkit;


//Component that automatically requests ownership of an object when manipulation through the corresponding object 
//manipulator is started and returns ownership to the server once manipulation is exited
public class OwnershipController : NetworkBehaviour
{

    private ObjectManipulator objectManipulator;
    private BoundsControl boundsControl;
    public ObjectManipulator ObjectManipulator
    {
        get
        {
            return objectManipulator;
        }
        set
        {
            if (objectManipulator != value)
            {
                DeregisterObjectManipulatorEvents();
                //Set new object manipulator and register events
                objectManipulator = value;
                RegisterObjectManipulatorEvents();
            }
        }
    }
    public BoundsControl BoundsControl
    {
        get
        {
            return boundsControl;
        }
        set
        {
            if (boundsControl != value)
            {
                DeregisterBoundsControlEvents();
                boundsControl = value;
                RegisterBoundsControlEvents();
            }
        }
    }

    private NetworkObject objectManipulatorTarget
    {
        get
        {
            if (ObjectManipulator == null)
            {
                return null;
            }

            if (ObjectManipulator.HostTransform == null)
            {
                return null;
            }
            else
            {
                return ObjectManipulator.HostTransform.GetComponent<NetworkObject>();
            }            
        }
    }
    
    private NetworkObject boundsControlTarget
    {
        get
        {
            if (BoundsControl == null)
            {
                return null;
            }

            if (BoundsControl.Target == null)
            {
                return null;
            }
            else
            {
                return BoundsControl.Target.GetComponent<NetworkObject>();
            }            
        }
    }
    
    private void Start()
    {
        ObjectManipulator = GetComponent<ObjectManipulator>();
        BoundsControl = GetComponent<BoundsControl>();
    }

    private void OnEnable()
    {
        RegisterObjectManipulatorEvents();
        RegisterBoundsControlEvents();
    }

    private void OnDisable()
    {
        DeregisterObjectManipulatorEvents();
        DeregisterBoundsControlEvents();
    }

    private void OnDestroy()
    {
        DeregisterObjectManipulatorEvents();
        DeregisterBoundsControlEvents();
    }

    private void OnApplicationQuit()
    {
        DeregisterObjectManipulatorEvents();
        DeregisterBoundsControlEvents();
    }

    private void RegisterObjectManipulatorEvents()
    {
        if (objectManipulator != null)
        {
            objectManipulator.lastSelectExited.AddListener(OnLastSelectExited);
            objectManipulator.firstSelectEntered.AddListener(OnFirstSelectEntered);
        }
    }

    private void DeregisterObjectManipulatorEvents()
    {
        //Deregister from previous events
        if (objectManipulator != null)
        {
            objectManipulator.lastSelectExited.RemoveListener(OnLastSelectExited);
            objectManipulator.firstSelectEntered.RemoveListener(OnFirstSelectEntered);
        }
    }
    
    private void RegisterBoundsControlEvents()
    {
        if (boundsControl != null)
        {
            boundsControl.ManipulationStarted.AddListener(OnFirstSelectEntered);
            boundsControl.ManipulationEnded.AddListener(OnLastSelectExited);
        }
    }

    private void DeregisterBoundsControlEvents()
    {
        //Deregister from previous events
        if (boundsControl != null)
        {
            boundsControl.ManipulationStarted.RemoveListener(OnFirstSelectEntered);
            boundsControl.ManipulationEnded.RemoveListener(OnLastSelectExited);
        }
    }

    
    private void OnLastSelectExited(SelectExitEventArgs eventArgs)
    {
        NetworkObject target = eventArgs.interactableObject.Equals(objectManipulator)
            ? objectManipulatorTarget
            : boundsControlTarget;
        if (target != null)
        {
            ReturnOwnershipServerRpc(target);
        }
    }
    
    private void OnFirstSelectEntered(SelectEnterEventArgs eventArgs)
    {
        NetworkObject target = eventArgs.interactableObject.Equals(objectManipulator)
            ? objectManipulatorTarget
            : boundsControlTarget;
        if (target != null)
        {
            RequestOwnershipServerRpc(target, NetworkManager.Singleton.LocalClientId);
        }
    }
    
    
    [ServerRpc(RequireOwnership = false)]
    private void RequestOwnershipServerRpc(NetworkObjectReference targetObjectReference, ulong clientId)
    {
        targetObjectReference.TryGet(out NetworkObject targetObject);
        if (targetObject != null)
        {
            if (targetObject.OwnerClientId != clientId)
            {
                targetObject.ChangeOwnership(clientId);
            }
        }
    }
    
    
    [ServerRpc(RequireOwnership = false)]
    private void ReturnOwnershipServerRpc(NetworkObjectReference targetObjectReference)
    {
        targetObjectReference.TryGet(out NetworkObject targetObject);
        if (targetObject != null)
        {
            if (!targetObject.IsOwner)
            {
                targetObject.RemoveOwnership();
            }
        }
    }
    
}

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;
using Unity.Netcode;
using Unity.Services.Authentication;
using Unity.Services.Core;

public static class TransformHelper
{

	public static Bounds TransformBounds(Bounds bounds, Matrix4x4 matrix)
	{
		// Extract the center and extents of the original bounds
		Vector3 originalCenter = bounds.center;
		Vector3 originalExtents = bounds.extents;

		// Calculate the rotated corners of the bounds in the new coordinate system
		Vector3[] rotatedCorners = new Vector3[8];
		for (int i = 0; i < 8; i++)
		{
			Vector3 corner = originalCenter + Vector3.Scale(originalExtents, GetCornerPoint(i));
			rotatedCorners[i] = matrix.MultiplyPoint(corner);
		}

		// Find the tightest bounds within the new coordinate system
		Bounds rotatedBounds = new Bounds(rotatedCorners[0], Vector3.zero);
		for (int i = 1; i < 8; i++)
		{
			rotatedBounds.Encapsulate(rotatedCorners[i]);
		}

		return rotatedBounds;
	}

	// Helper method to get the corner points of the bounds
	private static Vector3 GetCornerPoint(int index)
	{
		return new Vector3((index & 1) == 0 ? -1 : 1, (index & 2) == 0 ? -1 : 1, (index & 4) == 0 ? -1 : 1);
	}

}


public static class SerializationHelper
{
	//Network serialization for local messaging
	public static byte[] Serialize<T>(T serializableObject) 
	{
		try
		{
			string json = JsonConvert.SerializeObject(serializableObject);
			return Encoding.UTF8.GetBytes(json);
		}
		catch (Exception e)
		{
			Debug.LogError($"Failed to serialize object: {e}");
			return null;
		}
	}

	public static T Deserialize<T>(byte[] data)
	{
		try
		{
			string json = Encoding.UTF8.GetString(data);
			return JsonConvert.DeserializeObject<T>(json);
		}
		catch (Exception e)
		{
			Debug.LogError($"Failed to deserialize data: {e}");
			return default;
		}
	}
	
	public static void SerializeNullable<T>(BufferSerializer<T> serializer, ref SN<Vector3> value) 
		where T : IReaderWriter 
	{
		bool hasValue = value.HasValue;
		serializer.SerializeValue(ref hasValue);

		if (hasValue)
		{
			Vector3 actualValue = value.HasValue ? value.Value : new Vector3(); 
			serializer.SerializeValue(ref actualValue);
			value = actualValue;
		}
	}
	
	public static void SerializeNullable<T>(BufferSerializer<T> serializer, ref SN<Quaternion> value) 
		where T : IReaderWriter 
	{
		bool hasValue = value.HasValue;
		serializer.SerializeValue(ref hasValue);

		if (hasValue)
		{
			Quaternion actualValue = value.HasValue ? value.Value : new Quaternion(); 
			serializer.SerializeValue(ref actualValue);
			value = actualValue;
		}
	}
	

	public static void SerializeNullable<T>(BufferSerializer<T> serializer, ref Vector3? value) 
        where T : IReaderWriter 
    {
        bool hasValue = value.HasValue;
        serializer.SerializeValue(ref hasValue);

        if (hasValue)
        {
	        Vector3 actualValue = value ?? new Vector3(); 
	        serializer.SerializeValue(ref actualValue);
            value = actualValue;
        }
    }
    
    public static void SerializeNullable<T>(BufferSerializer<T> serializer, ref Quaternion? value) 
        where T : IReaderWriter 
    {
        bool hasValue = value.HasValue;
        serializer.SerializeValue(ref hasValue);

        if (hasValue)
        {
            Quaternion actualValue = value ?? new Quaternion(); 
            serializer.SerializeValue(ref actualValue);
            value = actualValue;
        }
    }
    
    public static void SerializeNullable<T>(BufferSerializer<T> serializer, ref int? value) 
	    where T : IReaderWriter 
    {
	    bool hasValue = value.HasValue;
	    serializer.SerializeValue(ref hasValue);

	    if (hasValue)
	    {
		    int actualValue = value ?? new int();
		    serializer.SerializeValue(ref actualValue);
		    value = actualValue;
	    }
    }
    
    public static void SerializeNullable<T>(BufferSerializer<T> serializer, ref LocalTransform? value) 
	    where T : IReaderWriter 
    {
	    bool hasValue = value.HasValue;
	    serializer.SerializeValue(ref hasValue);

	    if (hasValue)
	    {
		    LocalTransform actualValue = value ?? new LocalTransform();
		    serializer.SerializeValue(ref actualValue);
		    value = actualValue;
	    }
    }
}

public class NetworkHelper
{
	public static IPAddress GetLocalIPAddress()
	{
		IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
		foreach (IPAddress ip in host.AddressList)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{
				return ip;
			}
		}
		throw new Exception("Local IP Address not found.");
	}


	private static bool signingIn = false;
	public static async Task InitializeRemoteServices()
	{
		if (UnityServices.State == ServicesInitializationState.Uninitialized)
		{
			await UnityServices.InitializeAsync();
		}
		

		
		if (!AuthenticationService.Instance.IsSignedIn && !signingIn)
		{
			signingIn = true;
			try
			{
				// ParrelSync should only be used within the Unity Editor so you should use the UNITY_EDITOR define
				#if UNITY_EDITOR
					if (ParrelSync.ClonesManager.IsClone())
					{
						// When using a ParrelSync clone, switch to a different authentication profile to force the clone
						// to sign in as a different anonymous user account.
						string customArgument = ParrelSync.ClonesManager.GetArgument();
						AuthenticationService.Instance.SwitchProfile($"Clone_{customArgument}_Profile");
					}
				#endif
				await AuthenticationService.Instance.SignInAnonymouslyAsync();
			}
			finally
			{
				signingIn = false;
			}
		}
	}

}



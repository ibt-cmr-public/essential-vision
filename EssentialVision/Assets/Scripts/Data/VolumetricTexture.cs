using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "New Volumetric Texture", menuName = "EssentialVision/Volumetric Texture")]
[Serializable]
public class VolumetricTexture: ScriptableObject
{
    [JsonIgnore]
    public Texture3D texture;
    public string caption;
    public string description;
    [JsonProperty(ItemConverterType = typeof(Vector3JSONConverter))]
    public Vector3 localPosition;
    [JsonProperty(ItemConverterType = typeof(Vector3JSONConverter))]
    public Vector3 localRotation;
    [JsonProperty(ItemConverterType = typeof(Vector3JSONConverter))]
    public Vector3 localScale;

    public void SaveTextureDataToFile(string filePath)
    {
        // Get texture dimensions
        int width = texture.width;
        int height = texture.height;
        int depth = texture.depth;

        // Get raw pixel data from texture
        Color[] pixels = texture.GetPixels();

        // Create binary writer for file
        using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
        {
            // Write texture dimensions
            writer.Write(width);
            writer.Write(height);
            writer.Write(depth);

            // Write pixel data
            foreach (Color pixel in pixels)
            {
                writer.Write(pixel.r);
                writer.Write(pixel.g);
                writer.Write(pixel.b);
                writer.Write(pixel.a);
            }
        }
    }
    
    public void LoadTextureFromFile(string filePath)
    {
        // Create binary reader for file
        using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
        {
            // Read texture dimensions
            int width = reader.ReadInt32();
            int height = reader.ReadInt32();
            int depth = reader.ReadInt32();

            // Read pixel data
            Color[] pixels = new Color[width * height * depth];
            for (int i = 0; i < pixels.Length; i++)
            {
                float r = reader.ReadSingle();
                float g = reader.ReadSingle();
                float b = reader.ReadSingle();
                float a = reader.ReadSingle();
                pixels[i] = new Color(r, g, b, a);
            }

            // Create Texture3D and set pixel data
            texture = new Texture3D(width, height, depth, TextureFormat.RGBA32, false);
            texture.SetPixels(pixels);
            texture.Apply();
        }
    }
    
    public byte[] ToBinary()
    {
        return new byte[0];
    }
    
    public static VolumetricTexture FromBinary(byte[] data)
    {
        return CreateInstance<VolumetricTexture>();
    }
    
    
    
}

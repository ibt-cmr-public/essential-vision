using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

[Serializable]
public struct LocalTransform : INetworkSerializable
{
    public Vector3 localPosition;
    public Quaternion localRotation;
    public Vector3 localScale ;

    // public LocalTransform()
    // {
    // }
    public LocalTransform(Vector3 position, Quaternion rotation, Vector3 scale)
    {
        localPosition = position;
        localRotation = rotation;
        localScale = scale;
    }

    public LocalTransform(Transform transform)
    {
        localPosition = transform.localPosition;
        localRotation = transform.localRotation;
        localScale = transform.localScale;
    }

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref localPosition);
        serializer.SerializeValue(ref localRotation);
        serializer.SerializeValue(ref localScale);
        // SerializationHelper.SerializeNullable(serializer, ref localPosition);
        // SerializationHelper.SerializeNullable(serializer, ref localRotation);
        // SerializationHelper.SerializeNullable(serializer, ref localScale);
    }
}


[Serializable]
public class NodeState: INetworkSerializable
{
    public LocalTransform localTransform;
    public bool tooltipShowing;
    
    public virtual void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref localTransform);
        serializer.SerializeValue(ref tooltipShowing);
    }
}


[Serializable]
public class LayerState: NodeState
{
    public bool isHidden;
    public bool isOutlined;
    public bool isFaded;
    
    public override void NetworkSerialize<T>(BufferSerializer<T> serializer)
    {
        base.NetworkSerialize<T>(serializer);
        serializer.SerializeValue(ref isHidden);
        serializer.SerializeValue(ref isOutlined);
        serializer.SerializeValue(ref isFaded);
    }
}


[Serializable]
public class AnimationState: INetworkSerializable
{
    [SerializeField] public float currentTime = 0f;
    [SerializeField] public bool isPlaying = true;
    [SerializeField] public float animationSpeed = 1f;
    [SerializeField] public AnimationMode animationMode = AnimationMode.Loop;
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref currentTime);
        serializer.SerializeValue(ref isPlaying);
        serializer.SerializeValue(ref animationSpeed);
        serializer.SerializeValue(ref animationMode);
    }
}

[Serializable]
public class ClippingPlaneState: INetworkSerializable
{
    [SerializeField] public bool isActive = false;
    [SerializeField] public int volumetricTextureIdx = 0;
    [SerializeField] public ClippingPlaneType type = ClippingPlaneType.Plane;
    [SerializeField] public LocalTransform transform = new LocalTransform();
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref isActive);
        serializer.SerializeValue(ref volumetricTextureIdx);
        serializer.SerializeValue(ref type);
        serializer.SerializeValue(ref transform);
    }
}

[Serializable]
public class UIState: INetworkSerializable
{
    [Header("Active buttons")]
    // Shortcut Buttons
    [SerializeField] private bool sideMenuButton = true;
    [SerializeField] private bool boundingBoxButton = true;
    [SerializeField] private bool freeMoveButton = true;
    [SerializeField] private bool explodeViewButton = true;
    [SerializeField] private bool resetViewButton = true;

    [Header("Active menus")]
    // SideMenus
    [SerializeField] private bool animationMenuActive = true;
    [SerializeField] private bool sideMenuActive = true;
        
    //Tabs
    [Header("Tabs")]
    [SerializeField] private int currentTab = 0;
    [SerializeField] private bool[] tabsActive = new bool[] { true, true, true };
   
        
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref sideMenuButton);
        serializer.SerializeValue(ref boundingBoxButton);
        serializer.SerializeValue(ref freeMoveButton);
        serializer.SerializeValue(ref explodeViewButton);
        serializer.SerializeValue(ref resetViewButton);

        serializer.SerializeValue(ref animationMenuActive);
        serializer.SerializeValue(ref sideMenuActive);

        serializer.SerializeValue(ref currentTab);
        serializer.SerializeValue(ref tabsActive);
    }
}


[Serializable]
[CreateAssetMenu(fileName = "New Model State", menuName = "EssentialVision/Model State")]
public class ModelInstanceState : ScriptableObject, INetworkSerializable
{
    [Header("Model transform")] 
    [SerializeField] public LocalTransform modelTransform;
    
    [Header("Layers")] 
    [SerializeField] public Guid selectedLayerGuid = Guid.Empty;

    [SerializeField]
    public Dictionary<Guid, NodeState> nodeStates = new Dictionary<Guid, NodeState>();
    
    [Header("Substates")] 
    [SerializeField] public UIState interfaceState = new UIState();
    [SerializeField] public AnimationState animationState = new AnimationState();
    [SerializeField] public ClippingPlaneState clippingPlaneState = new ClippingPlaneState();
    
    [Header ("Settings")]
    [SerializeField] public bool boundsActive = false;
    [SerializeField] public bool freeMovementActive = false;
    [SerializeField] public bool autoRotation = false;
    [SerializeField] public float rotationSpeed = 0.1f;
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref modelTransform);
        serializer.SerializeValue(ref boundsActive);
        serializer.SerializeValue(ref freeMovementActive);
        serializer.SerializeValue(ref autoRotation);
        serializer.SerializeValue(ref rotationSpeed);
        
        //Layers
        string selectedLayerGuidString = selectedLayerGuid.ToString();
        serializer.SerializeValue(ref selectedLayerGuidString);
        selectedLayerGuid = Guid.Parse(selectedLayerGuidString);
        
        int nodeStateCount = nodeStates.Count;
        serializer.SerializeValue(ref nodeStateCount);
        if (serializer.IsReader)
        {
            nodeStates.Clear();
            for (int i = 0; i < nodeStateCount; i++)
            {
                string guidString = string.Empty;
                serializer.SerializeValue(ref guidString);
                Guid guid = Guid.Parse(guidString);
            
                NodeState nodeState = new NodeState();
                serializer.SerializeValue(ref nodeState);
            
                nodeStates[guid] = nodeState;
            }
        }
        else
        {
            foreach (var kvp in nodeStates)
            {
                string guidString = kvp.Key.ToString();
                serializer.SerializeValue(ref guidString);
            
                NodeState nodeState = kvp.Value;
                serializer.SerializeValue(ref nodeState);
            }
        }

        //Substates
        bool serializeInterfaceState = interfaceState != null;
        serializer.SerializeValue(ref serializeInterfaceState);
        if (serializeInterfaceState)
        {
            serializer.SerializeValue(ref interfaceState);
        }
        
        bool serializeAnimationState = animationState != null;
        serializer.SerializeValue(ref serializeAnimationState);
        if (serializeAnimationState)
        {
            serializer.SerializeValue(ref animationState);
        }

        bool serializeClippingPlaneState = clippingPlaneState != null;
        serializer.SerializeValue(ref serializeClippingPlaneState);
        if (serializeClippingPlaneState)
        {
            serializer.SerializeValue(ref clippingPlaneState);
        }
        
    }
}




using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using UnityEngine;


[System.Serializable]
class SerializableMeshInfo
{
    [JsonProperty] private int meshTopology;
    [JsonProperty] private float[] vertices;
    [JsonProperty] private float[] uv;
    [JsonProperty] private float[] uv2;
    [JsonProperty] private float[] normals;
    [JsonProperty] private float[] tangents;
    [JsonProperty] private int[] indices;
    [JsonProperty] private SerializableColor[] serializedColors;
    [JsonProperty] private BlendShapeData[] blendShapes;
    [JsonProperty] private BoundsData boundsData;
    
    [System.Serializable]
    class BoundsData
    {
        [JsonProperty] public float[] center;
        [JsonProperty] public float[] size;
    }
    
    [System.Serializable]
    class BlendShapeData
    {
        [JsonProperty] public string name;
        [JsonProperty] public float[] vertices;
        [JsonProperty] public float[] normals;
        [JsonProperty] public float[] tangents;
    }
    

    public SerializableMeshInfo()
    {
    }

    public SerializableMeshInfo(Mesh m)
    {
        meshTopology = (int)m.GetTopology(0);
        vertices = FlattenVector3s(m.vertices);
        indices = m.GetIndices(0);
        uv = FlattenVector2s(m.uv);
        uv2 = FlattenVector2s(m.uv2);
        normals = FlattenVector3s(m.normals);
        tangents = FlattenVector4s(m.tangents);
        serializedColors = FlattenColors(m.colors);
        
        boundsData = new BoundsData
        {
            center = new float[] {m.bounds.center.x, m.bounds.center.y, m.bounds.center.z},
            size = new float[] {m.bounds.size.x, m.bounds.size.y, m.bounds.size.z}
        };
        
        blendShapes = new BlendShapeData[m.blendShapeCount];
        for (int i = 0; i < m.blendShapeCount; i++)
        {
            string blendShapeName = m.GetBlendShapeName(i);
            Vector3[] blendShapeVertices = new Vector3[m.vertexCount];
            Vector3[] blendShapeNormals = new Vector3[m.vertexCount];
            Vector3[] blendShapeTangents = new Vector3[m.vertexCount];
            m.GetBlendShapeFrameVertices(i, 0, blendShapeVertices, blendShapeNormals, blendShapeTangents);
            blendShapes[i] = new BlendShapeData
            {
                name = blendShapeName,
                vertices = FlattenVector3s(blendShapeVertices),
                normals = FlattenVector3s(blendShapeNormals),
                tangents = FlattenVector3s(blendShapeTangents)
            };
        }
    }

    // Helper methods for UVs, normals, colors, and blend shapes are unchanged.

    public Mesh GetMesh()
    {
        Mesh m = new Mesh();
        m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        // Vertices
        Vector3[] meshVertices = new Vector3[vertices.Length / 3];
        for (int i = 0; i < meshVertices.Length; i++)
        {
            int index = i * 3;
            meshVertices[i] = new Vector3(vertices[index], vertices[index + 1], vertices[index + 2]);
        }
        m.vertices = meshVertices;
        
        // UVs
        m.uv = UnflattenVector2s(uv);

        // UV2s
        m.uv2 = UnflattenVector2s(uv2);

        // Triangles
        m.SetIndices(indices, (MeshTopology)meshTopology, 0);
        
        // Normals
        m.normals = UnflattenVector3s(normals);

        // Colors
        m.colors = UnflattenColors(serializedColors);
        
        //Bounds
        m.bounds = new Bounds(
            new Vector3(boundsData.center[0], boundsData.center[1], boundsData.center[2]),
            new Vector3(boundsData.size[0], boundsData.size[1], boundsData.size[2]));

        // Blend Shapes
        for (int i = 0; i < blendShapes.Length; i++)
        {
            BlendShapeData blendShape = blendShapes[i];
            m.AddBlendShapeFrame(
                blendShape.name,
                100f,
                UnflattenVector3s(blendShape.vertices),
                UnflattenVector3s(blendShape.normals),
                UnflattenVector3s(blendShape.tangents));
        }
        
        return m;
    }


    // Helper method to flatten UVs or UV2s
    private float[] FlattenVector2s(Vector2[] uvs)
    {
        float[] flattenedVectors = new float[uvs.Length * 2];
        for (int i = 0; i < uvs.Length; i++)
        {
            int index = i * 2;
            Vector2 uv = uvs[i];
            flattenedVectors[index] = uv.x;
            flattenedVectors[index + 1] = uv.y;
        }
        return flattenedVectors;
    }

    // Helper method to flatten normals
    private float[] FlattenVector3s(Vector3[] vectors)
    {
        float[] flattenedVectors = new float[vectors.Length * 3];
        for (int i = 0; i < vectors.Length; i++)
        {
            int index = i * 3;
            Vector3 vector = vectors[i];
            flattenedVectors[index] = vector.x;
            flattenedVectors[index + 1] = vector.y;
            flattenedVectors[index + 2] = vector.z;
        }
        return flattenedVectors;
    }
    
    // Helper method to flatten Vector4s
    private float[] FlattenVector4s(Vector4[] vectors)
    {
        float[] flattenedVectors = new float[vectors.Length * 4];
        for (int i = 0; i < vectors.Length; i++)
        {
            int index = i * 4;
            Vector4 vector = vectors[i];
            flattenedVectors[index] = vector.x;
            flattenedVectors[index + 1] = vector.y;
            flattenedVectors[index + 2] = vector.z;
            flattenedVectors[index + 3] = vector.w;
        }
        return flattenedVectors;
    }
    
    // Helper method to flatten colors
    private SerializableColor[] FlattenColors(Color[] colors)
    {
        SerializableColor[] flattenedColors = new SerializableColor[colors.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            flattenedColors[i] = new SerializableColor(colors[i]);
        }
        return flattenedColors;
    }
    
    
    // Helper method to unflatten UVs or UV2s
    private Vector2[] UnflattenVector2s(float[] flattenedVectors)
    {
        Vector2[] vectors = new Vector2[flattenedVectors.Length / 2];
        for (int i = 0; i < vectors.Length; i++)
        {
            int index = i * 2;
            vectors[i] = new Vector2(flattenedVectors[index], flattenedVectors[index + 1]);
        }
        return vectors;
    }

    // Helper method to unflatten colors
    private Color[] UnflattenColors(SerializableColor[] serializedColors)
    {
        Color[] colors = new Color[serializedColors.Length];
        for (int i = 0; i < serializedColors.Length; i++)
        {
            colors[i] = serializedColors[i].ToColor();
        }
        return colors;
    }

    // Helper method to unflatten normals
    private Vector3[] UnflattenVector3s(float[] flattenedVectors)
    {
        Vector3[] vectors = new Vector3[flattenedVectors.Length / 3];
        for (int i = 0; i < vectors.Length; i++)
        {
            int index = i * 3;
            vectors[i] = new Vector3(flattenedVectors[index], flattenedVectors[index + 1], flattenedVectors[index + 2]);
        }
        return vectors;
    }
    
    // Helper method to unflatten Vector4s
    private Vector4[] UnflattenVector4s(float[] flattenedVectors)
    {
        Vector4[] vectors = new Vector4[flattenedVectors.Length / 4];
        for (int i = 0; i < vectors.Length; i++)
        {
            int index = i * 4;
            vectors[i] = new Vector4(flattenedVectors[index], flattenedVectors[index + 1], flattenedVectors[index + 2], flattenedVectors[index + 3]);
        }
        return vectors;
    }
    
    public byte[] ToBinary()
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            using (BsonWriter bsonWriter = new BsonWriter(memoryStream))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(bsonWriter, this);
            }
            return memoryStream.ToArray();
        }
    }

    public static SerializableMeshInfo FromBinary(byte[] data)
    {
        using (MemoryStream memoryStream = new MemoryStream(data))
        {
            using (BsonReader bsonReader = new BsonReader(memoryStream))
            {
                JsonSerializer serializer = new JsonSerializer();
                return serializer.Deserialize<SerializableMeshInfo>(bsonReader);
            }
        }
    }
    

}
﻿using Kitware.VTK;
using UnityEngine;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    class TurbulenceFrame : VTKFrame
    {
        private int numberOfVertices;

        public TurbulenceFrame(string inputPath) : base(inputPath)
        {
            numberOfVertices = (int)vtkModel.GetNumberOfCells();
            ImportVerticesAndVectors();
            ComputePointIndices(numberOfVertices);
            ImportTurbulenceColors();
        }

        private void ImportVerticesAndVectors()
        {
            Vertices = new Vector3[numberOfVertices];
            Normals = new Vector3[numberOfVertices];
            for (int currentVertexNumber = 0; currentVertexNumber < numberOfVertices; currentVertexNumber++)
            {
                int[] cellIds = new int[2] {
                    (int)vtkModel.GetCell(currentVertexNumber).GetPointIds().GetId(0),
                    (int)vtkModel.GetCell(currentVertexNumber).GetPointIds().GetId(1)
                };
                Vertices[currentVertexNumber] = new Vector3((float)vtkModel.GetPoint(cellIds[1])[0], (float)vtkModel.GetPoint(cellIds[1])[1], (float)-vtkModel.GetPoint(cellIds[1])[2]);
                Normals[currentVertexNumber] = new Vector3((float)vtkModel.GetPoint(cellIds[1])[0], (float)vtkModel.GetPoint(cellIds[1])[1], (float)-vtkModel.GetPoint(cellIds[1])[2]) - new Vector3((float)vtkModel.GetPoint(cellIds[0])[0], (float)vtkModel.GetPoint(cellIds[0])[1], (float)-vtkModel.GetPoint(cellIds[0])[2]);
            }
        }

        private void ImportTurbulenceColors()
        {
            // Kitware.VTK.dll automatically scales colours to 0-255 range.
            Tangents = new Vector3[numberOfVertices];
            vtkDataArray colors = vtkModel.GetCellData().GetScalars("Colors");
            for(int i = 0; i < numberOfVertices; i++)
            {
                Tangents[i] = new Vector3((float)colors.GetTuple3(i)[0] / 255, (float)colors.GetTuple3(i)[1] / 255, (float)colors.GetTuple3(i)[2] / 255);
            }
        }
    }
}

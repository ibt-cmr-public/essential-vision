﻿using System.IO;

using ModelConversion.LayerConversion.FrameImport.VTK;

namespace ModelConversion.LayerConversion.FrameImport
{
    class FrameFactory
    {
        private string extension;

        public IFrame Import(string inputPath, NodeType dataType)
        {
            extension = Path.GetExtension(inputPath);
            switch (extension)
            {
                case ".vtk":
                case ".vtu":
                case ".vtp":
                    return ImportVTKFrame(inputPath, dataType);
                default:
                    throw new IOException("Wrong file extension! Only supporting VTK files");
                    break;
            }
        }

        private IFrame ImportVTKFrame(string inputPath, NodeType dataType)
        {
            switch (dataType)
            {
                case NodeType.Mesh:
                    return new AnatomyFrame(inputPath);
                case NodeType.Fibre:
                    return new FibreFrame(inputPath);
                case NodeType.Flow:
                    return new FlowFrame(inputPath);
                case NodeType.Turbulence:
                    return new TurbulenceFrame(inputPath);
                case NodeType.Displacement:
                    return new DisplacementFrame(inputPath);
                default:
                    throw new IOException("Wrong model datatype in ModelInfo.json! \n Currently supporting: \"anatomy\" \"fibre\" \"flow\" and \"turbulence\" ");
                    break;
            }
        }
    }
}

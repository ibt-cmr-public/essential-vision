﻿using System.Linq;
using Kitware.VTK;
using UnityEngine;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    abstract class VTKFrame : IFrame
    {

        public Bounds Bounds { get; protected set; }
        public Vector3[] Vertices { get; protected set; }
        public int NumberOfFacetEdges { get; protected set; }
        public int[] Indices { get; protected set; }
        public Vector3[] Normals { get; protected set; } = null;
        public Vector3[] Tangents { get; protected set; } = null;

        protected vtkDataSet vtkModel;

        public VTKFrame(string inputPath)
        {
            vtkModel = ReadVTKData(inputPath);
            ImportFrameBounds();
        }

        public void NormalizeVectors(float scalingFactor)
        {
            Bounds = new Bounds(Bounds.center * scalingFactor, Bounds.size * scalingFactor);
            Vertices = ScaleVector3s(Vertices, scalingFactor);
            if (Normals != null)
            {
                Normals = ScaleVector3s(Normals, scalingFactor);
            }
        }

        private vtkDataSet ReadVTKData(string path)
        {
            using (vtkDataSetReader reader = new vtkDataSetReader())
            {
                reader.ReadAllScalarsOn();
                reader.GetReadAllScalars();
                reader.ReadAllVectorsOn();
                reader.GetReadAllVectors();
                reader.ReadAllColorScalarsOn();
                reader.GetReadAllColorScalars();
                reader.SetFileName(path);
                reader.Update();
                return reader.GetOutput();
            }
        }

        private void ImportFrameBounds()
        {
            double[] boundingCoordinates = vtkModel.GetBounds();
            Vector3 minVertex = new Vector3((float)boundingCoordinates[0], (float)boundingCoordinates[2], (float)-boundingCoordinates[4]);
            Vector3 maxVertex = new Vector3((float)boundingCoordinates[1], (float)boundingCoordinates[3], (float)-boundingCoordinates[5]);
            Bounds = new Bounds((minVertex + maxVertex) / 2, maxVertex - minVertex);
        }

        protected void ImportVertices()
        {
            int numberOfPoints = (int)vtkModel.GetNumberOfPoints();
            Vertices = new Vector3[numberOfPoints];
            for (int i = 0; i < numberOfPoints; i++)
            {
                double[] currentPoint = vtkModel.GetPoint(i);
                Vertices[i] = new Vector3((float)currentPoint[0], (float)currentPoint[1], (float)-currentPoint[2]);
            }
        }

        protected virtual void ImportIndices()
        {
            int numberOfCells = (int)vtkModel.GetNumberOfCells();
            NumberOfFacetEdges = vtkModel.GetMaxCellSize();
            Indices = new int[NumberOfFacetEdges * numberOfCells];
            int currentIndexNumber = 0;
            for (int i = 0; i < numberOfCells; i++)
            {
                currentIndexNumber = ImportCellIndices(currentIndexNumber, vtkModel.GetCell(i).GetPointIds());
            }
        }

        protected void ComputePointIndices(int numberOfPoints)
        {
            NumberOfFacetEdges = 3;
            Indices = new int[numberOfPoints * 3];
            int currentVertexNumber = 0;
            for (int i = 0; i < Indices.Length; i += 3)
            {
                for (int j = 0; j < 3; j++)
                {
                    Indices[i + j] = currentVertexNumber;
                }
                currentVertexNumber++;
            }
        }

        private int ImportCellIndices(int currentIndexNumber, vtkIdList cellIndices)
        {
            int numberOfIndices = (int)cellIndices.GetNumberOfIds();
            for (int j = 0; j < numberOfIndices; j++)
            {
                Indices[currentIndexNumber] = (int)cellIndices.GetId(j);
                currentIndexNumber += 1;
            }
            return currentIndexNumber;
        }
        

        private Vector3[] ScaleVector3s(Vector3[] array, float scalingFactor)
        {
            return array.Select(x => x * scalingFactor).ToArray();
        }
        
        
        protected void CalculateMeshNormals()
        {
            Normals = new Vector3[Vertices.Length];
            int[] facetIndices = new int[3];
            for (int i = 0; i < Indices.Length; i += NumberOfFacetEdges)
            {
                for (int j = 0; j < NumberOfFacetEdges; j++)
                {
                    facetIndices[j] = Indices[i + j];
                }
                UpdateNormals(facetIndices);
            }
            foreach (Vector3 normal in Normals)
            {
                normal.Normalize();
            }
        }
        //Updates normals of the vertices belonging to the input facet.
        private void UpdateNormals(int[] facetIndices)
        {
            Vector3 currentNormal = new Vector3();
            currentNormal = CalculateFacetNormal(facetIndices);
            foreach (int index in facetIndices)
            {
                Normals[index] += currentNormal;
            }
        }

        //Calculates a normal of a facet.
        private Vector3 CalculateFacetNormal(int[] facetIndices)
        {
            Vector3[] facetVertices = new Vector3[3];
            for (int i = 0; i < 3; i++)
            {
                facetVertices[i] = Vertices[facetIndices[i]];
            }
            Vector3 normal = Vector3.Cross(facetVertices[0] - facetVertices[2], facetVertices[1] - facetVertices[0]);
            normal.Normalize();
            return normal;
        }
    }
}

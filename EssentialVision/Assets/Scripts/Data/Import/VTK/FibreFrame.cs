﻿
using Kitware.VTK;
using UnityEngine;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    class FibreFrame : VTKFrame
    {
        private int numberOfPoints;

        public FibreFrame(string inputPath) : base(inputPath)
        {
            numberOfPoints = (int)vtkModel.GetNumberOfPoints();
            ComputePointIndices(numberOfPoints);
            ImportVertices();
            ImportVectors();
            ImportAngles();
        }

        private void ImportVectors()
        {
            Normals = new Vector3[numberOfPoints];

            vtkDataArray vtkVectors = vtkModel.GetPointData().GetVectors("fn");
            for (int i = 0; i < numberOfPoints; i++)
            {
                Normals[i] = new Vector3((float)vtkVectors.GetTuple3(i)[0], (float)vtkVectors.GetTuple3(i)[1], (float)-vtkVectors.GetTuple3(i)[2]);
            }
        }

        private void ImportAngles()
        {
            Tangents = new Vector3[numberOfPoints];
            vtkPointData pointData = vtkModel.GetPointData();
            int arrayNumbers = pointData.GetNumberOfArrays();
            vtkDataArray alphaAngles = pointData.GetScalars("alpha");
            vtkDataArray betaAngles = pointData.GetScalars("beta");
            for (int i = 0; i < numberOfPoints; i++)
            {
                Tangents[i] = new Vector3((float)alphaAngles.GetTuple1(i), (float)betaAngles.GetTuple1(i), 0.0f);
            }
        }
    }
}

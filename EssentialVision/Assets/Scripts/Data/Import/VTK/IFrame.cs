﻿using UnityEngine;


interface IFrame
{
    public Bounds Bounds { get; }
    Vector3[] Vertices { get; }
    int NumberOfFacetEdges { get; }
    int[] Indices { get; }
    Vector3[] Normals { get; }
    Vector3[] Tangents { get; }

    void NormalizeVectors(float scalingFactor);
    
}


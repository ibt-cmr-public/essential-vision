﻿using Kitware.VTK;
using UnityEngine;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    class DisplacementFrame : VTKFrame
    {
        private int numberOfVertices;
        private int numberOfPoints;

        public DisplacementFrame(string inputPath) : base(inputPath)
        {
            ImportVertices();
            ImportIndices();
            CalculateMeshNormals();
            numberOfVertices = (int)vtkModel.GetNumberOfCells();
            numberOfPoints = (int)vtkModel.GetNumberOfPoints();
            ImportDisplacementColors();
        }

        private void ImportDisplacementColors()
        {
            // Kitware.VTK.dll automatically scales colours to 0-255 range.
            //Scalars = new double[numberOfVertices][];
            Tangents = new Vector3[numberOfPoints];
            vtkPointData pointData = vtkModel.GetPointData();
            int arrayNumbers = pointData.GetNumberOfArrays();
            vtkDataArray alphaAngles = pointData.GetScalars("Displacement");
            for (int i = 0; i < numberOfPoints; i++)
            {
                Tangents[i] = new Vector3((float)alphaAngles.GetTuple1(i), 0.0f, 0.0f);
            }
        }
    }
}

using System.Collections.Generic;
using System.IO;
using System.Linq;
using ModelConversion.LayerConversion.FrameImport;
using UnityEngine;

public class MeshImporter 
{
    
    private static FrameFactory frameImporter = new FrameFactory();
    
    // Start is called before the first frame update
    public Mesh StaticMeshFromVTK(string filePath, NodeType dataType)
    {
        IFrame frame = frameImporter.Import(filePath, dataType);
        int numberOfVertices = frame.Vertices.Length;
        Mesh mesh = new Mesh();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.SetVertices(frame.Vertices);
        mesh.bounds = frame.Bounds; ;
        mesh.SetIndices(frame.Indices, MeshTopology.Triangles, 0);
        
        if (frame.Normals != null)
        {
            mesh.SetNormals(frame.Normals);
        }
        else
        {
            mesh.RecalculateNormals();
        }
        
        Vector4[] tangents = new Vector4[numberOfVertices];
        for (int i = 0; i < tangents.Length; i++)
        {
            tangents[i].w = 1;
        }
        mesh.SetTangents(tangents);
        return mesh;
    }

    public Mesh AnimatedMeshFromVTK(List<string> filePaths, NodeType dataType)
    {

        bool firstMesh = true;
        Mesh mesh = new Mesh();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        for (int i = 0; i < filePaths.Count; i++)
        {
            string logMessage = "Importing file nr: " + i.ToString();
            Debug.Log(logMessage);
            IFrame frame = frameImporter.Import(filePaths[i], dataType);

            if (firstMesh)
            {
                int numberOfVertices = frame.Vertices.Length;
                mesh.SetVertices(new Vector3[numberOfVertices]);
                mesh.bounds = frame.Bounds;
                mesh.SetIndices(frame.Indices, MeshTopology.Triangles, 0);
                mesh.SetNormals(new Vector3[numberOfVertices]);
                Vector4[] tangents = new Vector4[numberOfVertices];
                for (int j = 0; j < tangents.Length; j++)
                {
                    tangents[j].w = 1;
                }

                mesh.SetTangents(tangents);
            }

            //Check topology
            bool equalTopology = mesh.GetIndices(0).SequenceEqual(frame.Indices);
            if (!equalTopology)
            {
                Debug.LogWarning("Topology isn't the same! Frame nr: " + i.ToString());
            }

            mesh.bounds.Encapsulate(frame.Bounds);
            mesh.AddBlendShapeFrame(Path.GetFileName(filePaths[i]), 100f, frame.Vertices, frame.Normals,
                frame.Tangents);
        }

        return mesh;
    }
    

    
    // private double GetScalingFactor(IFrame frame)
    // {
    //     double minAbsBounding = Math.Abs(frame.BoundingBox[0]);
    //     foreach (double bounding in frame.BoundingBox)
    //     {
    //         minAbsBounding = Math.Min(Math.Abs(minAbsBounding), Math.Abs(bounding));
    //     }
    //
    //     double scalingPower = 0 - Math.Floor(Math.Log10(minAbsBounding));
    //     return Math.Pow(10, scalingPower);
    //
    // }
}

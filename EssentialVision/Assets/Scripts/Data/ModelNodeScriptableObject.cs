using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using TreeView;


public enum NodeType
{
    Empty,
    Mesh,
    Displacement,
    Flow,
    Turbulence,
    Fibre
}


[CreateAssetMenu(fileName = "New Layer", menuName = "EssentialVision/Node/Empty Node")]
[Serializable]
[JsonConverter(typeof(NodeJSONConverter))]
public class ModelNodeScriptableObject: ScriptableObject, ITreeIMGUIData
{

    public event Action OnNodeInfoChanged;
    
    //Serialized private fields
    [SerializeField] private string name;
    public string Name
    {
        get { return name; }
        set
        {
            if (name != value)
            {
                name = value;
                OnNodeInfoChanged?.Invoke();
            }
        }
    }
    
    [SerializeField] private string description;
    public string Description
    {
        get { return description; }
        set
        {
            if (description != value)
            {
                description = value;
                OnNodeInfoChanged?.Invoke();
            }
        }
    }
    
    public virtual NodeType Type
    {
        get { return NodeType.Empty; }
    }
    
    [JsonIgnore]
    public bool isExpanded { get; set; }
    
    #region Serialization
    public virtual async Task<byte[]> ToBinary()
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            using (BinaryWriter writer = new BinaryWriter(memoryStream))
            {
                writer.Write(Name);
                writer.Write(Description);
                writer.Write((int)Type);
            }
            return memoryStream.ToArray();
        }
    }
    
    public static ModelNodeScriptableObject FromBinary(byte[] data)
    {
        
        ModelNodeScriptableObject node = CreateInstance<ModelNodeScriptableObject>();
        
        using (MemoryStream memoryStream = new MemoryStream(data))
        {
            using (BinaryReader reader = new BinaryReader(memoryStream))
            {
                node.Name = reader.ReadString();
                node.Description = reader.ReadString();
            }
        }
        
        return node;
    }
    #endregion
    

}

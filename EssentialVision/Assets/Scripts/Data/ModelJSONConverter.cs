
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.AddressableAssets;


public class ColorJSONConverter : JsonConverter<Color>
{
    public override void WriteJson(JsonWriter writer, Color value, JsonSerializer serializer)
    {
        writer.WriteStartArray();
        writer.WriteValue(value.r);
        writer.WriteValue(value.g);
        writer.WriteValue(value.b);
        writer.WriteValue(value.a);
        writer.WriteEndArray();
    }

    public override Color ReadJson(JsonReader reader, System.Type objectType, Color existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        JArray jsonArray = JArray.Load(reader);
        if (jsonArray.Count == 4)
        {
            float r = jsonArray[0].Value<float>();
            float g = jsonArray[1].Value<float>();
            float b = jsonArray[2].Value<float>();
            float a = jsonArray[3].Value<float>();
            return new Color(r, g, b, a);
        }
        else
        {
            return Color.white; // Default color if JSON doesn't contain valid color data
        }
    }
}

public class Vector3JSONConverter : JsonConverter<Vector3>
{
    public override void WriteJson(JsonWriter writer, Vector3 value, JsonSerializer serializer)
    {
        writer.WriteStartArray();
        writer.WriteValue(value.x);
        writer.WriteValue(value.y);
        writer.WriteValue(value.z);
        writer.WriteEndArray();
    }

    public override Vector3 ReadJson(JsonReader reader, System.Type objectType, Vector3 existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        JArray jsonArray = JArray.Load(reader);
        if (jsonArray.Count == 3)
        {
            float x = jsonArray[0].Value<float>();
            float y = jsonArray[1].Value<float>();
            float z = jsonArray[2].Value<float>();
            return new Vector3(x, y, z);
        }
        else
        {
            return Vector3.zero; // Default vector if JSON doesn't contain valid vector data
        }
    }
}

public class NodeJSONConverter : JsonConverter<ModelNodeScriptableObject>
{
    
    public override void WriteJson(JsonWriter writer, ModelNodeScriptableObject value, JsonSerializer serializer)
    {
        writer.WriteStartObject();
        writer.WritePropertyName("Name");
        writer.WriteValue(value.Name);
        writer.WritePropertyName("Description");
        writer.WriteValue(value.Description);
        writer.WritePropertyName("Type");
        writer.WriteValue(value.Type);
        
        //Check if model node is a layer
        if (value is LayerScriptableObject layer)
        {
            writer.WritePropertyName("IsAnimated");
            writer.WriteValue(layer.IsAnimated);
            writer.WritePropertyName("Color");
            writer.WriteStartArray();
            writer.WriteValue(layer.Color.r);
            writer.WriteValue(layer.Color.g);
            writer.WriteValue(layer.Color.b);
            writer.WriteValue(layer.Color.a);
            writer.WriteEndArray();
            writer.WritePropertyName("Metallic");
            writer.WriteValue(layer.Metallic);
            writer.WritePropertyName("Smoothness");
            writer.WriteValue(layer.Smoothness);
            writer.WritePropertyName("BaseOpacity");
            writer.WriteValue(layer.BaseOpacity);
            writer.WritePropertyName("MeshReferenceGUID");
            writer.WriteValue(layer.MeshReference.AssetGUID);
        }
        
        writer.WriteEndObject();
    }

    public override ModelNodeScriptableObject ReadJson(JsonReader reader, System.Type objectType, ModelNodeScriptableObject existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        JObject jsonObject = JObject.Load(reader);
        // Check the "Type" property to determine the type of object to create
        int typeInt = jsonObject.GetValue("Type")?.Value<int>() ?? (int)NodeType.Empty;
        NodeType type = (NodeType)typeInt;
        ModelNodeScriptableObject node;
        
        switch (type)
        {
            case NodeType.Mesh:
            case NodeType.Displacement:
            case NodeType.Flow:
            case NodeType.Turbulence:
                node = new LayerScriptableObject();
                break;
            default:
                // If the type is not recognized or is empty, create a default ModelNodeScriptableObject
                node = new ModelNodeScriptableObject();
                break;
        }
        
        // Safely access and assign values using null-conditional operator
        node.Name = jsonObject.GetValue("Name")?.Value<string>() ?? "";
        node.Description = jsonObject.GetValue("Description")?.Value<string>() ?? "";
        if (node is LayerScriptableObject layer)
        {
            layer.IsAnimated = jsonObject.GetValue("IsAnimated")?.Value<bool>() ?? false;
            JArray colorArray = jsonObject.GetValue("Color") as JArray;
            if (colorArray != null && colorArray.Count == 4)
            {
                float r = colorArray[0].Value<float>();
                float g = colorArray[1].Value<float>();
                float b = colorArray[2].Value<float>();
                float a = colorArray[3].Value<float>();
                layer.Color = new Color(r, g, b, a);
            }
            else
            {
                layer.Color = Color.white; // Default color if JSON doesn't contain valid color data
            }
            layer.Metallic = jsonObject.GetValue("Metallic")?.Value<float>() ?? 0.89f;
            layer.Smoothness = jsonObject.GetValue("Smoothness")?.Value<float>() ?? 0.5f;
            layer.BaseOpacity = jsonObject.GetValue("BaseOpacity")?.Value<float>() ?? 1f;
            string meshReferenceGuid = jsonObject.GetValue("MeshReferenceGUID")?.Value<string>();
            if (!string.IsNullOrEmpty(meshReferenceGuid))
            {
                layer.MeshReference = new AssetReferenceT<Mesh>(meshReferenceGuid);
            }
        }
        
        return node;
    }
}


// public class LayerJSONConverter : JsonConverter<LayerScriptableObject>
// {
//     
//     public override void WriteJson(JsonWriter writer, LayerScriptableObject value, JsonSerializer serializer)
//     {
//         writer.WriteStartObject();
//         writer.WritePropertyName("Name");
//         writer.WriteValue(value.Name);
//         writer.WritePropertyName("Description");
//         writer.WriteValue(value.Description);
//         writer.WritePropertyName("IsAnimated");
//         writer.WriteValue(value.IsAnimated);
//         writer.WritePropertyName("Color");
//         writer.WriteStartArray();
//         writer.WriteValue(value.Color.r);
//         writer.WriteValue(value.Color.g);
//         writer.WriteValue(value.Color.b);
//         writer.WriteValue(value.Color.a);
//         writer.WriteEndArray();
//         writer.WritePropertyName("Metallic");
//         writer.WriteValue(value.Metallic);
//         writer.WritePropertyName("Smoothness");
//         writer.WriteValue(value.Smoothness);
//         writer.WritePropertyName("BaseOpacity");
//         writer.WriteValue(value.BaseOpacity);
//         writer.WritePropertyName("MeshReferenceGUID");
//         writer.WriteValue(value.MeshReference.AssetGUID);
//         writer.WriteEndObject();
//     }
//
//     public override LayerScriptableObject ReadJson(JsonReader reader, System.Type objectType, LayerScriptableObject existingValue, bool hasExistingValue, JsonSerializer serializer)
//     {
//         JObject jsonObject = JObject.Load(reader);
//         LayerScriptableObject layer = ScriptableObject.CreateInstance<LayerScriptableObject>();
//         // Safely access and assign values using null-conditional operator
//         layer.Name = jsonObject.GetValue("Name")?.Value<string>() ?? "";
//         layer.Description = jsonObject.GetValue("Description")?.Value<string>() ?? "";
//         layer.IsAnimated = jsonObject.GetValue("IsAnimated")?.Value<bool>() ?? false;
//         JArray colorArray = jsonObject.GetValue("Color") as JArray;
//         if (colorArray != null && colorArray.Count == 4)
//         {
//             float r = colorArray[0].Value<float>();
//             float g = colorArray[1].Value<float>();
//             float b = colorArray[2].Value<float>();
//             float a = colorArray[3].Value<float>();
//             layer.Color = new Color(r, g, b, a);
//         }
//         else
//         {
//             layer.Color = Color.white; // Default color if JSON doesn't contain valid color data
//         }
//         layer.Metallic = jsonObject.GetValue("Metallic")?.Value<float>() ?? 0.89f;
//         layer.Smoothness = jsonObject.GetValue("Smoothness")?.Value<float>() ?? 0.5f;
//         layer.BaseOpacity = jsonObject.GetValue("BaseOpacity")?.Value<float>() ?? 1f;
//         string meshReferenceGuid = jsonObject.GetValue("MeshReferenceGUID")?.Value<string>();
//         if (!string.IsNullOrEmpty(meshReferenceGuid))
//         {
//             layer.MeshReference = new AssetReferenceT<Mesh>(meshReferenceGuid);
//         }
//         return layer;
//     }
// }

public class ModelJSONConverter : JsonConverter<ModelScriptableObject>
{
    
    public override void WriteJson(JsonWriter writer, ModelScriptableObject value, JsonSerializer serializer)
    {
        value.OnBeforeSerialize();
        writer.WriteStartObject();
        writer.WritePropertyName("Name");
        writer.WriteValue(value.Name);
        writer.WritePropertyName("Description");
        writer.WriteValue(value.Description);
        writer.WritePropertyName("serializedNodes");
        serializer.Serialize(writer, value.serializedNodes);
        writer.WritePropertyName("textures");
        serializer.Serialize(writer, value.VolumetricTextures);
        writer.WritePropertyName("annotations");
        serializer.Serialize(writer, value.annotations);
        writer.WriteEndObject();
    }
    
    public override ModelScriptableObject ReadJson(JsonReader reader, System.Type objectType, ModelScriptableObject existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        JObject jsonObject = JObject.Load(reader);
        ModelScriptableObject model = ScriptableObject.CreateInstance<ModelScriptableObject>();
        model.Name = jsonObject.GetValue("Name")?.Value<string>() ?? "";
        model.Description = jsonObject.GetValue("Description")?.Value<string>() ?? "";
        model.VolumetricTextures = jsonObject.GetValue("textures")?.ToObject<List<VolumetricTexture>>() ?? new List<VolumetricTexture>();
        model.serializedNodes = jsonObject.GetValue("serializedNodes")?.ToObject<List<ModelScriptableObject.SerializableNode>>() ?? new List<ModelScriptableObject.SerializableNode>();
        model.annotations = jsonObject.GetValue("annotations")?.ToObject<List<AnnotationData>>() ?? new List<AnnotationData>();
        model.OnAfterDeserialize();
        return model;
    }
    
}


using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;


[Serializable]
public class LocalTransformTransition: INetworkSerializable
{
    public bool updateLocalPosition = true;
    public bool updateLocalRotation = true;
    public bool updateLocalScale= true;
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref updateLocalPosition);
        serializer.SerializeValue(ref updateLocalRotation);
        serializer.SerializeValue(ref updateLocalScale);
    }
}

[Serializable]
public class NodeStateTransition: INetworkSerializable
{
    public bool updateLocalTransform = true;
    public LocalTransformTransition transformTransition = new LocalTransformTransition();
    public bool updateTooltip = true;
    
    public virtual void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref updateLocalTransform);
        serializer.SerializeValue(ref transformTransition);
        serializer.SerializeValue(ref updateTooltip);
    }
}

[Serializable]
public class LayerStateTransition: NodeStateTransition
{
    public bool updateHidden=true;
    public bool updateOutline=true;
    public bool updateFaded=true;
    
    public override void NetworkSerialize<T>(BufferSerializer<T> serializer)
    {
        base.NetworkSerialize(serializer);
        serializer.SerializeValue(ref updateHidden);
        serializer.SerializeValue(ref updateOutline);
        serializer.SerializeValue(ref updateFaded);
    }
}

[Serializable]
public class AnimationStateTransition: INetworkSerializable
{
    
    public bool updateCurrentTime = true;
    public bool updateIsPlaying = true;
    public bool updateAnimationSpeed = true;
    public bool updateAnimationMode = true;

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref updateCurrentTime);
        serializer.SerializeValue(ref updateIsPlaying);
        serializer.SerializeValue(ref updateAnimationSpeed);
        serializer.SerializeValue(ref updateAnimationMode);
    }
}

[Serializable]
public class ClippingPlaneStateTransition: INetworkSerializable
{
    public bool updateIsActive = true;
    public bool updateVolumetricTextureIdx = true;
    public bool updateType = true;
    public bool updateTransform = true;
    public LocalTransformTransition transformTransition = new LocalTransformTransition();
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref updateIsActive);
        serializer.SerializeValue(ref updateVolumetricTextureIdx);
        serializer.SerializeValue(ref updateType);
        serializer.SerializeValue(ref updateTransform);
        serializer.SerializeValue(ref transformTransition);
    }
}

[Serializable]
public class UIStateTransition: INetworkSerializable
{
    public bool updateQuickButtons =true;
    public bool updateAnimationMenuActive =true;
    public bool updateSideMenuActive=true;
    public bool updateCurrentTab=true;
    public bool updateTabsActive=true;
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref updateQuickButtons);
        serializer.SerializeValue(ref updateAnimationMenuActive);
        serializer.SerializeValue(ref updateSideMenuActive);
        serializer.SerializeValue(ref updateCurrentTab);
        serializer.SerializeValue(ref updateTabsActive);
    }
}


[Serializable]
public class ModelInstanceStateTransition : INetworkSerializable
{

    public bool updateModelTransform = true;
    public LocalTransformTransition modelTransformTransition = new LocalTransformTransition();

    public bool updateLayerIdx = true;
    public bool updateLayerStates = true;
    public bool updateInterfaceState = true;
    public bool updateAnimationState = true;
    public bool updateClippingPlaneState = true;

    public bool updateBoundsActive = true;
    public bool updateFreeMovementActive = true;
    public bool updateAutoRotation = true;
    public bool updateRotationSpeed = true;

    public Dictionary<Guid, NodeStateTransition> nodeStateUpdates = new Dictionary<Guid, NodeStateTransition>();
    public AnimationStateTransition animationStateTransition = new AnimationStateTransition();
    public ClippingPlaneStateTransition clippingPlaneStateTransition = new ClippingPlaneStateTransition();
    public UIStateTransition uiStateTransition = new UIStateTransition();


    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        // Serialize basic fields
        serializer.SerializeValue(ref updateModelTransform);
        serializer.SerializeValue(ref modelTransformTransition);
        serializer.SerializeValue(ref updateLayerIdx);
        serializer.SerializeValue(ref updateLayerStates);
        serializer.SerializeValue(ref updateInterfaceState);
        serializer.SerializeValue(ref updateAnimationState);
        serializer.SerializeValue(ref updateClippingPlaneState);
        serializer.SerializeValue(ref updateBoundsActive);
        serializer.SerializeValue(ref updateFreeMovementActive);
        serializer.SerializeValue(ref updateAutoRotation);
        serializer.SerializeValue(ref updateRotationSpeed);

        // Serialize nodeStateUpdates dictionary
        if (serializer.IsReader)
        {
            // Deserialize the number of elements
            int nodeStateUpdateCount = 0;
            serializer.SerializeValue(ref nodeStateUpdateCount);

            nodeStateUpdates.Clear();
            for (int i = 0; i < nodeStateUpdateCount; i++)
            {
                string guidString = string.Empty;
                serializer.SerializeValue(ref guidString);
                Guid guid = Guid.Parse(guidString);

                NodeStateTransition nodeStateTransition = new NodeStateTransition();
                serializer.SerializeValue(ref nodeStateTransition);

                nodeStateUpdates[guid] = nodeStateTransition;
            }
        }
        else
        {
            // Serialize the number of elements
            int nodeStateUpdateCount = nodeStateUpdates.Count;
            serializer.SerializeValue(ref nodeStateUpdateCount);

            foreach (var kvp in nodeStateUpdates)
            {
                string guidString = kvp.Key.ToString();
                serializer.SerializeValue(ref guidString);

                NodeStateTransition nodeStateTransition = kvp.Value;
                serializer.SerializeValue(ref nodeStateTransition);
            }
        }

        // Serialize sub-states
        serializer.SerializeValue(ref animationStateTransition);
        serializer.SerializeValue(ref clippingPlaneStateTransition);
        serializer.SerializeValue(ref uiStateTransition);
    }
}




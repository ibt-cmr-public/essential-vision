using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.AddressableAssets;
using FileMode = System.IO.FileMode;
using Task = System.Threading.Tasks.Task;


[CreateAssetMenu(fileName = "New Layer", menuName = "EssentialVision/Node/Layer")]
[JsonConverter(typeof(NodeJSONConverter))]
[Serializable]
public class LayerScriptableObject : ModelNodeScriptableObject
{
    public Action OnNodeMaterialChanged;
    public Action OnModelMeshChanged;
    
    [Header("Node Type")]
    [SerializeField] private NodeType nodeType = NodeType.Mesh;
    public override NodeType Type => nodeType;

    [Header("Mesh")]
    [SerializeField] private Mesh mesh;
    [SerializeField] private string meshFilePath;
    [SerializeField] private AssetReferenceT<Mesh> meshReference;
    
    [Header("Interaction")]
    public bool IsInteractable = true;

    [Header("Animation")]
    public bool IsAnimated = false;

    [Header("Material Properties")]
    [SerializeField] private Color color = Color.white;
    public Color Color
    {
        get { return color; }
        set
        {
            color = value;
            OnNodeMaterialChanged?.Invoke();
        }
    }
    
    [SerializeField] private Texture2D mainTexture;
    public Texture2D MainTexture
    {
        get { return mainTexture; }
        set
        {
            mainTexture = value;
            OnNodeMaterialChanged?.Invoke();
        }
    }

    [SerializeField] private float metallic = 0.89f;
    public float Metallic
    {
        get { return metallic; }
        set
        {
            metallic = value;
            OnNodeMaterialChanged?.Invoke();
        }
    }
    
    [SerializeField] private float smoothness = 0.5f;
    public float Smoothness
    {
        get { return smoothness; }
        set
        {
            smoothness = value;
            OnNodeMaterialChanged?.Invoke();
        }
    }
    
    [SerializeField] private float baseOpacity = 1f;
    public float BaseOpacity
    {
        get { return baseOpacity; }
        set
        {
            baseOpacity = value;
            OnNodeMaterialChanged?.Invoke();
        }
    }
    

    //Materials
    public Material OpaqueMaterial
    {
        get
        {
            Material material = new Material(Shader.Find(ApplicationDefaults.DefaultShaders[nodeType]));
            material.color = Color;
            if (MainTexture != null)
            {
                material.mainTexture = MainTexture;
            }
            material.SetFloat("_Metallic", Metallic);
            material.SetFloat("_Glossiness", Smoothness);
            material.SetFloat("_Fade", BaseOpacity);
            if (BaseOpacity < 1)
            {
                material.SetFloat("_Mode", 2);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            }
            return material;
        }
    }
    public Material TransparentMaterial(float opacity)
    {
        Material material = OpaqueMaterial;
        if (opacity < 1)
        {
            material.SetFloat("_Mode", 2);
            material.SetFloat("_Fade", BaseOpacity * opacity);
            material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            material.SetInt("_ZWrite", 0);
            material.DisableKeyword("_ALPHATEST_ON");
            material.EnableKeyword("_ALPHABLEND_ON");
            material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        }
        return material;
    }
    
    //Mesh
    public AssetReferenceT<Mesh> MeshReference
    {
        get
        {
            return meshReference;
        }
        set
        {
            if (meshReference != value)
            {
                meshReference = value;
                mesh = null;
                meshFilePath = null;
                OnModelMeshChanged?.Invoke();
            }
        }
    }
    public string MeshFilePath
    {
        get
        {
            return meshFilePath;
        }
        set
        {
            if (meshFilePath != value)
            {
                meshFilePath = value;
                meshReference = null;
                mesh = null;
                OnModelMeshChanged?.Invoke();
            }
        }
    }
    public async Task<Mesh> GetMesh()
    {
        if (mesh == null)
        {
            try
            {
                if (MeshReference != null)
                {
                    mesh = await LoadMeshFromAddressable(MeshReference);
                }
                else if (MeshFilePath != null)
                {
                    mesh = await LoadMeshFromFile(MeshFilePath);
                }
                else
                {
                    throw new Exception("Could not load mesh. No mesh reference or file path provided.");
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error loading mesh from Addressable: {ex.Message}");

                if (MeshFilePath != null)
                {
                    mesh = await LoadMeshFromFile(MeshFilePath);
                }
                else
                {
                    throw; // Re-throw the exception if no file path is provided
                }
            }
        }
        return mesh;
    }
    public void SetMesh(Mesh newMesh)
    {
        if (mesh != newMesh)
        {
            mesh = newMesh;
            meshReference = null;
            meshFilePath = null;
            OnModelMeshChanged?.Invoke();
        }
    }
    public async Task SaveMeshToFile(string filePath, bool updateFilePath = true)
    {
        Mesh meshToWrite = await GetMesh();
        SerializableMeshInfo meshInfo = new SerializableMeshInfo(meshToWrite);
        
        // Create the directory if it does not exist
        string directoryPath = Path.GetDirectoryName(filePath);
        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }
        
        //Save as binary file
        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, meshInfo);
        }

        if (updateFilePath)
        {
            meshFilePath = filePath;
        }
    }
    public async Task<Mesh> LoadMeshFromFile(string filePath)
    {
        SerializableMeshInfo meshInfo;
        using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            meshInfo = (SerializableMeshInfo)binaryFormatter.Deserialize(fileStream);
        }
        Mesh mesh = meshInfo.GetMesh();
        return mesh;
    }
    private async Task<Mesh> LoadMeshFromAddressable(AssetReferenceT<Mesh> assetReference)
    {
        try
        {
            var meshes = await Addressables.LoadAssetsAsync<Mesh>(assetReference, null).Task;

            if (meshes != null && meshes.Count > 0)
            {
                // Assuming you only expect one mesh, you can return the first one
                return meshes[0];
            }
            else
            {
                throw new System.Exception("Failed to load mesh.");
            }
        }
        catch (System.Exception ex)
        {
            // Catch and handle any exceptions thrown during the loading process
            throw new System.Exception("Failed to load mesh.", ex);
        }
    }

    
    //Serialization and file saving
    public override async Task<byte[]> ToBinary()
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            using (BinaryWriter writer = new BinaryWriter(memoryStream))
            {
                writer.Write(Name);
                writer.Write(Description);
                writer.Write((int)Type);

                // Serialize Mesh
                bool includeMesh = true;
                if (includeMesh)
                {
                    Mesh mesh = await GetMesh();
                    SerializableMeshInfo meshInfo = new SerializableMeshInfo(mesh);
                    byte[] meshBytes = meshInfo.ToBinary();
                    writer.Write(meshBytes.Length);
                    writer.Write(meshBytes);
                }
                else
                {
                    writer.Write(0);
                }
                
                writer.Write(IsAnimated);

                // Serialize Color
                writer.Write(Color.r);
                writer.Write(Color.g);
                writer.Write(Color.b);
                writer.Write(Color.a);

                // Serialize MainTexture
                if (MainTexture != null)
                {
                    byte[] textureBytes = MainTexture.EncodeToPNG();
                    writer.Write(textureBytes.Length);
                    writer.Write(textureBytes);
                }
                else
                {
                    writer.Write(0);
                }

                // Serialize Metallic, Smoothness, BaseOpacity
                writer.Write(Metallic);
                writer.Write(Smoothness);
                writer.Write(BaseOpacity);
            }

            return memoryStream.ToArray();
        }
    }
    public static LayerScriptableObject FromBinary(byte[] data)
    {
        LayerScriptableObject layer = ScriptableObject.CreateInstance<LayerScriptableObject>();

        using (MemoryStream memoryStream = new MemoryStream(data))
        {
            using (BinaryReader reader = new BinaryReader(memoryStream))
            {
                layer.Name = reader.ReadString();
                layer.Description = reader.ReadString();
                layer.nodeType = (NodeType)reader.ReadInt32();
                
                // Deserialize Mesh
                int meshLength = reader.ReadInt32();
                if (meshLength > 0)
                {
                    byte[] meshBytes = reader.ReadBytes(meshLength);
                    SerializableMeshInfo meshInfo = SerializableMeshInfo.FromBinary(meshBytes);
                    layer.mesh = meshInfo.GetMesh();
                }

                // Deserialize IsAnimated
                layer.IsAnimated = reader.ReadBoolean();


                // Deserialize Color
                float r = reader.ReadSingle();
                float g = reader.ReadSingle();
                float b = reader.ReadSingle();
                float a = reader.ReadSingle();
                layer.Color = new Color(r, g, b, a);

                // Deserialize MainTexture
                int textureLength = reader.ReadInt32();
                if (textureLength > 0)
                {
                    byte[] textureBytes = reader.ReadBytes(textureLength);
                    Texture2D texture = new Texture2D(2, 2);
                    texture.LoadImage(textureBytes);
                    layer.MainTexture = texture;
                }

                // Deserialize Metallic, Smoothness, BaseOpacity
                layer.Metallic = reader.ReadSingle();
                layer.Smoothness = reader.ReadSingle();
                layer.BaseOpacity = reader.ReadSingle();
            }
        }

        return layer;
    }
    public async void ToBinaryFile(string filePath, bool overwriteFile = true)
    {
        //Create directory
        if (File.Exists(filePath))
        {
            if (overwriteFile)
            {
                Directory.Delete(filePath, true);
            }
            else
            {
                Debug.LogError("File already exists. Please choose a different directory.");
            }
        }

        //Write layer to file
        byte[] data = await ToBinary();
        File.WriteAllBytes(filePath, data);
    }
    public static LayerScriptableObject FromBinaryFile(string filePath)
    {
        byte[] data = File.ReadAllBytes(filePath);
        LayerScriptableObject layer = FromBinary(data);
        return layer;
    }
    public async void ToFile(string directoryPath, bool overwriteFile = true)
    {
        //Create directory
        if (Directory.Exists(directoryPath))
        {
            if (overwriteFile)
            {
                Directory.Delete(directoryPath, true);
                Directory.CreateDirectory(directoryPath);
            }
            else
            {
                Debug.LogError("File already exists. Please choose a different directory.");
            }
        }
        else 
        {
            Directory.CreateDirectory(directoryPath);
        }

        //Write mesh to file
        string filePath = directoryPath + "/mesh.bin";
        await SaveMeshToFile(filePath);
        
        //Write material texture to file
        if (MainTexture != null)
        {
            string texturePath = directoryPath + "/material_texture.png";
            File.WriteAllBytes(texturePath, MainTexture.EncodeToPNG());
        }
        
        //Write info to file
        string infoFilePath = directoryPath + "/info.json";
        string json = JsonConvert.SerializeObject(this, Formatting.Indented);
        using (StreamWriter writer = new StreamWriter(infoFilePath))
        {
            await writer.WriteAsync(json);
        }
    }
    public static LayerScriptableObject FromFile(string directoryPath)
    {
        string json;
        using (StreamReader reader = new StreamReader(directoryPath + "/info.json"))
        {
            json = reader.ReadToEnd();
        }
        LayerScriptableObject layer = JsonConvert.DeserializeObject<LayerScriptableObject>(json);
        
        //Set mesh file path
        string meshPath = directoryPath + "/mesh.bin";
        if (File.Exists(meshPath))
        {
            layer.meshFilePath = directoryPath + "/mesh.bin";
        }
        
        //Set material texture
        string texturePath = directoryPath + "/material_texture.png";
        if (File.Exists(texturePath))
        {
            byte[] textureData = File.ReadAllBytes(texturePath);
            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(textureData);
            layer.MainTexture = texture;
        }
        
        return layer;
    }

}

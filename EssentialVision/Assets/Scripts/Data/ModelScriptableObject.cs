using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TreeView;
using UnityEngine;

[CreateAssetMenu(fileName = "New Model", menuName = "EssentialVision/Model")]
[JsonConverter(typeof(ModelJSONConverter))]
public class ModelScriptableObject : ScriptableObject,  ISerializationCallbackReceiver
{
    public string Name;
    public string Description;
    public Guid guid = Guid.NewGuid();
    public string guidString = "";
    [JsonIgnore]
    public Texture2D Icon;
    [JsonIgnore]
    public TreeNode<ModelNodeScriptableObject> ModelTree;
    public List<VolumetricTexture> VolumetricTextures = new List<VolumetricTexture>();
    public List<AnnotationData> annotations = new List<AnnotationData>();
    //Fields used for serialization
    public List<SerializableNode> serializedNodes = new List<SerializableNode>();
    
    //Versioning
    private const int CURRENT_VERSION = 1;
    private int version;


    public event Action OnNodeAdded;
    public event Action OnNodeRemoved;
    
    // Property to check if any layer in the model is animated
    public bool IsAnimated
    {
        get
        {
            foreach (TreeNode<ModelNodeScriptableObject> node in ModelTree)
            {
                if (node.Data is LayerScriptableObject { IsAnimated: true })
                {
                    return true;
                }
            }
            return false;
        }
    }

    // Property to get the Icon as a Sprite
    public Sprite IconSprite
    {
        get
        {
            if (Icon == null)
            {
                return null;
            }
            else
            {
                return Sprite.Create(Icon, new Rect(0, 0, Icon.width, Icon.height), new Vector2(Icon.width / 2, Icon.height / 2));
            }
        }
    }
    
    //Node manipulation
    public Guid AddNode(ModelNodeScriptableObject newNode, Guid parentGuid = default)
    {
        TreeNode<ModelNodeScriptableObject> newTreeNode = new TreeNode<ModelNodeScriptableObject>(newNode);
        
        //Find parent
        TreeNode<ModelNodeScriptableObject> parentNode = ModelTree;
        foreach (TreeNode<ModelNodeScriptableObject> node in ModelTree)
        {
            if (node.guid == parentGuid)
            {
                parentNode = node;
            }
        }
        parentNode.AddChild(new TreeNode<ModelNodeScriptableObject>(newNode));
        guid = Guid.NewGuid();
        OnNodeAdded?.Invoke();
        return newTreeNode.guid;
    }

    public ModelNodeScriptableObject RemoveNode(Guid guid)
    {
        foreach (TreeNode<ModelNodeScriptableObject> node in ModelTree)
        {
            if (node.guid == guid)
            {
                ModelNodeScriptableObject removedNode = node.Data;
                node.Parent.RemoveChild(node);
                guid = Guid.NewGuid();
                OnNodeRemoved?.Invoke();
                return removedNode;
            }
        }
        return null;
    }

    public ModelNodeScriptableObject GetNode(Guid nodeGuid)
    {
        foreach (TreeNode<ModelNodeScriptableObject> node in ModelTree)
        {
            if (node.guid == nodeGuid)
            {
                return node.Data;
            }
        }

        return null;
    }
    

    // Method to save the model data to a JSON file (currently not implemented)

    public async Task<byte[]> ToBinary()
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            using (BinaryWriter writer = new BinaryWriter(memoryStream))
            {
                OnBeforeSerialize();
                
                //Write version
                writer.Write(Name);
                writer.Write(Description);
                writer.Write(guidString);
                
                //Serialize nodes
                writer.Write(serializedNodes.Count);
                foreach (SerializableNode node in serializedNodes)
                {
                    byte[] nodeData = await node.ToBinary();
                    writer.Write(nodeData.Length);
                    writer.Write(nodeData);
                }
                
                //Serialize volumetric textures
                writer.Write(VolumetricTextures.Count);
                foreach (VolumetricTexture volumetricTexture in VolumetricTextures)
                {
                    byte[] textureData = volumetricTexture.ToBinary();
                    writer.Write(textureData.Length);
                    writer.Write(textureData);
                }
                
                //Serialize annotations
                writer.Write(annotations.Count);
                foreach (AnnotationData annotation in annotations)
                {
                    byte[] annotationData = annotation.ToBinary();
                    writer.Write(annotationData.Length);
                    writer.Write(annotationData);
                }
                
                // Serialize Icon
                if (Icon != null)
                {
                    byte[] iconBytes = Icon.EncodeToPNG();
                    writer.Write(iconBytes.Length);
                    writer.Write(iconBytes);
                }
                else
                {
                    writer.Write(0);
                }
            }
            return memoryStream.ToArray();
        }
    }


    public static ModelScriptableObject FromBinary(byte[] data)
    {
        ModelScriptableObject model = CreateInstance<ModelScriptableObject>();
        using (MemoryStream memoryStream = new MemoryStream(data))
        {
            using (BinaryReader reader = new BinaryReader(memoryStream))
            {
                model.Name = reader.ReadString();
                model.Description = reader.ReadString();
                model.guidString = reader.ReadString();
                
                //Deserialize nodes
                int numNodes = reader.ReadInt32();
                model.serializedNodes = new List<SerializableNode>();
                for (int i = 0; i < numNodes; i++)
                {
                    int nodeDataLength = reader.ReadInt32();
                    byte[] nodeData = reader.ReadBytes(nodeDataLength);
                    model.serializedNodes.Add(SerializableNode.FromBinary(nodeData));
                }
                
                //Deserialize volumetric textures
                int numTextures = reader.ReadInt32();
                model.VolumetricTextures = new List<VolumetricTexture>();
                for (int i = 0; i < numTextures; i++)
                {
                    int textureDataLength = reader.ReadInt32();
                    byte[] textureData = reader.ReadBytes(textureDataLength);
                    model.VolumetricTextures.Add(VolumetricTexture.FromBinary(textureData));
                }
                
                //Deserialize annotations
                int numAnnotations = reader.ReadInt32();
                model.annotations = new List<AnnotationData>();
                for (int i = 0; i < numAnnotations; i++)
                {
                    int annotationDataLength = reader.ReadInt32();
                    byte[] annotationData = reader.ReadBytes(annotationDataLength);
                    model.annotations.Add(AnnotationData.FromBinary(annotationData));
                }
                
                // Deserialize Icon
                int iconLength = reader.ReadInt32();
                if (iconLength > 0)
                {
                    byte[] iconData = reader.ReadBytes(iconLength);
                    Texture2D icon = new Texture2D(2, 2);
                    icon.LoadImage(iconData);
                    model.Icon = icon;
                }
            }
        }
        model.OnAfterDeserialize();
        return model;    
    }


    public async void ToBinaryFile(string filePath, bool overwriteFile = true)
    {
        //Create directory
        if (File.Exists(filePath))
        {
            if (overwriteFile)
            {
                Directory.Delete(filePath, true);
            }
            else
            {
                Debug.LogError("File already exists. Please choose a different directory.");
            }
        }

        //Write layer to file
        byte[] data = await ToBinary();
        File.WriteAllBytes(filePath, data);
    }
    
    public static ModelScriptableObject FromBinaryFile(string filePath)
    {
        byte[] data = File.ReadAllBytes(filePath);
        ModelScriptableObject model = FromBinary(data);
        return model;
    }

    
    public async Task ToFile(string directoryPath, bool overwriteFile = true)
    {
        //Create base folders
        if (Directory.Exists(directoryPath))
        {
            if (overwriteFile)
            {
                Directory.Delete(directoryPath, true);
                Directory.CreateDirectory(directoryPath);
            }
            else
            {
                Debug.LogError("File already exists. Please choose a different directory.");
            }
        }
        else
        {
            Directory.CreateDirectory(directoryPath);
        }
        Directory.CreateDirectory(directoryPath + "/VolumetricTextures");
        Directory.CreateDirectory(directoryPath + "/Meshes");
        
        
        //Save icon
        if (Icon != null)
        {
            string iconPath = directoryPath + "/icon.png";
            File.WriteAllBytes(iconPath, Icon.EncodeToPNG());
        }
        
        //Save all volumetric textures
        int i = 0;
        foreach (VolumetricTexture volumetricTexture in VolumetricTextures)
        {
            string filePath = directoryPath + "/VolumetricTextures/texture_" + i + ".bin";
            volumetricTexture.SaveTextureDataToFile(filePath);
            i++;
        }
        
        //Save all meshes
        foreach (TreeNode<ModelNodeScriptableObject> node in ModelTree)
        {
            if (node.Data is LayerScriptableObject layer)
            {
                string filePath = directoryPath + "/Meshes/" + layer.Name + "_mesh.bin";
                await layer.SaveMeshToFile(filePath);
            }
        }
        
        //Create model JSON
        string json = JsonConvert.SerializeObject(this, Formatting.Indented);
        using (StreamWriter writer = new StreamWriter(directoryPath + "/model.json"))
        {
            writer.Write(json);
        }
    }

    public static ModelScriptableObject FromFile(string directoryPath)
    {
        ModelScriptableObject model = ScriptableObject.CreateInstance<ModelScriptableObject>();
        //Read model
        string filepath = directoryPath + "/model.json";
        JsonSerializerSettings settings = new JsonSerializerSettings();
        settings.Converters.Add(new ModelJSONConverter());
        JsonSerializer serializer = JsonSerializer.Create(settings);
        using (StreamReader sr = new StreamReader(filepath))
        using (JsonReader reader = new JsonTextReader(sr))
        {
            model = serializer.Deserialize<ModelScriptableObject>(reader);
        }
        
        //Load icon
        string iconPath = directoryPath + "/icon.png";
        if (File.Exists(iconPath))
        {
            byte[] iconData = File.ReadAllBytes(iconPath);
            Texture2D icon = new Texture2D(2, 2);
            icon.LoadImage(iconData);
            model.Icon = icon;
        }
        
        //Load textures
        int i = 0;
        foreach (VolumetricTexture texture in model.VolumetricTextures)
        {
            string textureFile = directoryPath + "/VolumetricTextures/texture_" + i + ".bin";
            texture.LoadTextureFromFile(textureFile);
            i++;
        }
        
        //Set mesh file paths
        foreach (TreeNode<ModelNodeScriptableObject> node in model.ModelTree)
        {
            if (node.Data is LayerScriptableObject layer)
            {
                string meshPath = directoryPath + "/Meshes/" + layer.Name + "_mesh.bin";
                if (File.Exists(meshPath))
                {
                    layer.MeshFilePath = meshPath;
                }
                else
                {
                    Debug.LogWarning("Mesh file not found for layer: " + layer.Name);
                }
            }
        }

        return model;
    }

    //node class that we will use for serialization
    [Serializable]
    public struct SerializableNode
    {
        public ModelNodeScriptableObject data;
        public int parentIdx;
        public List<int> children;
        
        public async Task<byte[]> ToBinary()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(memoryStream))
                {
                    writer.Write(parentIdx);
                    writer.Write(children.Count);
                    foreach (int child in children)
                    {
                        writer.Write(child);
                    }
                    writer.Write((int)data.Type);
                    byte[] nodeData = await data.ToBinary();
                    writer.Write(nodeData.Length);
                    writer.Write(nodeData);
                }

                return memoryStream.ToArray();
            }
        }
        
        public static SerializableNode FromBinary(byte[] data)
        {
            SerializableNode node = new SerializableNode();
            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(memoryStream))
                {
                    node.parentIdx = reader.ReadInt32();
                    int numChildren = reader.ReadInt32();
                    node.children = new List<int>();
                    for (int i = 0; i < numChildren; i++)
                    {
                        node.children.Add(reader.ReadInt32());
                    }
                    NodeType nodeType = (NodeType)reader.ReadInt32();
                    int nodeDataLength = reader.ReadInt32();
                    byte[] nodeData = reader.ReadBytes(nodeDataLength);
                    switch (nodeType)
                    {
                        case NodeType.Empty:
                            node.data = ModelNodeScriptableObject.FromBinary(nodeData);
                            break;
                        default:
                            node.data = LayerScriptableObject.FromBinary(nodeData);
                            break;
                    }
                }
            }

            return node;
        }
    }


    public void OnBeforeSerialize()
    {
        //unity is about to read the serializedNodes field's contents. lets make sure
        //we write out the correct data into that field "just in time".
        serializedNodes = new List<SerializableNode>(Enumerable.Repeat(new SerializableNode(), NumNodes));
        if (ModelTree != null)
        {
            AddNodeToSerializedNodes(ModelTree, 0, -1);
        }
        guidString = guid.ToString();
    }

    private int NumNodes
    {
        get
        {
            return ModelTree != null ? ModelTree.Size : 0;
        }
    }


    void AddNodeToSerializedNodes(TreeNode<ModelNodeScriptableObject> node, int idx, int parentIdx)
    {
        var serializedNode = new SerializableNode()
        {
            data = node.Data,
            parentIdx = parentIdx,
            children = new List<int>()
        };

        List<int> tempChildren = new List<int>();
        int i = 1;
        foreach (var child in node.Children)
        {
            int childIdx = idx + i;
            tempChildren.Add(childIdx);
            AddNodeToSerializedNodes(child, childIdx, idx);
            i+= child.Size;
        }

        serializedNode.children = tempChildren; // Assign the temporary list to serializedNode.children
        serializedNodes[idx] = serializedNode;
    }

    public void OnAfterDeserialize()
    {
        //Unity has just written new data into the serializedNodes field.
        //let's populate our actual runtime data with those new values.

        if (serializedNodes.Count > 0)
            ModelTree = ReadNodeFromSerializedNodes(0);
        else
            ModelTree = null;
        
        if (guidString != guid.ToString())
        {
            guid = new Guid(guidString);
        }
    }

    TreeNode<ModelNodeScriptableObject> ReadNodeFromSerializedNodes(int index)
    {
        var serializedNode = serializedNodes[index];
        Debug.Log(serializedNodes[0].children);
        TreeNode<ModelNodeScriptableObject> node = new TreeNode<ModelNodeScriptableObject>(serializedNode.data);
        foreach (int i in serializedNode.children)
            node.AddChild(ReadNodeFromSerializedNodes(i));
        return node;
    }
}
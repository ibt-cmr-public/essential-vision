using MixedReality.Toolkit.SpatialManipulation;
using UnityEngine;


public class MainMenu : MonoBehaviour
{
    [SerializeField]
    public SolverHandler solverHandler;
    [SerializeField]
    public Solver followSolver;
    [SerializeField]
    public Solver handSolver;
    [SerializeField]
    public GameObject canvas;

    [SerializeField] private bool hideMenuOnStart = true;
    private bool inFocus = true;

    public bool InFocus
    {
        get
        {
            return inFocus;
        }
        set
        {
            if (inFocus != value)
            {
                inFocus = value;
                if (inFocus)
                {
                    followSolver.enabled = true;
                    solverHandler.TrackedTargetType = TrackedObjectType.Head;
                    handSolver.enabled = false;
                    canvas.SetActive(true);
                }
                else
                {
                    followSolver.enabled = false;
                    solverHandler.TrackedTargetType = TrackedObjectType.HandJoint;
                    handSolver.enabled = true;
                    canvas.SetActive(false);
                }
            }
        }
    } 

    // Start is called before the first frame update
    void Start()
    {
        if (hideMenuOnStart)
        {
            HideMenu();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HideMenu()
    {
        InFocus = false;
    }

    public void ShowSharedExperience()
    {
        InFocus = false;
    }

    public void ShowOptions()
    {
        InFocus = false;
    }

    public void ShowLibrary()
    {
        InFocus = false;
    }
}

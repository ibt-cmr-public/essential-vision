using System.Collections.Generic;

public static class ApplicationDefaults
{
    private const string DefaultMaterialPath = "Assets/Graphics/Materials/Layers/";
    
    public static readonly Dictionary<NodeType, string> DefaultShaders = new Dictionary<NodeType, string>
    {
        { NodeType.Mesh, "Graphics Tools/Standard" },
        { NodeType.Flow,  "Holo/DataFlow" },
        { NodeType.Displacement,  "Holo/DataDisplacement" },
        { NodeType.Turbulence, "Holo/DataTurbulence" }
    };

    public static readonly Dictionary<NodeType, string> DefaultOpaqueMaterials = new Dictionary<NodeType, string>
    {
        { NodeType.Mesh, DefaultMaterialPath + "DefaultModelMaterial.mat" },
        { NodeType.Flow, DefaultMaterialPath + "DefaultFlowMaterial.mat" },
        { NodeType.Displacement, DefaultMaterialPath + "DefaultDisplacementMaterial.mat" },
        { NodeType.Turbulence, DefaultMaterialPath + "DefaultTurbulenceMaterial.mat" }
    };

    public static readonly Dictionary<NodeType, string> DefaultTransparentMaterials = new Dictionary<NodeType, string>
    {
        { NodeType.Mesh, DefaultMaterialPath + "DefaultModelTransparentMaterial.mat" },
        { NodeType.Flow, DefaultMaterialPath + "DefaultFlowTransparentMaterial.mat" },
        { NodeType.Displacement, DefaultMaterialPath + "DefaultDisplacementTransparentMaterial.mat" },
        { NodeType.Turbulence, DefaultMaterialPath + "DefaultTurbulenceTransparentMaterial.mat" }
    };

    public static string DefaultOutlineMaterial = "Assets/Graphics/Materials/DefaultOutlineMaterial.mat";

    public static string modelInfoAssetGroupName = "Model Info";
}
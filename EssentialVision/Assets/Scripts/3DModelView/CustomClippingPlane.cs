using Microsoft.MixedReality.GraphicsTools;
using UnityEngine;
using UnityEngine.Rendering;

public class CustomClippingPlane : ClippingPlane
{
    [Header("Volumetric Slicing")]
    //The volumetric texture used for slicing
    [SerializeField] private VolumetricTexture volumetricTexture;
    //Renderer for the actual quad or plane object
    [SerializeField] private MeshRenderer planeRenderer;
    //Transform that volumetric cube is attached to (defaults to parent transform)
    [SerializeField] private Transform volumeParentTransform;
    
    //The slice material that renders the volumetric slice
    private Material sliceMaterialInstance
    {
        get
        {
            return planeRenderer == null ? null: planeRenderer.material;
        }
    }

    public VolumetricTexture VolumetricTexture
    {
        get
        {
            return volumetricTexture;
        }
        set
        {
            if (sliceMaterialInstance == null) return;
            LocalKeyword display_texture = new LocalKeyword(sliceMaterialInstance.shader, "DISPLAY_TEXTURE");
            if (value == null)
            {
                sliceMaterialInstance.SetKeyword(display_texture, false);
            }
            else
            {
                sliceMaterialInstance.SetKeyword(display_texture, true);
                sliceMaterialInstance.SetTexture("_MainTex", value.texture);
                sliceMaterialInstance.SetVector("_LocalPosition", value.localPosition);
                sliceMaterialInstance.SetVector("_LocalRotation", value.localRotation);
                sliceMaterialInstance.SetVector("_LocalScale", value.localScale);
            }
            volumetricTexture = value;
        }
    }
    // Start is called before the first frame update

    private void Start()
    {
        if (planeRenderer == null)
        {
            //Try to get renderer
            planeRenderer = GetComponentInChildren<MeshRenderer>();
        }

        if (planeRenderer == null)
        {
            Debug.LogError("No plane renderer found for clipping plane.");
        }

        if (volumeParentTransform == null)
        {
            volumeParentTransform = transform.parent;
        }

        if (VolumetricTexture != null && sliceMaterialInstance != null)
        {
            LocalKeyword display_texture = new LocalKeyword(sliceMaterialInstance.shader, "DISPLAY_TEXTURE");
            sliceMaterialInstance.SetKeyword(display_texture, true);
            sliceMaterialInstance.SetTexture("_MainTex", VolumetricTexture.texture);
            sliceMaterialInstance.SetVector("_LocalPosition", VolumetricTexture.localPosition);
            sliceMaterialInstance.SetVector("_LocalRotation", VolumetricTexture.localRotation);
            sliceMaterialInstance.SetVector("_LocalScale", VolumetricTexture.localScale);
        }
    }

    private void Update()
    {
        if (volumetricTexture != null && sliceMaterialInstance != null)
        {
            Matrix4x4 parentMatrix = volumeParentTransform != null
                ? volumeParentTransform.worldToLocalMatrix
                : Matrix4x4.identity;
            sliceMaterialInstance.SetMatrix("_ParentMatrix", parentMatrix);
        }
    }
    public void InitializeOnStart()
    {
        //We need to do this because of a MRTK bug I think 
        Initialize();
    }
    
}
using UnityEngine;

public class MoveWithScale : MonoBehaviour
{

    [SerializeField]
    public Transform target;
    [SerializeField]
    public float targetSize = 0.3f;
    [SerializeField]
    public float boundsDistance = 0f;
    [SerializeField]
    public float minTargetDistance = 0f;

    // Update is called once per frame
    void Update()
    {
        //Renderer[] allRenderers = target.GetComponentsInChildren<Renderer>();
        //Bounds targetBounds = CalculateEncapsulatedBounds(allRenderers);
        //Vector3 relativeDirection = (target.position - transform.position).normalized;

        //Ray ray = new Ray(transform.position, relativeDirection);
        //float intersectionDistance;
        //targetBounds.IntersectRay(ray, out intersectionDistance);
        //Vector3 intersectionPoint = ray.GetPoint(intersectionDistance);

        //transform.position = intersectionPoint - distanceBounds * relativeDirection;

        if (target == null)
        {
            return;
        }
        float relativeScale = target.localScale.x;
        float currentDistance = Mathf.Max(minTargetDistance, boundsDistance + relativeScale * targetSize);
        Vector3 relativeDirection = (transform.position - target.position).normalized;
        transform.position = target.position + currentDistance * relativeDirection;
    }
}


using MixedReality.Toolkit;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public float rotationSpeed = 10f;
    [SerializeField]
    public Axis axis = Axis.Y;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rotationVector = Vector3.zero;
        switch (axis)
        {
            case Axis.X:
                rotationVector = Vector3.right;
                break;
            case Axis.Y:
                rotationVector = Vector3.up;
                break;
            case Axis.Z:
                rotationVector = Vector3.forward;
                break;
        }
        transform.Rotate(rotationVector, rotationSpeed * Time.deltaTime, Space.Self);
    }
}

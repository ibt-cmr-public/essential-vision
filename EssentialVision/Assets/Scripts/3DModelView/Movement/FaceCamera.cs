using UnityEngine;

public class FaceCamera : MonoBehaviour
{

    [SerializeField]
    public bool ignoreUpwardsRotation;

    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        if (ignoreUpwardsRotation)
        {

            var directionToTarget =  transform.position - _camera.transform.position;
            directionToTarget.y = 0f;
            transform.rotation = Quaternion.LookRotation(directionToTarget, Vector3.up);
        }
        else
        {
            transform.LookAt(_camera.transform);
        }
    }
}
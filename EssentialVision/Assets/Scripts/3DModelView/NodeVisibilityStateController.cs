using System;

public class NodeVisibilityStateController
{
    public event Action NodeVisibilityChanged;
    //public Dictionary<string, LayerInstance> instantiatedLayers;
    private NodeController nodeController;

    //Selection settings
    private bool outlineSelected = true;
    private bool showTooltipSelected = true;
    private bool outlineHovered = true;
    private bool showTooltipHovered = true;
    private bool showSelected = true;
    private bool showHovered = false;
    private bool groupedSelection = true;
    private float transparencyValue = 0.5f;
    
    NodeVisibilityState? hoveredNodeState;
    NodeVisibilityState? hoveredLayerState;
    
    public float TransparencyValue
    {
        get
        {
            return transparencyValue;
        }
        set
        {
            transparencyValue = value;
            nodeController.RootNode.FadeAlpha = transparencyValue;
        }
    }
    
    public bool ShowTooltipSelected
    {
        get
        {
            return showTooltipSelected;
        }
        set
        {
            showTooltipSelected = value;
            ModelNode selectedNode = nodeController.SelectedNode;
            if (selectedNode != null)
            {
                selectedNode.TooltipShowing = showTooltipSelected;
            }
        }
    }
    public bool ShowTooltipHovered
    {
        get
        {
            return showTooltipHovered;
        }
        set
        {
            showTooltipHovered = value;
            ModelNode hoveredNode = nodeController.HoveredNode;
            if (hoveredNode != null)
            {
                hoveredNode.TooltipShowing = showTooltipHovered;
            }
        }
    }
    public bool OutlineSelected
    {
        get
        {
            return outlineSelected;
        }
        set
        {
            outlineSelected = value;
            ModelNode selectedNode = nodeController.SelectedNode;
            if (selectedNode != null)
            {
                selectedNode.IsOutlined = outlineSelected;
            }
        }
    }
    public bool OutlineHovered
    {
        get
        {
            return outlineHovered;
        }
        set
        {
            outlineHovered = value;
            ModelNode hoveredNode = nodeController.HoveredNode;
            if (hoveredNode != null)
            {
                hoveredNode.IsOutlined = outlineHovered;
            }
        }
    }
    public NodeController NodeController { 
        get 
        { 
            return nodeController; 
        }
        set
        {
            if (nodeController != value)
            {
                UnregisterEvents();
                nodeController = value;
                RegisterEvents();
            }
        }
    }

    private void Start()
    {
        RegisterEvents();
    }

    public void OnDestroy()
    {
        UnregisterEvents();
    }
    
    private void RegisterEvents()
    {
        if (nodeController != null)
        {
            nodeController.NodeSelected += OnNodeSelected;
            nodeController.NodeHovered += OnNodeHovered;
            nodeController.NodeDeselected += OnNodeDeselected;
            nodeController.NodeUnhovered += OnNodeUnhovered;
            nodeController.NodeAdded += OnNodeAdded;
            foreach (LayerInstance layerInstance in nodeController.Layers)
            {
                layerInstance.isHidden.OnValueChanged += (oldValue, newValue) => NodeVisibilityChanged?.Invoke();
                layerInstance.isFaded.OnValueChanged += (oldValue, newValue) => NodeVisibilityChanged?.Invoke();
            }
        }
    }
    
    private void UnregisterEvents()
    {
        if (nodeController != null)
        {
            nodeController.NodeSelected -= OnNodeSelected;
            nodeController.NodeHovered -= OnNodeHovered;
            nodeController.NodeDeselected -= OnNodeDeselected;
            nodeController.NodeUnhovered -= OnNodeUnhovered;
            nodeController.NodeAdded -= OnNodeAdded;
        }
    }

    public void OnNodeAdded(ModelNode node)
    {
        if (node is LayerInstance layerInstance)
        {
            layerInstance.isHidden.OnValueChanged += (oldValue, newValue) => NodeVisibilityChanged?.Invoke();
            layerInstance.isFaded.OnValueChanged += (oldValue, newValue) => NodeVisibilityChanged?.Invoke();
        }
    }

    //Selection and Hover logic
    private void OnNodeSelected(Guid nodeGuid)
    {
        if (outlineSelected)
        {
            nodeController.Node(nodeGuid).IsOutlined = true;
        }
        if (showTooltipSelected)
        {
            nodeController.Node(nodeGuid).TooltipShowing = true;
        }
        if (showSelected)
        {
            ShowNode(nodeGuid);
        }
    }
    private void OnNodeHovered(Guid nodeGuid)
    {
        ModelNode hoveredNode = nodeController.Node(nodeGuid);
        if (outlineHovered)
        {
            hoveredNode.IsOutlined = true;
            hoveredNode.OutlineColorIdx = 1;
        }
        if (showTooltipHovered)
        {
            hoveredNode.TooltipShowing = true;
        }
        if (showHovered)
        {
            hoveredNodeState = hoveredNode.VisibilityState;
            ShowNode(nodeGuid);
        }
    }
    private void OnNodeDeselected(Guid nodeGuid)
    {
        if (outlineSelected)
        {
            if (!outlineHovered | nodeController.HoveredNodeGuid != nodeGuid)
            {
                nodeController.Node(nodeGuid).IsOutlined = false;
            }
        }
        if (showTooltipSelected)
        {
            if (!showTooltipHovered | nodeController.HoveredNodeGuid != nodeGuid)
            {
                nodeController.Node(nodeGuid).TooltipShowing = false;
            }
        }

    }
    private void OnNodeUnhovered(Guid nodeGuid)
    {
        ModelNode unhoveredNode = nodeController.Node(nodeGuid);
        if (outlineHovered)
        {
            unhoveredNode.IsOutlined = false;
            unhoveredNode.OutlineColorIdx = 0;
            if (outlineSelected && nodeController.SelectedNode != null)
            {
                nodeController.SelectedNode.IsOutlined = true;
            }
        }
        if (showTooltipHovered)
        {
            if (!showTooltipSelected | nodeController.SelectedNodeGuid != nodeGuid)
            {
                nodeController.Node(nodeGuid).TooltipShowing = false;
            }
        }
        if (showHovered)
        {
            switch (hoveredNodeState)
            {
                case NodeVisibilityState.Hidden:
                    HideNode(nodeGuid);
                    break;
                case NodeVisibilityState.Faded:
                    FadeNode(nodeGuid);
                    break;
            }
            hoveredNodeState = null;
        }
        
    }
    public bool IsHidden(Guid nodeGuid)
    {
        return nodeController.Node(nodeGuid).IsHidden;
    }
    public bool IsFaded(Guid nodeGuid)
    {
        return nodeController.Node(nodeGuid).IsFaded;
    }

    public bool OtherLayersHidden(Guid nodeGuid)
    {
        ModelNode targetNode = nodeController.Node(nodeGuid);
        foreach (ModelNode node in nodeController.Nodes)
        {
            if (node.IsChildOf(targetNode) || node == targetNode ||
                node.IsParentOf(targetNode))
            {
                continue;
            }

            if (!node.IsHidden)
            {
                return false;
            }
        }

        return true;
    }

    public bool OtherLayersFaded(Guid nodeGuid)
    {
        ModelNode targetNode = nodeController.Node(nodeGuid);
        foreach (ModelNode node in nodeController.Nodes)
        {
            if (node.IsChildOf(targetNode) || node == targetNode ||
                node.IsParentOf(targetNode))
            {
                continue;
            }

            if (!node.IsFaded)
            {
                return false;
            }
        }

        return true;
    }

    //Layer visibility methods
    public void ToggleHideNode(Guid nodeGuid)
    {
        nodeController.Node(nodeGuid).ToggleHidden();
        NodeVisibilityChanged?.Invoke();
    }

    public void ToggleFadeNode(Guid nodeGuid)
    {
        nodeController.Node(nodeGuid).ToggleFaded();
        NodeVisibilityChanged?.Invoke();
    }

    public void HideNode(Guid nodeGuid)
    {
        nodeController.Node(nodeGuid).IsHidden = true;
        NodeVisibilityChanged?.Invoke();
    }

    public void FadeNode(Guid nodeGuid)
    {
        nodeController.Node(nodeGuid).IsFaded = true;
        NodeVisibilityChanged?.Invoke();
    }

    public void ShowNode(Guid nodeGuid)
    {
        nodeController.Node(nodeGuid).IsHidden = false;
        NodeVisibilityChanged?.Invoke();
    }

    public void UnfadeNode(Guid nodeGuid)
    {
        nodeController.Node(nodeGuid).IsFaded = false;
        NodeVisibilityChanged?.Invoke();
    }

    public void ShowAll()
    {
        nodeController.RootNode.IsHidden = false;
        NodeVisibilityChanged?.Invoke();
    }

    public void UnfadeAll()
    {
        nodeController.RootNode.IsFaded = false;
        NodeVisibilityChanged?.Invoke();
    }

    public void HideAll()
    {
        nodeController.RootNode.IsHidden = true;
        NodeVisibilityChanged?.Invoke();
    }
    public void FadeAll()
    {
        nodeController.RootNode.IsFaded = true;
        NodeVisibilityChanged?.Invoke();
    }

    public void HideOtherLayers(Guid nodeGuid, bool hideLayers = true)
    {
        ModelNode currentNode = nodeController.Node(nodeGuid);
        foreach (ModelNode node in nodeController.RootNode.Subnodes)
        {
            if (node != currentNode && !currentNode.IsParentOf(node) && !currentNode.IsChildOf(node))
            {
                node.IsHidden = hideLayers;
            }
        }
        NodeVisibilityChanged?.Invoke();
    }


    public void FadeOtherLayers(Guid nodeGuid, bool fadeLayers = true)
    {
        ModelNode currentNode = nodeController.Node(nodeGuid);
        foreach (ModelNode node in nodeController.RootNode.Subnodes)
        {
            if (node != currentNode && !currentNode.IsParentOf(node) && !currentNode.IsChildOf(node))
            {
                node.IsFaded = fadeLayers;
            }
        }
    }
    
    
}

﻿/* Add this to a GameObject with SkinnedMeshRenderer having some blend shapes,
 * with each shape representing consecutive desired look of the mesh.
 * This script will perform a smooth looping animation between all blend shapes.
 */

using UnityEngine;

public class BlendShapeAnimation : MonoBehaviour, IAnimatable
{
    /* Private fields constant after Start() */
    private int blendShapeCount;
    private SkinnedMeshRenderer skinnedMeshRenderer;
    private Mesh skinnedMesh;
    // At any time, only these two blend shapes may have non-zero weights.
    private int lastPreviousShape, lastNextShape;

    /* Current state, may change in each Update() */
    private float currentIndex = 0f;

    /* Public fields, configurable from Unity Editor */
    private bool loopAnimation = true;

    public bool LoopAnimation
    {
        get
        {
            return loopAnimation;
        }
        set
        {
            loopAnimation = value;
        }
    }
    
    /* Call this after creation, before using anything that depends on blend shape count,
     * like CurrentTime or SpeedNormalized.
     * This makes sure that internal blendShapeCount is initialized.
     * Waiting until Start() is called on this component is sometimes not comfortable.
     */
    public void InitializeBlendShapes()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        skinnedMesh = skinnedMeshRenderer.sharedMesh;
        //meshCollider = GetComponent<MeshCollider>();

        blendShapeCount = skinnedMesh.blendShapeCount;
        if (blendShapeCount == 0) {
            Debug.LogWarning("No blendShapes, will not animate");
        }

        // Make sure all blend shapes start with weight 0, in case user was playing around with them in Unity Editor.
        skinnedMeshRenderer.SetBlendShapeWeight(0, 100f);
        for (int i = 1; i < blendShapeCount; i++)
        {
            skinnedMeshRenderer.SetBlendShapeWeight(i, 0f);
        }
        //Mesh bakeMesh = new Mesh();
       // skinnedMeshRenderer.BakeMesh(bakeMesh);
        //meshCollider.sharedMesh = bakeMesh;
    }
    

    /* Update current mesh shape, looking at currentIndex. */
    private void UpdateBlendShapes()
    {
        if (blendShapeCount == 0) {
            return;
        }

        skinnedMeshRenderer.SetBlendShapeWeight(lastPreviousShape, 0f);
        skinnedMeshRenderer.SetBlendShapeWeight(lastNextShape, 0f);
        if (currentIndex == 42f)
        {
            Debug.Log("test");
        }
        int previousShape = (int)currentIndex % MaxCurrentIndex();
        float frac = currentIndex - previousShape;
        int nextShape = previousShape + 1;
        if (nextShape > blendShapeCount - 1)
        {
            nextShape = 0;
        }

        skinnedMeshRenderer.SetBlendShapeWeight(previousShape, 100f * (1f - frac));
        skinnedMeshRenderer.SetBlendShapeWeight(nextShape, 100f * frac);

        lastPreviousShape = previousShape;
        lastNextShape = nextShape;
    }

    private int MaxCurrentIndex()
    {
        return loopAnimation ? blendShapeCount: blendShapeCount - 1;
    }

    // Current time in animation, in range 0..1.
    public float CurrentTime
    {
        get
        {
            return currentIndex / MaxCurrentIndex();
        }
        set
        {
            currentIndex = MaxCurrentIndex() * value;
            UpdateBlendShapes();
        }
    }

}

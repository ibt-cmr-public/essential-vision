public interface IAnimatable
{
    float CurrentTime { get; set; }
    bool LoopAnimation { get; set; }   
}

using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

/* Keeps track of a list of BlendShapeAnimations and controls them in a unified manner
 * Exposes methods to stop/pause/etc. animations and set animation speed
 */
public enum AnimationMode
{
    Loop,    // Animation plays repeatedly from start to end
    Reverse, // Animation plays forward and then reverses back to start
    Once     // Animation plays once and stops at the end
}

public class ModelAnimator: NetworkBehaviour
{
    public NetworkVariable<bool> isPlaying = new NetworkVariable<bool>(true);
    public NetworkVariable<float> animationSpeed = new NetworkVariable<float>(1f);
    private NetworkVariable<float> currentTime = new NetworkVariable<float>(0f);
    private NetworkVariable<AnimationMode> animationMode = new NetworkVariable<AnimationMode>(AnimationMode.Loop);
    private NetworkVariable<float> animationDuration = new NetworkVariable<float>(1f);
    
    // List of animations that are controlled - In the future, update this to use a common interface for animations
    private List<IAnimatable> animations = new List<IAnimatable>();
    private bool mirrorIncreasing;
    
    // Event that gets invoked when the animation state changes
    public event Action AnimationStateChanged;


    public override void OnNetworkSpawn()
    {
        currentTime.OnValueChanged += OnCurrentTimeChanged;
        animationMode.OnValueChanged += OnAnimationModeValueChanged;
    }

    // Reset the animation controller
    public void Reset()
    {
        ResetServerRpc();
    }

    [Rpc (SendTo.Server)] 
    public void ResetServerRpc()
    {
        currentTime.Value = 0f;
        animationSpeed.Value = 1f;
        animationMode.Value = AnimationMode.Loop;
        isPlaying.Value = true;
        ResetClientRpc();
    }
    
    [Rpc (SendTo.Everyone)] 
    public void ResetClientRpc()
    {
        animations.Clear();
    }
    

    //Animation update logic
    private void Update()
    {
        if (!IsServer)
        {
            return;
        }
        if (isPlaying.Value)
        {
            switch (animationMode.Value)
            {
                case AnimationMode.Loop:
                    UpdateCyclic();
                    break;
                case AnimationMode.Reverse:
                    UpdateMirror();
                    break;
                case AnimationMode.Once:
                    UpdateOnce();
                    break;
            }
        }
    }
    private void UpdateMirror()
    {
        float newTime = CurrentTime;
        if (mirrorIncreasing)
        {
            newTime += (Time.deltaTime * AnimationSpeed)/animationDuration.Value;
            if (newTime > 1.0f)
            {
                mirrorIncreasing = false;
                newTime = 1f - (newTime - 1f);
            }
        }
        else
        {
            newTime -= (Time.deltaTime * AnimationSpeed)/animationDuration.Value;
            if (newTime < 0f)
            {
                mirrorIncreasing = true;
                newTime = -newTime;
            }

        }

        CurrentTime = newTime;
    }
    private void UpdateCyclic()
    {
        float newTime = CurrentTime;
        newTime += (Time.deltaTime * AnimationSpeed) / animationDuration.Value;
        newTime = Mathf.Repeat(newTime, 1f);

        CurrentTime = newTime;
    }
    private void UpdateOnce()
    {
        float newTime = CurrentTime;
        newTime += (Time.deltaTime * AnimationSpeed) / animationDuration.Value;
        newTime = Mathf.Min(newTime, 1f);
        if (Math.Abs(newTime - 1f) < 0.0001)
        {
            IsPlaying = false;
        }
        CurrentTime = newTime;
    }
    
    // Add a BlendShapeAnimation to the list of controlled animations
    public void AddAnimation(IAnimatable animation)
    {
        animations.Add(animation);
        animation.CurrentTime = CurrentTime;
    }
    
    //Setting and getting the animation state
    public void SetAnimationState(AnimationState state, AnimationStateTransition stateTransition = null)
    {
        if (stateTransition == null)
        {
            stateTransition = new AnimationStateTransition();
        }
        SetAnimationStateServerRpc(state, stateTransition);
    }
    
    [ServerRpc (RequireOwnership = false)] 
    private void SetAnimationStateServerRpc(AnimationState state, AnimationStateTransition stateTransition)
    {
        if (state == null) return;
        if (stateTransition.updateIsPlaying) SetIsPlayingServerRpc(state.isPlaying);
        if (stateTransition.updateCurrentTime) SetCurrentTimeServerRpc(state.currentTime);
        if (stateTransition.updateAnimationSpeed) SetAnimationSpeedServerRpc(state.animationSpeed);
        if (stateTransition.updateAnimationMode) SetAnimationModeServerRpc(state.animationMode);
    }
    public AnimationState state
    {
        get
        {
            AnimationState currentState = new AnimationState();
            currentState.animationSpeed = AnimationSpeed;
            currentState.animationMode = AnimationMode;
            currentState.currentTime = CurrentTime;
            currentState.isPlaying = IsPlaying;
            return currentState;
        }
    }
    
    //Networked properties
    // Property to get or set the animation mode (Loop, Reverse, Once)
    public AnimationMode AnimationMode
    {
        get { return animationMode.Value; }
        set
        {
           SetAnimationModeServerRpc(value);
        }
    }
    [ServerRpc (RequireOwnership = false)]
    private void SetAnimationModeServerRpc(AnimationMode value)
    {
        animationMode.Value = value;
    }
    private void OnAnimationModeValueChanged(AnimationMode oldValue, AnimationMode newValue)
    {
        if (newValue == AnimationMode.Loop)
        {
            foreach (IAnimatable animation in animations)
            {
                animation.LoopAnimation = true;
            }
        }
        else
        {
            foreach (IAnimatable animation in animations)
            {
                animation.LoopAnimation = false;
            }
        }
        AnimationStateChanged?.Invoke();
    }

    // Property to get or set the current time of the animations
    public float CurrentTime
    {
        get
        {
            return currentTime.Value;
        }
        set
        {
            SetCurrentTimeServerRpc(value);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    private void SetCurrentTimeServerRpc(float value)
    {
        currentTime.Value = value;
    }
    private void OnCurrentTimeChanged(float oldValue, float newValue)
    {
        //Filter out null animations
        animations = animations.Where(animation => animation != null).ToList();

        foreach (IAnimatable animation in animations)
        {
            animation.CurrentTime = newValue;
        }
    }

    [ServerRpc (RequireOwnership = false)]
    private void SetAnimationSpeedServerRpc(float value)
    {
        animationSpeed.Value = value;
    }
    public float AnimationSpeed
    {
        get
        {
            return animationSpeed.Value;
        }
        set
        {
            SetAnimationSpeedServerRpc(value);
        }
    }
    public bool IsPlaying
    {
        get
        {
            return isPlaying.Value;
        }
        set
        {
            SetIsPlayingServerRpc(value);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetIsPlayingServerRpc(bool value)
    {
        isPlaying.Value = value;
    }
    
    //Public methods for controlling the animation
    //Stop all animations and set the current time to 0
    public void Stop()
    {
        IsPlaying = false;
        CurrentTime = 0;
    }

    // Toggle the IsPlaying property (pause if playing, play if paused)
    public void TogglePlay()
    {
        IsPlaying = !IsPlaying;
    }

    // Start playing the animations
    public void Play()
    {
        IsPlaying = true;
    }

    // Pause the animations
    public void Pause()
    {
        IsPlaying = false;
    }
}
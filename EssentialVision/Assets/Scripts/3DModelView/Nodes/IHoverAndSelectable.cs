using System;

public interface IHoverAndSelectable
{
    event Action HoverEntered;
    event Action HoverExited;
    event Action Selected;
}

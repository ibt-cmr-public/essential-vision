using System.Collections;
using System.Collections.Generic;
using UnityEngine;using UnityEngine.EventSystems;
public class DesktopHoverAndSelectable : MonoBehaviour, IHoverAndSelectable, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    public event System.Action HoverEntered;
    public event System.Action HoverExited;
    public event System.Action Selected;

    public void OnPointerEnter(PointerEventData eventData)
    {
        HoverEntered?.Invoke();
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        HoverExited?.Invoke();
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        Selected?.Invoke();
    }
    
}

using UnityEngine;
using UnityEngine.EventSystems;

public class HoverAndSelectable : MonoBehaviour, IHoverAndSelectable, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public event System.Action HoverEntered;
    public event System.Action HoverExited;
    public event System.Action Selected;

    public void OnPointerEnter(PointerEventData eventData)
    {
        HoverEntered?.Invoke();
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        HoverExited?.Invoke();
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        Selected?.Invoke();
    }
}

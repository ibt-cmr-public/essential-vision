using System;
using MixedReality.Toolkit.SpatialManipulation;
using UnityEngine;

public class XRHoverAndSelectable : MonoBehaviour, IHoverAndSelectable
{
    public event Action HoverEntered;
    public event Action HoverExited;
    public event Action Selected;

    [SerializeField] private ObjectManipulator objectManipulator;
    
    public ObjectManipulator ObjectManipulator
    {
        get { return objectManipulator; }
        set
        {
            objectManipulator = value;
            objectManipulator.IsRayHovered.OnEntered.AddListener((_) => HoverEntered?.Invoke());
            objectManipulator.IsRayHovered.OnExited.AddListener((_) => HoverExited?.Invoke());
            objectManipulator.IsRaySelected.OnEntered.AddListener((_) => Selected?.Invoke());
        }
    }

    public void Start()
    {
        if (ObjectManipulator == null)
        {
            ObjectManipulator = GetComponent<ObjectManipulator>();
        }
        else
        {
            objectManipulator.IsRayHovered.OnEntered.AddListener((_) => HoverEntered?.Invoke());
            objectManipulator.IsRayHovered.OnExited.AddListener((_) => HoverExited?.Invoke());
            objectManipulator.IsRaySelected.OnEntered.AddListener((_) => Selected?.Invoke());
        }
    }
}

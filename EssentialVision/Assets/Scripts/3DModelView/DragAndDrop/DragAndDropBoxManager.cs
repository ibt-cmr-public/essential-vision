using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class DragAndDropBoxManager : NetworkBehaviour
{
    [SerializeField] private NodeController nodeController;
    [SerializeField] private GameObject dragAndDropBoxPrefab;
    [SerializeField] private GameObject dragAndDropBoxContainer;
    private Dictionary<string, DragAndDropBox> boxes = new Dictionary<string, DragAndDropBox>();
    
    
    public void SpawnDragAndDropBox(Vector3 position, Vector3 size, string title = "")
    {
        if (boxes.ContainsKey(title))
        {
            Debug.LogError("A box with the same title already exists.");
            return;
        }
        SpawnDragAndDropBoxServerRpc(position, size, title);
    }
    
    [ServerRpc (RequireOwnership = false)]
    private void SpawnDragAndDropBoxServerRpc(Vector3 position, Vector3 size, string title = "")
    {
        if (boxes.ContainsKey(title))
        {
            Debug.LogError("A box with the same title already exists.");
            return;
        }
        DragAndDropBox box = Instantiate(dragAndDropBoxPrefab, dragAndDropBoxContainer.transform).GetComponent<DragAndDropBox>();
        NetworkObject boxNetworkObject = box.GetComponent<NetworkObject>();
        boxNetworkObject.Spawn();
        boxNetworkObject.TrySetParent(dragAndDropBoxContainer.transform);
        box.transform.localPosition = position;
        box.transform.localScale = size;
        box.Caption = title;
        SpawnDragAndDropBoxClientRpc(boxNetworkObject);
    }
    
    [ClientRpc]
    private void SpawnDragAndDropBoxClientRpc(NetworkObjectReference boxReference)
    {
        boxReference.TryGet(out NetworkObject boxObject);
        DragAndDropBox box = boxObject.GetComponent<DragAndDropBox>();
        AddSpawnedBox(box);
    }
    
    private void AddSpawnedBox(DragAndDropBox box)
    {
        foreach (LayerInstance layer in nodeController.Layers)
        {
            Collider layerCollider = layer.GetComponent<Collider>();
            if (layerCollider != null) box.registeredColliders.Add(layerCollider);
        }

        box.OnNodeHovered += (node) => NodeHovered(box, node);
        box.OnNodeUnhovered += (node) => NodeUnhovered(box, node);
        box.OnNodeAssigned += (node) => NodeAssigned(box, node);
        box.OnNodeUnassigned += (node) => NodeUnassigned(box, node);
        boxes[box.Caption] = box;
    }

    public void DestroyAllBoxes()
    {
        foreach (string title in boxes.Keys.ToList())
        {
            DestroyBox(title);
        }
    }
    
    private void DestroyBox(string title)
    {
        DestroyBoxServerRpc(title);
    }
    
    [ServerRpc]
    private void DestroyBoxServerRpc(string title = "")
    {
        if (!boxes.ContainsKey(title))
        {
            Debug.LogError("No box with the given title exists.");
            return;
        }
        boxes[title].GetComponent<NetworkObject>().Despawn();
        DestroyBoxClientRpc(title);
    }
    
    
    [ClientRpc]
    private void DestroyBoxClientRpc(string title)
    {
        boxes.Remove(title);
    }
    
    private void NodeHovered(DragAndDropBox hoveredBox, ModelNode node)
    {
        foreach (DragAndDropBox box in boxes.Values)
        {
            if (box == hoveredBox) continue;
            Collider nodeCollider = node.GetComponent<Collider>();
            if (box.registeredColliders.Contains(nodeCollider));
            {
                box.registeredColliders.Remove(nodeCollider);
            }
        }
    }
    
    private void NodeUnhovered(DragAndDropBox hoveredBox, ModelNode node)
    {
        foreach (DragAndDropBox box in boxes.Values)
        {
            if (box == hoveredBox) continue;
            Collider nodeCollider = node.GetComponent<Collider>();
            if (!box.registeredColliders.Contains(nodeCollider));
            {
                box.registeredColliders.Add(nodeCollider);
            }
        }
    }
    
    private void NodeAssigned(DragAndDropBox hoveredBox, ModelNode node)
    {
        foreach (DragAndDropBox box in boxes.Values)
        {
            if (box == hoveredBox) continue;
            Collider nodeCollider = node.GetComponent<Collider>();
            if (box.registeredColliders.Contains(nodeCollider));
            {
                box.registeredColliders.Remove(nodeCollider);
            }
        }
    }
    
    private void NodeUnassigned(DragAndDropBox hoveredBox, ModelNode node)
    {
        foreach (DragAndDropBox box in boxes.Values)
        {
            if (box == hoveredBox) continue;
            Collider nodeCollider = node.GetComponent<Collider>();
            if (!box.registeredColliders.Contains(nodeCollider));
            {
                box.registeredColliders.Add(nodeCollider);
            }
        }
    }
    
}

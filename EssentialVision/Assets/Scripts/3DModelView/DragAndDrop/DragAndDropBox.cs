using System;
using System.Collections.Generic;
using DG.Tweening;
using MixedReality.Toolkit;
using MixedReality.Toolkit.SpatialManipulation;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


//Node is assigned whenever it is dragged and released inside the box
//Node is unassigned whenever it is moved outside of the box in any way or when it is selected in the box
//Node is unassigned whenever another node is assigned (and automatically moved outside the box)
public class DragAndDropBox : NetworkBehaviour
{

    //State and visuals
    public enum State
    {
        Idle,
        Hovered,
        Assigned
    }
    
    private State previousState = State.Idle;
    private State state = State.Idle;
    [SerializeField] private GameObject cube;
    [SerializeField] private Material hoverMaterial;
    [SerializeField] private Material idleMaterial;
    [SerializeField] private TextMeshProUGUI captionText;
    [SerializeField] private GameObject captionCanvas;
    
    public List<Collider> registeredColliders = new List<Collider>();
    public Action<ModelNode> OnNodeHovered;
    public Action<ModelNode> OnNodeUnhovered;
    public Action<ModelNode> OnNodeAssigned;
    public Action<ModelNode> OnNodeUnassigned;
    private ModelNode assignedNode = null;
    private List<ModelNode> hoveredNodes = new List<ModelNode>();
    private string caption;


    public string Caption
    {
        get
        {
            return caption;
        }
        set
        {
            captionCanvas.SetActive(value != null && value != "");
            captionText.text = value;
            caption = value;
        }
    }

    private Bounds bounds
    {
        get
        {
            Renderer renderer = cube.GetComponent<Renderer>();
            return renderer.bounds;
        }
    }
    public ModelNode AssignedNode
    {
        get => assignedNode;
    }

    private void UpdateState()
    {
        if (hoveredNodes.Count > 0)
        {
            SetState(State.Hovered);
        }
        else if (assignedNode != null)
        {
            SetState(State.Assigned);
        }
        else
        {
            SetState(State.Idle);
        }
    }
    
    //Change visual state of the box
    public void SetState(State newState)
    {
        if (state == newState)
        {
            return;
        }
        previousState = state;
        state = newState;
        switch (state)
        {
            case State.Idle:
                cube.transform.DOScale(1.0f, 0.2f);
                cube.GetComponent<Renderer>().material = idleMaterial;
                break;
            case State.Hovered:
                cube.transform.DOScale(1.2f, 0.2f);
                cube.GetComponent<Renderer>().material = hoverMaterial;
                break;
            case State.Assigned:
                cube.transform.DOScale(1.0f, 0.2f);
                cube.GetComponent<Renderer>().material = hoverMaterial;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    //Use this to determine when a node is moved into the box through user interaction
    private void OnTriggerEnter(Collider other)
    {
        if (registeredColliders.Contains(other))
        {
            if (other.TryGetComponent(out ModelNode modelNode) && (other.TryGetComponent(out ObjectManipulator objectManipulator)))
            {
                if (objectManipulator.isSelected){
                    AddHoveredNode(modelNode);
                }
            }
        }
    }
    
    //Use this to determine when a node is moved outside of the box through user interaction
    private void OnTriggerExit(Collider other)
    {
        if (registeredColliders.Contains(other))
        {
            if (other.TryGetComponent(out ModelNode modelNode))
            {
                if (hoveredNodes.Contains(modelNode))
                {
                    ObjectManipulator objectManipulator = modelNode.GetComponent<ObjectManipulator>();
                    objectManipulator.lastSelectExited.RemoveListener(OnNodeReleasedInBox);
                    hoveredNodes.Remove(modelNode);
                    OnNodeUnhovered?.Invoke(modelNode);
                    UpdateState();
                }
                else if (assignedNode == modelNode)
                {
                    UnassignNode();
                    UpdateState();
                }
            }
        }
    }
    
    private void AddHoveredNode(ModelNode node)
    {
        if (!hoveredNodes.Contains(node))
        {
            hoveredNodes.Add(node);
            ObjectManipulator objectManipulator = node.GetComponent<ObjectManipulator>();
            objectManipulator.lastSelectExited.AddListener(OnNodeReleasedInBox);
            OnNodeHovered?.Invoke(node);
            UpdateState();
        }
    }

    private void RemoveHoveredNode(ModelNode node)
    {
        if (hoveredNodes.Contains(node))
        {
            ObjectManipulator objectManipulator = node.GetComponent<ObjectManipulator>();
            objectManipulator.lastSelectExited.RemoveListener(OnNodeReleasedInBox);
            hoveredNodes.Remove(node);
            OnNodeUnhovered?.Invoke(node);
            UpdateState();
        }
    }

    private void UnassignNode(bool ejectFromBox = false)
    {
        if (assignedNode  == null) return;
        ModelNode unassignedNode = assignedNode;
        ObjectManipulator objectManipulator = assignedNode.GetComponent<ObjectManipulator>();
        objectManipulator.firstSelectEntered.RemoveListener(OnAssignedNodeSelected);
        assignedNode = null;
        OnNodeUnassigned?.Invoke(unassignedNode);
        //Hover or eject node
        if (!ejectFromBox)
        {
            //We need to temporarily disallow scaling in order to reset the scale of the node to its initial value
            objectManipulator.AllowedManipulations &= ~TransformFlags.Scale;
            unassignedNode.ScaleCenterServerRpc(unassignedNode.InitialLocalTransform.localScale, 0.2f);
            objectManipulator.AllowedManipulations |= ~TransformFlags.Scale;
            AddHoveredNode(unassignedNode);
        }
        else
        {
            //TODO: Fix ejection logic
            unassignedNode.MoveAndScaleCenterGlobalServerRpc(cube.transform.position + new Vector3(0f,1f,0f), unassignedNode.InitialLocalTransform.localScale);
        }
        UpdateState();
    }

    private void AssignNode(ModelNode node)
    {
        //Remove previously assigned node out of box 
        UnassignNode(true);
        
        //Assign new node
        if (node != null)
        {
            RemoveHoveredNode(node);
            //Fit assigned node to cube bounds
            Bounds localNodeBounds = TransformHelper.TransformBounds(node.bounds, cube.transform.worldToLocalMatrix);
            float scaleFactor = 1.0f/Mathf.Max(localNodeBounds.size.x, localNodeBounds.size.y, localNodeBounds.size.z);
            node.MoveAndScaleCenterGlobalServerRpc(cube.transform.position, new Vector3(scaleFactor, scaleFactor, scaleFactor));

            //Add listeners
            ObjectManipulator objectManipulator = node.GetComponent<ObjectManipulator>();
            objectManipulator.firstSelectEntered.AddListener(OnAssignedNodeSelected);
        }
        assignedNode = node;
        OnNodeAssigned?.Invoke(node);
        UpdateState();
    }
    
    private void OnNodeReleasedInBox(SelectExitEventArgs args)
    {
        if (args.interactableObject.transform.TryGetComponent(out ModelNode modelNode))
        {
            AssignNode(modelNode);
        }
    }
    
    private void OnAssignedNodeSelected(SelectEnterEventArgs args)
    {
        if (args.interactableObject.transform.TryGetComponent(out ModelNode modelNode))
        {
            UnassignNode();
        }
    }
}

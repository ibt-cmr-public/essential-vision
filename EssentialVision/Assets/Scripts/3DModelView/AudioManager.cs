using System;
using MixedReality.Toolkit.Subsystems;
using MixedReality.Toolkit;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioManager : MonoBehaviour
{
    
    public bool PlayDescriptionOnSelection = false;
    private TextToSpeechSubsystem textToSpeechSubsystem;
    [SerializeField]
    private AudioSource audioSource;
    [FormerlySerializedAs("LayerController")] [SerializeField]
    private NodeController nodeController;


    public NodeController NodeController
    {
        get
        {
            return nodeController;
        }
        set
        {
            if (nodeController != null)
            {
                nodeController.NodeSelected -= OnNodeSelected;
            }
            nodeController = value;
            if (nodeController != null)
            {
                nodeController.NodeSelected += OnNodeSelected;
            }
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        textToSpeechSubsystem = XRSubsystemHelpers.GetFirstRunningSubsystem<TextToSpeechSubsystem>();
    }

    private void OnDestroy()
    {
        if (nodeController != null)
        {
            nodeController.NodeSelected -= OnNodeSelected;
        }
    }

    private void OnNodeSelected(Guid layerGuid)
    {
        if (PlayDescriptionOnSelection && textToSpeechSubsystem != null)
        {
            textToSpeechSubsystem.TrySpeak(nodeController.SelectedNode.NodeInfo.Description, audioSource);
        }
    }
}

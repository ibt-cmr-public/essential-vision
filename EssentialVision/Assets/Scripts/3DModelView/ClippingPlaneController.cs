using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using Unity.Netcode;

public enum ClippingPlaneType
{
    Plane,
    Cube,
    Sphere
}
public class ClippingPlaneController : NetworkBehaviour
{
    [SerializeField]
    public CustomClippingPlane clippingPlane;
    public List<VolumetricTexture> textures;

    public NetworkVariable<bool> isActive = new NetworkVariable<bool>(false);
    public NetworkVariable<int> textureIdx = new NetworkVariable<int>(-1);
    private NetworkVariable<ClippingPlaneType> clippingPlaneType = new NetworkVariable<ClippingPlaneType>(
        ClippingPlaneType.Plane);
    public event Action clippingPlaneStateChanged;
    
    public override void OnNetworkSpawn()
    {
        isActive.OnValueChanged += OnIsActiveValueChanged;
        textureIdx.OnValueChanged += OnTextureIdxValueChanged;
    }


    //Add a single layer instance to the clipping plane
    public void AddLayer(LayerInstance layer)
    {
        foreach (Renderer renderer in layer.Renderers){
            clippingPlane.AddRenderer(renderer);
        }
    }
    
    //Add a node (and its children) to the clipping plane
    public void AddNode(ModelNode node)
    {
        foreach (Renderer renderer in node.Renderers){
            clippingPlane.AddRenderer(renderer);
        }
    }
    //Set clipping plane state
    public ClippingPlaneState state
    {
        get
        {
            var clippingPlaneState = new ClippingPlaneState();
            clippingPlaneState.transform = new LocalTransform(transform);
            clippingPlaneState.isActive = IsActive;
            clippingPlaneState.type = Type;
            clippingPlaneState.volumetricTextureIdx = TextureIdx;
            return clippingPlaneState;
        }
    }
    public void SetClippingPlaneState(ClippingPlaneState state, ClippingPlaneStateTransition stateTransition = null)
    {
        if (state == null) return;
        if (stateTransition == null)
        {
            stateTransition = new ClippingPlaneStateTransition();
        }
        SetClippingPlaneStateServerRpc(state, stateTransition);
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetClippingPlaneStateServerRpc(ClippingPlaneState state, ClippingPlaneStateTransition stateTransition)
    {
        if (state == null) return;
        if (stateTransition.updateIsActive) SetIsActiveServerRpc(state.isActive);
        if (stateTransition.updateVolumetricTextureIdx) SetTextureIdxServerRpc(state.volumetricTextureIdx);
        if (stateTransition.updateType) SetClippingPlaneTypeServerRpc(state.type);
        if (stateTransition.updateTransform) SetLocalTransformServerRpc(
            state.transform,
            1f, 
            stateTransition.transformTransition.updateLocalPosition,
            stateTransition.transformTransition.updateLocalRotation,
            stateTransition.transformTransition.updateLocalScale);
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SetLocalTransformServerRpc(LocalTransform localTransform, float duration, bool setPosition = true, bool setRotation = true, bool setScale = true)
    {
        if (setPosition) clippingPlane.transform.DOLocalMove(localTransform.localPosition, duration);
        if (setRotation) clippingPlane.transform.DOLocalRotateQuaternion(localTransform.localRotation, duration);
        if (setScale) clippingPlane.transform.DOScale(localTransform.localScale, duration);
    }

    //Networked properties
    public bool IsActive { 
        get 
        { 
            return isActive.Value; 
        }
        set
        {
            SetIsActiveServerRpc(value);
        }
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SetIsActiveServerRpc(bool value)
    {
        isActive.Value = value;
    }
    
    private void OnIsActiveValueChanged(bool oldValue, bool newValue)
    {
        if (clippingPlane != null)
        {
            clippingPlane.gameObject.SetActive(newValue);
        }
        clippingPlaneStateChanged?.Invoke();
    }
    
    public int TextureIdx
    {
        get
        {
            return textureIdx.Value;
        }
        set
        {
            SetTextureIdxServerRpc(value);
        }
    }
    
    [ServerRpc (RequireOwnership = false)]
    private void SetTextureIdxServerRpc(int value)
    {
        textureIdx.Value = value;
    }
    
    private void OnTextureIdxValueChanged(int oldValue, int newValue)
    {
        clippingPlane.VolumetricTexture = newValue == -1 ? null : textures[newValue];
    }
    
    public ClippingPlaneType Type
    {
        get { return clippingPlaneType.Value; }
        set
        {
            SetClippingPlaneTypeServerRpc(value);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetClippingPlaneTypeServerRpc(ClippingPlaneType value)
    {
        clippingPlaneType.Value = value;
    }
    
}

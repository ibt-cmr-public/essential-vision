using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MixedReality.Toolkit.SpatialManipulation;
using TreeView;
using Unity.Netcode;
using UnityEngine;

//Class for spawning and initializing layers 
public class LayerSpawner : NetworkBehaviour
{

    [SerializeField]
    public ModelAnimator animator;
    [SerializeField]
    public ClippingPlaneController clippingPlaneController;

    [SerializeField] public Model3DView modelView;
    
    [SerializeField] private GameObject tooltipPrefab;
    [SerializeField] private GameObject tooltipContainer;
    [SerializeField] private GameObject emptyModelNodePrefab;
    [SerializeField] private GameObject staticModelNodePrefab;
    [SerializeField] private GameObject animatedModelNodePrefab;
    
    

    //Spawns a model node and all corresponding children in the scene
    //The tree structure of the node is also preserved in the scene
    public async Task<List<NetworkObject>> SpawnNodeAsync(TreeNode<ModelNodeScriptableObject> modelNode, Transform parentTransform)
    {
        if (!IsServer)
        {
            return null;
        }

        NetworkObject spawnedObject = null;
        List<NetworkObject> spawnedObjects = new List<NetworkObject>();
        switch (modelNode.Data)
        {
            case LayerScriptableObject layer:
                if (layer.IsAnimated)
                {
                    spawnedObject =  Instantiate(animatedModelNodePrefab, parentTransform).GetComponent<NetworkObject>(); 
                }
                else
                {
                    spawnedObject = Instantiate(staticModelNodePrefab, parentTransform).GetComponent<NetworkObject>();
                }
                break;
            
            default:
                spawnedObject = Instantiate(emptyModelNodePrefab, parentTransform).GetComponent<NetworkObject>();
                break;
        }
        spawnedObject.Spawn();
        spawnedObject.TrySetParent(parentTransform);
        
        if (spawnedObject == null)
        {
            return spawnedObjects;
        }
        
        spawnedObjects.Add(spawnedObject);
        
        foreach (TreeNode<ModelNodeScriptableObject> node in modelNode.Children)
        {
            spawnedObjects.AddRange(await SpawnNodeAsync(node, spawnedObject.transform));
        }

        return spawnedObjects;
    }



    public void RecreateTree(ModelScriptableObject model, List<ModelNode> instantiatedNodes)
    {
        List<TreeNode<ModelNodeScriptableObject>> nodeList = model.ModelTree.NodeList;
        foreach (TreeNode<ModelNodeScriptableObject> node in nodeList)
        {
            if (node.Parent != null)
            {
                int parentIdx = nodeList.IndexOf(node.Parent);
                int childIdx = nodeList.IndexOf(node);
                instantiatedNodes[parentIdx].AddChild(instantiatedNodes[childIdx]);
            }

        }
    }
    
    
    public async Task InitializeInstantiatedLayersAsync(IEnumerable<ModelNodeScriptableObject> nodeInfos, NetworkObjectReference[] instantiatedLayersNetworkReferenceList)
{
    // Kick off all InitializeNodeAsync calls
    var initializeTasks = instantiatedLayersNetworkReferenceList.Zip(nodeInfos, async (networkReference, nodeInfo) =>
    {
        NetworkObjectReference layerNetworkObjectReference = networkReference;
        layerNetworkObjectReference.TryGet(out NetworkObject nodeNetworkObject);
        GameObject nodeObject = nodeNetworkObject.gameObject;
        ModelNode modelNode = nodeNetworkObject.GetComponent<ModelNode>();

        if (modelNode == null)
        {
            Debug.LogError("ModelNode component is missing from layer object.");
            return;
        }

        nodeObject.SetActive(true);
        // Set node info
        await modelNode.SetNodeInfoAsync(nodeInfo);
    });
    

    // Await the completion of InitializeNodeAsync calls
    await Task.WhenAll(initializeTasks);
    
    
        // While the node initialization tasks are running, perform other tasks
    // Instantiate tooltips, add to clipping plane controller, set object manipulator stuff
    foreach (var pair in instantiatedLayersNetworkReferenceList.Zip(nodeInfos, (networkReference, nodeInfo) => new { NetworkReference = networkReference, NodeInfo = nodeInfo }))
    {
        NetworkObjectReference layerNetworkObjectReference = pair.NetworkReference;
        ModelNodeScriptableObject nodeInfo = pair.NodeInfo;
        layerNetworkObjectReference.TryGet(out NetworkObject nodeNetworkObject);
        GameObject nodeObject = nodeNetworkObject.gameObject;
        ModelNode modelNode = nodeNetworkObject.GetComponent<ModelNode>();

        //Instantiate tooltips
        Tooltip tooltip = Instantiate(tooltipPrefab, tooltipContainer.transform).GetComponent<Tooltip>();
        tooltip.TargetObject = nodeObject;
        tooltip.Caption = nodeInfo.Name;
        tooltip.gameObject.SetActive(false);
        modelNode.Tooltip = tooltip;

        if (modelNode is LayerInstance layerInstance && nodeInfo is LayerScriptableObject layerScriptableObject)
        {
            //Add to clipping plane controller
            clippingPlaneController.AddLayer(layerInstance);

            // Add capsule collider
            CapsuleCollider meshCollider = nodeObject.AddComponent<CapsuleCollider>();
            
            // Set object manipulator stuff
            if (layerScriptableObject.IsInteractable)
            {
                //Check if current operating system is hololens
                if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
                {
                    ObjectManipulator objectManipulator = nodeObject.AddComponent<ObjectManipulator>();
                    objectManipulator.HostTransform = modelView.transform;
                    XRHoverAndSelectable xrHoverAndSelectable = nodeObject.AddComponent<XRHoverAndSelectable>();
                    xrHoverAndSelectable.ObjectManipulator = objectManipulator;
                    OwnershipController ownershipController = nodeObject.AddComponent<OwnershipController>();
                    ownershipController.ObjectManipulator = objectManipulator;
                }
                else if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.Desktop)
                {
                    DesktopHoverAndSelectable desktopHoverAndSelectable = nodeObject.AddComponent<DesktopHoverAndSelectable>();
                }
            }

            // Add BlendShapeAnimation to animator if it is animated
            if (layerScriptableObject.IsAnimated)
            {
                BlendShapeAnimation animation = nodeObject.GetComponent<BlendShapeAnimation>();
                animation.InitializeBlendShapes();
                animator.AddAnimation(animation);
            }
        }
    }
}


}

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using Sequence = DG.Tweening.Sequence;


public enum NodeVisibilityState
{
    Visible,
    Faded,
    Hidden
}

public class ModelNode : NetworkBehaviour
{
    
    //Node Info
    public Action OnNodeInfoChanged;
    private NetworkVariable<FixedString64Bytes> name = new NetworkVariable<FixedString64Bytes>(new FixedString64Bytes(""));
    private NetworkVariable<FixedString512Bytes> description = new NetworkVariable<FixedString512Bytes>(new FixedString512Bytes(""));

    public virtual NodeType NodeType
    {
        get { return NodeType.Empty; }
    }
    public string Name
    {
        get => name.Value.ToString();
        set
        {
            SetNameServerRpc(value);
        }
    }
    [Rpc]
    private void SetNameServerRpc(string value)
    {
        name.Value = new FixedString64Bytes(value);
    }
    private void OnNameValueChanged(FixedString64Bytes oldValue, FixedString64Bytes newValue)
    {
        if (Tooltip != null)
        {
            Tooltip.Caption = newValue.ToString();
        }
        OnNodeInfoChanged?.Invoke();
    }
    
    public string Description
    {
        get => description.Value.ToString();
        set
        {
            SetDescriptionServerRpc(value);
        }
    }
    [Rpc]
    private void SetDescriptionServerRpc(string value)
    {
        description.Value = new FixedString512Bytes(value);
    }
    private void OnDescriptionValueChanged(FixedString512Bytes oldValue, FixedString512Bytes newValue)
    {
        OnNodeInfoChanged?.Invoke();
    }
    
    public virtual ModelNodeScriptableObject NodeInfo
    {
        get
        {
            ModelNodeScriptableObject nodeInfo = ScriptableObject.CreateInstance<ModelNodeScriptableObject>();
            nodeInfo.Name = Name;
            nodeInfo.Description = Description;
            return nodeInfo;
        }
        set
        {
            Name = value.Name;
            Description = value.Description;
        }
    }
     
    #region Tree structure
    
    private Guid guid;
    private int level = 0;
    private ModelNode parent = null;
    protected List<ModelNode> children = new List<ModelNode>();

    // public int Idx
    // {
    //     get => idx;
    //     set => idx = value;
    // }
    
    public Guid Guid
    {
        get => guid;
        set => guid = value;
    }

    public List<ModelNode> Children
    {
        get => children;
    }
    
    public ModelNode Parent
    {
        get => parent;
    }

    public ModelNode ParentOnLevel(int parentLevel)
    {
        if (parentLevel > level)
        {
            return null;
        }

        if (parentLevel == level)
        {
            return this;
        }
        
        return parent.ParentOnLevel(parentLevel);
    }

    public int Level
    {
        get => level;
    }
    
    public bool IsRoot
    {
        get => parent == null;
    }
    
    public bool IsLeaf
    {
        get => children.Count == 0;
    }

    public void AddChild(ModelNode child)
    {
        children.Add(child);
        if (child.Parent != null)
        {
            child.Parent.RemoveChild(child);
        }

        child.level = level + 1;
        child.parent = this;
    }

    public bool RemoveChild(ModelNode child)
    {
        child.parent = null;
        child.level = 0;
        return children.Remove(child);
    }

    public bool IsChildOf(ModelNode node)
    {
        if (parent == null)
        {
            return false;
        }
        if (parent == node)
        {
            return true;
        }
        else
        {
            return parent.IsChildOf(node);
        }
    }

    public bool IsParentOf(ModelNode node)
    {
        return node.IsChildOf(this);
    }
    
    //Enumerates over all subnodes (including the node itself) from the top down
    public IEnumerable<ModelNode> Subnodes
    {
        get
        {
            yield return this;

            foreach (var child in children)
            {
                foreach (var subnode in child.Subnodes)
                {
                    yield return subnode;
                }
            }
        }
    }
    
    //Enumerates over all subnodes (including the node itself) from the bottom up
    public IEnumerable<ModelNode> SubnodesBottomup
    {
        get
        {
            foreach (var child in children)
            {
                foreach (var subnode in child.SubnodesBottomup)
                {
                    yield return subnode;
                }
            }
            yield return this;
        }
    }
    
    public int NumSubnodes
    {
        get
        {
            int count = 1; // Count the current node

            foreach (var child in children)
            {
                count += child.NumSubnodes; // Recursively count nodes in each child subtree
            }

            return count;
        }
    }
    
    public ModelNode ClosestCommonParent(ModelNode node2)
    {
        if (this == node2)
        {
            return this;
        }
        else if (level > node2.level)
        {
            return parent.ClosestCommonParent(node2);
        }
        else if (level < node2.level)
        {
            return ClosestCommonParent(node2.parent);
        }
        else
        {
            return parent.ClosestCommonParent(node2.parent);
        }
    }
    
    #endregion
    
    //Stored transforms (used for positioning model correctly within a box of specified size)
    public LocalTransform InitialLocalTransform;

    //Tooltips
    private Tooltip tooltip;
    public NetworkVariable<bool> tooltipShowing = new NetworkVariable<bool>(false);
    
    //Node state
    public NodeState State
    {
        get
        {
            NodeState state = new NodeState();
            state.tooltipShowing = TooltipShowing;
            state.localTransform = localTransform;
            return state;
        }
    }

    public NodeState DefaultState
    {
        get
        {
            NodeState state = new NodeState();
            state.localTransform = InitialLocalTransform;
            return state;
        }
    }
    public virtual void SetNodeState(NodeState state, NodeStateTransition stateTransition = null)
    {
        if (state == null) return;
        if (stateTransition == null)
        {
            stateTransition = new NodeStateTransition();
        }
        if (stateTransition.updateTooltip) SetTooltipShowingServerRpc(state.tooltipShowing);
        if (stateTransition.updateLocalTransform) SetLocalTransformServerRpc(
            state.localTransform,
            1f, 
            stateTransition.transformTransition.updateLocalPosition,
            stateTransition.transformTransition.updateLocalRotation,
            stateTransition.transformTransition.updateLocalScale);
    }
    
    //Initialization and spawning
    private void Awake()
    {
        SetInitialSpecs();
    }
    private void SetInitialSpecs()
    {
        InitialLocalTransform = new LocalTransform();
        InitialLocalTransform.localPosition = transform.localPosition;
        InitialLocalTransform.localRotation = transform.localRotation;
        InitialLocalTransform.localScale = transform.localScale;
    }
    
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        tooltipShowing.OnValueChanged += OnTooltipShowingValueChanged;
    }

    #region Renderers and bounds
    public Renderer[] Renderers
    {
        get
        {
            return gameObject.GetComponentsInChildren<Renderer>();
        }
    }
    
    //Current bounds of the node in world space (axis aligned with world space)
    public Bounds bounds
    {
        get
        {

            Bounds bounds = new Bounds();
            bool firstBounds = true;
            foreach (Renderer renderer in Renderers)
            {
                if (firstBounds)
                {
                    bounds = renderer.bounds;
                    firstBounds = false;
                }
                else
                {
                    bounds.Encapsulate(renderer.bounds);
                }
            }

            return bounds;
        }
    }
    
    //Current bounds of the node in local space (axis-aligned in its own coordinate system)
    public Bounds localBounds
    {
        get
        {
            Bounds bounds = new Bounds();
            bool firstBounds = true;
            foreach (ModelNode child in Children)
            {
                Bounds newBounds = child.localBounds;
                Matrix4x4 childTransformMatrix = Matrix4x4.TRS(
                    child.localTransform.localPosition, child.localTransform.localRotation, child.localTransform.localScale);
                newBounds = TransformHelper.TransformBounds(newBounds, childTransformMatrix);

                if (firstBounds)
                {
                    bounds = newBounds;
                    firstBounds = false;
                }
                else
                {
                    bounds.Encapsulate(newBounds);
                }
            }
            
            foreach (Renderer renderer in GetComponents<Renderer>())             
            {
                Bounds newBounds = renderer.localBounds; 
                if (firstBounds)
                {
                    bounds = newBounds;
                    firstBounds = false;
                }
                else
                {
                    bounds.Encapsulate(newBounds);
                }
            }
            return bounds;
        }
    }

    
    //Returns the local bounds of the node (i.e. axis-aligned in its own coordinate system)
    //under all initial transforms (assuming all children have their initial local transforms) 
    public Bounds InitialLocalBounds 
    {
        get
        {
            Bounds bounds = new Bounds();
            bool firstBounds = true;
            foreach (ModelNode child in Children)
            {
                Bounds newBounds = child.InitialLocalBounds;
                Matrix4x4 childTransformMatrix = Matrix4x4.TRS(
                    child.InitialLocalTransform.localPosition, child.InitialLocalTransform.localRotation, child.InitialLocalTransform.localScale);
                newBounds = TransformHelper.TransformBounds(newBounds, childTransformMatrix);

                if (firstBounds)
                {
                    bounds = newBounds;
                    firstBounds = false;
                }
                else
                {
                    bounds.Encapsulate(newBounds);
                }
            }
            
            foreach (Renderer renderer in GetComponents<Renderer>())             
            {
                Bounds newBounds = renderer.localBounds; 
                if (firstBounds)
                {
                    bounds = newBounds;
                    firstBounds = false;
                }
                else
                {
                    bounds.Encapsulate(newBounds);
                }
            }
            return bounds;
        }
    }
    
    #endregion

    public virtual async Task SetNodeInfoAsync(ModelNodeScriptableObject value)
    {
        Name = value.Name;
        Description = value.Description;
    }
    

    #region Transform
    public LocalTransform localTransform
    {
        get
        {
            return new LocalTransform(transform);
        }
    }
        
    [ServerRpc(RequireOwnership = false)]
    public void SetLocalTransformServerRpc(LocalTransform localTransform, float duration, bool setPosition = true, bool setRotation = true, bool setScale = true)
    {
        if (setPosition) transform.DOLocalMove(localTransform.localPosition, duration);
        if (setRotation) transform.DOLocalRotateQuaternion(localTransform.localRotation, duration);
        if (setScale) transform.DOScale(localTransform.localScale, duration);
    }

    [ServerRpc(RequireOwnership = false)]
    public void MoveLocalServerRpc(Vector3 localPosition, float duration = 0.2f)
    {
        transform.DOLocalMove(localTransform.localPosition, duration);
    }

    [ServerRpc(RequireOwnership = false)]
    public void MoveGlobalServerRpc(Vector3 globalPosition, float duration = 0.2f)
    {
        transform.DOMove(globalPosition, duration);
    }

    [ServerRpc(RequireOwnership = false)]
    public void MoveCenterLocalServerRpc(Vector3 localPosition, float duration = 0.2f)
    {
        transform.DOLocalMove(localTransform.localPosition + transform.localPosition - localBounds.center, duration);
    }

    [ServerRpc(RequireOwnership = false)]
    public void MoveCenterGlobalServerRpc(Vector3 globalPosition, float duration = 0.2f)
    {
        transform.DOMove(transform.position + globalPosition - bounds.center, duration);
    }

    [ServerRpc(RequireOwnership = false)]
    public void MoveAndScaleCenterLocalServerRpc(Vector3 localPosition, Vector3 localScale, float duration = 0.2f)
    {
        MoveAndScaleCenterLocal(localPosition, localScale, duration);

    }

    public void MoveAndScaleCenterLocal(Vector3 localPosition, Vector3 localScale, float duration = 0.2f)
    {
        Sequence sequence = DOTween.Sequence();
        Vector3 scaleFactor = new Vector3(localScale.x / transform.localScale.x, localScale.y / transform.localScale.y, localScale.z / transform.localScale.z);
        sequence.Insert(0, transform.DOScale(localScale, duration));
        sequence.Insert(0, transform.DOLocalMove(localPosition + Vector3.Scale(scaleFactor, transform.localPosition - localBounds.center), duration));
        sequence.Play();
    }

    [ServerRpc(RequireOwnership = false)]
    public void MoveAndScaleCenterGlobalServerRpc(Vector3 globalPosition, Vector3 localScale, float duration = 0.2f)
    {
        MoveAndScaleCenterGlobal(globalPosition, localScale, duration);
    }

    public void MoveAndScaleCenterGlobal(Vector3 globalPosition, Vector3 localScale, float duration = 0.2f)
    { 
        Sequence sequence = DOTween.Sequence();
        Vector3 scaleFactor = new Vector3(localScale.x / transform.localScale.x, localScale.y / transform.localScale.y, localScale.z / transform.localScale.z);
        sequence.Insert(0, transform.DOScale(localScale, duration));
        sequence.Insert(0, transform.DOMove(globalPosition + Vector3.Scale(scaleFactor, transform.position - bounds.center), duration));
        sequence.Play();
    }

    [ServerRpc(RequireOwnership = false)]
    public void ScaleCenterServerRpc(Vector3 localScale, float duration = 0.2f)
    {
        ScaleCenter(localScale, duration);
    }

    public void ScaleCenter(Vector3 localScale, float duration = 0.2f)
    {
        Sequence sequence = DOTween.Sequence();
        Vector3 scaleFactor = new Vector3(transform.localScale.x - localScale.x, transform.localScale.y - localScale.y, transform.localScale.z - localScale.z);
        sequence.Insert(0, transform.DOScale(localScale, duration));
        sequence.Insert(0, transform.DOLocalMove(transform.localPosition + Vector3.Scale(scaleFactor, localBounds.center), duration));
        sequence.Play();
    }


    [ServerRpc(RequireOwnership = false)]
    public void ScaleServerRpc(Vector3 scale, float duration = 0.2f)
    {
        Debug.Log("ScaleServerRpc called.");
        transform.DOScale(scale, duration);
    }
    public void ScaleWithRespectToBoundsCenter(Vector3 scale, float duration = 0.2f)
    {
        Vector3 boundsCenter = localBounds.center;

        // Calculate the new position after scaling with respect to bounds center
        Vector3 newPosition = boundsCenter - (boundsCenter - transform.position) * scale.x;

        // Set the new position and scale using ScaleServerRpc
        transform.position = newPosition;
        ScaleServerRpc(scale, duration);
    }
    
    public void FitBoundsToBox(Vector3 boxDimensions, float duration = 0.2f)
    {
        float scaleFactor = Mathf.Min(boxDimensions.x / localBounds.size.x, boxDimensions.y / localBounds.size.y, boxDimensions.z / localBounds.size.z);
        ScaleServerRpc(scaleFactor*transform.localScale, duration);
    }

    public void FitIntoBoundsGlobal(Bounds bounds)
    {
        
    }
    
    #endregion
    
    #region Tooltip
    public Tooltip Tooltip
    {
        get
        {
            return tooltip;
        }
        set
        {
            tooltip = value;
            if (NodeInfo != null)
            {
                tooltip.Caption = NodeInfo.Name;
            }
        }
    }
    public bool TooltipShowing
    {
        get => tooltipShowing.Value;
        set
        {
            SetTooltipShowingServerRpc(value);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    private void SetTooltipShowingServerRpc(bool value)
    {
        tooltipShowing.Value = value;
    }
    protected void OnTooltipShowingValueChanged(bool oldValue, bool newValue)
    {
        if (Tooltip != null)
        {
            Tooltip.gameObject.SetActive(newValue);
        }
    }
    
    #endregion
    
    #region Apperance
    public virtual int OutlineColorIdx
    {
        set
        {
            foreach (ModelNode child in children)
            {
                child.OutlineColorIdx = value;
            }
        }
    }
    public virtual bool IsOutlined 
    {
        get
        {
            foreach (ModelNode child in children)
            {
                if (!child.IsOutlined)
                {
                    return false;
                }
            }
            return true;
        }
        set
        {
            foreach (ModelNode child in children)
            {
                child.IsOutlined = value;
            }
        }
    }
    public virtual bool IsHidden
    {
        get
        {
            foreach (ModelNode child in children)
            {
                if (!child.IsHidden)
                {
                    return false;
                }
            }
            return true;
        }
        set
        {
            foreach (ModelNode child in children)
            {
                child.IsHidden = value;
            }
        }
    }
    public virtual bool IsFaded
    {
        get
        {
            foreach (ModelNode child in children)
            {
                if (!child.IsFaded)
                {
                    return false;
                }
            }
            return true;
        }
        set
        {
            foreach (ModelNode child in children)
            {
                child.IsFaded = value;
            }
        }
    }
    
    public virtual float FadeAlpha
    {
        get
        {
            return 1.0f;
        }
        set
        {
            foreach (ModelNode child in children)
            {
                child.FadeAlpha = value;
            };
        }
    }
    public void ToggleHidden()
    {
        IsHidden = !IsHidden;
    }
    public void ToggleFaded()
    {
        IsFaded = !IsFaded;
    }
    public void ToggleOutline()
    {
        IsOutlined = !IsOutlined;
    }
    public NodeVisibilityState VisibilityState
    {
        get
        {
            if (IsHidden){
                return NodeVisibilityState.Hidden;
            }
            else if (IsFaded)
            {
                return NodeVisibilityState.Faded;
            }
            else
            {
                return NodeVisibilityState.Visible;
            }
        }
    }
    
    #endregion
    
}

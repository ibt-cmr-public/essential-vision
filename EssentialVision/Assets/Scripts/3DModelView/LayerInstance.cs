using System;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Profiling;
using Debug = UnityEngine.Debug;

public class LayerInstance : ModelNode
{
    
    public Action OnMaterialChanged;
    #region Networked properties
    //Networked properties
    //Temporary
    public NetworkVariable<bool> isHidden = new NetworkVariable<bool>(false);
    public NetworkVariable<bool> isFaded = new NetworkVariable<bool>(false);
    private NetworkVariable<float> fadeAlpha = new NetworkVariable<float>(0.5f);
    private NetworkVariable<bool> isOutlined = new NetworkVariable<bool>(false);
    private NetworkVariable<int> outlineColorIdx = new NetworkVariable<int>(0);
    
    //Permanent
    private NetworkVariable<Color> color = new NetworkVariable<Color>(Color.white);
    private NetworkVariable<float> baseOpacity = new NetworkVariable<float>(1f);
    private NetworkVariable<float> metallic = new NetworkVariable<float>(0.89f);
    private NetworkVariable<float> smoothness = new NetworkVariable<float>(0.5f);
    private NetworkVariable<NodeType> nodeType = new NetworkVariable<NodeType>(NodeType.Mesh);
    
    public override bool IsOutlined
    {
        get => isOutlined.Value;
        set
        {
            base.IsOutlined = value;
            SetIsOutlinedServerRpc(value);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    private void SetIsOutlinedServerRpc(bool value)
    {
        isOutlined.Value = value;
    }
    private void OnIsOutlinedValueChanged(bool oldValue, bool newValue)
    {
        if (outline != null)
        {
            outline.enabled = newValue;
        }
    }

    public override int OutlineColorIdx
    {
        set
        {
            base.OutlineColorIdx = value;
            SetOutlineColorIdxServerRpc(value);
        }
    }
    private void OnOutlineColorIdxChanged(int oldValue, int newValue)
    {
        if (outline != null)
        {
            outline.color = newValue;
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void SetOutlineColorIdxServerRpc(int value)
    {
        outlineColorIdx.Value = value;
    }

    public override bool IsHidden
    {
        get
        {
            if (!isHidden.Value) return false;
            foreach (ModelNode child in children)
            {
                if (!child.IsHidden)
                {
                    return false;
                }
            }
            return true;
        }
        set
        {
            base.IsHidden = value;
            SetIsHiddenServerRpc(value);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    private void SetIsHiddenServerRpc(bool value)
    {
        Profiler.BeginSample("SetIsHiddenServerRpc");
        isHidden.Value = value;
        Profiler.EndSample();
    }
    private void OnIsHiddenValueChanged(bool oldValue, bool newValue)
    {
        foreach (Renderer renderer in GetComponents<Renderer>())
        {
            renderer.enabled = !newValue;
        }
    }
    
    public override bool IsFaded
    {
        get
        {
            if (!isFaded.Value) return false;
            foreach (ModelNode child in children)
            {
                if (!child.IsFaded)
                {
                    return false;
                }
            }
            return true;
        }
        set
        {
            base.IsFaded = value;
            SetIsFadedServerRpc(value);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    private void SetIsFadedServerRpc(bool value)
    {
        isFaded.Value = value;
    }
    private void OnIsFadedValueChanged(bool oldValue, bool newValue)
    {
        UpdateMaterial();
    }


    public override float FadeAlpha
    {
        get
        {
            return fadeAlpha.Value;
        }
        set
        {
            base.FadeAlpha = value;
            SetFadeAlphaServerRpc(value);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetFadeAlphaServerRpc(float value)
    {
        fadeAlpha.Value = value;
    }
    public void OnFadeAlphaValueChanged(float oldValue, float newValue)
    {
        UpdateMaterial();
    }

    public Color Color
    {
        get
        {
            return color.Value;
        }
        set
        {
            SetColorServerRpc(value);
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void SetColorServerRpc(Color value)
    {
        color.Value = value;
    }
    
    private void OnColorChanged(Color oldValue, Color newValue)
    {
        UpdateMaterial();
    }
    
    public float BaseOpacity
    {
        get
        {
            return baseOpacity.Value;
        }
        set
        {
            SetBaseOpacityServerRpc(value);
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void SetBaseOpacityServerRpc(float value)
    {
        baseOpacity.Value = value;
    }
    
    private void OnBaseOpacityChanged(float oldValue, float newValue)
    {
        UpdateMaterial();
    }
    
    public float Metallic
    {
        get
        {
            return metallic.Value;
        }
        set
        {
            SetMetallicServerRpc(value);
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void SetMetallicServerRpc(float value)
    {
        metallic.Value = value;
    }
    
    private void OnMetallicChanged(float oldValue, float newValue)
    {
        UpdateMaterial();
    }
    
    public float Smoothness
    {
        get
        {
            return smoothness.Value;
        }
        set
        {
            SetSmoothnessServerRpc(value);
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void SetSmoothnessServerRpc(float value)
    {
        smoothness.Value = value;
    }
    
    private void OnSmoothnessChanged(float oldValue, float newValue)
    {
        UpdateMaterial();
    }

    public NodeType NodeType
    {
        get
        {
            return nodeType.Value;
        }
        set
        {
            SetNodeTypeServerRpc(value);
        }
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void SetNodeTypeServerRpc(NodeType value)
    {
        nodeType.Value = value;
    }
    
    private void OnNodeTypeChanged(NodeType oldValue, NodeType newValue)
    {
        UpdateMaterial();
    }
    private void UpdateMaterial()
    {
        Material currentMaterial = new Material(Shader.Find(ApplicationDefaults.DefaultShaders[nodeType.Value]));
        currentMaterial.color = Color;
        currentMaterial.SetFloat("_Metallic", Metallic);
        currentMaterial.SetFloat("_Glossiness", Smoothness);
        currentMaterial.SetFloat("_Fade", BaseOpacity);
        float opacity = IsFaded ? FadeAlpha*BaseOpacity : BaseOpacity;
        if (opacity < 1)
        {
            currentMaterial.SetFloat("_Mode", 2);
            currentMaterial.SetFloat("_Fade", opacity);
            currentMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            currentMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            currentMaterial.SetInt("_ZWrite", 0);
            currentMaterial.DisableKeyword("_ALPHATEST_ON");
            currentMaterial.EnableKeyword("_ALPHABLEND_ON");
            currentMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        }
        
        foreach (Renderer renderer in Renderers)
        {
            Material[] materials = new Material[renderer.materials.Length];

            for (int i = 0; i < materials.Length; i++)
            {
                materials[i] = currentMaterial;
            }

            renderer.sharedMaterials = materials;
        }
    }
    
    #endregion
    
    //Layer Info
    public override ModelNodeScriptableObject NodeInfo
    {
        get
        {
            LayerScriptableObject layerInfo = ScriptableObject.CreateInstance<LayerScriptableObject>();
            layerInfo.Name = Name;
            layerInfo.Description = Description;
            layerInfo.Color = Color;
            layerInfo.Smoothness = Smoothness;
            layerInfo.Metallic = Metallic;
            layerInfo.BaseOpacity = BaseOpacity;
            return layerInfo;
        }
    }
    
    public LayerScriptableObject LayerInfo
    {
        get
        {
            return (LayerScriptableObject) NodeInfo;
        }
    }

    public override async Task SetNodeInfoAsync(ModelNodeScriptableObject value)
    {
        if (value is not LayerScriptableObject)
        {
            Debug.LogError("LayerInstance.NodeInfo must be a LayerScriptableObject");
        }
        else
        {
            await SetNodeInfoAsync((LayerScriptableObject) value);
        }
    }
    
    public async Task SetNodeInfoAsync(LayerScriptableObject value)
    {
        
        await base.SetNodeInfoAsync(value);
        Color = value.Color;
        Smoothness = value.Smoothness;
        Metallic = value.Metallic;
        BaseOpacity = value.BaseOpacity;
        
        Mesh mesh = await LayerInfo.GetMesh();
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        if (meshFilter != null)
        {
            meshFilter.mesh = mesh;
        }
        
        foreach (Renderer renderer in Renderers)
        {
            if (renderer is SkinnedMeshRenderer skinnedMeshRenderer){
                skinnedMeshRenderer.sharedMesh = mesh;
                skinnedMeshRenderer.localBounds = mesh.bounds;
            }
        }
    }
    
    //Mesh
    public void SetLocalMesh(Mesh mesh)
    {
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        if (meshFilter != null)
        {
            meshFilter.mesh = mesh;
        }
        
        foreach (Renderer renderer in Renderers)
        {
            if (renderer is SkinnedMeshRenderer skinnedMeshRenderer){
                skinnedMeshRenderer.sharedMesh = mesh;
                skinnedMeshRenderer.localBounds = mesh.bounds;
            }
        }
    }
    
    [Rpc]
    public void SetMeshServerRpc(Guid modelID, Guid nodeID)
    {
        SetMeshClientRpc(modelID, nodeID);
    }
    
    [Rpc]
    public void SetMeshClientRpc(Guid modelID, Guid nodeID)
    {
        if (modelID == ModelID && nodeID == NodeID)
        {
            Mesh mesh = LayerInfo.GetMesh().Result;
            SetLocalMesh(mesh);
        }
    }
    
    
    
    private cakeslice.Outline outline
    {
        get
        {
            return gameObject.GetComponent<cakeslice.Outline>();
        }
    }

    
    //Initialization and spawning
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        isHidden.OnValueChanged += OnIsHiddenValueChanged;
        isFaded.OnValueChanged += OnIsFadedValueChanged;
        isOutlined.OnValueChanged += OnIsOutlinedValueChanged;
        outlineColorIdx.OnValueChanged += OnOutlineColorIdxChanged;
        fadeAlpha.OnValueChanged += OnFadeAlphaValueChanged;
        color.OnValueChanged += OnColorChanged;
        baseOpacity.OnValueChanged += OnBaseOpacityChanged;
        metallic.OnValueChanged += OnMetallicChanged;
        smoothness.OnValueChanged += OnSmoothnessChanged;
        nodeType.OnValueChanged += OnNodeTypeChanged;
    }

    //Setting layer state
    public LayerState State
    {
        get
        {
            LayerState state = new LayerState();
            state.isHidden = IsHidden;
            state.isOutlined = IsOutlined;
            state.isFaded = IsFaded;
            state.tooltipShowing = TooltipShowing;
            state.localTransform = localTransform;
            return state;
        }
    }


    public LayerState DefaultState
    {
        get
        {
            LayerState state = new LayerState();
            state.localTransform = InitialLocalTransform;
            return state;
        }
    }
    
    public override void SetNodeState(NodeState state, NodeStateTransition stateTransition = null)
    {
        if (state is LayerState && stateTransition is LayerStateTransition)
        {
            SetNodeState((LayerState) state, (LayerStateTransition) stateTransition);
        }
        else
        {
            Debug.LogError("LayerInstance.SetNodeState must be called with a LayerState and LayerStateTransition");
        }
    }
    
    public void SetNodeState(LayerState state, LayerStateTransition stateTransition = null)
    {
        if (stateTransition == null)
        {
            stateTransition = new LayerStateTransition();
        }
        base.SetNodeState(state, stateTransition);
        if (stateTransition.updateHidden) SetIsHiddenServerRpc(state.isHidden);
        if (stateTransition.updateFaded) SetIsFadedServerRpc(state.isFaded);
        if (stateTransition.updateOutline) SetIsOutlinedServerRpc(state.isOutlined);
        

    }
    

    //Networked properties
    

}

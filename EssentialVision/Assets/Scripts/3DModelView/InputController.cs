using System;
using MixedReality.Toolkit.SpatialManipulation;
using UnityEngine;

public class InputController : MonoBehaviour
{

    [SerializeField]
    public NodeController nodeController;
    [SerializeField]
    public Transform modelViewTransform;
    private bool individualMovementActive = false;
    

    public bool IndividualMovementActive
    {
        get
        {
            return individualMovementActive;
        }
        set
        {
            if (individualMovementActive != value)
            {
                foreach (ModelNode node in nodeController.Nodes)
                {
                    ObjectManipulator objectManipulator = node.GetComponent<ObjectManipulator>();
                    if (objectManipulator == null) continue;
                    objectManipulator.HostTransform = value ? node.transform : modelViewTransform;
                }
                individualMovementActive = value;
                SetHostTransformsForHoveredNode(nodeController.HoveredNodeGuid);
            }
        }
    }

    public void SetHostTransformsForNodeSelection(Guid nodeGuid)
    {
        ModelNode selectedNode = nodeController.Node(nodeGuid);
        if (individualMovementActive && nodeController.GroupedSelection)
        {
            foreach (ModelNode node in nodeController.Nodes)
            {
                ObjectManipulator objectManipulator = node.GetComponent<ObjectManipulator>();
                if (objectManipulator == null) continue;
                if (selectedNode == node)
                {
                    objectManipulator.HostTransform = node.transform;
                }
                else if (selectedNode.IsParentOf(node))
                {
                    objectManipulator.HostTransform = node.ParentOnLevel(selectedNode.Level + 1).transform;
                }
                else
                {
                    ModelNode commonParent = selectedNode.ClosestCommonParent(node);
                    if (commonParent.IsRoot)
                    {
                       objectManipulator.HostTransform = node.ParentOnLevel(1).transform;
                    }
                    else
                    {
                        objectManipulator.HostTransform = commonParent.transform;
                    } 
                }
            }
        }
    }

    public void SetHostTransformsForHoveredNode(Guid hoveredNodeGuid)
    {
        ModelNode hoveredNode = nodeController.Node(hoveredNodeGuid);
        if (hoveredNode == null)
        {
            return;
        }
        if (individualMovementActive)
        {
            foreach (ModelNode node in nodeController.Nodes)
            {
                ObjectManipulator objectManipulator = node.GetComponent<ObjectManipulator>();
                if (objectManipulator == null) continue;
                if (hoveredNode.IsParentOf(node) || hoveredNode == node)
                {
                    objectManipulator.HostTransform = hoveredNode.transform;
                }
            }
        }
    }

    public void Start()
    {
        nodeController.NodeHovered += SetHostTransformsForHoveredNode;
    }
}


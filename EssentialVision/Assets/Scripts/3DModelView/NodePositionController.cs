using System;
using System.Collections.Generic;
using DG.Tweening;
using MixedReality.Toolkit.SpatialManipulation;
using UnityEngine;

public class NodePositionController : MonoBehaviour
{
    public GameObject nodeContainer;
    [SerializeField] private NodeController nodeController;
    [SerializeField] private float maxModelSize = 0.5f;
    public event Action<Vector3> BoundsSizeChanged;


    public void FitInBounds()
    {
        ModelNode rootNode = nodeController.RootNode;

        if (rootNode == null)
        {
            return;
        }
        
        Vector3 boundsSize = rootNode.InitialLocalBounds.size;
        Vector3 shift = rootNode.InitialLocalBounds.center;
        float maxSize = Mathf.Max(new float[] {boundsSize.x, boundsSize.z });
        float scale = 1.0f;
        if (maxSize > Mathf.Epsilon)
        {
            scale = maxModelSize / maxSize;
        }
        
        rootNode.transform.localScale = new Vector3(scale, scale, scale);
        rootNode.transform.localPosition = - scale * shift;
        rootNode.InitialLocalTransform = rootNode.localTransform;
        BoundsSizeChanged?.Invoke(scale*boundsSize);
        
        // Refresh the interaction manager for object manipulation
        BoundsControl boundsControl = nodeContainer.GetComponent<BoundsControl>();
        if(boundsControl != null)
        {
            boundsControl.IncludeInactiveObjects = true;
            boundsControl.RecomputeBounds();
        }
    }
    
    public void SetDefaultTransforms()
    {
        foreach (ModelNode node in nodeController.Nodes)
        {
            node.SetLocalTransformServerRpc(node.InitialLocalTransform, 0.2f);
        }

    }

    public void SetExplodeTransforms()
    {
        foreach (ModelNode node in nodeController.Nodes)
        {
            if (node.IsRoot)
            {
                continue;
            }
            
            // Apply translation and rotation to child's center
            LocalTransform localTransform = node.InitialLocalTransform;
            Matrix4x4 localTransformMatrix = Matrix4x4.TRS(node.InitialLocalTransform.localPosition,
                node.InitialLocalTransform.localRotation, node.InitialLocalTransform.localScale);
            Bounds childBoundsInParentSpace = TransformBounds(node.InitialLocalBounds, localTransformMatrix);
            Vector3 childCenterToParentCenter = childBoundsInParentSpace.center - node.Parent.InitialLocalBounds.center;
            localTransform.localPosition = node.InitialLocalTransform.localPosition + 1.5f * childCenterToParentCenter;
            node.SetLocalTransformServerRpc(localTransform, 0.2f);
        }

    }

    private Bounds TransformBounds(Bounds bounds, Matrix4x4 matrix)
    {
        // Extract the center and extents of the original bounds
        Vector3 originalCenter = bounds.center;
        Vector3 originalExtents = bounds.extents;

        // Calculate the rotated corners of the bounds in the new coordinate system
        Vector3[] rotatedCorners = new Vector3[8];
        for (int i = 0; i < 8; i++)
        {
            Vector3 corner = originalCenter + Vector3.Scale(originalExtents, GetCornerPoint(i));
            rotatedCorners[i] = matrix.MultiplyPoint(corner);
        }

        // Find the tightest bounds within the new coordinate system
        Bounds rotatedBounds = new Bounds(rotatedCorners[0], Vector3.zero);
        for (int i = 1; i < 8; i++)
        {
            rotatedBounds.Encapsulate(rotatedCorners[i]);
        }

        return rotatedBounds;
    }

// Helper method to get the corner points of the bounds
    private Vector3 GetCornerPoint(int index)
    {
        return new Vector3((index & 1) == 0 ? -1 : 1, (index & 2) == 0 ? -1 : 1, (index & 4) == 0 ? -1 : 1);
    }


    public void CompareNodesGrid(List<Guid> nodeIDs, int numRows = -1, int numCols = -1, float viewingSquareLength = 0.6f)
    {
        //Get largest size of node
        List<ModelNode> nodes = new List<ModelNode>();
        foreach(Guid nodeID in nodeIDs)
        {
            nodes.Add(nodeController.Node(nodeID));
        }
        
        Sequence sequence = DOTween.Sequence();
        //Determine max node size
        float maxNodeSize = 0;
        foreach (ModelNode node in nodes)
        {
            Vector3 size = Vector3.Scale(node.InitialLocalBounds.size, node.transform.lossyScale);
            float currentNodeSize = Mathf.Max(size.x, size.y, size.z);
            if (currentNodeSize > maxNodeSize)
            {
                maxNodeSize = currentNodeSize;
            }
        }

        if (numCols == -1)
        {
            numCols = Mathf.CeilToInt(Mathf.Sqrt(nodeIDs.Count));
        }
        if (numRows == -1)
        {
            numRows = Mathf.CeilToInt((float)nodeIDs.Count / numCols);
        }
        float nodeSizeX = viewingSquareLength / numCols;
        float nodeSizeY = viewingSquareLength / numRows;
        float rescaling = nodeSizeX / maxNodeSize;
        sequence.Insert(0,nodeContainer.transform.DOScale(rescaling * nodeContainer.transform.localScale.x, 0.5f));
        
        //Position nodes
        int i = 0;
        foreach (ModelNode node in nodes)
        {
            float localPositionX = (i % numCols) * nodeSizeX - viewingSquareLength / 2 + nodeSizeX / 2;
            float localPositionY = - (i / numCols) * nodeSizeY + viewingSquareLength / 2 - nodeSizeX / 2;
            Vector3 localPosition = new Vector3(localPositionX, localPositionY, 0);
            Vector3 boundsCenterPositionInGlobalSpace = nodeContainer.transform.parent.TransformPoint(localPosition);
            Vector3 nodePositionInGlobalSpace = boundsCenterPositionInGlobalSpace - Vector3.Scale(node.InitialLocalBounds.center,node.transform.lossyScale)*rescaling;

            foreach (ModelNode childNode in node.Subnodes)
            {
                if (!nodeIDs.Contains(childNode.Guid))
                {
                    sequence.Insert(0,childNode.transform.DOLocalMove(childNode.InitialLocalTransform.localPosition, 0.5f));
                }
            }
            sequence.Insert(0,node.transform.DOMove(nodePositionInGlobalSpace, 0.5f));
            i++;
        }
    }
    public void CompareNodesRadial(List<Guid> nodeIDs, float viewSphereRadius = 0.3f)
    {
        //Get largest size of node
        List<ModelNode> nodes = new List<ModelNode>();
        foreach(Guid nodeID in nodeIDs)
        {
            nodes.Add(nodeController.Node(nodeID));
        }

        Sequence sequence = DOTween.Sequence();
        //Set size of model
        float maxNodeSize = 0;
        foreach (ModelNode node in nodes)
        {
            Vector3 size = Vector3.Scale(node.InitialLocalBounds.size, node.transform.lossyScale);
            float currentNodeSize = Mathf.Max(size.x, size.y, size.z);
            if (currentNodeSize > maxNodeSize)
            {
                maxNodeSize = currentNodeSize;
            }
        }
        int numPolygonSides = nodeIDs.Count <= 4 ? nodeIDs.Count : nodeIDs.Count - 1;
        float nodeSize = 2f * viewSphereRadius * (float)Math.Sin(Mathf.PI / numPolygonSides);
        float rescaling = nodeSize / maxNodeSize;
        sequence.Insert(0,nodeContainer.transform.DOScale(rescaling * nodeContainer.transform.localScale.x, 0.5f));
        
        //Position nodes
        int i = 0;
        foreach (ModelNode node in nodes)
        {
            Vector3 localPosition = Vector3.zero;
            if (i==0 && nodeIDs.Count > 4)
            {
                localPosition = nodeContainer.transform.localPosition;
            }
            else
            {
                float angleRadians = 360f * i / numPolygonSides;
                Quaternion rotation = Quaternion.Euler(0, 0, angleRadians);
                localPosition = rotation * Vector3.up * viewSphereRadius + nodeContainer.transform.localPosition;
            }
            Vector3 boundsCenterPositionInGlobalSpace = nodeContainer.transform.parent.TransformPoint(localPosition);
            Vector3 nodePositionInGlobalSpace = boundsCenterPositionInGlobalSpace - Vector3.Scale(node.InitialLocalBounds.center,node.transform.lossyScale)*rescaling;

            foreach (ModelNode childNode in node.Subnodes)
            {
                if (!nodeIDs.Contains(childNode.Guid))
                {
                    sequence.Insert(0,childNode.transform.DOLocalMove(childNode.InitialLocalTransform.localPosition, 0.5f));
                }
            }
            sequence.Insert(0,node.transform.DOMove(nodePositionInGlobalSpace, 0.5f));
            i++;
        }

        sequence.Play();
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using Unity.XR.CoreUtils;
using UnityEngine;


//Class that manages instantiated layers as well as hover and selection states
public class NodeController : NetworkBehaviour
{
    
    // Indices of the selected and currently hovered layers
    private NetworkVariable<FixedString64Bytes> selectedNodeGuid = new NetworkVariable<FixedString64Bytes>(Guid.Empty.ToString());
    private NetworkVariable<FixedString64Bytes> hoveredNodeGuid  = new NetworkVariable<FixedString64Bytes>(Guid.Empty.ToString());
    private NetworkVariable<bool> groupedSelection = new NetworkVariable<bool>(true);
    
    public event Action<Guid> NodeSelected;
    public event Action<Guid> NodeDeselected;
    public event Action<Guid> NodeHovered;
    public event Action<Guid> NodeUnhovered;
    public event Action<ModelNode> NodeAdded;
    public event Action<ModelNode> NodeRemoved;
    public event Action<Vector3> BoundsSizeChanged;


    // Reference to the model root node
   [SerializeField] private ModelNode rootNode = null;
    
    //Maximum model size used when initially displaying the model TODO: should be part of model SO
    private float maxModelSize = 0.3f;
    
    
    public void Awake()
    {
        if (!IsServer)
        {
            return;
        }
        SelectedNodeGuid = Guid.Empty;
        HoveredNodeGuid = Guid.Empty;
        GroupedSelection = true;
    }
    
    public override void OnNetworkSpawn()
    {
        selectedNodeGuid.OnValueChanged += OnSelectedNodeGuidValueChanged;
        hoveredNodeGuid.OnValueChanged += OnHoveredNodeGuidValueChanged;
    }
    
    //Reset the node controller
    public void Reset()
    {
        ResetServerRpc();
    }
    
    [Rpc(SendTo.Server)]
    public void ResetServerRpc()
    {
        SelectedNodeGuid = Guid.Empty;
        HoveredNodeGuid = Guid.Empty;
        GroupedSelection = true;
        ResetClientRpc();
    }
    
    [Rpc(SendTo.Everyone)]
    public void ResetClientRpc()
    {
        RootNode = null;
    }
    

    // Accessing nodes

    public ModelNode SpawnNode(ModelNodeScriptableObject modelNode, Guid parentGuid)
    {
        
    }

    public SpawnNodeServerRpc()
    {
        
    }
    
    public void AddNode(ModelNode node, Guid parentGuid)
    {
        ModelNode parent = Node(parentGuid);
        if (parent == null)
        {
            Debug.LogError("Cannot add a child node to a parent that does not exist.");
        }
        else
        {
            parent.AddChild(node);
            int i = NumNodes;
            foreach(ModelNode childNode in node.Subnodes) {
                if (childNode is LayerInstance &&
                    childNode.GetComponent<IHoverAndSelectable>() is { } hoverAndSelectableComponent)
                {
                    //Hook up events
                    hoverAndSelectableComponent.HoverEntered += () => OnLayerHoverEnter(childNode);
                    hoverAndSelectableComponent.HoverExited += OnLayerHoverExit;
                    hoverAndSelectableComponent.Selected += () => OnLayerSelected(childNode);
                }
                childNode.Idx =  i;
                NodeAdded?.Invoke(childNode);
                i++;
            }
        }
    }
    public ModelNode RootNode
    {
        get { return rootNode; }
        set
        {
            if (rootNode != value)
            {
                if (rootNode != null)
                {
                    foreach (ModelNode childNode in rootNode.SubnodesBottomup)
                    {
                        if (childNode is LayerInstance &&
                            childNode.GetComponent<IHoverAndSelectable>() is { } hoverAndSelectableComponent)
                        {
                            //Unhook events
                            hoverAndSelectableComponent.HoverEntered -= () => OnLayerHoverEnter(childNode);
                            hoverAndSelectableComponent.HoverExited -= OnLayerHoverExit;
                            hoverAndSelectableComponent.Selected -= () => OnLayerSelected(childNode);
                        }
                        NodeRemoved?.Invoke(childNode);
                        
                    }
                }
                rootNode = value;
                if (value != null)
                {
                    int i = 0;
                    foreach(ModelNode childNode in rootNode.Subnodes) {
                        if (childNode is LayerInstance &&
                            childNode.GetComponent<IHoverAndSelectable>() is { } hoverAndSelectableComponent)
                        {
                            //Hook up events
                            hoverAndSelectableComponent.HoverEntered += () => OnLayerHoverEnter(childNode);
                            hoverAndSelectableComponent.HoverExited += OnLayerHoverExit;
                            hoverAndSelectableComponent.Selected += () => OnLayerSelected(childNode);
                        }
                        childNode.Idx =  i;
                        NodeAdded?.Invoke(childNode);
                        i++;
                    }
                }
            }
        }
    }
    
    public IEnumerable<ModelNode> RootNodes
    {
        get
        {
            return Nodes.Where(node => node.IsRoot);
        }
    }

    public ModelNode Node(Guid nodeGuid)
    {
        foreach (ModelNode node in Nodes)
        {
            if (node.Guid == nodeGuid)
            {
                return node;
            }
        }
        return null;
    }

    public IEnumerable<ModelNode> Nodes
    {
        get
        {
            if (RootNode != null)
            {
                return RootNode.Subnodes;
            }
            else
            {
                return new List<ModelNode>();
            }
        }
    }
    
    public IEnumerable<ModelNode> NodesBottomup
    {
        get
        {
            if (RootNode != null)
            {
                return RootNode.SubnodesBottomup;
            }
            else
            {
                return new List<ModelNode>();
            }
        }
    }
    
    public IEnumerable<LayerInstance> Layers
    {
        get
        {
            return Nodes.Where(node => node is LayerInstance).Cast<LayerInstance>();
        }
    }
    
    public int NumNodes
    {
        get
        {
            return RootNode == null? 0 : RootNode.NumSubnodes;
        }
    }
    
    //Networked properties
    //Selected node
    public Guid SelectedNodeGuid
    {
        get { return new Guid(selectedNodeGuid.Value.ToString()); }
        set
        {
            SetSelectedNodeGuidServerRpc(value);
        }
    }
    
    [ServerRpc (RequireOwnership = false)]
    private void SetSelectedNodeGuidServerRpc(Guid value)
    {
        if (selectedNodeGuid.Value != value.ToString())
        {
            if (value == Guid.Empty || Node(value) != null)
            {
                selectedNodeGuid.Value = value.ToString();
            }
            else
            {
                Debug.LogWarning($"Could not select layer with layer index {value} from list of instantiated nodes.");
            }
        }
    }
    private void OnSelectedNodeGuidValueChanged(FixedString64Bytes oldValue, FixedString64Bytes newValue)
    {
        Guid oldGuid = new Guid(oldValue.ToString());
        Guid newGuid = new Guid(newValue.ToString());
        if (oldGuid != Guid.Empty)
        {
            NodeDeselected?.Invoke(oldGuid);
        }
        if (newGuid != Guid.Empty)
        {
            NodeSelected?.Invoke(newGuid);
        }
    }
    
    public ModelNode SelectedNode
    {
        get
        {
            if (SelectedNodeGuid == Guid.Empty)
            {
                return null;
            }
            return Node(SelectedNodeGuid);
        }
    }
    
    public void DeselectNode() 
    {
        SelectedNodeGuid = Guid.Empty;
    }
    
    public void SelectRootNode()
    {
        SelectedNodeGuid = rootNode.Guid;
    }

    // Hovered node
    public Guid HoveredNodeGuid
    {
        get { return new Guid(hoveredNodeGuid.Value.ToString()); }
        set
        {
            SetHoveredNodeIdxServerRpc(value);
        }
    }
    [ServerRpc (RequireOwnership = false)]
    private void SetHoveredNodeIdxServerRpc(Guid value)
    {
        if (hoveredNodeGuid.Value != value.ToString())
        {
            if (value == Guid.Empty || Node(value) != null)
            {
                hoveredNodeGuid.Value = value.ToString();
            }
            else
            {
                Debug.LogWarning($"Could not hover layer with layer index {value} from list of instantiated nodes.");
            }
        }
    }
    private void OnHoveredNodeGuidValueChanged(FixedString64Bytes oldValue, FixedString64Bytes newValue)
    {
        Guid oldGuid = new Guid(oldValue.ToString());
        Guid newGuid = new Guid(newValue.ToString());
        if (oldGuid != Guid.Empty)
        {
            NodeUnhovered?.Invoke(oldGuid);
        }
        if (newGuid != Guid.Empty)
        {
            NodeHovered?.Invoke(newGuid);
        }
    }
    public ModelNode HoveredNode
    {
        get { return Node(HoveredNodeGuid); }
    }
    
    public bool GroupedSelection
    {
        get { return groupedSelection.Value; }
        set
        {
            SetGroupedSelectionServerRpc(value);
        }
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SetGroupedSelectionServerRpc(bool value)
    {
        groupedSelection.Value = value;
    }
    
    //Ray hover and selection events
    public void OnLayerHoverEnter(ModelNode node)
    {
        if (node == null)
        {
            Debug.LogError("Hovered object does not have a ModelNode component");
        }
        if (node == HoveredNode)
        {
            return;
        }
        if (groupedSelection.Value)
        {
            if (SelectedNode == node)
            {
                HoveredNodeGuid = node.Guid;
            }
            else if (SelectedNode.IsParentOf(node))
            {
                HoveredNodeGuid = node.ParentOnLevel(SelectedNode.Level + 1).Guid;
            }
            else
            {
                ModelNode commonParent = SelectedNode.ClosestCommonParent(node);
                if (commonParent == rootNode)
                {
                    HoveredNodeGuid = node.ParentOnLevel(1).Guid;
                }
                else
                {
                    HoveredNodeGuid = commonParent.Guid;
                } 
            }
        }
        else
        {
            HoveredNodeGuid = node.Guid;
        }
    }
    
    public void OnLayerHoverExit()
    {
        HoveredNodeGuid = Guid.Empty;
    }
    
    public void OnLayerSelected(ModelNode node)
    {
        if (node == null)
        {
            Debug.LogError("Selected object does not have a ModelNode component");
        }
        if (node == SelectedNode)
        {
            return;
        }
        if (groupedSelection.Value)
        {
            ModelNode commonParent = SelectedNode.ClosestCommonParent(node);
            if (commonParent == SelectedNode || commonParent == rootNode)
            {
                SelectedNodeGuid = node.ParentOnLevel(commonParent.Level + 1).Guid;
            }
            else
            {
                SelectedNodeGuid = commonParent.Guid;
            } 
        }
        else
        {
            SelectedNodeGuid = node.Guid;
        }
    }
    


    public void RemoveNode(int nodeIdx)
    {
        throw new NotImplementedException();
    }
    
  
    
    //Layer states
    public void SetNodeStates(Dictionary<Guid, NodeState> states, Dictionary<Guid, NodeStateTransition> stateUpdates = null)
    {
        stateUpdates ??= new Dictionary<Guid, NodeStateTransition>();
        foreach (Guid nodeGuid in states.Keys)
        {
            ModelNode node = Node(nodeGuid);
            if (node != null)
            {
                node.SetNodeState(states[nodeGuid], stateUpdates.ContainsKey(nodeGuid)?stateUpdates[nodeGuid]:new NodeStateTransition());
            }
        }
    }
    
    public Dictionary<Guid,NodeState> NodeStates
    {
        get
        {
            Dictionary<Guid,NodeState> states = new Dictionary<Guid, NodeState>();
            foreach (ModelNode node in Nodes)
            {
                states.Add(node.Guid, node.State);
            }
            return states;
        }
    }

    public Dictionary<Guid,NodeState> DefaultNodeStates
    {
        get
        {
            Dictionary<Guid,NodeState> states = new Dictionary<Guid, NodeState>();
            foreach (ModelNode node in Nodes)
            {
                states.Add(node.Guid, node.DefaultState);
            }
            return states;
        }
    }
    
    

}
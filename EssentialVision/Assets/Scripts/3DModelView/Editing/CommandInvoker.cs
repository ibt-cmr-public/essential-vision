using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker<T> where T : class
{
    public Action OnUndo;
    public Action OnRedo;
    public Action OnClear;
    public Action OnCommandAdded;
    
    private T _receiver;
    private List<ICommand<T>> _commands = new List<ICommand<T>>();
    private int _currentCommandIndex = -1;
    
    public CommandInvoker(T receiver)
    {
        _receiver = receiver;
    }

    public void AddCommand(ICommand<T> command)
    {
        if (_currentCommandIndex < _commands.Count - 1)
        {
            _commands.RemoveRange(_currentCommandIndex + 1, _commands.Count - _currentCommandIndex - 1);
        }
        _commands.Add(command);
        _currentCommandIndex++;
        OnCommandAdded?.Invoke();
    }

    public void Undo()
    {
        if (_currentCommandIndex >= 0)
        {
            _commands[_currentCommandIndex].Undo(_receiver);
            _currentCommandIndex--;
            OnUndo?.Invoke();
        }
    }

    public void Redo()
    {
        if (_currentCommandIndex < _commands.Count - 1)
        {
            _currentCommandIndex++;
            _commands[_currentCommandIndex].Execute(_receiver);
            OnRedo?.Invoke();
        }
    }

    public void Clear()
    {
        _commands.Clear();
        _currentCommandIndex = -1;
        OnClear?.Invoke();
    }

    public bool CanUndo
    {
        get {
            return _currentCommandIndex >= 0;
        }
    }

    public bool CanRedo
    {
        get {
            return _currentCommandIndex < _commands.Count - 1;
        }
    }
}

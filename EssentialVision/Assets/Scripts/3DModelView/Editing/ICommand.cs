using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Input;
using UnityEngine;
using UnityEngine.SocialPlatforms;


public interface ICommand<T>
{
    public void Execute(T nodeInstace);
    public void Undo(T nodeInstance);
    public void Redo(T nodeInstance);
}
  

#region Layer Commands
public class ChangeNodeColorCommand: ICommand<LayerInstance>
{
    
    private Color oldColor;
    private Color newColor;
    
    public ChangeNodeColorCommand (Color newColor)
    {
        this.newColor = newColor;
    }
    
    public void Execute(LayerInstance nodeInstace)
    {
        oldColor = nodeInstace.Color;
        nodeInstace.Color = newColor;
    }
    
    public void Undo(LayerInstance nodeInstance)
    {
        nodeInstance.Color = oldColor;
    }
    
    public void Redo(LayerInstance nodeInstance)
    {
        Execute(nodeInstance);
    }
}
public class ChangeNodePropertyCommand<TProperty>: ICommand<ModelNode>
{
    private readonly Expression propertyExpression;
    private TProperty oldValue;
    private TProperty newValue;
    private readonly PropertyInfo propertyInfo;

    public ChangeNodePropertyCommand(Expression<Func<ModelNode, TProperty>> propertyExpression, TProperty newValue)
    {
        this.propertyExpression = propertyExpression;
        this.newValue = newValue;
        this.propertyInfo = (PropertyInfo)((MemberExpression)propertyExpression.Body).Member;
    }

    public void Execute(ModelNode nodeInstace)
    {
        oldValue = (TProperty)propertyInfo.GetValue(nodeInstace);
        propertyInfo.SetValue(nodeInstace, newValue);
    }

    public void Undo(ModelNode nodeInstance)
    {
        propertyInfo.SetValue(nodeInstance, oldValue);
    }

    public void Redo(ModelNode nodeInstance)
    {
        Execute(nodeInstance);
    }
}
    
#endregion

#region Model commands

public class ChangeModelNodePropertyCommand<TProperty>: ICommand<Model3DView>
{
    ChangeNodePropertyCommand<TProperty> changeNodeCommand;
    private Guid nodeGuid;

    public ChangeModelNodePropertyCommand(ChangeNodePropertyCommand<TProperty> changeNodeCommand, Guid nodeGuid)
    {
        this.changeNodeCommand = changeNodeCommand;
        this.nodeGuid = nodeGuid;
    }

    public void Execute(Model3DView nodeInstace)
    {
        var node = nodeInstace.nodeController.Node(nodeGuid);
        changeNodeCommand.Execute(node);
    }
    
    public void Undo(Model3DView nodeInstance)
    {
        var node = nodeInstance.nodeController.Node(nodeGuid);
        changeNodeCommand.Undo(node);
    }
    
    public void Redo(Model3DView nodeInstance)
    {
        var node = nodeInstance.nodeController.Node(nodeGuid);
        changeNodeCommand.Redo(node);
    }
}

public class ChangeModelPropertyCommand<TProperty>: ICommand<Model3DView>
{
    private readonly Expression propertyExpression;
    private TProperty oldValue;
    private TProperty newValue;
    private readonly PropertyInfo propertyInfo;

    public ChangeModelPropertyCommand(Expression<Func<Model3DView, TProperty>> propertyExpression, TProperty newValue)
    {
        this.propertyExpression = propertyExpression;
        this.newValue = newValue;
        this.propertyInfo = (PropertyInfo)((MemberExpression)propertyExpression.Body).Member;
    }

    public void Execute(Model3DView nodeInstace)
    {
        oldValue = (TProperty)propertyInfo.GetValue(nodeInstace);
        propertyInfo.SetValue(nodeInstace, newValue);
    }

    public void Undo(Model3DView nodeInstance)
    {
        propertyInfo.SetValue(nodeInstance, oldValue);
    }

    public void Redo(Model3DView nodeInstance)
    {
        Execute(nodeInstance);
    }
}

public class AddNodeCommand : ICommand<Model3DView>
{
    private ModelNode newNode;
    private Guid parentGuid;
    private Guid addedNodeGuid;
    
    public void Execute(Model3DView nodeInstace)
    {
        addedNodeGuid = nodeInstace.AddNode(newNode, parentGuid);
    }
    
    public void Undo(Model3DView nodeInstance)
    {
        nodeInstance.RemoveNode(addedNodeGuid);
    }
    
    public void Redo(Model3DView nodeInstance)
    {
        Execute(nodeInstance);
    }
}

public class RemoveNodeCommand: ICommand<Model3DView>
{
    private ModelNode removedNode;
    private Guid parentGuid;
    private Guid removedNodeGuid;
    
    public void Execute(Model3DView nodeInstace)
    {
        foreach (var node in nodeInstace.ModelTree)
        {
            if (node.guid == removedNodeGuid)
            {
                parentGuid = node.Parent.guid;
                break;
            }
        }
        removedNode = nodeInstace.RemoveNode(removedNodeGuid);
    }
    
    public void Undo(Model3DView nodeInstance)
    {
        nodeInstance.AddNode(removedNode, parentGuid);
    }
    
    public void Redo(Model3DView nodeInstance)
    {
        Execute(nodeInstance);
    }
}

#endregion
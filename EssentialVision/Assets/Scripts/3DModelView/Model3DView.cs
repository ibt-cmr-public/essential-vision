using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using MixedReality.Toolkit.SpatialManipulation;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

public class Model3DView : NetworkBehaviour
{
    
    private ModelScriptableObject currentModel = null;
    public NodeVisibilityStateController stateController = new NodeVisibilityStateController();
    public NetworkObject nodeContainer;
    
    [Header("Prefab References")]
    [SerializeField] private GameObject nodeContainerPrefab;
    [SerializeField] private GameObject clippingPlanePrefab;

    [Header("Logic Components")]
    [SerializeField] public NodeController nodeController;
    [SerializeField] public ClippingPlaneController clippingPlaneController;
    [SerializeField] public ModelAnimator animator;
    [SerializeField] public LayerSpawner layerSpawner;
    [SerializeField] public NodePositionController nodePositionController;
    [SerializeField] public AnnotationController annotationController;
    [SerializeField] public DragAndDropBoxManager dragAndDropBoxManager;
    [SerializeField] public InputController inputController;
    
    [Header("UI Components")]
    [SerializeField] private LoadingIndicator loadingIndicator;
    
    [Header("Networked model Settings")]
    private NetworkVariable<FixedString64Bytes> modelInstanceID = new NetworkVariable<FixedString64Bytes>(Guid.Empty.ToString());
    private NetworkVariable<FixedString64Bytes> name = new NetworkVariable<FixedString64Bytes>("");
    private NetworkVariable<FixedString512Bytes> description = new NetworkVariable<FixedString512Bytes>("");
    public NetworkVariable<bool> autorotateModel = new NetworkVariable<bool>(false);
    public NetworkVariable<float> rotationSpeed = new NetworkVariable<float>(20f);

    public bool IsLoading
    {
        get
        {
            return currentModel == null;
        }
    }
    
    public Action<ModelScriptableObject> OnModelSet;
    public Action OnModelInfoChanged;
    public Action<bool> LoadingStateChanged;
    
    //Initialization
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        InitializeComponents();
    }
    
    public void InitializeComponents()
    {
        //Create non-serialized components if null
        stateController ??= new NodeVisibilityStateController();
        animator ??= gameObject.AddComponent<ModelAnimator>();
        clippingPlaneController ??= gameObject.AddComponent<ClippingPlaneController>();
        nodeController ??= gameObject.AddComponent<NodeController>();
        nodePositionController ??= gameObject.AddComponent<NodePositionController>();

        if (IsServer)
        {
            SpawnNodeContainerServerRpc();
            SpawnClippingPlaneServerRpc();
        }

        //Deactivate clipping plane initially
        //We need to do this in Awake for now because MRTK has a bug that won't initialize clipping plane properly if it is not initially active
        clippingPlaneController.IsActive = false;

        //Hook up components
        layerSpawner.animator = animator;
        layerSpawner.clippingPlaneController = clippingPlaneController;
        stateController.NodeController = nodeController;
    }
    
    [ServerRpc]
    private void SpawnClippingPlaneServerRpc()
    {
        NetworkObject clippingPlane = Instantiate(clippingPlanePrefab, nodeContainer.transform).GetComponent<NetworkObject>();
        clippingPlane.Spawn();
        clippingPlane.TrySetParent(nodeContainer.transform);
        InitializeClippingPlaneClientRpc(clippingPlane);
    }
    
    [ServerRpc]
    private void SpawnNodeContainerServerRpc()
    {
        NetworkObject nodeContainer = Instantiate(nodeContainerPrefab, transform).GetComponent<NetworkObject>();
        nodeContainer.Spawn();
        nodeContainer.TrySetParent(transform);
        InitializeNodeContainerClientRpc(nodeContainer);
    }

    [ClientRpc]
    public void InitializeClippingPlaneClientRpc(NetworkObjectReference clippingPlaneReference)
    {
        clippingPlaneReference.TryGet(out NetworkObject clippingPlane);
        clippingPlaneController.clippingPlane = clippingPlane.GetComponent<CustomClippingPlane>();
        clippingPlane.gameObject.SetActive(false);
    }
    
    [ClientRpc]
    public void InitializeNodeContainerClientRpc(NetworkObjectReference nodeContainerReference)
    {
        nodeContainerReference.TryGet(out nodeContainer);
        nodePositionController.nodeContainer = nodeContainer.gameObject;
        annotationController.nodeContainer = nodeContainer.gameObject;
    }


    //Networked components
    public string Name
    {
        get => name.Value.ToString();
        set
        {
            SetNameServerRpc(value);
        }
    }
    [Rpc]
    private void SetNameServerRpc(string value)
    {
        name.Value = new FixedString64Bytes(value);
    }
    private void OnNameValueChanged(FixedString64Bytes oldValue, FixedString64Bytes newValue)
    {
        OnModelInfoChanged?.Invoke();
    }
    
    public string Description
    {
        get => description.Value.ToString();
        set
        {
            SetDescriptionServerRpc(value);
        }
    }
    [Rpc]
    private void SetDescriptionServerRpc(string value)
    {
        description.Value = new FixedString512Bytes(value);
    }
    private void OnDescriptionValueChanged(FixedString512Bytes oldValue, FixedString512Bytes newValue)
    {
        OnModelInfoChanged?.Invoke();
    }

    


    //Set the model
    public void SetModel(string modelGuid)
    {
        SetModelServerRpc(modelGuid);
    }
    
    public void SetModel(Guid modelGuid)
    {
        SetModelServerRpc(modelGuid.ToString());
    }
    
    
    [ServerRpc(RequireOwnership = false)]
    public void SetModelServerRpc(string modelGuid, ServerRpcParams serverRpcParams = default)
    {
        SetLoadingStateClientRpc(true);
        var clientId = serverRpcParams.Receive.SenderClientId;
        _ = SetModelServerAsync(modelGuid, clientId);
    }

    private async Task SetModelServerAsync(string modelGuid, ulong clientId)
    {
        //Destroy previously spawned layers
        foreach (ModelNode node in nodeController.NodesBottomup)
        {
            node.GetComponent<NetworkObject>().Despawn();
        }
        
        // Get model scriptable object from library
        ModelScriptableObject model = LibraryManager.Instance.GetModel(modelGuid, false);
        if (model == null)
        {
            if (ActiveSessionManager.Instance.SessionActive)
            {
                Debug.Log("Model not found in local library. Trying to fetch from client.");
                try
                {
                    model = await DataTransferManager.Instance.RequestModelFileFromClient(modelGuid, clientId);
                    LibraryManager.Instance.AddModel(model);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Error while fetching model from client: " + ex);
                    return;
                }
            }
            else
            {
                Debug.LogError("Could not find model with ID " + modelGuid + " on server.");
                return;
            }
        }

        // Instantiate all layers and layer groups
        List<NetworkObject> instantiatedNodes= null;
        try
        {
            instantiatedNodes = await layerSpawner.SpawnNodeAsync(model.ModelTree, nodeContainer.transform);
        }
        catch (Exception ex)
        {
            Debug.LogError("Error while instantiating layers: " + ex);
            return;
        }

        var instantiatedNodesReferences = new NetworkObjectReference[instantiatedNodes.Count];
        for (int i = 0; i < instantiatedNodes.Count; i++)
        {
            instantiatedNodesReferences[i] = instantiatedNodes[i];
        }

        // Call a client RPC to send the result to clients
        SetModelClientRpc(modelGuid, instantiatedNodesReferences);
    }

    //Handles initialization of the model on the client after model layers have been spawned server-side
    [ClientRpc]
    public void SetModelClientRpc(string modelGuid, NetworkObjectReference[] instantiatedNodesReferenceList)
    {
        SetModelClientAsync(modelGuid, instantiatedNodesReferenceList);
    }

    public async void SetModelClientAsync(string modelGuid, NetworkObjectReference[] instantiatedNodesReferenceList)
    {
        //Initialize instantiated layers 
        ModelScriptableObject model = LibraryManager.Instance.GetModel(modelGuid, false);
        if (model == null)
        {
            if (ActiveSessionManager.Instance.SessionActive)
            {
                Debug.Log("Model not found in local library. Trying to fetch from server.");
                try
                {
                    model = await DataTransferManager.Instance.RequestModelFileFromServer(modelGuid);
                    LibraryManager.Instance.AddModel(model);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Error while fetching model from server: " + ex);
                    return;
                }
            }
            else
            {
                Debug.LogError("Could not find model with ID " + modelGuid);
                return;
            }
        }
        
        //Recreate tree structure
        List<ModelNode> instantiatedNodes = new List<ModelNode>();
        foreach (NetworkObjectReference nodeReference in instantiatedNodesReferenceList)
        {
            nodeReference.TryGet(out NetworkObject networkObject);
            instantiatedNodes.Add(networkObject.GetComponent<ModelNode>());
        }
        layerSpawner.RecreateTree(model, instantiatedNodes);
        
        //Initialize layers
        await layerSpawner.InitializeInstantiatedLayersAsync(model.ModelTree.NodesData(), instantiatedNodesReferenceList);
        
        //Add root node to node controller 
        ModelNode rootNode = instantiatedNodes[0];
        nodeController.RootNode = rootNode;
        
        //Fit in viewing bounds
        nodePositionController.FitInBounds();
        
        //Spawn annotations
        foreach (AnnotationData annotationData in model.annotations)
        {
            annotationController.SpawnAnnotation(annotationData);
        }
        
        clippingPlaneController.textures = model.VolumetricTextures;
        currentModel = model;
        OnModelSet?.Invoke(model);

        //Select first layer initially
        if (IsServer)
        {
            if (nodeController.NumNodes > 0)
            {
                nodeController.SelectRootNode();
            }
        }
        
        //Exit loading state
        SetLoadingState(false);
    }
    
    
    //Loading State
    [ClientRpc]
    public void SetLoadingStateClientRpc(bool isLoading, string loadingText = "Loading")
    {
       SetLoadingState(isLoading);
    }
    
    private void SetLoadingState(bool isLoading, string loadingText = "Loading"){
        loadingIndicator.gameObject.SetActive(isLoading);
        loadingIndicator.LoadingText = loadingText;
    }
    

    //TODO: Generalize for Desktop
    public void ToggleBoundingBox()
    {
        BoundsControl boundsControl = nodeContainer.GetComponent<BoundsControl>();
        if (boundsControl == null) return;
        boundsControl.HandlesActive = !boundsControl.HandlesActive;
    }
    
    //Set model state
    public ModelInstanceState state
    {
        get
        {
            ModelInstanceState currentState = new ModelInstanceState();
            currentState.animationState = animator.state;
            currentState.clippingPlaneState = clippingPlaneController.state;
            currentState.nodeStates = nodeController.NodeStates;
            currentState.modelTransform = new LocalTransform(transform);
            currentState.autoRotation = AutorotateModel;
            currentState.rotationSpeed = RotationSpeed;
            return currentState;
        }
    }
    
    public void SetModelState(ModelInstanceState state, ModelInstanceStateTransition stateTransition, bool relativeToUser = true)
    {
        SetModelStateServerRpc(state, stateTransition, relativeToUser ? Camera.main.transform.position : Vector3.zero, relativeToUser ? Camera.main.transform.rotation : Quaternion.identity);
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetModelStateServerRpc(ModelInstanceState state, ModelInstanceStateTransition stateTransition, Vector3 currentUserPosition = default, Quaternion currentUserRotation = default)
    {
        if (stateTransition.updateAnimationState && state.animationState != null)  animator.SetAnimationState(state.animationState, stateTransition.animationStateTransition);
        if (stateTransition.updateClippingPlaneState && state.clippingPlaneState != null) clippingPlaneController.SetClippingPlaneState(state.clippingPlaneState, stateTransition.clippingPlaneStateTransition);
        if (stateTransition.updateLayerStates && state.nodeStates != null) nodeController.SetNodeStates(state.nodeStates, stateTransition.nodeStateUpdates);
        if (stateTransition.updateLayerIdx) nodeController.SelectedNodeGuid = state.selectedLayerGuid;
        if (stateTransition.updateModelTransform)
        {
            Vector3 globalPosition = currentUserPosition + state.modelTransform.localPosition;
            Quaternion globalRotation = currentUserRotation * state.modelTransform.localRotation;
            if (stateTransition.modelTransformTransition.updateLocalPosition) SetNodeContainerGlobalPositionServerRpc(globalPosition);
            if (stateTransition.modelTransformTransition.updateLocalRotation) SetNodeContainerGlobalRotationServerRpc(globalRotation);
            if (stateTransition.modelTransformTransition.updateLocalScale) SetNodeContainerScaleServerRpc(state.modelTransform.localScale);
        }
        if (stateTransition.updateAutoRotation) SetAutorotateModelServerRpc(state.autoRotation);
        if (stateTransition.updateRotationSpeed) SetRotationSpeedServerRpc(state.rotationSpeed);
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SetNodeContainerGlobalRotationServerRpc(Quaternion rotation, float duration = 1f)
    {
        nodeContainer.transform.DORotate(rotation.eulerAngles, duration);
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SetNodeContainerGlobalPositionServerRpc(Vector3 position, float duration = 1f)
    {
        nodeContainer.transform.DOMove(position, duration);
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SetNodeContainerScaleServerRpc(Vector3 scale, float duration = 1f)
    {
        nodeContainer.transform.DOScale(scale, duration);
    }

    public ModelScriptableObject CurrentModel
    {
        get
        {
            return currentModel;
        }
    }
    
    public void DestroyModel()
    {
        DestroyModelServerRpc();
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void DestroyModelServerRpc()
    { 
        animator.Stop(); //Stop the animation to avoid an invalid Rpc call in the last update loop
        foreach (ModelNode node in nodeController.NodesBottomup)
        {
            node.GetComponent<NetworkObject>().Despawn();
        }
        clippingPlaneController.clippingPlane.GetComponent<NetworkObject>().Despawn();
        nodeContainer.Despawn();
        gameObject.GetComponent<NetworkObject>().Despawn();
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetRotationSpeedServerRpc(float value)
    {
        rotationSpeed.Value = value;
        nodeContainer.GetComponent<Rotate>().rotationSpeed = rotationSpeed.Value;
    }

    public float RotationSpeed
    {
        get
        {
            return rotationSpeed.Value;
        }
        set
        {
            SetRotationSpeedServerRpc(value);
        }
    }
    

    [ServerRpc(RequireOwnership = false)]
    private void SetAutorotateModelServerRpc(bool value)
    {
        
        autorotateModel.Value = value;
        nodeContainer.GetComponent<Rotate>().enabled = value;
    }
    
    public bool AutorotateModel
    {
        get
        {
            return autorotateModel.Value;
        }
        set
        {
            SetAutorotateModelServerRpc(value);
        }
    }
    
    
    public void FocusOnNode(Guid nodeID, bool hideOthers = false, bool resetNodePositions = true, bool relativeToUser = true)
    {
        FocusOnNodeServerRpc(nodeID, hideOthers, resetNodePositions, relativeToUser ? Camera.main.transform.position : Vector3.zero);
    }

    
    [ServerRpc(RequireOwnership = false)]
    private void FocusOnNodeServerRpc(
        Guid nodeID, 
        bool hideOthers = false, 
        bool resetNodePositions = true,
        Vector3 currentUserPosition = default)
    {
        //Select focused node
        nodeController.SelectedNodeGuid = nodeID;
        
        //Node visibility
        stateController.ShowNode(nodeID);
        stateController.UnfadeNode(nodeID);
        if (hideOthers) {
            stateController.HideOtherLayers(nodeID, true);
        }
        else {
            stateController.FadeOtherLayers(nodeID, true);
        }
        
        //Node positioning
        if (resetNodePositions) nodePositionController.SetDefaultTransforms();
        
        //Rotate model accordingly
        Vector3 modelToUser = currentUserPosition - nodeContainer.transform.position;
        Vector3 modelToNode = nodeController.Node(nodeID).bounds.center - nodeContainer.transform.position;
        modelToNode.y = 0;
        modelToUser.y = 0;
        float angle = Vector3.SignedAngle(modelToNode, modelToUser, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
        SetNodeContainerGlobalRotationServerRpc(rotation * nodeContainer.transform.rotation);
    }
    
    public void CompareNodes(List<Guid> nodeIDs, bool radialPositioning = true, bool relativeToUser = true)
    {
        CompareNodesServerRpc(nodeIDs.ToArray(), relativeToUser ? Camera.main.transform.position : Vector3.zero, radialPositioning);
    }
    
    
    [ServerRpc(RequireOwnership = false)]
    private void CompareNodesServerRpc(
        Guid[] nodeIDs, 
        Vector3 currentUserPosition = default,
        bool radialPositioning = true)
    {
        //Select focused node
        nodeController.SelectedNodeGuid = nodeIDs[0];
        
        //Node visibility
        stateController.HideAll();
        foreach (Guid nodeID in nodeIDs)
        {
            stateController.ShowNode(nodeID);
            stateController.UnfadeNode(nodeID);
        }
        
        //Node positioning
        if (radialPositioning)
        {
            nodePositionController.CompareNodesRadial(nodeIDs.ToList());
        }
        else
        {
            nodePositionController.CompareNodesGrid(nodeIDs.ToList());
        }

        //Rotate model to align with user view 
        Vector3 userToModel = nodeContainer.transform.position - currentUserPosition;
        Vector3 modelViewDirection = nodeContainer.transform.forward;
        userToModel.y = 0;
        modelViewDirection.y = 0;
        float angle = Vector3.SignedAngle(modelViewDirection, userToModel, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
        SetNodeContainerGlobalRotationServerRpc(rotation * nodeContainer.transform.rotation);
    }
    
    
    public void DragAndDropNodes(List<Guid> nodeIDs, bool relativeToUser = true)
    {
        DragAndDropNodesServerRpc(nodeIDs.ToArray(), relativeToUser ? Camera.main.transform.position : Vector3.zero);
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void DragAndDropNodesServerRpc(
        Guid[] nodeIDs, 
        Vector3 currentUserPosition = default)
    {
        //Select root node
        nodeController.SelectRootNode();
        inputController.IndividualMovementActive = true;
        
        //Node visibility
        stateController.HideAll();
        foreach (Guid nodeID in nodeIDs)
        {
            stateController.ShowNode(nodeID);
            stateController.UnfadeNode(nodeID);
        }
        
        
        //Node and box positioning
        float viewingSquareLength = 0.6f;
        float cubeSize = viewingSquareLength / nodeIDs.Length;
        nodePositionController.CompareNodesGrid(nodeIDs.ToList(), 2, nodeIDs.Length, viewingSquareLength);
        dragAndDropBoxManager.DestroyAllBoxes();
        int i = 0;
        foreach (Guid nodeID in nodeIDs)
        {
            float localPositionX = i  * cubeSize - viewingSquareLength / 2 + cubeSize / 2;
            float localPositionY = - cubeSize + viewingSquareLength / 2 - cubeSize / 2;
            Vector3 localPosition = new Vector3(localPositionX, localPositionY, 0);
            Vector3 size = new Vector3(cubeSize, cubeSize, cubeSize) * 0.8f;
            dragAndDropBoxManager.SpawnDragAndDropBox(localPosition, size, nodeController.Node(nodeID).NodeInfo.Name);
            i++;
        }

        //Rotate model to align with user view 
        Vector3 userToModel = nodeContainer.transform.position - currentUserPosition;
        Vector3 modelViewDirection = nodeContainer.transform.forward;
        userToModel.y = 0;
        modelViewDirection.y = 0;
        float angle = Vector3.SignedAngle(modelViewDirection, userToModel, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
        SetNodeContainerGlobalRotationServerRpc(rotation * nodeContainer.transform.rotation);
    }
    
}

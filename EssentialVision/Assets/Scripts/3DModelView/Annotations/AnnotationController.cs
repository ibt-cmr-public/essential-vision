using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class AnnotationController : MonoBehaviour
{
    [SerializeField] private GameObject annotationPrefab;
    [SerializeField] private NodeController nodeController;
    [SerializeField] private ModelAnimator animator;
    public GameObject nodeContainer;
    private List<ModelAnnotation> spawnedAnnotations = new List<ModelAnnotation>();
    private NetworkVariable<bool> showAnnotations = new NetworkVariable<bool>(false);

    public bool ShowAnnotations
    {
        get
        {
            return showAnnotations.Value;
        }
        set
        {
            SetShowAnnotationsServerRpc(value);
        }
    }
    
    [ServerRpc (RequireOwnership = false)]
    private void SetShowAnnotationsServerRpc(bool value)
    {
        showAnnotations.Value = value;
    }

    private void OnShowAnnotationsValueChanged(bool oldValue, bool newValue)
    {
        foreach (ModelAnnotation annotation in spawnedAnnotations)
        {
            annotation.gameObject.SetActive(newValue);
        }
    }
    
    public void SpawnAnnotation(AnnotationData annotationData)
    {
        //Get node
        Transform parentTransform = null;
        if (annotationData.attachedToNode){
            parentTransform = nodeController.Node(annotationData.nodeGuid).transform;
        }
        else
        {
            parentTransform = nodeContainer.transform;
        }
        
        //Spawn annotation
        ModelAnnotation spawnedAnnotation = Instantiate(annotationPrefab, parentTransform).GetComponent<ModelAnnotation>();
        spawnedAnnotation.SetAnnotationData(annotationData);
        spawnedAnnotations.Add(spawnedAnnotation);
        animator.AddAnimation(spawnedAnnotation);
        spawnedAnnotation.gameObject.SetActive(ShowAnnotations);
    }


    public void Start()
    {
        showAnnotations.OnValueChanged += OnShowAnnotationsValueChanged;
    }
}

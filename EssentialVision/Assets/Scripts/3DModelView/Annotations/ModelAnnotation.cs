using DG.Tweening;
using MixedReality.Toolkit;
using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ModelAnnotation : MonoBehaviour, IAnimatable
{

    [SerializeField] private StatefulInteractable annotationMarker;
    [SerializeField] private TextMeshProUGUI annotationTitle;
    [SerializeField] private TextMeshProUGUI annotationInfo;
    [SerializeField] private bool isSelected = false;

    [SerializeField] private GameObject infoWindow;
    [SerializeField] private PressableButton windowPinnedButton;

    [SerializeField] private Material selectedMarkerMaterial;
    [SerializeField] private Material unselectedMarkerMaterial;

    [SerializeField] private float selectedMarkerSize = 0.08f;
    [SerializeField] private float unselectedMarkerSize = 0.05f;
    
    //Animation (only of time dependent)
    private float startTime;
    private float endTime;
    private float currentTime;
    private bool loopAnimation = false;
    
    
    public float CurrentTime
    {
        get
        {
            return currentTime;
        }
        set
        {
            currentTime = value;
            if (currentTime < startTime || currentTime > endTime)
            {
                annotationMarker.transform.DOScale(new Vector3(0f,0f,0f), 0.1f);
                CloseWindow();
            }
            else
            {
                if(isSelected) OpenWindow();
                Vector3 currentScale = isSelected ? new Vector3(selectedMarkerSize, selectedMarkerSize, selectedMarkerSize) : new Vector3(unselectedMarkerSize, unselectedMarkerSize, unselectedMarkerSize);
                annotationMarker.transform.DOScale(currentScale, 0.1f);
            }
        }
    }
    
    public bool LoopAnimation
    {
        get
        {
            return loopAnimation;
        }
        set
        {
            loopAnimation = value;
        }
    }

    public bool IsSelected
    {
        get
        {
            return isSelected;
        }
        set
        {
            isSelected = value;
            OpenWindow();
            windowPinnedButton.ForceSetToggled(isSelected);
            annotationMarker.gameObject.GetComponent<MeshRenderer>().material = isSelected ? selectedMarkerMaterial : unselectedMarkerMaterial;
        }
    }
    public void OnFirstHoverEntered(HoverEnterEventArgs args)
    {
        Vector3 currentScale = new Vector3(selectedMarkerSize, selectedMarkerSize, selectedMarkerSize);
        annotationMarker.transform.DOScale(currentScale, 0.1f);
        OpenWindow();
    }
    
    public void OnLastHoverExited(HoverExitEventArgs args)
    {
        if (!isSelected) 
        {                
            Vector3 currentScale = new Vector3(unselectedMarkerSize, unselectedMarkerSize, unselectedMarkerSize);
            annotationMarker.transform.DOScale(currentScale, 0.1f);
            CloseWindow();
        }
    }
    
    public void OnClicked()
    {
        IsSelected = !IsSelected;
    }

    public void OpenWindow()
    {
        infoWindow.SetActive(true);
        infoWindow.transform.DOMove(annotationMarker.transform.position + new Vector3(0f,0.2f,0f), 0.1f);
        infoWindow.transform.DOScale(new Vector3(0.001f,0.001f,0.001f), 0.1f);
    }

    public void CloseWindow()
    {
        infoWindow.transform.DOMove(annotationMarker.transform.position, 0.1f);
        infoWindow.transform.DOScale(new Vector3(0f,0f,0f), 0.1f).OnComplete(()=>infoWindow.SetActive(false));;
    }
    public void SetAnnotationData(AnnotationData annotationData)
    {
        annotationTitle.text = annotationData.title;
        annotationInfo.text = annotationData.info;
        annotationMarker.transform.localPosition = annotationData.localPosition;
        startTime = annotationData.startTime;
        endTime = annotationData.endTime;
    }


    public void Start()
    {
        infoWindow.transform.localScale = new Vector3(0f,0f,0f);
        infoWindow.SetActive(false);
        annotationMarker.firstHoverEntered.AddListener(OnFirstHoverEntered);
        annotationMarker.lastHoverExited.AddListener(OnLastHoverExited);
        annotationMarker.OnClicked.AddListener(OnClicked);
    }
}

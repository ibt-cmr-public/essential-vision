using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Annotation", menuName = "EssentialVision/Annotation")]
public class AnnotationData : ScriptableObject
{
    public Guid nodeGuid = Guid.Empty;
    public bool attachedToNode = false;
    public float startTime = 0f;
    public float endTime = 1f;
    public string title = "";
    public string info = "";
    public Vector3 localPosition = Vector3.zero;


    public byte[] ToBinary()
    {
        return SerializationHelper.Serialize(this);
    }
    
    public static AnnotationData FromBinary(byte[] data)
    {
        return SerializationHelper.Deserialize<AnnotationData>(data);
    }
    
}

using System;
using System.Collections.Generic;
using System.Linq;
using MixedReality.Toolkit.SpatialManipulation;
using Nova;
using NovaSamples.UIControls;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;


//Spawns model 3D views
public class ModelInstanceManager : NetworkBehaviour
{
    
    //Other components
    [SerializeField] public LibraryManager libraryManager;
    [SerializeField] private ActiveSessionManager activeSessionManager;
    
    //3D View spawning
    [SerializeField] public GameObject Model3DViewPrefab;
    [SerializeField] private GameObject ModelViewContainer;
    [SerializeField] private GameObject placeholderPrefab;
    
    //Model instance spawning
    public enum ModelInstanceType
    {
        DesktopView,
        DesktopEdit,
        XR
    }

    public Dictionary<Guid, ModelSetup> modelInstances = new Dictionary<Guid, ModelSetup>();
    [SerializeField] private GameObject XRModelInstancePrefab;
    [SerializeField] private GameObject DesktopViewModelInstancePrefab;
    [SerializeField] private GameObject DesktopEditModelInstancePrefab;
    private GameObject currentPlaceholder;
    public Action<ModelSetup> OnModelInstanceSpawned;
    
    
    private ModelInstanceType DefaultModelInstanceType {
        get 
        {
            if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.Desktop)
            {
                return ModelInstanceType.DesktopView;
            }
            else
            {
                return ModelInstanceType.XR;
            }
        }
    }
    
    //Model spawning
    public void SpawnModelWithPlacement(string modelID)
    {
        if (PlatformManager.Instance.GetPlatformType() != PlatformManager.PlatformType.XR)
        {
            Debug.LogWarning("Can't spawn model with placement on desktop platform. Spawning without placement.");
            SpawnModelServerRpc(modelID, Vector3.zero);
        }
        
        //Show placeholder
        currentPlaceholder = Instantiate(placeholderPrefab, ModelViewContainer.transform);
        
        //Start placement
        TapToPlace placementSolver = currentPlaceholder.GetComponent<TapToPlace>();
        placementSolver.OnPlacingStopped.AddListener(() =>  SpawnModelServerRpc(modelID, currentPlaceholder.transform.position - activeSessionManager.anchorPosition));
        placementSolver.OnPlacingStopped.AddListener(() => Destroy(currentPlaceholder));
    }
    
    public void SpawnModelWithPlacement(Guid modelID)
    {
        SpawnModelWithPlacement(modelID.ToString());
    }
    
    [ServerRpc (RequireOwnership = false)]
    public void SpawnModelServerRpc(
        string modelID, 
        Vector3 relativeSpawnPosition, 
        PlatformManager.PlatformType[] platformTypes = null, //We need to pass in plaftorm types and model instance types as arrays instead of dict so it is serializable (since this is an RPC)
        ModelInstanceType[] modelInstanceTypes = null,
        bool isNetworked = true)
    {
        
        //Zip arrays to get dictionary
        Dictionary<PlatformManager.PlatformType, ModelInstanceType> instanceTypes;
        if (platformTypes == null || modelInstanceTypes == null || platformTypes.Length != modelInstanceTypes.Length)
        {
            instanceTypes = new Dictionary<PlatformManager.PlatformType, ModelInstanceType>();
        }
        else
        {
            instanceTypes = platformTypes.Zip(modelInstanceTypes, (p, m) => new { Platform = p, Instance = m }).ToDictionary(x => x.Platform, x => x.Instance);
        }
        
        //Spawn 3D view
        Model3DView model3DView = Instantiate(Model3DViewPrefab).GetComponent<Model3DView>();
        model3DView.GetComponent<NetworkObject>().Spawn();
        model3DView.GetComponent<NetworkObject>().TrySetParent(ModelViewContainer.transform);
        
        //Set initial position and rotation
        model3DView.transform.position = relativeSpawnPosition + activeSessionManager.anchorPosition;
        Vector3 targetPosition = Camera.main.transform.position;
        Vector3 horizontalDirection = Vector3.ProjectOnPlane(relativeSpawnPosition - targetPosition, Vector3.up).normalized;
        Quaternion instanceRotation = Quaternion.LookRotation(horizontalDirection);
        model3DView.transform.rotation = instanceRotation;
        
        //Set model (spawns layers and groups) 
        model3DView.SetModel(modelID);
        SpawnModelClientRpc(modelID, model3DView.GetComponent<NetworkObject>(), instanceTypes.Keys.ToArray(), instanceTypes.Values.ToArray());
    }
    
    [ClientRpc]
    private void SpawnModelClientRpc(
        string modelID, 
        NetworkObjectReference model3DViewReference, 
        PlatformManager.PlatformType[] platformTypes = null, //We need to pass in plaftorm types and model instance types as arrays instead of dict so it is serializable (since this is an RPC)
        ModelInstanceType[] modelInstanceTypes = null)
    {
        //Zip arrays to get dictionary
        Dictionary<PlatformManager.PlatformType, ModelInstanceType> instanceTypes;
        if (platformTypes == null || modelInstanceTypes == null || platformTypes.Length != modelInstanceTypes.Length)
        {
            instanceTypes = new Dictionary<PlatformManager.PlatformType, ModelInstanceType>();
        }
        else
        {
            instanceTypes = platformTypes.Zip(modelInstanceTypes, (p, m) => new { Platform = p, Instance = m }).ToDictionary(x => x.Platform, x => x.Instance);
        }
        //Get model3DView
        model3DViewReference.TryGet(out NetworkObject model3DViewObject);
        Model3DView model3DView = model3DViewObject.GetComponent<Model3DView>();
        PlatformManager.PlatformType currentPlatform = PlatformManager.Instance.GetPlatformType();
        ModelInstanceType modelInstanceType = instanceTypes.ContainsKey(currentPlatform) ? instanceTypes[currentPlatform] : DefaultModelInstanceType;
        ModelSetup spawnedModelSetup;
        switch (modelInstanceType)
        {
            case ModelInstanceType.XR:
                ModelSetup xrModelSetup = Instantiate(XRModelInstancePrefab, model3DView.transform.position, Quaternion.identity, transform).GetComponent<ModelSetup>();
                xrModelSetup.ModelView = model3DView;
                spawnedModelSetup = xrModelSetup;
                break;
            case ModelInstanceType.DesktopView:
                //Find model instance parent by tag
                DesktopModelSetup desktopModelSetup = Instantiate(DesktopViewModelInstancePrefab, model3DView.transform.position, Quaternion.identity).GetComponent<DesktopModelSetup>();
                UIBlock2D modelInstanceUIParent = GameObject.FindGameObjectWithTag("DesktopModelInstanceUIParent").GetComponent<UIBlock2D>();
                UIBlock2D modelInstanceUIRoot = GameObject.FindGameObjectWithTag("DesktopMainUI").GetComponent<UIBlock2D>();
                MultiCameraInputManager inputManager = GameObject.FindObjectOfType<MultiCameraInputManager>();
                inputManager.cameras.Add(desktopModelSetup.camera);
                desktopModelSetup.cameraFitter.uiBlock2D = modelInstanceUIParent;
                desktopModelSetup.cameraFitter.rootBlock = modelInstanceUIRoot;
                //desktopModelInstance.desktopUI.transform.SetParent(modelInstanceUIParent, false);
                desktopModelSetup.ModelView = model3DView;
                spawnedModelSetup = desktopModelSetup;
                break;
            case ModelInstanceType.DesktopEdit:
                throw new System.NotImplementedException();
                break;
            default:
                throw new System.ArgumentOutOfRangeException();
        }
        modelInstances.Add(spawnedModelSetup.InstanceGuid, spawnedModelSetup);
        OnModelInstanceSpawned?.Invoke(spawnedModelSetup);
    }


    void Start()
    {
        Addressables.InitializeAsync();
    }
}

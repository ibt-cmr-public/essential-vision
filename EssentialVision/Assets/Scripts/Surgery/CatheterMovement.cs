using UnityEngine;

public class CatheterMovement : MonoBehaviour
{

    public void UpdatePosition(Vector3 primary)
    {
        Vector3 oldPosition = transform.position;
        transform.localPosition = primary;
        transform.LookAt(oldPosition);
    }
    
    public void UpdatePositions(Vector3 primary, Vector3 secondary)
    {
        transform.localPosition = primary;
    
        // Check if there is a parent transform
        if (transform.parent != null)
        {
            Vector3 secondaryWorldPosition = transform.parent.TransformPoint(secondary);
            Vector3 primaryWorldPosition = transform.parent.TransformPoint(primary);
            transform.LookAt(primaryWorldPosition - (secondaryWorldPosition - primaryWorldPosition));
        }
        else
        {
            // If there is no parent, use the secondary position directly without transformation
            transform.LookAt(secondary);
        }
    }
}

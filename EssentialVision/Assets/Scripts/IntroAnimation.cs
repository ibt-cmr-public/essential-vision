using System.Collections;
using UnityEngine;
using UnityEngine.Video;

public class IntroAnimation : MonoBehaviour
{
    
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private MainMenu mainMenu;
    [SerializeField] private bool playIntro = true;
    private float animationTime;




    // Start is called before the first frame update
    void Start()
    {
        if (!playIntro)
        {
            OnIntroFinished();
        }
        
        if (videoPlayer == null)
        {
            videoPlayer = GetComponent<VideoPlayer>();
        }

        if (videoPlayer == null)
        {
            Debug.LogWarning("Intro animation present without video player. Animation skipped.");
            OnIntroFinished();
        }
        animationTime = (float)videoPlayer.clip.length;
        StartCoroutine(AnimationIntro());
    }

    private IEnumerator AnimationIntro()
    {
        yield return new WaitForSeconds(animationTime + 0.2f);
        OnIntroFinished();
    }
    public void OnIntroFinished()
    {
        mainMenu.InFocus = true;
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

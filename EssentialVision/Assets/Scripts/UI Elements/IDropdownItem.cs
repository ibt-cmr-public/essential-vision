public interface IDropdownItem {
    bool IsExpanded { set; }
    bool IsLeaf { set; }
    int Level { set; }
    
}
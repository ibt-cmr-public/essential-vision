﻿
// Copyright (c) Mixed Reality Toolkit Contributors
// Licensed under the BSD 3-Clause

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class VirtualizedScrollDropdown : MonoBehaviour
{

    //Class used to represent the tree of nodes in the dropdown hierarchy
    private class DropdownNode {
        public int id;
        public DropdownNode parent;
        public List<DropdownNode> children;
        public bool isExpanded = false;
        public bool isRoot {
            get {
                return parent == null;
            }
        }
        public bool isLeaf {
            get {
                return children.Count == 0;
            }
        }
        
        public int Level {
            get {
                if (isRoot) {
                    return -1;
                }
                else {
                    return parent.Level + 1;
                }
            }
        }
        
        public IEnumerable<DropdownNode> TraverseTree() {
            yield return this;
            if (isExpanded) {
                foreach (DropdownNode child in children)
                {
                    //Traverse subtree of each child
                    foreach (DropdownNode grandchild in child.TraverseTree())
                    {
                        yield return grandchild;
                    }
                }
            }
        }
        
        public IEnumerable<DropdownNode> TraverseExpandedTree() {
            yield return this;
            foreach (DropdownNode child in children)
            {
                //Traverse subtree of each child
                foreach (DropdownNode grandchild in child.TraverseExpandedTree())
                {
                    yield return grandchild;
                }
            }
        }
        
        public int Size() {
            int i = 1;
            if (isExpanded) {
                foreach (DropdownNode child in children)
                {
                    i += child.Size();
                }
            }

            return i;
        }
        
        public int ExpandedSize() {
            int i = 1;
            foreach (DropdownNode child in children)
            {
                i += child.ExpandedSize();
            }
            return i;
        }
    }
    
    #region Inspector and Private Fields

    /// <summary>
    /// The direction the cell layout should flow in, top to bottom, or
    /// left to right.
    /// </summary>
    public enum Layout
    {
        /// <summary>
        /// This layout flows from top to bottom along the Y axis.
        /// </summary>
        Vertical,

        /// <summary>
        /// This layout flows from left to right along the X axis.
        /// </summary>
        Horizontal,
    }

    // Header Objects
    [Header("Objects")]
    [Tooltip("The pool of list item objects will be composed of this Prefab.")]
    [SerializeField]
    private GameObject prefab;

    [Tooltip("The ScrollRect used to display this list. If unspecified, VirtualizedScrollRectList will look for a ScrollRect on the current GameObject.")]
    [SerializeField]
    private ScrollRect scrollRect;

    // Header Layout

    [Header("Layout")]
    [Tooltip("Should the layout cells scroll horizontally on the X axis, or vertically on the Y axis?")]
    [SerializeField]
    private Layout layoutDirection = Layout.Vertical;

    [Tooltip("(Optional) The size of each layout cell. If an axis is 0, VirtualizedList will pull this dimension directly from the prefab's RectTransform.")]
    [SerializeField]
    private Vector2 cellSize;

    [Tooltip("This is the spacing between each layout cell in local units.")]
    [SerializeField]
    private float gutter;

    [Tooltip("This is the spacing between the area where the layout cells are, and the boundaries of the ScrollRect's Content area.")]
    [SerializeField]
    private float margin;

    [Tooltip("When scrolling to the end of the list, it can be nice to show some empty space to indicate the end of the list. This is in local units.")]
    [SerializeField]
    private float trailingSpace;

    //Scroll variables
    private float scroll = 0;
    private float requestScroll;

    private int screenCount;
    private float viewSize;
    private float contentStart;
    private float layoutPrefabSize;
    private bool initialized = false;

    private int visibleStart;
    private int visibleEnd;
    private List<int> assignedNodes = new List<int>();
    private bool visibleValid;

    private Queue<GameObject> pool = new Queue<GameObject>();
    private Dictionary<int, GameObject> poolDict = new Dictionary<int, GameObject>();

    
    //Dropdown nodes
    private DropdownNode rootNode = new DropdownNode() {
        id = -1,
        parent = null,
        children = new List<DropdownNode>(),
        isExpanded = true
    };
    
    #endregion

    #region Public properties and fields

    /// <summary>
    /// This is the index based scroll value of the list. You can set this,
    /// and it will snap the ScrollRect content to the correct location. If
    /// you want to scroll to halfway through item #3, you can just set the
    /// Scroll to 3.5.
    ///
    /// Things to note here, if you have multiple RowsOrColumns, like 2,
    /// then there are 2 indices per Row, and 3 would be halfway through
    /// the second row. Due to the scroll viewport size, also may not be
    /// able scroll directly to the last element. Scroll will be clamped
    /// between 0 and MaxScroll.
    /// </summary>
    public float Scroll
    {
        get => scroll;
        set
        {
            requestScroll = value;
            UpdateScrollView(requestScroll);
        }
    }

    /// <summary>
    /// This is the current number of items that this list is handling for
    /// display.
    /// </summary>
    public int NumNodes => rootNode.Size()-1 ;
    
    public int NumExpandedNodes => rootNode.ExpandedSize()-1 ;
    
    /// <summary>
    /// This is the maximum number of potentially visible layout cells that
    /// may appear on the ScrollRect's viewport. This includes partially
    /// obscured cells.
    /// </summary>
    public int PartiallyVisibleCount => 2*Mathf.CeilToInt(viewSize / layoutPrefabSize) - Mathf.FloorToInt(viewSize/layoutPrefabSize) ;

    /// <summary>
    /// This is the number of completely unobscured layout cells the
    /// ScrollRect's viewport can display. This is useful for things like
    /// paginated scrolling, where you want to advance to the next page of
    /// items, and want to start with whatever might be obscured right now.
    /// </summary>
    public int TotallyVisibleCount => Mathf.FloorToInt(viewSize / layoutPrefabSize);

    /// <summary>
    /// This is the number of layout cells that fit on the viewport in
    /// total, as a floating point value.
    /// </summary>
    public float ScreenItemCountF => (viewSize / layoutPrefabSize);

    /// <summary>
    /// This is the maximum value for the Scroll. The Scroll value will be
    /// clamped to this value.
    /// </summary>
    public float MaxScroll => layoutDirection == Layout.Vertical
        ? ((scrollRect.content.rect.height - viewSize) / layoutPrefabSize)
        : ((scrollRect.content.rect.width - viewSize) / layoutPrefabSize);

    /// <summary>
    /// Called when a node in the hierarchy is assigned to a GameObject just before it is made visible
    /// </summary>
    public Action<GameObject, int> OnAssignedNode { get; set; }

    /// <summary>
    /// When a pooled prefab instance is just removed from visibility on
    /// the scroll content area, this callback is called, with an integer
    /// representing the index of the corresponding dropdown node. VirtualizedScrollRectList
    /// will make this GameObject invisible for you after this callback is
    /// invoked.
    /// </summary>
    public Action<GameObject, int> OnUnassignedNode { get; set; }

    #endregion

    #region Private Methods
    
    private int listPositionToDropdownNodeID(int listPosition)
    {
        int i = -1;
        foreach (DropdownNode node in rootNode.TraverseTree())
        {
            if (i == listPosition)
            {
                return node.id;
            }
            i++;
        }

        return -1;
    }
    
    private int dropdownNodeIDToListPosition(int nodeID)
    {
        int i =  -1;
        foreach (DropdownNode node in rootNode.TraverseTree())
        {
            if (node.id == nodeID)
            {
                return i;
            }
            i++;
        }
        return -1;
    }
    
    private DropdownNode GetNode(int nodeID)
    {
        foreach (DropdownNode node in rootNode.TraverseExpandedTree())
        {
            if (node.id == nodeID)
            {
                return node;
            }
        }

        return null;
    }

    /// <summary>
    /// Given an index, this calculates the position of the associated
    /// layout cell based on the current layout direction and padding /
    /// gutter sizes.
    /// </summary>
    /// <param name="index">
    /// Index of the layout cell, this will still produce valid results
    /// outside of the 0-count range.
    /// </param>
    /// <returns>
    /// A ScrollRect relative location for the layout cell.
    /// </returns>
    private Vector3 ItemLocation(int index) => layoutDirection == Layout.Vertical
        ? new Vector3(
            scrollRect.content.rect.xMin + margin,
            contentStart - (margin + index  * layoutPrefabSize),
            0)
        : new Vector3(
            contentStart + (margin + index * layoutPrefabSize),
            scrollRect.content.rect.yMax - margin,
            0);

    /// <summary>
    /// This converts a ScrollRect scroll position to an index based scroll
    /// value that we can use for managing our scroll in a more intelligent
    /// manner.
    /// </summary>
    /// <param name="pos">
    /// A position on the ScrollRect viewport, this value should match the
    /// axis indicated by layoutDirection.
    /// </param>
    /// <returns>
    /// A scroll index value representing the itemCount index at the
    /// position. Not bounded to the 0-itemCount range at all.
    /// </returns>
    private float PosToScroll(float pos) => layoutDirection == Layout.Vertical
        ? ((pos - (margin - gutter)) / layoutPrefabSize) 
        : ((-pos - (margin - gutter)) / layoutPrefabSize) ;

    /// <summary>
    /// This converts an index based scroll value into a ScrollRect scroll
    /// position that we can use for positioning the ScrollRect content.
    /// </summary>
    /// <param name="scroll">
    /// A scroll index value representing the itemCount index at the
    /// position. Not bounded to the 0-itemCount range at all.
    /// </param>
    /// <returns>
    /// A position on the ScrollRect viewport, this value matches the axis
    /// indicated by layoutDirection.
    /// </returns>
    private float ScrollToPos(float scroll) => layoutDirection == Layout.Vertical
        ? (scroll * (layoutPrefabSize )) + (margin - gutter)
        : (-scroll * (layoutPrefabSize )) + (margin - gutter);

    /// <summary>
    /// A Unity Editor only event function that is called when the script is loaded or a value changes in the Unity Inspector.
    /// </summary>
    private void OnValidate()
    {
        // We only want to reset things if it has already been initialized,
        // we don't want to initialize it prematurely!
        if (initialized == false) { return; }

        if (margin < gutter) { margin = gutter; }

        visibleValid = false;
        Initialize();
    }

    /// <summary>
    /// A Unity event function that is called on the frame when a script is enabled just before any of the update methods are called the first time.
    /// </summary>
    private void Start()
    {
        visibleValid = false;
        scrollRect = scrollRect == null ? GetComponent<ScrollRect>() : scrollRect;
        BakeCachedValues();

        // Unity RectTransforms don't know anything about sizes until after
        // a frame has passed after Start.
        StartCoroutine(EndOfFrameInitialize());
    }

    private IEnumerator EndOfFrameInitialize()
    {
        yield return new WaitForEndOfFrame();

        Initialize();
    }

    private void BakeCachedValues()
    {
        RectTransform prefabRect = prefab.GetComponent<RectTransform>();
        if (cellSize.x == 0) cellSize.x = prefabRect.rect.width;
        if (cellSize.y == 0) cellSize.y = prefabRect.rect.height;
        layoutPrefabSize = layoutDirection == Layout.Vertical ? cellSize.y + gutter : cellSize.x + gutter;
    }

    private void Initialize()
    {
        BakeCachedValues();

        if (layoutDirection == Layout.Vertical)
        {
            Vector2 topCenter = new Vector2(0.5f, 1);
            scrollRect.content.anchorMin = topCenter;
            scrollRect.content.anchorMax = topCenter;
            scrollRect.content.pivot = topCenter;
            scrollRect.content.sizeDelta = new Vector2(
                2 * margin +  cellSize.x,
                2 * margin + NumNodes * layoutPrefabSize + trailingSpace);
            viewSize = scrollRect.viewport.rect.height;
            contentStart = scrollRect.content.rect.yMax;
        }
        else
        {
            Vector2 midLeft = new Vector2(0, 0.5f);
            scrollRect.content.anchorMin = midLeft;
            scrollRect.content.anchorMax = midLeft;
            scrollRect.content.pivot = midLeft;
            scrollRect.content.sizeDelta = new Vector2(
                2 * margin + NumNodes * layoutPrefabSize + trailingSpace,
                2 * margin + cellSize.y);
            viewSize = scrollRect.viewport.rect.width;
            contentStart = 0;
        }
        screenCount = Mathf.CeilToInt(viewSize / layoutPrefabSize) ;

        InitializePool();

        initialized = true;
        scrollRect.onValueChanged.AddListener(v => UpdateScroll(PosToScroll(layoutDirection == Layout.Vertical
            ? scrollRect.content.localPosition.y
            : scrollRect.content.localPosition.x)));

        UpdateScrollView(requestScroll);
    }

    private void InitializePool()
    {
        // Support resetting everything from OnValidate
        foreach (int i in poolDict.Keys.ToArray())
        {
            MakeInvisible(i);
        }
        poolDict.Clear();
        while (pool.Count > 0)
        {
            Destroy(pool.Dequeue());
        }
        visibleStart = -1;
        visibleEnd = -1;

        // Create the pool of prefabs
        int poolSize = screenCount + 1;
        for (int i = 0; i < poolSize; i++)
        {
            GameObject go = Instantiate(prefab, ItemLocation(-(i + 1)), scrollRect.content.rotation, scrollRect.content);
            pool.Enqueue(go);
        }
    }

    private void UpdateScrollView(float newScroll)
    {
        newScroll = Mathf.Clamp(newScroll, 0, MaxScroll);

        Vector3 pos = scrollRect.content.localPosition;
        scrollRect.content.localPosition = layoutDirection == Layout.Vertical
            ? new Vector3(pos.x, ScrollToPos(newScroll), pos.z)
            : new Vector3(ScrollToPos(newScroll), pos.y, pos.z);

        UpdateScroll(newScroll);
    }

    private void UpdateScroll(float newScroll)
    {
        if ((scroll == newScroll) && visibleValid || initialized == false) { return; }
        scroll = newScroll;

        // Based on this scroll, calculate the new relevant ranges of
        // indices
        float paddedScroll = newScroll - (margin / layoutPrefabSize);
        int newVisibleStart = Math.Max(0, ((int)paddedScroll ) );
        int newVisibleEnd = Math.Min(NumNodes, Mathf.CeilToInt(paddedScroll  + (viewSize / layoutPrefabSize)) );
        
        // If it's the same as we already have, then we can just stop here!
        if (newVisibleStart != visibleStart || newVisibleEnd != visibleEnd)
        {

            // Demote all items that are no longer relevant
            for (int i = visibleStart; i < visibleEnd; i++)
            {
                bool wasVisible = i >= visibleStart && i < visibleEnd;
                bool remainsVisible = i >= newVisibleStart && i < newVisibleEnd;
                if (wasVisible == true && remainsVisible == false)
                {
                    MakeInvisible(i);
                }
            }

            // Promote all items that are now relevant
            for (int i = newVisibleStart; i < newVisibleEnd; i++)
            {
                bool wasVisible = i >= visibleStart && i < visibleEnd;
                bool nowVisible = i >= newVisibleStart && i < newVisibleEnd;
                if (wasVisible == false && nowVisible == true)
                {
                    MakeVisible(i);
                }
            }
            visibleStart = newVisibleStart;
            visibleEnd = newVisibleEnd;
            visibleValid = true;
        }
    }
    
    private void UpdateVisibleNodes()
    {
         List<int> newAssignedNodes = new List<int>();
         for (int i = 0; i < NumNodes; i++)
         {
             newAssignedNodes.Add(listPositionToDropdownNodeID(i));
         }
         
         for (int i = visibleStart; i < visibleEnd; i++)
         {
             if (i >= newAssignedNodes.Count || i >= assignedNodes.Count)
             {
                 break;
             }
             GameObject go = poolDict[i];
             
             //Update dropdown item state
             DropdownNode node = GetNode(newAssignedNodes[i]);
             IDropdownItem dropdownItem = go.GetComponent<IDropdownItem>();
             if(dropdownItem != null)
             {
                 dropdownItem.IsExpanded = node.isExpanded;
                 dropdownItem.Level = node.Level;
                 dropdownItem.IsLeaf = node.isLeaf;
             }
             
             //Reassign node if node value changed 
             if (assignedNodes[i] != newAssignedNodes[i])
             {
                 OnUnassignedNode?.Invoke(go, assignedNodes[i]);
                 OnAssignedNode?.Invoke(go, newAssignedNodes[i]);
             }
         }
         assignedNodes = newAssignedNodes;
    }

    private void MakeInvisible(int i)
    {
        if (TryGetVisible(i, out GameObject go) == false) { return; }
        poolDict.Remove(i);
        go.SetActive(false);
        pool.Enqueue(go);
        OnUnassignedNode?.Invoke(go, listPositionToDropdownNodeID(i));
    }

    private void MakeVisible(int i)
    {
        //Get game object from pool and add to visible node dict
        GameObject go = pool.Dequeue();
        go.transform.localPosition = ItemLocation(i);
        poolDict.Add(i, go);
        
        //Set node 
        int nodeID = listPositionToDropdownNodeID(i);
        DropdownNode node = GetNode(nodeID);
        IDropdownItem dropdownItem = go.GetComponent<IDropdownItem>();
        if(dropdownItem != null)
        {
            dropdownItem.IsExpanded = node.isExpanded;
            dropdownItem.Level = node.Level;
            dropdownItem.IsLeaf = node.isLeaf;
        }
        OnAssignedNode?.Invoke(go, nodeID);
        
        //Activate GameObject
        go.SetActive(true);
    }


    private void ReassignNode(int listPosition, int newNodeID)
    {
        if (assignedNodes[listPosition] != newNodeID)
        {
            int oldNodeID = assignedNodes[listPosition];
            if (TryGetVisible(listPosition, out GameObject go))
            {
                DropdownNode node = GetNode(newNodeID);
                IDropdownItem dropdownItem = go.GetComponent<IDropdownItem>();
                if(dropdownItem != null)
                {
                    dropdownItem.IsExpanded = node.isExpanded;
                    dropdownItem.Level = node.Level;
                    dropdownItem.IsLeaf = node.isLeaf;
                }

                if (oldNodeID != -1)
                {
                    OnUnassignedNode?.Invoke(go, oldNodeID);
                }

                if (newNodeID != -1)
                {
                    OnAssignedNode?.Invoke(go, newNodeID);
                }
            }
            assignedNodes[listPosition] = newNodeID;
        }
    }

    private void UpdateNumNodes()
    {
        scrollRect.content.sizeDelta = new Vector2(scrollRect.content.sizeDelta.x, margin + NumNodes * layoutPrefabSize + trailingSpace);
        contentStart = scrollRect.content.rect.yMax;
        visibleValid = false;
    }
    
    

    #endregion

    #region Public Methods

    /// <summary>
    /// This will get the prefab instance representing the given index, if
    /// it's visible. You should NOT store this instance, as it will be
    /// re-used as the content scrolls.
    /// </summary>
    /// <param name="i">
    /// List index to retrieve.
    /// </param>
    /// <param name="visibleObject">
    /// The prefab instance representing the index, or null, if not visible.
    /// </param>
    /// <returns>
    /// <see langword="true"/> if the item was visible, <see langword="false"/> if not.
    /// </returns>
    public bool TryGetVisible(int i, out GameObject visibleObject)
    {
        if (i >= visibleStart && i < visibleEnd)
        {
            visibleObject = poolDict[i];
            return true;
        }
        visibleObject = null;
        return false;
    }
    
    
    public bool TryGetVisibleFromID(int nodeID, out GameObject visibleObject)
    {
        int i = dropdownNodeIDToListPosition(nodeID);
        if (i >= visibleStart && i < visibleEnd)
        {
            visibleObject = poolDict[i];
            return true;
        }
        visibleObject = null;
        return false;
    }

    
    public int AddNode(int parentID)
    {
        DropdownNode parentNode = GetNode(parentID);
        if ( parentNode != null)
        {
            int newNodeID = NumExpandedNodes;
            parentNode.children.Add(new DropdownNode()
            {
                id = newNodeID,
                parent = parentNode,
                children = new List<DropdownNode>(),
                isExpanded = true
            });
            UpdateNumNodes();
            UpdateScrollView(scroll);
            UpdateVisibleNodes();
            return newNodeID;
        }
        else
        {
            Debug.LogWarning($"Could not add node to parent with node index {parentID}, parent node does not exist.");
            return -1;
        }
    }
    
    public void RemoveNode(int nodeID)
    {
        DropdownNode node = GetNode(nodeID);
        if (node != null)
        {
            DropdownNode parentNode = node.parent;
            if (parentNode != null)
            {
                parentNode.children.Remove(node);
                scrollRect.content.sizeDelta = new Vector2(scrollRect.content.sizeDelta.x, margin + NumNodes * layoutPrefabSize + trailingSpace);
                contentStart = scrollRect.content.rect.yMax;
                visibleValid = false;
                UpdateScrollView(scroll);
            }
            else
            {
                Debug.LogWarning($"Could not remove node with node index {nodeID}, parent node does not exist.");
            }
        }
        else
        {
            Debug.LogWarning($"Could not remove node with node index {nodeID}, node does not exist.");
        }
    }
    
        
    public void SetNodeExpanded(int nodeID, bool isExpanded)
    {
        DropdownNode node = GetNode(nodeID);
        if (node != null)
        {
            node.isExpanded = isExpanded;
            UpdateNumNodes();
            UpdateScrollView(scroll);
            UpdateVisibleNodes();
        }
        else
        {
            Debug.LogWarning($"Could not set node with node index {nodeID} expanded to {isExpanded}, node does not exist.");
        }
    }
    
    public void ToggleNodeExpanded(int nodeID)
    {
        DropdownNode node = GetNode(nodeID);
        if (node != null)
        {
            SetNodeExpanded(nodeID, !node.isExpanded);    
        }
        
    }
    
    #endregion
}

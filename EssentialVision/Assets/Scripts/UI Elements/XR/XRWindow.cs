using System.Collections.Generic;
using UnityEngine;

public class XRWindow : MonoBehaviour, IWindow
{

    [SerializeField]
    bool HideAtStart = true;
    [SerializeField]
    private float spawnDistance = 1.5f;
    [SerializeField] private int currentViewIdx = 0;
    [SerializeField] public List<GameObject> views;

    public int CurrentViewIdx
    {
        get
        {
            return currentViewIdx;
        }
        set
        {
            if (value > views.Count || value < 0)
            {
                Debug.LogError($"Window view index {value} out of bounds. View not switched.");
            }
            if (currentViewIdx != value)
            {
                views[currentViewIdx].SetActive(false);
                views[value].SetActive(true);
            }
            currentViewIdx = value;
        }
    }
    

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Open()
    {
        gameObject.SetActive(true);    
    }

    public void BringIntoFocus()
    {
        Open();
        Vector3 spawnPosition = Camera.main.transform.position + Camera.main.transform.forward * spawnDistance;
        Vector3 targetPosition = Camera.main.transform.position;
        Vector3 horizontalDirection = Vector3.ProjectOnPlane(spawnPosition - targetPosition, Vector3.up).normalized;
        Quaternion windowRotation = Quaternion.LookRotation(horizontalDirection);
        gameObject.transform.rotation = windowRotation;
        gameObject.transform.position = spawnPosition;
    }


    // Start is called before the first frame update
    void Start()
    {
        if (views.Count > 0){
            foreach (GameObject view in views)
            {
                view.SetActive(false);
            }
            views[currentViewIdx].SetActive(true);
        }
        if (HideAtStart)
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

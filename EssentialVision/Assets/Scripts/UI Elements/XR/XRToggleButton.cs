using System;
using MixedReality.Toolkit.UX;

public class XRToggleButton : PressableButton, IToggleButton
{
    public event Action OnButtonToggled;
    public event Action OnButtonUntoggled;
    private void Start()
    {
        IsToggled.OnEntered.AddListener((_) => OnButtonToggled?.Invoke());
        IsToggled.OnExited.AddListener((_) => OnButtonUntoggled?.Invoke());
    }
    
}

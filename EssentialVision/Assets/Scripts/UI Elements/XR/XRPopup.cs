using MixedReality.Toolkit.UX;
using System;
using System.Collections;
using UnityEngine;
using MixedReality.Toolkit.SpatialManipulation;

public class XRPopup : MonoBehaviour
{

    [SerializeField]
    [Tooltip("The button representing the positive action. If specified by the user, " +
         "the button will be enabled and activated, with actions hooked up through code.")]
    private PressableButton positiveButton = null;

    [SerializeField]
    [Tooltip("The button representing the negative action. If specified by the user, " +
             "the button will be enabled and activated, with actions hooked up through code.")]
    private PressableButton negativeButton = null;

    [SerializeField]
    [Tooltip("The button representing the neutral action. If specified by the user, " +
             "the button will be enabled and activated, with actions hooked up through code.")]
    private PressableButton neutralButton = null;


    public Action positiveAction = null;

    public Action negativeAction = null;

    public Action neutralAction = null;

    private Action onDismissed;

    private bool hasDismissed = false;

    /// <inheritdoc />
    public Action OnDismissed
    {
        get => onDismissed;
        set => onDismissed = value;
    }

    public virtual void Reset()
    {
        positiveAction = null;
        negativeAction = null;
        neutralAction = null;
        OnDismissed = null;
        hasDismissed = false;
    }

    protected virtual void Awake()
    {
        if (negativeButton != null)
        {
            negativeButton.OnClicked.AddListener(() => {
                negativeAction?.Invoke();
                Dismiss();
            });
        }

        if (positiveButton != null)
        {
            positiveButton.OnClicked.AddListener(() => {
                positiveAction?.Invoke();
                Dismiss();
            });
        }

        if (neutralButton != null)
        {
            neutralButton.OnClicked.AddListener(() => {
                neutralAction?.Invoke();
                Dismiss();
            });
        }

        if (gameObject.GetComponent<Follow>() == null)
        {
            Follow followSolver = gameObject.AddComponent<Follow>();
            followSolver.Smoothing = true;
            followSolver.MoveLerpTime = 1.0f;
            followSolver.RotateLerpTime = 1.0f;
            followSolver.OrientToControllerDeadZoneDegrees = 25.0f;
        }
    }

    /// <inheritdoc />
    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    /// <inheritdoc />
    public virtual void Dismiss()
    {

        // Lock. Dismissal idempotent.
        if (hasDismissed) { return; }
        hasDismissed = true;

        OnDismissed?.Invoke();
        StartCoroutine(DestroyAfterAnimation());
    }

    /// <summary>
    /// Coroutine to set the animation trigger, wait for the animation to finish,
    /// and then hide the dialog and invoke the dismissal action. This coroutine
    /// is started by the base <see cref="Dismiss"/> method once all listeners
    /// have been removed.
    private IEnumerator DestroyAfterAnimation()
    {
        Animator animator = GetComponent<Animator>();
        animator.SetTrigger("Dismiss");
        yield return null;

        // Spin while we're still animating.
        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1 || animator.IsInTransition(0))
        {
            yield return null;
        }
        Destroy(gameObject);
    }
}
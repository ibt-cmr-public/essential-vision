public interface IWindow
{
    public int CurrentViewIdx { get; set; }
    public void Close();
    public void Open();
}

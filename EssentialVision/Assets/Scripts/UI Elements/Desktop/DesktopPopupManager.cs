using UnityEngine;

public class DesktopPopupManager : PopupManager
{
    [SerializeField] private GameObject overlay;
    
    public override XRPopup SpawnPopup(Policy spawnPolicy = Policy.DismissExisting, GameObject prefab = null)
    {
        XRPopup xrPopup = base.SpawnPopup(spawnPolicy, prefab);
        overlay.SetActive(true);
        return xrPopup;
    }

    protected override void OnDialogDismissed()
    {
        base.OnDialogDismissed();
        overlay.SetActive(false);
    }
}

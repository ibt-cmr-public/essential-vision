using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RightClickButton : Button
{
    [Serializable]
    /// <summary>
    /// Function definition for a button click event.
    /// </summary>
    public class ButtonPositionClickedEvent : UnityEvent<Vector2> {}
    
    [SerializeField]
    private ButtonPositionClickedEvent m_OnRightClick = new ButtonPositionClickedEvent();
    
    public ButtonPositionClickedEvent onRightClick
    {
        get { return m_OnRightClick; }
        set { m_OnRightClick = value; }
    }

    // Start is called before the first frame update
    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            base.OnPointerClick(eventData);
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            Vector2 pointerPosition = eventData.position;
            m_OnRightClick.Invoke(pointerPosition);
        }
    }
    
}

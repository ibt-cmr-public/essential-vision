using System;
using NovaSamples.UIControls;

public class DesktopToggleButton : Toggle, IToggleButton
{
    public void Start()
    {
        OnToggled.AddListener((toggled) => 
        {
            if (toggled)
            {
                OnButtonToggled?.Invoke();
            }
            else
            {
                OnButtonUntoggled?.Invoke();
            }
        });
    }
    
    public event Action OnButtonToggled;
    public event Action OnButtonUntoggled;
}

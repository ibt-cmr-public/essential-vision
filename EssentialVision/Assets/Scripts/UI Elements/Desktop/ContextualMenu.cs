using UnityEngine;
using UnityEngine.InputSystem.UI;

public class ContextualMenu : MonoBehaviour
{
    
    [SerializeField]
    private RectTransform rectTransform;

    [SerializeField] private InputSystemUIInputModule inputModule;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
        inputModule.leftClick.action.performed += ctx => this.enabled = false;
    }

    public void MakeVisible(Vector2 position)
    {
        rectTransform.position = position;
        gameObject.SetActive(true);
    }
}

using System;

public interface IToggleButton : IComponent
{
    public event Action OnButtonToggled;
    public event Action OnButtonUntoggled;
    public bool enabled { set; }

}

using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    [SerializeField]
    public float distanceFromBounds;
    [SerializeField]
    public float minTargetDistance;
    [SerializeField]
    private GameObject targetObject;
    [SerializeField]
    private TextMeshProUGUI textElement;
    [SerializeField]
    public Camera tooltipCamera;

    [SerializeField]
    [Range(0, 360)]  public int horizontalAngle = 90;
    [SerializeField]
    [Range(-180, 180)] public int verticalAngle = 0;



    private void Start()
    {
        if (tooltipCamera == null)
        {
            tooltipCamera = Camera.main;
        }
    }


    public string Caption
    {
        get
        {
            return textElement.text;
        }
        set
        {
            textElement.text = value;
        }
    }
    
    
    public GameObject TargetObject
    {
        get { return targetObject; }
        set { targetObject = value; }
    }


    public Collider TargetCollider
    {
        get
        {
            return TargetObject.GetComponentInChildren<Collider>();
        }
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (TargetCollider == null)
        {
            return;
        }

        //Rotate to face user
        Vector3 tooltipToCamera = transform.position - Camera.main.transform.position;
        tooltipToCamera.y = 0f;
        transform.rotation = Quaternion.LookRotation(tooltipToCamera, Vector3.up);

        //Keep constant angles between user, target, and tooltip
        Vector3 targetToCamera = Camera.main.transform.position - TargetCollider.bounds.center;
        Quaternion horizontalRotation = Quaternion.AngleAxis(horizontalAngle, Camera.main.transform.up);
        Quaternion verticalRotation = Quaternion.AngleAxis(verticalAngle, Vector3.Cross(targetToCamera, Camera.main.transform.up));
        //Quaternion verticalRotation = Quaternion.Euler(-verticalAngle, 0, 0);
        Vector3 targetToTooltip = (horizontalRotation * verticalRotation * targetToCamera).normalized;

        //Determine distance from target
        // We need to cast the ray from outside the collider because otherwise no hit is detected 
        Ray ray = new Ray(TargetCollider.bounds.center, targetToTooltip);
        Ray inverseRay = new Ray(ray.GetPoint(100f), -ray.direction);
        float boundsDistance = 0f;
        if (TargetCollider.Raycast(inverseRay, out RaycastHit hit, 100f))
        {
            boundsDistance = 100f - hit.distance;
        }
        float currentDistance = Mathf.Max(minTargetDistance, distanceFromBounds + boundsDistance);
        transform.position = TargetCollider.bounds.center + currentDistance * targetToTooltip;
    }

}

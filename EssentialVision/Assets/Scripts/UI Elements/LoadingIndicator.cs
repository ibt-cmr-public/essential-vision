using TMPro;
using UnityEngine;

public class LoadingIndicator : MonoBehaviour
{
    private string loadingText = "Loading";
    [SerializeField] private TextMeshProUGUI textMesh;
    
    private float timer = 0f;
    private int currentNumDots = 0;

    public string LoadingText
    {
        get => loadingText;
        set
        {
            loadingText = value;
            textMesh.text = value;
        }
    }

    void Update()
    {
        // Increment the timer
        timer += Time.deltaTime;

        // Add a dot every second, reset after three dots
        if (timer >= 1f)
        {
            timer = 0f;

            if (currentNumDots < 3)
            {
                currentNumDots++;
            }
            else
            {
                currentNumDots = 0;
            }

            textMesh.text = loadingText + new string('.', currentNumDots);
        }
    }
}
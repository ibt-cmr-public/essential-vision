using UnityEngine.Events;

public interface IClickable: IComponent
{
    UnityEvent OnClicked { get; }
}

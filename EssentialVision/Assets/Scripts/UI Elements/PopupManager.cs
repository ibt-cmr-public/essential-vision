using MixedReality.Toolkit.UX;
using UnityEngine;


public class PopupManager : MonoBehaviour
{
    /// <summary>
    /// Specifies the <see cref="PopupManager"/>'s behavior
    /// when opening a dialog while one is already active.
    /// </summary>
    public enum Policy
    {
        /// <summary> Dismisses any existing dialog, and then spawns its own. </summary>
        DismissExisting,

        /// <summary> Aborts spawning if a dialog is already active. </summary>
        AbortIfExisting
    }

    /// <summary>
    /// The default prefab to instantiate when spawning a dialog.
    /// </summary>
    [field: SerializeField, Tooltip("The default prefab to instantiate when spawning a dialog.")]

    // Static reference to the dialog instance, if active and spawned.
    // Will be null when no dialog is open.
    private static XRPopup dialogInstance = null;


    /// <summary>
    /// Retrieves or creates a new dialog instance. Specify a <paramref name="spawnPolicy"/> to
    /// determine how existing dialogs are handled. By default, the spawner will use its <see cref="DialogPrefab"/>,
    /// but a custom <paramref name="prefab"/> can also be specified. Dialogs of different types
    /// will be pooled independently. Only one dialog can be visible at a time, globally.
    /// </summary>
    /// <remarks>
    /// To build your dialog, call the fluent builder methods on the <see cref="IDialog"/> instance retuirned
    /// by this method.
    /// </remarks>
    /// <param name="spawnPolicy">How the spawner should deal with existing dialogs (dismiss or abort)</param>
    /// <param name="prefab">The prefab to use for the dialog. If null, the spawner's <see cref="DialogPrefab"/> will be used.</param>
    /// <returns>A dialog instance, or null if the spawner was unable to spawn a dialog.</returns>
    public virtual XRPopup SpawnPopup(Policy spawnPolicy = Policy.DismissExisting, GameObject prefab = null)
    {
        if (prefab == null)
        {
            Debug.LogError("DialogPool's default dialog prefab is null.", this);
            return null;
        }

        // Is there a dialog already open, and is our policy to dismiss the existing dialog?
        if (dialogInstance != null && spawnPolicy == Policy.DismissExisting)
        {
            // Dismiss the existing dialog.
            dialogInstance.Dismiss();
        }
        else if (dialogInstance != null && spawnPolicy == Policy.AbortIfExisting)
        {
            // Otherwise, we abort.
            Debug.LogWarning("Tried to open a dialog, but one is already open. " +
                             "To dismiss existing dialogs when opening a new one, " +
                             "use DialogPool.Policy.DismissExisting.", this);
            return null;
        }


        XRPopup dialog = Instantiate(prefab).GetComponent<XRPopup>();
        dialog.OnDismissed += OnDialogDismissed;

        //Set current dialog
        dialogInstance = dialog;
        return dialog;
    }

    protected virtual void OnDialogDismissed()
    {
        dialogInstance.OnDismissed -= OnDialogDismissed;
        dialogInstance = null;
    }

}
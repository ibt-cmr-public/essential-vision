﻿#if UNITY_EDITOR
using ParrelSync;
#endif
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

/** Add this to any GameObject in a scene, to initialize and keep updating available model bundles. */
public class LibraryManager : MonoBehaviour
{
    private static LibraryManager instance;
    [SerializeField] private AssetLocations assetLocations;
    [SerializeField] private List<AssetLocations> cloneAssetLocations = new List<AssetLocations>();
    [SerializeField] private IDictionary<Guid, ModelScriptableObject> models = new Dictionary<Guid, ModelScriptableObject>();
    [SerializeField] private IDictionary<Guid, ModelCheckpointGraph> modelCheckpointGraphs = new Dictionary<Guid, ModelCheckpointGraph>();
    private string assetLocationFilePath;
    private AsyncOperationHandle<IList<ModelScriptableObject>> modelLoadHandle;
    private AsyncOperationHandle<IList<ModelCheckpointGraph>> checkpointGraphLoadHandle;
    
    public Action<ModelScriptableObject> OnModelAdded;

    // Public property to get the singleton instance
    public static LibraryManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LibraryManager>();
                if (instance == null)
                {
                    GameObject singletonObject = new GameObject(typeof(LibraryManager).Name);
                    instance = singletonObject.AddComponent<LibraryManager>();
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        // Ensure there's only one instance of the singleton
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        // Set this instance as the singleton
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        
        //Load asset locations
        assetLocationFilePath = Application.persistentDataPath + "/assetLocations.json";
        LoadAssetLocations();
        
        //Load assets
        LoadModels();
        LoadCheckpointGraphs();
    }

    
    //Handling of model asset locations
    private void LoadAssetLocations()
    {
        if (File.Exists(assetLocationFilePath))
        {
            assetLocations.LoadFromJSON(assetLocationFilePath);
        }
        #if UNITY_EDITOR
        if (ClonesManager.IsClone())
        {
            string cloneID = ClonesManager.GetArgument();
            if (cloneAssetLocations.Count > int.Parse(cloneID))
                assetLocations = cloneAssetLocations[int.Parse(cloneID)];
        }
        #endif
    }

    private void SaveAssetLocations()
    {
        assetLocations.SaveToJSON(assetLocationFilePath);
    }
    
    
    //Adding models at runtime (in working memory)
    public void AddModel(ModelScriptableObject model)
    {
        models[model.guid] = model;
        OnModelAdded?.Invoke(model);
    }
    
    //Adding models permanantly to library
    private void AddModelFromAddressable(string addressableLocation)
    {
        if (!assetLocations.ModelAddressableLocations.Contains(addressableLocation))
        {
            assetLocations.ModelAddressableLocations.Add(addressableLocation);
            SaveAssetLocations();
        }
    }
    
    private void AddModelFromFile(string fileLocation)
    {
        if (!assetLocations.ModelFileLocations.Contains(fileLocation))
        {
            assetLocations.ModelFileLocations.Add(fileLocation);
            SaveAssetLocations();
        }
    }
    
    
    //Loading models
    private void LoadModels()
    {
        // Load addressable models
        if (assetLocations.ModelAddressableLocations != null && assetLocations.ModelAddressableLocations.Count != 0)
        {
            modelLoadHandle = Addressables.LoadAssetsAsync<ModelScriptableObject>(
                assetLocations.ModelAddressableLocations,
                (loadedModel) => { models[loadedModel.guid] = loadedModel; },
                Addressables.MergeMode.Union,
                true);
            IList<ModelScriptableObject> addressableModels = modelLoadHandle.WaitForCompletion();
        }

        foreach (string fileLocation in assetLocations.ModelFileLocations)
        {
            ModelScriptableObject model;
            try
            {
                model = ModelScriptableObject.FromFile(fileLocation);
            }
            catch (Exception e)
            {
                Debug.LogError("Could not load model from file: " + fileLocation + " with error: " + e.Message);
                continue;
            }
            
            if (model != null)
            {
                models[model.guid] = model;
            }
        }
    }

    private void LoadCheckpointGraphs()
    {
        if (assetLocations.CheckpointAddressableLocations == null || assetLocations.CheckpointAddressableLocations.Count == 0)
        {
            return;
        }
        
        checkpointGraphLoadHandle = Addressables.LoadAssetsAsync<ModelCheckpointGraph>(
            assetLocations.CheckpointAddressableLocations,
            (loadedCourse) => {
                if (modelCheckpointGraphs.ContainsKey(loadedCourse.guid))
                {
                    modelCheckpointGraphs[loadedCourse.guid] = loadedCourse;
                }
                else
                {
                    modelCheckpointGraphs.Add(loadedCourse.guid, loadedCourse);
                }
            },
            Addressables.MergeMode.Union,
            true);
    }

    public ModelScriptableObject GetModel(Guid modelGuid, bool throwErrorIfNotFound = true)
    {
        if (!models.ContainsKey(modelGuid))
        {
            if (throwErrorIfNotFound)
            {
                throw new ArgumentException("Could not find model with name " + modelGuid);
            }
            return null;
        }
        return models[modelGuid]; 
    }
    
    public ModelScriptableObject GetModel(string modelGuid, bool throwErrorIfNotFound = true)
    {
        return GetModel(new Guid(modelGuid), throwErrorIfNotFound);
    }
    
    public ModelNodeScriptableObject GetNode(Guid modelGuid, Guid nodeGuid, bool throwErrorIfNotFound = true)
    {
        ModelScriptableObject model = GetModel(modelGuid, throwErrorIfNotFound);
        if (model == null)
        {
            return null;
        }
        ModelNodeScriptableObject node = model.GetNode(nodeGuid);
        if (node == null && throwErrorIfNotFound)
        {
            throw new ArgumentException("Could not find node with name " + nodeGuid);
        }
        return node;
    }
    
    public ModelNodeScriptableObject GetNode(string modelGuid, string nodeGuid, bool throwErrorIfNotFound = true)
    {
        return GetNode(new Guid(modelGuid), new Guid(nodeGuid), throwErrorIfNotFound);
    }
    
    public ModelCheckpointGraph GetGraph(Guid graphGuid, bool throwErrorIfNotFound = true)
    {
        if (!modelCheckpointGraphs.ContainsKey(graphGuid))
        {
            if (throwErrorIfNotFound)
            {
                throw new ArgumentException("Could not find graph with name " + graphGuid);
            }

            return null;
        }
        return modelCheckpointGraphs[graphGuid]; 
    }
    
    public ModelCheckpointGraph GetGraph(string graphGuid, bool throwErrorIfNotFound = true)
    {
        return GetGraph(new Guid(graphGuid), throwErrorIfNotFound);
    }
    

    public List<ModelScriptableObject> Models
    {
        get
        {
            return models.Values.ToList();
        }
    }
    
    public List<ModelCheckpointGraph> CheckpointGraphs
    {
        get
        {
            return modelCheckpointGraphs.Values.ToList();
        }
    }
    
    public List<ModelCheckpointGraph> CheckpointGraphsForModel(string modelName)
    {
           return modelCheckpointGraphs.Values.Where(graph => graph.modelName == modelName).ToList();
    }
    

    private void OnDestroy()
    {
        if (modelLoadHandle.IsValid()) Addressables.Release(modelLoadHandle);
        if (checkpointGraphLoadHandle.IsValid()) Addressables.Release(checkpointGraphLoadHandle);
    }

    private void OnApplicationQuit()
    {
        if (modelLoadHandle.IsValid()) Addressables.Release(modelLoadHandle);
        if (checkpointGraphLoadHandle.IsValid()) Addressables.Release(checkpointGraphLoadHandle);
    }
}

using System;
using Nova;
using System.Collections.Generic;
using UnityEngine;

public class ModelTabMenu : MonoBehaviour
{

    [SerializeField]
    public List<DesktopModelSetup> ModelInstancesCollection = null;
    public ListView TabBar = null;
    public int selectedIndex = -1;
    private ModelInstanceManager modelInstanceManager;
    private bool isInitialized = false;
    
    
    public ModelInstanceManager ModelInstanceManager
    {
        set
        {
            Initialize();
            if (modelInstanceManager == value)
            {
                return;
            }
            modelInstanceManager = value;
            TabBar.SetDataSource(modelInstanceManager.modelInstances.Values);
            if (TabBar.TryGetItemView(0, out ItemView firstTab))
            {
                SelectTab(firstTab.Visuals as DesktopTabButtonVisuals, 0);
            }
            modelInstanceManager.OnModelInstanceSpawned += OnModelInstanceSpawned;
        }
    }

    private void OnModelInstanceSpawned(ModelSetup modelSetup)
    {
        modelSetup.OnModelViewSet += OnModelInstanceViewSet;
        TabBar.Refresh();
        int newIndex = modelInstanceManager.modelInstances.Count - 1;
        if (TabBar.TryGetItemView(newIndex, out ItemView currentItemView))
        {
            SelectTab((DesktopTabButtonVisuals)currentItemView.Visuals, newIndex);
        }
    }
    
    private void OnModelInstanceViewSet(Model3DView modelView)
    {
        TabBar.Refresh();
    }

    private void Start()
    {
        Initialize();
        ModelInstanceManager = FindObjectOfType<ModelInstanceManager>();
    }

    // Add gesture handlers and data binder
    private void Initialize()
    { 
        if(isInitialized)
        {
            return;
        }
        TabBar.AddDataBinder<DesktopModelSetup, DesktopTabButtonVisuals>(BindTab);
        TabBar.AddGestureHandler<Gesture.OnClick, DesktopTabButtonVisuals>(HandleTabClicked);
        TabBar.AddGestureHandler<Gesture.OnHover, DesktopTabButtonVisuals>(DesktopTabButtonVisuals.HandleHover);
        TabBar.AddGestureHandler<Gesture.OnPress, DesktopTabButtonVisuals>(DesktopTabButtonVisuals.HandlePress);
        TabBar.AddGestureHandler<Gesture.OnRelease, DesktopTabButtonVisuals>(DesktopTabButtonVisuals.HandleRelease);
        TabBar.AddGestureHandler<Gesture.OnUnhover, DesktopTabButtonVisuals>(DesktopTabButtonVisuals.HandleUnhover);
        TabBar.SetDataSource(ModelInstancesCollection);
        isInitialized = true;
    }

    // private void OnDisable()
    // {
    //     // Remove the tab view's databinder
    //     TabBar.RemoveDataBinder<DesktopModelInstance, DesktopTabButtonVisuals>(BindTab);
    //
    //     // Remove the tab view's gesture handlers
    //     TabBar.RemoveGestureHandler<Gesture.OnClick, DesktopTabButtonVisuals>(HandleTabClicked);
    // }

    private void HandleTabClicked(Gesture.OnClick evt, DesktopTabButtonVisuals target, int index)
    {
        SelectTab(target, index);
    }

    private void SelectTab(DesktopTabButtonVisuals visuals, int index)
    {
        if (index == selectedIndex)
        {
            return;
        }
        
        if (index < 0 || index >= modelInstanceManager.modelInstances.Count)
        {
            throw new ArgumentOutOfRangeException("Index out of range");
        }
        
        if (selectedIndex >= 0 && TabBar.TryGetItemView(selectedIndex, out ItemView currentItemView))
        {
            (currentItemView.Visuals as DesktopTabButtonVisuals).IsSelected = false;
        }

        selectedIndex = index;
        visuals.IsSelected = true;
        foreach (var modelInstance in modelInstanceManager.modelInstances)
        {
            modelInstance.Hide();
        }

        modelInstanceManager.modelInstances[index].Show();

    }

    private void BindTab(Data.OnBind<DesktopModelSetup> evt, DesktopTabButtonVisuals target, int index)
    {
        target.IsSelected = false;
        target.Label.Text = evt.UserData.ModelName;
    }
}

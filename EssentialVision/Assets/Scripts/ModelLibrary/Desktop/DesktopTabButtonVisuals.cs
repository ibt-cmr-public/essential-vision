using Nova;
using UnityEngine;

public class DesktopTabButtonVisuals : ItemVisuals
{
    public TextBlock Label = null;
    public UIBlock2D Background = null;
    public Color UnselectedColor;
    public Color SelectedColor;
    public Color HoverColor;
    public Color PressedColor;

    private bool selectedState = false;

    public bool IsSelected
    {
        get => selectedState;
        set
        {
            selectedState = value;
            Background.Color = DefaultColor;
        }
    }

    public Color DefaultColor
    {
        get
        {
            return selectedState ? SelectedColor : UnselectedColor;
        }
    }

    internal static void HandleHover(Gesture.OnHover evt, DesktopTabButtonVisuals target, int index)
    {
        if (!target.selectedState)
        {
            target.Background.Color = target.HoverColor;    
        }
    }

    internal static void HandlePress(Gesture.OnPress evt, DesktopTabButtonVisuals target, int index)
    {
        target.Background.Color = target.PressedColor;
    }

    internal static void HandleRelease(Gesture.OnRelease evt, DesktopTabButtonVisuals target, int index)
    {
        target.Background.Color = target.DefaultColor;
    }

    internal static void HandleUnhover(Gesture.OnUnhover evt, DesktopTabButtonVisuals target, int index)
    {
        target.Background.Color = target.DefaultColor;
    }
}

using UnityEngine;

public class ModelItemUIHandler : MonoBehaviour
{
    public GameObject ModelContent;

    public void ToggleModelContent()
    {
        ModelContent.SetActive(!ModelContent.activeSelf);
    }
}

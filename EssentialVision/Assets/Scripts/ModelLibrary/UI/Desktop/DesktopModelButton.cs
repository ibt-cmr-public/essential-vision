using Nova;
using UnityEngine;

public class DesktopModelButton : DesktopToggleButton , IModelButton
{
    [SerializeField] private TextBlock buttonLabel;
    [SerializeField] private TextBlock descriptionLabel;
    [SerializeField] private UIBlock2D IconImage;
    public void SetModel(ModelScriptableObject model)
    {
        if (model != null)
        {
            buttonLabel.Text = model.Name;
            descriptionLabel.Text = model.Description;
            if (model.IconSprite != null)
            {
                IconImage.gameObject.SetActive(true);
                IconImage.SetImage(model.IconSprite);
            }
            else
            {
               IconImage.gameObject.SetActive(false);
            }
        }
    }
}

using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.Serialization;

public class ModelLibraryUI : MonoBehaviour
{
    private bool isModelSelected = true;
    private Guid selectedModelID;
    protected Dictionary<Guid, IModelButton> modelSelectionButtons = new Dictionary<Guid, IModelButton>();

    [Header("UI Elements")]
    [SerializeField]
    public GameObject ModelButtonPrefab;
    [SerializeField]
    private GameObject modelButtonCollection;
   
    
    [Header("Global Managers")]
    [SerializeField]
    public LibraryManager libraryManager;
    [FormerlySerializedAs("modelSpawner")] [FormerlySerializedAs("modelInstanceSpawner")] [SerializeField]
    public ModelInstanceManager modelInstanceManager;


    // Start is called before the first frame update
    protected void Start()
    {
        libraryManager = LibraryManager.Instance;
        libraryManager.OnModelAdded += (value) => { UpdateModelButton(value); };
        modelInstanceManager = FindObjectOfType<ModelInstanceManager>();
        List<ModelScriptableObject> models = libraryManager.Models;
        InitializeSelectionButtons(models);
    }

    public void SetModels(List<ModelScriptableObject> models)
    {
        InitializeSelectionButtons(models);
    }

    protected virtual void InitializeSelectionButtons(List<ModelScriptableObject> models)
    {
        
        //Delete all initial children of model button collection
        foreach (Transform child in modelButtonCollection.transform)
        {
            Destroy(child.gameObject);
        }
        
        //Add buttons for all loaded models
        foreach (ModelScriptableObject model in models)
        {
            AddModelButton(model);
        }
        
        SelectDefault();
    }

    private void SelectDefault()
    {
        if (modelSelectionButtons.Count > 0)
        {
            SelectModel(modelSelectionButtons.Keys.First());
        }
        else
        {
            DeselectModel();
        }
    }
    
    
    //Button management
    private void RemoveAllButtons()
    {
        foreach (Guid modelID in modelSelectionButtons.Keys)
        {
            RemoveModelButton(modelID);
        }
    }
    
    protected virtual void AddModelButton (ModelScriptableObject model)
    {
        IModelButton button = Instantiate(ModelButtonPrefab, modelButtonCollection.transform).GetComponent<IModelButton>();
        button.SetModel(model);
        button.OnButtonToggled += () => { SelectModel(model.guid); };
        modelSelectionButtons.Add(model.guid, button);
    }
    
    protected virtual void RemoveModelButton (Guid modelID)
    {
        if (modelSelectionButtons.ContainsKey(modelID))
        {
            IModelButton button = modelSelectionButtons[modelID];
            modelSelectionButtons.Remove(modelID);
            Destroy(button.gameObject);
            if (modelID == selectedModelID)
            {
                SelectDefault();
            }
        }
    }
    
    protected virtual void UpdateModelButton (ModelScriptableObject model)
    {
        if (modelSelectionButtons.ContainsKey(model.guid))
        {
            IModelButton button = modelSelectionButtons[model.guid];
            button.SetModel(model);
        }
        else
        {
            AddModelButton(model);
        }
    }

    //Logic
    public void SelectModel(Guid modelID)
    {
        SelectedModelID = modelID;
    }

    public void DeselectModel()
    {
        IsModelSelected = false;
    }

    public virtual Guid SelectedModelID {
        get {
            return selectedModelID;
        }
        set
        {
            if (selectedModelID == value)
            {
                return;
            }
            selectedModelID = value;
        }
    }
    
    public virtual bool IsModelSelected
    {
        get
        {
            return isModelSelected;
        }
        set
        {
            isModelSelected = value;
        }
    }
    
    //Events
    public void OnClickSpawn()
    {
        if (PlatformManager.Instance.GetPlatformType() == PlatformManager.PlatformType.XR)
        {
            modelInstanceManager.SpawnModelWithPlacement(selectedModelID);
        }
        else
        {
            modelInstanceManager.SpawnModelServerRpc(selectedModelID.ToString(), Vector3.zero);
        }

    }

    public void OnClickNetworkSpawn()
    {
        OnClickSpawn();
    }

    public void OnClickEdit()
    {
        PlatformManager.PlatformType[] platformTypes = new PlatformManager.PlatformType[] {PlatformManager.PlatformType.Desktop};
        ModelInstanceManager.ModelInstanceType[] modelInstanceTypes = new ModelInstanceManager.ModelInstanceType[] {ModelInstanceManager.ModelInstanceType.DesktopEdit};
        modelInstanceManager.SpawnModelServerRpc(selectedModelID.ToString(), Vector3.zero, platformTypes, modelInstanceTypes);
    }
    
    public void OnClickAddModel()
    {
        ModelScriptableObject model = ScriptableObject.CreateInstance<ModelScriptableObject>();
        libraryManager.AddModel(model);
    }

}

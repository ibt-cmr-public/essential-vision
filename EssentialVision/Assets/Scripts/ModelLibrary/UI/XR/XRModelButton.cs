using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class XRModelButton : XRToggleButton, IModelButton
{
    [SerializeField]
    private TextMeshProUGUI buttonLabel;
    [SerializeField]
    private Image buttonImage;
    [SerializeField]
    private Sprite defaultIconSprite;
    
    
    public void SetModel(ModelScriptableObject model)
    {
        if (model != null)
        {
            buttonLabel.text = model.Name;
            if (model.IconSprite != null)
            {
                buttonImage.sprite = model.IconSprite;
            }
            else {
                buttonImage.sprite = defaultIconSprite;
            }
           
        }
    }
    
}

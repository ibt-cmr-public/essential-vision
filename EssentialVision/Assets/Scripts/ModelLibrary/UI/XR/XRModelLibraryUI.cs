using System;
using MixedReality.Toolkit;
using MixedReality.Toolkit.UX;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class XRModelLibraryUI : ModelLibraryUI
{
    [SerializeField] private ToggleCollection modelButtonCollection;
    
    [SerializeField]
    public TextMeshProUGUI ModelDescriptionText;
    [SerializeField]
    public TextMeshProUGUI ModelTitleText;
    [SerializeField]
    public Image ModelIcon;
    
    protected override void AddModelButton(ModelScriptableObject model)
    {
        base.AddModelButton(model);
        StatefulInteractable buttonInteractable = modelSelectionButtons[model.guid].gameObject.GetComponent<StatefulInteractable>();
        modelButtonCollection.Toggles.Add(buttonInteractable);
    }
    
    protected override void RemoveModelButton(Guid modelID)
    {
        if (!modelSelectionButtons.ContainsKey(modelID))
        {
            return;
        }
        
        StatefulInteractable buttonInteractable = modelSelectionButtons[modelID].gameObject.GetComponent<StatefulInteractable>();
        modelButtonCollection.Toggles.Remove(buttonInteractable);
        base.RemoveModelButton(modelID);
    }


    public override Guid SelectedModelID
    {
        set 
        {
            base.SelectedModelID = value;

            //Set selected model view
            ModelScriptableObject model = libraryManager.GetModel(value, true);
            ModelTitleText.text = model.Name;
            ModelDescriptionText.text = model.Description;
            if (model.IconSprite != null)
            {
                ModelIcon.enabled = true;
                ModelIcon.sprite = model.IconSprite;
            }
            else
            {
                ModelIcon.enabled = false;
            }
            
            //set buttons
            foreach (StatefulInteractable button in modelButtonCollection.Toggles)
            {
                button.ForceSetToggled(false, false);
            }
            StatefulInteractable buttonInteractable = modelSelectionButtons[value].gameObject.GetComponent<StatefulInteractable>();
            buttonInteractable.ForceSetToggled(true, false);
        }
    }
    
    public override bool IsModelSelected
    {
        set
        {
            base.IsModelSelected = value;
            if (!value)
            {
                foreach (StatefulInteractable button in modelButtonCollection.Toggles)
                {
                    button.ForceSetToggled(false, false);
                }
            }
            ModelTitleText.text = "Select a model";
            ModelDescriptionText.text = "";
            ModelIcon.enabled = false;
        }
    }
    
    
}

public interface IModelButton: IToggleButton
{
    public void SetModel(ModelScriptableObject model);
}

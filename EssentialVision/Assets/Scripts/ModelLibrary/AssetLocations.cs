using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultAssetLocations", menuName = "EssentialVision/Other/Asset Locations")]
public class AssetLocations : ScriptableObject
{
    public List<string> ModelAddressableLocations;
    public List<string> ModelFileLocations;
    public List<string> CheckpointAddressableLocations;
    public List<string> CheckpointFileLocations;
    
    public void LoadFromJSON(string filePath)
    {
        if (File.Exists(filePath))
        {
            string json = File.ReadAllText(filePath);
            JsonUtility.FromJsonOverwrite(json, this);
        }
        else
        {
            Debug.LogError("JSON file does not exist at path: " + filePath);
        }
    }

    public void SaveToJSON(string filePath)
    {
        string json = JsonUtility.ToJson(this, true);
        File.WriteAllText(filePath, json);
    }
}

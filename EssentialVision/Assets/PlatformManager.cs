using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    public enum PlatformType
    {
        Desktop,
        XR
    }
    
    [SerializeField] private PlatformType editorPlatformType = PlatformType.Desktop;
    [SerializeField] private List<PlatformType> cloneEditorPlatformTypes = new List<PlatformType>();
    
    // Singleton principle backed by rpivate variable
    private static PlatformManager _instance;
    public static PlatformManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<PlatformManager>();
            }
            return _instance;
        }
    }
    
    //Only one should exist ever
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public PlatformType GetPlatformType()
    {
        #if UNITY_EDITOR
        if (ParrelSync.ClonesManager.IsClone())
        {
            try
            {
                return cloneEditorPlatformTypes[int.Parse(ParrelSync.ClonesManager.GetArgument())];    
            }
            catch (System.Exception e)
            {
                return editorPlatformType;
            }
            
        }
        else
        {
            return editorPlatformType;    
        }
        #else 
        if (Application.platform == RuntimePlatform.WSAPlayerARM ||
                 Application.platform == RuntimePlatform.WSAPlayerX64 ||
                 Application.platform == RuntimePlatform.WSAPlayerX86)
        {
            return PlatformType.XR;
        }
        else
        {
            return PlatformType.Desktop;
        }
        #endif
    }
    
}

using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using OpenCover.Framework.Model;
using UnityEngine;
using File = System.IO.File;

/* Classes that contain information about a model and layers, usually deserialized from ModelInfo.json. */

namespace ModelImport
{
    // Information about a whole model, usually deserialized from ModelInfo.json.
    public class ModelInfo
    {
	    public string Name;
        public string Description;
        // Icon filename.
        // Initially (when deserialized from JSON) this is relative to ModelInfo.json,
        // but it is converted to an absolute path during SingleModel.ReadInfoFile,
        // so most of the code can safely assume it's an absolute filename.
        // Must be a PNG file now.
        public string Icon;
        public ModelNodeInfo ModelTree;
        
        public IEnumerable<ModelNodeInfo> Nodes
        {
            get
            {
                return ModelTree.Subnodes;
            }
        }
        
        public static ModelInfo FromJsonFile(string filePath, string RootDirectory = "")
        {
            ModelInfo Info = null;
            
            if (!File.Exists(filePath))
            {
                Debug.LogError("File not found: " + filePath); 
            }
        
            using (StreamReader r = new StreamReader(filePath))
            {
                try
                {
                    string json = r.ReadToEnd();
                    Info =  JsonConvert.DeserializeObject<ModelInfo>(json);
                }
                catch (JsonReaderException ex)
                {
                    Debug.LogError("Corrupted ModelInfo.json file:" + ex.Message);
                }
            }
        
            // convert some paths to be absolute filenames
            foreach (ModelNodeInfo nodeInfo in Info.Nodes)
            {
                Debug.Log(nodeInfo.AssetFileName + "____________" + nodeInfo.DataType);
                if (nodeInfo.Directory != null) { 
                    nodeInfo.Directory = RootDirectory + @"\" + nodeInfo.Directory;
                }
            }
            if (!string.IsNullOrEmpty(Info.Icon))
            {
                Info.Icon = RootDirectory + @"\" + Info.Icon;
            }

            return Info;
        }
	}

    // Information about a single layer.
    public class ModelNodeInfo
    {
	    public string Name;
        public string Description;
		public string DataType;
		// Directory with VTK or GameObject models inside.
		// Initially (when deserialized) this is relative to ModelInfo.json,
        // but it is converted to an absolute path during SingleModel.ReadInfoFile,
        // so most of the code can safely assume it's an absolute filename.
		public string Directory;
        // In case of using the importer to load GameObject from Unity Assets,
        // this indicates the GameObject filename (appended to the Directory).
        public string AssetFileName;
        public string SpecialMaterial;
        // the colorscale has a min, max and unit to be shown min-max-unit
        public string ColorScaleVariables;
        public string RGB;
        public bool UseAsIcon;
        public List<ModelNodeInfo> Children = new List<ModelNodeInfo>();
        
        public IEnumerable<ModelNodeInfo> Subnodes
        {
            get
            {
                yield return this;
                foreach (var child in Children)
                {
                    yield return child;
                }
            }
        }
        public Color Color
        {
            get
            {
                if (RGB == null)
                {
                    return Color.white;
                }
                string[] rgbValues = RGB.Split(',');

                if (rgbValues.Length != 3)
                {
                    Debug.LogError("Invalid color string format. Expected format: '255,123,223'");
                    return Color.white; // Return a default color if the format is invalid
                }

                int red, green, blue;
                if (!int.TryParse(rgbValues[0].Trim(), out red) || !int.TryParse(rgbValues[1].Trim(), out green) || !int.TryParse(rgbValues[2].Trim(), out blue))
                {
                    Debug.LogError("Invalid color component values. Expected integer values ranging from 0 to 255.");
                    return Color.white; // Return a default color if the component values are invalid
                }

                return new Color(red / 255f, green / 255f, blue / 255f);
            }
        }

        public NodeType NodeType
        {
            get
            {
                switch (DataType)
                {
                    case "anatomy": 
                    case "anatomyRGB":
                        return NodeType.Mesh;
                    case "flow":
                        return NodeType.Flow;
                    case "turbulence":
                        return NodeType.Turbulence;
                    case "displacement":
                        return NodeType.Displacement;
                    default:
                        return NodeType.Empty;
                }
            }
        }
    }
}

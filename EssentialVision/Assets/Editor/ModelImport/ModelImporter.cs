﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using TreeView;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;

namespace ModelImport
{
    public class ModelImporter
    {
        public ModelInfo Info { get; protected set; }
        public ModelScriptableObject ImportedModel;
        public string ModelInfoFilePath { get; protected set;}
        public string OutputRootDirectory { get; protected set; }

        //Loads a single model, with its body and/or simulationData.
        public ModelImporter(string modelInfoFilePath)
        {
            ModelInfoFilePath = modelInfoFilePath;
        }

        public void ImportModel()
        {
            
            ReadInfoFile();

            //Create output asset directory for the imported model
            OutputRootDirectory = "Assets/Models/" + Info.Name;
            AssetDirs.CreateAssetDirectory(OutputRootDirectory);
            AssetDirs.CreateAssetDirectory(OutputRootDirectory + "/Meshes");
            AssetDirs.CreateAssetDirectory(OutputRootDirectory + "/Nodes");


            //Make model asset folder addressable and put in its own addressables group
            AddressableAssetSettings assetSettings = AddressableAssetSettingsDefaultObject.Settings;
            var modelGroup = assetSettings.FindGroup(Info.Name);
            if (!modelGroup)
                modelGroup = assetSettings.CreateGroup(Info.Name, false, false, true, null, typeof(ContentUpdateGroupSchema), typeof(BundledAssetGroupSchema));
            assetSettings.CreateOrMoveEntry(AssetDatabase.AssetPathToGUID(OutputRootDirectory + "/Meshes"), modelGroup);

            //Make model info files addressable and put in model info group
            var modelInfoGroup = assetSettings.FindGroup(ApplicationDefaults.modelInfoAssetGroupName);
            if (!modelInfoGroup)
                modelInfoGroup = assetSettings.CreateGroup(Info.Name, false, false, true, null, typeof(ContentUpdateGroupSchema), typeof(BundledAssetGroupSchema));
            assetSettings.CreateOrMoveEntry(AssetDatabase.AssetPathToGUID(OutputRootDirectory + "/Nodes"), modelInfoGroup);

            //Create ModelScriptableObject
            ImportedModel = ScriptableObject.CreateInstance<ModelScriptableObject>();
            ImportedModel.Name = Info.Name;
            ImportedModel.Description = Info.Description;
            
            //Import each node
            TreeNode<ModelNodeScriptableObject> RecursivelyImportModelNode(ModelNodeInfo nodeInfo)
            {
                Debug.Log("Importing node: \"" + nodeInfo.Name);
                ModelNodeScriptableObject importedNode;
                if (nodeInfo.NodeType == NodeType.Empty)
                {
                    importedNode = ScriptableObject.CreateInstance<ModelNodeScriptableObject>();
                    importedNode.Name = nodeInfo.Name;
                    importedNode.Description = nodeInfo.Description;
                }
                else
                {
                    //Import mesh
                    MeshImporter meshImporter = new MeshImporter();
                    string[] filePaths = GetFilepaths(nodeInfo.Directory);
                    Mesh importedMesh;
                    if (filePaths.Length == 1)
                    {
                        importedMesh = meshImporter.StaticMeshFromVTK(filePaths[0], nodeInfo.NodeType);
                    }
                    else
                    {
                        importedMesh = meshImporter.AnimatedMeshFromVTK(filePaths.ToList(), nodeInfo.NodeType);
                    }
                    string meshPath = OutputRootDirectory + "/Meshes/" + nodeInfo.Name + ".asset";
                    AssetDatabase.CreateAsset(importedMesh, meshPath);
                    
                    //Create and return LayerScriptableObject
                    LayerScriptableObject layerNode = ScriptableObject.CreateInstance<LayerScriptableObject>();
                    layerNode = ScriptableObject.CreateInstance<LayerScriptableObject>();
                    layerNode.Name = nodeInfo.Name;
                    layerNode.Description = nodeInfo.Description;
                    layerNode.MeshReference = new AssetReferenceT<Mesh>(AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(importedMesh)));
                    layerNode.Color = nodeInfo.Color;
                    importedNode = layerNode;
                }
                AssetDatabase.CreateAsset(importedNode, OutputRootDirectory + "/Nodes/" + nodeInfo.Name + ".asset");
                TreeNode<ModelNodeScriptableObject> node = new TreeNode<ModelNodeScriptableObject>(importedNode);
                foreach (ModelNodeInfo childNodeInfo in nodeInfo.Children)
                    node.AddChild(RecursivelyImportModelNode(childNodeInfo));
                return node;
            }
            ImportedModel.ModelTree =RecursivelyImportModelNode(Info.ModelTree);
            
            //Import icon (after creating ImportedModel)
            ImportIcon();

            //Create model asset
            AssetDatabase.CreateAsset(ImportedModel, OutputRootDirectory + "/Model.asset");
            assetSettings.CreateOrMoveEntry(AssetDatabase.AssetPathToGUID(OutputRootDirectory + "/Model.asset"), modelInfoGroup);
        }
        protected void ReadInfoFile()
        {
            if (!File.Exists(ModelInfoFilePath))
            {
                throw new FileNotFoundException("ModelInfo file not found at: " + ModelInfoFilePath);
            }

            using (StreamReader r = new StreamReader(ModelInfoFilePath))
            {
                try
                {
                    string json = r.ReadToEnd();
                    Info = JsonConvert.DeserializeObject<ModelInfo>(json);
                    Debug.Log(json);
                }
                catch (JsonReaderException ex)
                {
                    throw new JsonReaderException("Corrupted ModelInfo.json file!");
                }
            }

            // convert some paths to be absolute filenames
            string RootDirectory = Path.GetDirectoryName(ModelInfoFilePath);
            foreach (ModelNodeInfo nodeInfo in Info.Nodes)
            {
                Debug.Log(nodeInfo.AssetFileName + "____________" + nodeInfo.DataType);
                if (nodeInfo.Directory != null) { 
                    nodeInfo.Directory = RootDirectory + @"\" + nodeInfo.Directory;
                }
            }
            if (!string.IsNullOrEmpty(Info.Icon))
            {
                Info.Icon = RootDirectory + @"\" + Info.Icon;
            }
        }
        
        private Texture2D layerAutomaticIcon;
        protected void LayerAutomaticIconGenerate(UnityEngine.Object obj)
        { 
            layerAutomaticIcon = IconGenerator.GetIcon(obj);
        }

        private void ImportIcon()
        {
            bool hasIconFileName = !string.IsNullOrEmpty(Info.Icon);
            if (hasIconFileName || layerAutomaticIcon != null)
            {
                /* Note: At one point I tried to optimize it, by detecting when
                 * icon is already in the assets
                 * ( Info.IconFileName.StartsWith(Application.dataPath) )
                 * and then just adding the existing asset-relative path to AssetPaths.
                 * 
                 * But it doesn't work: we need the file to be called "icon.asset"
                 * ("icon.png" is ignored by Unity bundle building, as it has unrecognized
                 * extension). So we need to read + write the file anyway.
                 */

                Texture2D texture;
                if (hasIconFileName) {
                    byte[] data = File.ReadAllBytes(Info.Icon);
                    texture = new Texture2D(1, 1);
                    texture.LoadImage(data);
                } else
                {
                    texture = layerAutomaticIcon;
                }

                string iconAssetPath = OutputRootDirectory + "/Assets/icon.asset";
                AssetDatabase.CreateAsset(texture, iconAssetPath);
                ImportedModel.Icon = texture;
            }
        }
        
        private void PrepareForPreview(GameObject go, ModelNodeInfo nodeInfo)
        {
            SkinnedMeshRenderer renderer = go.GetComponent<SkinnedMeshRenderer>();
            if (renderer != null && 
                renderer.sharedMesh != null &&
                renderer.sharedMesh.blendShapeCount != 0)
            {
                renderer.SetBlendShapeWeight(0, 100f);
                Material defaultMaterial = AssetDatabase.LoadAssetAtPath<Material>(ApplicationDefaults.DefaultOpaqueMaterials[nodeInfo.NodeType]);
                if (defaultMaterial == null)
                {
                    throw new FileNotFoundException("Cannot read default material asset from " + ApplicationDefaults.DefaultOpaqueMaterials[nodeInfo.NodeType]);
                }
                renderer.material = defaultMaterial;
                
            }
        }
        
        private string[] GetFilepaths(string layerDirectory)
        {
            string[] filePaths;
            try
            {
                filePaths = Directory.GetFiles(layerDirectory + @"\", "*.txt");
                Array.Sort(filePaths);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new DirectoryNotFoundException("Directory does not exist at: " + layerDirectory);
            }
            if (filePaths == null)
            {
                throw new FileNotFoundException("No files found in: " + layerDirectory);
            }
            return filePaths;
        }
        
    }
}

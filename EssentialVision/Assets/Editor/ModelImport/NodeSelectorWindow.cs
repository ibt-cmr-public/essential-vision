using UnityEditor;
using UnityEngine;

public class NodeSelectorWindow : EditorWindow
{
    private static NodeSelectorWindow window;
    private static NodeType selectedNodeType = NodeType.Empty;

    public static NodeType SelectedNodeType => selectedNodeType;

    [MenuItem("Window/Node Selector")]
    public static void ShowWindow()
    {
        window = GetWindow<NodeSelectorWindow>("Node Selector");
    }

    private void OnGUI()
    {
        GUILayout.Label("Select Node Type", EditorStyles.boldLabel);
        selectedNodeType = (NodeType)EditorGUILayout.EnumPopup("Node Type:", selectedNodeType);

        if (GUILayout.Button("Import"))
        {
            window.Close();
        }
    }
}
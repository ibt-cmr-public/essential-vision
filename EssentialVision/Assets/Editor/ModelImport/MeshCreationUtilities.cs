using ModelImport;
using System.IO;
using System.Linq;
using ModelConversion;
using UnityEditor;
using UnityEngine;
using ModelImporter = ModelImport.ModelImporter;

public class MeshCreationUtilities
{
    [MenuItem("ModelCreation/Create a mesh from an external format.")]
    public static void ImportWithConversion()
    {
        string inputDirectory = EditorUtility.OpenFolderPanel("Choose input directory with vtk files.", Application.dataPath, "");
        var modelImporter = new ModelImporter(inputDirectory);
        modelImporter.ImportModel();
    }


    [MenuItem("ModelCreation/Create layer from selected object")]
    static void CreateLayerFromSelectedObject()
    {
        GameObject selectedObject = Selection.activeGameObject;

        if (selectedObject == null)
        {
            Debug.LogWarning("No object selected.");
            return;
        }

        string prefabPath = "Assets/Models/Layers"; // Set your desired prefab folder path
        string prefabName = selectedObject.name;
        GameObject prefab = PrefabUtility.SaveAsPrefabAsset(selectedObject, $"{prefabPath}/{prefabName}.prefab");
    }
    
    [MenuItem("ModelCreation/Import static mesh from VTK file")]
    public static void ImportStaticVTKMesh()
    {
        // Choose VTK file
        string filePath = EditorUtility.OpenFilePanel("Choose VTK file.", Application.dataPath, "vtk");
        // Choose node type
        NodeType selectedNodeType = SelectNodeType();
        //Choose output asset directory
        //Choose output asset directory
        string meshPath= EditorUtility.SaveFilePanel("Choose output file for the imported mesh.", Application.dataPath, Path.GetFileNameWithoutExtension(filePath)+ "_mesh", "asset");
        if (string.IsNullOrEmpty(meshPath))
        {
            Debug.LogWarning("Invalid file path.");
            return;
        }
        meshPath = "Assets/" + meshPath.Substring(Application.dataPath.Length + 1);
        
        var meshImporter = new MeshImporter();
        Mesh mesh = meshImporter.StaticMeshFromVTK(filePath, selectedNodeType);
        AssetDatabase.CreateAsset(mesh, meshPath);
    }

    [MenuItem("ModelCreation/Import animated mesh from VTK files")]
    public static void ImportAnimatedVTKMesh()
    {
        // Choose input directory
        string inputDirectory = EditorUtility.OpenFolderPanel("Choose input directory with vtk files.", Application.dataPath, "");
        // Choose node type
        NodeType selectedNodeType = SelectNodeType();
        //Choose output asset directory
        string meshPath= EditorUtility.SaveFilePanel("Choose output file for the imported mesh.", Application.dataPath, Path.GetDirectoryName(inputDirectory)+ "_mesh", "asset");
        if (string.IsNullOrEmpty(meshPath))
        {
            Debug.LogWarning("Invalid file path.");
            return;
        }
        meshPath = "Assets/" + meshPath.Substring(Application.dataPath.Length + 1);
        string[] files = Directory.GetFiles(inputDirectory, "*.vtk");
        if (files.Length == 0)
        {
            Debug.LogWarning("No vtk files found in the selected directory.");
            return;
        }

        var meshImporter = new MeshImporter();
        Mesh mesh = meshImporter.AnimatedMeshFromVTK(files.ToList(), selectedNodeType);
        AssetDatabase.CreateAsset(mesh, meshPath);
    }

    // Method to select node type using a dialog box
    private static NodeType SelectNodeType()
    {
        int option = EditorUtility.DisplayDialogComplex("Select Node Type", "Choose the node type for mesh import:", "Empty", "Mesh", "Displacement");

        switch (option)
        {
            case 0:
                return NodeType.Empty;
            case 1:
                return NodeType.Mesh;
            case 2:
                return NodeType.Displacement;
            default:
                Debug.Log("Import canceled.");
                return NodeType.Empty;
        }
    }
}


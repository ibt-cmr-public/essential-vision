using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class QuestionGenerator : EditorWindow
{
    private string jsonFilePath;
    private string saveLocation;

    [MenuItem("Custom/Generate Questions")]
    public static void ShowWindow()
    {
        GetWindow<QuestionGenerator>("Question Generator");
    }

    private void OnGUI()
    {
        GUILayout.Label("Choose JSON File:", EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        jsonFilePath = EditorGUILayout.TextField(jsonFilePath);
        if (GUILayout.Button("Choose", GUILayout.Width(80)))
        {
            jsonFilePath = EditorUtility.OpenFilePanel("Choose JSON File", "", "json");
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(10);

        GUILayout.Label("Choose Save Location:", EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        saveLocation = EditorGUILayout.TextField(saveLocation);
        if (GUILayout.Button("Choose", GUILayout.Width(80)))
        {
            saveLocation = EditorUtility.OpenFolderPanel("Choose Save Location", "", "");
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(20);

        if (GUILayout.Button("Generate Questions"))
        {
            GenerateQuestions();
        }
    }

    private void GenerateQuestions()
    {
        if (string.IsNullOrEmpty(jsonFilePath) || string.IsNullOrEmpty(saveLocation))
        {
            Debug.LogError("Please choose a valid JSON file and select a save location.");
            return;
        }

        try
        {
            string jsonContent = File.ReadAllText(jsonFilePath);
            QuestionData questionData = JsonUtility.FromJson<QuestionData>(jsonContent);

            for (int i = 0; i < questionData.questions.Count; i++)
            {
                MultipleChoiceCheckpointData checkpointData = ScriptableObject.CreateInstance<MultipleChoiceCheckpointData>();
                checkpointData.title = string.IsNullOrEmpty(questionData.questions[i].title) ? "" : questionData.questions[i].title;
                checkpointData.question = questionData.questions[i].question;
                checkpointData.answers = questionData.questions[i].answers;
                checkpointData.correctAnswerIndex = questionData.questions[i].correct_answer;

                // Convert absolute path to relative path
                string relativePath = "Assets" + Path.GetFullPath(saveLocation).Substring(Application.dataPath.Length);
                string path = Path.Combine(relativePath, $"Question_{i + 1}.asset");

                AssetDatabase.CreateAsset(checkpointData, path);
                AssetDatabase.SaveAssets();
            }

            Debug.Log("Questions generated successfully.");
        }
        catch (System.Exception e)
        {
            Debug.LogError($"Error generating questions: {e.Message}");
        }
    }

    [System.Serializable]
    private class QuestionData
    {
        public List<QuestionInfo> questions;
    }

    [System.Serializable]
    private class QuestionInfo
    {
        public string title;
        public string question;
        public List<string> answers;
        public int correct_answer;
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(ModelScriptableObject))]
public class ModelScriptableObjectEditor : Editor
{
    private SerializedProperty guidStringProp;
    private SerializedProperty nameProp;
    private SerializedProperty descriptionProp;
    private SerializedProperty iconProp;
    private SerializedProperty volumetricTexturesProp;
    private SerializedProperty annotationsProp;
    private SerializedProperty isAnimatedProp;
    private SerializedProperty serializedNodesProp;
    private List<bool> foldoutStates = new List<bool>();

    
    private Color FoldoutColor(int indentLevel)
    {
        float greyscaleValue = 0.25f + Math.Min(0.05f * indentLevel, 0.6f);
        return new Color(greyscaleValue, greyscaleValue, greyscaleValue);
    }
    
    

    private void DrawFoldoutBackground(Rect rect, int indentLevel)
    {
        Color backgroundColor = FoldoutColor(indentLevel);
    
        // Draw background
        EditorGUI.DrawRect(rect, backgroundColor);
    
        // Draw border if folded out
        if (foldoutStates[indentLevel])
        {
            float borderWidth = 0.5f; // Adjust as needed
            EditorGUI.DrawRect(new Rect(rect.x, rect.y, rect.width, borderWidth), Color.black); // Top border
            EditorGUI.DrawRect(new Rect(rect.x, rect.y + rect.height - borderWidth, rect.width, borderWidth), Color.black); // Bottom border
            EditorGUI.DrawRect(new Rect(rect.x, rect.y, borderWidth, rect.height), Color.black); // Left border
            EditorGUI.DrawRect(new Rect(rect.x + rect.width - borderWidth, rect.y, borderWidth, rect.height), Color.black); // Right border
        }
    }

    private void OnEnable()
    {
        guidStringProp = serializedObject.FindProperty("guidString");
        nameProp = serializedObject.FindProperty("Name");
        descriptionProp = serializedObject.FindProperty("Description");
        iconProp = serializedObject.FindProperty("Icon");
        volumetricTexturesProp = serializedObject.FindProperty("VolumetricTextures");
        annotationsProp = serializedObject.FindProperty("annotations");
        serializedNodesProp = serializedObject.FindProperty("serializedNodes");
        
        for (int i = 0; i < serializedNodesProp.arraySize; i++)
        {
            foldoutStates.Add(true); // Adjust the initial state as needed
            // NumChildrenProps.Add(serializedNodesProp.GetArrayElementAtIndex(i).FindPropertyRelative("numChildren"));
            // FirstChildIdxProps.Add(serializedNodesProp.GetArrayElementAtIndex(i).FindPropertyRelative("numChildren"));
        }
        
    }

public override void OnInspectorGUI()
{
    serializedObject.Update();

    EditorGUILayout.PropertyField(guidStringProp);
    EditorGUILayout.PropertyField(nameProp);
    EditorGUILayout.PropertyField(descriptionProp);
    EditorGUILayout.PropertyField(iconProp);
    EditorGUILayout.PropertyField(volumetricTexturesProp);
    EditorGUILayout.PropertyField(annotationsProp);

    EditorGUILayout.Space();

    EditorGUILayout.LabelField("Model Tree", EditorStyles.boldLabel);

    if (serializedNodesProp.arraySize > 0)
    {
        DrawSerializedNode(serializedNodesProp, 0);
    }
    else
    {
        Rect buttonsRect = EditorGUILayout.GetControlRect();

        Rect addButtonRect = new Rect(buttonsRect.x + buttonsRect.width - 40f, buttonsRect.y, 20f, 16f);

        if (GUI.Button(addButtonRect, "+"))
        {
            // Add a child
            AddEmptyChildNode(serializedNodesProp, -1);
        }
    }

    EditorGUILayout.Space();

    // First line of buttons
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Export To File"))
    {
        string filePath = EditorUtility.SaveFilePanel("Export Model", "", "model", "json");
        if (!string.IsNullOrEmpty(filePath))
        {
            // Call the export function
            ExportModel(filePath);
        }
    }

    if (GUILayout.Button("Import From File"))
    {
        string directoryPath = EditorUtility.OpenFolderPanel("Import Model", "", "");
        if (!string.IsNullOrEmpty(directoryPath))
        {
            // Call the import function
            ImportModel(directoryPath);
        }
    }
    GUILayout.EndHorizontal();

    // Second line of buttons
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Save To Binary"))
    {
        string filePath = EditorUtility.SaveFilePanel("Save Model To Binary", "", "model", "bin");
        if (!string.IsNullOrEmpty(filePath))
        {
            // Call the save to binary function
            ModelScriptableObject model = (ModelScriptableObject)target;
            model.ToBinaryFile(filePath);
        }
    }

    if (GUILayout.Button("Load From Binary"))
    {
        string filePath = EditorUtility.OpenFilePanel("Load Model From Binary", "", "bin");
        if (!string.IsNullOrEmpty(filePath))
        {
            ModelScriptableObject model = (ModelScriptableObject)target;
            ModelScriptableObject loadedModel = ModelScriptableObject.FromBinaryFile(filePath);
            if (loadedModel != null)
            {
                Undo.RecordObject(model, "Load Model From Binary");
                EditorUtility.CopySerialized(loadedModel, model);
                EditorUtility.SetDirty(model);
            }
        }
    }
    GUILayout.EndHorizontal();

    if (serializedObject.ApplyModifiedProperties())
    {
        // If any property has been modified, update the GUID
        guidStringProp.stringValue = Guid.NewGuid().ToString();
    }

    serializedObject.ApplyModifiedProperties();
}

    void ExportModel(string filePath)
    {
        ModelScriptableObject model = (ModelScriptableObject)target;
        model.ToFile(filePath);
    }

    void ImportModel(string directoryPath)
    {
        ModelScriptableObject model = (ModelScriptableObject)target;
        ModelScriptableObject importedModel = ModelScriptableObject.FromFile(directoryPath);
        if (importedModel != null)
        {
            Undo.RecordObject(model, "Import Model");
            EditorUtility.CopySerialized(importedModel, model);
            EditorUtility.SetDirty(model);
        }
    }
    
    
    private void AddEmptyChildNode(SerializedProperty nodesProp, int parentIdx)
    {
        int childIdx = nodesProp.arraySize;
        
        //Add child index to parent
        if (parentIdx != -1)
        {
            SerializedProperty parentProp = nodesProp.GetArrayElementAtIndex(parentIdx);
            SerializedProperty childrenIndices = parentProp.FindPropertyRelative("children");
            childrenIndices.InsertArrayElementAtIndex(childrenIndices.arraySize);
            childrenIndices.GetArrayElementAtIndex(childrenIndices.arraySize - 1).intValue = childIdx;
        }

        //Insert new node
        nodesProp.InsertArrayElementAtIndex(childIdx);
        SerializedProperty newNodeProp = nodesProp.GetArrayElementAtIndex(childIdx);
        newNodeProp.FindPropertyRelative("parentIdx").intValue = parentIdx;
        newNodeProp.FindPropertyRelative("children").ClearArray();
        
        //Add foldout state
        foldoutStates.Add(true);
    }

    private void RemoveLastChildNode(SerializedProperty nodesProp, int parentIdx)
    {
        SerializedProperty nodeProp = nodesProp.GetArrayElementAtIndex(parentIdx);
        SerializedProperty childrenProp = nodeProp.FindPropertyRelative("children");
        int childIdx = childrenProp.GetArrayElementAtIndex(childrenProp.arraySize - 1).intValue;
        RemoveNode(nodesProp, childIdx);
    }
    
    private void RemoveNode(SerializedProperty nodesProp, int nodeIdx)
    {
        int parentIdx = nodesProp.GetArrayElementAtIndex(nodeIdx).FindPropertyRelative("parentIdx").intValue;
        SerializedProperty parentProp = nodesProp.GetArrayElementAtIndex(parentIdx);
        SerializedProperty nodeProp = nodesProp.GetArrayElementAtIndex(nodeIdx);
        SerializedProperty childrenProp = nodeProp.FindPropertyRelative("children");
        for (int i = 0; i < childrenProp.arraySize; i++)
        {
            int childIdx = childrenProp.GetArrayElementAtIndex(i).intValue;
            RemoveNode(nodesProp, childIdx);
        }
        
        nodesProp.DeleteArrayElementAtIndex(nodeIdx);
        //Fix indices after deletion
        for (int i = 0; i < nodesProp.arraySize; i++)
        {
            SerializedProperty node = nodesProp.GetArrayElementAtIndex(i);
            SerializedProperty nodeParentIdx = node.FindPropertyRelative("parentIdx");
            SerializedProperty nodeChildren = node.FindPropertyRelative("children");
            if (nodeParentIdx.intValue > nodeIdx)
            {
                nodeParentIdx.intValue--;
            }

            for (int j = 0; j < nodeChildren.arraySize; j++)
            {
                if (nodeChildren.GetArrayElementAtIndex(j).intValue > nodeIdx)
                {
                    nodeChildren.GetArrayElementAtIndex(j).intValue--;
                }
                else if(nodeChildren.GetArrayElementAtIndex(j).intValue == nodeIdx)
                {
                    nodeChildren.DeleteArrayElementAtIndex(j);
                    j--;
                }
                
            }
        }
    }
    
    private float GetNodeHeight(SerializedProperty nodesProp, int index)
    {
        SerializedProperty nodeProp = nodesProp.GetArrayElementAtIndex(index);
        SerializedProperty dataProp = nodeProp.FindPropertyRelative("data");
        SerializedProperty childrenProp = nodeProp.FindPropertyRelative("children");

        float height = EditorGUIUtility.singleLineHeight;
        if (foldoutStates[index])
        {
            height += EditorGUI.GetPropertyHeight(dataProp);
            height += EditorGUIUtility.standardVerticalSpacing;
            for (int i = 0; i < childrenProp.arraySize; i++)
            {
                int childIdx = childrenProp.GetArrayElementAtIndex(i).intValue;
                height += GetNodeHeight(nodesProp, childIdx);
            }
            height+= EditorGUIUtility.singleLineHeight;
            height+= 3f*EditorGUIUtility.standardVerticalSpacing;

        }

        height += EditorGUIUtility.standardVerticalSpacing;
        return height;
    }
    

   private void DrawSerializedNode(SerializedProperty nodesProp, int index)
    {
        
        SerializedProperty nodeProp = nodesProp.GetArrayElementAtIndex(index);
        SerializedProperty dataProp = nodeProp.FindPropertyRelative("data");
        SerializedProperty childrenProp = nodeProp.FindPropertyRelative("children");

        float nodeHeight = GetNodeHeight(nodesProp, index);
        Rect foldoutRect = EditorGUILayout.GetControlRect();

        float inspectorWidth = EditorGUIUtility.currentViewWidth;
        int indentationLevel = EditorGUI.indentLevel;
        float estimatedWidth = inspectorWidth - (indentationLevel * 15);
        Rect foldoutBackgroundRect = new Rect(foldoutRect.x - EditorGUIUtility.standardVerticalSpacing , foldoutRect.y + EditorGUIUtility.singleLineHeight, EditorGUIUtility.currentViewWidth, nodeHeight - EditorGUIUtility.singleLineHeight - EditorGUIUtility.standardVerticalSpacing);
        foldoutBackgroundRect = EditorGUI.IndentedRect(foldoutBackgroundRect);
        // Draw background and border if the node is expanded
        if (foldoutStates[index])
        {
            DrawFoldoutBackground(foldoutBackgroundRect, EditorGUI.indentLevel);
        }

        //foldoutStates[index] = EditorGUI.Foldout(foldoutRect, foldoutStates[index], dataProp.objectReferenceValue == null ? $"Node {index}" : ToString(), true);
        foldoutStates[index] = EditorGUI.Foldout(foldoutRect, foldoutStates[index],$"Node {index}", true);

        if (foldoutStates[index])
        {
            EditorGUILayout.PropertyField(dataProp, new GUIContent());
            EditorGUI.indentLevel++;

            // Begin a vertical group to encapsulate the content without affecting the layout
            EditorGUILayout.BeginVertical();
            for (int i = 0; i < childrenProp.arraySize; i++)
            {
                int childIdx = childrenProp.GetArrayElementAtIndex(i).intValue;
                DrawSerializedNode(nodesProp, childIdx);
            }
            
            

            Color oldbackgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = new Color(1f, 1f, 1f, 0f); // Change the color as needed
            Rect buttonsRect = EditorGUILayout.GetControlRect();
            Rect addButtonRect = new Rect(buttonsRect.x + buttonsRect.width - 40f, buttonsRect.y, 20f, EditorGUIUtility.singleLineHeight);
            Rect removeButtonRect = new Rect(buttonsRect.x + buttonsRect.width - 20f, buttonsRect.y, 20f, EditorGUIUtility.singleLineHeight);
            
            if (GUI.Button(addButtonRect, "+"))
            {
                // Add a child
                AddEmptyChildNode(nodesProp, index);
            }

            if (GUI.Button(removeButtonRect, "-") && childrenProp.arraySize > 0)
            {
                RemoveLastChildNode(nodesProp, index);
            }

            GUI.backgroundColor = oldbackgroundColor;
            
            EditorGUILayout.Space(EditorGUIUtility.standardVerticalSpacing);
            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel--;
        }
    }



    
}
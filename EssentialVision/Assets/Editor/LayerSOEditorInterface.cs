﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LayerScriptableObject))]
public class LayerScriptableObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LayerScriptableObject layerScriptableObject = (LayerScriptableObject)target;

        if (GUILayout.Button("Export To File"))
        {
            string filePath = EditorUtility.SaveFilePanel("Export LayerScriptableObject", "", layerScriptableObject.Name + ".model", "model");
            if (!string.IsNullOrEmpty(filePath))
            {
                layerScriptableObject.ToFile(filePath);
            }
        }
        
        if (GUILayout.Button("Import From File"))
        {
            string directoryPath = EditorUtility.OpenFolderPanel("Import LayerScriptableObject", "", "");
            if (!string.IsNullOrEmpty(directoryPath))
            {
                LayerScriptableObject importedLayer = LayerScriptableObject.FromFile(directoryPath);
                if (importedLayer != null)
                {
                    Undo.RecordObject(layerScriptableObject, "Import LayerScriptableObject");
                    EditorUtility.CopySerialized(importedLayer, layerScriptableObject);
                    EditorUtility.SetDirty(layerScriptableObject);
                }
            }
        }
        
        if (GUILayout.Button("Export To Binary File"))
        {
            string filePath = EditorUtility.SaveFilePanel("Export LayerScriptableObject (Binary)", "", layerScriptableObject.Name + ".bin", "bin");
            if (!string.IsNullOrEmpty(filePath))
            {
                layerScriptableObject.ToBinaryFile(filePath);
            }
        }
        
        if (GUILayout.Button("Import From Binary File"))
        {
            string filePath = EditorUtility.OpenFilePanel("Import LayerScriptableObject (Binary)", "", "bin");
            if (!string.IsNullOrEmpty(filePath))
            {
                LayerScriptableObject importedLayer = LayerScriptableObject.FromBinaryFile(filePath);
                if (importedLayer != null)
                {
                    Undo.RecordObject(layerScriptableObject, "Import LayerScriptableObject (Binary)");
                    EditorUtility.CopySerialized(importedLayer, layerScriptableObject);
                    EditorUtility.SetDirty(layerScriptableObject);
                }
            }
        }
        
    }
}
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEditor;
using Unity.VisualScripting;
using System.Drawing.Imaging;
using System;
using Graphics = System.Drawing.Graphics;

public class ImportTIFAs3DTextureEditor : EditorWindow
{
    [MenuItem("Custom/Import TIF as 3D Texture")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ImportTIFAs3DTextureEditor), false, "Import TIF as 3D Texture");
    }

    private string tifFilePath = "";

    private void OnGUI()
    {
        EditorGUILayout.LabelField("Select a TIF file to import as a 3D Texture:");

        GUILayout.Space(10);

        EditorGUILayout.BeginHorizontal();
        tifFilePath = EditorGUILayout.TextField("TIF File Path", tifFilePath);

        if (GUILayout.Button("Browse", GUILayout.Width(80)))
        {
            tifFilePath = EditorUtility.OpenFilePanel("Select TIF File", "", "tif");
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(20);

        if (GUILayout.Button("Import as 3D Texture"))
        {
            if (!string.IsNullOrEmpty(tifFilePath))
            {
                CreateTiledTIF(tifFilePath, "Assets/Models/test.tif");
                Close();
            }
            else
            {
                EditorUtility.DisplayDialog("Error", "Please select a valid TIF file.", "OK");
            }
        }
    }
    

    public static void CreateTiledTIF(string inputFileName, string outputFileName)
    {
        try
        {
            Bitmap bitmap = (Bitmap)Image.FromFile(inputFileName);
            int count = bitmap.GetFrameCount(FrameDimension.Page);
            int numColumns = (int)Math.Ceiling(Mathf.Sqrt(count));
            // Create a new Bitmap to hold the tiled layers
            int width = bitmap.Width * numColumns;
            int height = bitmap.Height * numColumns;
            Bitmap tiledBitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(tiledBitmap))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;

                // Loop through each layer and copy it to the tiled bitmap
                for (int idx = 0; idx < count; idx++)
                {
                    int colIdx = idx % numColumns;
                    int rowIdx = idx / numColumns;
                    bitmap.SelectActiveFrame(FrameDimension.Page, idx);
                    g.DrawImage(bitmap, new Point(colIdx * bitmap.Width, rowIdx * bitmap.Height));
                }
            }

            // Save the tiled image as a TIF file
            tiledBitmap.Save(outputFileName, ImageFormat.Tiff);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }

}

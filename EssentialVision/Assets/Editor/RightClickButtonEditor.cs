using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

[CustomEditor(typeof(RightClickButton), true)]
[CanEditMultipleObjects]
public class RightClickButtonEditor : ButtonEditor
{
    SerializedProperty m_OnRightClickProperty;

    protected override void OnEnable()
    {
        base.OnEnable();
        m_OnRightClickProperty = serializedObject.FindProperty("m_OnRightClick");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();

        serializedObject.Update();
        EditorGUILayout.PropertyField(m_OnRightClickProperty);
        serializedObject.ApplyModifiedProperties();
    }
}

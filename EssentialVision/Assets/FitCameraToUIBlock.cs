using System.Collections;
using System.Collections.Generic;
using Nova;
using UnityEngine;

public class FitCameraToUIBlock : MonoBehaviour
{
    
    public Camera camera;
    public RectTransform rectTransform;
    public UIBlock2D uiBlock2D;
    public UIBlock2D rootBlock;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (uiBlock2D == null || rootBlock == null || camera == null)
        {
            return;
        }
        Rect cameraRect = new Rect();
        float width = uiBlock2D.CalculatedSize.X.Value / rootBlock.CalculatedSize.X.Value;
        float height = uiBlock2D.CalculatedSize.Y.Value / rootBlock.CalculatedSize.Y.Value;
        cameraRect.width = width;
        cameraRect.height = height;
        Vector3 screenSpacePosition = rootBlock.transform.worldToLocalMatrix * uiBlock2D.transform.position;
        cameraRect.x = 0.5f + screenSpacePosition.x/rootBlock.CalculatedSize.X.Value - 0.5f * width;
        cameraRect.y = 0.5f + screenSpacePosition.y / rootBlock.CalculatedSize.Y.Value - 0.5f * height;
        if (camera.rect == cameraRect)
        {
            return;
        }
        camera.rect = cameraRect;
        // //Get screen space coordinates of UI BLock transform
        // Vector3 screenSpacePosition = screenSpace.transform.parent.worldToLocalMatrix  * uiBlock2D.transform.position;
        // float width = uiBlock2D.CalculatedSize.X.Value/screenSpace.ReferenceResolution.x;
        // float height = uiBlock2D.CalculatedSize.Y.Value/screenSpace.ReferenceResolution.y;
        //

        // cameraRect.x = screenSpace
        // cameraRect.y = uiBlock2D.Position.Y.Percent;
        // cameraRect.width = uiBlock2D.CalculatedSize.X.Percent;
        // cameraRect.height = uiBlock2D.CalculatedSize.Y.Percent;
        // camera.rect = cameraRect;
    }
}

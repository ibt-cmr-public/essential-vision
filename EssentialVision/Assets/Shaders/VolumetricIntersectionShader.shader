Shader "Unlit/MRIShader"
{
    Properties
    {
        _Color ("Color", Vector) = (1,1,1,1)
        _MainTex ("Texture", 3D) = "white" {}
        _LocalPosition ("Local Position", Vector) = (0,0,0,0)
        _LocalRotation ("Local Rotation", Vector) = (0,0,0,0)
        _LocalScale ("Local Scale", Vector) = (1,1,1,1)
        _MinValue ("MinimumValue", Float) = 0
        _MaxValue ("Maximum Value", Float) = 1
    }
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue" = "Transparent"
        }
        
        Pass
        {
            ZWrite Off
            ZClip Off
            Cull Off
            Blend  SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            #pragma multi_compile _ DISPLAY_TEXTURE

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 parentPos: TEXCOORD0;
            };

            sampler3D _MainTex;
            float4x4 _ParentMatrix;
            float4 _LocalScale;
            float4 _LocalPosition;
            float4 _LocalRotation;
            float4 _Color;
            float _MinValue;
            float _MaxValue;

            float3x3 EulerToRotationMatrix(float3 eulerAngles)
            {
                // Convert degrees to radians
                float3 eulerAnglesRadians = radians(eulerAngles);

                float cx = cos(eulerAnglesRadians.x);
                float sx = sin(eulerAnglesRadians.x);
                float cy = cos(eulerAnglesRadians.y);
                float sy = sin(eulerAnglesRadians.y);
                float cz = cos(eulerAnglesRadians.z);
                float sz = sin(eulerAnglesRadians.z);

                float3x3 rotationMatrix;
                rotationMatrix[0] = float3(cy * cz, -cy * sz, sy);
                rotationMatrix[1] = float3(sx * sy * cz + cx * sz, -sx * sy * sz + cx * cz, -sx * cy);
                rotationMatrix[2] = float3(-cx * sy * cz + sx * sz, cx * sy * sz + sx * cz, cx * cy);

                return rotationMatrix;
            }
            

            v2f vert (appdata v)
            {
                v2f o;
                //v.vertex*=1.1;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);

                

                // Inverse transformations
                float4x4 invLocalTransform = float4x4(
                    float4(1/_LocalScale.x, 0, 0, 0),
                    float4(0, 1/_LocalScale.y, 0, 0),
                    float4(0, 0, 1/_LocalScale.z, 0),
                    float4(-_LocalPosition.xyz, 1)
                );

                float3 invLocalRotation = -_LocalRotation.xyz; // Inverse rotation

                // Apply the inverse transformations to worldPos
                float3 parentPos = mul(_ParentMatrix, worldPos);
                parentPos.xyz -= _LocalPosition;
                parentPos.xyz = mul(EulerToRotationMatrix(_LocalRotation), parentPos.xyz);
                parentPos.xyz /= _LocalScale;
                parentPos += 0.5;
                
                //parentPos.xyz = mul(EulerToRotationMatrix(_LocalRotation), parentPos.xyz);
                o.parentPos = parentPos;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

                #ifdef DISPLAY_TEXTURE
                    clip(1-i.parentPos);
                    clip(i.parentPos);
                    fixed4 col = tex3D(_MainTex, i.parentPos);
                    return lerp(_Color, float4(0,0,0,0.8), (1-col.x)*(1-col.x)*(1-col.x));
                    float val = -(col.x-1)*(col.x-1)+1;
                    return val * float4(1,0,0, col.x > 0) + (1-val) *float4(1,1,1,col.x > 0);
                    return float4(2*col.xyz + 0.1,0.9);
                    return float4(_Color.xyz, _Color.w * col.x *2 + 0.1);
                #else
                    return float4(0,0,0,0);
                #endif
                
            }
            ENDCG
        }
    }
}

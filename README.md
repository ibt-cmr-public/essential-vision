# Essential Vision

Essential Vision is an open-source project that makes 3D anatomy interactively accessible on the Microsoft HoloLens. The app is optimized for individual learning as well as group teaching. More about the project, the app and its features can be found on the project website: https://www.essential-vision.org/


## App Features and Navigation

### Models

The essential vision app comes with a range of pre-installed annotated models, such as a detailed animated model of the human heart and an atlas of bones, muscles and vasculature. Additionally, individual models can be created and uploaded as described in the section "Creating a New Model". Models can be fully animated based on blend shapes. Users have full control over animation speed and playback. Essential Vision also supports flow, displacement and turbulence simulations. Each model is constructed from different layers which can be individually activated, deactivated and made transparent.

<img src="docs/images/rotating_heart.gif" width="300"> <img src="docs/images/atlas.gif" width="300"> <img src="docs/images/aortic_flow.gif" width="300">

### Navigation

The app can be navigated by HoloLens 2 hand gestures and voice commands. Model manipulations such as movement, rotation and scaling can be controlled by grabbing the object of interest with two hands and moving the hand correspondingly. Alternatively, the object has an integrating object-handling-cube which can be manipulated one-handedly.

### Teaching

The app includes several features specifically designed for teaching. MRI data can be explored through a clipping plane which helps generate a better 3D understanding. On top, models can be annotated, predefined model states can be saved and interactive quizzes can be created. A fully guided interactive teaching session without a teacher present is thus possible.

<img src="docs/images/look_inside.gif" width="300"> <img src="docs/images/quiz.gif" width="400"> <img src="docs/images/quiz.png" width="400">

### Shared Experience

In addition, the app supports shared experience, meaning that multiple HoloLens can connect to the same session, see and interact with the same model and participate in the same quiz.

## Set-Up and Overview

This project runs on Unity version 2021.3.1.19f1 and uses the MRTK package. The scene called "Main Scene" should be loaded if running the app in the Unity editor. The UI is based on MRTK-prefabs and set up in a modular fashion. The main objects (and sometimes prefabs) are the following:
- Main Menue: contains the menu initially open in the app.
- ModelInstance: contains the menu for handling and manipulating the anatomical models. It is loaded in the ModelInstanceContainer.
- Shared Experience: contains the interface and backend to connect multiple HoloLens to the same session (share experience).
- Debug Console: used to display debugging messages on the HoloLens

## Creating a New Model
Select ModelCreation in the top bar. Then choose the function "create a mesh from an external format." This function creates a model (asset bundle) from vtk files. The vtk files should be sorted by layer and each layer is stored in a separate subfolder. Different vtk files for the same layer represent different layer states in the model animation. All layers must have the same number of vtk files. The folder and file structure should follow the following scheme. 

- model_folder
    - layer_1_folder
        - layer_1_state_01.vtk
        - layer_1_state_02.vtk
        - layer_1_state_02.vtk
    - layer_2_folder
        - layer_2_state_01.vtk
        - layer_2_state_02.vtk
        - layer_2_state_02.vtk
    - ModelInfo.json
    - model-icon.png

The ModelInfo.json file stored in the main folder contains the specific model information and has the following structure:

```json
{
    Caption: "Model Name",
    IconFileName: "model-icon.png",
    Layers: [
        {
        "Caption": "Layer 1",
        "DataType": "anatomyRGB",
        "Directory": "layer_1_folder",
        "RGB": "255,186,186",
        "UseAsIcon": false
        },
        {
        "Caption": "Layer 2",
        "DataType": "anatomyRGB",
        "Directory": "layer_2_folder",
        "RGB": "205,0,0"
        }
  ]
}
```

The model icon should be stored in the main folder. Alternatively, a "UseAsIcon" key set to true can be added to the layer that should be used as an icon. The icon will then be generated as a layer snap-shot during the model generation. No image file has to be added to the main folder.

The following DataTypes are available:
- `anatomy` - Surface mesh shown as grey material
- `fibre` or `flow` - Vectors of certain length and colored based on scalar value per vector. Min, max and unit of the color scale can be added in the dictionary field "ColorScaleVariables" as "min-max-unit".
- `turbulence` - Colored point cloud based on scalar value per point. Min, max and unit of the color scale can be added in the dictionary field "ColorScaleVariables" as "min-max-unit".
- `displacement` - Colored surface mesh based on scalar value per mesh node. Min, max and unit of the color scale can be added in the dictionary field "ColorScaleVariables" as "min-max-unit".
- `anatomyRGB` - Colored surface mesh with one color which is specified in the "RGB" dictionary field of the ModelInfo.json
- `special` - Surface mesh similar to `anatomy` but it will use a specialized material on the HoloLens. The name of the material must be added in the dictionary field "SpecialMaterial".

## Build the App


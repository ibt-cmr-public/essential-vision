﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 <NetworkReference>j__TPar <>f__AnonymousType0`2::get_NetworkReference()
// 0x00000002 <NodeInfo>j__TPar <>f__AnonymousType0`2::get_NodeInfo()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<NetworkReference>j__TPar,<NodeInfo>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 System.Void AnswerButton::SetFeedbackMode(System.Boolean,AnswerButton/FeedbackState)
extern void AnswerButton_SetFeedbackMode_m3947B2F3C278BE2AD574BD4455141D0EA4A3DAB8 (void);
// 0x00000008 System.Void AnswerButton::SetAnswerText(System.String)
extern void AnswerButton_SetAnswerText_m52E83EFA95C22D414889294E29E5B98611BF8E9E (void);
// 0x00000009 System.Single AnswerButton::PreferredHeight()
extern void AnswerButton_PreferredHeight_m057E8EA02E99B0888845B231BF4BC2CDD987FD18 (void);
// 0x0000000A System.Void AnswerButton::.ctor()
extern void AnswerButton__ctor_m9AE7119B8134C31A6CE1DE7870EAA67A4575121D (void);
// 0x0000000B System.Boolean BlendShapeAnimation::get_LoopAnimation()
extern void BlendShapeAnimation_get_LoopAnimation_mAFB0C4E5FDE9CA15A9A9900163E0632B6C91B170 (void);
// 0x0000000C System.Void BlendShapeAnimation::set_LoopAnimation(System.Boolean)
extern void BlendShapeAnimation_set_LoopAnimation_m463F547FCCED018DDD2D2E65CE815C85C911A6EE (void);
// 0x0000000D System.Void BlendShapeAnimation::InitializeBlendShapes()
extern void BlendShapeAnimation_InitializeBlendShapes_m4D9C11D15A511B5663288C12D392392916BA1374 (void);
// 0x0000000E System.Void BlendShapeAnimation::Start()
extern void BlendShapeAnimation_Start_m20AC607E3B2429DDB9881C486D37172E069A9888 (void);
// 0x0000000F System.Void BlendShapeAnimation::UpdateBlendShapes()
extern void BlendShapeAnimation_UpdateBlendShapes_m919CB0C01F89C0F4905CEC1B2C59E412BB4F6F51 (void);
// 0x00000010 System.Int32 BlendShapeAnimation::MaxCurrentIndex()
extern void BlendShapeAnimation_MaxCurrentIndex_m05E6738698DAEEE752C8C9004D58FA7689683C1A (void);
// 0x00000011 System.Single BlendShapeAnimation::get_CurrentTime()
extern void BlendShapeAnimation_get_CurrentTime_m51AEEC7AA0CE3C1989AF9012B34E786A768DC37D (void);
// 0x00000012 System.Void BlendShapeAnimation::set_CurrentTime(System.Single)
extern void BlendShapeAnimation_set_CurrentTime_m38F5903205C5F9C01F995FB00314BD3CC96750BA (void);
// 0x00000013 System.Void BlendShapeAnimation::.ctor()
extern void BlendShapeAnimation__ctor_m06A787150E2ECB0D7F40B45BFD07429D6228FD41 (void);
// 0x00000014 System.Single IAnimatable::get_CurrentTime()
// 0x00000015 System.Void IAnimatable::set_CurrentTime(System.Single)
// 0x00000016 System.Boolean IAnimatable::get_LoopAnimation()
// 0x00000017 System.Void IAnimatable::set_LoopAnimation(System.Boolean)
// 0x00000018 System.Void ApplicationDefaults::.cctor()
extern void ApplicationDefaults__cctor_mC0677F9AF9940565F0C38342CA2D11C0921A01A3 (void);
// 0x00000019 NodeType LayerScriptableObject::get_Type()
extern void LayerScriptableObject_get_Type_m44C1311C5CEC5CDAC591B597E3194C3F1A75845A (void);
// 0x0000001A System.Void LayerScriptableObject::initFromDict(System.String)
extern void LayerScriptableObject_initFromDict_m313084DB13C08B57AB5E756658EC9DECA707BFBC (void);
// 0x0000001B System.Void LayerScriptableObject::.ctor()
extern void LayerScriptableObject__ctor_m8F32E5A82BDCC393BA36CE8689B00D9CFF5AAC62 (void);
// 0x0000001C System.Void LocalTransform::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void LocalTransform__ctor_m5B614313B67478392333D4BF0AC97F9FF5E16B3D (void);
// 0x0000001D System.Void LocalTransform::.ctor(UnityEngine.Transform)
extern void LocalTransform__ctor_m6236FFC8C68D10344A687DED13BAA193C967A635 (void);
// 0x0000001E System.Void LocalTransform::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x0000001F System.Void NodeState::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000020 System.Void NodeState::.ctor()
extern void NodeState__ctor_mC1FF3DE9C1F10F505B0312DB606B06565896CE66 (void);
// 0x00000021 System.Void LayerState::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000022 System.Void LayerState::.ctor()
extern void LayerState__ctor_m5B9B1C3D19684026BECDDB7B0C3CACB60CCBEDA3 (void);
// 0x00000023 System.Void AnimationState::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000024 System.Void AnimationState::.ctor()
extern void AnimationState__ctor_m10E9FDCF07CD51207C96EB1A68D45B440B9A3945 (void);
// 0x00000025 System.Void ClippingPlaneState::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000026 System.Void ClippingPlaneState::.ctor()
extern void ClippingPlaneState__ctor_m4A80255E1896F1B694E6A4C4BD5C9DD8906AEA23 (void);
// 0x00000027 System.Void UIState::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000028 System.Void UIState::.ctor()
extern void UIState__ctor_mA4BC93EA34AF2D6D737E5BB3B1C390ED888F3537 (void);
// 0x00000029 System.Void ModelInstanceState::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x0000002A System.Void ModelInstanceState::.ctor()
extern void ModelInstanceState__ctor_m24687CF3EAA877A1DA3FC421C3E5C81ACC6DE2E4 (void);
// 0x0000002B System.Void LocalTransformTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x0000002C System.Void LocalTransformTransition::.ctor()
extern void LocalTransformTransition__ctor_m41B4AAF4D35E8CCDE2F226C8BB45176E33AB7F16 (void);
// 0x0000002D System.Void NodeStateTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x0000002E System.Void NodeStateTransition::.ctor()
extern void NodeStateTransition__ctor_m4CAD17CB9A6B96D309058A92A4064C05C255D9D5 (void);
// 0x0000002F System.Void LayerStateTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000030 System.Void LayerStateTransition::.ctor()
extern void LayerStateTransition__ctor_m54D9FE611A76E270CE5B111EE32E410E353AB6A3 (void);
// 0x00000031 System.Void AnimationStateTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000032 System.Void AnimationStateTransition::.ctor()
extern void AnimationStateTransition__ctor_m1871B1075DBEE0EBE82BB627D2D47846BA696AE9 (void);
// 0x00000033 System.Void ClippingPlaneStateTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000034 System.Void ClippingPlaneStateTransition::.ctor()
extern void ClippingPlaneStateTransition__ctor_m230F240FCE9BC863096DFB6DD11CB45AB90CF433 (void);
// 0x00000035 System.Void UIStateTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000036 System.Void UIStateTransition::.ctor()
extern void UIStateTransition__ctor_m0E8C427A23BBF93BAB4E7395FFBD2CC3CA4331BD (void);
// 0x00000037 System.Void ModelInstanceStateTransition::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x00000038 System.Void ModelInstanceStateTransition::.ctor()
extern void ModelInstanceStateTransition__ctor_m51D3D7272FBA810B9633964EC4478DD863B6C0D6 (void);
// 0x00000039 System.Boolean ModelNodeScriptableObject::get_isExpanded()
extern void ModelNodeScriptableObject_get_isExpanded_mE6662A63F808513276FB7C02C7ABC8F17960E93B (void);
// 0x0000003A System.Void ModelNodeScriptableObject::set_isExpanded(System.Boolean)
extern void ModelNodeScriptableObject_set_isExpanded_m75C0DC2B41CC54CCBCFDE51D955C15B54B3BDC0D (void);
// 0x0000003B NodeType ModelNodeScriptableObject::get_Type()
extern void ModelNodeScriptableObject_get_Type_mAA4C734834DBEF454EBDAC0472C18FD1B30EA821 (void);
// 0x0000003C System.Void ModelNodeScriptableObject::.ctor()
extern void ModelNodeScriptableObject__ctor_m574BA40B77CBAC87EB967C5259FBFE5443802C17 (void);
// 0x0000003D System.Boolean ModelScriptableObject::get_IsAnimated()
extern void ModelScriptableObject_get_IsAnimated_m7A0AA88E3F6FEE29B0F96F18871AFE004938CE75 (void);
// 0x0000003E UnityEngine.Sprite ModelScriptableObject::get_IconSprite()
extern void ModelScriptableObject_get_IconSprite_m080F6A59683A508A4C0655BF8F0640885DD95280 (void);
// 0x0000003F System.Void ModelScriptableObject::SaveToJSON(System.String)
extern void ModelScriptableObject_SaveToJSON_m6742AFA1C4619EFD848695DF89B867D1817BEA13 (void);
// 0x00000040 System.Void ModelScriptableObject::InitFromJSON(System.String)
extern void ModelScriptableObject_InitFromJSON_m78741172B16DE4DBCD42DF2B35926572ADAAD9B1 (void);
// 0x00000041 System.Void ModelScriptableObject::OnBeforeSerialize()
extern void ModelScriptableObject_OnBeforeSerialize_m46147308C41EFD2A882750C8C4C6DA2F1C9EAD51 (void);
// 0x00000042 System.Int32 ModelScriptableObject::get_NumNodes()
extern void ModelScriptableObject_get_NumNodes_m39E5C8386D84746132A73143A3E50657280F272A (void);
// 0x00000043 System.Void ModelScriptableObject::AddNodeToSerializedNodes(TreeView.TreeNode`1<ModelNodeScriptableObject>,System.Int32,System.Int32)
extern void ModelScriptableObject_AddNodeToSerializedNodes_m4A22DCC5C2391426A6AE7D120442BB0B8C443455 (void);
// 0x00000044 System.Void ModelScriptableObject::OnAfterDeserialize()
extern void ModelScriptableObject_OnAfterDeserialize_mF18788ABC8F06DD513ABF7DF5B43CAD183BDA0EF (void);
// 0x00000045 TreeView.TreeNode`1<ModelNodeScriptableObject> ModelScriptableObject::ReadNodeFromSerializedNodes(System.Int32)
extern void ModelScriptableObject_ReadNodeFromSerializedNodes_mFB0204487A2F6638B92329DBC5014E25F21DB5C3 (void);
// 0x00000046 System.Void ModelScriptableObject::.ctor()
extern void ModelScriptableObject__ctor_mD0E4F2B9F96851217EDCF69EEF036FA66AFFD6FB (void);
// 0x00000047 System.Void VolumetricTexture::.ctor()
extern void VolumetricTexture__ctor_m8E59372708AAB845C7A2C36C029397EB1C6C814F (void);
// 0x00000048 System.Void DebugConsole::Start()
extern void DebugConsole_Start_mD24DFF76A9D7DD4CA7793EB152F8646C0C2A3FB7 (void);
// 0x00000049 System.Void DebugConsole::OnEnable()
extern void DebugConsole_OnEnable_m163FE5682B0205E61F6C82AAD45608DC54C1741D (void);
// 0x0000004A System.Void DebugConsole::OnDisable()
extern void DebugConsole_OnDisable_m61452992A217FF6C82E0F5D3110A7C7FC4CEBA6E (void);
// 0x0000004B System.Void DebugConsole::LogMessage(System.String,System.String,UnityEngine.LogType)
extern void DebugConsole_LogMessage_mF31E85201F24A3DC61C802855685FA1038B0E472 (void);
// 0x0000004C System.Void DebugConsole::.ctor()
extern void DebugConsole__ctor_m0FFD1EC98F6CA96234BEC9AD11809E4E86261BC8 (void);
// 0x0000004D System.Void DebugTester::Start()
extern void DebugTester_Start_m44B4D4DC97D9F1313DEA62E2365E1E873BB383AE (void);
// 0x0000004E System.Void DebugTester::.ctor()
extern void DebugTester__ctor_m7BF0A9A11A35B7AB5970479397D18099E303E4FF (void);
// 0x0000004F System.Void DebugTester/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_mD2A371AD44B08D643FCC1CBE31AEC1E2FF13A2EE (void);
// 0x00000050 System.Void DebugTester/<Start>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__1_SetStateMachine_mD189FAB04CE51EC7DC2E00C4547993B95ACD637E (void);
// 0x00000051 System.Void TestMultiplayerUI::Awake()
extern void TestMultiplayerUI_Awake_mA2941443C3345CAF2E683485D7A681F1ECAC38A7 (void);
// 0x00000052 System.Void TestMultiplayerUI::.ctor()
extern void TestMultiplayerUI__ctor_m0440FB3F6A0E8A6DA0DF36564799CC6D3823A962 (void);
// 0x00000053 System.Void TestMultiplayerUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m1E17597C4EF621F369A5AF2C97531CB03F507CA0 (void);
// 0x00000054 System.Void TestMultiplayerUI/<>c::.ctor()
extern void U3CU3Ec__ctor_m6117EB0A5DE8E828D6E4EF7E3655291250AA1913 (void);
// 0x00000055 System.Void TestMultiplayerUI/<>c::<Awake>b__2_0()
extern void U3CU3Ec_U3CAwakeU3Eb__2_0_m816E0D375D51C1D92E9BB5E26D7E6FF3D3BE154A (void);
// 0x00000056 System.Void TestMultiplayerUI/<>c::<Awake>b__2_1()
extern void U3CU3Ec_U3CAwakeU3Eb__2_1_mB7A24790784A0F2DB48BCD4A798685523D681B69 (void);
// 0x00000057 System.Void IntroAnimation::Start()
extern void IntroAnimation_Start_mC03ADF18E34D64573A5AA0E6E819665F3D2A76F6 (void);
// 0x00000058 System.Collections.IEnumerator IntroAnimation::AnimationIntro()
extern void IntroAnimation_AnimationIntro_m2376FE658F0CAA1903A8833E0A1B2D9055A7092D (void);
// 0x00000059 System.Void IntroAnimation::OnIntroFinished()
extern void IntroAnimation_OnIntroFinished_mA39C91E005F01AF355A149B84499AEFBC165CCA1 (void);
// 0x0000005A System.Void IntroAnimation::Update()
extern void IntroAnimation_Update_mD993AA946CF2627F1677E431DB1B1EC59302755A (void);
// 0x0000005B System.Void IntroAnimation::.ctor()
extern void IntroAnimation__ctor_m720D235E10930BB16FC6EB2DC0AB4C8198B83EA4 (void);
// 0x0000005C System.Void IntroAnimation/<AnimationIntro>d__5::.ctor(System.Int32)
extern void U3CAnimationIntroU3Ed__5__ctor_m1EF920F3A1AD2CA29839C88B223B6303D9DE3F42 (void);
// 0x0000005D System.Void IntroAnimation/<AnimationIntro>d__5::System.IDisposable.Dispose()
extern void U3CAnimationIntroU3Ed__5_System_IDisposable_Dispose_mBCA2AE77BBB9159B8176F3FC44349141D421F20B (void);
// 0x0000005E System.Boolean IntroAnimation/<AnimationIntro>d__5::MoveNext()
extern void U3CAnimationIntroU3Ed__5_MoveNext_m7EF32CEF730B2F6778C42E6C69FCE85C663A6210 (void);
// 0x0000005F System.Object IntroAnimation/<AnimationIntro>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimationIntroU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABA81E04AB2A4F244AE801A2C2450B45E9D4C4C1 (void);
// 0x00000060 System.Void IntroAnimation/<AnimationIntro>d__5::System.Collections.IEnumerator.Reset()
extern void U3CAnimationIntroU3Ed__5_System_Collections_IEnumerator_Reset_mFB1E6AD8437802EDF9E3DA18DABD4630D90B91F9 (void);
// 0x00000061 System.Object IntroAnimation/<AnimationIntro>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CAnimationIntroU3Ed__5_System_Collections_IEnumerator_get_Current_m87CF85793149D96CDC6D7BBAAFB3A8D561EE78D5 (void);
// 0x00000062 LibraryManager LibraryManager::get_Instance()
extern void LibraryManager_get_Instance_m6C62BACDA93185181A64F99107E4DBA70F60FDB5 (void);
// 0x00000063 System.Void LibraryManager::Awake()
extern void LibraryManager_Awake_m91D52FE20A0F4A6DC87DAF46AE243CE1E02762A5 (void);
// 0x00000064 System.Void LibraryManager::LoadModels()
extern void LibraryManager_LoadModels_m42073D2724C182D998F3D0B19905F8279429465D (void);
// 0x00000065 System.Void LibraryManager::LoadCheckpointGraphs()
extern void LibraryManager_LoadCheckpointGraphs_mEE43FF20CD7C713E14C795D173F3538D29AE5738 (void);
// 0x00000066 ModelScriptableObject LibraryManager::GetModel(System.String)
extern void LibraryManager_GetModel_m1E03B950B3AA59954BFF35C6814A8D0B30F52A8E (void);
// 0x00000067 ModelCheckpointGraph LibraryManager::GetGraph(System.String)
extern void LibraryManager_GetGraph_m934D401390B46EBCB9A9302BB4AFF8EB719BEC7E (void);
// 0x00000068 System.Collections.Generic.List`1<ModelScriptableObject> LibraryManager::get_Models()
extern void LibraryManager_get_Models_m76612DED4EBD669B0B3FCF39C5EB84E0FA25D457 (void);
// 0x00000069 System.Collections.Generic.List`1<ModelCheckpointGraph> LibraryManager::get_CheckpointGraphs()
extern void LibraryManager_get_CheckpointGraphs_m0C4C492720D676F5AD146FE89033515E63782DCE (void);
// 0x0000006A System.Collections.Generic.List`1<ModelCheckpointGraph> LibraryManager::CheckpointGraphsForModel(System.String)
extern void LibraryManager_CheckpointGraphsForModel_m7458CBFD002D220469EE10E25C6C7C30D7F82312 (void);
// 0x0000006B System.Void LibraryManager::OnDestroy()
extern void LibraryManager_OnDestroy_m0B0A7F1886C3ED3495C9F27883ED1B0E4ECA9C6A (void);
// 0x0000006C System.Void LibraryManager::OnApplicationQuit()
extern void LibraryManager_OnApplicationQuit_m9078D954FB268C99782F3FF29306C80F59D88207 (void);
// 0x0000006D System.Void LibraryManager::.ctor()
extern void LibraryManager__ctor_m21BF26278C2864E2BC4D073F66DDF178C724E67D (void);
// 0x0000006E System.Void LibraryManager::<LoadModels>b__8_0(ModelScriptableObject)
extern void LibraryManager_U3CLoadModelsU3Eb__8_0_m7E4F2EA6B8E3ACC7A0BC7BF93F78137F30508FF3 (void);
// 0x0000006F System.Void LibraryManager::<LoadCheckpointGraphs>b__9_0(ModelCheckpointGraph)
extern void LibraryManager_U3CLoadCheckpointGraphsU3Eb__9_0_mCB92E0BEE46B704D34F826D18A88CA7A627F3B99 (void);
// 0x00000070 System.Void LibraryManager/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mBE88D929FBF91B08C36D367E595E87F29309AC4A (void);
// 0x00000071 System.Boolean LibraryManager/<>c__DisplayClass16_0::<CheckpointGraphsForModel>b__0(ModelCheckpointGraph)
extern void U3CU3Ec__DisplayClass16_0_U3CCheckpointGraphsForModelU3Eb__0_mE7030BC24FC0B274050F5642747B18138904DB9F (void);
// 0x00000072 System.String LocalConfig::GetBundlesDirectory()
extern void LocalConfig_GetBundlesDirectory_m10D15DEE4F1906FE243CB252624F77B5F3F21F14 (void);
// 0x00000073 System.Void LocalConfig::.ctor()
extern void LocalConfig__ctor_mF66C1D10C88FEE0410E4D2A80968FCCEC782BB2B (void);
// 0x00000074 System.Boolean MainMenu::get_InFocus()
extern void MainMenu_get_InFocus_mE000858446FAEA8DC5743FA59D3373691480E796 (void);
// 0x00000075 System.Void MainMenu::set_InFocus(System.Boolean)
extern void MainMenu_set_InFocus_m0A1217FA1023B862C1933A8CE47044AF40B4DF95 (void);
// 0x00000076 System.Void MainMenu::Start()
extern void MainMenu_Start_m1729BDE6D096D9F4C92DBE72B392BA89E9A9ECAD (void);
// 0x00000077 System.Void MainMenu::Update()
extern void MainMenu_Update_m6D9E8EB1A42CC68CFAA865B9CF18FAEB81595C5C (void);
// 0x00000078 System.Void MainMenu::HideMenu()
extern void MainMenu_HideMenu_m9A25BA3B9FD5C2A691F3E0F9B23E1FD2F30FF449 (void);
// 0x00000079 System.Void MainMenu::ShowSharedExperience()
extern void MainMenu_ShowSharedExperience_m66087E37D7CDFEAE3B6D62EFF8083CF588617F89 (void);
// 0x0000007A System.Void MainMenu::ShowOptions()
extern void MainMenu_ShowOptions_m361CB78A440EA73038A7622BB7770310FF339635 (void);
// 0x0000007B System.Void MainMenu::ShowLibrary()
extern void MainMenu_ShowLibrary_m7D0A7464042D753B5B24FAB3C89917795FE20E40 (void);
// 0x0000007C System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m8209CEC1D907C87A96D777961F4D0536E6E948DD (void);
// 0x0000007D System.Void CheckpointData::.ctor()
extern void CheckpointData__ctor_m35703B470D8D2E175B3FA067681F7A89D408F1C6 (void);
// 0x0000007E System.Void FreeformQuestionSO::.ctor()
extern void FreeformQuestionSO__ctor_m9608FA497581E237D03732B73506B9D9CEFE7941 (void);
// 0x0000007F System.Void InformationSO::.ctor()
extern void InformationSO__ctor_m0073E1B4420391CEF5A070A714E0324F12B4AFB2 (void);
// 0x00000080 System.Void MultipleChoiceQuestionSO::.ctor()
extern void MultipleChoiceQuestionSO__ctor_m4E501335F5C60C6188CBBC9BE6DF07CFDC8C1F9A (void);
// 0x00000081 System.Void ModelCheckpoint::.ctor()
extern void ModelCheckpoint__ctor_m8A2DAD38D62B5FDF045D81B5F865CD701F683107 (void);
// 0x00000082 System.Int32 ModelCheckpointGraph::get_numCheckpoints()
extern void ModelCheckpointGraph_get_numCheckpoints_m7B7987692DA302E81BF301518E953FE071DCDDC3 (void);
// 0x00000083 System.Int32 ModelCheckpointGraph::get_numData()
extern void ModelCheckpointGraph_get_numData_m62B151450AAFE689C67A8559CD4D3ABE462B2326 (void);
// 0x00000084 System.Int32 ModelCheckpointGraph::get_maxScore()
extern void ModelCheckpointGraph_get_maxScore_m73D08DDB81B468635E191EC212AA83A36F947EBE (void);
// 0x00000085 System.Int32 ModelCheckpointGraph::score(System.Collections.Generic.List`1<System.Object>)
extern void ModelCheckpointGraph_score_m5DF987AAB3A5AD3E2043C35F7417431697981B38 (void);
// 0x00000086 System.Int32 ModelCheckpointGraph::checkpointScore(ModelCheckpoint,System.Object)
extern void ModelCheckpointGraph_checkpointScore_m02C5951B0E6E96C55E014E1483A4F37B8DEA6661 (void);
// 0x00000087 System.Int32 ModelCheckpointGraph::maxCheckpointScore(ModelCheckpoint)
extern void ModelCheckpointGraph_maxCheckpointScore_m8C56065617635D697F16E1CD16022D8EDBC3CE53 (void);
// 0x00000088 System.Void ModelCheckpointGraph::.ctor()
extern void ModelCheckpointGraph__ctor_m619F438C2E0C0B93BBACCA25D0CFFC2385CAAFE2 (void);
// 0x00000089 ModelCheckpoint ModelCheckpointManager::get_currentCheckpoint()
extern void ModelCheckpointManager_get_currentCheckpoint_mB94227839CB307305F45EF91A6D6D310C5F109E7 (void);
// 0x0000008A System.Void ModelCheckpointManager::EnterCheckpointGraph(ModelCheckpointGraph,System.Int32)
extern void ModelCheckpointManager_EnterCheckpointGraph_m61D3BEC535A9C1EACFC01D9FE3A8F8E30CF594CD (void);
// 0x0000008B System.Void ModelCheckpointManager::ExitCheckpointGraph()
extern void ModelCheckpointManager_ExitCheckpointGraph_m6F97C3D4B93ACBC36ACB6B212201669188C840C2 (void);
// 0x0000008C System.Void ModelCheckpointManager::SetAnswer(System.Int32,System.Object)
extern void ModelCheckpointManager_SetAnswer_m903FE7C899B40BE3B5D616702543BD2DC4A240A9 (void);
// 0x0000008D System.Void ModelCheckpointManager::SetCurrentAnswer(System.Object)
extern void ModelCheckpointManager_SetCurrentAnswer_mD9460524D061938852E85F923BB02C2DA6C06873 (void);
// 0x0000008E System.Void ModelCheckpointManager::TransitionToNext()
extern void ModelCheckpointManager_TransitionToNext_m361AAEADBF5B49AEEBC12780A328A672DF1C01CC (void);
// 0x0000008F System.Void ModelCheckpointManager::TransitionToPrevious()
extern void ModelCheckpointManager_TransitionToPrevious_m034CBCAA2D375EA51125387F31032E696131F3C0 (void);
// 0x00000090 System.Void ModelCheckpointManager::ResetCurrentCheckpoint()
extern void ModelCheckpointManager_ResetCurrentCheckpoint_m624D898BD7FCD72E3280A850F6FEF05B77BC4181 (void);
// 0x00000091 System.Void ModelCheckpointManager::TransitionToCheckpoint(System.Int32)
extern void ModelCheckpointManager_TransitionToCheckpoint_m53E3010135F291894A43DCA368E439FD29B68A8A (void);
// 0x00000092 System.Void ModelCheckpointManager::UpdateUI(ModelCheckpoint)
extern void ModelCheckpointManager_UpdateUI_mB92A3C81B0227BEB48159426089707B9FD0FAC32 (void);
// 0x00000093 System.Void ModelCheckpointManager::RegisterTransitions(System.Collections.Generic.List`1<ModelCheckpointTransition>)
extern void ModelCheckpointManager_RegisterTransitions_m1F54CDB4CF902480A50C45E1D833069C30B2F769 (void);
// 0x00000094 System.Void ModelCheckpointManager::UnregisterTransitions(System.Collections.Generic.List`1<ModelCheckpointTransition>)
extern void ModelCheckpointManager_UnregisterTransitions_m00E99AED02899CE166D16170FBD05467F477FF28 (void);
// 0x00000095 System.Void ModelCheckpointManager::RegisterTransitionConditions(System.Collections.Generic.List`1<ModelCheckpointTransitionCondition>)
extern void ModelCheckpointManager_RegisterTransitionConditions_m7E7BA2E2A34CE989189D0FC87BB8EEAC1694934A (void);
// 0x00000096 System.Void ModelCheckpointManager::UnregisterTransitionConditions(System.Collections.Generic.List`1<ModelCheckpointTransitionCondition>)
extern void ModelCheckpointManager_UnregisterTransitionConditions_m46D13BE795F728A5DEA3B1123BCD5D2066F129BC (void);
// 0x00000097 System.Void ModelCheckpointManager::.ctor()
extern void ModelCheckpointManager__ctor_m0024192EF51E956C2C83983D680ACFBBD14FFEA5 (void);
// 0x00000098 System.Void ModelCheckpointTransition::.ctor()
extern void ModelCheckpointTransition__ctor_mEB97A87D26DA24FF6CF80988F4C220B87DE08F23 (void);
// 0x00000099 System.Void ModelCheckpointTransitionCondition::RegisterWithModelInstance(ModelInstance)
// 0x0000009A System.Void ModelCheckpointTransitionCondition::UnregisterWithModelInstance(ModelInstance)
// 0x0000009B System.Void ModelCheckpointTransitionCondition::.ctor()
extern void ModelCheckpointTransitionCondition__ctor_mBA2E82308777F4F50F3FE96462FAC063594D8A03 (void);
// 0x0000009C System.Void LayerSelectedTransitionCondition::RegisterWithModelInstance(ModelInstance)
extern void LayerSelectedTransitionCondition_RegisterWithModelInstance_m23BCD0D83B0729C77160D60075A56DE122440E46 (void);
// 0x0000009D System.Void LayerSelectedTransitionCondition::UnregisterWithModelInstance(ModelInstance)
extern void LayerSelectedTransitionCondition_UnregisterWithModelInstance_m5BB241DA77DF022703C288F0E777D60C63A56F90 (void);
// 0x0000009E System.Void LayerSelectedTransitionCondition::.ctor()
extern void LayerSelectedTransitionCondition__ctor_m1EF8938128CE8591DEC6602190959E59E26255A6 (void);
// 0x0000009F System.Void FinalScoreUI::InitializeUI(System.Int32,System.Int32)
extern void FinalScoreUI_InitializeUI_m32C888843A4BFA7DE168DD74167442CED1B26B36 (void);
// 0x000000A0 System.Void FinalScoreUI::OnClickNext()
extern void FinalScoreUI_OnClickNext_mF9D50230AC3ADF564A78DB663AF6BE800FF53204 (void);
// 0x000000A1 System.Void FinalScoreUI::OnClickPrevious()
extern void FinalScoreUI_OnClickPrevious_mCBF0969A752B91720EE84F97724A7D3BADF48CD4 (void);
// 0x000000A2 System.Void FinalScoreUI::.ctor()
extern void FinalScoreUI__ctor_m0EDC340CF9C42DE96E9527EC2D1BFFB73F15F4D3 (void);
// 0x000000A3 System.Void ModelCheckpointUI::OnClickNext()
extern void ModelCheckpointUI_OnClickNext_m86E77D763D6994A27BDB0F6A3292960942B8CC13 (void);
// 0x000000A4 System.Void ModelCheckpointUI::OnClickPrevious()
extern void ModelCheckpointUI_OnClickPrevious_m099CAB6463295333756E1271D456E6CD3E6AC8DC (void);
// 0x000000A5 System.Void ModelCheckpointUI::.ctor()
extern void ModelCheckpointUI__ctor_m270CC8811F3E12344017FB3B32599A1FFA5725BB (void);
// 0x000000A6 System.Void MultipleChoiceUI::Start()
extern void MultipleChoiceUI_Start_mDB6EF356A27BD4CA3CA01BD5146EE059CDD2AAFB (void);
// 0x000000A7 System.Void MultipleChoiceUI::OnClickAnswer(System.Int32)
extern void MultipleChoiceUI_OnClickAnswer_mCBA48BB51BE1892BD70517990D8DB4E90DB51B8C (void);
// 0x000000A8 System.Void MultipleChoiceUI::InitializeUI(MultipleChoiceQuestionSO,System.Int32,System.Boolean,System.Boolean)
extern void MultipleChoiceUI_InitializeUI_m9DBDC702F9A7E66D1DC1E79200B3DEDB95B087CF (void);
// 0x000000A9 System.Void MultipleChoiceUI::OnClickNext()
extern void MultipleChoiceUI_OnClickNext_mC680466739F1B47DF8CB65E3BD215AF8FBE0DA15 (void);
// 0x000000AA System.Void MultipleChoiceUI::.ctor()
extern void MultipleChoiceUI__ctor_m170486305A6A8A0247A1EAD3245AB1790FDB7526 (void);
// 0x000000AB System.Void MultipleChoiceUI/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mA7A218F2E9C77FB26268730C96EE60460D6A5EC5 (void);
// 0x000000AC System.Void MultipleChoiceUI/<>c__DisplayClass9_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__0_m75C99484C0F726527A0A3687A4ECD46DE92DD317 (void);
// 0x000000AD System.Void MultipleChoiceUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m7A05CC31F8FED27EFEA8C00703876FB51E70409C (void);
// 0x000000AE System.Void MultipleChoiceUI/<>c::.ctor()
extern void U3CU3Ec__ctor_m288C77D78EAC61B9A11FBFD75F51CFD5240F47F3 (void);
// 0x000000AF System.Single MultipleChoiceUI/<>c::<InitializeUI>b__11_0(AnswerButton)
extern void U3CU3Ec_U3CInitializeUIU3Eb__11_0_mCF17191E2E1D70083F0CD123FFB8651FEAFAF340 (void);
// 0x000000B0 System.Void SequentialUI::InitializeUI(InformationSO)
extern void SequentialUI_InitializeUI_m8ED5CAA4CEE8D6B02C1577BF29EABD840610A112 (void);
// 0x000000B1 System.Void SequentialUI::.ctor()
extern void SequentialUI__ctor_m9C1E968898698AD18928055881385630F61DDD17 (void);
// 0x000000B2 System.Boolean AnnotationController::get_ShowAnnotations()
extern void AnnotationController_get_ShowAnnotations_mBFA03A11C19DBACE1D769A3C24C74232DBC31B32 (void);
// 0x000000B3 System.Void AnnotationController::set_ShowAnnotations(System.Boolean)
extern void AnnotationController_set_ShowAnnotations_m9AF6811BA19AE918D1BFE68C4C0CE6993DED00BA (void);
// 0x000000B4 System.Void AnnotationController::SetShowAnnotationsServerRpc(System.Boolean)
extern void AnnotationController_SetShowAnnotationsServerRpc_m3703CE74CDFAAF20733046AC16E077151E67D168 (void);
// 0x000000B5 System.Void AnnotationController::OnShowAnnotationsValueChanged(System.Boolean,System.Boolean)
extern void AnnotationController_OnShowAnnotationsValueChanged_m1FA40AF3FF36A62D103415D41BF54CA4EB9A8D59 (void);
// 0x000000B6 System.Void AnnotationController::SpawnAnnotation(AnnotationData)
extern void AnnotationController_SpawnAnnotation_m8A7234908B2EA0E0955E37175D0A83E5D60A1687 (void);
// 0x000000B7 System.Void AnnotationController::Start()
extern void AnnotationController_Start_m37B98FB591E634FFBC6E503FE5588BFC922878CB (void);
// 0x000000B8 System.Void AnnotationController::.ctor()
extern void AnnotationController__ctor_m31AE8A7B1EC9D71319BF28757A13DB5C69DE83FF (void);
// 0x000000B9 System.Void AnnotationData::.ctor()
extern void AnnotationData__ctor_mF8ED02FCDCE484F6746E4E005508FCBACB99BF16 (void);
// 0x000000BA System.Single ModelAnnotation::get_CurrentTime()
extern void ModelAnnotation_get_CurrentTime_mF97CD04DA28F26C693A43CDAAE31CBB20920B476 (void);
// 0x000000BB System.Void ModelAnnotation::set_CurrentTime(System.Single)
extern void ModelAnnotation_set_CurrentTime_m31A7C12BF2AEB610C69C99B87DDE10BA8CAE53B8 (void);
// 0x000000BC System.Boolean ModelAnnotation::get_LoopAnimation()
extern void ModelAnnotation_get_LoopAnimation_m1810CF3356DB6EDFF40A2584CA90DB10D87718F1 (void);
// 0x000000BD System.Void ModelAnnotation::set_LoopAnimation(System.Boolean)
extern void ModelAnnotation_set_LoopAnimation_m4E3261BDC6546A266646CAE169D61A58078EF293 (void);
// 0x000000BE System.Boolean ModelAnnotation::get_IsSelected()
extern void ModelAnnotation_get_IsSelected_mC128BBB3BAA86895DC23DB80458E11BCCFD793CE (void);
// 0x000000BF System.Void ModelAnnotation::set_IsSelected(System.Boolean)
extern void ModelAnnotation_set_IsSelected_mCB0100D52563EB986862B0903839AA93D6BF9CAD (void);
// 0x000000C0 System.Void ModelAnnotation::OnFirstHoverEntered(UnityEngine.XR.Interaction.Toolkit.HoverEnterEventArgs)
extern void ModelAnnotation_OnFirstHoverEntered_m91DE0D8D6B3EAB7CEEB819D0C4DFBC3579D94BFA (void);
// 0x000000C1 System.Void ModelAnnotation::OnLastHoverExited(UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs)
extern void ModelAnnotation_OnLastHoverExited_mF99CC04D1A21A8BEBFAFBF22D3434096D91207DF (void);
// 0x000000C2 System.Void ModelAnnotation::OnClicked()
extern void ModelAnnotation_OnClicked_m14BBDD528D30F78B5CAFDA4DFAD381BDEC517A02 (void);
// 0x000000C3 System.Void ModelAnnotation::OpenWindow()
extern void ModelAnnotation_OpenWindow_m11261310B765DAAAB7A0AA33D1454457CABA95B6 (void);
// 0x000000C4 System.Void ModelAnnotation::CloseWindow()
extern void ModelAnnotation_CloseWindow_m67062A594780FC6B62FA10C143BBC24EB6B4A486 (void);
// 0x000000C5 System.Void ModelAnnotation::SetAnnotationData(AnnotationData)
extern void ModelAnnotation_SetAnnotationData_mDB3E5C90D19D95DFEC83E048298693C0CCA9F266 (void);
// 0x000000C6 System.Void ModelAnnotation::Start()
extern void ModelAnnotation_Start_m6132D032D969CF83F11749093CEBBBDA8DB49BB9 (void);
// 0x000000C7 System.Void ModelAnnotation::.ctor()
extern void ModelAnnotation__ctor_m5B898CE67FC24CF042A535965F18E0F4D0E98E3D (void);
// 0x000000C8 System.Void ModelAnnotation::<CloseWindow>b__27_0()
extern void ModelAnnotation_U3CCloseWindowU3Eb__27_0_mD5DC5F7DC25C193391D1C5833E565D5C9350B074 (void);
// 0x000000C9 System.Void AudioManager::Start()
extern void AudioManager_Start_m3C0FEAF19F58B6D28A9E6D815B3AAF94FEA21B69 (void);
// 0x000000CA System.Void AudioManager::OnDestroy()
extern void AudioManager_OnDestroy_m94C77771783976448AE56345E76E7AE6A7C2CD6C (void);
// 0x000000CB System.Void AudioManager::OnNodeSelected(System.Int32)
extern void AudioManager_OnNodeSelected_m7200214879BF213F793179D629E790792B14670F (void);
// 0x000000CC System.Void AudioManager::.ctor()
extern void AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6 (void);
// 0x000000CD System.Void ClippingPlaneController::add_clippingPlaneStateChanged(System.Action)
extern void ClippingPlaneController_add_clippingPlaneStateChanged_mC39F39C0241DB78D4D39818C8928E0806AB2AF02 (void);
// 0x000000CE System.Void ClippingPlaneController::remove_clippingPlaneStateChanged(System.Action)
extern void ClippingPlaneController_remove_clippingPlaneStateChanged_mAC4A481C21C2040FABFFC352FF1B071453B3513F (void);
// 0x000000CF System.Void ClippingPlaneController::OnNetworkSpawn()
extern void ClippingPlaneController_OnNetworkSpawn_mB485343A8549C05FA9E1D8AF9AE0333EF5D33E81 (void);
// 0x000000D0 System.Void ClippingPlaneController::AddLayer(LayerInstance)
extern void ClippingPlaneController_AddLayer_m831187D7217015B76241163ACFBC1443AC2243A8 (void);
// 0x000000D1 System.Void ClippingPlaneController::AddNode(ModelNode)
extern void ClippingPlaneController_AddNode_m21E3C1FA68673808EC86D2303D25565016FB4D2B (void);
// 0x000000D2 ClippingPlaneState ClippingPlaneController::get_state()
extern void ClippingPlaneController_get_state_m2C510CEEEFE64529FFB3E5CC140D3A67D18DD988 (void);
// 0x000000D3 System.Void ClippingPlaneController::SetClippingPlaneState(ClippingPlaneState,ClippingPlaneStateTransition)
extern void ClippingPlaneController_SetClippingPlaneState_mC555EE06E9380DFE14E910FBECB22863E05E82EB (void);
// 0x000000D4 System.Void ClippingPlaneController::SetClippingPlaneStateServerRpc(ClippingPlaneState,ClippingPlaneStateTransition)
extern void ClippingPlaneController_SetClippingPlaneStateServerRpc_m58F18CDFDBF4D0536D8222632C045CFE9F050FEA (void);
// 0x000000D5 System.Void ClippingPlaneController::SetLocalTransformServerRpc(LocalTransform,System.Single,System.Boolean,System.Boolean,System.Boolean)
extern void ClippingPlaneController_SetLocalTransformServerRpc_m71212BE765B7D2D4BB9F89C44600660EE6C3CC68 (void);
// 0x000000D6 System.Boolean ClippingPlaneController::get_IsActive()
extern void ClippingPlaneController_get_IsActive_m5954FE12AA81F00ED6F4DA8FA59E3147A02B5F5E (void);
// 0x000000D7 System.Void ClippingPlaneController::set_IsActive(System.Boolean)
extern void ClippingPlaneController_set_IsActive_mCDD90EF30934CF3FAED7CA802799627D39C4EC5F (void);
// 0x000000D8 System.Void ClippingPlaneController::SetIsActiveServerRpc(System.Boolean)
extern void ClippingPlaneController_SetIsActiveServerRpc_m00EA41F590AC49483A9B60037E8E4DE25D1D0DB9 (void);
// 0x000000D9 System.Void ClippingPlaneController::OnIsActiveValueChanged(System.Boolean,System.Boolean)
extern void ClippingPlaneController_OnIsActiveValueChanged_m06168D978037095B0661AAE77FC7454E66D6D044 (void);
// 0x000000DA System.Int32 ClippingPlaneController::get_TextureIdx()
extern void ClippingPlaneController_get_TextureIdx_m9ACC522B1D4F8040685B93E321122F8E8EDD0CB7 (void);
// 0x000000DB System.Void ClippingPlaneController::set_TextureIdx(System.Int32)
extern void ClippingPlaneController_set_TextureIdx_m5D2B4D7E9E8CB7FB83C9A43E344A15D10E9F2C67 (void);
// 0x000000DC System.Void ClippingPlaneController::SetTextureIdxServerRpc(System.Int32)
extern void ClippingPlaneController_SetTextureIdxServerRpc_m88F009CA70D8F0B6BB4FA0DAA34F621BA8686942 (void);
// 0x000000DD System.Void ClippingPlaneController::OnTextureIdxValueChanged(System.Int32,System.Int32)
extern void ClippingPlaneController_OnTextureIdxValueChanged_m3D30A00D87A5744E45136C38DF7D8DE63B141B5B (void);
// 0x000000DE ClippingPlaneType ClippingPlaneController::get_Type()
extern void ClippingPlaneController_get_Type_m58F002E187DCB93F9411D5AD3AB1BDAA083F9D26 (void);
// 0x000000DF System.Void ClippingPlaneController::set_Type(ClippingPlaneType)
extern void ClippingPlaneController_set_Type_mF979EBF4D6BE918D28254BD86034FF532E62CC3D (void);
// 0x000000E0 System.Void ClippingPlaneController::SetClippingPlaneTypeServerRpc(ClippingPlaneType)
extern void ClippingPlaneController_SetClippingPlaneTypeServerRpc_m555C78FEFAF0320052D821DE839C104C1ED86922 (void);
// 0x000000E1 System.Void ClippingPlaneController::.ctor()
extern void ClippingPlaneController__ctor_mDB4AF6C8E7D15EC271B36411B6B1E2A6E5A42194 (void);
// 0x000000E2 System.Void ClippingPlaneController::__initializeVariables()
extern void ClippingPlaneController___initializeVariables_m65F8B42A0970FC3DBE748CAB2FB7407CCA14BBF7 (void);
// 0x000000E3 System.Void ClippingPlaneController::__initializeRpcs()
extern void ClippingPlaneController___initializeRpcs_m1CAA5EBC3F767A2F0236616F573F7D63E72DA10C (void);
// 0x000000E4 System.Void ClippingPlaneController::__rpc_handler_72846158(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ClippingPlaneController___rpc_handler_72846158_m9910D552A146D80C13E63D568B1203E859A72B6F (void);
// 0x000000E5 System.Void ClippingPlaneController::__rpc_handler_3722869538(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ClippingPlaneController___rpc_handler_3722869538_mA508C28A6293BE8EC89E611D5C8F8348512353D8 (void);
// 0x000000E6 System.Void ClippingPlaneController::__rpc_handler_3602782283(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ClippingPlaneController___rpc_handler_3602782283_m2EADE9B1DF7DCD9782CDDD421EFD1A3D9FE9885D (void);
// 0x000000E7 System.Void ClippingPlaneController::__rpc_handler_267534928(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ClippingPlaneController___rpc_handler_267534928_mD4A623D6595461564045A297838432AAC36A337C (void);
// 0x000000E8 System.Void ClippingPlaneController::__rpc_handler_2099587104(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ClippingPlaneController___rpc_handler_2099587104_m03FACAD6A80DAE12A1B8542AF2AE2FF4F00F5D09 (void);
// 0x000000E9 System.String ClippingPlaneController::__getTypeName()
extern void ClippingPlaneController___getTypeName_m343EB1C4D2BB55C6FF501E5F70967156213F8EE6 (void);
// 0x000000EA UnityEngine.Material CustomClippingPlane::get_sliceMaterialInstance()
extern void CustomClippingPlane_get_sliceMaterialInstance_mBD1E5CA6A0FE07A7DF3D368D9B85A7DB31F965D3 (void);
// 0x000000EB VolumetricTexture CustomClippingPlane::get_VolumetricTexture()
extern void CustomClippingPlane_get_VolumetricTexture_m7F931365C13F18A2AC26F59CE719AADE374557D3 (void);
// 0x000000EC System.Void CustomClippingPlane::set_VolumetricTexture(VolumetricTexture)
extern void CustomClippingPlane_set_VolumetricTexture_m7FF9EB5ADA3C8FA85B3EF5C0FA9923810FDA9A07 (void);
// 0x000000ED System.Void CustomClippingPlane::Start()
extern void CustomClippingPlane_Start_m8949BC9D71A967AD03E3DC1A1B520AF165DE716F (void);
// 0x000000EE System.Void CustomClippingPlane::Update()
extern void CustomClippingPlane_Update_mF6EF7114DE905C9E9801EF5FAD6529F728D79242 (void);
// 0x000000EF System.Void CustomClippingPlane::InitializeOnStart()
extern void CustomClippingPlane_InitializeOnStart_m6FEBA6F907BFF639AB4C7BFB6E6FBAD06AEC79E1 (void);
// 0x000000F0 System.Void CustomClippingPlane::.ctor()
extern void CustomClippingPlane__ctor_m4F0A8078DA5644A33A0F47D5B512985D3147E816 (void);
// 0x000000F1 System.Boolean InputController::get_IndividualMovementActive()
extern void InputController_get_IndividualMovementActive_mF9DBF69BDBF8D34059DB5B515C452A3046980E2C (void);
// 0x000000F2 System.Void InputController::set_IndividualMovementActive(System.Boolean)
extern void InputController_set_IndividualMovementActive_m9AA9406AB4B4AF1AE500C14245AC33AE4047660B (void);
// 0x000000F3 System.Void InputController::SetHostTransformsForNodeSelection(System.Int32)
extern void InputController_SetHostTransformsForNodeSelection_m3FC448024436A1F8E8915D770176AA7D59F0EBFC (void);
// 0x000000F4 System.Void InputController::SetHostTransformsForHoveredNode(System.Int32)
extern void InputController_SetHostTransformsForHoveredNode_m9773A5A959B520EFB0B522CEE733A9EF8C408263 (void);
// 0x000000F5 System.Void InputController::Start()
extern void InputController_Start_mC887BF47D15F6F54A07FCE6717EBB85DFBDEE479 (void);
// 0x000000F6 System.Void InputController::.ctor()
extern void InputController__ctor_m0EDBE66635C62BD8D770DC8E9C8D90BECD52A00D (void);
// 0x000000F7 System.Void InputController::__initializeVariables()
extern void InputController___initializeVariables_mF046FD9A1349DDF428DE1ED8B62006A37A9068A1 (void);
// 0x000000F8 System.String InputController::__getTypeName()
extern void InputController___getTypeName_m3630C659AA422AA875E22FF8BA3B79D6274D2432 (void);
// 0x000000F9 ModelNodeScriptableObject LayerInstance::get_NodeInfo()
extern void LayerInstance_get_NodeInfo_m7734018445E118724F6089A829A78E62C9096256 (void);
// 0x000000FA System.Void LayerInstance::set_NodeInfo(ModelNodeScriptableObject)
extern void LayerInstance_set_NodeInfo_mA56291AF6E1C999041BEFA8E6744EDEE4AFC2F3E (void);
// 0x000000FB LayerScriptableObject LayerInstance::get_LayerInfo()
extern void LayerInstance_get_LayerInfo_mE43E0E6C6793F15EEF5B514E7F9D6CB310D689B2 (void);
// 0x000000FC System.Void LayerInstance::set_LayerInfo(LayerScriptableObject)
extern void LayerInstance_set_LayerInfo_m996FAEABD85D0EE02E1C6924A6026A0488209405 (void);
// 0x000000FD cakeslice.Outline LayerInstance::get_outline()
extern void LayerInstance_get_outline_m899E9D6F694AC870AF8C910F9313559FAD4DA5C3 (void);
// 0x000000FE System.Void LayerInstance::OnNetworkSpawn()
extern void LayerInstance_OnNetworkSpawn_m34D844ABCE8A01C0661FF2FAF19A012C636FD14E (void);
// 0x000000FF LayerState LayerInstance::get_State()
extern void LayerInstance_get_State_mC0F204E00DF3FAF88E23E706BFF4305E2B7CC99F (void);
// 0x00000100 LayerState LayerInstance::get_DefaultState()
extern void LayerInstance_get_DefaultState_mDE36347458C584CD8E59E2499B01FD41472CCA4B (void);
// 0x00000101 System.Void LayerInstance::SetNodeState(NodeState,NodeStateTransition)
extern void LayerInstance_SetNodeState_m35D6BE092FF14509FBFCCA29D2EC3C37431BC456 (void);
// 0x00000102 System.Void LayerInstance::SetNodeState(LayerState,LayerStateTransition)
extern void LayerInstance_SetNodeState_m831D503015CB6E7BAB7090A15A76D2FBA6EB96D2 (void);
// 0x00000103 System.Boolean LayerInstance::get_IsOutlined()
extern void LayerInstance_get_IsOutlined_m1F565A9BE9913A292547274104439453780DD0E2 (void);
// 0x00000104 System.Void LayerInstance::set_IsOutlined(System.Boolean)
extern void LayerInstance_set_IsOutlined_m6F75130C216EF00257D6E3A73F7B727ADF17A6A0 (void);
// 0x00000105 System.Void LayerInstance::SetIsOutlinedServerRpc(System.Boolean)
extern void LayerInstance_SetIsOutlinedServerRpc_m32E0385C71A5FD139248E5062D74D0E373428F33 (void);
// 0x00000106 System.Void LayerInstance::OnIsOutlinedValueChanged(System.Boolean,System.Boolean)
extern void LayerInstance_OnIsOutlinedValueChanged_m2C00CC0CD34F670CA7BB9100C794525FE6EE0B70 (void);
// 0x00000107 System.Void LayerInstance::set_OutlineColorIdx(System.Int32)
extern void LayerInstance_set_OutlineColorIdx_m72EC7BF108728F6B3BA79143D2C56FCC289784DB (void);
// 0x00000108 System.Void LayerInstance::OnOutlineColorIdxChanged(System.Int32,System.Int32)
extern void LayerInstance_OnOutlineColorIdxChanged_m014A0A7715CD5037DF462F6E64D86275C44D6795 (void);
// 0x00000109 System.Void LayerInstance::SetOutlineColorIdxServerRpc(System.Int32)
extern void LayerInstance_SetOutlineColorIdxServerRpc_m5C84A66F8DDAE0BC0FC4EF8442FF5A4BF627EE81 (void);
// 0x0000010A System.Boolean LayerInstance::get_IsHidden()
extern void LayerInstance_get_IsHidden_m78E336C4A2F834E17E8CA81A71E2020C52B04733 (void);
// 0x0000010B System.Void LayerInstance::set_IsHidden(System.Boolean)
extern void LayerInstance_set_IsHidden_m7EB1192F9618A1D8400ADF485C3B0FCC1F35E2B0 (void);
// 0x0000010C System.Void LayerInstance::SetIsHiddenServerRpc(System.Boolean)
extern void LayerInstance_SetIsHiddenServerRpc_mFFAC8785B21C4CCACB9C591B068584FCEC722A14 (void);
// 0x0000010D System.Void LayerInstance::OnIsHiddenValueChanged(System.Boolean,System.Boolean)
extern void LayerInstance_OnIsHiddenValueChanged_m80D7F93F5680A052B988414DC3D92F412F151FD1 (void);
// 0x0000010E System.Boolean LayerInstance::get_IsFaded()
extern void LayerInstance_get_IsFaded_m422D59B8FB140CEDF8E8443E467124B3317D8781 (void);
// 0x0000010F System.Void LayerInstance::set_IsFaded(System.Boolean)
extern void LayerInstance_set_IsFaded_m5E6BE5AB15C3F24E9DC6C70DFB7C85EA10E2380C (void);
// 0x00000110 System.Void LayerInstance::SetIsFadedServerRpc(System.Boolean)
extern void LayerInstance_SetIsFadedServerRpc_m8D966EC8336469F56A93C58246B92EC15122C726 (void);
// 0x00000111 System.Void LayerInstance::OnIsFadedValueChanged(System.Boolean,System.Boolean)
extern void LayerInstance_OnIsFadedValueChanged_m5B6DD7BD00DE52E1B2D13231299BCBEE8F13B03B (void);
// 0x00000112 System.Void LayerInstance::.ctor()
extern void LayerInstance__ctor_m24B751A229800818299E16316FCBBFE892F3F9CD (void);
// 0x00000113 System.Void LayerInstance::__initializeVariables()
extern void LayerInstance___initializeVariables_mBB27B3B7F9C2F9AEF7A4CFE1D833812403E5E6A7 (void);
// 0x00000114 System.Void LayerInstance::__initializeRpcs()
extern void LayerInstance___initializeRpcs_m2ACB71C420356F63560EFE84C5781A030A922A21 (void);
// 0x00000115 System.Void LayerInstance::__rpc_handler_449397666(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void LayerInstance___rpc_handler_449397666_m4F931DB3EFBF858B8DF1BDA3490CF61C7BCF8DCB (void);
// 0x00000116 System.Void LayerInstance::__rpc_handler_3704825583(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void LayerInstance___rpc_handler_3704825583_m16C047F2855C40300096FA4F9FE6D6235A96A304 (void);
// 0x00000117 System.Void LayerInstance::__rpc_handler_268871636(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void LayerInstance___rpc_handler_268871636_mBA1878DC5AA2BB76CD073D4A9546A03E5FBAD141 (void);
// 0x00000118 System.Void LayerInstance::__rpc_handler_3328835206(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void LayerInstance___rpc_handler_3328835206_mEC8A017C52A69B2AA6BF0AC358F3E2E10E7BFE7A (void);
// 0x00000119 System.String LayerInstance::__getTypeName()
extern void LayerInstance___getTypeName_mFF3C275D57A965101E54DE935E8C94E398A67BA0 (void);
// 0x0000011A System.Threading.Tasks.Task`1<System.Collections.Generic.List`1<Unity.Netcode.NetworkObject>> LayerSpawner::SpawnNodeAsync(TreeView.TreeNode`1<ModelNodeScriptableObject>,UnityEngine.Transform)
extern void LayerSpawner_SpawnNodeAsync_m9C281B992CE7FBB4268C4BE8F96EF02644037EB3 (void);
// 0x0000011B System.Void LayerSpawner::RecreateTree(ModelScriptableObject,System.Collections.Generic.List`1<ModelNode>)
extern void LayerSpawner_RecreateTree_mA4F75A39730B3E738273BAD0C24453531A6E55FA (void);
// 0x0000011C System.Threading.Tasks.Task LayerSpawner::InitializeInstantiatedLayersAsync(System.Collections.Generic.IEnumerable`1<ModelNodeScriptableObject>,Unity.Netcode.NetworkObjectReference[])
extern void LayerSpawner_InitializeInstantiatedLayersAsync_m10A6B32B3EA41B435BA0843B5B72F8048F34E532 (void);
// 0x0000011D System.Void LayerSpawner::.ctor()
extern void LayerSpawner__ctor_m6C9D5FD769B4FA0DBDFBB4C455A80278DEA396EE (void);
// 0x0000011E System.Void LayerSpawner::__initializeVariables()
extern void LayerSpawner___initializeVariables_m23907B4E15E6129C1E7A87755F16DA7FA96FF3E9 (void);
// 0x0000011F System.String LayerSpawner::__getTypeName()
extern void LayerSpawner___getTypeName_m279372698B26DB977A2F9D87F6D0ECAB7934EE4E (void);
// 0x00000120 System.Void LayerSpawner/<SpawnNodeAsync>d__6::MoveNext()
extern void U3CSpawnNodeAsyncU3Ed__6_MoveNext_mECD34BB602497E62BD79E97238DE2A82B177253D (void);
// 0x00000121 System.Void LayerSpawner/<SpawnNodeAsync>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSpawnNodeAsyncU3Ed__6_SetStateMachine_mBE32AC90DEA5CE29BD86EDC48EC557CEB4207597 (void);
// 0x00000122 System.Void LayerSpawner/<>c::.cctor()
extern void U3CU3Ec__cctor_m022F97DB2194341D2028ACE07D6684AE6E2B6672 (void);
// 0x00000123 System.Void LayerSpawner/<>c::.ctor()
extern void U3CU3Ec__ctor_mABC0DF3B1B4B397D55392B7164D3A24076EB9F2B (void);
// 0x00000124 <>f__AnonymousType0`2<Unity.Netcode.NetworkObjectReference,ModelNodeScriptableObject> LayerSpawner/<>c::<InitializeInstantiatedLayersAsync>b__8_0(Unity.Netcode.NetworkObjectReference,ModelNodeScriptableObject)
extern void U3CU3Ec_U3CInitializeInstantiatedLayersAsyncU3Eb__8_0_m303A29D403F69282983913882E0413DC953EA1DA (void);
// 0x00000125 System.Void LayerSpawner/<InitializeInstantiatedLayersAsync>d__8::MoveNext()
extern void U3CInitializeInstantiatedLayersAsyncU3Ed__8_MoveNext_m391D984F9F94FA0589D9B4A8839FBAB9056F9698 (void);
// 0x00000126 System.Void LayerSpawner/<InitializeInstantiatedLayersAsync>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInitializeInstantiatedLayersAsyncU3Ed__8_SetStateMachine_m3C625409AFF0F2D49092A283948F50AFEE5C54FF (void);
// 0x00000127 System.Void ModelAnimator::add_AnimationStateChanged(System.Action)
extern void ModelAnimator_add_AnimationStateChanged_mDDC3FF52CCCCF4249BCB7B6D06E230EDC35C2CD8 (void);
// 0x00000128 System.Void ModelAnimator::remove_AnimationStateChanged(System.Action)
extern void ModelAnimator_remove_AnimationStateChanged_mEE1872E7E3B2AD3065FFBBD14F5DC7DDDD5EF36D (void);
// 0x00000129 System.Void ModelAnimator::OnNetworkSpawn()
extern void ModelAnimator_OnNetworkSpawn_m5FA49F52879D63F5F8A755612850949CF9155453 (void);
// 0x0000012A System.Void ModelAnimator::Update()
extern void ModelAnimator_Update_m26BC96D3AC5BDAB3E401CA207085044B1CA57A4A (void);
// 0x0000012B System.Void ModelAnimator::UpdateMirror()
extern void ModelAnimator_UpdateMirror_m993373EA45B84296F74C7F4CCE38F5D697FEB71F (void);
// 0x0000012C System.Void ModelAnimator::UpdateCyclic()
extern void ModelAnimator_UpdateCyclic_m67E606F2CEA2453D9D264513F80728A0A69211F1 (void);
// 0x0000012D System.Void ModelAnimator::UpdateOnce()
extern void ModelAnimator_UpdateOnce_mE7C9EF77D315817BCF43B3E2AA8EE563F02C0DCD (void);
// 0x0000012E System.Void ModelAnimator::AddAnimation(IAnimatable)
extern void ModelAnimator_AddAnimation_m2D092C041661058F9D9ED6DEBA86FEED11BB0E4C (void);
// 0x0000012F System.Void ModelAnimator::SetAnimationState(AnimationState,AnimationStateTransition)
extern void ModelAnimator_SetAnimationState_mDC1CF6B70E7D48418B141304F6FC671501D00709 (void);
// 0x00000130 System.Void ModelAnimator::SetAnimationStateServerRpc(AnimationState,AnimationStateTransition)
extern void ModelAnimator_SetAnimationStateServerRpc_m3F996DF2582C5AFA41246590A5977A875249AAFC (void);
// 0x00000131 AnimationState ModelAnimator::get_state()
extern void ModelAnimator_get_state_mAB3F7ED5FF65C7D3946D705B5E8A7C8CE28C78AC (void);
// 0x00000132 AnimationMode ModelAnimator::get_AnimationMode()
extern void ModelAnimator_get_AnimationMode_m167AE77E0721E0B7CA98A1460DF7A9779838227E (void);
// 0x00000133 System.Void ModelAnimator::set_AnimationMode(AnimationMode)
extern void ModelAnimator_set_AnimationMode_mB34F9F78F41DD14393B1030430D270F76CB38811 (void);
// 0x00000134 System.Void ModelAnimator::SetAnimationModeServerRpc(AnimationMode)
extern void ModelAnimator_SetAnimationModeServerRpc_mB4981B5176AAE47F0AAF84DC8792DBE00C402E76 (void);
// 0x00000135 System.Void ModelAnimator::OnAnimationModeValueChanged(AnimationMode,AnimationMode)
extern void ModelAnimator_OnAnimationModeValueChanged_m5F08C09B662843F97865B8462C74324CA6444A38 (void);
// 0x00000136 System.Single ModelAnimator::get_CurrentTime()
extern void ModelAnimator_get_CurrentTime_mC927C5576C3CC900A683DE11956D70DCC0EF02D4 (void);
// 0x00000137 System.Void ModelAnimator::set_CurrentTime(System.Single)
extern void ModelAnimator_set_CurrentTime_mFD120F65FAC9E1C9540599654106C238C2B76713 (void);
// 0x00000138 System.Void ModelAnimator::SetCurrentTimeServerRpc(System.Single)
extern void ModelAnimator_SetCurrentTimeServerRpc_mC0CDEFC47DC56771DE38874F9E5039C8D69A0A8F (void);
// 0x00000139 System.Void ModelAnimator::OnCurrentTimeChanged(System.Single,System.Single)
extern void ModelAnimator_OnCurrentTimeChanged_mAB249A8B99941562020AA5E07B8E9EEE2F7ADE27 (void);
// 0x0000013A System.Void ModelAnimator::SetAnimationSpeedServerRpc(System.Single)
extern void ModelAnimator_SetAnimationSpeedServerRpc_m3B5F8EC4B131AC1FBCF4DB8B806DDCC271A68CB0 (void);
// 0x0000013B System.Single ModelAnimator::get_AnimationSpeed()
extern void ModelAnimator_get_AnimationSpeed_mCE1D61A885DCB263D8CEFD9514EF480AF375EC42 (void);
// 0x0000013C System.Void ModelAnimator::set_AnimationSpeed(System.Single)
extern void ModelAnimator_set_AnimationSpeed_m4BBD8441F3A7AEEA8234834A8A2295AE814C4530 (void);
// 0x0000013D System.Boolean ModelAnimator::get_IsPlaying()
extern void ModelAnimator_get_IsPlaying_mA88505311AF4B85FE33A95DFAE80DA5510973281 (void);
// 0x0000013E System.Void ModelAnimator::set_IsPlaying(System.Boolean)
extern void ModelAnimator_set_IsPlaying_m0843798CB8FC60D56B650534CDDD614C4487410C (void);
// 0x0000013F System.Void ModelAnimator::SetIsPlayingServerRpc(System.Boolean)
extern void ModelAnimator_SetIsPlayingServerRpc_m5D3D273972D2FDDD360DE61B4336E726B4AB0413 (void);
// 0x00000140 System.Void ModelAnimator::Stop()
extern void ModelAnimator_Stop_mC105889983436F213262C43F3F52397216584373 (void);
// 0x00000141 System.Void ModelAnimator::TogglePlay()
extern void ModelAnimator_TogglePlay_m59D54C3C5711B76623B69E2CB5F2F3E55318F9F3 (void);
// 0x00000142 System.Void ModelAnimator::Play()
extern void ModelAnimator_Play_m5AE3C2FDD24A38EA756095A7E06AAA54E180D294 (void);
// 0x00000143 System.Void ModelAnimator::Pause()
extern void ModelAnimator_Pause_m05D1D6F5AD891D8583EEC13E7F1AFEE51C1E97E5 (void);
// 0x00000144 System.Void ModelAnimator::.ctor()
extern void ModelAnimator__ctor_m80904E202E59D051FDA4C0A21EB0884FC4587645 (void);
// 0x00000145 System.Void ModelAnimator::__initializeVariables()
extern void ModelAnimator___initializeVariables_m47AD8CFB78D4C34BA5287BCB514C5DF32AA314A9 (void);
// 0x00000146 System.Void ModelAnimator::__initializeRpcs()
extern void ModelAnimator___initializeRpcs_mDF08140C806500B7DFE27BDFBCA7021608C6A321 (void);
// 0x00000147 System.Void ModelAnimator::__rpc_handler_2956263290(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelAnimator___rpc_handler_2956263290_mB084B7652A2C3FE251DA5C37F6FBB4B53D03A908 (void);
// 0x00000148 System.Void ModelAnimator::__rpc_handler_2433975334(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelAnimator___rpc_handler_2433975334_m3A497F37D71A9EEEF4BDA058A949030864E714DA (void);
// 0x00000149 System.Void ModelAnimator::__rpc_handler_805743151(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelAnimator___rpc_handler_805743151_m6BBE20BBC2C401ED815449997E77CD3DE6E4EE4F (void);
// 0x0000014A System.Void ModelAnimator::__rpc_handler_1141981096(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelAnimator___rpc_handler_1141981096_m4EC3F50D6F00C3230601F9B048FC97095A37F70F (void);
// 0x0000014B System.Void ModelAnimator::__rpc_handler_866973770(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelAnimator___rpc_handler_866973770_m6105B6AF72200FD85979D8CA52B8976BE1DB8544 (void);
// 0x0000014C System.String ModelAnimator::__getTypeName()
extern void ModelAnimator___getTypeName_m9A206A0B608260D25686AB88F13009FB09999011 (void);
// 0x0000014D System.Void ModelInstance::Start()
extern void ModelInstance_Start_mD78A7AE42FCF01704B069578DE7ED3054D2C70D5 (void);
// 0x0000014E System.Void ModelInstance::InitializeComponents()
extern void ModelInstance_InitializeComponents_m46A27AA4AAAC77C88AFDE7A769316118D1FB2906 (void);
// 0x0000014F System.Void ModelInstance::OnDestroy()
extern void ModelInstance_OnDestroy_m6D54E721C57F3E803530483EC01FB956AEB5736A (void);
// 0x00000150 System.Void ModelInstance::PositionUIMenus(UnityEngine.Vector3)
extern void ModelInstance_PositionUIMenus_mDE83631CE388833C4B4486F1C3283F2A38BD26BD (void);
// 0x00000151 System.Void ModelInstance::SetModel(System.String)
extern void ModelInstance_SetModel_mE1C22F6A7F9FAEA4E6B68803DCE73CB1265CB8EE (void);
// 0x00000152 System.Void ModelInstance::SetModelServerRpc(System.String)
extern void ModelInstance_SetModelServerRpc_mAE6750E0AC15D698915A907B435CB86BA7C52339 (void);
// 0x00000153 System.Void ModelInstance::SetModelServerAsync(System.String)
extern void ModelInstance_SetModelServerAsync_mCA39FD0E53D091C6BA30077C67BC94CFEA0B6460 (void);
// 0x00000154 System.Void ModelInstance::SetModelClientRpc(System.String,Unity.Netcode.NetworkObjectReference[])
extern void ModelInstance_SetModelClientRpc_m07885ECF29CFA6CE2285AE6E35011745A6EDF36A (void);
// 0x00000155 System.Void ModelInstance::SetModelClientAsync(System.String,Unity.Netcode.NetworkObjectReference[])
extern void ModelInstance_SetModelClientAsync_m5986DC5C683770D3970C8BAB8F927A6DEEC650A7 (void);
// 0x00000156 System.Void ModelInstance::SetLoadingStateClientRpc(System.Boolean,System.String)
extern void ModelInstance_SetLoadingStateClientRpc_mE43DFE0B954BD201232FF9182688DCB29CFD127E (void);
// 0x00000157 System.Void ModelInstance::SetLoadingState(System.Boolean,System.String)
extern void ModelInstance_SetLoadingState_mF74B45AA60EA35DB303EFE4BAD814157C8D99E78 (void);
// 0x00000158 System.Void ModelInstance::InitializeClippingPlaneClientRpc(Unity.Netcode.NetworkObjectReference)
extern void ModelInstance_InitializeClippingPlaneClientRpc_m569A41F3809BE622DC4E03F521E3267F147D9282 (void);
// 0x00000159 System.Void ModelInstance::InitializeNodeContainerClientRpc(Unity.Netcode.NetworkObjectReference)
extern void ModelInstance_InitializeNodeContainerClientRpc_mE3031D6E8B0B6D1AA51706F0E5DA3C1F33E49230 (void);
// 0x0000015A System.Void ModelInstance::ToggleBoundingBox()
extern void ModelInstance_ToggleBoundingBox_m8196477E6916BE6E8C8454017B3C29E84E0B41E1 (void);
// 0x0000015B ModelInstanceState ModelInstance::get_state()
extern void ModelInstance_get_state_mCC0E2EC824D2BF90D5AEC9329B3CEC5528166E0A (void);
// 0x0000015C System.Void ModelInstance::SetModelState(ModelInstanceState,ModelInstanceStateTransition,System.Boolean)
extern void ModelInstance_SetModelState_m69C93D07959A0A6C826A7EB208F1A21D75546180 (void);
// 0x0000015D System.Void ModelInstance::SetModelStateServerRpc(ModelInstanceState,ModelInstanceStateTransition,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ModelInstance_SetModelStateServerRpc_mF957427F2448619757A46D988199FC896FFC1D96 (void);
// 0x0000015E System.Void ModelInstance::SetNodeContainerGlobalRotationServerRpc(UnityEngine.Quaternion,System.Single)
extern void ModelInstance_SetNodeContainerGlobalRotationServerRpc_m6E9DC95430BA74A72A48054D0A2CB249A0F46583 (void);
// 0x0000015F System.Void ModelInstance::SetNodeContainerGlobalPositionServerRpc(UnityEngine.Vector3,System.Single)
extern void ModelInstance_SetNodeContainerGlobalPositionServerRpc_m5D17B55A3294B8172DB2A56F911617E836D0DE40 (void);
// 0x00000160 System.Void ModelInstance::SetNodeContainerScaleServerRpc(UnityEngine.Vector3,System.Single)
extern void ModelInstance_SetNodeContainerScaleServerRpc_m5D536B0BBFA832E41D921812F745C7C91BC8E46A (void);
// 0x00000161 ModelScriptableObject ModelInstance::get_CurrentModel()
extern void ModelInstance_get_CurrentModel_mEAD5830B10BE1100571289CA04567085DC1F0B9B (void);
// 0x00000162 System.Void ModelInstance::DestroyModel()
extern void ModelInstance_DestroyModel_m20698CB1281B14036D531B86063D32518F9A5DFB (void);
// 0x00000163 System.Void ModelInstance::DestroyModelServerRpc()
extern void ModelInstance_DestroyModelServerRpc_m11B0794ACD84313CFA8B41B5DBCD41FC472D1007 (void);
// 0x00000164 System.Void ModelInstance::SetRotationSpeedServerRpc(System.Single)
extern void ModelInstance_SetRotationSpeedServerRpc_mCD8676431829B6322AC19C32BF1F8999D1287E54 (void);
// 0x00000165 System.Single ModelInstance::get_RotationSpeed()
extern void ModelInstance_get_RotationSpeed_m5A5ECCDD1A6CD8677FEA5F71FB0AE08E0EA0FDA5 (void);
// 0x00000166 System.Void ModelInstance::set_RotationSpeed(System.Single)
extern void ModelInstance_set_RotationSpeed_m6154C8033F0AB013B5A482E21494C6C56384C687 (void);
// 0x00000167 System.Void ModelInstance::SetAutorotateModelServerRpc(System.Boolean)
extern void ModelInstance_SetAutorotateModelServerRpc_m4BABD0FCF9A34534E5F0C663E20DD91A19D97162 (void);
// 0x00000168 System.Boolean ModelInstance::get_AutorotateModel()
extern void ModelInstance_get_AutorotateModel_m92276D7FD2C575E4D9D219294FF1D6A39050163D (void);
// 0x00000169 System.Void ModelInstance::set_AutorotateModel(System.Boolean)
extern void ModelInstance_set_AutorotateModel_mFF4B207A3785B401386B5A4C0483AA74F882D2CF (void);
// 0x0000016A System.Void ModelInstance::FocusOnNode(System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void ModelInstance_FocusOnNode_m4C413F7EC76FF2EC3FE0219237E0D331AF9B374E (void);
// 0x0000016B System.Void ModelInstance::FocusOnNodeServerRpc(System.Int32,System.Boolean,System.Boolean,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ModelInstance_FocusOnNodeServerRpc_m5DFA0A15D50160A6E3D032D1753A06A04B3C033E (void);
// 0x0000016C System.Void ModelInstance::.ctor()
extern void ModelInstance__ctor_mD27274FE9E11CBA6CB39E54D1944B1C09418ED0F (void);
// 0x0000016D System.Void ModelInstance::__initializeVariables()
extern void ModelInstance___initializeVariables_m2203DC6318BFEEBD4773360E2977D528925E33C1 (void);
// 0x0000016E System.Void ModelInstance::__initializeRpcs()
extern void ModelInstance___initializeRpcs_m4598FA28E9B98CD1EBED4A7A9F9D2C6B5536DA74 (void);
// 0x0000016F System.Void ModelInstance::__rpc_handler_2472231065(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_2472231065_mDC0AA69CECAE236D60814AA0FF43333166931542 (void);
// 0x00000170 System.Void ModelInstance::__rpc_handler_771431870(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_771431870_mC435CF2108CCA3866C13E87AE482844AA985F393 (void);
// 0x00000171 System.Void ModelInstance::__rpc_handler_37011299(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_37011299_mA33E4A521AF7C5E427291227C5CDADE52A4011BE (void);
// 0x00000172 System.Void ModelInstance::__rpc_handler_1441208538(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_1441208538_mFA24AAD7F75B117B41A6CBC19F54A33EE1302B8F (void);
// 0x00000173 System.Void ModelInstance::__rpc_handler_1369999954(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_1369999954_m2B4248924E5EFB6DC0671ED947AB128FB4D2DA9C (void);
// 0x00000174 System.Void ModelInstance::__rpc_handler_405319951(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_405319951_m2722EA3700B75485FBB8DC2FE3B0FBB6DB0127D8 (void);
// 0x00000175 System.Void ModelInstance::__rpc_handler_3458780303(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_3458780303_mB47755F6A46D11F9A0C5B2887DFE4A91D2CFA7BA (void);
// 0x00000176 System.Void ModelInstance::__rpc_handler_2935864450(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_2935864450_mB66811DC358CA5CB7731EB1AC0AEAE41D9948E30 (void);
// 0x00000177 System.Void ModelInstance::__rpc_handler_2644131848(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_2644131848_m2D57A90ACF6241702BE796EF058E6D93E69E2E56 (void);
// 0x00000178 System.Void ModelInstance::__rpc_handler_2945997562(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_2945997562_m2337A29239A454D5915E84FE35E5A8E4FA4A9572 (void);
// 0x00000179 System.Void ModelInstance::__rpc_handler_1202017353(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_1202017353_m47F55DD58A67188A183DD1381018F678B80671AA (void);
// 0x0000017A System.Void ModelInstance::__rpc_handler_1229847090(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_1229847090_m74B4121E852B47EF3B9463D2856B05E5403924D1 (void);
// 0x0000017B System.Void ModelInstance::__rpc_handler_2598283162(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstance___rpc_handler_2598283162_m0280759200DBE30570B439E2B3551DE34623210E (void);
// 0x0000017C System.String ModelInstance::__getTypeName()
extern void ModelInstance___getTypeName_m6F93327D675E67A469DEA79361A010DF05AA12CE (void);
// 0x0000017D System.Void ModelInstance/<SetModelServerAsync>d__28::MoveNext()
extern void U3CSetModelServerAsyncU3Ed__28_MoveNext_m6DF37858A28B30105DACAF6ECCBA557114CAE733 (void);
// 0x0000017E System.Void ModelInstance/<SetModelServerAsync>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSetModelServerAsyncU3Ed__28_SetStateMachine_m8D4E88707BDB0867A619D67317DF84000D230704 (void);
// 0x0000017F System.Void ModelInstance/<SetModelClientAsync>d__30::MoveNext()
extern void U3CSetModelClientAsyncU3Ed__30_MoveNext_m3970AA534DC9529520D7A7828A315D58AB120556 (void);
// 0x00000180 System.Void ModelInstance/<SetModelClientAsync>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSetModelClientAsyncU3Ed__30_SetStateMachine_m08B8D8353C6879666DFC6EB7A1D66D32759517A7 (void);
// 0x00000181 System.Int32 ModelNode::get_Idx()
extern void ModelNode_get_Idx_m470153D04ED1915B748F4E07A5DB8D592BF926F9 (void);
// 0x00000182 System.Void ModelNode::set_Idx(System.Int32)
extern void ModelNode_set_Idx_m98E0488FCC348ABBB0828E411C66EFAAF612C3E0 (void);
// 0x00000183 System.Collections.Generic.List`1<ModelNode> ModelNode::get_Children()
extern void ModelNode_get_Children_mFF4D2C5A42BBD89B1E3FFA5312344365395DF3EB (void);
// 0x00000184 ModelNode ModelNode::get_Parent()
extern void ModelNode_get_Parent_m6375F585D6E3A650A448BCF4E40167A9D1E04643 (void);
// 0x00000185 ModelNode ModelNode::ParentOnLevel(System.Int32)
extern void ModelNode_ParentOnLevel_mE0AC7A43B1C162A5B9ED126ADA1F895F319CF73D (void);
// 0x00000186 System.Int32 ModelNode::get_Level()
extern void ModelNode_get_Level_mB73F870EBAD9B9F50292216385C4938DAE2320FF (void);
// 0x00000187 System.Boolean ModelNode::get_IsRoot()
extern void ModelNode_get_IsRoot_mB45E23692275319F6DC534B44809DAE3BFE9CE4C (void);
// 0x00000188 System.Boolean ModelNode::get_IsLeaf()
extern void ModelNode_get_IsLeaf_mD1B1F9292FF2E62ECE9BAEF2436E67F2AD625EFC (void);
// 0x00000189 System.Void ModelNode::AddChild(ModelNode)
extern void ModelNode_AddChild_m0F0BED29D463C6A59373ACEDC619319273AF102B (void);
// 0x0000018A System.Boolean ModelNode::RemoveChild(ModelNode)
extern void ModelNode_RemoveChild_mCF0ADAFE70DA9934DF97E19CCC1A04B507DCC5D0 (void);
// 0x0000018B System.Boolean ModelNode::IsChildOf(ModelNode)
extern void ModelNode_IsChildOf_m5D1D3683A4B828EFF16CD31E779373F196CB0997 (void);
// 0x0000018C System.Boolean ModelNode::IsParentOf(ModelNode)
extern void ModelNode_IsParentOf_m03FEABC9964416E3CAB3A5E2502503F7FCAC64B6 (void);
// 0x0000018D System.Collections.Generic.IEnumerable`1<ModelNode> ModelNode::get_Subnodes()
extern void ModelNode_get_Subnodes_mBC9105B2B42E92CAD2897C2D1D3068821E9FB278 (void);
// 0x0000018E System.Collections.Generic.IEnumerable`1<ModelNode> ModelNode::get_SubnodesBottomup()
extern void ModelNode_get_SubnodesBottomup_m9A1F0DA324C0150DC3FCE1664740EB6D73DEA391 (void);
// 0x0000018F System.Int32 ModelNode::get_NumSubnodes()
extern void ModelNode_get_NumSubnodes_m50A45F64DDEF377B7FA4AF07A3BAB99E2E8D2A27 (void);
// 0x00000190 ModelNode ModelNode::ClosestCommonParent(ModelNode)
extern void ModelNode_ClosestCommonParent_m09E4C1428A6AF6922246CB769EBDE383CC570DC4 (void);
// 0x00000191 NodeState ModelNode::get_State()
extern void ModelNode_get_State_mC1C9413A48FDA86ECCBC8A2AF5A103843D7FD39B (void);
// 0x00000192 NodeState ModelNode::get_DefaultState()
extern void ModelNode_get_DefaultState_m827080CE676044C40AB01C282690EB0516EDE5A6 (void);
// 0x00000193 System.Void ModelNode::SetNodeState(NodeState,NodeStateTransition)
extern void ModelNode_SetNodeState_mD4DC82E1A58ECE727FFD5F1228B3A34E550430E2 (void);
// 0x00000194 System.Void ModelNode::Awake()
extern void ModelNode_Awake_m622DEB4141D514F84A183A95FDBC82D5676433AD (void);
// 0x00000195 System.Void ModelNode::SetInitialSpecs()
extern void ModelNode_SetInitialSpecs_mD8BCFFCE4990E6E5F71E901C828A9388D70B2A39 (void);
// 0x00000196 System.Void ModelNode::OnNetworkSpawn()
extern void ModelNode_OnNetworkSpawn_m1A8B2804FE5E69ECD2C62711EA4B331E1857B9D0 (void);
// 0x00000197 UnityEngine.Renderer[] ModelNode::get_Renderers()
extern void ModelNode_get_Renderers_m519AA84AE424E7ED6688A286ED59893440B501E2 (void);
// 0x00000198 UnityEngine.Bounds ModelNode::TransformBounds(UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern void ModelNode_TransformBounds_mBCDAB363B70345EA7E0CD68CCF8C05CCA1AB8738 (void);
// 0x00000199 UnityEngine.Vector3 ModelNode::GetCornerPoint(System.Int32)
extern void ModelNode_GetCornerPoint_m5E498DC619DCA7157E7C6C7D0ADD55C1343BC23C (void);
// 0x0000019A UnityEngine.Bounds ModelNode::get_bounds()
extern void ModelNode_get_bounds_m5A3DDA8062EF12793DFA99B2052C312FD78259C9 (void);
// 0x0000019B UnityEngine.Bounds ModelNode::get_localBounds()
extern void ModelNode_get_localBounds_m4A603833BD10571A5C391AE1568D49BAF6BB87D1 (void);
// 0x0000019C UnityEngine.Bounds ModelNode::get_InitialLocalBounds()
extern void ModelNode_get_InitialLocalBounds_m5CF1929A367704C09AA1AF8F2552617649248F82 (void);
// 0x0000019D LocalTransform ModelNode::get_localTransform()
extern void ModelNode_get_localTransform_m9ED61F496983D6ABB10230D19DE5CAE25D4111A1 (void);
// 0x0000019E System.Void ModelNode::SetLocalTransformServerRpc(LocalTransform,System.Single,System.Boolean,System.Boolean,System.Boolean)
extern void ModelNode_SetLocalTransformServerRpc_m8280C54BE756A66308788467FC3723A41303669A (void);
// 0x0000019F ModelNodeScriptableObject ModelNode::get_NodeInfo()
extern void ModelNode_get_NodeInfo_m70D1078BAEE6D47DD22744DC810CEBE23B92A3C9 (void);
// 0x000001A0 System.Void ModelNode::set_NodeInfo(ModelNodeScriptableObject)
extern void ModelNode_set_NodeInfo_m81DE0A18978CE1B8BB4F46BEE9258FBF59FD1618 (void);
// 0x000001A1 Tooltip ModelNode::get_Tooltip()
extern void ModelNode_get_Tooltip_mC031631900DD81F1A11827CD6846D406E99920F9 (void);
// 0x000001A2 System.Void ModelNode::set_Tooltip(Tooltip)
extern void ModelNode_set_Tooltip_m3EB042924ACF8E5D15611E9E9D2FCB0E207A5B47 (void);
// 0x000001A3 System.Boolean ModelNode::get_TooltipShowing()
extern void ModelNode_get_TooltipShowing_m35DEBE42E033A621D9B644930AAD2DA9424B9CBA (void);
// 0x000001A4 System.Void ModelNode::set_TooltipShowing(System.Boolean)
extern void ModelNode_set_TooltipShowing_m273E29DDF760D5CBC8A42CEFA9063B682A75DF11 (void);
// 0x000001A5 System.Void ModelNode::SetTooltipShowingServerRpc(System.Boolean)
extern void ModelNode_SetTooltipShowingServerRpc_m7DB0F90A9FB4E5F6E1353BB3E300D44C7F28D66B (void);
// 0x000001A6 System.Void ModelNode::OnTooltipShowingValueChanged(System.Boolean,System.Boolean)
extern void ModelNode_OnTooltipShowingValueChanged_m6554A55ECFE4E18D6C9A4C73F9E1F9E0D1F88160 (void);
// 0x000001A7 System.Void ModelNode::set_OutlineColorIdx(System.Int32)
extern void ModelNode_set_OutlineColorIdx_m461F6500C1C851873DC52C3B5EB9D700D8D12B6A (void);
// 0x000001A8 System.Boolean ModelNode::get_IsOutlined()
extern void ModelNode_get_IsOutlined_m37903FAF397A681452FE5B1A8CD0000D46225E22 (void);
// 0x000001A9 System.Void ModelNode::set_IsOutlined(System.Boolean)
extern void ModelNode_set_IsOutlined_m680F05F4D5B9DCAA885EE11A2A646E42499B3DB6 (void);
// 0x000001AA System.Boolean ModelNode::get_IsHidden()
extern void ModelNode_get_IsHidden_m263943BFBB527ADF3E980547FB808EF0C1A98B0B (void);
// 0x000001AB System.Void ModelNode::set_IsHidden(System.Boolean)
extern void ModelNode_set_IsHidden_m3CDA03271AD0076F4E2AB902229C92400147FF28 (void);
// 0x000001AC System.Boolean ModelNode::get_IsFaded()
extern void ModelNode_get_IsFaded_mA36C7603644B6C5A05EFC15C527ED9B28AA7B876 (void);
// 0x000001AD System.Void ModelNode::set_IsFaded(System.Boolean)
extern void ModelNode_set_IsFaded_m83B24E8F5BA91598F8E3A6F3D2DC82180AC18F83 (void);
// 0x000001AE System.Void ModelNode::ToggleHidden()
extern void ModelNode_ToggleHidden_m10CAC96C7A3B74D8B5D94DF45CAD591819CDB16B (void);
// 0x000001AF System.Void ModelNode::ToggleFaded()
extern void ModelNode_ToggleFaded_m75A152D903070B860946DE320C3FDB46E6E766E5 (void);
// 0x000001B0 System.Void ModelNode::ToggleOutline()
extern void ModelNode_ToggleOutline_mB8C04193FFB57477652C11796CFEABDE26BEECFA (void);
// 0x000001B1 NodeVisibilityState ModelNode::get_VisibilityState()
extern void ModelNode_get_VisibilityState_m7D27E7C01612736BB7A687278A65C8FBC27F371D (void);
// 0x000001B2 System.Void ModelNode::.ctor()
extern void ModelNode__ctor_m9CE5A58CB12C479FABEB3869EE0119601A273099 (void);
// 0x000001B3 System.Void ModelNode::__initializeVariables()
extern void ModelNode___initializeVariables_mA1E6C11F0A6784CF6A12D58DFA299C89FA253FCB (void);
// 0x000001B4 System.Void ModelNode::__initializeRpcs()
extern void ModelNode___initializeRpcs_mCE73A7F729592933EE91B3D52F8A955D1B21AD26 (void);
// 0x000001B5 System.Void ModelNode::__rpc_handler_3333735372(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelNode___rpc_handler_3333735372_m7C6FDE13053755624A6AAA07713277516B77D270 (void);
// 0x000001B6 System.Void ModelNode::__rpc_handler_997827599(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelNode___rpc_handler_997827599_m7820F524D7175FC14650EE565FEBFFE22F609EDD (void);
// 0x000001B7 System.String ModelNode::__getTypeName()
extern void ModelNode___getTypeName_mD5CA22908D097ECCABA1C8E76FF0D20B18BC4BD8 (void);
// 0x000001B8 System.Void ModelNode/<get_Subnodes>d__24::.ctor(System.Int32)
extern void U3Cget_SubnodesU3Ed__24__ctor_mA63749C9CF5E4B56717C07E77FC8FD31BFA84571 (void);
// 0x000001B9 System.Void ModelNode/<get_Subnodes>d__24::System.IDisposable.Dispose()
extern void U3Cget_SubnodesU3Ed__24_System_IDisposable_Dispose_m6DC1096CEDF337C5783590C8B29874822986ADEF (void);
// 0x000001BA System.Boolean ModelNode/<get_Subnodes>d__24::MoveNext()
extern void U3Cget_SubnodesU3Ed__24_MoveNext_mF36A99FB12DDA56EE69F4D277948AA0DAE938E47 (void);
// 0x000001BB System.Void ModelNode/<get_Subnodes>d__24::<>m__Finally1()
extern void U3Cget_SubnodesU3Ed__24_U3CU3Em__Finally1_m6722EE0D419CF9210C62C1ADC663691064AFD7A1 (void);
// 0x000001BC System.Void ModelNode/<get_Subnodes>d__24::<>m__Finally2()
extern void U3Cget_SubnodesU3Ed__24_U3CU3Em__Finally2_mDC7C3469A97794A3C512A70ABBA4F5BF8021E14F (void);
// 0x000001BD ModelNode ModelNode/<get_Subnodes>d__24::System.Collections.Generic.IEnumerator<ModelNode>.get_Current()
extern void U3Cget_SubnodesU3Ed__24_System_Collections_Generic_IEnumeratorU3CModelNodeU3E_get_Current_mCB96B0EBBBB59A75CFA4D500BDFC5D68D6D728F5 (void);
// 0x000001BE System.Void ModelNode/<get_Subnodes>d__24::System.Collections.IEnumerator.Reset()
extern void U3Cget_SubnodesU3Ed__24_System_Collections_IEnumerator_Reset_m483BD4D760A5C900CB26B7AF905B77CFE5B26833 (void);
// 0x000001BF System.Object ModelNode/<get_Subnodes>d__24::System.Collections.IEnumerator.get_Current()
extern void U3Cget_SubnodesU3Ed__24_System_Collections_IEnumerator_get_Current_mE1997EA3980F0C305F745ADAE6DC5B1281802546 (void);
// 0x000001C0 System.Collections.Generic.IEnumerator`1<ModelNode> ModelNode/<get_Subnodes>d__24::System.Collections.Generic.IEnumerable<ModelNode>.GetEnumerator()
extern void U3Cget_SubnodesU3Ed__24_System_Collections_Generic_IEnumerableU3CModelNodeU3E_GetEnumerator_m3FCC2E3D2A7CC0B153F56E19AF20F915E79DE29F (void);
// 0x000001C1 System.Collections.IEnumerator ModelNode/<get_Subnodes>d__24::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_SubnodesU3Ed__24_System_Collections_IEnumerable_GetEnumerator_m253817175AA1C700F952FDCEED6F686288A404E6 (void);
// 0x000001C2 System.Void ModelNode/<get_SubnodesBottomup>d__26::.ctor(System.Int32)
extern void U3Cget_SubnodesBottomupU3Ed__26__ctor_m727C6A2A5CF2B5AD3D9B4834B31D8DDD7F3B6E49 (void);
// 0x000001C3 System.Void ModelNode/<get_SubnodesBottomup>d__26::System.IDisposable.Dispose()
extern void U3Cget_SubnodesBottomupU3Ed__26_System_IDisposable_Dispose_mDF8BF8C2FB65EC5830A93673BDD8E8C190055C53 (void);
// 0x000001C4 System.Boolean ModelNode/<get_SubnodesBottomup>d__26::MoveNext()
extern void U3Cget_SubnodesBottomupU3Ed__26_MoveNext_m785C4B5947E18C88FEFFBF97F5EB8D1A1C497A08 (void);
// 0x000001C5 System.Void ModelNode/<get_SubnodesBottomup>d__26::<>m__Finally1()
extern void U3Cget_SubnodesBottomupU3Ed__26_U3CU3Em__Finally1_m3BDEDBACA984CB3B9E6D60351AD5FB17417EADD9 (void);
// 0x000001C6 System.Void ModelNode/<get_SubnodesBottomup>d__26::<>m__Finally2()
extern void U3Cget_SubnodesBottomupU3Ed__26_U3CU3Em__Finally2_mBBE08B0D50749631112EDDE3E99CC0DF5E5E3852 (void);
// 0x000001C7 ModelNode ModelNode/<get_SubnodesBottomup>d__26::System.Collections.Generic.IEnumerator<ModelNode>.get_Current()
extern void U3Cget_SubnodesBottomupU3Ed__26_System_Collections_Generic_IEnumeratorU3CModelNodeU3E_get_Current_m3818FE367D5CB97FA29C12AE08A2866F5F52C495 (void);
// 0x000001C8 System.Void ModelNode/<get_SubnodesBottomup>d__26::System.Collections.IEnumerator.Reset()
extern void U3Cget_SubnodesBottomupU3Ed__26_System_Collections_IEnumerator_Reset_m18458D953AA2CF944ACA16E46B5F1106C55972FA (void);
// 0x000001C9 System.Object ModelNode/<get_SubnodesBottomup>d__26::System.Collections.IEnumerator.get_Current()
extern void U3Cget_SubnodesBottomupU3Ed__26_System_Collections_IEnumerator_get_Current_m538B9916ED559986519EB58251C4F3909BD8F1BF (void);
// 0x000001CA System.Collections.Generic.IEnumerator`1<ModelNode> ModelNode/<get_SubnodesBottomup>d__26::System.Collections.Generic.IEnumerable<ModelNode>.GetEnumerator()
extern void U3Cget_SubnodesBottomupU3Ed__26_System_Collections_Generic_IEnumerableU3CModelNodeU3E_GetEnumerator_m3FFEEA6D968A4EEB4AB14B86F228490B14556CE3 (void);
// 0x000001CB System.Collections.IEnumerator ModelNode/<get_SubnodesBottomup>d__26::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_SubnodesBottomupU3Ed__26_System_Collections_IEnumerable_GetEnumerator_mB43F370B4F868C9F430D4DDF4CA94C7EF9DE1492 (void);
// 0x000001CC System.Void NodeController::add_NodeSelected(System.Action`1<System.Int32>)
extern void NodeController_add_NodeSelected_mA08E3710533051D10915CA576D3AE131918D17F1 (void);
// 0x000001CD System.Void NodeController::remove_NodeSelected(System.Action`1<System.Int32>)
extern void NodeController_remove_NodeSelected_m1E90F92DFF04ED7ED5540F837E76BAD16475883B (void);
// 0x000001CE System.Void NodeController::add_NodeDeselected(System.Action`1<System.Int32>)
extern void NodeController_add_NodeDeselected_m500A44F24EDA7A98D6C0E0ABEA6AFF02078E4BC5 (void);
// 0x000001CF System.Void NodeController::remove_NodeDeselected(System.Action`1<System.Int32>)
extern void NodeController_remove_NodeDeselected_m5A817FCDA7F320F39C613839324D07330D7ED96E (void);
// 0x000001D0 System.Void NodeController::add_NodeHovered(System.Action`1<System.Int32>)
extern void NodeController_add_NodeHovered_m85D7AC8CE8DF8071A0E8046F96A01FE5040B791D (void);
// 0x000001D1 System.Void NodeController::remove_NodeHovered(System.Action`1<System.Int32>)
extern void NodeController_remove_NodeHovered_m9278E79CD40A99125AD3905C7A1B957A858C5C96 (void);
// 0x000001D2 System.Void NodeController::add_NodeUnhovered(System.Action`1<System.Int32>)
extern void NodeController_add_NodeUnhovered_m73BCA4DC35EAB72ADD877A300239FF214D87189C (void);
// 0x000001D3 System.Void NodeController::remove_NodeUnhovered(System.Action`1<System.Int32>)
extern void NodeController_remove_NodeUnhovered_mA83BD650DA4303B0AA9BFD6E647619374ACEBF0F (void);
// 0x000001D4 System.Void NodeController::add_NodeAdded(System.Action`1<ModelNode>)
extern void NodeController_add_NodeAdded_m241980B76E127FCD34D04DDBA7566AC5C57B9088 (void);
// 0x000001D5 System.Void NodeController::remove_NodeAdded(System.Action`1<ModelNode>)
extern void NodeController_remove_NodeAdded_m78F38218C1B64DD67154E6738BE6D217BE8E01E2 (void);
// 0x000001D6 System.Void NodeController::add_NodeRemoved(System.Action`1<ModelNode>)
extern void NodeController_add_NodeRemoved_m4EFD8D34C2BD49CDCD03D1FCA53D4989F14FF922 (void);
// 0x000001D7 System.Void NodeController::remove_NodeRemoved(System.Action`1<ModelNode>)
extern void NodeController_remove_NodeRemoved_m74D8172EFF48E13CFB8ECF57331815F12B3DB8BF (void);
// 0x000001D8 System.Void NodeController::add_BoundsSizeChanged(System.Action`1<UnityEngine.Vector3>)
extern void NodeController_add_BoundsSizeChanged_m995851F32772B7BA388CD2403AE084BE7DD68F42 (void);
// 0x000001D9 System.Void NodeController::remove_BoundsSizeChanged(System.Action`1<UnityEngine.Vector3>)
extern void NodeController_remove_BoundsSizeChanged_m73225EC52DB6BA434A9802565F18C51908D7E44B (void);
// 0x000001DA System.Void NodeController::Awake()
extern void NodeController_Awake_mC45B0157156A7AE2B2CC7A5C86DC7EF8BDBD4E33 (void);
// 0x000001DB System.Void NodeController::OnNetworkSpawn()
extern void NodeController_OnNetworkSpawn_mF65AE39C799BF8973B7DD41EC9EEC3CFC8F3C253 (void);
// 0x000001DC ModelNode NodeController::get_RootNode()
extern void NodeController_get_RootNode_mB805CBA9B34507469D63D818104AF76A85E42E10 (void);
// 0x000001DD System.Void NodeController::set_RootNode(ModelNode)
extern void NodeController_set_RootNode_mD6593246594D07711554495307B21EE521F00C5B (void);
// 0x000001DE System.Collections.Generic.IEnumerable`1<ModelNode> NodeController::get_RootNodes()
extern void NodeController_get_RootNodes_m5B08E7CABD23D8C54E9058ED739018D614CF7FF3 (void);
// 0x000001DF ModelNode NodeController::Node(System.Int32)
extern void NodeController_Node_m3E1EDFA9EB2FD2583B747D33ABD0DE07D06A2BED (void);
// 0x000001E0 System.Collections.Generic.IEnumerable`1<ModelNode> NodeController::get_Nodes()
extern void NodeController_get_Nodes_mDADBD21674E262201993C37E1EBFF9CF69D7BDFE (void);
// 0x000001E1 System.Collections.Generic.IEnumerable`1<ModelNode> NodeController::get_NodesBottomup()
extern void NodeController_get_NodesBottomup_mCA5BFA748B56C012E9552B005B4B9857B145BD0A (void);
// 0x000001E2 System.Collections.Generic.IEnumerable`1<LayerInstance> NodeController::get_Layers()
extern void NodeController_get_Layers_mC1631C607B7D0E0A8216A455EE944FF3FAF2160E (void);
// 0x000001E3 System.Int32 NodeController::get_NumNodes()
extern void NodeController_get_NumNodes_m7FC53FA04309411EB987F946F0D7F9C2EE79437F (void);
// 0x000001E4 System.Int32 NodeController::get_SelectedNodeIdx()
extern void NodeController_get_SelectedNodeIdx_m4B46B501CA8E204D9A36C2A2E59C428991706F3E (void);
// 0x000001E5 System.Void NodeController::set_SelectedNodeIdx(System.Int32)
extern void NodeController_set_SelectedNodeIdx_m0E2E7E6138D35E0E81B44353930A745198FDF33A (void);
// 0x000001E6 System.Void NodeController::SetSelectedNodeIdxServerRpc(System.Int32)
extern void NodeController_SetSelectedNodeIdxServerRpc_m8C6E19D314318CAE1D68249A76DB8105130E71DB (void);
// 0x000001E7 System.Void NodeController::OnSelectedNodeIdxValueChanged(System.Int32,System.Int32)
extern void NodeController_OnSelectedNodeIdxValueChanged_m4545349C537E01EA643A24D7E73B71676D1B318B (void);
// 0x000001E8 ModelNode NodeController::get_SelectedNode()
extern void NodeController_get_SelectedNode_mB2B0F14A51FC2D04BDE3EDDF80F5494630416808 (void);
// 0x000001E9 System.Int32 NodeController::get_HoveredNodeIdx()
extern void NodeController_get_HoveredNodeIdx_mF7EDAC68588BD8B07AF64388429A8CCA084D33A5 (void);
// 0x000001EA System.Void NodeController::set_HoveredNodeIdx(System.Int32)
extern void NodeController_set_HoveredNodeIdx_m7BA16E87AC59E446DE4CB871A3719D141A9D0F87 (void);
// 0x000001EB System.Void NodeController::SetHoveredNodeIdxServerRpc(System.Int32)
extern void NodeController_SetHoveredNodeIdxServerRpc_mC5ACF746B5D29DAC5C3BF66129DECA5ECCAD670B (void);
// 0x000001EC System.Void NodeController::OnHoveredNodeIdxValueChanged(System.Int32,System.Int32)
extern void NodeController_OnHoveredNodeIdxValueChanged_mBA5EDA7B95E744EA40762269005D522686E499CF (void);
// 0x000001ED ModelNode NodeController::get_HoveredNode()
extern void NodeController_get_HoveredNode_m3C1FE9FDEDB5428D96EDDAF20DCC1A5997DEEA69 (void);
// 0x000001EE System.Boolean NodeController::get_GroupedSelection()
extern void NodeController_get_GroupedSelection_m3292DEC97B73AF4E92FFDF9255473FCFC70A811B (void);
// 0x000001EF System.Void NodeController::set_GroupedSelection(System.Boolean)
extern void NodeController_set_GroupedSelection_m634B94BDDAB406143EC43962878C577059377CCC (void);
// 0x000001F0 System.Void NodeController::SetGroupedSelectionServerRpc(System.Boolean)
extern void NodeController_SetGroupedSelectionServerRpc_m97B2456286A23A7084C89742E8AC3847E05019D6 (void);
// 0x000001F1 System.Void NodeController::OnLayerHoverEnter(ModelNode)
extern void NodeController_OnLayerHoverEnter_mD7E03939F495899957FCB49B7671156CAA464C34 (void);
// 0x000001F2 System.Void NodeController::OnLayerHoverExit()
extern void NodeController_OnLayerHoverExit_m1B107DD8BB9DCE01730A90F7168E618404F47623 (void);
// 0x000001F3 System.Void NodeController::OnLayerSelected(ModelNode)
extern void NodeController_OnLayerSelected_m76720BF754BE87418464E39779FFB9DA0FFCA1DA (void);
// 0x000001F4 System.Void NodeController::RemoveNode(System.Int32)
extern void NodeController_RemoveNode_m7E3A48134C24E33B20AC492CCA82ACD0A0180D3E (void);
// 0x000001F5 System.Void NodeController::SetNodeStates(System.Collections.Generic.List`1<NodeState>,System.Collections.Generic.List`1<NodeStateTransition>)
extern void NodeController_SetNodeStates_m3D100F6966860DC9E3E1C2D426CC95A0315A198A (void);
// 0x000001F6 System.Collections.Generic.List`1<NodeState> NodeController::get_NodeStates()
extern void NodeController_get_NodeStates_m5BDD13DB9A972D00EF67282C28C5701249B7B7BD (void);
// 0x000001F7 System.Collections.Generic.List`1<NodeState> NodeController::get_DefaultNodeStates()
extern void NodeController_get_DefaultNodeStates_m8C585818030BDE0752D08482B200E08E3EA20517 (void);
// 0x000001F8 System.Void NodeController::.ctor()
extern void NodeController__ctor_mE0C6C1BA739ECFC7EBCA77EF1DCA73C947408019 (void);
// 0x000001F9 System.Void NodeController::<set_RootNode>b__30_1(System.Single)
extern void NodeController_U3Cset_RootNodeU3Eb__30_1_m1C617741785B4D11B7990A1F1A2408F47618AD54 (void);
// 0x000001FA System.Void NodeController::__initializeVariables()
extern void NodeController___initializeVariables_mC9AB7DFABB75478ACEF4CBC3C6BE3EF6995BA10E (void);
// 0x000001FB System.Void NodeController::__initializeRpcs()
extern void NodeController___initializeRpcs_mEE95381CF92043DF17795DE532DDCFAF5B0A6FE0 (void);
// 0x000001FC System.Void NodeController::__rpc_handler_2277630493(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void NodeController___rpc_handler_2277630493_m0B268225D5208B13A409F40EC71BDE268E3C7B7B (void);
// 0x000001FD System.Void NodeController::__rpc_handler_1356188875(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void NodeController___rpc_handler_1356188875_m61A5ED577DB765399839C4E6D003D1772B46437F (void);
// 0x000001FE System.Void NodeController::__rpc_handler_3206384415(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void NodeController___rpc_handler_3206384415_mFA33823112BC02759E40A70E60245C3A9C637267 (void);
// 0x000001FF System.String NodeController::__getTypeName()
extern void NodeController___getTypeName_mAF170B46C0C63434518F9D29B3AF13C88D106C35 (void);
// 0x00000200 System.Void NodeController/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mCF91A0A339674BBD148825210685E65D2D2A1C05 (void);
// 0x00000201 System.Void NodeController/<>c__DisplayClass30_0::<set_RootNode>b__0(System.Single)
extern void U3CU3Ec__DisplayClass30_0_U3Cset_RootNodeU3Eb__0_m6F09DA9A01504A2CCEC07E7876B0F66AF103BD40 (void);
// 0x00000202 System.Void NodeController/<>c__DisplayClass30_0::<set_RootNode>b__2(System.Single)
extern void U3CU3Ec__DisplayClass30_0_U3Cset_RootNodeU3Eb__2_mC8FA07C9BA74F6AD8878B0F6A6119DFCF30C485B (void);
// 0x00000203 System.Void NodeController/<>c::.cctor()
extern void U3CU3Ec__cctor_m990D5477F1FA651CBCBA6DCFE9A6936573108B79 (void);
// 0x00000204 System.Void NodeController/<>c::.ctor()
extern void U3CU3Ec__ctor_m4ABE993B1910C2C8CBF0F8C385EFE68FAE723666 (void);
// 0x00000205 System.Boolean NodeController/<>c::<get_RootNodes>b__32_0(ModelNode)
extern void U3CU3Ec_U3Cget_RootNodesU3Eb__32_0_mAD2FA24D3BB2027F3DAE99FFB2EA7734B9C11E68 (void);
// 0x00000206 System.Boolean NodeController/<>c::<get_Layers>b__39_0(ModelNode)
extern void U3CU3Ec_U3Cget_LayersU3Eb__39_0_mB4D275C66A58E03257F1056827804B5450C7FCE6 (void);
// 0x00000207 System.Void NodePositionController::add_BoundsSizeChanged(System.Action`1<UnityEngine.Vector3>)
extern void NodePositionController_add_BoundsSizeChanged_m03C17170D4B1455B45C91E07A167EB0761FFEB0D (void);
// 0x00000208 System.Void NodePositionController::remove_BoundsSizeChanged(System.Action`1<UnityEngine.Vector3>)
extern void NodePositionController_remove_BoundsSizeChanged_m8E83F65C49A207C4F9BF6342E97680E0D7320EEE (void);
// 0x00000209 System.Void NodePositionController::FitInBounds()
extern void NodePositionController_FitInBounds_m0D7C349AF1F762E035A81515290A3891C2602C70 (void);
// 0x0000020A System.Void NodePositionController::SetDefaultTransforms()
extern void NodePositionController_SetDefaultTransforms_m7E2E5A2FA4A00C4992C33F82B3D4A807388AE0EC (void);
// 0x0000020B System.Void NodePositionController::SetExplodeTransforms()
extern void NodePositionController_SetExplodeTransforms_mAB76875CAECF2F628601101C34CF4843EA634B78 (void);
// 0x0000020C UnityEngine.Bounds NodePositionController::TransformBounds(UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern void NodePositionController_TransformBounds_mDCB5E4EA1D4F2B1661DCD72241E70AE58C281E32 (void);
// 0x0000020D UnityEngine.Vector3 NodePositionController::GetCornerPoint(System.Int32)
extern void NodePositionController_GetCornerPoint_mCF2B22C53D94C2FC2768EF193C8BE55D92D8BB4B (void);
// 0x0000020E System.Void NodePositionController::.ctor()
extern void NodePositionController__ctor_m4697DAAD0F0493AB677E4C7351D881FB8C9F8F0F (void);
// 0x0000020F System.Void NodeVisibilityStateController::add_NodeVisibilityChanged(System.Action)
extern void NodeVisibilityStateController_add_NodeVisibilityChanged_mA074BF5C6CBE7629FBD6EBE5E8E137BB26FF1EAA (void);
// 0x00000210 System.Void NodeVisibilityStateController::remove_NodeVisibilityChanged(System.Action)
extern void NodeVisibilityStateController_remove_NodeVisibilityChanged_mCDD3A8A6B0EA986F6E855D7305F4EBF9A808817A (void);
// 0x00000211 System.Boolean NodeVisibilityStateController::get_ShowTooltipSelected()
extern void NodeVisibilityStateController_get_ShowTooltipSelected_mAE456EF0C858114E11409B543FA1B4C844E42062 (void);
// 0x00000212 System.Void NodeVisibilityStateController::set_ShowTooltipSelected(System.Boolean)
extern void NodeVisibilityStateController_set_ShowTooltipSelected_m138ED3BE3DE60E9E8CE55F3E94AE01D700E22264 (void);
// 0x00000213 System.Boolean NodeVisibilityStateController::get_ShowTooltipHovered()
extern void NodeVisibilityStateController_get_ShowTooltipHovered_mD927A2C292699F487C2EC7073370962D04DA09AE (void);
// 0x00000214 System.Void NodeVisibilityStateController::set_ShowTooltipHovered(System.Boolean)
extern void NodeVisibilityStateController_set_ShowTooltipHovered_m83C663CF46F0CACF191FDE6C49E45C4383ACA5CB (void);
// 0x00000215 System.Boolean NodeVisibilityStateController::get_OutlineSelected()
extern void NodeVisibilityStateController_get_OutlineSelected_m11CB38F4D357E29FADFE31BB5190DFF0E06B30D1 (void);
// 0x00000216 System.Void NodeVisibilityStateController::set_OutlineSelected(System.Boolean)
extern void NodeVisibilityStateController_set_OutlineSelected_mF4EDF4DC583E636BECFC743C427FA10898EDCEDA (void);
// 0x00000217 System.Boolean NodeVisibilityStateController::get_OutlineHovered()
extern void NodeVisibilityStateController_get_OutlineHovered_m2EF237C7FD26493AD7C597CAD20079F84482755F (void);
// 0x00000218 System.Void NodeVisibilityStateController::set_OutlineHovered(System.Boolean)
extern void NodeVisibilityStateController_set_OutlineHovered_m80418AB986A5FFDF1BDC796FA25E5AD9B420F485 (void);
// 0x00000219 NodeController NodeVisibilityStateController::get_NodeController()
extern void NodeVisibilityStateController_get_NodeController_m5F6BF11594011B0223A4A5582FDF10CC07381974 (void);
// 0x0000021A System.Void NodeVisibilityStateController::set_NodeController(NodeController)
extern void NodeVisibilityStateController_set_NodeController_m707D189E4188CDAD27A0764E176117420331855A (void);
// 0x0000021B System.Void NodeVisibilityStateController::Start()
extern void NodeVisibilityStateController_Start_m838050143B8A595D40A6CA58D968D49973D2B4F6 (void);
// 0x0000021C System.Void NodeVisibilityStateController::OnDestroy()
extern void NodeVisibilityStateController_OnDestroy_mE5274783EEB67274622B9D81F81BA9E286846282 (void);
// 0x0000021D System.Void NodeVisibilityStateController::RegisterEvents()
extern void NodeVisibilityStateController_RegisterEvents_mE85066357DC5B2669E6163BFB0BB84EAFCF9457B (void);
// 0x0000021E System.Void NodeVisibilityStateController::UnregisterEvents()
extern void NodeVisibilityStateController_UnregisterEvents_m840B4CDD375FB97475B164E350123BEBF5A04124 (void);
// 0x0000021F System.Void NodeVisibilityStateController::OnNodeAdded(ModelNode)
extern void NodeVisibilityStateController_OnNodeAdded_mE7EE6EDCA3E582FD58D21B8BADC09C7D402514DA (void);
// 0x00000220 System.Void NodeVisibilityStateController::OnNodeSelected(System.Int32)
extern void NodeVisibilityStateController_OnNodeSelected_m3C8FB1F1A2D0D824795829C87262F4A07A21E004 (void);
// 0x00000221 System.Void NodeVisibilityStateController::OnNodeHovered(System.Int32)
extern void NodeVisibilityStateController_OnNodeHovered_mA036937B0DB96B9AC44C0C4683B568D49CE6CB29 (void);
// 0x00000222 System.Void NodeVisibilityStateController::OnNodeDeselected(System.Int32)
extern void NodeVisibilityStateController_OnNodeDeselected_mC8178693F6EF69B00B58D31803A4A8B6C29F3BC5 (void);
// 0x00000223 System.Void NodeVisibilityStateController::OnNodeUnhovered(System.Int32)
extern void NodeVisibilityStateController_OnNodeUnhovered_mE99384D59F5A6800928C0D8EFCFDE7EE5061A8CB (void);
// 0x00000224 System.Boolean NodeVisibilityStateController::IsHidden(System.Int32)
extern void NodeVisibilityStateController_IsHidden_m73779E6CB3783A62AC87C8E826F8284A5C0DE83B (void);
// 0x00000225 System.Boolean NodeVisibilityStateController::IsFaded(System.Int32)
extern void NodeVisibilityStateController_IsFaded_m731A50A14843285595ECCA03D23FCDED06FD36A4 (void);
// 0x00000226 System.Boolean NodeVisibilityStateController::OtherLayersHidden(System.Int32)
extern void NodeVisibilityStateController_OtherLayersHidden_m0BF9FCA6F53C1D1B206017C7DCC15CD04114BD57 (void);
// 0x00000227 System.Boolean NodeVisibilityStateController::OtherLayersFaded(System.Int32)
extern void NodeVisibilityStateController_OtherLayersFaded_m4ABDA720E5D77FC9E2C9C028452C1CD540F3B261 (void);
// 0x00000228 System.Void NodeVisibilityStateController::ToggleHideNode(System.Int32)
extern void NodeVisibilityStateController_ToggleHideNode_mC0234DDDF211650A69EB0D954DAD7C63E3384D22 (void);
// 0x00000229 System.Void NodeVisibilityStateController::ToggleFadeNode(System.Int32)
extern void NodeVisibilityStateController_ToggleFadeNode_mCE0D8612D9B9743F35E1201A92EC1C1FD5E6824C (void);
// 0x0000022A System.Void NodeVisibilityStateController::HideNode(System.Int32)
extern void NodeVisibilityStateController_HideNode_m9E34FDB0E8DAA7F707E4AADC1EDEB6F39A61EFA3 (void);
// 0x0000022B System.Void NodeVisibilityStateController::FadeNode(System.Int32)
extern void NodeVisibilityStateController_FadeNode_m02F4813518E5BDD2831B99E27B2D9DCACC02CAA8 (void);
// 0x0000022C System.Void NodeVisibilityStateController::ShowNode(System.Int32)
extern void NodeVisibilityStateController_ShowNode_mD0BDCF8F9DFAF57B1C4F65F8E27741D634EC4F7F (void);
// 0x0000022D System.Void NodeVisibilityStateController::UnfadeNode(System.Int32)
extern void NodeVisibilityStateController_UnfadeNode_mBB0D92EBDC016838992E271D79794CA38C437E0E (void);
// 0x0000022E System.Void NodeVisibilityStateController::ShowAll()
extern void NodeVisibilityStateController_ShowAll_m4DD4CDB7BDF9EE1564C5C7FEA2B9B9C99301CA91 (void);
// 0x0000022F System.Void NodeVisibilityStateController::UnfadeAll()
extern void NodeVisibilityStateController_UnfadeAll_mB0246734BF6F8A8637E0EF3E870434EC53E52D94 (void);
// 0x00000230 System.Void NodeVisibilityStateController::HideAll()
extern void NodeVisibilityStateController_HideAll_m3B2D063B876AE108473A081A7C418E674C2AA572 (void);
// 0x00000231 System.Void NodeVisibilityStateController::FadeAll()
extern void NodeVisibilityStateController_FadeAll_mAC156C8AB2EBEA2C092146EDAE06DB16447037DF (void);
// 0x00000232 System.Void NodeVisibilityStateController::HideOtherLayers(System.Int32,System.Boolean)
extern void NodeVisibilityStateController_HideOtherLayers_m9841BFC2F3C3EC6F6E567A0BB2179DE8580B737F (void);
// 0x00000233 System.Void NodeVisibilityStateController::FadeOtherLayers(System.Int32,System.Boolean)
extern void NodeVisibilityStateController_FadeOtherLayers_m7E4374B453D67C78675E694713526C89154325B2 (void);
// 0x00000234 System.Void NodeVisibilityStateController::.ctor()
extern void NodeVisibilityStateController__ctor_m4B30C3FAF9D2265B3F16A1D21A392E628CBF775D (void);
// 0x00000235 System.Void NodeVisibilityStateController::<RegisterEvents>b__30_0(System.Boolean,System.Boolean)
extern void NodeVisibilityStateController_U3CRegisterEventsU3Eb__30_0_m41902C51F2737F191B54DDBD9E3503B48931DEC7 (void);
// 0x00000236 System.Void NodeVisibilityStateController::<RegisterEvents>b__30_1(System.Boolean,System.Boolean)
extern void NodeVisibilityStateController_U3CRegisterEventsU3Eb__30_1_m8A586A5ED9797BEAD33FA2D1611B5842D56D8C88 (void);
// 0x00000237 System.Void NodeVisibilityStateController::<OnNodeAdded>b__32_0(System.Boolean,System.Boolean)
extern void NodeVisibilityStateController_U3COnNodeAddedU3Eb__32_0_m88962164BB7B01403C078AF6C4D25AC6B15BC160 (void);
// 0x00000238 System.Void NodeVisibilityStateController::<OnNodeAdded>b__32_1(System.Boolean,System.Boolean)
extern void NodeVisibilityStateController_U3COnNodeAddedU3Eb__32_1_m01293661F5FEEEF471689DCAA6CFFA5268421120 (void);
// 0x00000239 System.Void AnimationMenu::Awake()
extern void AnimationMenu_Awake_m373435FBADE18D6ABD6139E83DE4E7D03A4112C7 (void);
// 0x0000023A System.Void AnimationMenu::OnIsPlayingValueChanged(System.Boolean,System.Boolean)
extern void AnimationMenu_OnIsPlayingValueChanged_mF5682A9DB34F4B40C943449DBDC60FBFF75617F3 (void);
// 0x0000023B System.Void AnimationMenu::OnClickPausePlay()
extern void AnimationMenu_OnClickPausePlay_m33451FC1F961DCC0130B64CA098BB5554A6CA806 (void);
// 0x0000023C System.Void AnimationMenu::OnClickStop()
extern void AnimationMenu_OnClickStop_mD9C322C1E63FC1948F82D9A0D3495085C04419C6 (void);
// 0x0000023D System.Void AnimationMenu::OnAnimationSliderUpdated(MixedReality.Toolkit.UX.SliderEventData)
extern void AnimationMenu_OnAnimationSliderUpdated_m33D790A5F8F4371E52D6ACC23A50A116D71F2B64 (void);
// 0x0000023E System.Void AnimationMenu::Update()
extern void AnimationMenu_Update_mCBA8F3C08102823EEE46F9CE49342F3E138D156E (void);
// 0x0000023F System.Void AnimationMenu::.ctor()
extern void AnimationMenu__ctor_m5E0935D7684BC162E036C07477E9A6B59BF20E26 (void);
// 0x00000240 System.String CourseMenu::get_SelectedCheckpointGraphName()
extern void CourseMenu_get_SelectedCheckpointGraphName_mE7485583238B4F692A814A945CECBD60CF325678 (void);
// 0x00000241 System.Void CourseMenu::set_SelectedCheckpointGraphName(System.String)
extern void CourseMenu_set_SelectedCheckpointGraphName_mEEA997763934F5D26D01F8E3DFA9186E5EE16005 (void);
// 0x00000242 System.Void CourseMenu::OnClickStartCourse()
extern void CourseMenu_OnClickStartCourse_mF311853DC459CBC6D73D6EDF61288A514F78D90A (void);
// 0x00000243 System.Void CourseMenu::LoadCourses(System.String)
extern void CourseMenu_LoadCourses_m1AEBF04DA67008A842EBDC5F341510C87200484B (void);
// 0x00000244 System.Void CourseMenu::.ctor()
extern void CourseMenu__ctor_m6F5B4C5378588CF9ED02253D7DAF049233F924F8 (void);
// 0x00000245 System.Void CourseMenu/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4640DC48A59D9989F12CFBE56CD69CF9A42F9D1A (void);
// 0x00000246 System.Void CourseMenu/<>c__DisplayClass10_0::<LoadCourses>b__0(UnityEngine.GameObject,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CLoadCoursesU3Eb__0_mA8CCA323A5A1EF0E055665F74B83A261F4275F98 (void);
// 0x00000247 System.Void CourseMenu/<>c__DisplayClass10_1::.ctor()
extern void U3CU3Ec__DisplayClass10_1__ctor_m90A5B3BE280DF6189578333933A4B4C22CB62AF2 (void);
// 0x00000248 System.Void CourseMenu/<>c__DisplayClass10_1::<LoadCourses>b__2()
extern void U3CU3Ec__DisplayClass10_1_U3CLoadCoursesU3Eb__2_mE70B340315BACFA5FEBB52374F2CFE1B5D30BB9A (void);
// 0x00000249 System.Void CourseMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_m0182A1B2CCB01155E87C96A35CA98673DC8153B7 (void);
// 0x0000024A System.Void CourseMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_m1687C2A533A3290CDD2758DDCA821B4820A96878 (void);
// 0x0000024B System.Void CourseMenu/<>c::<LoadCourses>b__10_1(UnityEngine.GameObject,System.Int32)
extern void U3CU3Ec_U3CLoadCoursesU3Eb__10_1_m730CBCAC9EEB066487EA614BDBEFD8A0C8FCD043 (void);
// 0x0000024C System.Void LayerMenu::SetUIDirty()
extern void LayerMenu_SetUIDirty_m43DCFCC06B1A76DD94FF4EF173D646722508F43B (void);
// 0x0000024D System.Int32 LayerMenu::buttonIDToNodeID(System.Int32)
extern void LayerMenu_buttonIDToNodeID_m0976D668098B61EDBD3580DCDC71C90EE48C7527 (void);
// 0x0000024E System.Int32 LayerMenu::nodeIDToButtonID(System.Int32)
extern void LayerMenu_nodeIDToButtonID_mF3E146159082136968EE91126B7B2E98D3CC1042 (void);
// 0x0000024F NodeVisibilityStateController LayerMenu::get_StateController()
extern void LayerMenu_get_StateController_m7ED0F7AB7AD056A484AA3FA45A65FCBAFCA72F73 (void);
// 0x00000250 System.Void LayerMenu::set_StateController(NodeVisibilityStateController)
extern void LayerMenu_set_StateController_mE014352D09519195EE20AFE038D211A156B91C54 (void);
// 0x00000251 System.Int32 LayerMenu::get_NumButtons()
extern void LayerMenu_get_NumButtons_m7DB608DD8C8C24FB45A03F1F191968ED7EE71BE1 (void);
// 0x00000252 NodeController LayerMenu::get_NodeController()
extern void LayerMenu_get_NodeController_m5B7B309401E64621101E7165005EE2D2E9898EE9 (void);
// 0x00000253 System.Void LayerMenu::set_NodeController(NodeController)
extern void LayerMenu_set_NodeController_m82F460BD02258F5B419E413832334885A42C5534 (void);
// 0x00000254 System.Void LayerMenu::OnNodeAdded(ModelNode)
extern void LayerMenu_OnNodeAdded_mBBED3CA04190677C53F021EE69B786A1F44219EC (void);
// 0x00000255 System.Void LayerMenu::OnNodeSelected(System.Int32)
extern void LayerMenu_OnNodeSelected_mCE901AA3BE3D782EEFEE3580F68A1CADA3E959E1 (void);
// 0x00000256 System.Void LayerMenu::OnClickHide()
extern void LayerMenu_OnClickHide_m3195379BF76B6CBD5191AC08415BC58EE5DAB80A (void);
// 0x00000257 System.Void LayerMenu::OnClickHideOthers()
extern void LayerMenu_OnClickHideOthers_m1F9E79A9A5474A4C3E147480F257CF3DA061D9CF (void);
// 0x00000258 System.Void LayerMenu::OnClickFade()
extern void LayerMenu_OnClickFade_m1A1B1D71EED4C87B6198B6589A88D42423DBFB35 (void);
// 0x00000259 System.Void LayerMenu::OnClickFadeOthers()
extern void LayerMenu_OnClickFadeOthers_m59179E112E76BBC85C88E2E61A8C4D8E4A8241E2 (void);
// 0x0000025A System.Void LayerMenu::OnClickShowAll()
extern void LayerMenu_OnClickShowAll_m10D7EE07003526374ACB0EF66F147E17CA9A4094 (void);
// 0x0000025B System.Void LayerMenu::OnClickUnfadeAll()
extern void LayerMenu_OnClickUnfadeAll_m7B864D11FBA4B14C55F3CC8A1165843DB2E0F695 (void);
// 0x0000025C System.Void LayerMenu::UpdateUI()
extern void LayerMenu_UpdateUI_mDC61D8BD72752616CBD5D4D6044099F14F732BAB (void);
// 0x0000025D System.Void LayerMenu::OnDestroy()
extern void LayerMenu_OnDestroy_m6626FAD1F5DDC7C9A479CD65878ADA19D31506F4 (void);
// 0x0000025E System.Void LayerMenu::Update()
extern void LayerMenu_Update_m23D4C1374DDFC3BB24B00B7B44ED674EC2BCFC90 (void);
// 0x0000025F System.Void LayerMenu::.ctor()
extern void LayerMenu__ctor_m8EFE84B60F0D8CA708744946391AE9B96C8B04A7 (void);
// 0x00000260 System.Void LayerMenu::<set_NodeController>b__27_0(UnityEngine.GameObject,System.Int32)
extern void LayerMenu_U3Cset_NodeControllerU3Eb__27_0_m8043AEEF4542C6EED423333E930AB386E99C6DAC (void);
// 0x00000261 System.Void LayerMenu/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m5AB9B23E58BCB793749BB4F3245A1A6D44F93E56 (void);
// 0x00000262 System.Void LayerMenu/<>c__DisplayClass27_0::<set_NodeController>b__2()
extern void U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__2_m42A4BF08F230F812CEB7FC07E12C4703D0CB5539 (void);
// 0x00000263 System.Void LayerMenu/<>c__DisplayClass27_0::<set_NodeController>b__3()
extern void U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__3_mD9DBB46D148B07E0A3871768156EA24548990520 (void);
// 0x00000264 System.Void LayerMenu/<>c__DisplayClass27_0::<set_NodeController>b__4()
extern void U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__4_mC83FF2057180B5940C2B2CCCDC7B16F8503C82E0 (void);
// 0x00000265 System.Void LayerMenu/<>c__DisplayClass27_0::<set_NodeController>b__5()
extern void U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__5_m58CF3E24336795B72A235062EE7E4F792A75D418 (void);
// 0x00000266 System.Void LayerMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_mA0E2DFBCCE6779162AFEC87C974263DF8A68243E (void);
// 0x00000267 System.Void LayerMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_m353A9DC42FD5DE0CECA17F31F0D0174AD341A3B4 (void);
// 0x00000268 System.Void LayerMenu/<>c::<set_NodeController>b__27_1(UnityEngine.GameObject,System.Int32)
extern void U3CU3Ec_U3Cset_NodeControllerU3Eb__27_1_mE05442CFBB1709F29A2F2E93C64D20EC8C9D6881 (void);
// 0x00000269 System.Boolean NodeButton::get_IsLeaf()
extern void NodeButton_get_IsLeaf_m16C4F324748A06489E8E7E02D94CCEADB8167FB8 (void);
// 0x0000026A System.Void NodeButton::set_IsLeaf(System.Boolean)
extern void NodeButton_set_IsLeaf_m6A2F97455FBB594E859055AE02EC8447217303C7 (void);
// 0x0000026B System.Boolean NodeButton::get_IsExpanded()
extern void NodeButton_get_IsExpanded_mF15ADC3482C85837D4ACA05E82CC47A980322C96 (void);
// 0x0000026C System.Void NodeButton::set_IsExpanded(System.Boolean)
extern void NodeButton_set_IsExpanded_m1C655AC358C19687882D7F50094F5ECB1324A020 (void);
// 0x0000026D System.Int32 NodeButton::get_Level()
extern void NodeButton_get_Level_mBFC40B9DD56F137235972460B734507A6F09C00C (void);
// 0x0000026E System.Void NodeButton::set_Level(System.Int32)
extern void NodeButton_set_Level_m6084C3DEC2F7B7CDF64582C475724C68123D9438 (void);
// 0x0000026F System.Boolean NodeButton::get_IsVisible()
extern void NodeButton_get_IsVisible_mDA207A8F7EAEBEDD554E69A3BB384DB88614E67B (void);
// 0x00000270 System.Void NodeButton::set_IsVisible(System.Boolean)
extern void NodeButton_set_IsVisible_mACB89EB05B4C5284D892589FEA78F47B7C752C39 (void);
// 0x00000271 System.Boolean NodeButton::get_IsOpaque()
extern void NodeButton_get_IsOpaque_mB3532E400EB01E0D7B0B45E696969BA9601F955C (void);
// 0x00000272 System.Void NodeButton::set_IsOpaque(System.Boolean)
extern void NodeButton_set_IsOpaque_m81AA456CD023250F73A19000BB729AE55EA969CF (void);
// 0x00000273 System.Void NodeButton::SetNodeInfo(ModelNodeScriptableObject)
extern void NodeButton_SetNodeInfo_mD418A0E9BFAE93B35EBB672004A9CECD291FCBC8 (void);
// 0x00000274 System.Void NodeButton::.ctor()
extern void NodeButton__ctor_mDDB4B3B0F75CF064A2A55F74134341182439A294 (void);
// 0x00000275 System.Void SettingsMenu::Awake()
extern void SettingsMenu_Awake_mCDFFB87F0B63DED3D973531C7E3CC95E3CF1495C (void);
// 0x00000276 System.Void SettingsMenu::OnClippingPlaneIsActiveValueChanged(System.Boolean,System.Boolean)
extern void SettingsMenu_OnClippingPlaneIsActiveValueChanged_m0399DAEA914559A296B5968218AE9C727D6294D0 (void);
// 0x00000277 System.Void SettingsMenu::OnClippingPlaneTextureIdxValueChanged(System.Int32,System.Int32)
extern void SettingsMenu_OnClippingPlaneTextureIdxValueChanged_m943863E91E31614F39E2A86E912AF695E3710824 (void);
// 0x00000278 System.Void SettingsMenu::OnAnimationSpeedChanged(System.Single,System.Single)
extern void SettingsMenu_OnAnimationSpeedChanged_m57651F442265423B2FF90AFEE0B505D0810BF0B5 (void);
// 0x00000279 System.Void SettingsMenu::OnRotationSpeedChanged(System.Single,System.Single)
extern void SettingsMenu_OnRotationSpeedChanged_mE952487B07F18581AD0F256C88E852B997BA8353 (void);
// 0x0000027A System.Void SettingsMenu::OnAutorotateModelValueChanged(System.Boolean,System.Boolean)
extern void SettingsMenu_OnAutorotateModelValueChanged_mAACB6BC2AAFFFB2AADF5C439DCC0B3C9D49D3986 (void);
// 0x0000027B System.Void SettingsMenu::OnChangeAnimationSpeed(MixedReality.Toolkit.UX.SliderEventData)
extern void SettingsMenu_OnChangeAnimationSpeed_m7108D368ACBAF429619006FB9C238757314954B0 (void);
// 0x0000027C System.Void SettingsMenu::OnToggleAnimationMenu()
extern void SettingsMenu_OnToggleAnimationMenu_m2415C6016E565D91E8125C54F64D4988C71B7191 (void);
// 0x0000027D System.Void SettingsMenu::OnToggleModelRotation(System.Boolean)
extern void SettingsMenu_OnToggleModelRotation_mBC7D2C4655CC94B522BC35D77661CF68DA0C4938 (void);
// 0x0000027E System.Void SettingsMenu::OnChangeRotationSpeed(MixedReality.Toolkit.UX.SliderEventData)
extern void SettingsMenu_OnChangeRotationSpeed_m396C6F5636031E3A50D9387FFDF10A46E708BCAC (void);
// 0x0000027F System.Void SettingsMenu::OnToggleClippingPlane()
extern void SettingsMenu_OnToggleClippingPlane_m387B9183E340E3873F14AB207BE41A333A840B27 (void);
// 0x00000280 System.Void SettingsMenu::OnSelectVolumetricTexture(System.Int32)
extern void SettingsMenu_OnSelectVolumetricTexture_m7168349B722232B4D0CDC7B534E95D9F9C384ED3 (void);
// 0x00000281 System.Void SettingsMenu::OnCheckSelectionMode(System.Int32)
extern void SettingsMenu_OnCheckSelectionMode_m1A03EB05D72745AB71D74367FDABC1A7BCD944F7 (void);
// 0x00000282 System.Void SettingsMenu::OnToggleShowAnnotations()
extern void SettingsMenu_OnToggleShowAnnotations_mEC91B55D15AF6F9D66D1F0EECAAA0AE5A6566766 (void);
// 0x00000283 System.Void SettingsMenu::OnToggleReadDescriptions()
extern void SettingsMenu_OnToggleReadDescriptions_m296014715B40541EB4FB63A96AF6C04F240DFE9E (void);
// 0x00000284 System.Void SettingsMenu::OnToggleGroupedSelection()
extern void SettingsMenu_OnToggleGroupedSelection_mF6738EB77B987B360D6EDECAE3E46D071EE26289 (void);
// 0x00000285 System.Void SettingsMenu::OnToggleGazeInteractions()
extern void SettingsMenu_OnToggleGazeInteractions_mC78F6A12373D2C311F6A63F9FDD05CC9BF018A0B (void);
// 0x00000286 System.Void SettingsMenu::OnCheckHighlightSelectedLayers()
extern void SettingsMenu_OnCheckHighlightSelectedLayers_mD7C8539D38271B67EE8017587F154C78023C8BE0 (void);
// 0x00000287 System.Void SettingsMenu::OnUncheckHighlightSelectedLayers()
extern void SettingsMenu_OnUncheckHighlightSelectedLayers_m62778E32C3146F55CCEEC32850CFDEF687E9C326 (void);
// 0x00000288 System.Void SettingsMenu::OnCheckHighlightHoveredLayers()
extern void SettingsMenu_OnCheckHighlightHoveredLayers_mAAFA70F12FE459B3702A41AF0DFDEB9CFFD8CE4F (void);
// 0x00000289 System.Void SettingsMenu::OnUncheckHighlightHoveredLayers()
extern void SettingsMenu_OnUncheckHighlightHoveredLayers_m58E3690888BED5A481895D36BF4E7EA92BA754D0 (void);
// 0x0000028A System.Void SettingsMenu::OnCheckTooltipSelectedLayers()
extern void SettingsMenu_OnCheckTooltipSelectedLayers_mE0B8EE32ECB351C9A7C25DFAB5F1C111D578A758 (void);
// 0x0000028B System.Void SettingsMenu::OnUncheckTooltipSelectedLayers()
extern void SettingsMenu_OnUncheckTooltipSelectedLayers_m40E398B8CA5A898BA2B41A075071E503C3146987 (void);
// 0x0000028C System.Void SettingsMenu::OnCheckTooltipHoveredLayers()
extern void SettingsMenu_OnCheckTooltipHoveredLayers_m73C979AA247FC11208ADD9531E2ECB6D4DB2EDA1 (void);
// 0x0000028D System.Void SettingsMenu::OnUncheckTooltipHoveredLayers()
extern void SettingsMenu_OnUncheckTooltipHoveredLayers_mFFA1EE00CC7C6D4B821F4E7C8EC1C928FA44D2FD (void);
// 0x0000028E System.Void SettingsMenu::OnToggleColorLayers(System.Int32)
extern void SettingsMenu_OnToggleColorLayers_mFE4A5D8A444D62F9471CF133207724FED0C36331 (void);
// 0x0000028F System.Void SettingsMenu::SetModel(ModelScriptableObject)
extern void SettingsMenu_SetModel_mAA2DB9619E5F19AA630F479B26FF9EF88DFA52D1 (void);
// 0x00000290 System.Void SettingsMenu::.ctor()
extern void SettingsMenu__ctor_mAB642D09DCFB585F2998C81DE33DD08C5C420E1C (void);
// 0x00000291 System.Void ShortcutMenu::OnClickCloseModel()
extern void ShortcutMenu_OnClickCloseModel_m6ACE1740413E5A4F6EA45CBF55EBE4D72B80F712 (void);
// 0x00000292 System.Void ShortcutMenu::OnClickShowMenu()
extern void ShortcutMenu_OnClickShowMenu_m449328845141DE1F9E362A1717E96D3ED7337243 (void);
// 0x00000293 System.Void ShortcutMenu::OnClickShowBoundingBox()
extern void ShortcutMenu_OnClickShowBoundingBox_m2C0A4AC4BE481244D50B74D58FAD357F34E584B4 (void);
// 0x00000294 System.Void ShortcutMenu::OnToggleMovementType()
extern void ShortcutMenu_OnToggleMovementType_m1E83C4A30FEAB75A3C3E0C82CD7EFE1F53963999 (void);
// 0x00000295 System.Void ShortcutMenu::OnClickExplodeView()
extern void ShortcutMenu_OnClickExplodeView_m493B8F877F200C2BB3808468BE71DA34647890AF (void);
// 0x00000296 System.Void ShortcutMenu::OnClickResetView()
extern void ShortcutMenu_OnClickResetView_m90E725C215BBB9702DA7B40C08B8326811D4CAD6 (void);
// 0x00000297 System.Void ShortcutMenu::.ctor()
extern void ShortcutMenu__ctor_m10EF98CBBFA8429E2F8A0C06774EA245104E980B (void);
// 0x00000298 System.Int32 UIController::get_CurrentTab()
extern void UIController_get_CurrentTab_m1BC9AFE655D91A01C370BC5F3A0A007775E98D7D (void);
// 0x00000299 System.Void UIController::set_CurrentTab(System.Int32)
extern void UIController_set_CurrentTab_m672F6C81839419233E8C80FBA99D095AF8E2FB32 (void);
// 0x0000029A System.Boolean UIController::get_SideMenuActive()
extern void UIController_get_SideMenuActive_mF0683DC3E7FCBD0276A3CA2092AA02E3C6530064 (void);
// 0x0000029B System.Void UIController::set_SideMenuActive(System.Boolean)
extern void UIController_set_SideMenuActive_m5C462EEF0783B8BC4126BA4BBC4E9EE1AAA0042F (void);
// 0x0000029C System.Boolean UIController::get_AnimationMenuActive()
extern void UIController_get_AnimationMenuActive_mFDE1F70C52451C3717BE83F25F9A732E4D3A9ADF (void);
// 0x0000029D System.Void UIController::set_AnimationMenuActive(System.Boolean)
extern void UIController_set_AnimationMenuActive_m3850C11E8C14AF0A3BA923D80C27C94778F7A709 (void);
// 0x0000029E System.Collections.Generic.List`1<System.Boolean> UIController::get_TabsActive()
extern void UIController_get_TabsActive_m322AE6983C1B9341A8B85EF7C58BC28E029B0964 (void);
// 0x0000029F System.Void UIController::set_TabsActive(System.Collections.Generic.List`1<System.Boolean>)
extern void UIController_set_TabsActive_m781D217EAD65283A949292F06743E8A5F94A1429 (void);
// 0x000002A0 System.Void UIController::ToggleSideMenu()
extern void UIController_ToggleSideMenu_m5BFA7DF1438848343829E30D6E0FBFBDB4F0C9BC (void);
// 0x000002A1 System.Void UIController::ToggleAnimationMenu()
extern void UIController_ToggleAnimationMenu_mF7138D086387EE5324CE75AC635DC7E4BDAC2681 (void);
// 0x000002A2 UIState UIController::get_state()
extern void UIController_get_state_m03F931F9E53911B99798BA40C35CC73FE6DD5CC9 (void);
// 0x000002A3 System.Void UIController::SetUIState(UIState)
extern void UIController_SetUIState_mBFDE8CDE7EEF1D9F2DFA9A297F787AC33F33895B (void);
// 0x000002A4 System.Void UIController::SetUIStateServerRpc(UIState)
extern void UIController_SetUIStateServerRpc_m03F6569E7BE9B8F913D6DD216F91320B8C20C739 (void);
// 0x000002A5 System.Void UIController::.ctor()
extern void UIController__ctor_mFF218DBC8CCEFE36AAC295D2376501658CD8B7A2 (void);
// 0x000002A6 System.Void UIController::__initializeVariables()
extern void UIController___initializeVariables_m1510A4F84E5851F32AFD8D6D125F5104F7432F69 (void);
// 0x000002A7 System.Void UIController::__initializeRpcs()
extern void UIController___initializeRpcs_m83A8CEA1408B47ED405E2A449C04C721419F110A (void);
// 0x000002A8 System.Void UIController::__rpc_handler_4105482430(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void UIController___rpc_handler_4105482430_m4094C44647DE97A52211ED34C03B0E3B51A27045 (void);
// 0x000002A9 System.String UIController::__getTypeName()
extern void UIController___getTypeName_m9DAF56EBE6947A15BBFAD9B3A1F191413707A154 (void);
// 0x000002AA System.Void ModelInstanceSpawner::SpawnModel(System.String)
extern void ModelInstanceSpawner_SpawnModel_mD086D4B7B704881D4664061564930491F533FF29 (void);
// 0x000002AB System.Void ModelInstanceSpawner::SpawnModelServerRpc(System.String,UnityEngine.Vector3)
extern void ModelInstanceSpawner_SpawnModelServerRpc_mA59C14969E9F1BCF5792E3475B7AB3B52ECAD0BD (void);
// 0x000002AC System.Void ModelInstanceSpawner::Start()
extern void ModelInstanceSpawner_Start_m8649BE53E8E63548C1339746570DC79EBF9AAD8D (void);
// 0x000002AD System.Void ModelInstanceSpawner::.ctor()
extern void ModelInstanceSpawner__ctor_mB6015FF84127E16F2A5AEA57B1C95523A5705EE5 (void);
// 0x000002AE System.Void ModelInstanceSpawner::__initializeVariables()
extern void ModelInstanceSpawner___initializeVariables_m26A221CD816989645FC9A18BDBC4BBF937466E78 (void);
// 0x000002AF System.Void ModelInstanceSpawner::__initializeRpcs()
extern void ModelInstanceSpawner___initializeRpcs_m9A58BEAF0A054C7E87CA7ADB9244606D9A4235AB (void);
// 0x000002B0 System.Void ModelInstanceSpawner::__rpc_handler_2237502063(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void ModelInstanceSpawner___rpc_handler_2237502063_mD08A176DE377C74494F7B9DA5C61E6869F617A03 (void);
// 0x000002B1 System.String ModelInstanceSpawner::__getTypeName()
extern void ModelInstanceSpawner___getTypeName_mDB29FBF854C6F2DC5A9C53085BAC5B4539C750EC (void);
// 0x000002B2 System.Void ModelInstanceSpawner/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mE745783EA9BECFAF435DFE55E0E750EC8AEE2798 (void);
// 0x000002B3 System.Void ModelInstanceSpawner/<>c__DisplayClass9_0::<SpawnModel>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CSpawnModelU3Eb__0_m84AA7B89C78C39FA7119565FAF9D99F6560CB979 (void);
// 0x000002B4 System.Void ModelInstanceSpawner/<>c__DisplayClass9_0::<SpawnModel>b__1()
extern void U3CU3Ec__DisplayClass9_0_U3CSpawnModelU3Eb__1_mB628926E8C1BC5EB6726204252213AED392BAAF0 (void);
// 0x000002B5 System.Void ModelButton::SetModel(ModelScriptableObject)
extern void ModelButton_SetModel_mA7F07EB8DBC3EF06250370EABF155362C3A78259 (void);
// 0x000002B6 System.Void ModelButton::.ctor()
extern void ModelButton__ctor_mB0A326B1309322468724DB8C37E2C4A66F7264B2 (void);
// 0x000002B7 System.Void ModelSelectionView::Start()
extern void ModelSelectionView_Start_mBED178692CCD393B727BCBA5B114900E99884355 (void);
// 0x000002B8 System.Void ModelSelectionView::SetModels(System.Collections.Generic.List`1<ModelScriptableObject>)
extern void ModelSelectionView_SetModels_mC83E43B1CE9FD3C6FFD93A22338B0FF53EB19548 (void);
// 0x000002B9 System.Void ModelSelectionView::InitializeSelectionButtons(System.Collections.Generic.List`1<ModelScriptableObject>)
extern void ModelSelectionView_InitializeSelectionButtons_mAD430A8509190FFB72BB7B4057946C2DA1AFD6CD (void);
// 0x000002BA System.Void ModelSelectionView::SelectModel(System.String)
extern void ModelSelectionView_SelectModel_m058885C6E9ACDF8DFE54D7765DFC34C88AC573B7 (void);
// 0x000002BB System.String ModelSelectionView::get_SelectedModelName()
extern void ModelSelectionView_get_SelectedModelName_m4F1022505A8BC00CC6E93E75CD48054006EE3F94 (void);
// 0x000002BC System.Void ModelSelectionView::set_SelectedModelName(System.String)
extern void ModelSelectionView_set_SelectedModelName_m4159DD2D7CEAC20D12C01AF455F953E9B4858FD5 (void);
// 0x000002BD System.Void ModelSelectionView::InitializeSpawnButtons()
extern void ModelSelectionView_InitializeSpawnButtons_mA344BD21858A2A0067E89BEFCB570CC22FB431F7 (void);
// 0x000002BE System.Void ModelSelectionView::SpawnSelectedModel()
extern void ModelSelectionView_SpawnSelectedModel_m435A5B71DC4D77241F45AB4538FF24D910F82840 (void);
// 0x000002BF System.Void ModelSelectionView::.ctor()
extern void ModelSelectionView__ctor_m866FF33FAEE00D8DC0DCC296CBEB5BE523B57822 (void);
// 0x000002C0 System.Void ModelSelectionView/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m6F6C57530FF060B871E435A008D6085431DC4922 (void);
// 0x000002C1 System.Void ModelSelectionView/<>c__DisplayClass14_0::<InitializeSelectionButtons>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CInitializeSelectionButtonsU3Eb__0_m8C912A84AD9FB4AE2B6D81FC9C7A738D94F6D42A (void);
// 0x000002C2 System.Void FaceCamera::Start()
extern void FaceCamera_Start_m36C189451F380B7FA03494B1466276854CF49FBD (void);
// 0x000002C3 System.Void FaceCamera::Update()
extern void FaceCamera_Update_m295E828F20DC5F8CADC64934C989C94F27592CA3 (void);
// 0x000002C4 System.Void FaceCamera::.ctor()
extern void FaceCamera__ctor_mC5D44A53DFAE40B1103AEBB2A513D3CE453AE25A (void);
// 0x000002C5 System.Void MoveWithScale::Update()
extern void MoveWithScale_Update_mA4F9AE47ABBED7BAED9889BAA34D0B03D7741D1F (void);
// 0x000002C6 System.Void MoveWithScale::.ctor()
extern void MoveWithScale__ctor_mD1FB2A319A8EE4CA0A04D2DEA01F406290F42266 (void);
// 0x000002C7 System.Void Rotate::Start()
extern void Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB (void);
// 0x000002C8 System.Void Rotate::Update()
extern void Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7 (void);
// 0x000002C9 System.Void Rotate::.ctor()
extern void Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4 (void);
// 0x000002CA T SN`1::get_Value()
// 0x000002CB System.Boolean SN`1::get_HasValue()
// 0x000002CC T SN`1::GetValueOrDefault(T)
// 0x000002CD System.Void SN`1::.ctor(System.Boolean,T)
// 0x000002CE System.Void SN`1::.ctor(T)
// 0x000002CF SN`1<T> SN`1::op_Implicit(T)
// 0x000002D0 SN`1<T> SN`1::op_Implicit(System.Nullable`1<T>)
// 0x000002D1 System.Nullable`1<T> SN`1::op_Implicit(SN`1<T>)
// 0x000002D2 System.Void AddressableGUID::NetworkSerialize(Unity.Netcode.BufferSerializer`1<T>)
// 0x000002D3 System.Int32 AddressableGUID::GetHashCode()
extern void AddressableGUID_GetHashCode_m1B75548F336B04CBCC0F79D343C8B7793A5428CD (void);
// 0x000002D4 System.String AddressableGUID::ToString()
extern void AddressableGUID_ToString_m4FEFA14381A279352C222CAC78765D64877B0F57 (void);
// 0x000002D5 System.Int32 AddressableGUIDEqualityComparer::GetHashCode(AddressableGUID)
extern void AddressableGUIDEqualityComparer_GetHashCode_m0ADF96C8AE96E81949584C4957A5841F99CFE400 (void);
// 0x000002D6 System.Boolean AddressableGUIDEqualityComparer::Equals(AddressableGUID,AddressableGUID)
extern void AddressableGUIDEqualityComparer_Equals_m0EBF00D28B970E0FDAAC4288F762C5289AF85668 (void);
// 0x000002D7 System.Void AddressableGUIDEqualityComparer::.ctor()
extern void AddressableGUIDEqualityComparer__ctor_mDFD280AEECF2CE714179798711BA35ED48744DDC (void);
// 0x000002D8 System.Void ConnectionPayload::.ctor()
extern void ConnectionPayload__ctor_mB9FE78986306A8A447B39B2F3E9FE0DF402DCD36 (void);
// 0x000002D9 System.Void DisconnectionPayload::.ctor()
extern void DisconnectionPayload__ctor_mA0E6EEC2B19E7AD14A949ADCA0613617CB148880 (void);
// 0x000002DA System.Int32 DynamicPrefabLoadingUtilities::get_HashOfDynamicPrefabGUIDs()
extern void DynamicPrefabLoadingUtilities_get_HashOfDynamicPrefabGUIDs_m19BE6FB0EC2F55CAC5D5526EE38D145311BF173A (void);
// 0x000002DB System.Void DynamicPrefabLoadingUtilities::set_HashOfDynamicPrefabGUIDs(System.Int32)
extern void DynamicPrefabLoadingUtilities_set_HashOfDynamicPrefabGUIDs_m70F46E1894B38B2695D9AC801F96BB4124F9606C (void);
// 0x000002DC System.Boolean DynamicPrefabLoadingUtilities::HasClientLoadedPrefab(System.UInt64,System.Int32)
extern void DynamicPrefabLoadingUtilities_HasClientLoadedPrefab_m5FDF779AD901113110E7DEBB5FDAD32F7F79799A (void);
// 0x000002DD System.Boolean DynamicPrefabLoadingUtilities::IsPrefabLoadedOnAllClients(AddressableGUID)
extern void DynamicPrefabLoadingUtilities_IsPrefabLoadedOnAllClients_m0547BB30966EF228D54C95698266D4F02084BE52 (void);
// 0x000002DE System.Boolean DynamicPrefabLoadingUtilities::TryGetLoadedGameObjectFromGuid(AddressableGUID,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>&)
extern void DynamicPrefabLoadingUtilities_TryGetLoadedGameObjectFromGuid_m590430CCE0D7B8B27796D289AA9712B19F8C6A59 (void);
// 0x000002DF System.Collections.Generic.Dictionary`2<AddressableGUID,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>> DynamicPrefabLoadingUtilities::get_LoadedDynamicPrefabResourceHandles()
extern void DynamicPrefabLoadingUtilities_get_LoadedDynamicPrefabResourceHandles_mE3FE39EBF4C9C60C2B9C37553CC8A4BDB04A496C (void);
// 0x000002E0 System.Int32 DynamicPrefabLoadingUtilities::get_LoadedPrefabCount()
extern void DynamicPrefabLoadingUtilities_get_LoadedPrefabCount_m7C1CC63B963E14E571472E008EA8A14371408A85 (void);
// 0x000002E1 System.Void DynamicPrefabLoadingUtilities::.cctor()
extern void DynamicPrefabLoadingUtilities__cctor_m26546A581E4ADF74C06B13411561260B42E7988D (void);
// 0x000002E2 System.Void DynamicPrefabLoadingUtilities::Init(Unity.Netcode.NetworkManager)
extern void DynamicPrefabLoadingUtilities_Init_m8E8CC5A0009C96664F9F456AB9DD56BBC3F57384 (void);
// 0x000002E3 System.Threading.Tasks.Task`1<UnityEngine.GameObject> DynamicPrefabLoadingUtilities::LoadDynamicPrefab(AddressableGUID,System.Boolean)
extern void DynamicPrefabLoadingUtilities_LoadDynamicPrefab_m552BB90861F6C915C1B67491430E3E0446C372BE (void);
// 0x000002E4 System.Threading.Tasks.Task`1<System.Collections.Generic.IList`1<UnityEngine.GameObject>> DynamicPrefabLoadingUtilities::LoadDynamicPrefabs(AddressableGUID[])
extern void DynamicPrefabLoadingUtilities_LoadDynamicPrefabs_mE8922623F2A8555D7DEA57A5CFC505506CF19954 (void);
// 0x000002E5 System.Void DynamicPrefabLoadingUtilities::RecordThatClientHasLoadedAPrefab(System.Int32,System.UInt64)
extern void DynamicPrefabLoadingUtilities_RecordThatClientHasLoadedAPrefab_m467D99718CCC83E2E98A18D9FE0C40CD4323DCE3 (void);
// 0x000002E6 System.Void DynamicPrefabLoadingUtilities::UnloadAndReleaseAllDynamicPrefabs()
extern void DynamicPrefabLoadingUtilities_UnloadAndReleaseAllDynamicPrefabs_m9174EAABC95326B2FE7A4357524E0DD64FA41DEF (void);
// 0x000002E7 System.Byte[] DynamicPrefabLoadingUtilities::GenerateRequestPayload()
extern void DynamicPrefabLoadingUtilities_GenerateRequestPayload_mE03D3133A759A5E90DF8B72299E7D644A7580E88 (void);
// 0x000002E8 System.Void DynamicPrefabLoadingUtilities::RefreshLoadedPrefabGuids()
extern void DynamicPrefabLoadingUtilities_RefreshLoadedPrefabGuids_m47C3E0A258A7E251AB0B1CD66CEC128E2F8F33A3 (void);
// 0x000002E9 System.Void DynamicPrefabLoadingUtilities::CalculateDynamicPrefabArrayHash()
extern void DynamicPrefabLoadingUtilities_CalculateDynamicPrefabArrayHash_mDD33053B593190159B45A369A256C36C7303A397 (void);
// 0x000002EA System.Void DynamicPrefabLoadingUtilities/<LoadDynamicPrefab>d__18::MoveNext()
extern void U3CLoadDynamicPrefabU3Ed__18_MoveNext_mDAA5B95B0DEC4B3CD160BE6FA7A41995C186005A (void);
// 0x000002EB System.Void DynamicPrefabLoadingUtilities/<LoadDynamicPrefab>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadDynamicPrefabU3Ed__18_SetStateMachine_m7201FDC8677C4768C3AA33252F3DD9916A159DE3 (void);
// 0x000002EC System.Void DynamicPrefabLoadingUtilities/<LoadDynamicPrefabs>d__19::MoveNext()
extern void U3CLoadDynamicPrefabsU3Ed__19_MoveNext_mBAF81017A61D5A5310906EC39EFA0762615B4647 (void);
// 0x000002ED System.Void DynamicPrefabLoadingUtilities/<LoadDynamicPrefabs>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadDynamicPrefabsU3Ed__19_SetStateMachine_mDECD003D0E34E262C5F471404EF89DBAFB80948F (void);
// 0x000002EE System.Void DynamicPrefabLoadingUtilities/<>c::.cctor()
extern void U3CU3Ec__cctor_m35FAF4C9C8712BC1CBC9A022AEDD2B1334C207A1 (void);
// 0x000002EF System.Void DynamicPrefabLoadingUtilities/<>c::.ctor()
extern void U3CU3Ec__ctor_m29428986C77FD6386DCAA646F72B95C9D6880EB7 (void);
// 0x000002F0 System.Int32 DynamicPrefabLoadingUtilities/<>c::<CalculateDynamicPrefabArrayHash>b__24_0(AddressableGUID,AddressableGUID)
extern void U3CU3Ec_U3CCalculateDynamicPrefabArrayHashU3Eb__24_0_m26647CE7AA489E827A481FCE704ABF3639AE0C04 (void);
// 0x000002F1 System.Void DynamicPrefabSpawner::Start()
extern void DynamicPrefabSpawner_Start_mB0B47EDF54532290D0E2D09C9D9FD48076F48AF1 (void);
// 0x000002F2 DynamicPrefabSpawner DynamicPrefabSpawner::get_Instance()
extern void DynamicPrefabSpawner_get_Instance_m36AC7CE906776D734C683CA154EB5C32360FE103 (void);
// 0x000002F3 System.Void DynamicPrefabSpawner::OnDestroy()
extern void DynamicPrefabSpawner_OnDestroy_m4735259261C8F3A219E988E8BF319A7E940A74EC (void);
// 0x000002F4 System.Threading.Tasks.Task`1<System.ValueTuple`2<System.Boolean,Unity.Netcode.NetworkObject>> DynamicPrefabSpawner::TrySpawnDynamicPrefabSynchronously(System.String,UnityEngine.Transform,System.Single)
extern void DynamicPrefabSpawner_TrySpawnDynamicPrefabSynchronously_mE47945473775AA77493F3A392B18B4AE838292BC (void);
// 0x000002F5 System.Void DynamicPrefabSpawner::LoadAddressableClientRpc(AddressableGUID,Unity.Netcode.ClientRpcParams)
extern void DynamicPrefabSpawner_LoadAddressableClientRpc_m9DFEE617D31EFFA3E90BE63ED719099D2632C1CB (void);
// 0x000002F6 System.Void DynamicPrefabSpawner::AcknowledgeSuccessfulPrefabLoadServerRpc(System.Int32,Unity.Netcode.ServerRpcParams)
extern void DynamicPrefabSpawner_AcknowledgeSuccessfulPrefabLoadServerRpc_m61B640057F09727F81B1E1D24B83D6684886AFA9 (void);
// 0x000002F7 System.Void DynamicPrefabSpawner::.ctor()
extern void DynamicPrefabSpawner__ctor_m4E4A4B82143EB0E9D5B7568AA3D3FCCD2F73C012 (void);
// 0x000002F8 Unity.Netcode.NetworkObject DynamicPrefabSpawner::<TrySpawnDynamicPrefabSynchronously>g__Spawn|9_0(AddressableGUID,DynamicPrefabSpawner/<>c__DisplayClass9_0&)
extern void DynamicPrefabSpawner_U3CTrySpawnDynamicPrefabSynchronouslyU3Eg__SpawnU7C9_0_m0C693D961713E5CD866FA519E31A4700152EFD8F (void);
// 0x000002F9 System.Void DynamicPrefabSpawner::<LoadAddressableClientRpc>g__Load|10_0(AddressableGUID)
extern void DynamicPrefabSpawner_U3CLoadAddressableClientRpcU3Eg__LoadU7C10_0_m4503A9D24CFF35AE281924EE95D874290E8B0447 (void);
// 0x000002FA System.Void DynamicPrefabSpawner::__initializeVariables()
extern void DynamicPrefabSpawner___initializeVariables_m8CB06D5038D080C2241CD7FBED43A8AF31147BF4 (void);
// 0x000002FB System.Void DynamicPrefabSpawner::__initializeRpcs()
extern void DynamicPrefabSpawner___initializeRpcs_mD964C13E99FFDCD98C2D4C0A07CA8F9742870E6C (void);
// 0x000002FC System.Void DynamicPrefabSpawner::__rpc_handler_1610375934(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void DynamicPrefabSpawner___rpc_handler_1610375934_mAD3467A88B31FE10394C88E064259741AAB037FF (void);
// 0x000002FD System.Void DynamicPrefabSpawner::__rpc_handler_3270584422(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void DynamicPrefabSpawner___rpc_handler_3270584422_mC653BCDC5394C010A1CCAB0E35A6204B1F6BE11E (void);
// 0x000002FE System.String DynamicPrefabSpawner::__getTypeName()
extern void DynamicPrefabSpawner___getTypeName_m486A7A4C8B4036C42484F515C3FF55A5EBC4E9D5 (void);
// 0x000002FF System.Void DynamicPrefabSpawner/<TrySpawnDynamicPrefabSynchronously>d__9::MoveNext()
extern void U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_MoveNext_m5A60F480F8432D153F33112324D84A881C8012F7 (void);
// 0x00000300 System.Void DynamicPrefabSpawner/<TrySpawnDynamicPrefabSynchronously>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_SetStateMachine_m6646A5927C420EDF0F8AA7F7D6D686905FBA611F (void);
// 0x00000301 System.Void DynamicPrefabSpawner/<<LoadAddressableClientRpc>g__Load|10_0>d::MoveNext()
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_MoveNext_m84534720935749153F37C828F1959A67C50394B5 (void);
// 0x00000302 System.Void DynamicPrefabSpawner/<<LoadAddressableClientRpc>g__Load|10_0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_SetStateMachine_mC4A20EEAFB77E59E681CD112D7AC68B391272CC3 (void);
// 0x00000303 System.Void DynamicPrefabSpawnerOld::Start()
extern void DynamicPrefabSpawnerOld_Start_m568FDF57F95A186A18352347624BF6C3E8A0C967 (void);
// 0x00000304 System.Void DynamicPrefabSpawnerOld::OnDestroy()
extern void DynamicPrefabSpawnerOld_OnDestroy_mABF481ACFFE29EC9A01CD289FD0FF4281B4EB505 (void);
// 0x00000305 System.Threading.Tasks.Task`1<Unity.Netcode.NetworkObject> DynamicPrefabSpawnerOld::SpawnDynamicPrefab(System.String,UnityEngine.Transform)
extern void DynamicPrefabSpawnerOld_SpawnDynamicPrefab_m4D6A71687B53D57F4D2DA764255A5ADBC4A8CF90 (void);
// 0x00000306 System.Void DynamicPrefabSpawnerOld::ShowHiddenObjectsToClient(System.Int32,System.UInt64)
extern void DynamicPrefabSpawnerOld_ShowHiddenObjectsToClient_m80EE8FDB7AA2D7733DDD5D5CA0279B167076FA65 (void);
// 0x00000307 System.Void DynamicPrefabSpawnerOld::LoadAddressableClientRpc(AddressableGUID,Unity.Netcode.ClientRpcParams)
extern void DynamicPrefabSpawnerOld_LoadAddressableClientRpc_mCD37E86E519B61F6E1D012520BD30BEFDFD8CF36 (void);
// 0x00000308 System.Void DynamicPrefabSpawnerOld::AcknowledgeSuccessfulPrefabLoadServerRpc(System.Int32,Unity.Netcode.ServerRpcParams)
extern void DynamicPrefabSpawnerOld_AcknowledgeSuccessfulPrefabLoadServerRpc_m232CD356B4D12D8F28470964F3117C93E5F2F8D7 (void);
// 0x00000309 System.Void DynamicPrefabSpawnerOld::.ctor()
extern void DynamicPrefabSpawnerOld__ctor_m5FE5990ED3402C5E2906978FA6600110238E29BA (void);
// 0x0000030A System.Void DynamicPrefabSpawnerOld::<LoadAddressableClientRpc>g__Load|6_0(AddressableGUID)
extern void DynamicPrefabSpawnerOld_U3CLoadAddressableClientRpcU3Eg__LoadU7C6_0_m44946B3C3C2C8158514203593B6F596353F706C2 (void);
// 0x0000030B System.Void DynamicPrefabSpawnerOld::__initializeVariables()
extern void DynamicPrefabSpawnerOld___initializeVariables_mCAAB45ED7CB411BCF42021D2E554CA305645A1E5 (void);
// 0x0000030C System.Void DynamicPrefabSpawnerOld::__initializeRpcs()
extern void DynamicPrefabSpawnerOld___initializeRpcs_m6E2F9006E195C79183192DD9A06F12C67FA955F4 (void);
// 0x0000030D System.Void DynamicPrefabSpawnerOld::__rpc_handler_3300809267(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void DynamicPrefabSpawnerOld___rpc_handler_3300809267_mB15D4F9708662014008E5DCCDD163EF4E45C9969 (void);
// 0x0000030E System.Void DynamicPrefabSpawnerOld::__rpc_handler_1977834903(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void DynamicPrefabSpawnerOld___rpc_handler_1977834903_mACF6F6FBCC5559F2919EB2BA76A8CB11FDFE3CE0 (void);
// 0x0000030F System.String DynamicPrefabSpawnerOld::__getTypeName()
extern void DynamicPrefabSpawnerOld___getTypeName_m3497A2B0044806A6F3D36192D65D6267C2AED4D5 (void);
// 0x00000310 System.Void DynamicPrefabSpawnerOld/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mCFDFE43FC78830C0EA7537108540338DB6C29920 (void);
// 0x00000311 System.Threading.Tasks.Task`1<Unity.Netcode.NetworkObject> DynamicPrefabSpawnerOld/<>c__DisplayClass4_0::<SpawnDynamicPrefab>g__Spawn|0(AddressableGUID)
extern void U3CU3Ec__DisplayClass4_0_U3CSpawnDynamicPrefabU3Eg__SpawnU7C0_mCDECFD2395BA8F993F17E29CCBD96C57D6F31345 (void);
// 0x00000312 System.Void DynamicPrefabSpawnerOld/<>c__DisplayClass4_0/<<SpawnDynamicPrefab>g__Spawn|0>d::MoveNext()
extern void U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_MoveNext_mB062C756B59D76F04BB4EF0B3C4A3704712512EC (void);
// 0x00000313 System.Void DynamicPrefabSpawnerOld/<>c__DisplayClass4_0/<<SpawnDynamicPrefab>g__Spawn|0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_SetStateMachine_mC1A34E7246CFDA1FD65C6B857FF0E8110B126386 (void);
// 0x00000314 System.Void DynamicPrefabSpawnerOld/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mA2AA687212B817970414D709DBC42FED5C085BA8 (void);
// 0x00000315 System.Boolean DynamicPrefabSpawnerOld/<>c__DisplayClass4_1::<SpawnDynamicPrefab>b__1(System.UInt64)
extern void U3CU3Ec__DisplayClass4_1_U3CSpawnDynamicPrefabU3Eb__1_mF264D4648102270E4ACC1D024338A1623AF9AB9F (void);
// 0x00000316 System.Void DynamicPrefabSpawnerOld/<SpawnDynamicPrefab>d__4::MoveNext()
extern void U3CSpawnDynamicPrefabU3Ed__4_MoveNext_mB88B52A3253E26DCA53797F7312041743C7A6A05 (void);
// 0x00000317 System.Void DynamicPrefabSpawnerOld/<SpawnDynamicPrefab>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSpawnDynamicPrefabU3Ed__4_SetStateMachine_m287475ABB1EE5F9AAF6CB4194AD5FD71B141C724 (void);
// 0x00000318 System.Void DynamicPrefabSpawnerOld/<<LoadAddressableClientRpc>g__Load|6_0>d::MoveNext()
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_MoveNext_m848CA185D81D33BB8E707503D26BEDBF3F4BFEF8 (void);
// 0x00000319 System.Void DynamicPrefabSpawnerOld/<<LoadAddressableClientRpc>g__Load|6_0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_SetStateMachine_m46CEE3262BCDF0776CD304BCF73B1715360897D2 (void);
// 0x0000031A MixedReality.Toolkit.SpatialManipulation.ObjectManipulator OwnershipController::get_ObjectManipulator()
extern void OwnershipController_get_ObjectManipulator_m61005773A3C82488D60462D0CB4F989249A5864E (void);
// 0x0000031B System.Void OwnershipController::set_ObjectManipulator(MixedReality.Toolkit.SpatialManipulation.ObjectManipulator)
extern void OwnershipController_set_ObjectManipulator_mAF273D77C60A6605F6CD10481D7BA31DA4284A8B (void);
// 0x0000031C MixedReality.Toolkit.SpatialManipulation.BoundsControl OwnershipController::get_BoundsControl()
extern void OwnershipController_get_BoundsControl_mB274C53130F3D3FD2A5A67E5A8EDE57C8AAF6444 (void);
// 0x0000031D System.Void OwnershipController::set_BoundsControl(MixedReality.Toolkit.SpatialManipulation.BoundsControl)
extern void OwnershipController_set_BoundsControl_m0E115ED5171C4002B146E7BB9DE6ECCFE1730D0F (void);
// 0x0000031E Unity.Netcode.NetworkObject OwnershipController::get_objectManipulatorTarget()
extern void OwnershipController_get_objectManipulatorTarget_m5A2C83F0CC1026FFFDBEEE88578263750AA2CB16 (void);
// 0x0000031F Unity.Netcode.NetworkObject OwnershipController::get_boundsControlTarget()
extern void OwnershipController_get_boundsControlTarget_mA9C68245BB52B763E487601E7C54A43AA718ED34 (void);
// 0x00000320 System.Void OwnershipController::Start()
extern void OwnershipController_Start_mE4F9D271F6CF8C6ACB8D640042F9930E01A64F02 (void);
// 0x00000321 System.Void OwnershipController::OnEnable()
extern void OwnershipController_OnEnable_m6FCEAF67FBC662AA4647CB907AAF44979C05C2CD (void);
// 0x00000322 System.Void OwnershipController::OnDisable()
extern void OwnershipController_OnDisable_m8F9D4F2BC664F698AB52135DB6F96B68542AF7B7 (void);
// 0x00000323 System.Void OwnershipController::OnDestroy()
extern void OwnershipController_OnDestroy_m70E8FD87D3822C72E670754DCC8955952BE8D748 (void);
// 0x00000324 System.Void OwnershipController::OnApplicationQuit()
extern void OwnershipController_OnApplicationQuit_m02F6445DF1E934173E68CA252D86A9613E3BD3B9 (void);
// 0x00000325 System.Void OwnershipController::RegisterObjectManipulatorEvents()
extern void OwnershipController_RegisterObjectManipulatorEvents_m8DA55A37B445A18CA0F6ED5670DE62B6BCB553B5 (void);
// 0x00000326 System.Void OwnershipController::DeregisterObjectManipulatorEvents()
extern void OwnershipController_DeregisterObjectManipulatorEvents_m480E784B4E75A0238AC24AC5088EE6BB3BFC95C6 (void);
// 0x00000327 System.Void OwnershipController::RegisterBoundsControlEvents()
extern void OwnershipController_RegisterBoundsControlEvents_m16725AEEE5F82B9EFCA3FB862A493A6C18655DB7 (void);
// 0x00000328 System.Void OwnershipController::DeregisterBoundsControlEvents()
extern void OwnershipController_DeregisterBoundsControlEvents_m2D6D25DDE283329D1A48A91264C931770F149097 (void);
// 0x00000329 System.Void OwnershipController::OnLastSelectExited(UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs)
extern void OwnershipController_OnLastSelectExited_m31762788BA6C3D32D17EFB254E631682BB1DBDA9 (void);
// 0x0000032A System.Void OwnershipController::OnFirstSelectEntered(UnityEngine.XR.Interaction.Toolkit.SelectEnterEventArgs)
extern void OwnershipController_OnFirstSelectEntered_mEEAA386A701B1EDDE8D7E6023FF35062AAE95992 (void);
// 0x0000032B System.Void OwnershipController::RequestOwnershipServerRpc(Unity.Netcode.NetworkObjectReference,System.UInt64)
extern void OwnershipController_RequestOwnershipServerRpc_m66484EB34B34BED2D164D96925931D1B306C11A5 (void);
// 0x0000032C System.Void OwnershipController::ReturnOwnershipServerRpc(Unity.Netcode.NetworkObjectReference)
extern void OwnershipController_ReturnOwnershipServerRpc_mE6F54C3B7C2F2DBF1CCC5B388749F42C6EC2B9BA (void);
// 0x0000032D System.Void OwnershipController::.ctor()
extern void OwnershipController__ctor_mDBDE5993C25E1C267E2EADC540B1452ECA748CE5 (void);
// 0x0000032E System.Void OwnershipController::__initializeVariables()
extern void OwnershipController___initializeVariables_m2CA9521F217BFD208AAF7C08EE78419EF4B4A75D (void);
// 0x0000032F System.Void OwnershipController::__initializeRpcs()
extern void OwnershipController___initializeRpcs_m5A9703E3EB4C724D0EA9DB10311C69B3C1672E21 (void);
// 0x00000330 System.Void OwnershipController::__rpc_handler_977887190(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void OwnershipController___rpc_handler_977887190_m75BDF3C3283617B0543D428A8AB6833F70F6E774 (void);
// 0x00000331 System.Void OwnershipController::__rpc_handler_3349833540(Unity.Netcode.NetworkBehaviour,Unity.Netcode.FastBufferReader,Unity.Netcode.__RpcParams)
extern void OwnershipController___rpc_handler_3349833540_m6CEA80AE24F5B2F1AC6A223524C4FEAFEC4E1E67 (void);
// 0x00000332 System.String OwnershipController::__getTypeName()
extern void OwnershipController___getTypeName_m3CA781E07A91B6250E23557630123FF059C711D5 (void);
// 0x00000333 Unity.Services.Lobbies.Models.Lobby ActiveSessionManager::get_activeRemoteLobby()
extern void ActiveSessionManager_get_activeRemoteLobby_m3B30BCE0B63AB9B49CF326979A519470C43A7DD4 (void);
// 0x00000334 LocalLobby ActiveSessionManager::get_activeLocalLobby()
extern void ActiveSessionManager_get_activeLocalLobby_m497D720DBCCE2EA11BEBDE6C4306A0BC05D4FC54 (void);
// 0x00000335 System.Boolean ActiveSessionManager::get_SessionActive()
extern void ActiveSessionManager_get_SessionActive_m5D1646B17E67801AEBD403A83799C730C6367DAA (void);
// 0x00000336 System.Boolean ActiveSessionManager::get_IsHost()
extern void ActiveSessionManager_get_IsHost_mC7B96E2CECE4A0D9CCA9DCFC89BABBC6A95E0E0F (void);
// 0x00000337 System.Boolean ActiveSessionManager::get_IsRemote()
extern void ActiveSessionManager_get_IsRemote_m7AFD530EE86FFC411C06DC6F7A3EE1F0E7F2B40B (void);
// 0x00000338 LobbyInfo ActiveSessionManager::get_activeLobbyInfo()
extern void ActiveSessionManager_get_activeLobbyInfo_m53926F408AB7C309367B216A9B7AF24C41B9A098 (void);
// 0x00000339 System.Void ActiveSessionManager::Start()
extern void ActiveSessionManager_Start_m1BB3409145C55D4A61622F60A47B67218724AD1C (void);
// 0x0000033A System.Void ActiveSessionManager::StartSession(LobbyConfiguration)
extern void ActiveSessionManager_StartSession_m24C41E3E45BF550F6D95A398115BDCD6ED93E4A9 (void);
// 0x0000033B System.Void ActiveSessionManager::StartLocalSession(LobbyConfiguration)
extern void ActiveSessionManager_StartLocalSession_m08D4649D25A7D4617F33B57DE419BCB148DF0A2D (void);
// 0x0000033C System.Void ActiveSessionManager::StartRemoteSession(LobbyConfiguration)
extern void ActiveSessionManager_StartRemoteSession_m6A92962E515186677BE7BA8404BD100806AA1C6C (void);
// 0x0000033D System.Void ActiveSessionManager::JoinLocalSession(LocalLobby)
extern void ActiveSessionManager_JoinLocalSession_mB2C6537CE273BD38ADC36A91F329B1B7B8059FFB (void);
// 0x0000033E System.Void ActiveSessionManager::JoinRemoteSession(Unity.Services.Lobbies.Models.Lobby)
extern void ActiveSessionManager_JoinRemoteSession_m759E806864C402974AE1BF6C5B9A698E5BBFBB2D (void);
// 0x0000033F System.Void ActiveSessionManager::ExitSession()
extern void ActiveSessionManager_ExitSession_m36B16BF66C83C4DA7454D3A2F28376C021B55D94 (void);
// 0x00000340 System.Void ActiveSessionManager::TransferHost(System.String)
extern void ActiveSessionManager_TransferHost_mB2FF473C23D0C790F06E11E5EB857D2566AB9CA5 (void);
// 0x00000341 System.Void ActiveSessionManager::OnLocalSessionUpdated(LocalLobby)
extern void ActiveSessionManager_OnLocalSessionUpdated_m56997287A2B4F93C17B0D937A1D85ED2368713D2 (void);
// 0x00000342 System.Void ActiveSessionManager::OnRemoteSessionUpdated(Unity.Services.Lobbies.Models.Lobby)
extern void ActiveSessionManager_OnRemoteSessionUpdated_mD66FC6DC917406087E31B7A5A8BD3310E32FB651 (void);
// 0x00000343 System.Void ActiveSessionManager::KickPlayer(System.String)
extern void ActiveSessionManager_KickPlayer_m7B67744E70BF11CA7B7D9AA741AB7F391E1462F5 (void);
// 0x00000344 System.Threading.Tasks.Task ActiveSessionManager::ShutdownNetworkManager()
extern void ActiveSessionManager_ShutdownNetworkManager_mE293CEF418420305AF0E931F10AF556DCF688B6C (void);
// 0x00000345 UnityEngine.Vector3 ActiveSessionManager::get_anchorPosition()
extern void ActiveSessionManager_get_anchorPosition_mAF9CB4EAE75213EB77672CE495DEDD0B2ED7950D (void);
// 0x00000346 System.Void ActiveSessionManager::LocateAnchor(System.String)
extern void ActiveSessionManager_LocateAnchor_mDED0100CD00C439D517EE77D44F4DFC46CBE0C94 (void);
// 0x00000347 System.Void ActiveSessionManager::AnchorLocated(System.Object,Microsoft.Azure.SpatialAnchors.AnchorLocatedEventArgs)
extern void ActiveSessionManager_AnchorLocated_m9E8C44E914A28106160CE0E958EA3E02A7A95D47 (void);
// 0x00000348 System.Threading.Tasks.Task ActiveSessionManager::PlaceLocalAnchor()
extern void ActiveSessionManager_PlaceLocalAnchor_mD92CA05EFE6C1CF9CFE9084DA402A814E6DE46C0 (void);
// 0x00000349 System.Threading.Tasks.Task`1<Microsoft.Azure.SpatialAnchors.CloudSpatialAnchor> ActiveSessionManager::PlaceRemoteAnchor()
extern void ActiveSessionManager_PlaceRemoteAnchor_m23F7008E0273138807F67E58150E2E20A1986BDB (void);
// 0x0000034A System.Threading.Tasks.Task`1<System.String> ActiveSessionManager::CreateRelay(System.Int32)
extern void ActiveSessionManager_CreateRelay_mD80CC991E27E6E33A2A9CCFD062E52AA95CEE80F (void);
// 0x0000034B System.Threading.Tasks.Task ActiveSessionManager::JoinRelay(System.String)
extern void ActiveSessionManager_JoinRelay_mE20D85053EB6DC7565622FDE0CA8ECA987355FA3 (void);
// 0x0000034C System.Void ActiveSessionManager::.ctor()
extern void ActiveSessionManager__ctor_mAB9D922A4DA294AE7A3AD6F7972B5A4812AE5F29 (void);
// 0x0000034D System.Threading.Tasks.Task ActiveSessionManager::<ExitSession>b__33_0()
extern void ActiveSessionManager_U3CExitSessionU3Eb__33_0_m54B1F2C38833E6398B00139513BCF7C806D07543 (void);
// 0x0000034E System.Void ActiveSessionManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m6AB018065D4872C7AC56381A4B27BD82618095C0 (void);
// 0x0000034F System.Void ActiveSessionManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mA585605B0EE44C1169C1169EAA497D95AA3D76F5 (void);
// 0x00000350 System.Void ActiveSessionManager/<>c::<Start>b__27_0(System.Object,Microsoft.Azure.SpatialAnchors.OnLogDebugEventArgs)
extern void U3CU3Ec_U3CStartU3Eb__27_0_mC88780AAC375893E8A0731E3D0AEFF2BF51DABAD (void);
// 0x00000351 System.Void ActiveSessionManager/<>c::<Start>b__27_1(System.Object,Microsoft.Azure.SpatialAnchors.SessionErrorEventArgs)
extern void U3CU3Ec_U3CStartU3Eb__27_1_mCD2924BE4C88F194F38E25A15D779CC183729BCF (void);
// 0x00000352 System.Void ActiveSessionManager/<StartLocalSession>d__29::MoveNext()
extern void U3CStartLocalSessionU3Ed__29_MoveNext_mC11450688E2D0A5ECEF84F28F37829DA0017B62B (void);
// 0x00000353 System.Void ActiveSessionManager/<StartLocalSession>d__29::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartLocalSessionU3Ed__29_SetStateMachine_mBC67BDED374E292E47757E00BB3F26D1A870B2BD (void);
// 0x00000354 System.Void ActiveSessionManager/<StartRemoteSession>d__30::MoveNext()
extern void U3CStartRemoteSessionU3Ed__30_MoveNext_mB8D0CBAC544812F856C95679905AD6F1AA41FA97 (void);
// 0x00000355 System.Void ActiveSessionManager/<StartRemoteSession>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartRemoteSessionU3Ed__30_SetStateMachine_mB4894FD3F5D0E09A81BE6DC98C304351981276C7 (void);
// 0x00000356 System.Void ActiveSessionManager/<JoinLocalSession>d__31::MoveNext()
extern void U3CJoinLocalSessionU3Ed__31_MoveNext_m0DE55FB91C1D2392FFD73CD6C4E4DF93234C3C1E (void);
// 0x00000357 System.Void ActiveSessionManager/<JoinLocalSession>d__31::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinLocalSessionU3Ed__31_SetStateMachine_m663F267133CBC463162F19434B82D50EDFD16E59 (void);
// 0x00000358 System.Void ActiveSessionManager/<JoinRemoteSession>d__32::MoveNext()
extern void U3CJoinRemoteSessionU3Ed__32_MoveNext_mB0BF725FBA739BA3411520ECB2EEC0D04487D2E0 (void);
// 0x00000359 System.Void ActiveSessionManager/<JoinRemoteSession>d__32::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinRemoteSessionU3Ed__32_SetStateMachine_mA2A614F5864911C4EC4866301D52DB8BF0CA9F5C (void);
// 0x0000035A System.Void ActiveSessionManager/<ExitSession>d__33::MoveNext()
extern void U3CExitSessionU3Ed__33_MoveNext_m97083D94FFDB66ABE557DA3F100E8B592B25493D (void);
// 0x0000035B System.Void ActiveSessionManager/<ExitSession>d__33::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CExitSessionU3Ed__33_SetStateMachine_m2F34597B460DEEA622D677CE0C816C2A63589AE3 (void);
// 0x0000035C System.Void ActiveSessionManager/<TransferHost>d__34::MoveNext()
extern void U3CTransferHostU3Ed__34_MoveNext_m0EF821494E6C1BDE589B6762779195861AB9DAA4 (void);
// 0x0000035D System.Void ActiveSessionManager/<TransferHost>d__34::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTransferHostU3Ed__34_SetStateMachine_mCCD291FA1D801B61D8A57C3E1C076A041BA21AD7 (void);
// 0x0000035E System.Void ActiveSessionManager/<ShutdownNetworkManager>d__38::MoveNext()
extern void U3CShutdownNetworkManagerU3Ed__38_MoveNext_m3CFF850A83E2614256D3D33EE12BFD67B9DABA7D (void);
// 0x0000035F System.Void ActiveSessionManager/<ShutdownNetworkManager>d__38::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CShutdownNetworkManagerU3Ed__38_SetStateMachine_m56E3983D843FD13B901ED58E2C2E52689DF3B836 (void);
// 0x00000360 System.Void ActiveSessionManager/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_mFD5B2A1C7CFE02BE8FC8ED10CB9F13B13B1FA074 (void);
// 0x00000361 System.Void ActiveSessionManager/<>c__DisplayClass42_0::<AnchorLocated>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CAnchorLocatedU3Eb__0_mDB02FCD59786E1BCB853C4D57958FDD99606B9F4 (void);
// 0x00000362 System.Void ActiveSessionManager/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_mF009B9C6CCE3A3E787BAFBAC8E9962CC2A26087B (void);
// 0x00000363 System.Void ActiveSessionManager/<>c__DisplayClass43_0::<PlaceLocalAnchor>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CPlaceLocalAnchorU3Eb__0_m176386270C3E4ADEAF792DCEB01A0527EDD55B6C (void);
// 0x00000364 System.Void ActiveSessionManager/<PlaceLocalAnchor>d__43::MoveNext()
extern void U3CPlaceLocalAnchorU3Ed__43_MoveNext_mE4857E80B20205CE4CADC54540E85D7B17FB5FBE (void);
// 0x00000365 System.Void ActiveSessionManager/<PlaceLocalAnchor>d__43::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPlaceLocalAnchorU3Ed__43_SetStateMachine_mA8ECAE99CD4D1459DC3423061E32F1BD4482A10E (void);
// 0x00000366 System.Void ActiveSessionManager/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m8351ACBD950C7844C7C3E8D73B863CF944EAC29A (void);
// 0x00000367 System.Void ActiveSessionManager/<>c__DisplayClass44_0::<PlaceRemoteAnchor>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CPlaceRemoteAnchorU3Eb__0_mBA9F18A7B28E91ED699C1450CA073502B5DCE4DC (void);
// 0x00000368 System.Void ActiveSessionManager/<PlaceRemoteAnchor>d__44::MoveNext()
extern void U3CPlaceRemoteAnchorU3Ed__44_MoveNext_m1E6AAB357B1316C8EE163F5657501D8D16A03862 (void);
// 0x00000369 System.Void ActiveSessionManager/<PlaceRemoteAnchor>d__44::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPlaceRemoteAnchorU3Ed__44_SetStateMachine_m1CC20A722C021B1ACE4A330CA4A77EC73B5F4071 (void);
// 0x0000036A System.Void ActiveSessionManager/<CreateRelay>d__45::MoveNext()
extern void U3CCreateRelayU3Ed__45_MoveNext_mB5970CAAA03FD31E5EAD2512DDCC334A8601B66D (void);
// 0x0000036B System.Void ActiveSessionManager/<CreateRelay>d__45::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateRelayU3Ed__45_SetStateMachine_m258A3525393C6ED7F1FC4541E3B98018658517D4 (void);
// 0x0000036C System.Void ActiveSessionManager/<JoinRelay>d__46::MoveNext()
extern void U3CJoinRelayU3Ed__46_MoveNext_m89BC4291D60E5009914A2969424C0919BA6A5AB4 (void);
// 0x0000036D System.Void ActiveSessionManager/<JoinRelay>d__46::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinRelayU3Ed__46_SetStateMachine_m40C03F5C232047EBC7397C2CE181CF6C8F794458 (void);
// 0x0000036E System.Void LobbyConfiguration::.ctor()
extern void LobbyConfiguration__ctor_m43F7688305E45E3137EAA40FD6B19DAD7EE61C09 (void);
// 0x0000036F System.Void LobbyConfiguration::.ctor(Unity.Services.Lobbies.Models.Lobby)
extern void LobbyConfiguration__ctor_m69F0D77EFA68BF87DED2CC0E2DBEA4BEDA49B868 (void);
// 0x00000370 System.Void LobbyConfiguration::.ctor(LocalLobby)
extern void LobbyConfiguration__ctor_mD3B41416AA2EA4065F11E6A6E03EB41596BAFC6B (void);
// 0x00000371 System.Int32 LobbyInfo::get_currentParticipantCount()
extern void LobbyInfo_get_currentParticipantCount_m7B6EB91D24BD2B4802AAC578CE8A46E2904A7793 (void);
// 0x00000372 System.Void LobbyInfo::.ctor(LocalLobby)
extern void LobbyInfo__ctor_mAE6536178812C5A7943257B815CB480371F3EE21 (void);
// 0x00000373 System.Void LobbyInfo::.ctor(Unity.Services.Lobbies.Models.Lobby)
extern void LobbyInfo__ctor_m54D85F5829123B20FCE81D8D56BB4297202C1C8D (void);
// 0x00000374 System.Collections.Generic.Dictionary`2<System.String,Unity.Services.Lobbies.Models.Lobby> LobbyDiscovery::get_RemoteLobbies()
extern void LobbyDiscovery_get_RemoteLobbies_mB27D8B3A5B6E53DD14E88DF3F1F1C9F9A1C50FBD (void);
// 0x00000375 System.Void LobbyDiscovery::Update()
extern void LobbyDiscovery_Update_mEABEBFCB69353C571A3F71161A2267E0F467EB9F (void);
// 0x00000376 System.Void LobbyDiscovery::HandleRefreshLobbyList()
extern void LobbyDiscovery_HandleRefreshLobbyList_m8DD3230A0892090D4661C14DAC998B671A643732 (void);
// 0x00000377 System.Void LobbyDiscovery::RefreshLobbyList()
extern void LobbyDiscovery_RefreshLobbyList_m7C56BCA481FA256B69D6DD79F4BCB9C5972DE7BE (void);
// 0x00000378 System.Void LobbyDiscovery::StartSearching()
extern void LobbyDiscovery_StartSearching_mE6FE20548623554A0965B92DC18D87FA1366C7CA (void);
// 0x00000379 System.Void LobbyDiscovery::StopSearching()
extern void LobbyDiscovery_StopSearching_m81CA8162A26DD29E5D9367C16A400EDE7D6C1CCD (void);
// 0x0000037A System.Void LobbyDiscovery::.ctor()
extern void LobbyDiscovery__ctor_m1B9AB1120F9E1610853BBD941F40E0897F6E542C (void);
// 0x0000037B System.Void LobbyDiscovery/<RefreshLobbyList>d__9::MoveNext()
extern void U3CRefreshLobbyListU3Ed__9_MoveNext_m25444895F8ECC7ABEF7D86B5EE6C0714CDDDA59D (void);
// 0x0000037C System.Void LobbyDiscovery/<RefreshLobbyList>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRefreshLobbyListU3Ed__9_SetStateMachine_mACA4060E704D5DF0652FF515F3C6076290027169 (void);
// 0x0000037D System.Void LobbyDiscovery/<StartSearching>d__10::MoveNext()
extern void U3CStartSearchingU3Ed__10_MoveNext_m1BC4133C328BB76EBDA027DB0286198AC8BB3865 (void);
// 0x0000037E System.Void LobbyDiscovery/<StartSearching>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartSearchingU3Ed__10_SetStateMachine_mFD2AF4BC5CC765B4DD70B09BA164991BF0E4813E (void);
// 0x0000037F System.Void LobbyDiscovery/<StopSearching>d__11::MoveNext()
extern void U3CStopSearchingU3Ed__11_MoveNext_m94A73BD34E875E523A3106B3A35850EF49931757 (void);
// 0x00000380 System.Void LobbyDiscovery/<StopSearching>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStopSearchingU3Ed__11_SetStateMachine_m2BB6F3CBE7BB647554A07CBA9B7471EA9186C8CA (void);
// 0x00000381 System.Collections.Generic.Dictionary`2<System.String,LocalLobby> LobbyListManager::get_LocalLobbies()
extern void LobbyListManager_get_LocalLobbies_mB9469211838751A138915982DD58FC39922EE8C2 (void);
// 0x00000382 System.Collections.Generic.Dictionary`2<System.String,Unity.Services.Lobbies.Models.Lobby> LobbyListManager::get_RemoteLobbies()
extern void LobbyListManager_get_RemoteLobbies_m2323FFB98BA8EA23AE709C56F5347FCEBF137FAE (void);
// 0x00000383 System.Void LobbyListManager::add_LocalLobbyAdded(System.Action`1<LocalLobby>)
extern void LobbyListManager_add_LocalLobbyAdded_m02469213AA76BFAB5240A75AD378EC36BB610130 (void);
// 0x00000384 System.Void LobbyListManager::remove_LocalLobbyAdded(System.Action`1<LocalLobby>)
extern void LobbyListManager_remove_LocalLobbyAdded_mAB666CD7E510A1BA6059BC7785AE0EB546DC061D (void);
// 0x00000385 System.Void LobbyListManager::add_LocalLobbyRemoved(System.Action`1<LocalLobby>)
extern void LobbyListManager_add_LocalLobbyRemoved_m4CE8BC60A048B7F48D8BEAA2EA6589DEE05AEEA1 (void);
// 0x00000386 System.Void LobbyListManager::remove_LocalLobbyRemoved(System.Action`1<LocalLobby>)
extern void LobbyListManager_remove_LocalLobbyRemoved_m03FB878E18924F0B0D24B9DC92E29AD9696B2306 (void);
// 0x00000387 System.Void LobbyListManager::add_LocalLobbyUpdated(System.Action`1<LocalLobby>)
extern void LobbyListManager_add_LocalLobbyUpdated_m44EB684401AD4F80ABC7596F9DB0C09C16BAF9D3 (void);
// 0x00000388 System.Void LobbyListManager::remove_LocalLobbyUpdated(System.Action`1<LocalLobby>)
extern void LobbyListManager_remove_LocalLobbyUpdated_m05FF38C0E2827EF8999A143D24A1360B6B3CE24D (void);
// 0x00000389 System.Void LobbyListManager::add_RemoteLobbyAdded(System.Action`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_add_RemoteLobbyAdded_m5B247CEBDCA8176A5962C5CDC8369FDD11305038 (void);
// 0x0000038A System.Void LobbyListManager::remove_RemoteLobbyAdded(System.Action`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_remove_RemoteLobbyAdded_m773CE5894EBA6F4E6F8B4505A165787A45D0FDF3 (void);
// 0x0000038B System.Void LobbyListManager::add_RemoteLobbyRemoved(System.Action`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_add_RemoteLobbyRemoved_mA5AEC824110F9C2946BFE5921488D7DD3FE402F7 (void);
// 0x0000038C System.Void LobbyListManager::remove_RemoteLobbyRemoved(System.Action`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_remove_RemoteLobbyRemoved_m57522F54BE792AC1B76FC12982E0C8DA4C7F579D (void);
// 0x0000038D System.Void LobbyListManager::add_RemoteLobbyUpdated(System.Action`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_add_RemoteLobbyUpdated_mBC15B68780E5849CB6AED1DE1191258EDEB20CE5 (void);
// 0x0000038E System.Void LobbyListManager::remove_RemoteLobbyUpdated(System.Action`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_remove_RemoteLobbyUpdated_m872BF35DAFC3D4CA54411435EF0401DBB52A82CA (void);
// 0x0000038F System.Void LobbyListManager::Start()
extern void LobbyListManager_Start_m1200238E56A483492F46770B1ABF15447A6AD9A4 (void);
// 0x00000390 System.Void LobbyListManager::StartLocalLobbySearch()
extern void LobbyListManager_StartLocalLobbySearch_m9EEA20B6E9378326DE92AA4A0B6A1C2F74B4796C (void);
// 0x00000391 System.Void LobbyListManager::StartRemoteLobbySearch()
extern void LobbyListManager_StartRemoteLobbySearch_mBB781225B8640925B72525FEA65C09E5E91F894A (void);
// 0x00000392 System.Void LobbyListManager::OnEnable()
extern void LobbyListManager_OnEnable_m59C1CA96EABE366CCF7FC26175C7DB1C5D588AA6 (void);
// 0x00000393 System.Void LobbyListManager::OnDisable()
extern void LobbyListManager_OnDisable_m70942B3C6435A2AFBE4C641112A0CB067D4BAAD0 (void);
// 0x00000394 System.Void LobbyListManager::RemoveAllLobbies()
extern void LobbyListManager_RemoveAllLobbies_m86EB4FB306368D74E4FD35FE43A4F82BA7D1A83A (void);
// 0x00000395 System.Void LobbyListManager::UpdateRemoteLobbies(System.Collections.Generic.List`1<Unity.Services.Lobbies.Models.Lobby>)
extern void LobbyListManager_UpdateRemoteLobbies_m6F2B726A97635EDD04B08A3457C880D0386D0826 (void);
// 0x00000396 System.Void LobbyListManager::AddOrUpdateLocalLobby(LocalLobby)
extern void LobbyListManager_AddOrUpdateLocalLobby_m5B4155CDE6EE9329F2BDC0568E50C14F0FE27D4D (void);
// 0x00000397 System.Void LobbyListManager::RemoveLocalLobby(LocalLobby)
extern void LobbyListManager_RemoveLocalLobby_m9C85FB21E0713E0E821FAAEDCB3B6C0722DFBA95 (void);
// 0x00000398 System.Void LobbyListManager::.ctor()
extern void LobbyListManager__ctor_m04CA6AF124044DC0FD9DCFCFC5B09CE568EB6C5B (void);
// 0x00000399 LobbyManager LobbyManager::get_Instance()
extern void LobbyManager_get_Instance_m67B14E9BC9AE2C192CEB05F7CCACF44A351D0A4C (void);
// 0x0000039A System.Void LobbyManager::set_Instance(LobbyManager)
extern void LobbyManager_set_Instance_m3423E6546EF52A57B2E855196DE88DDA85224C16 (void);
// 0x0000039B System.Void LobbyManager::Awake()
extern void LobbyManager_Awake_m55E6DF51682162CFF126F53F17428BDA50BB1734 (void);
// 0x0000039C System.Void LobbyManager::Update()
extern void LobbyManager_Update_m2EDDF5EAAD4D4C87A19CF970492F05044168A99C (void);
// 0x0000039D System.Void LobbyManager::HandleLobbyHeartbeat()
extern void LobbyManager_HandleLobbyHeartbeat_mEB88C36CAFC16C7E034C642187C545035957CA3A (void);
// 0x0000039E System.Void LobbyManager::HandleLobbyPolling()
extern void LobbyManager_HandleLobbyPolling_m451863141B4D930ABACEF5BBF82DCF79ACC27B36 (void);
// 0x0000039F System.Boolean LobbyManager::IsPlayerInLobby()
extern void LobbyManager_IsPlayerInLobby_mD65F8792B7455B02864616AC569BBAF235BCB192 (void);
// 0x000003A0 Unity.Services.Lobbies.Models.Lobby LobbyManager::get_JoinedLobby()
extern void LobbyManager_get_JoinedLobby_m50AB58743CC6D185A02B84741257F6FA918129CC (void);
// 0x000003A1 System.Boolean LobbyManager::IsLobbyHost()
extern void LobbyManager_IsLobbyHost_m5CF768BAB88F769DBCBBAC5069610F725B949617 (void);
// 0x000003A2 Unity.Services.Lobbies.Models.Player LobbyManager::GetPlayer()
extern void LobbyManager_GetPlayer_m00825770FA1DCA8AA39FB605F1F734181F7E75E6 (void);
// 0x000003A3 System.Void LobbyManager::UpdatePlayerName(System.String)
extern void LobbyManager_UpdatePlayerName_mBF1471DF3B589358B0DCD5821CA880B46F2C00FD (void);
// 0x000003A4 System.Threading.Tasks.Task`1<Unity.Services.Lobbies.Models.Lobby> LobbyManager::CreateLobby(System.String,System.String,System.Int32,System.Boolean)
extern void LobbyManager_CreateLobby_m31BCF6482AB561BA784A2F6239B7176D539E1199 (void);
// 0x000003A5 System.Threading.Tasks.Task LobbyManager::JoinLobbyByCode(System.String)
extern void LobbyManager_JoinLobbyByCode_m8E911B83D6ECFD3980BCB064AE8E2576B30E0775 (void);
// 0x000003A6 System.Threading.Tasks.Task LobbyManager::JoinLobby(Unity.Services.Lobbies.Models.Lobby)
extern void LobbyManager_JoinLobby_mC60BCB3F5109631B879A092BCD68AC60AE1E3184 (void);
// 0x000003A7 System.Void LobbyManager::LeaveLobby()
extern void LobbyManager_LeaveLobby_m5EB7FB963A346DD0F49EB123D9D0A52DE46A1AFF (void);
// 0x000003A8 System.Void LobbyManager::KickPlayer(System.String)
extern void LobbyManager_KickPlayer_m87D27B7251D72C637C550750C58D895F15BDE2DD (void);
// 0x000003A9 System.Void LobbyManager::DestroyLobby()
extern void LobbyManager_DestroyLobby_m76A0DB6357794347ED2448587E1669AC8C368519 (void);
// 0x000003AA System.Void LobbyManager::TransferLobbyHost(System.String)
extern void LobbyManager_TransferLobbyHost_mA521E76A63B6F0EF92A5B64746511F04789EE897 (void);
// 0x000003AB System.Void LobbyManager::.ctor()
extern void LobbyManager__ctor_m1A4D2FDD6CF3611918EF889457B0D4ABA7384C4B (void);
// 0x000003AC System.Void LobbyManager/<HandleLobbyHeartbeat>d__16::MoveNext()
extern void U3CHandleLobbyHeartbeatU3Ed__16_MoveNext_mABB2068123612A856345C26D9F29469C08FB7650 (void);
// 0x000003AD System.Void LobbyManager/<HandleLobbyHeartbeat>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleLobbyHeartbeatU3Ed__16_SetStateMachine_mE54FAB3A73B11C01A066A9E101317C15B252344D (void);
// 0x000003AE System.Void LobbyManager/<HandleLobbyPolling>d__17::MoveNext()
extern void U3CHandleLobbyPollingU3Ed__17_MoveNext_mF41C7C2B380BEADF3FFC3FDB0F29F927AEC162AE (void);
// 0x000003AF System.Void LobbyManager/<HandleLobbyPolling>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleLobbyPollingU3Ed__17_SetStateMachine_m85B92A652AAA0C42BC81732DE2208C91AB2837BE (void);
// 0x000003B0 System.Void LobbyManager/<UpdatePlayerName>d__23::MoveNext()
extern void U3CUpdatePlayerNameU3Ed__23_MoveNext_mB6A64351A0BECD3340DCF230846172D90C0B87DA (void);
// 0x000003B1 System.Void LobbyManager/<UpdatePlayerName>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUpdatePlayerNameU3Ed__23_SetStateMachine_m46E12BFD3917F99E43B9CE9627E19EB3B33D0FFC (void);
// 0x000003B2 System.Void LobbyManager/<CreateLobby>d__24::MoveNext()
extern void U3CCreateLobbyU3Ed__24_MoveNext_mEBAEC77D233648DC14816A0220460FBC34FA4E4F (void);
// 0x000003B3 System.Void LobbyManager/<CreateLobby>d__24::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateLobbyU3Ed__24_SetStateMachine_m028997C9F1E7FBA40ADCDE13D7EA17355CEADD6D (void);
// 0x000003B4 System.Void LobbyManager/<JoinLobbyByCode>d__25::MoveNext()
extern void U3CJoinLobbyByCodeU3Ed__25_MoveNext_m6F9BAE91C9FE08A92E3AD62D2FD2AB27583B3372 (void);
// 0x000003B5 System.Void LobbyManager/<JoinLobbyByCode>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinLobbyByCodeU3Ed__25_SetStateMachine_mB5FD19586A74F53A9ED43337FA2256DCE8412ACC (void);
// 0x000003B6 System.Void LobbyManager/<JoinLobby>d__26::MoveNext()
extern void U3CJoinLobbyU3Ed__26_MoveNext_m9F9C11809B250A10242DEE4F2A028B96DE79410E (void);
// 0x000003B7 System.Void LobbyManager/<JoinLobby>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinLobbyU3Ed__26_SetStateMachine_m9DE0AD576552F531E92DCB603535B298396E931D (void);
// 0x000003B8 System.Void LobbyManager/<LeaveLobby>d__27::MoveNext()
extern void U3CLeaveLobbyU3Ed__27_MoveNext_m8A73C2A8A0C45CD7156A42D1CAE243AB5D79AD78 (void);
// 0x000003B9 System.Void LobbyManager/<LeaveLobby>d__27::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLeaveLobbyU3Ed__27_SetStateMachine_mF80F0204D2765208BC35273C8D4E2A17E6A7D353 (void);
// 0x000003BA System.Void LobbyManager/<KickPlayer>d__28::MoveNext()
extern void U3CKickPlayerU3Ed__28_MoveNext_m9872BE9C0490DE8C8A946F27CC0FEBA09C94E50F (void);
// 0x000003BB System.Void LobbyManager/<KickPlayer>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CKickPlayerU3Ed__28_SetStateMachine_m61E87687F064F4D623EEE7A77240E9DF349D6503 (void);
// 0x000003BC System.Void LobbyManager/<DestroyLobby>d__29::MoveNext()
extern void U3CDestroyLobbyU3Ed__29_MoveNext_m34FDFD43B2A080EC6EE88636C9D733CB5C81FAB2 (void);
// 0x000003BD System.Void LobbyManager/<DestroyLobby>d__29::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDestroyLobbyU3Ed__29_SetStateMachine_m3D8DB8CDACA3D15D72B442DD617A13DFE56738BF (void);
// 0x000003BE System.Void LobbyManager/<TransferLobbyHost>d__30::MoveNext()
extern void U3CTransferLobbyHostU3Ed__30_MoveNext_m4FC6D9D4AA9383060CA5DDB317E3E1E686DA8877 (void);
// 0x000003BF System.Void LobbyManager/<TransferLobbyHost>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTransferLobbyHostU3Ed__30_SetStateMachine_m9B00D73A248FE24B300127054BC90CC69CBE10CE (void);
// 0x000003C0 System.Int32 LocalLobby::get_participantCount()
extern void LocalLobby_get_participantCount_m11F6F03A1A434513FA044957AFF3775D5D428A77 (void);
// 0x000003C1 System.Void LocalLobby::.ctor(System.Byte[])
extern void LocalLobby__ctor_mD89412120EC9014E50088BA840D416C464727CA8 (void);
// 0x000003C2 System.Void LocalLobby::.ctor()
extern void LocalLobby__ctor_mFC023660723363DA3BD932F28D6C75EEB95A5566 (void);
// 0x000003C3 System.Void LocalLobby::.ctor(LobbyConfiguration)
extern void LocalLobby__ctor_m276E0B5D348ACC467B55CE8BB2679BAEABA080D4 (void);
// 0x000003C4 System.Net.IPEndPoint LocalLobby::get_endpoint()
extern void LocalLobby_get_endpoint_m7F7D1ECE978ED88E4764CB934E95EFAACD0919EC (void);
// 0x000003C5 System.Void LocalLobby::AddPlayer(LocalPlayer)
extern void LocalLobby_AddPlayer_m83E6EF9DAE22890A0A135DC6D9F2A798B7455586 (void);
// 0x000003C6 System.Boolean LocalLobbyDiscovery::get_IsAdvertising()
extern void LocalLobbyDiscovery_get_IsAdvertising_mDCC931CCC6798A7D7FF0B95956B3FFF264795218 (void);
// 0x000003C7 System.Boolean LocalLobbyDiscovery::get_IsSearching()
extern void LocalLobbyDiscovery_get_IsSearching_mA43F672E4891435A585150B34714E64720020CDA (void);
// 0x000003C8 System.Void LocalLobbyDiscovery::add_ServerFoundCallback(System.Action`2<System.Net.IPEndPoint,LobbyConfiguration>)
extern void LocalLobbyDiscovery_add_ServerFoundCallback_mA73150E8CE95383A28D624C75221066E0FDD00D3 (void);
// 0x000003C9 System.Void LocalLobbyDiscovery::remove_ServerFoundCallback(System.Action`2<System.Net.IPEndPoint,LobbyConfiguration>)
extern void LocalLobbyDiscovery_remove_ServerFoundCallback_m703B00475FDEDD8102F0398B5E4FEF33B25B4523 (void);
// 0x000003CA System.Void LocalLobbyDiscovery::OnDisable()
extern void LocalLobbyDiscovery_OnDisable_mF2534D5AE8D8D4013E99F6E4CF5841B9FF457089 (void);
// 0x000003CB System.Void LocalLobbyDiscovery::OnDestroy()
extern void LocalLobbyDiscovery_OnDestroy_m25E85F205AE69B322CC2BCD3C0C2730E770F1B99 (void);
// 0x000003CC System.Void LocalLobbyDiscovery::OnApplicationQuit()
extern void LocalLobbyDiscovery_OnApplicationQuit_m602708B602B670542F00A28184124C8D9E6C8B6C (void);
// 0x000003CD System.Void LocalLobbyDiscovery::StartAdvertisingServer()
extern void LocalLobbyDiscovery_StartAdvertisingServer_m6BF503D244E3F5EAAB6E41FE65045374E8E2AA90 (void);
// 0x000003CE System.Void LocalLobbyDiscovery::StopAdvertisingServer()
extern void LocalLobbyDiscovery_StopAdvertisingServer_m226D3BFD60384FA595D70710D8D368B36CF9BCE5 (void);
// 0x000003CF System.Void LocalLobbyDiscovery::AdvertiseServerAsync(LobbyConfiguration)
extern void LocalLobbyDiscovery_AdvertiseServerAsync_mDFC23F6187F0CF9BD770C1DF878E6C1BCECC9A49 (void);
// 0x000003D0 System.Void LocalLobbyDiscovery::StartSearchingForServers()
extern void LocalLobbyDiscovery_StartSearchingForServers_m80F7A7B8F2D57C3A2D65A3C5025E838246908D90 (void);
// 0x000003D1 System.Void LocalLobbyDiscovery::StopSearchingForServers()
extern void LocalLobbyDiscovery_StopSearchingForServers_m52591539302BC3C18E5639F5F7EFEBDCC0C76CB9 (void);
// 0x000003D2 System.Void LocalLobbyDiscovery::SearchForServersAsync()
extern void LocalLobbyDiscovery_SearchForServersAsync_mAD893E6C6065C283446364384D716E90F30E43F3 (void);
// 0x000003D3 System.Void LocalLobbyDiscovery::.ctor()
extern void LocalLobbyDiscovery__ctor_mE64DA509F0ABE56C4F444EBE10042BB44ED77D11 (void);
// 0x000003D4 System.Void LocalLobbyDiscovery::<StartAdvertisingServer>b__20_0()
extern void LocalLobbyDiscovery_U3CStartAdvertisingServerU3Eb__20_0_mB7676E2EBA8ED3E003DC5CC06CF9C94A8C883AE0 (void);
// 0x000003D5 System.Void LocalLobbyDiscovery/<AdvertiseServerAsync>d__22::MoveNext()
extern void U3CAdvertiseServerAsyncU3Ed__22_MoveNext_mA4953319F3B567A79ECB0EBB397A59A28D2D7A12 (void);
// 0x000003D6 System.Void LocalLobbyDiscovery/<AdvertiseServerAsync>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAdvertiseServerAsyncU3Ed__22_SetStateMachine_m90936D6BD07428A4D6C7B052D5E26E806DA0EE2A (void);
// 0x000003D7 System.Void LocalLobbyDiscovery/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mABE9AE499F607DD1A5B2EC81389A3B3BA1F0CC3A (void);
// 0x000003D8 System.Void LocalLobbyDiscovery/<>c__DisplayClass25_0::<SearchForServersAsync>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CSearchForServersAsyncU3Eb__0_mBCE40A67CCC918E29D47E4B8DC8C4D57220C5F3B (void);
// 0x000003D9 System.Void LocalLobbyDiscovery/<SearchForServersAsync>d__25::MoveNext()
extern void U3CSearchForServersAsyncU3Ed__25_MoveNext_m285A8B5B876066DAF7FAD3F800DFEB229B8BFAED (void);
// 0x000003DA System.Void LocalLobbyDiscovery/<SearchForServersAsync>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSearchForServersAsyncU3Ed__25_SetStateMachine_m515B2595011C1B82C34D358DF3C3D201E5B31F7A (void);
// 0x000003DB LocalLobbyManager LocalLobbyManager::get_Instance()
extern void LocalLobbyManager_get_Instance_m190F052099D7FCDDB3ACFE52094B13D127A443B5 (void);
// 0x000003DC System.Void LocalLobbyManager::set_Instance(LocalLobbyManager)
extern void LocalLobbyManager_set_Instance_mE4B277B7D220EB55DA6389822718C1C1ABBBDC58 (void);
// 0x000003DD System.Collections.Generic.Dictionary`2<System.String,LocalLobby> LocalLobbyManager::get_Lobbies()
extern void LocalLobbyManager_get_Lobbies_mF07D8DD7B2C15A2EF73C3F97586E867F400929CB (void);
// 0x000003DE System.UInt16 LocalLobbyManager::get_ServerPort()
extern void LocalLobbyManager_get_ServerPort_m3EAC3B1D7CF263C45EAEC883B6710A2188D1B5BB (void);
// 0x000003DF System.UInt16 LocalLobbyManager::get_ClientPort()
extern void LocalLobbyManager_get_ClientPort_m2D5F2E0908C84C6F3A97C38E73701C4D935808AE (void);
// 0x000003E0 System.Boolean LocalLobbyManager::get_IsHost()
extern void LocalLobbyManager_get_IsHost_mC0C324086AA62F97BA712A283A1FF57692FA998B (void);
// 0x000003E1 LocalLobby LocalLobbyManager::get_JoinedLobby()
extern void LocalLobbyManager_get_JoinedLobby_mF424C48D9E5DED94CF2461722D034BF745D3F5A8 (void);
// 0x000003E2 LocalPlayer LocalLobbyManager::GetPlayer()
extern void LocalLobbyManager_GetPlayer_m65FD3348B312F658A1F70E61D73544750A2AB5A6 (void);
// 0x000003E3 System.Void LocalLobbyManager::Awake()
extern void LocalLobbyManager_Awake_m84C8039A1C6073AD11E7F66F964D84F7D688DB73 (void);
// 0x000003E4 System.Void LocalLobbyManager::OnDisable()
extern void LocalLobbyManager_OnDisable_mEAC7039DF1FE20E68193852277E5A6CF38FCBB0C (void);
// 0x000003E5 System.Void LocalLobbyManager::OnDestroy()
extern void LocalLobbyManager_OnDestroy_m752E0C352650267909E3DFC781E57A785BB0FA3B (void);
// 0x000003E6 System.Void LocalLobbyManager::OnApplicationQuit()
extern void LocalLobbyManager_OnApplicationQuit_m37E896E4D5CCC9713B461E012FC8C0B41715F42F (void);
// 0x000003E7 System.Void LocalLobbyManager::Update()
extern void LocalLobbyManager_Update_m146CF0A9575815578D92B4A87986DF4A37B450F2 (void);
// 0x000003E8 System.Void LocalLobbyManager::HandleAdvertisedLobbyHeartbeats()
extern void LocalLobbyManager_HandleAdvertisedLobbyHeartbeats_mCA695C1DE4DE3B926DA17BB3A80052606333E902 (void);
// 0x000003E9 System.Void LocalLobbyManager::HandleJoinedLobbyHeartbeats()
extern void LocalLobbyManager_HandleJoinedLobbyHeartbeats_m5421A0422BEDC4AD427DE29BF476350230CD15F2 (void);
// 0x000003EA System.Void LocalLobbyManager::StartClient()
extern void LocalLobbyManager_StartClient_m8F18A224218CFB2C59AF382CD34E9EB165E13356 (void);
// 0x000003EB System.Void LocalLobbyManager::StopClient()
extern void LocalLobbyManager_StopClient_m7BAFE744411202A92F00392C9261FC0610841BA4 (void);
// 0x000003EC System.Void LocalLobbyManager::StartServer()
extern void LocalLobbyManager_StartServer_m52FC8461CF8497F94675BFFC80C5AE4217A8795D (void);
// 0x000003ED System.Void LocalLobbyManager::StopServer()
extern void LocalLobbyManager_StopServer_m38888F44BADE905D4797FF98B17CFF7FC401D5BB (void);
// 0x000003EE System.Void LocalLobbyManager::StartListeningClient()
extern void LocalLobbyManager_StartListeningClient_m87BECF9579313309365DE8BA0D45FEBB1118F1C1 (void);
// 0x000003EF System.Threading.Tasks.Task LocalLobbyManager::StartListeningClientAsync()
extern void LocalLobbyManager_StartListeningClientAsync_mFF36434DC24B293B709133134895F1EEC5DF0719 (void);
// 0x000003F0 System.Void LocalLobbyManager::StopListeningClient()
extern void LocalLobbyManager_StopListeningClient_mF8BEEB5F6913C479A5E29A0D9982EB206E410065 (void);
// 0x000003F1 System.Threading.Tasks.Task LocalLobbyManager::StartListeningServerAsync()
extern void LocalLobbyManager_StartListeningServerAsync_mE02DF11F5A4C3F65476F1115BAAC7B6DFCB49E7E (void);
// 0x000003F2 System.Void LocalLobbyManager::StopListeningServer()
extern void LocalLobbyManager_StopListeningServer_m13F814367D4D7E0D7C9982ECCC6ECE0BF7130915 (void);
// 0x000003F3 System.Void LocalLobbyManager::StartSearchingClient()
extern void LocalLobbyManager_StartSearchingClient_m49BC52E6FE2B55D0759F53A4A228B5F1FB98356D (void);
// 0x000003F4 System.Threading.Tasks.Task LocalLobbyManager::StartSearchingClientAsync()
extern void LocalLobbyManager_StartSearchingClientAsync_m0A4D577970CB997FB774DA7BE11CC63617BB8E01 (void);
// 0x000003F5 System.Void LocalLobbyManager::StopSearchingClient()
extern void LocalLobbyManager_StopSearchingClient_m7D3BC20837947A43283E95403422384F34541D52 (void);
// 0x000003F6 System.Threading.Tasks.Task LocalLobbyManager::SendClientMessageAsync(System.Byte[],System.Net.IPEndPoint)
extern void LocalLobbyManager_SendClientMessageAsync_m4B49769F5A791B4DE4B56CA69D69422DB1C19333 (void);
// 0x000003F7 System.Threading.Tasks.Task LocalLobbyManager::SendServerMessageAsync(System.Byte[],System.Net.IPEndPoint)
extern void LocalLobbyManager_SendServerMessageAsync_mA92B5A2E7DA50D6F32AC122CD8BBDE6F07802852 (void);
// 0x000003F8 System.Boolean LocalLobbyManager::get_clientIsSearching()
extern void LocalLobbyManager_get_clientIsSearching_mBABA1E8E6C3EA277137AB7DD8377BEB6D3F88A97 (void);
// 0x000003F9 System.Boolean LocalLobbyManager::get_clientIsListening()
extern void LocalLobbyManager_get_clientIsListening_m03C27DCE09D0ACE42784DDC9F41514FABD4D45A6 (void);
// 0x000003FA System.Boolean LocalLobbyManager::get_serverIsListening()
extern void LocalLobbyManager_get_serverIsListening_mA23F6BA5BBE14F66E6D6E823C056021A0B837BF0 (void);
// 0x000003FB System.Boolean LocalLobbyManager::SenderIsHost(System.Net.IPEndPoint)
extern void LocalLobbyManager_SenderIsHost_mB593252651ECDFFF9B9C0977513039E53DE2C296 (void);
// 0x000003FC System.Boolean LocalLobbyManager::SenderIsMember(System.Net.IPEndPoint)
extern void LocalLobbyManager_SenderIsMember_mE4198D7029842521D5349F1516A99288C7A68D5D (void);
// 0x000003FD System.Void LocalLobbyManager::HandleMessage(LocalMessage,System.Net.IPEndPoint)
extern void LocalLobbyManager_HandleMessage_m1FD6E508519CD7F99153ABD83FF41CB057CCADC7 (void);
// 0x000003FE System.Void LocalLobbyManager::HandleClientHeartbeat(System.String,System.Net.IPEndPoint)
extern void LocalLobbyManager_HandleClientHeartbeat_mFA6CA39C7231E6E0A90AECEFE12D042FA4375734 (void);
// 0x000003FF System.Void LocalLobbyManager::HandleServerHeartbeat()
extern void LocalLobbyManager_HandleServerHeartbeat_m956B3706AF80ECFB7F928829BD4079DB391A0C4F (void);
// 0x00000400 System.Void LocalLobbyManager::HandleLobbyAdvertisement(LocalLobby)
extern void LocalLobbyManager_HandleLobbyAdvertisement_mE4FA0C9A63183B284628D9E549405C6D3CA76479 (void);
// 0x00000401 System.Void LocalLobbyManager::HandleLobbySearchRequest(System.Net.IPEndPoint)
extern void LocalLobbyManager_HandleLobbySearchRequest_m95C75CC8173261E63EE0F86C42BF664C41F04923 (void);
// 0x00000402 System.Void LocalLobbyManager::HandleLobbyUpdate(LocalLobby)
extern void LocalLobbyManager_HandleLobbyUpdate_m0F5B994FB7D0181EB51F84B1ECC38E863099B27F (void);
// 0x00000403 System.Void LocalLobbyManager::HandleLobbyDestroyed(LocalLobby)
extern void LocalLobbyManager_HandleLobbyDestroyed_mC87CAF350596BE9CB0A20AC6F68BC390BE147CBF (void);
// 0x00000404 System.Void LocalLobbyManager::HandleLobbyJoinRequest(LocalPlayer)
extern void LocalLobbyManager_HandleLobbyJoinRequest_m353901E5BB150435C35DFADA02148BFA0FCEE0A9 (void);
// 0x00000405 System.Void LocalLobbyManager::HandleLobbyJoinResponse(PlayerJoinResponseData)
extern void LocalLobbyManager_HandleLobbyJoinResponse_mFEEB1498E07CEA861F5F523B8DA95B7A6A61A084 (void);
// 0x00000406 System.Void LocalLobbyManager::HandleLobbyLeaveRequest(LocalPlayer)
extern void LocalLobbyManager_HandleLobbyLeaveRequest_m423180F0F928AF541E4409E6925B84F2BD19DC47 (void);
// 0x00000407 System.Void LocalLobbyManager::HandlePlayerUpdate(LocalPlayer)
extern void LocalLobbyManager_HandlePlayerUpdate_m7AF0062AE676D06075B87C1610EC55D4A48F2512 (void);
// 0x00000408 System.Void LocalLobbyManager::HandlePlayerKick()
extern void LocalLobbyManager_HandlePlayerKick_m56B0F460E36FEAE3041CCF95670645D1F30C0DCF (void);
// 0x00000409 System.Void LocalLobbyManager::SendLobbyUpdate()
extern void LocalLobbyManager_SendLobbyUpdate_m03579034EAF81A4E4F2E0BF8B04A13982C7CCEE9 (void);
// 0x0000040A System.Void LocalLobbyManager::SendPlayerUpdate()
extern void LocalLobbyManager_SendPlayerUpdate_mCE5D548107675E3CDD79150EF28E3570BE23CE42 (void);
// 0x0000040B System.Threading.Tasks.Task LocalLobbyManager::SendLobbySearchRequest()
extern void LocalLobbyManager_SendLobbySearchRequest_m08AF7DA76746217296DA4FC86EC272E16367EFE1 (void);
// 0x0000040C System.Threading.Tasks.Task LocalLobbyManager::SendClientHeartbeat()
extern void LocalLobbyManager_SendClientHeartbeat_m0B7B6F5833BE8C80AD08CBFF6336F58462FCC8D1 (void);
// 0x0000040D System.Threading.Tasks.Task`1<LocalLobby> LocalLobbyManager::CreateLobby(LobbyConfiguration)
extern void LocalLobbyManager_CreateLobby_mD8B9D38B75B3D95AFD4ADFC9C915B75F134846EA (void);
// 0x0000040E System.Threading.Tasks.Task LocalLobbyManager::JoinLobbyByCode(System.String,System.Net.IPEndPoint)
extern void LocalLobbyManager_JoinLobbyByCode_m7EF0F8DEE0504C6E245B7F777E9E371774F4B43D (void);
// 0x0000040F System.Threading.Tasks.Task LocalLobbyManager::JoinLobby(LocalLobby)
extern void LocalLobbyManager_JoinLobby_m06EEDFD02588DD484EAB75CE0B029AA25B115DE8 (void);
// 0x00000410 System.Void LocalLobbyManager::LeaveLobby()
extern void LocalLobbyManager_LeaveLobby_mBC0F1D7B7A203D7B7B5C5FC733895E5C412032C8 (void);
// 0x00000411 System.Void LocalLobbyManager::DestroyLobby()
extern void LocalLobbyManager_DestroyLobby_mBC8DA820B5E2520F16E98FBF767062B1246A1B6A (void);
// 0x00000412 System.Void LocalLobbyManager::KickPlayer(System.String)
extern void LocalLobbyManager_KickPlayer_m9A5808C3B4EE193651EAAEEFBD6B1D77E3FDC036 (void);
// 0x00000413 System.Void LocalLobbyManager::TransferLobbyHost(System.String)
extern void LocalLobbyManager_TransferLobbyHost_m8B7DDED2BB097E8D2591156563BEB7558430A328 (void);
// 0x00000414 System.Void LocalLobbyManager::AddLobby(LocalLobby)
extern void LocalLobbyManager_AddLobby_m43A114B909A9EBAF16B402AE9B822C4E9F5EE149 (void);
// 0x00000415 LocalLobby LocalLobbyManager::RemoveLobby(System.String)
extern void LocalLobbyManager_RemoveLobby_m190DD326CB7284FB12FC9EE0AD1AEF1FA7C4618F (void);
// 0x00000416 System.Void LocalLobbyManager::RemoveAllLobbies()
extern void LocalLobbyManager_RemoveAllLobbies_mB014E12211527AB6C6E2C5F51E12862385BDA9E1 (void);
// 0x00000417 System.Void LocalLobbyManager::.ctor()
extern void LocalLobbyManager__ctor_m9C8D56DFFF3472043C4E9FA775C647056F47FD66 (void);
// 0x00000418 System.Threading.Tasks.Task LocalLobbyManager::<CreateLobby>b__84_0()
extern void LocalLobbyManager_U3CCreateLobbyU3Eb__84_0_mD53222765B7AE95A190C4D991A8AFDF23B33C701 (void);
// 0x00000419 System.Void LocalLobbyManager/<>c__DisplayClass51_0::.ctor()
extern void U3CU3Ec__DisplayClass51_0__ctor_m7A1F6DC32DA14E53D0F6E96D8F85C641DD92C444 (void);
// 0x0000041A System.Void LocalLobbyManager/<>c__DisplayClass51_0::<StartListeningClientAsync>b__0()
extern void U3CU3Ec__DisplayClass51_0_U3CStartListeningClientAsyncU3Eb__0_mD9D1915D6A03BB622CE7EB5574BE4C0A0E7D224F (void);
// 0x0000041B System.Void LocalLobbyManager/<StartListeningClientAsync>d__51::MoveNext()
extern void U3CStartListeningClientAsyncU3Ed__51_MoveNext_mC296F54F2EEFC5AFD81551A534F467ABA06159FF (void);
// 0x0000041C System.Void LocalLobbyManager/<StartListeningClientAsync>d__51::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartListeningClientAsyncU3Ed__51_SetStateMachine_mB595BD4394C387BD7DF0E7208720317C2364E296 (void);
// 0x0000041D System.Void LocalLobbyManager/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m828EA0457644B9115AF70CA360EC9F9692A7CB3F (void);
// 0x0000041E System.Void LocalLobbyManager/<>c__DisplayClass53_0::<StartListeningServerAsync>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CStartListeningServerAsyncU3Eb__0_mFDB289338481C9555AD600ACDD739EEE62D34DA7 (void);
// 0x0000041F System.Void LocalLobbyManager/<StartListeningServerAsync>d__53::MoveNext()
extern void U3CStartListeningServerAsyncU3Ed__53_MoveNext_m343428C0B7362B8E517B9BBAE531CCBC0F2594D0 (void);
// 0x00000420 System.Void LocalLobbyManager/<StartListeningServerAsync>d__53::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartListeningServerAsyncU3Ed__53_SetStateMachine_m7F74BCAA5437DF0508E0F095B85F561E9409B75B (void);
// 0x00000421 System.Void LocalLobbyManager/<StartSearchingClientAsync>d__56::MoveNext()
extern void U3CStartSearchingClientAsyncU3Ed__56_MoveNext_m2D1E6DBE748978539DE360BBB9CA49A37A98C2F9 (void);
// 0x00000422 System.Void LocalLobbyManager/<StartSearchingClientAsync>d__56::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartSearchingClientAsyncU3Ed__56_SetStateMachine_m67B5E70F7BC655728EDC778350C8176F8BBBC90D (void);
// 0x00000423 System.Void LocalLobbyManager/<SendClientMessageAsync>d__58::MoveNext()
extern void U3CSendClientMessageAsyncU3Ed__58_MoveNext_mD66C90618809CF9A70C25959978F5F5E644F3541 (void);
// 0x00000424 System.Void LocalLobbyManager/<SendClientMessageAsync>d__58::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendClientMessageAsyncU3Ed__58_SetStateMachine_m6D290276DF0382BD9589625ECA249F2B4348BDAC (void);
// 0x00000425 System.Void LocalLobbyManager/<SendServerMessageAsync>d__59::MoveNext()
extern void U3CSendServerMessageAsyncU3Ed__59_MoveNext_m4E42D1CB25FB4F3A0BF6C129E30F630177E67506 (void);
// 0x00000426 System.Void LocalLobbyManager/<SendServerMessageAsync>d__59::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendServerMessageAsyncU3Ed__59_SetStateMachine_mA67FCC5226AA7F338976A0BD63E36F10F1FA0F07 (void);
// 0x00000427 System.Void LocalLobbyManager/<HandleLobbySearchRequest>d__72::MoveNext()
extern void U3CHandleLobbySearchRequestU3Ed__72_MoveNext_mD43858816A0807484305BF003FACD2D0D32D652E (void);
// 0x00000428 System.Void LocalLobbyManager/<HandleLobbySearchRequest>d__72::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleLobbySearchRequestU3Ed__72_SetStateMachine_mD9705204E6BF8C12370E1E44F027CADA0FA79D1F (void);
// 0x00000429 System.Void LocalLobbyManager/<SendLobbyUpdate>d__80::MoveNext()
extern void U3CSendLobbyUpdateU3Ed__80_MoveNext_mF7243DBCDB8E10D5DC0DAA519D04A491BA1C0E85 (void);
// 0x0000042A System.Void LocalLobbyManager/<SendLobbyUpdate>d__80::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendLobbyUpdateU3Ed__80_SetStateMachine_m398D67B94FA145E40C4795B2EC7A2C13AB5705C1 (void);
// 0x0000042B System.Void LocalLobbyManager/<SendPlayerUpdate>d__81::MoveNext()
extern void U3CSendPlayerUpdateU3Ed__81_MoveNext_m68B140503BF008CB9C3F5EC3B6777C7014501462 (void);
// 0x0000042C System.Void LocalLobbyManager/<SendPlayerUpdate>d__81::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendPlayerUpdateU3Ed__81_SetStateMachine_m0544B8DD9FE6B4FC9C9606CE0F825F435692D87B (void);
// 0x0000042D System.Void LocalLobbyManager/<SendLobbySearchRequest>d__82::MoveNext()
extern void U3CSendLobbySearchRequestU3Ed__82_MoveNext_m759975C2302973B31ED94F8205E0E2BAE33B7A4D (void);
// 0x0000042E System.Void LocalLobbyManager/<SendLobbySearchRequest>d__82::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendLobbySearchRequestU3Ed__82_SetStateMachine_m43B1C2551233FF1177772227F7B0C93ED08A9B83 (void);
// 0x0000042F System.Void LocalLobbyManager/<SendClientHeartbeat>d__83::MoveNext()
extern void U3CSendClientHeartbeatU3Ed__83_MoveNext_m3E15302EBB44E9F9913E273D79A363ED4B327B3D (void);
// 0x00000430 System.Void LocalLobbyManager/<SendClientHeartbeat>d__83::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendClientHeartbeatU3Ed__83_SetStateMachine_mE3EF863032125870B8B3E4F5996713D2E6359DCD (void);
// 0x00000431 System.Void LocalLobbyManager/<CreateLobby>d__84::MoveNext()
extern void U3CCreateLobbyU3Ed__84_MoveNext_m218701A5A677B7C500C48A0C272794D051B11917 (void);
// 0x00000432 System.Void LocalLobbyManager/<CreateLobby>d__84::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateLobbyU3Ed__84_SetStateMachine_m6CBBAB0CE132420589F3F5629A380256A1539CDB (void);
// 0x00000433 System.Void LocalLobbyManager/<JoinLobbyByCode>d__85::MoveNext()
extern void U3CJoinLobbyByCodeU3Ed__85_MoveNext_mD72A3D5FD08210B803129F88CC3D8C3B45402B48 (void);
// 0x00000434 System.Void LocalLobbyManager/<JoinLobbyByCode>d__85::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinLobbyByCodeU3Ed__85_SetStateMachine_mDA60AE5B340745B217794FF36D61CB7372CE914D (void);
// 0x00000435 System.Void LocalLobbyManager/<JoinLobby>d__86::MoveNext()
extern void U3CJoinLobbyU3Ed__86_MoveNext_m123B4D24C1ED48F8FA612BE2900BD65C330B28AB (void);
// 0x00000436 System.Void LocalLobbyManager/<JoinLobby>d__86::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CJoinLobbyU3Ed__86_SetStateMachine_m5B34EFB95E2B8B3467845E4442ECED18733DA047 (void);
// 0x00000437 System.Void LocalLobbyManager/<LeaveLobby>d__87::MoveNext()
extern void U3CLeaveLobbyU3Ed__87_MoveNext_m04DA6B1D686AD5A7080C27709914228BE95C81A2 (void);
// 0x00000438 System.Void LocalLobbyManager/<LeaveLobby>d__87::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLeaveLobbyU3Ed__87_SetStateMachine_m1816662CBEACB2A24E946D7CAE3DD1E21EE453D2 (void);
// 0x00000439 System.Void LocalLobbyManager/<DestroyLobby>d__88::MoveNext()
extern void U3CDestroyLobbyU3Ed__88_MoveNext_m50A01D84130D48C3E13AEC498339E1D45F836CCE (void);
// 0x0000043A System.Void LocalLobbyManager/<DestroyLobby>d__88::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDestroyLobbyU3Ed__88_SetStateMachine_m70B496D1E400BEDF360BAD4875BCA85C3B021D96 (void);
// 0x0000043B System.Void LocalLobbyManager/<KickPlayer>d__89::MoveNext()
extern void U3CKickPlayerU3Ed__89_MoveNext_mEFF5F9A5414632CAB14C0BD10E301B72E7E8AD04 (void);
// 0x0000043C System.Void LocalLobbyManager/<KickPlayer>d__89::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CKickPlayerU3Ed__89_SetStateMachine_m873D5625E53C2781F3BD2BC364A6F98F18052AC6 (void);
// 0x0000043D System.Void LocalLobbyManager/<TransferLobbyHost>d__90::MoveNext()
extern void U3CTransferLobbyHostU3Ed__90_MoveNext_m2DE99B1A92DD04B6FD9A94111D42BB73CAB042AA (void);
// 0x0000043E System.Void LocalLobbyManager/<TransferLobbyHost>d__90::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTransferLobbyHostU3Ed__90_SetStateMachine_mCCC607DCC0303A0585D3AFC30782A8D52E578EB6 (void);
// 0x0000043F System.Void LocalLobbyManager/<<CreateLobby>b__84_0>d::MoveNext()
extern void U3CU3CCreateLobbyU3Eb__84_0U3Ed_MoveNext_m3091BB36BAD5C9AE3FF475D2D0CF4D801DD2B775 (void);
// 0x00000440 System.Void LocalLobbyManager/<<CreateLobby>b__84_0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CCreateLobbyU3Eb__84_0U3Ed_SetStateMachine_mBB98E6072F3F29C271793965A4409115595D468D (void);
// 0x00000441 System.Void LocalMessage::.ctor()
extern void LocalMessage__ctor_mDDB7FC57ADF1AB8A559D516687D4B073A25F1F99 (void);
// 0x00000442 System.Void PlayerJoinResponseData::.ctor(System.Boolean)
extern void PlayerJoinResponseData__ctor_m36F664E88EAA01C9E369725A837F179A2F366E05 (void);
// 0x00000443 System.Net.IPEndPoint LocalPlayer::get_endpoint()
extern void LocalPlayer_get_endpoint_m6A80FDDF3C5019DB66AEE83813EA3C1A852387CB (void);
// 0x00000444 System.Void LocalPlayer::.ctor(System.String)
extern void LocalPlayer__ctor_mC91FED2C8D3E57084CC8AC278585167E16ABF06F (void);
// 0x00000445 System.Void LocalPlayer::.ctor()
extern void LocalPlayer__ctor_m30D7A32DAF1540AAF2AF366FC4F03F3A1A6C3018 (void);
// 0x00000446 System.Void PlayerInfo::.ctor(LocalPlayer)
extern void PlayerInfo__ctor_mC32222D17D4407F9824227F42DF0F6967FEBB479 (void);
// 0x00000447 System.Void PlayerInfo::.ctor(Unity.Services.Lobbies.Models.Player)
extern void PlayerInfo__ctor_m4F95D1F2CEDE4C7D26379CC5D17D40DD7BC98192 (void);
// 0x00000448 PlayerManager PlayerManager::get_Instance()
extern void PlayerManager_get_Instance_m6A9319DF9F35EE81B0BB6F69F9CAEBEAF168418B (void);
// 0x00000449 System.Void PlayerManager::set_Instance(PlayerManager)
extern void PlayerManager_set_Instance_mEB9E08C3E990502415BF920F221F354EF18704B5 (void);
// 0x0000044A System.Void PlayerManager::Awake()
extern void PlayerManager_Awake_m4A997ABB547A7B4C3BA447681C1C8718B7B4A5B6 (void);
// 0x0000044B System.Void PlayerManager::Start()
extern void PlayerManager_Start_mD4281A9FA4F173B0A41018EA2F11F09281E574BE (void);
// 0x0000044C LocalPlayer PlayerManager::GetLocalPlayer()
extern void PlayerManager_GetLocalPlayer_mBB2B10841D02A5CE908B86F49C563ED4D9C010EB (void);
// 0x0000044D Unity.Services.Lobbies.Models.Player PlayerManager::GetRemotePlayer()
extern void PlayerManager_GetRemotePlayer_mB6F11D45130C226041AAB81C7F3DDEF51FF45D19 (void);
// 0x0000044E System.Void PlayerManager::.ctor()
extern void PlayerManager__ctor_mEDEFFACDDE66D7077DE8CDED6560CA06218B5A0E (void);
// 0x0000044F System.Void ActiveSessionUI::Start()
extern void ActiveSessionUI_Start_mE6462F168A10FAFA4E63BE78B503960A413A4327 (void);
// 0x00000450 System.Void ActiveSessionUI::OnEnable()
extern void ActiveSessionUI_OnEnable_mF433BE0945DE83D7521F3672E6AFE08F7860A3FE (void);
// 0x00000451 System.Void ActiveSessionUI::OnDestroy()
extern void ActiveSessionUI_OnDestroy_mA0D39A7C7C36DAE7B710F4D25E3B90F7ADC89670 (void);
// 0x00000452 System.Void ActiveSessionUI::OnDisable()
extern void ActiveSessionUI_OnDisable_m3AF7EBFA6F41B9717EB4E3055340F01631A4714F (void);
// 0x00000453 System.Void ActiveSessionUI::UpdateLobbyInfo()
extern void ActiveSessionUI_UpdateLobbyInfo_m76550D683A283D642CF446311E2B4BB8723C8B1B (void);
// 0x00000454 System.Void ActiveSessionUI::ResetUI()
extern void ActiveSessionUI_ResetUI_mCC39C9DF1D031EA1222CBF0F76F725800C8F7D02 (void);
// 0x00000455 System.String ActiveSessionUI::get_SelectedParticipantID()
extern void ActiveSessionUI_get_SelectedParticipantID_m0430BBFB877DA74B504E1ACE450291EE7190ADF7 (void);
// 0x00000456 System.Void ActiveSessionUI::set_SelectedParticipantID(System.String)
extern void ActiveSessionUI_set_SelectedParticipantID_m24CDC917FED740497354B0BED020B5D7276A4621 (void);
// 0x00000457 System.Void ActiveSessionUI::OnClickExitSession()
extern void ActiveSessionUI_OnClickExitSession_m9A1D4250EA17364E70F3461B89B925379F1C27D8 (void);
// 0x00000458 System.Void ActiveSessionUI::OnClickKickPlayer()
extern void ActiveSessionUI_OnClickKickPlayer_m96F7BAC67C5C0D549BE4C4A967CD79A779DFDD01 (void);
// 0x00000459 System.Void ActiveSessionUI::AddOrUpdatePlayerButton(PlayerInfo)
extern void ActiveSessionUI_AddOrUpdatePlayerButton_m6BB11CB092B3C4DC865AEA190F88AF12A86F657E (void);
// 0x0000045A System.Void ActiveSessionUI::OnParticipantLeft(PlayerInfo)
extern void ActiveSessionUI_OnParticipantLeft_m616CBFE1710D268F904A06A2805B584623130FA0 (void);
// 0x0000045B System.Void ActiveSessionUI::RemoveParticipantButton(System.String)
extern void ActiveSessionUI_RemoveParticipantButton_m3BECBB580F9E0C0988981E9A2844E72C8B17BF1C (void);
// 0x0000045C System.Void ActiveSessionUI::AddParticipantButton(PlayerInfo)
extern void ActiveSessionUI_AddParticipantButton_mE97AB85378DB2D3C14FFE2A7811347B510730586 (void);
// 0x0000045D System.Void ActiveSessionUI::.ctor()
extern void ActiveSessionUI__ctor_m1584C27E4C4A20058A44AA69D39E3DD8A2BF5A52 (void);
// 0x0000045E System.Void ActiveSessionUI/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mFCA759B62B0DEA71E04EAC186F1D14358B9D9038 (void);
// 0x0000045F System.Void ActiveSessionUI/<>c__DisplayClass23_0::<AddParticipantButton>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CAddParticipantButtonU3Eb__0_m699118BA78C6742C4440DADF2957CF4DC70A0A57 (void);
// 0x00000460 System.Void LobbyButton::SetLobbyData(LocalLobby)
extern void LobbyButton_SetLobbyData_m245977958FF3FB6390F441DBB5EBB587F830432B (void);
// 0x00000461 System.Void LobbyButton::SetLobbyData(Unity.Services.Lobbies.Models.Lobby)
extern void LobbyButton_SetLobbyData_mFC7860B206533FF6C938F0CCD4851C6A8AF5B09C (void);
// 0x00000462 System.Void LobbyButton::.ctor()
extern void LobbyButton__ctor_mC18F9FDEEC010FAAAB1CE25FE3F20E247A0FD643 (void);
// 0x00000463 LobbyConfiguration NewSessionDialog::get_EnteredLobbyConfiguration()
extern void NewSessionDialog_get_EnteredLobbyConfiguration_m59643D81CE7CCB59774237D346CCD95D08C3268B (void);
// 0x00000464 System.Void NewSessionDialog::.ctor()
extern void NewSessionDialog__ctor_m3ACAF28D80FB3C6B1299700F30812FC109F3B415 (void);
// 0x00000465 System.Boolean SessionListUI::get_ShowLocalLobbies()
extern void SessionListUI_get_ShowLocalLobbies_m2128AE29A06C0E897AA372D6865DD54020D7BD50 (void);
// 0x00000466 System.Void SessionListUI::set_ShowLocalLobbies(System.Boolean)
extern void SessionListUI_set_ShowLocalLobbies_m79372827EBB17435514599110C52A74BFE889FDE (void);
// 0x00000467 System.Void SessionListUI::Start()
extern void SessionListUI_Start_mF09416D52FAE4759105A96D3E3C0A743557AED0E (void);
// 0x00000468 System.Void SessionListUI::StartLocalLobbySearch()
extern void SessionListUI_StartLocalLobbySearch_mB376D8C12981E7E5C98344B047C0465088ACB688 (void);
// 0x00000469 System.Void SessionListUI::StartRemoteLobbySearch()
extern void SessionListUI_StartRemoteLobbySearch_m0F5C7E7EBC8B77FB3D351C8B44CE25BCD83EF8C6 (void);
// 0x0000046A System.Void SessionListUI::OnEnable()
extern void SessionListUI_OnEnable_mA3A2019E62414A0B99A35A535A70C11812DEABEA (void);
// 0x0000046B System.Void SessionListUI::OnDisable()
extern void SessionListUI_OnDisable_m5B572A52E95F5EDEEB90D9083865986F6396BEC4 (void);
// 0x0000046C System.String SessionListUI::get_SelectedRemoteSessionID()
extern void SessionListUI_get_SelectedRemoteSessionID_m0DF28CEEEDF0228D7EDFEF416AA23FA85961978D (void);
// 0x0000046D System.Void SessionListUI::set_SelectedRemoteSessionID(System.String)
extern void SessionListUI_set_SelectedRemoteSessionID_m6879640755F3922F5E55F4D0902BD6A119035C09 (void);
// 0x0000046E System.String SessionListUI::get_SelectedLocalSessionID()
extern void SessionListUI_get_SelectedLocalSessionID_m7BBA9817A9C43BDFBC2E4F85F07022A4155951A1 (void);
// 0x0000046F System.Void SessionListUI::set_SelectedLocalSessionID(System.String)
extern void SessionListUI_set_SelectedLocalSessionID_mF597DEAB4761450AD3D47A25BDB680FD66FE8ECA (void);
// 0x00000470 System.Void SessionListUI::DestroyAllButtons()
extern void SessionListUI_DestroyAllButtons_mB6A7FA185FDAACA34DA1C86F85F7A5624065DA95 (void);
// 0x00000471 System.Void SessionListUI::UpdateLocalLobbyButtons()
extern void SessionListUI_UpdateLocalLobbyButtons_m6A9938D206D60398E39836E3FC6572212942D4FD (void);
// 0x00000472 System.Void SessionListUI::UpdateRemoteLobbyButtons()
extern void SessionListUI_UpdateRemoteLobbyButtons_mDA1E9FF10DBD8240F3CC83BAA0003BD6C117DFDC (void);
// 0x00000473 System.Void SessionListUI::AddLocalLobbyButton(LocalLobby)
extern void SessionListUI_AddLocalLobbyButton_m009BCDD990E8656CEEA93E08182147FF44D5BF77 (void);
// 0x00000474 System.Void SessionListUI::RemoveLocalLobbyButton(System.String)
extern void SessionListUI_RemoveLocalLobbyButton_m30AC0336D31CC3D4FD8C3F90A9547120ECD6A52B (void);
// 0x00000475 System.Void SessionListUI::UpdateLocalLobbyButton(LocalLobby)
extern void SessionListUI_UpdateLocalLobbyButton_m70B5F86CA070681054B6F5EA7EB050A18B1A5DE4 (void);
// 0x00000476 System.Void SessionListUI::AddRemoteLobbyButton(Unity.Services.Lobbies.Models.Lobby)
extern void SessionListUI_AddRemoteLobbyButton_mC6BB74429E0AB3C7D0544AD65BC3659B3571C89E (void);
// 0x00000477 System.Void SessionListUI::RemoveRemoteLobbyButton(System.String)
extern void SessionListUI_RemoveRemoteLobbyButton_m633F425006915F6D61F5F3285A94A11923128A57 (void);
// 0x00000478 System.Void SessionListUI::UpdateRemoteLobbyButton(Unity.Services.Lobbies.Models.Lobby)
extern void SessionListUI_UpdateRemoteLobbyButton_m898D52DBA2FBF7E1CD365507B624201578E5DEC8 (void);
// 0x00000479 System.Void SessionListUI::OnClickNewSession()
extern void SessionListUI_OnClickNewSession_m5C2A3D572D1AAF3243258647461FDEE3EAF8E83E (void);
// 0x0000047A System.Void SessionListUI::OnClickJoinSession()
extern void SessionListUI_OnClickJoinSession_m0F284BE8D2E5CC169E3F6B26237917881C46D66F (void);
// 0x0000047B System.Void SessionListUI::OnConnectionTypeToggleSelected(System.Int32)
extern void SessionListUI_OnConnectionTypeToggleSelected_m67D9FD93083A937FD05D6A7E3C74411E641CCDB5 (void);
// 0x0000047C System.Void SessionListUI::.ctor()
extern void SessionListUI__ctor_mD79DC3E5227DCF16DCD57DBC82EF09D55527A8B4 (void);
// 0x0000047D System.Void SessionListUI/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mE9B8265E221839835DCFDC99232FB6442978E9EE (void);
// 0x0000047E System.Void SessionListUI/<>c__DisplayClass33_0::<AddLocalLobbyButton>b__0(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CAddLocalLobbyButtonU3Eb__0_m60D92BDFE96FCA8DAD070CE5A3C659327921EEE6 (void);
// 0x0000047F System.Void SessionListUI/<>c__DisplayClass33_0::<AddLocalLobbyButton>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CAddLocalLobbyButtonU3Eb__1_m2FBD6B8C70E7610F1CE4924B6EF000403E1DEC1A (void);
// 0x00000480 System.Void SessionListUI/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mE6FF1237D40FE2308ACF1AEAACBB9F5DCA4EA3EA (void);
// 0x00000481 System.Void SessionListUI/<>c__DisplayClass36_0::<AddRemoteLobbyButton>b__0(System.Single)
extern void U3CU3Ec__DisplayClass36_0_U3CAddRemoteLobbyButtonU3Eb__0_mDDAD61AAD2F07235681EC7ED0D589E90737E7CAE (void);
// 0x00000482 System.Void SessionListUI/<>c__DisplayClass36_0::<AddRemoteLobbyButton>b__1(System.Single)
extern void U3CU3Ec__DisplayClass36_0_U3CAddRemoteLobbyButtonU3Eb__1_m518798B06A807A23EED2AA7DAD78E4093671FE1D (void);
// 0x00000483 System.Void SessionListUI/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m44407C6125D25B6160597703796E724A82BC3C94 (void);
// 0x00000484 System.Void SessionListUI/<>c__DisplayClass39_0::<OnClickNewSession>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3COnClickNewSessionU3Eb__0_m6F71A4B0C372D5EA9297D52136B3B56EB915C092 (void);
// 0x00000485 System.Void SessionParticipantButton::SetPlayerInfo(PlayerInfo)
extern void SessionParticipantButton_SetPlayerInfo_mD03ACC7EDBB1581A5D3D24104AACB005E6462B84 (void);
// 0x00000486 System.Void SessionParticipantButton::.ctor()
extern void SessionParticipantButton__ctor_m65D6F449A777EF49924C3D62DB5A94AD04B762ED (void);
// 0x00000487 System.Void SharedExperienceUI::Start()
extern void SharedExperienceUI_Start_mAB4F9A79E691A53D0F02898DA9D3B1D432384797 (void);
// 0x00000488 System.Void SharedExperienceUI::OnDestroy()
extern void SharedExperienceUI_OnDestroy_m92A32985109D4D7AA7C6BAD81E0CEE262682BB31 (void);
// 0x00000489 System.Void SharedExperienceUI::OnSessionStarted()
extern void SharedExperienceUI_OnSessionStarted_mC46A57DE0119F71D742B33674251DFE9C6CD2797 (void);
// 0x0000048A System.Void SharedExperienceUI::OnSessionEnded()
extern void SharedExperienceUI_OnSessionEnded_m4CA579A6643C4B53A8E91311C2D8E8929671614A (void);
// 0x0000048B System.Void SharedExperienceUI::.ctor()
extern void SharedExperienceUI__ctor_mD7EE1FBDCC79B71182B6F616006F172B29E92D14 (void);
// 0x0000048C System.Void UnityMainThreadDispatcher::Update()
extern void UnityMainThreadDispatcher_Update_mA4581B031CF68811CC6957B72109257C5FB953BA (void);
// 0x0000048D System.Void UnityMainThreadDispatcher::Enqueue(System.Collections.IEnumerator)
extern void UnityMainThreadDispatcher_Enqueue_m3DC7FEF4D9722834BC491A5C13EF1249BF757480 (void);
// 0x0000048E System.Void UnityMainThreadDispatcher::Enqueue(System.Action)
extern void UnityMainThreadDispatcher_Enqueue_m41C8F287DAD216F87A4E3CC547B1D4E4E9760C14 (void);
// 0x0000048F System.Threading.Tasks.Task UnityMainThreadDispatcher::EnqueueAsync(System.Action)
extern void UnityMainThreadDispatcher_EnqueueAsync_m0BCC28431763AA0CBF211AA2A9170195F3454788 (void);
// 0x00000490 System.Collections.IEnumerator UnityMainThreadDispatcher::ActionWrapper(System.Action)
extern void UnityMainThreadDispatcher_ActionWrapper_mA9140D8E54337437EAA9E9E3EC19EEDBD0503EF7 (void);
// 0x00000491 System.Boolean UnityMainThreadDispatcher::Exists()
extern void UnityMainThreadDispatcher_Exists_mF21EC0F90BAE0E070BAAA14B7691E8711D469C62 (void);
// 0x00000492 UnityMainThreadDispatcher UnityMainThreadDispatcher::Instance()
extern void UnityMainThreadDispatcher_Instance_m0B17E9E95172FC70FF269E3E5EAEC2761BCB7E2A (void);
// 0x00000493 System.Void UnityMainThreadDispatcher::Awake()
extern void UnityMainThreadDispatcher_Awake_m518E1A8505DD06DAC84D9A11FE8C69E5501307AB (void);
// 0x00000494 System.Void UnityMainThreadDispatcher::OnDestroy()
extern void UnityMainThreadDispatcher_OnDestroy_mA5CE8D1450187C2F82327C120CC24BDC97B1830E (void);
// 0x00000495 System.Void UnityMainThreadDispatcher::.ctor()
extern void UnityMainThreadDispatcher__ctor_mEE26CFC31BB55E3BBC0BFBF761C2CD9A9CD3E5A1 (void);
// 0x00000496 System.Void UnityMainThreadDispatcher::.cctor()
extern void UnityMainThreadDispatcher__cctor_mBFF1612070ABC7A4DC5E10504BC6267387A1C1BF (void);
// 0x00000497 System.Void UnityMainThreadDispatcher/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m8A970E6582E1302186F63B9E37705398F45BA973 (void);
// 0x00000498 System.Void UnityMainThreadDispatcher/<>c__DisplayClass2_0::<Enqueue>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CEnqueueU3Eb__0_m9DCF63AE3D8BF2E9C9874C84E2D644E0193132CC (void);
// 0x00000499 System.Void UnityMainThreadDispatcher/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m70225B853DFA571BD40B2C66E4C5891332F22581 (void);
// 0x0000049A System.Void UnityMainThreadDispatcher/<>c__DisplayClass4_0::<EnqueueAsync>g__WrappedAction|0()
extern void U3CU3Ec__DisplayClass4_0_U3CEnqueueAsyncU3Eg__WrappedActionU7C0_mF22148EFA0776594EC9B22FE8399FC0AC64D4F2C (void);
// 0x0000049B System.Void UnityMainThreadDispatcher/<ActionWrapper>d__5::.ctor(System.Int32)
extern void U3CActionWrapperU3Ed__5__ctor_m9BDFC74813CC5245F648664F8DF0E750728D2DAB (void);
// 0x0000049C System.Void UnityMainThreadDispatcher/<ActionWrapper>d__5::System.IDisposable.Dispose()
extern void U3CActionWrapperU3Ed__5_System_IDisposable_Dispose_m6DFCB0B4116846E2C9857DAC58A7BE5E74D58FDB (void);
// 0x0000049D System.Boolean UnityMainThreadDispatcher/<ActionWrapper>d__5::MoveNext()
extern void U3CActionWrapperU3Ed__5_MoveNext_m539BDACB125A526C06DE11B32E9E3A694E95BECA (void);
// 0x0000049E System.Object UnityMainThreadDispatcher/<ActionWrapper>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActionWrapperU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7AE1A8ED6FA260A9DF0C9CC5C3E9E162DAD64BA (void);
// 0x0000049F System.Void UnityMainThreadDispatcher/<ActionWrapper>d__5::System.Collections.IEnumerator.Reset()
extern void U3CActionWrapperU3Ed__5_System_Collections_IEnumerator_Reset_m2E3C55B7DF0FAE95862C7A9C369E177D02706EFD (void);
// 0x000004A0 System.Object UnityMainThreadDispatcher/<ActionWrapper>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CActionWrapperU3Ed__5_System_Collections_IEnumerator_get_Current_m6D440EABF1C1B20808A448DEB30CD4826B28723B (void);
// 0x000004A1 System.String ButtonWithText::get_Text()
extern void ButtonWithText_get_Text_m7E91C654C173DE6B8770A20F0C39C6CE79A9876E (void);
// 0x000004A2 System.Void ButtonWithText::set_Text(System.String)
extern void ButtonWithText_set_Text_m6F27B96CECCF830CD0454D17BF4E1B3B4AD6CEBE (void);
// 0x000004A3 System.Void ButtonWithText::.ctor()
extern void ButtonWithText__ctor_m0D19225559805A6B111EE422B4B691829605C8B5 (void);
// 0x000004A4 System.Action CustomDialog::get_OnDismissed()
extern void CustomDialog_get_OnDismissed_m5C2702D3E859A2AC7C1B071F576EF94A4B8C60F1 (void);
// 0x000004A5 System.Void CustomDialog::set_OnDismissed(System.Action)
extern void CustomDialog_set_OnDismissed_mE7C544C03E66B0A2443EB0E0994B808ACC667BA4 (void);
// 0x000004A6 System.Void CustomDialog::Reset()
extern void CustomDialog_Reset_m1745F36743F9B6C9FAAEEED870E15989E08A009C (void);
// 0x000004A7 System.Void CustomDialog::Awake()
extern void CustomDialog_Awake_mFEA2048DE0B8BB82AE22D0B0CFC891CDD566C539 (void);
// 0x000004A8 System.Void CustomDialog::Show()
extern void CustomDialog_Show_mFB673DA2C340ACAE9EF64E2206672E6619225837 (void);
// 0x000004A9 System.Void CustomDialog::Dismiss()
extern void CustomDialog_Dismiss_mB29B1DF1A5A90742388ADE57D29D0E0DB12F3DA6 (void);
// 0x000004AA System.Collections.IEnumerator CustomDialog::DestroyAfterAnimation()
extern void CustomDialog_DestroyAfterAnimation_mA0AD22A995BED4B7A752C9D94D1A2E43C2BB2E39 (void);
// 0x000004AB System.Void CustomDialog::.ctor()
extern void CustomDialog__ctor_mB02A1EDF9A17AAA841172E537C364940FB247566 (void);
// 0x000004AC System.Void CustomDialog::<Awake>b__12_0()
extern void CustomDialog_U3CAwakeU3Eb__12_0_m8345EAB962AAE8EAD455204F379874645914F8B2 (void);
// 0x000004AD System.Void CustomDialog::<Awake>b__12_1()
extern void CustomDialog_U3CAwakeU3Eb__12_1_m973EBA0B955FBF92A194E18AC2662209A7BB9B76 (void);
// 0x000004AE System.Void CustomDialog::<Awake>b__12_2()
extern void CustomDialog_U3CAwakeU3Eb__12_2_mEBA8782B31F697D2A49110D20210E7415275059F (void);
// 0x000004AF System.Void CustomDialog/<DestroyAfterAnimation>d__15::.ctor(System.Int32)
extern void U3CDestroyAfterAnimationU3Ed__15__ctor_mD2283751092670C0A22A7A99E4C70DB1EE3DF3C5 (void);
// 0x000004B0 System.Void CustomDialog/<DestroyAfterAnimation>d__15::System.IDisposable.Dispose()
extern void U3CDestroyAfterAnimationU3Ed__15_System_IDisposable_Dispose_m5306FA8F42A83E8565D9C4F47120DC28C1481340 (void);
// 0x000004B1 System.Boolean CustomDialog/<DestroyAfterAnimation>d__15::MoveNext()
extern void U3CDestroyAfterAnimationU3Ed__15_MoveNext_m3477B972A5F6A01686D81F5D9E7A34598A7A3772 (void);
// 0x000004B2 System.Object CustomDialog/<DestroyAfterAnimation>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyAfterAnimationU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDA7F4DE4A0C7618858A100F4B79C7302116021C9 (void);
// 0x000004B3 System.Void CustomDialog/<DestroyAfterAnimation>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDestroyAfterAnimationU3Ed__15_System_Collections_IEnumerator_Reset_m85686D2DC4AC9CF2BF932958CBC73970125F8DC9 (void);
// 0x000004B4 System.Object CustomDialog/<DestroyAfterAnimation>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyAfterAnimationU3Ed__15_System_Collections_IEnumerator_get_Current_m83722BCB8A601C1F0F0204AA53A578920BD7F9FE (void);
// 0x000004B5 UnityEngine.GameObject CustomDialogManager::get_DialogPrefab()
extern void CustomDialogManager_get_DialogPrefab_m80014EA8741E28FC9B505885162B7BDDD0372C54 (void);
// 0x000004B6 System.Void CustomDialogManager::set_DialogPrefab(UnityEngine.GameObject)
extern void CustomDialogManager_set_DialogPrefab_m2962E9F071493B9191B3A26008B37EC6ED0D1AB9 (void);
// 0x000004B7 CustomDialog CustomDialogManager::SpawnDialog(CustomDialogManager/Policy,UnityEngine.GameObject)
extern void CustomDialogManager_SpawnDialog_m4977B6F4B1338355E95E6944915B2BEC22124F0F (void);
// 0x000004B8 System.Void CustomDialogManager::OnDialogDismissed()
extern void CustomDialogManager_OnDialogDismissed_mCAEAF70EED3619951B6FA3EDEE3047992EBFEAF8 (void);
// 0x000004B9 System.Void CustomDialogManager::.ctor()
extern void CustomDialogManager__ctor_m1F41AC17A377D4CE5CF7F751427D41A22129325C (void);
// 0x000004BA System.String LoadingIndicator::get_LoadingText()
extern void LoadingIndicator_get_LoadingText_m6B67F46055FC6EC65B00AC2FA27830D0F20CC46A (void);
// 0x000004BB System.Void LoadingIndicator::set_LoadingText(System.String)
extern void LoadingIndicator_set_LoadingText_m06A7FB0939FBA07E0935F8769DA1104338D37ECA (void);
// 0x000004BC System.Void LoadingIndicator::Update()
extern void LoadingIndicator_Update_mDC96632C85E29109FBD8B0EE34D8587AF1D521F9 (void);
// 0x000004BD System.Void LoadingIndicator::.ctor()
extern void LoadingIndicator__ctor_mDE8A60DBC8694C3DDCAF03D0E62CBEF9CA956748 (void);
// 0x000004BE System.Void Tooltip::Start()
extern void Tooltip_Start_m4AB8523D7CBBB7E82F57487B92149DA47D3F6C82 (void);
// 0x000004BF System.String Tooltip::get_Caption()
extern void Tooltip_get_Caption_m915D134C9A5D83C22CDFCF25F4BA069563396C6F (void);
// 0x000004C0 System.Void Tooltip::set_Caption(System.String)
extern void Tooltip_set_Caption_m259D64C96BEC060427008FB6E07605E783856D48 (void);
// 0x000004C1 UnityEngine.GameObject Tooltip::get_TargetObject()
extern void Tooltip_get_TargetObject_m8B7E7C39C41D59AFCE338D66973136A0ABED5D47 (void);
// 0x000004C2 System.Void Tooltip::set_TargetObject(UnityEngine.GameObject)
extern void Tooltip_set_TargetObject_mB251443FF03E012D232960ED25312789CE58E0E0 (void);
// 0x000004C3 UnityEngine.Collider Tooltip::get_TargetCollider()
extern void Tooltip_get_TargetCollider_mC0118A76F610FB266A4B470F80B9ABD07EBB6D6D (void);
// 0x000004C4 System.Void Tooltip::OnEnable()
extern void Tooltip_OnEnable_m7EC9D5AE25908D9F63DC334C9DEFE47924733739 (void);
// 0x000004C5 System.Void Tooltip::Update()
extern void Tooltip_Update_m7369D6BF08179D8BAC3C69E974A006668D91A3E2 (void);
// 0x000004C6 System.Void Tooltip::.ctor()
extern void Tooltip__ctor_m839D8DEE46301BEAA0E8102131A4E7A597860837 (void);
// 0x000004C7 System.Void IDropdownItem::set_IsExpanded(System.Boolean)
// 0x000004C8 System.Void IDropdownItem::set_IsLeaf(System.Boolean)
// 0x000004C9 System.Void IDropdownItem::set_Level(System.Int32)
// 0x000004CA System.Single VirtualizedScrollRectDropdown::get_Scroll()
extern void VirtualizedScrollRectDropdown_get_Scroll_m0EDA6499312232E62ED0FEBD6869DCD097121BE2 (void);
// 0x000004CB System.Void VirtualizedScrollRectDropdown::set_Scroll(System.Single)
extern void VirtualizedScrollRectDropdown_set_Scroll_m28B7F227B3BB1BF9B355CBB62A9EE5B899796E72 (void);
// 0x000004CC System.Int32 VirtualizedScrollRectDropdown::get_NumNodes()
extern void VirtualizedScrollRectDropdown_get_NumNodes_m89452939479227C2054143D101FE08DE0D893894 (void);
// 0x000004CD System.Int32 VirtualizedScrollRectDropdown::get_NumExpandedNodes()
extern void VirtualizedScrollRectDropdown_get_NumExpandedNodes_m9AB3E4E24C7B4D5E318718796978EB0040B890F1 (void);
// 0x000004CE System.Int32 VirtualizedScrollRectDropdown::get_PartiallyVisibleCount()
extern void VirtualizedScrollRectDropdown_get_PartiallyVisibleCount_m15CAFBC9EF70778D1011AA4ADF094ECE6182D101 (void);
// 0x000004CF System.Int32 VirtualizedScrollRectDropdown::get_TotallyVisibleCount()
extern void VirtualizedScrollRectDropdown_get_TotallyVisibleCount_m9915D15AC7EB613C74069E8D4FE20732ED71A51B (void);
// 0x000004D0 System.Single VirtualizedScrollRectDropdown::get_ScreenItemCountF()
extern void VirtualizedScrollRectDropdown_get_ScreenItemCountF_m879F9A52A6889130719C531A2E5E865A12237A3A (void);
// 0x000004D1 System.Single VirtualizedScrollRectDropdown::get_MaxScroll()
extern void VirtualizedScrollRectDropdown_get_MaxScroll_mFE4F3BF50B2BA8E7F9E817DB40AE7EFCB9536FA6 (void);
// 0x000004D2 System.Action`2<UnityEngine.GameObject,System.Int32> VirtualizedScrollRectDropdown::get_OnAssignedNode()
extern void VirtualizedScrollRectDropdown_get_OnAssignedNode_mE666EAE3FB5884D758864BB2443652520A6D4373 (void);
// 0x000004D3 System.Void VirtualizedScrollRectDropdown::set_OnAssignedNode(System.Action`2<UnityEngine.GameObject,System.Int32>)
extern void VirtualizedScrollRectDropdown_set_OnAssignedNode_m4D91EA386A72AC501969C29EB9AC0B25D7A79C4C (void);
// 0x000004D4 System.Action`2<UnityEngine.GameObject,System.Int32> VirtualizedScrollRectDropdown::get_OnUnassignedNode()
extern void VirtualizedScrollRectDropdown_get_OnUnassignedNode_m0042C5648AA43102AD358268C6C1B581258306AA (void);
// 0x000004D5 System.Void VirtualizedScrollRectDropdown::set_OnUnassignedNode(System.Action`2<UnityEngine.GameObject,System.Int32>)
extern void VirtualizedScrollRectDropdown_set_OnUnassignedNode_m179D28701D75137F915B4911F275DC560C343D2A (void);
// 0x000004D6 System.Int32 VirtualizedScrollRectDropdown::listPositionToDropdownNodeID(System.Int32)
extern void VirtualizedScrollRectDropdown_listPositionToDropdownNodeID_mB8FEA785BE60DEFB38F4F34692093300F353CBC7 (void);
// 0x000004D7 System.Int32 VirtualizedScrollRectDropdown::dropdownNodeIDToListPosition(System.Int32)
extern void VirtualizedScrollRectDropdown_dropdownNodeIDToListPosition_m7C5E76D59E05E7B717732FA843400703A9038F27 (void);
// 0x000004D8 VirtualizedScrollRectDropdown/DropdownNode VirtualizedScrollRectDropdown::GetNode(System.Int32)
extern void VirtualizedScrollRectDropdown_GetNode_m83E48D3AF78FD30D1D7B45DEE976A100687B422D (void);
// 0x000004D9 UnityEngine.Vector3 VirtualizedScrollRectDropdown::ItemLocation(System.Int32)
extern void VirtualizedScrollRectDropdown_ItemLocation_m8CABE1610825C3F0955B6D706F5FF9F61921818D (void);
// 0x000004DA System.Single VirtualizedScrollRectDropdown::PosToScroll(System.Single)
extern void VirtualizedScrollRectDropdown_PosToScroll_m074A78D61B4CA81498507E1A5C0DDB6FA0056637 (void);
// 0x000004DB System.Single VirtualizedScrollRectDropdown::ScrollToPos(System.Single)
extern void VirtualizedScrollRectDropdown_ScrollToPos_m5CCF23AA723C1A9803C1DF9D6708BA35E1A2D7DF (void);
// 0x000004DC System.Void VirtualizedScrollRectDropdown::OnValidate()
extern void VirtualizedScrollRectDropdown_OnValidate_mCEB90873B16527133A0C0686251789E86F78C821 (void);
// 0x000004DD System.Void VirtualizedScrollRectDropdown::Start()
extern void VirtualizedScrollRectDropdown_Start_mC5536132E5EBC4A4A9C233EC5CBA8D0AD693EFC4 (void);
// 0x000004DE System.Collections.IEnumerator VirtualizedScrollRectDropdown::EndOfFrameInitialize()
extern void VirtualizedScrollRectDropdown_EndOfFrameInitialize_mFC6F555256EF7D188CD9A7BD94BB2B66E7FC5326 (void);
// 0x000004DF System.Void VirtualizedScrollRectDropdown::BakeCachedValues()
extern void VirtualizedScrollRectDropdown_BakeCachedValues_m898462DB0D1C59FDE3F792DDF8AC12CD4686196E (void);
// 0x000004E0 System.Void VirtualizedScrollRectDropdown::Initialize()
extern void VirtualizedScrollRectDropdown_Initialize_m648D209FE743ADEFA7874A7CC1AECCFEAA450216 (void);
// 0x000004E1 System.Void VirtualizedScrollRectDropdown::InitializePool()
extern void VirtualizedScrollRectDropdown_InitializePool_mC9CDDEDFA34D953D83A5CB6A214C5276EC92D3D4 (void);
// 0x000004E2 System.Void VirtualizedScrollRectDropdown::UpdateScrollView(System.Single)
extern void VirtualizedScrollRectDropdown_UpdateScrollView_m9A3C90BF2D592FC4F9CE01B58BE85B6623C27F64 (void);
// 0x000004E3 System.Void VirtualizedScrollRectDropdown::UpdateScroll(System.Single)
extern void VirtualizedScrollRectDropdown_UpdateScroll_mEAF1C48560E1EA0054D3D63E5B6B7C0109AB3E24 (void);
// 0x000004E4 System.Void VirtualizedScrollRectDropdown::UpdateVisibleNodes()
extern void VirtualizedScrollRectDropdown_UpdateVisibleNodes_mDF25364E95139478E0DD1E179AC58D87661AB55D (void);
// 0x000004E5 System.Void VirtualizedScrollRectDropdown::MakeInvisible(System.Int32)
extern void VirtualizedScrollRectDropdown_MakeInvisible_m343F25C882433EB29D5E8F983D7B69D0B3CE0042 (void);
// 0x000004E6 System.Void VirtualizedScrollRectDropdown::MakeVisible(System.Int32)
extern void VirtualizedScrollRectDropdown_MakeVisible_m67D6C0B88C75D3908E693B4D9137A12E9234D510 (void);
// 0x000004E7 System.Void VirtualizedScrollRectDropdown::ReassignNode(System.Int32,System.Int32)
extern void VirtualizedScrollRectDropdown_ReassignNode_m4B700DEBFB811E975862543610F39C7FE474802F (void);
// 0x000004E8 System.Void VirtualizedScrollRectDropdown::UpdateNumNodes()
extern void VirtualizedScrollRectDropdown_UpdateNumNodes_m1F2E759D190739A1B5EAE1DBF3EE05E5BD448C8F (void);
// 0x000004E9 System.Boolean VirtualizedScrollRectDropdown::TryGetVisible(System.Int32,UnityEngine.GameObject&)
extern void VirtualizedScrollRectDropdown_TryGetVisible_mB3EE7699315018871D95148B86ECEA91F2EDEC4F (void);
// 0x000004EA System.Boolean VirtualizedScrollRectDropdown::TryGetVisibleFromID(System.Int32,UnityEngine.GameObject&)
extern void VirtualizedScrollRectDropdown_TryGetVisibleFromID_m52548A461055F9BBC7EC17BDF59D8494432C5153 (void);
// 0x000004EB System.Int32 VirtualizedScrollRectDropdown::AddNode(System.Int32)
extern void VirtualizedScrollRectDropdown_AddNode_m6191B32D49D86B283C32ED1A7A00C5841E11286D (void);
// 0x000004EC System.Void VirtualizedScrollRectDropdown::RemoveNode(System.Int32)
extern void VirtualizedScrollRectDropdown_RemoveNode_mD61094D8B0E8E23CE6F39F245CDBABA2CF7E1325 (void);
// 0x000004ED System.Void VirtualizedScrollRectDropdown::SetNodeExpanded(System.Int32,System.Boolean)
extern void VirtualizedScrollRectDropdown_SetNodeExpanded_mDDEF7271F8811D9E055A75F6D2BBA7F993F2F0F2 (void);
// 0x000004EE System.Void VirtualizedScrollRectDropdown::ToggleNodeExpanded(System.Int32)
extern void VirtualizedScrollRectDropdown_ToggleNodeExpanded_m9394EDFE0B1BDE4F463586768CB368C376187EEC (void);
// 0x000004EF System.Void VirtualizedScrollRectDropdown::.ctor()
extern void VirtualizedScrollRectDropdown__ctor_m10F7279FBB2C58F96FCAC67E2BF21C84C4A950A8 (void);
// 0x000004F0 System.Void VirtualizedScrollRectDropdown::<Initialize>b__56_0(UnityEngine.Vector2)
extern void VirtualizedScrollRectDropdown_U3CInitializeU3Eb__56_0_m3796DA571D11323CA1E8A21108A1EE77C163E4B4 (void);
// 0x000004F1 System.Boolean VirtualizedScrollRectDropdown/DropdownNode::get_isRoot()
extern void DropdownNode_get_isRoot_m200E3BF7F28A8A6BB3FD29F9746FA82A378E60D3 (void);
// 0x000004F2 System.Boolean VirtualizedScrollRectDropdown/DropdownNode::get_isLeaf()
extern void DropdownNode_get_isLeaf_m017C045AB5B419DAE15B656E0A75569E0717A900 (void);
// 0x000004F3 System.Int32 VirtualizedScrollRectDropdown/DropdownNode::get_Level()
extern void DropdownNode_get_Level_m6BEF8585A0BD48702D851FA2D19EBF45CF1C4CA7 (void);
// 0x000004F4 System.Collections.Generic.IEnumerable`1<VirtualizedScrollRectDropdown/DropdownNode> VirtualizedScrollRectDropdown/DropdownNode::TraverseTree()
extern void DropdownNode_TraverseTree_m62513DD47C7378252253C074307FD4B9A6324805 (void);
// 0x000004F5 System.Collections.Generic.IEnumerable`1<VirtualizedScrollRectDropdown/DropdownNode> VirtualizedScrollRectDropdown/DropdownNode::TraverseExpandedTree()
extern void DropdownNode_TraverseExpandedTree_m1B9AF0357A68DF72DC0275BE3968A2959BA1D361 (void);
// 0x000004F6 System.Int32 VirtualizedScrollRectDropdown/DropdownNode::Size()
extern void DropdownNode_Size_m9BCA339F8FE44107C189300B80E032DB3B78C127 (void);
// 0x000004F7 System.Int32 VirtualizedScrollRectDropdown/DropdownNode::ExpandedSize()
extern void DropdownNode_ExpandedSize_m8B4AA83C833A37843C7BA7A5F2F8DD5639172DD1 (void);
// 0x000004F8 System.Void VirtualizedScrollRectDropdown/DropdownNode::.ctor()
extern void DropdownNode__ctor_mAE23DCD81168AC6CA9EDF1AF64E3D8915A3CFAC8 (void);
// 0x000004F9 System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::.ctor(System.Int32)
extern void U3CTraverseTreeU3Ed__10__ctor_mF67E4A40887D006B397E7316DB1E9D11E5FAE3C9 (void);
// 0x000004FA System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::System.IDisposable.Dispose()
extern void U3CTraverseTreeU3Ed__10_System_IDisposable_Dispose_mF39A701F2613F5BA2BCFDFC7CFED9D7D20171501 (void);
// 0x000004FB System.Boolean VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::MoveNext()
extern void U3CTraverseTreeU3Ed__10_MoveNext_m3D8D90118261EAB1E14D72938765183589A50307 (void);
// 0x000004FC System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::<>m__Finally1()
extern void U3CTraverseTreeU3Ed__10_U3CU3Em__Finally1_m71734ACE7138881A27D9CA09B9FEA88E51BCE9A1 (void);
// 0x000004FD System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::<>m__Finally2()
extern void U3CTraverseTreeU3Ed__10_U3CU3Em__Finally2_m89C00DF0B65E208EAC012304034056E903637216 (void);
// 0x000004FE VirtualizedScrollRectDropdown/DropdownNode VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::System.Collections.Generic.IEnumerator<VirtualizedScrollRectDropdown.DropdownNode>.get_Current()
extern void U3CTraverseTreeU3Ed__10_System_Collections_Generic_IEnumeratorU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_get_Current_m8F34545F2F847C395EE7F3284502C09B78190529 (void);
// 0x000004FF System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTraverseTreeU3Ed__10_System_Collections_IEnumerator_Reset_mD68F98166DE34F035088C9B2269D467C73B27D07 (void);
// 0x00000500 System.Object VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTraverseTreeU3Ed__10_System_Collections_IEnumerator_get_Current_m8E192FB0D0B9C8BFD267AB977EE73CD9ADF6CAA3 (void);
// 0x00000501 System.Collections.Generic.IEnumerator`1<VirtualizedScrollRectDropdown/DropdownNode> VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::System.Collections.Generic.IEnumerable<VirtualizedScrollRectDropdown.DropdownNode>.GetEnumerator()
extern void U3CTraverseTreeU3Ed__10_System_Collections_Generic_IEnumerableU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_GetEnumerator_m0A128FC02F2E97F9568F54AEE20C52A14E3619D7 (void);
// 0x00000502 System.Collections.IEnumerator VirtualizedScrollRectDropdown/DropdownNode/<TraverseTree>d__10::System.Collections.IEnumerable.GetEnumerator()
extern void U3CTraverseTreeU3Ed__10_System_Collections_IEnumerable_GetEnumerator_mA419D41D79432CA8560C858616CF45CCE9246ECE (void);
// 0x00000503 System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::.ctor(System.Int32)
extern void U3CTraverseExpandedTreeU3Ed__11__ctor_m26B71E23ABBCD4EC693F495B9BE031005FFF6494 (void);
// 0x00000504 System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::System.IDisposable.Dispose()
extern void U3CTraverseExpandedTreeU3Ed__11_System_IDisposable_Dispose_m58213CE5125123D080C4A8FB591A02176DEA769F (void);
// 0x00000505 System.Boolean VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::MoveNext()
extern void U3CTraverseExpandedTreeU3Ed__11_MoveNext_m64873EDDC334029D8B4D65CB00DD5CF899DE34F9 (void);
// 0x00000506 System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::<>m__Finally1()
extern void U3CTraverseExpandedTreeU3Ed__11_U3CU3Em__Finally1_m065CA0B97F9478817448FFBE05881A3F99CB95F9 (void);
// 0x00000507 System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::<>m__Finally2()
extern void U3CTraverseExpandedTreeU3Ed__11_U3CU3Em__Finally2_mB6F15E0734678D2608A1F8B43174AB603BBE8A8B (void);
// 0x00000508 VirtualizedScrollRectDropdown/DropdownNode VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::System.Collections.Generic.IEnumerator<VirtualizedScrollRectDropdown.DropdownNode>.get_Current()
extern void U3CTraverseExpandedTreeU3Ed__11_System_Collections_Generic_IEnumeratorU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_get_Current_mA1D521CBA0E97D8EDB12ECE0E3A3BF4EA8D48BD8 (void);
// 0x00000509 System.Void VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::System.Collections.IEnumerator.Reset()
extern void U3CTraverseExpandedTreeU3Ed__11_System_Collections_IEnumerator_Reset_m56B0F7F7C55D5FEFDC753588F18488093A3533B5 (void);
// 0x0000050A System.Object VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CTraverseExpandedTreeU3Ed__11_System_Collections_IEnumerator_get_Current_mB59796F1B37FF3497144D84A6A2F8E8E001B16D7 (void);
// 0x0000050B System.Collections.Generic.IEnumerator`1<VirtualizedScrollRectDropdown/DropdownNode> VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::System.Collections.Generic.IEnumerable<VirtualizedScrollRectDropdown.DropdownNode>.GetEnumerator()
extern void U3CTraverseExpandedTreeU3Ed__11_System_Collections_Generic_IEnumerableU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_GetEnumerator_m2DD7F35FD932DE1F5931B2CC0BFBC0AE6F8EA465 (void);
// 0x0000050C System.Collections.IEnumerator VirtualizedScrollRectDropdown/DropdownNode/<TraverseExpandedTree>d__11::System.Collections.IEnumerable.GetEnumerator()
extern void U3CTraverseExpandedTreeU3Ed__11_System_Collections_IEnumerable_GetEnumerator_m1A403747CD73673217F6B52F158CE1310B32ACB7 (void);
// 0x0000050D System.Void VirtualizedScrollRectDropdown/<EndOfFrameInitialize>d__54::.ctor(System.Int32)
extern void U3CEndOfFrameInitializeU3Ed__54__ctor_mAEFB1DB6CFC7EC0849EA8BB97EDDA8D84BDBB345 (void);
// 0x0000050E System.Void VirtualizedScrollRectDropdown/<EndOfFrameInitialize>d__54::System.IDisposable.Dispose()
extern void U3CEndOfFrameInitializeU3Ed__54_System_IDisposable_Dispose_m0E11B05B209B989EAB93E0BE63D1121FE3321E33 (void);
// 0x0000050F System.Boolean VirtualizedScrollRectDropdown/<EndOfFrameInitialize>d__54::MoveNext()
extern void U3CEndOfFrameInitializeU3Ed__54_MoveNext_m533EAD14FEC8BA916B74C26EC49D938E983A0505 (void);
// 0x00000510 System.Object VirtualizedScrollRectDropdown/<EndOfFrameInitialize>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEndOfFrameInitializeU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12FCF0379F696C59EB3A887D0C7CDF90A161F210 (void);
// 0x00000511 System.Void VirtualizedScrollRectDropdown/<EndOfFrameInitialize>d__54::System.Collections.IEnumerator.Reset()
extern void U3CEndOfFrameInitializeU3Ed__54_System_Collections_IEnumerator_Reset_mB97056C82284E24994CBDDA6CD987122D14EFB36 (void);
// 0x00000512 System.Object VirtualizedScrollRectDropdown/<EndOfFrameInitialize>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CEndOfFrameInitializeU3Ed__54_System_Collections_IEnumerator_get_Current_m839FE56810966248491B02954A224C93BFDCCA7C (void);
// 0x00000513 System.Int32 Window::get_CurrentViewIdx()
extern void Window_get_CurrentViewIdx_m80116C0F124D5ADAC8F2433D92DA9239582FF688 (void);
// 0x00000514 System.Void Window::set_CurrentViewIdx(System.Int32)
extern void Window_set_CurrentViewIdx_mE2ECFA62E2A5B8686BFF43D6B60B2AD2C1F6534E (void);
// 0x00000515 System.Void Window::Close()
extern void Window_Close_m23280372D65090C0FFD3AF2498E9E0900F80607D (void);
// 0x00000516 System.Void Window::Open()
extern void Window_Open_mD928FAA24D4F13587362E04022968C000D9754A3 (void);
// 0x00000517 System.Void Window::BringIntoFocus()
extern void Window_BringIntoFocus_m99E44EECB3E0A338AF1E18192E051732FA387F21 (void);
// 0x00000518 System.Void Window::Start()
extern void Window_Start_mE735AD36D179FD96AE86617476E0456043EEA6F3 (void);
// 0x00000519 System.Void Window::Update()
extern void Window_Update_m6D6FEAE3CF208B12341F6BB2281823AAD35B9851 (void);
// 0x0000051A System.Void Window::.ctor()
extern void Window__ctor_mFB056C087DFE57E41DEB633825899473B59E9B0E (void);
// 0x0000051B System.Byte[] SerializationHelper::Serialize(T)
// 0x0000051C T SerializationHelper::Deserialize(System.Byte[])
// 0x0000051D System.Void SerializationHelper::SerializeNullable(Unity.Netcode.BufferSerializer`1<T>,SN`1<UnityEngine.Vector3>&)
// 0x0000051E System.Void SerializationHelper::SerializeNullable(Unity.Netcode.BufferSerializer`1<T>,SN`1<UnityEngine.Quaternion>&)
// 0x0000051F System.Void SerializationHelper::SerializeNullable(Unity.Netcode.BufferSerializer`1<T>,System.Nullable`1<UnityEngine.Vector3>&)
// 0x00000520 System.Void SerializationHelper::SerializeNullable(Unity.Netcode.BufferSerializer`1<T>,System.Nullable`1<UnityEngine.Quaternion>&)
// 0x00000521 System.Void SerializationHelper::SerializeNullable(Unity.Netcode.BufferSerializer`1<T>,System.Nullable`1<System.Int32>&)
// 0x00000522 System.Void SerializationHelper::SerializeNullable(Unity.Netcode.BufferSerializer`1<T>,System.Nullable`1<LocalTransform>&)
// 0x00000523 System.Net.IPAddress NetworkHelper::GetLocalIPAddress()
extern void NetworkHelper_GetLocalIPAddress_mEB955EA2B25CCD3AB946232A4C7EA759B5CEF9D6 (void);
// 0x00000524 System.Threading.Tasks.Task NetworkHelper::InitializeRemoteServices()
extern void NetworkHelper_InitializeRemoteServices_mADDBB8490D8C03F86F96FEEAD5F9763FFB8552DD (void);
// 0x00000525 System.Void NetworkHelper::.ctor()
extern void NetworkHelper__ctor_mB61C7D76612961EE5B815A16643BBFD88797E215 (void);
// 0x00000526 System.Void NetworkHelper/<InitializeRemoteServices>d__2::MoveNext()
extern void U3CInitializeRemoteServicesU3Ed__2_MoveNext_mA4D2A43BC7AAB416CD8F4B72BDED1F724DA30C81 (void);
// 0x00000527 System.Void NetworkHelper/<InitializeRemoteServices>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInitializeRemoteServicesU3Ed__2_SetStateMachine_mE82F2F99EBC77877DEF695C1F0BD36DA2C56C76E (void);
// 0x00000528 System.Boolean TreeView.ITreeIMGUIData::get_isExpanded()
// 0x00000529 System.Void TreeView.ITreeIMGUIData::set_isExpanded(System.Boolean)
// 0x0000052A System.Void TreeView.TreeNode`1::.ctor(T)
// 0x0000052B System.Void TreeView.TreeNode`1::.ctor(T,TreeView.TreeNode`1<T>)
// 0x0000052C System.Int32 TreeView.TreeNode`1::get_Level()
// 0x0000052D System.Int32 TreeView.TreeNode`1::get_Count()
// 0x0000052E System.Boolean TreeView.TreeNode`1::get_IsRoot()
// 0x0000052F System.Boolean TreeView.TreeNode`1::get_IsLeaf()
// 0x00000530 T TreeView.TreeNode`1::get_Data()
// 0x00000531 TreeView.TreeNode`1<T> TreeView.TreeNode`1::get_Parent()
// 0x00000532 System.Collections.Generic.List`1<TreeView.TreeNode`1<T>> TreeView.TreeNode`1::get_Children()
// 0x00000533 System.Int32 TreeView.TreeNode`1::get_Size()
// 0x00000534 TreeView.TreeNode`1<T> TreeView.TreeNode`1::get_Item(System.Int32)
// 0x00000535 System.Void TreeView.TreeNode`1::Clear()
// 0x00000536 TreeView.TreeNode`1<T> TreeView.TreeNode`1::AddChild(T)
// 0x00000537 System.Void TreeView.TreeNode`1::AddChild(TreeView.TreeNode`1<T>)
// 0x00000538 System.Boolean TreeView.TreeNode`1::HasChild(T)
// 0x00000539 TreeView.TreeNode`1<T> TreeView.TreeNode`1::FindInChildren(T)
// 0x0000053A System.Boolean TreeView.TreeNode`1::RemoveChild(TreeView.TreeNode`1<T>)
// 0x0000053B System.Collections.Generic.IEnumerator`1<TreeView.TreeNode`1<T>> TreeView.TreeNode`1::GetEnumerator()
// 0x0000053C System.Collections.Generic.IEnumerable`1<T> TreeView.TreeNode`1::NodesData()
// 0x0000053D System.Collections.Generic.List`1<TreeView.TreeNode`1<T>> TreeView.TreeNode`1::get_NodeList()
// 0x0000053E System.Void TreeView.TreeNode`1::Traverse(TreeView.TreeNode`1/TraversalDataDelegate<T>)
// 0x0000053F System.Void TreeView.TreeNode`1::Traverse(TreeView.TreeNode`1/TraversalNodeDelegate<T>)
// 0x00000540 System.Void TreeView.TreeNode`1/TraversalDataDelegate::.ctor(System.Object,System.IntPtr)
// 0x00000541 System.Boolean TreeView.TreeNode`1/TraversalDataDelegate::Invoke(T)
// 0x00000542 System.IAsyncResult TreeView.TreeNode`1/TraversalDataDelegate::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000543 System.Boolean TreeView.TreeNode`1/TraversalDataDelegate::EndInvoke(System.IAsyncResult)
// 0x00000544 System.Void TreeView.TreeNode`1/TraversalNodeDelegate::.ctor(System.Object,System.IntPtr)
// 0x00000545 System.Boolean TreeView.TreeNode`1/TraversalNodeDelegate::Invoke(TreeView.TreeNode`1<T>)
// 0x00000546 System.IAsyncResult TreeView.TreeNode`1/TraversalNodeDelegate::BeginInvoke(TreeView.TreeNode`1<T>,System.AsyncCallback,System.Object)
// 0x00000547 System.Boolean TreeView.TreeNode`1/TraversalNodeDelegate::EndInvoke(System.IAsyncResult)
// 0x00000548 System.Void TreeView.TreeNode`1/<GetEnumerator>d__32::.ctor(System.Int32)
// 0x00000549 System.Void TreeView.TreeNode`1/<GetEnumerator>d__32::System.IDisposable.Dispose()
// 0x0000054A System.Boolean TreeView.TreeNode`1/<GetEnumerator>d__32::MoveNext()
// 0x0000054B System.Void TreeView.TreeNode`1/<GetEnumerator>d__32::<>m__Finally1()
// 0x0000054C System.Void TreeView.TreeNode`1/<GetEnumerator>d__32::<>m__Finally2()
// 0x0000054D TreeView.TreeNode`1<T> TreeView.TreeNode`1/<GetEnumerator>d__32::System.Collections.Generic.IEnumerator<TreeView.TreeNode<T>>.get_Current()
// 0x0000054E System.Void TreeView.TreeNode`1/<GetEnumerator>d__32::System.Collections.IEnumerator.Reset()
// 0x0000054F System.Object TreeView.TreeNode`1/<GetEnumerator>d__32::System.Collections.IEnumerator.get_Current()
// 0x00000550 System.Void TreeView.TreeNode`1/<NodesData>d__33::.ctor(System.Int32)
// 0x00000551 System.Void TreeView.TreeNode`1/<NodesData>d__33::System.IDisposable.Dispose()
// 0x00000552 System.Boolean TreeView.TreeNode`1/<NodesData>d__33::MoveNext()
// 0x00000553 System.Void TreeView.TreeNode`1/<NodesData>d__33::<>m__Finally1()
// 0x00000554 System.Void TreeView.TreeNode`1/<NodesData>d__33::<>m__Finally2()
// 0x00000555 T TreeView.TreeNode`1/<NodesData>d__33::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000556 System.Void TreeView.TreeNode`1/<NodesData>d__33::System.Collections.IEnumerator.Reset()
// 0x00000557 System.Object TreeView.TreeNode`1/<NodesData>d__33::System.Collections.IEnumerator.get_Current()
// 0x00000558 System.Collections.Generic.IEnumerator`1<T> TreeView.TreeNode`1/<NodesData>d__33::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000559 System.Collections.IEnumerator TreeView.TreeNode`1/<NodesData>d__33::System.Collections.IEnumerable.GetEnumerator()
// 0x0000055A System.Boolean Unity.Multiplayer.Samples.Utilities.ClientAuthority.ClientNetworkTransform::OnIsServerAuthoritative()
extern void ClientNetworkTransform_OnIsServerAuthoritative_m0270CDAF368412EFF55D0CDD8D633127DDDF1753 (void);
// 0x0000055B System.Void Unity.Multiplayer.Samples.Utilities.ClientAuthority.ClientNetworkTransform::.ctor()
extern void ClientNetworkTransform__ctor_m5DFB7B5CCA0BCCB07B3F58E7C2BCD2E648043AAC (void);
// 0x0000055C System.Void Unity.Multiplayer.Samples.Utilities.ClientAuthority.ClientNetworkTransform::__initializeVariables()
extern void ClientNetworkTransform___initializeVariables_mF55990F97595B98244B63CB75F908420B8A0C9E0 (void);
// 0x0000055D System.String Unity.Multiplayer.Samples.Utilities.ClientAuthority.ClientNetworkTransform::__getTypeName()
extern void ClientNetworkTransform___getTypeName_mD015C2C216565C5492A4A10B9C77978255ED1FD7 (void);
// 0x0000055E System.Void Microsoft.MixedReality.GraphicsTools.Samples.MaterialGallery.Billboard::LateUpdate()
extern void Billboard_LateUpdate_m66475B437AE6267C7A5BB47DF60B27A7DECF8819 (void);
// 0x0000055F System.Void Microsoft.MixedReality.GraphicsTools.Samples.MaterialGallery.Billboard::.ctor()
extern void Billboard__ctor_m6FE92E731006932969545C0B8F83A974AFAFE62E (void);
// 0x00000560 System.Void Microsoft.MixedReality.GraphicsTools.Samples.MaterialGallery.MaterialMatrix::BuildMatrix()
extern void MaterialMatrix_BuildMatrix_m7A72C99F4F1BA20FA27476E421EA9DD15B32458F (void);
// 0x00000561 System.Void Microsoft.MixedReality.GraphicsTools.Samples.MaterialGallery.MaterialMatrix::.ctor()
extern void MaterialMatrix__ctor_m81B93D4D4BA51773CAC69423675EBB8596A24415 (void);
// 0x00000562 System.Void __GEN.NetworkVariableSerializationHelper::InitializeSerialization()
extern void NetworkVariableSerializationHelper_InitializeSerialization_mA4F17C9D8F4934C552263003358AF8CC0E55E6E3 (void);
static Il2CppMethodPointer s_methodPointers[1378] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AnswerButton_SetFeedbackMode_m3947B2F3C278BE2AD574BD4455141D0EA4A3DAB8,
	AnswerButton_SetAnswerText_m52E83EFA95C22D414889294E29E5B98611BF8E9E,
	AnswerButton_PreferredHeight_m057E8EA02E99B0888845B231BF4BC2CDD987FD18,
	AnswerButton__ctor_m9AE7119B8134C31A6CE1DE7870EAA67A4575121D,
	BlendShapeAnimation_get_LoopAnimation_mAFB0C4E5FDE9CA15A9A9900163E0632B6C91B170,
	BlendShapeAnimation_set_LoopAnimation_m463F547FCCED018DDD2D2E65CE815C85C911A6EE,
	BlendShapeAnimation_InitializeBlendShapes_m4D9C11D15A511B5663288C12D392392916BA1374,
	BlendShapeAnimation_Start_m20AC607E3B2429DDB9881C486D37172E069A9888,
	BlendShapeAnimation_UpdateBlendShapes_m919CB0C01F89C0F4905CEC1B2C59E412BB4F6F51,
	BlendShapeAnimation_MaxCurrentIndex_m05E6738698DAEEE752C8C9004D58FA7689683C1A,
	BlendShapeAnimation_get_CurrentTime_m51AEEC7AA0CE3C1989AF9012B34E786A768DC37D,
	BlendShapeAnimation_set_CurrentTime_m38F5903205C5F9C01F995FB00314BD3CC96750BA,
	BlendShapeAnimation__ctor_m06A787150E2ECB0D7F40B45BFD07429D6228FD41,
	NULL,
	NULL,
	NULL,
	NULL,
	ApplicationDefaults__cctor_mC0677F9AF9940565F0C38342CA2D11C0921A01A3,
	LayerScriptableObject_get_Type_m44C1311C5CEC5CDAC591B597E3194C3F1A75845A,
	LayerScriptableObject_initFromDict_m313084DB13C08B57AB5E756658EC9DECA707BFBC,
	LayerScriptableObject__ctor_m8F32E5A82BDCC393BA36CE8689B00D9CFF5AAC62,
	LocalTransform__ctor_m5B614313B67478392333D4BF0AC97F9FF5E16B3D,
	LocalTransform__ctor_m6236FFC8C68D10344A687DED13BAA193C967A635,
	NULL,
	NULL,
	NodeState__ctor_mC1FF3DE9C1F10F505B0312DB606B06565896CE66,
	NULL,
	LayerState__ctor_m5B9B1C3D19684026BECDDB7B0C3CACB60CCBEDA3,
	NULL,
	AnimationState__ctor_m10E9FDCF07CD51207C96EB1A68D45B440B9A3945,
	NULL,
	ClippingPlaneState__ctor_m4A80255E1896F1B694E6A4C4BD5C9DD8906AEA23,
	NULL,
	UIState__ctor_mA4BC93EA34AF2D6D737E5BB3B1C390ED888F3537,
	NULL,
	ModelInstanceState__ctor_m24687CF3EAA877A1DA3FC421C3E5C81ACC6DE2E4,
	NULL,
	LocalTransformTransition__ctor_m41B4AAF4D35E8CCDE2F226C8BB45176E33AB7F16,
	NULL,
	NodeStateTransition__ctor_m4CAD17CB9A6B96D309058A92A4064C05C255D9D5,
	NULL,
	LayerStateTransition__ctor_m54D9FE611A76E270CE5B111EE32E410E353AB6A3,
	NULL,
	AnimationStateTransition__ctor_m1871B1075DBEE0EBE82BB627D2D47846BA696AE9,
	NULL,
	ClippingPlaneStateTransition__ctor_m230F240FCE9BC863096DFB6DD11CB45AB90CF433,
	NULL,
	UIStateTransition__ctor_m0E8C427A23BBF93BAB4E7395FFBD2CC3CA4331BD,
	NULL,
	ModelInstanceStateTransition__ctor_m51D3D7272FBA810B9633964EC4478DD863B6C0D6,
	ModelNodeScriptableObject_get_isExpanded_mE6662A63F808513276FB7C02C7ABC8F17960E93B,
	ModelNodeScriptableObject_set_isExpanded_m75C0DC2B41CC54CCBCFDE51D955C15B54B3BDC0D,
	ModelNodeScriptableObject_get_Type_mAA4C734834DBEF454EBDAC0472C18FD1B30EA821,
	ModelNodeScriptableObject__ctor_m574BA40B77CBAC87EB967C5259FBFE5443802C17,
	ModelScriptableObject_get_IsAnimated_m7A0AA88E3F6FEE29B0F96F18871AFE004938CE75,
	ModelScriptableObject_get_IconSprite_m080F6A59683A508A4C0655BF8F0640885DD95280,
	ModelScriptableObject_SaveToJSON_m6742AFA1C4619EFD848695DF89B867D1817BEA13,
	ModelScriptableObject_InitFromJSON_m78741172B16DE4DBCD42DF2B35926572ADAAD9B1,
	ModelScriptableObject_OnBeforeSerialize_m46147308C41EFD2A882750C8C4C6DA2F1C9EAD51,
	ModelScriptableObject_get_NumNodes_m39E5C8386D84746132A73143A3E50657280F272A,
	ModelScriptableObject_AddNodeToSerializedNodes_m4A22DCC5C2391426A6AE7D120442BB0B8C443455,
	ModelScriptableObject_OnAfterDeserialize_mF18788ABC8F06DD513ABF7DF5B43CAD183BDA0EF,
	ModelScriptableObject_ReadNodeFromSerializedNodes_mFB0204487A2F6638B92329DBC5014E25F21DB5C3,
	ModelScriptableObject__ctor_mD0E4F2B9F96851217EDCF69EEF036FA66AFFD6FB,
	VolumetricTexture__ctor_m8E59372708AAB845C7A2C36C029397EB1C6C814F,
	DebugConsole_Start_mD24DFF76A9D7DD4CA7793EB152F8646C0C2A3FB7,
	DebugConsole_OnEnable_m163FE5682B0205E61F6C82AAD45608DC54C1741D,
	DebugConsole_OnDisable_m61452992A217FF6C82E0F5D3110A7C7FC4CEBA6E,
	DebugConsole_LogMessage_mF31E85201F24A3DC61C802855685FA1038B0E472,
	DebugConsole__ctor_m0FFD1EC98F6CA96234BEC9AD11809E4E86261BC8,
	DebugTester_Start_m44B4D4DC97D9F1313DEA62E2365E1E873BB383AE,
	DebugTester__ctor_m7BF0A9A11A35B7AB5970479397D18099E303E4FF,
	U3CStartU3Ed__1_MoveNext_mD2A371AD44B08D643FCC1CBE31AEC1E2FF13A2EE,
	U3CStartU3Ed__1_SetStateMachine_mD189FAB04CE51EC7DC2E00C4547993B95ACD637E,
	TestMultiplayerUI_Awake_mA2941443C3345CAF2E683485D7A681F1ECAC38A7,
	TestMultiplayerUI__ctor_m0440FB3F6A0E8A6DA0DF36564799CC6D3823A962,
	U3CU3Ec__cctor_m1E17597C4EF621F369A5AF2C97531CB03F507CA0,
	U3CU3Ec__ctor_m6117EB0A5DE8E828D6E4EF7E3655291250AA1913,
	U3CU3Ec_U3CAwakeU3Eb__2_0_m816E0D375D51C1D92E9BB5E26D7E6FF3D3BE154A,
	U3CU3Ec_U3CAwakeU3Eb__2_1_mB7A24790784A0F2DB48BCD4A798685523D681B69,
	IntroAnimation_Start_mC03ADF18E34D64573A5AA0E6E819665F3D2A76F6,
	IntroAnimation_AnimationIntro_m2376FE658F0CAA1903A8833E0A1B2D9055A7092D,
	IntroAnimation_OnIntroFinished_mA39C91E005F01AF355A149B84499AEFBC165CCA1,
	IntroAnimation_Update_mD993AA946CF2627F1677E431DB1B1EC59302755A,
	IntroAnimation__ctor_m720D235E10930BB16FC6EB2DC0AB4C8198B83EA4,
	U3CAnimationIntroU3Ed__5__ctor_m1EF920F3A1AD2CA29839C88B223B6303D9DE3F42,
	U3CAnimationIntroU3Ed__5_System_IDisposable_Dispose_mBCA2AE77BBB9159B8176F3FC44349141D421F20B,
	U3CAnimationIntroU3Ed__5_MoveNext_m7EF32CEF730B2F6778C42E6C69FCE85C663A6210,
	U3CAnimationIntroU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABA81E04AB2A4F244AE801A2C2450B45E9D4C4C1,
	U3CAnimationIntroU3Ed__5_System_Collections_IEnumerator_Reset_mFB1E6AD8437802EDF9E3DA18DABD4630D90B91F9,
	U3CAnimationIntroU3Ed__5_System_Collections_IEnumerator_get_Current_m87CF85793149D96CDC6D7BBAAFB3A8D561EE78D5,
	LibraryManager_get_Instance_m6C62BACDA93185181A64F99107E4DBA70F60FDB5,
	LibraryManager_Awake_m91D52FE20A0F4A6DC87DAF46AE243CE1E02762A5,
	LibraryManager_LoadModels_m42073D2724C182D998F3D0B19905F8279429465D,
	LibraryManager_LoadCheckpointGraphs_mEE43FF20CD7C713E14C795D173F3538D29AE5738,
	LibraryManager_GetModel_m1E03B950B3AA59954BFF35C6814A8D0B30F52A8E,
	LibraryManager_GetGraph_m934D401390B46EBCB9A9302BB4AFF8EB719BEC7E,
	LibraryManager_get_Models_m76612DED4EBD669B0B3FCF39C5EB84E0FA25D457,
	LibraryManager_get_CheckpointGraphs_m0C4C492720D676F5AD146FE89033515E63782DCE,
	LibraryManager_CheckpointGraphsForModel_m7458CBFD002D220469EE10E25C6C7C30D7F82312,
	LibraryManager_OnDestroy_m0B0A7F1886C3ED3495C9F27883ED1B0E4ECA9C6A,
	LibraryManager_OnApplicationQuit_m9078D954FB268C99782F3FF29306C80F59D88207,
	LibraryManager__ctor_m21BF26278C2864E2BC4D073F66DDF178C724E67D,
	LibraryManager_U3CLoadModelsU3Eb__8_0_m7E4F2EA6B8E3ACC7A0BC7BF93F78137F30508FF3,
	LibraryManager_U3CLoadCheckpointGraphsU3Eb__9_0_mCB92E0BEE46B704D34F826D18A88CA7A627F3B99,
	U3CU3Ec__DisplayClass16_0__ctor_mBE88D929FBF91B08C36D367E595E87F29309AC4A,
	U3CU3Ec__DisplayClass16_0_U3CCheckpointGraphsForModelU3Eb__0_mE7030BC24FC0B274050F5642747B18138904DB9F,
	LocalConfig_GetBundlesDirectory_m10D15DEE4F1906FE243CB252624F77B5F3F21F14,
	LocalConfig__ctor_mF66C1D10C88FEE0410E4D2A80968FCCEC782BB2B,
	MainMenu_get_InFocus_mE000858446FAEA8DC5743FA59D3373691480E796,
	MainMenu_set_InFocus_m0A1217FA1023B862C1933A8CE47044AF40B4DF95,
	MainMenu_Start_m1729BDE6D096D9F4C92DBE72B392BA89E9A9ECAD,
	MainMenu_Update_m6D9E8EB1A42CC68CFAA865B9CF18FAEB81595C5C,
	MainMenu_HideMenu_m9A25BA3B9FD5C2A691F3E0F9B23E1FD2F30FF449,
	MainMenu_ShowSharedExperience_m66087E37D7CDFEAE3B6D62EFF8083CF588617F89,
	MainMenu_ShowOptions_m361CB78A440EA73038A7622BB7770310FF339635,
	MainMenu_ShowLibrary_m7D0A7464042D753B5B24FAB3C89917795FE20E40,
	MainMenu__ctor_m8209CEC1D907C87A96D777961F4D0536E6E948DD,
	CheckpointData__ctor_m35703B470D8D2E175B3FA067681F7A89D408F1C6,
	FreeformQuestionSO__ctor_m9608FA497581E237D03732B73506B9D9CEFE7941,
	InformationSO__ctor_m0073E1B4420391CEF5A070A714E0324F12B4AFB2,
	MultipleChoiceQuestionSO__ctor_m4E501335F5C60C6188CBBC9BE6DF07CFDC8C1F9A,
	ModelCheckpoint__ctor_m8A2DAD38D62B5FDF045D81B5F865CD701F683107,
	ModelCheckpointGraph_get_numCheckpoints_m7B7987692DA302E81BF301518E953FE071DCDDC3,
	ModelCheckpointGraph_get_numData_m62B151450AAFE689C67A8559CD4D3ABE462B2326,
	ModelCheckpointGraph_get_maxScore_m73D08DDB81B468635E191EC212AA83A36F947EBE,
	ModelCheckpointGraph_score_m5DF987AAB3A5AD3E2043C35F7417431697981B38,
	ModelCheckpointGraph_checkpointScore_m02C5951B0E6E96C55E014E1483A4F37B8DEA6661,
	ModelCheckpointGraph_maxCheckpointScore_m8C56065617635D697F16E1CD16022D8EDBC3CE53,
	ModelCheckpointGraph__ctor_m619F438C2E0C0B93BBACCA25D0CFFC2385CAAFE2,
	ModelCheckpointManager_get_currentCheckpoint_mB94227839CB307305F45EF91A6D6D310C5F109E7,
	ModelCheckpointManager_EnterCheckpointGraph_m61D3BEC535A9C1EACFC01D9FE3A8F8E30CF594CD,
	ModelCheckpointManager_ExitCheckpointGraph_m6F97C3D4B93ACBC36ACB6B212201669188C840C2,
	ModelCheckpointManager_SetAnswer_m903FE7C899B40BE3B5D616702543BD2DC4A240A9,
	ModelCheckpointManager_SetCurrentAnswer_mD9460524D061938852E85F923BB02C2DA6C06873,
	ModelCheckpointManager_TransitionToNext_m361AAEADBF5B49AEEBC12780A328A672DF1C01CC,
	ModelCheckpointManager_TransitionToPrevious_m034CBCAA2D375EA51125387F31032E696131F3C0,
	ModelCheckpointManager_ResetCurrentCheckpoint_m624D898BD7FCD72E3280A850F6FEF05B77BC4181,
	ModelCheckpointManager_TransitionToCheckpoint_m53E3010135F291894A43DCA368E439FD29B68A8A,
	ModelCheckpointManager_UpdateUI_mB92A3C81B0227BEB48159426089707B9FD0FAC32,
	ModelCheckpointManager_RegisterTransitions_m1F54CDB4CF902480A50C45E1D833069C30B2F769,
	ModelCheckpointManager_UnregisterTransitions_m00E99AED02899CE166D16170FBD05467F477FF28,
	ModelCheckpointManager_RegisterTransitionConditions_m7E7BA2E2A34CE989189D0FC87BB8EEAC1694934A,
	ModelCheckpointManager_UnregisterTransitionConditions_m46D13BE795F728A5DEA3B1123BCD5D2066F129BC,
	ModelCheckpointManager__ctor_m0024192EF51E956C2C83983D680ACFBBD14FFEA5,
	ModelCheckpointTransition__ctor_mEB97A87D26DA24FF6CF80988F4C220B87DE08F23,
	NULL,
	NULL,
	ModelCheckpointTransitionCondition__ctor_mBA2E82308777F4F50F3FE96462FAC063594D8A03,
	LayerSelectedTransitionCondition_RegisterWithModelInstance_m23BCD0D83B0729C77160D60075A56DE122440E46,
	LayerSelectedTransitionCondition_UnregisterWithModelInstance_m5BB241DA77DF022703C288F0E777D60C63A56F90,
	LayerSelectedTransitionCondition__ctor_m1EF8938128CE8591DEC6602190959E59E26255A6,
	FinalScoreUI_InitializeUI_m32C888843A4BFA7DE168DD74167442CED1B26B36,
	FinalScoreUI_OnClickNext_mF9D50230AC3ADF564A78DB663AF6BE800FF53204,
	FinalScoreUI_OnClickPrevious_mCBF0969A752B91720EE84F97724A7D3BADF48CD4,
	FinalScoreUI__ctor_m0EDC340CF9C42DE96E9527EC2D1BFFB73F15F4D3,
	ModelCheckpointUI_OnClickNext_m86E77D763D6994A27BDB0F6A3292960942B8CC13,
	ModelCheckpointUI_OnClickPrevious_m099CAB6463295333756E1271D456E6CD3E6AC8DC,
	ModelCheckpointUI__ctor_m270CC8811F3E12344017FB3B32599A1FFA5725BB,
	MultipleChoiceUI_Start_mDB6EF356A27BD4CA3CA01BD5146EE059CDD2AAFB,
	MultipleChoiceUI_OnClickAnswer_mCBA48BB51BE1892BD70517990D8DB4E90DB51B8C,
	MultipleChoiceUI_InitializeUI_m9DBDC702F9A7E66D1DC1E79200B3DEDB95B087CF,
	MultipleChoiceUI_OnClickNext_mC680466739F1B47DF8CB65E3BD215AF8FBE0DA15,
	MultipleChoiceUI__ctor_m170486305A6A8A0247A1EAD3245AB1790FDB7526,
	U3CU3Ec__DisplayClass9_0__ctor_mA7A218F2E9C77FB26268730C96EE60460D6A5EC5,
	U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__0_m75C99484C0F726527A0A3687A4ECD46DE92DD317,
	U3CU3Ec__cctor_m7A05CC31F8FED27EFEA8C00703876FB51E70409C,
	U3CU3Ec__ctor_m288C77D78EAC61B9A11FBFD75F51CFD5240F47F3,
	U3CU3Ec_U3CInitializeUIU3Eb__11_0_mCF17191E2E1D70083F0CD123FFB8651FEAFAF340,
	SequentialUI_InitializeUI_m8ED5CAA4CEE8D6B02C1577BF29EABD840610A112,
	SequentialUI__ctor_m9C1E968898698AD18928055881385630F61DDD17,
	AnnotationController_get_ShowAnnotations_mBFA03A11C19DBACE1D769A3C24C74232DBC31B32,
	AnnotationController_set_ShowAnnotations_m9AF6811BA19AE918D1BFE68C4C0CE6993DED00BA,
	AnnotationController_SetShowAnnotationsServerRpc_m3703CE74CDFAAF20733046AC16E077151E67D168,
	AnnotationController_OnShowAnnotationsValueChanged_m1FA40AF3FF36A62D103415D41BF54CA4EB9A8D59,
	AnnotationController_SpawnAnnotation_m8A7234908B2EA0E0955E37175D0A83E5D60A1687,
	AnnotationController_Start_m37B98FB591E634FFBC6E503FE5588BFC922878CB,
	AnnotationController__ctor_m31AE8A7B1EC9D71319BF28757A13DB5C69DE83FF,
	AnnotationData__ctor_mF8ED02FCDCE484F6746E4E005508FCBACB99BF16,
	ModelAnnotation_get_CurrentTime_mF97CD04DA28F26C693A43CDAAE31CBB20920B476,
	ModelAnnotation_set_CurrentTime_m31A7C12BF2AEB610C69C99B87DDE10BA8CAE53B8,
	ModelAnnotation_get_LoopAnimation_m1810CF3356DB6EDFF40A2584CA90DB10D87718F1,
	ModelAnnotation_set_LoopAnimation_m4E3261BDC6546A266646CAE169D61A58078EF293,
	ModelAnnotation_get_IsSelected_mC128BBB3BAA86895DC23DB80458E11BCCFD793CE,
	ModelAnnotation_set_IsSelected_mCB0100D52563EB986862B0903839AA93D6BF9CAD,
	ModelAnnotation_OnFirstHoverEntered_m91DE0D8D6B3EAB7CEEB819D0C4DFBC3579D94BFA,
	ModelAnnotation_OnLastHoverExited_mF99CC04D1A21A8BEBFAFBF22D3434096D91207DF,
	ModelAnnotation_OnClicked_m14BBDD528D30F78B5CAFDA4DFAD381BDEC517A02,
	ModelAnnotation_OpenWindow_m11261310B765DAAAB7A0AA33D1454457CABA95B6,
	ModelAnnotation_CloseWindow_m67062A594780FC6B62FA10C143BBC24EB6B4A486,
	ModelAnnotation_SetAnnotationData_mDB3E5C90D19D95DFEC83E048298693C0CCA9F266,
	ModelAnnotation_Start_m6132D032D969CF83F11749093CEBBBDA8DB49BB9,
	ModelAnnotation__ctor_m5B898CE67FC24CF042A535965F18E0F4D0E98E3D,
	ModelAnnotation_U3CCloseWindowU3Eb__27_0_mD5DC5F7DC25C193391D1C5833E565D5C9350B074,
	AudioManager_Start_m3C0FEAF19F58B6D28A9E6D815B3AAF94FEA21B69,
	AudioManager_OnDestroy_m94C77771783976448AE56345E76E7AE6A7C2CD6C,
	AudioManager_OnNodeSelected_m7200214879BF213F793179D629E790792B14670F,
	AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6,
	ClippingPlaneController_add_clippingPlaneStateChanged_mC39F39C0241DB78D4D39818C8928E0806AB2AF02,
	ClippingPlaneController_remove_clippingPlaneStateChanged_mAC4A481C21C2040FABFFC352FF1B071453B3513F,
	ClippingPlaneController_OnNetworkSpawn_mB485343A8549C05FA9E1D8AF9AE0333EF5D33E81,
	ClippingPlaneController_AddLayer_m831187D7217015B76241163ACFBC1443AC2243A8,
	ClippingPlaneController_AddNode_m21E3C1FA68673808EC86D2303D25565016FB4D2B,
	ClippingPlaneController_get_state_m2C510CEEEFE64529FFB3E5CC140D3A67D18DD988,
	ClippingPlaneController_SetClippingPlaneState_mC555EE06E9380DFE14E910FBECB22863E05E82EB,
	ClippingPlaneController_SetClippingPlaneStateServerRpc_m58F18CDFDBF4D0536D8222632C045CFE9F050FEA,
	ClippingPlaneController_SetLocalTransformServerRpc_m71212BE765B7D2D4BB9F89C44600660EE6C3CC68,
	ClippingPlaneController_get_IsActive_m5954FE12AA81F00ED6F4DA8FA59E3147A02B5F5E,
	ClippingPlaneController_set_IsActive_mCDD90EF30934CF3FAED7CA802799627D39C4EC5F,
	ClippingPlaneController_SetIsActiveServerRpc_m00EA41F590AC49483A9B60037E8E4DE25D1D0DB9,
	ClippingPlaneController_OnIsActiveValueChanged_m06168D978037095B0661AAE77FC7454E66D6D044,
	ClippingPlaneController_get_TextureIdx_m9ACC522B1D4F8040685B93E321122F8E8EDD0CB7,
	ClippingPlaneController_set_TextureIdx_m5D2B4D7E9E8CB7FB83C9A43E344A15D10E9F2C67,
	ClippingPlaneController_SetTextureIdxServerRpc_m88F009CA70D8F0B6BB4FA0DAA34F621BA8686942,
	ClippingPlaneController_OnTextureIdxValueChanged_m3D30A00D87A5744E45136C38DF7D8DE63B141B5B,
	ClippingPlaneController_get_Type_m58F002E187DCB93F9411D5AD3AB1BDAA083F9D26,
	ClippingPlaneController_set_Type_mF979EBF4D6BE918D28254BD86034FF532E62CC3D,
	ClippingPlaneController_SetClippingPlaneTypeServerRpc_m555C78FEFAF0320052D821DE839C104C1ED86922,
	ClippingPlaneController__ctor_mDB4AF6C8E7D15EC271B36411B6B1E2A6E5A42194,
	ClippingPlaneController___initializeVariables_m65F8B42A0970FC3DBE748CAB2FB7407CCA14BBF7,
	ClippingPlaneController___initializeRpcs_m1CAA5EBC3F767A2F0236616F573F7D63E72DA10C,
	ClippingPlaneController___rpc_handler_72846158_m9910D552A146D80C13E63D568B1203E859A72B6F,
	ClippingPlaneController___rpc_handler_3722869538_mA508C28A6293BE8EC89E611D5C8F8348512353D8,
	ClippingPlaneController___rpc_handler_3602782283_m2EADE9B1DF7DCD9782CDDD421EFD1A3D9FE9885D,
	ClippingPlaneController___rpc_handler_267534928_mD4A623D6595461564045A297838432AAC36A337C,
	ClippingPlaneController___rpc_handler_2099587104_m03FACAD6A80DAE12A1B8542AF2AE2FF4F00F5D09,
	ClippingPlaneController___getTypeName_m343EB1C4D2BB55C6FF501E5F70967156213F8EE6,
	CustomClippingPlane_get_sliceMaterialInstance_mBD1E5CA6A0FE07A7DF3D368D9B85A7DB31F965D3,
	CustomClippingPlane_get_VolumetricTexture_m7F931365C13F18A2AC26F59CE719AADE374557D3,
	CustomClippingPlane_set_VolumetricTexture_m7FF9EB5ADA3C8FA85B3EF5C0FA9923810FDA9A07,
	CustomClippingPlane_Start_m8949BC9D71A967AD03E3DC1A1B520AF165DE716F,
	CustomClippingPlane_Update_mF6EF7114DE905C9E9801EF5FAD6529F728D79242,
	CustomClippingPlane_InitializeOnStart_m6FEBA6F907BFF639AB4C7BFB6E6FBAD06AEC79E1,
	CustomClippingPlane__ctor_m4F0A8078DA5644A33A0F47D5B512985D3147E816,
	InputController_get_IndividualMovementActive_mF9DBF69BDBF8D34059DB5B515C452A3046980E2C,
	InputController_set_IndividualMovementActive_m9AA9406AB4B4AF1AE500C14245AC33AE4047660B,
	InputController_SetHostTransformsForNodeSelection_m3FC448024436A1F8E8915D770176AA7D59F0EBFC,
	InputController_SetHostTransformsForHoveredNode_m9773A5A959B520EFB0B522CEE733A9EF8C408263,
	InputController_Start_mC887BF47D15F6F54A07FCE6717EBB85DFBDEE479,
	InputController__ctor_m0EDBE66635C62BD8D770DC8E9C8D90BECD52A00D,
	InputController___initializeVariables_mF046FD9A1349DDF428DE1ED8B62006A37A9068A1,
	InputController___getTypeName_m3630C659AA422AA875E22FF8BA3B79D6274D2432,
	LayerInstance_get_NodeInfo_m7734018445E118724F6089A829A78E62C9096256,
	LayerInstance_set_NodeInfo_mA56291AF6E1C999041BEFA8E6744EDEE4AFC2F3E,
	LayerInstance_get_LayerInfo_mE43E0E6C6793F15EEF5B514E7F9D6CB310D689B2,
	LayerInstance_set_LayerInfo_m996FAEABD85D0EE02E1C6924A6026A0488209405,
	LayerInstance_get_outline_m899E9D6F694AC870AF8C910F9313559FAD4DA5C3,
	LayerInstance_OnNetworkSpawn_m34D844ABCE8A01C0661FF2FAF19A012C636FD14E,
	LayerInstance_get_State_mC0F204E00DF3FAF88E23E706BFF4305E2B7CC99F,
	LayerInstance_get_DefaultState_mDE36347458C584CD8E59E2499B01FD41472CCA4B,
	LayerInstance_SetNodeState_m35D6BE092FF14509FBFCCA29D2EC3C37431BC456,
	LayerInstance_SetNodeState_m831D503015CB6E7BAB7090A15A76D2FBA6EB96D2,
	LayerInstance_get_IsOutlined_m1F565A9BE9913A292547274104439453780DD0E2,
	LayerInstance_set_IsOutlined_m6F75130C216EF00257D6E3A73F7B727ADF17A6A0,
	LayerInstance_SetIsOutlinedServerRpc_m32E0385C71A5FD139248E5062D74D0E373428F33,
	LayerInstance_OnIsOutlinedValueChanged_m2C00CC0CD34F670CA7BB9100C794525FE6EE0B70,
	LayerInstance_set_OutlineColorIdx_m72EC7BF108728F6B3BA79143D2C56FCC289784DB,
	LayerInstance_OnOutlineColorIdxChanged_m014A0A7715CD5037DF462F6E64D86275C44D6795,
	LayerInstance_SetOutlineColorIdxServerRpc_m5C84A66F8DDAE0BC0FC4EF8442FF5A4BF627EE81,
	LayerInstance_get_IsHidden_m78E336C4A2F834E17E8CA81A71E2020C52B04733,
	LayerInstance_set_IsHidden_m7EB1192F9618A1D8400ADF485C3B0FCC1F35E2B0,
	LayerInstance_SetIsHiddenServerRpc_mFFAC8785B21C4CCACB9C591B068584FCEC722A14,
	LayerInstance_OnIsHiddenValueChanged_m80D7F93F5680A052B988414DC3D92F412F151FD1,
	LayerInstance_get_IsFaded_m422D59B8FB140CEDF8E8443E467124B3317D8781,
	LayerInstance_set_IsFaded_m5E6BE5AB15C3F24E9DC6C70DFB7C85EA10E2380C,
	LayerInstance_SetIsFadedServerRpc_m8D966EC8336469F56A93C58246B92EC15122C726,
	LayerInstance_OnIsFadedValueChanged_m5B6DD7BD00DE52E1B2D13231299BCBEE8F13B03B,
	LayerInstance__ctor_m24B751A229800818299E16316FCBBFE892F3F9CD,
	LayerInstance___initializeVariables_mBB27B3B7F9C2F9AEF7A4CFE1D833812403E5E6A7,
	LayerInstance___initializeRpcs_m2ACB71C420356F63560EFE84C5781A030A922A21,
	LayerInstance___rpc_handler_449397666_m4F931DB3EFBF858B8DF1BDA3490CF61C7BCF8DCB,
	LayerInstance___rpc_handler_3704825583_m16C047F2855C40300096FA4F9FE6D6235A96A304,
	LayerInstance___rpc_handler_268871636_mBA1878DC5AA2BB76CD073D4A9546A03E5FBAD141,
	LayerInstance___rpc_handler_3328835206_mEC8A017C52A69B2AA6BF0AC358F3E2E10E7BFE7A,
	LayerInstance___getTypeName_mFF3C275D57A965101E54DE935E8C94E398A67BA0,
	LayerSpawner_SpawnNodeAsync_m9C281B992CE7FBB4268C4BE8F96EF02644037EB3,
	LayerSpawner_RecreateTree_mA4F75A39730B3E738273BAD0C24453531A6E55FA,
	LayerSpawner_InitializeInstantiatedLayersAsync_m10A6B32B3EA41B435BA0843B5B72F8048F34E532,
	LayerSpawner__ctor_m6C9D5FD769B4FA0DBDFBB4C455A80278DEA396EE,
	LayerSpawner___initializeVariables_m23907B4E15E6129C1E7A87755F16DA7FA96FF3E9,
	LayerSpawner___getTypeName_m279372698B26DB977A2F9D87F6D0ECAB7934EE4E,
	U3CSpawnNodeAsyncU3Ed__6_MoveNext_mECD34BB602497E62BD79E97238DE2A82B177253D,
	U3CSpawnNodeAsyncU3Ed__6_SetStateMachine_mBE32AC90DEA5CE29BD86EDC48EC557CEB4207597,
	U3CU3Ec__cctor_m022F97DB2194341D2028ACE07D6684AE6E2B6672,
	U3CU3Ec__ctor_mABC0DF3B1B4B397D55392B7164D3A24076EB9F2B,
	U3CU3Ec_U3CInitializeInstantiatedLayersAsyncU3Eb__8_0_m303A29D403F69282983913882E0413DC953EA1DA,
	U3CInitializeInstantiatedLayersAsyncU3Ed__8_MoveNext_m391D984F9F94FA0589D9B4A8839FBAB9056F9698,
	U3CInitializeInstantiatedLayersAsyncU3Ed__8_SetStateMachine_m3C625409AFF0F2D49092A283948F50AFEE5C54FF,
	ModelAnimator_add_AnimationStateChanged_mDDC3FF52CCCCF4249BCB7B6D06E230EDC35C2CD8,
	ModelAnimator_remove_AnimationStateChanged_mEE1872E7E3B2AD3065FFBBD14F5DC7DDDD5EF36D,
	ModelAnimator_OnNetworkSpawn_m5FA49F52879D63F5F8A755612850949CF9155453,
	ModelAnimator_Update_m26BC96D3AC5BDAB3E401CA207085044B1CA57A4A,
	ModelAnimator_UpdateMirror_m993373EA45B84296F74C7F4CCE38F5D697FEB71F,
	ModelAnimator_UpdateCyclic_m67E606F2CEA2453D9D264513F80728A0A69211F1,
	ModelAnimator_UpdateOnce_mE7C9EF77D315817BCF43B3E2AA8EE563F02C0DCD,
	ModelAnimator_AddAnimation_m2D092C041661058F9D9ED6DEBA86FEED11BB0E4C,
	ModelAnimator_SetAnimationState_mDC1CF6B70E7D48418B141304F6FC671501D00709,
	ModelAnimator_SetAnimationStateServerRpc_m3F996DF2582C5AFA41246590A5977A875249AAFC,
	ModelAnimator_get_state_mAB3F7ED5FF65C7D3946D705B5E8A7C8CE28C78AC,
	ModelAnimator_get_AnimationMode_m167AE77E0721E0B7CA98A1460DF7A9779838227E,
	ModelAnimator_set_AnimationMode_mB34F9F78F41DD14393B1030430D270F76CB38811,
	ModelAnimator_SetAnimationModeServerRpc_mB4981B5176AAE47F0AAF84DC8792DBE00C402E76,
	ModelAnimator_OnAnimationModeValueChanged_m5F08C09B662843F97865B8462C74324CA6444A38,
	ModelAnimator_get_CurrentTime_mC927C5576C3CC900A683DE11956D70DCC0EF02D4,
	ModelAnimator_set_CurrentTime_mFD120F65FAC9E1C9540599654106C238C2B76713,
	ModelAnimator_SetCurrentTimeServerRpc_mC0CDEFC47DC56771DE38874F9E5039C8D69A0A8F,
	ModelAnimator_OnCurrentTimeChanged_mAB249A8B99941562020AA5E07B8E9EEE2F7ADE27,
	ModelAnimator_SetAnimationSpeedServerRpc_m3B5F8EC4B131AC1FBCF4DB8B806DDCC271A68CB0,
	ModelAnimator_get_AnimationSpeed_mCE1D61A885DCB263D8CEFD9514EF480AF375EC42,
	ModelAnimator_set_AnimationSpeed_m4BBD8441F3A7AEEA8234834A8A2295AE814C4530,
	ModelAnimator_get_IsPlaying_mA88505311AF4B85FE33A95DFAE80DA5510973281,
	ModelAnimator_set_IsPlaying_m0843798CB8FC60D56B650534CDDD614C4487410C,
	ModelAnimator_SetIsPlayingServerRpc_m5D3D273972D2FDDD360DE61B4336E726B4AB0413,
	ModelAnimator_Stop_mC105889983436F213262C43F3F52397216584373,
	ModelAnimator_TogglePlay_m59D54C3C5711B76623B69E2CB5F2F3E55318F9F3,
	ModelAnimator_Play_m5AE3C2FDD24A38EA756095A7E06AAA54E180D294,
	ModelAnimator_Pause_m05D1D6F5AD891D8583EEC13E7F1AFEE51C1E97E5,
	ModelAnimator__ctor_m80904E202E59D051FDA4C0A21EB0884FC4587645,
	ModelAnimator___initializeVariables_m47AD8CFB78D4C34BA5287BCB514C5DF32AA314A9,
	ModelAnimator___initializeRpcs_mDF08140C806500B7DFE27BDFBCA7021608C6A321,
	ModelAnimator___rpc_handler_2956263290_mB084B7652A2C3FE251DA5C37F6FBB4B53D03A908,
	ModelAnimator___rpc_handler_2433975334_m3A497F37D71A9EEEF4BDA058A949030864E714DA,
	ModelAnimator___rpc_handler_805743151_m6BBE20BBC2C401ED815449997E77CD3DE6E4EE4F,
	ModelAnimator___rpc_handler_1141981096_m4EC3F50D6F00C3230601F9B048FC97095A37F70F,
	ModelAnimator___rpc_handler_866973770_m6105B6AF72200FD85979D8CA52B8976BE1DB8544,
	ModelAnimator___getTypeName_m9A206A0B608260D25686AB88F13009FB09999011,
	ModelInstance_Start_mD78A7AE42FCF01704B069578DE7ED3054D2C70D5,
	ModelInstance_InitializeComponents_m46A27AA4AAAC77C88AFDE7A769316118D1FB2906,
	ModelInstance_OnDestroy_m6D54E721C57F3E803530483EC01FB956AEB5736A,
	ModelInstance_PositionUIMenus_mDE83631CE388833C4B4486F1C3283F2A38BD26BD,
	ModelInstance_SetModel_mE1C22F6A7F9FAEA4E6B68803DCE73CB1265CB8EE,
	ModelInstance_SetModelServerRpc_mAE6750E0AC15D698915A907B435CB86BA7C52339,
	ModelInstance_SetModelServerAsync_mCA39FD0E53D091C6BA30077C67BC94CFEA0B6460,
	ModelInstance_SetModelClientRpc_m07885ECF29CFA6CE2285AE6E35011745A6EDF36A,
	ModelInstance_SetModelClientAsync_m5986DC5C683770D3970C8BAB8F927A6DEEC650A7,
	ModelInstance_SetLoadingStateClientRpc_mE43DFE0B954BD201232FF9182688DCB29CFD127E,
	ModelInstance_SetLoadingState_mF74B45AA60EA35DB303EFE4BAD814157C8D99E78,
	ModelInstance_InitializeClippingPlaneClientRpc_m569A41F3809BE622DC4E03F521E3267F147D9282,
	ModelInstance_InitializeNodeContainerClientRpc_mE3031D6E8B0B6D1AA51706F0E5DA3C1F33E49230,
	ModelInstance_ToggleBoundingBox_m8196477E6916BE6E8C8454017B3C29E84E0B41E1,
	ModelInstance_get_state_mCC0E2EC824D2BF90D5AEC9329B3CEC5528166E0A,
	ModelInstance_SetModelState_m69C93D07959A0A6C826A7EB208F1A21D75546180,
	ModelInstance_SetModelStateServerRpc_mF957427F2448619757A46D988199FC896FFC1D96,
	ModelInstance_SetNodeContainerGlobalRotationServerRpc_m6E9DC95430BA74A72A48054D0A2CB249A0F46583,
	ModelInstance_SetNodeContainerGlobalPositionServerRpc_m5D17B55A3294B8172DB2A56F911617E836D0DE40,
	ModelInstance_SetNodeContainerScaleServerRpc_m5D536B0BBFA832E41D921812F745C7C91BC8E46A,
	ModelInstance_get_CurrentModel_mEAD5830B10BE1100571289CA04567085DC1F0B9B,
	ModelInstance_DestroyModel_m20698CB1281B14036D531B86063D32518F9A5DFB,
	ModelInstance_DestroyModelServerRpc_m11B0794ACD84313CFA8B41B5DBCD41FC472D1007,
	ModelInstance_SetRotationSpeedServerRpc_mCD8676431829B6322AC19C32BF1F8999D1287E54,
	ModelInstance_get_RotationSpeed_m5A5ECCDD1A6CD8677FEA5F71FB0AE08E0EA0FDA5,
	ModelInstance_set_RotationSpeed_m6154C8033F0AB013B5A482E21494C6C56384C687,
	ModelInstance_SetAutorotateModelServerRpc_m4BABD0FCF9A34534E5F0C663E20DD91A19D97162,
	ModelInstance_get_AutorotateModel_m92276D7FD2C575E4D9D219294FF1D6A39050163D,
	ModelInstance_set_AutorotateModel_mFF4B207A3785B401386B5A4C0483AA74F882D2CF,
	ModelInstance_FocusOnNode_m4C413F7EC76FF2EC3FE0219237E0D331AF9B374E,
	ModelInstance_FocusOnNodeServerRpc_m5DFA0A15D50160A6E3D032D1753A06A04B3C033E,
	ModelInstance__ctor_mD27274FE9E11CBA6CB39E54D1944B1C09418ED0F,
	ModelInstance___initializeVariables_m2203DC6318BFEEBD4773360E2977D528925E33C1,
	ModelInstance___initializeRpcs_m4598FA28E9B98CD1EBED4A7A9F9D2C6B5536DA74,
	ModelInstance___rpc_handler_2472231065_mDC0AA69CECAE236D60814AA0FF43333166931542,
	ModelInstance___rpc_handler_771431870_mC435CF2108CCA3866C13E87AE482844AA985F393,
	ModelInstance___rpc_handler_37011299_mA33E4A521AF7C5E427291227C5CDADE52A4011BE,
	ModelInstance___rpc_handler_1441208538_mFA24AAD7F75B117B41A6CBC19F54A33EE1302B8F,
	ModelInstance___rpc_handler_1369999954_m2B4248924E5EFB6DC0671ED947AB128FB4D2DA9C,
	ModelInstance___rpc_handler_405319951_m2722EA3700B75485FBB8DC2FE3B0FBB6DB0127D8,
	ModelInstance___rpc_handler_3458780303_mB47755F6A46D11F9A0C5B2887DFE4A91D2CFA7BA,
	ModelInstance___rpc_handler_2935864450_mB66811DC358CA5CB7731EB1AC0AEAE41D9948E30,
	ModelInstance___rpc_handler_2644131848_m2D57A90ACF6241702BE796EF058E6D93E69E2E56,
	ModelInstance___rpc_handler_2945997562_m2337A29239A454D5915E84FE35E5A8E4FA4A9572,
	ModelInstance___rpc_handler_1202017353_m47F55DD58A67188A183DD1381018F678B80671AA,
	ModelInstance___rpc_handler_1229847090_m74B4121E852B47EF3B9463D2856B05E5403924D1,
	ModelInstance___rpc_handler_2598283162_m0280759200DBE30570B439E2B3551DE34623210E,
	ModelInstance___getTypeName_m6F93327D675E67A469DEA79361A010DF05AA12CE,
	U3CSetModelServerAsyncU3Ed__28_MoveNext_m6DF37858A28B30105DACAF6ECCBA557114CAE733,
	U3CSetModelServerAsyncU3Ed__28_SetStateMachine_m8D4E88707BDB0867A619D67317DF84000D230704,
	U3CSetModelClientAsyncU3Ed__30_MoveNext_m3970AA534DC9529520D7A7828A315D58AB120556,
	U3CSetModelClientAsyncU3Ed__30_SetStateMachine_m08B8D8353C6879666DFC6EB7A1D66D32759517A7,
	ModelNode_get_Idx_m470153D04ED1915B748F4E07A5DB8D592BF926F9,
	ModelNode_set_Idx_m98E0488FCC348ABBB0828E411C66EFAAF612C3E0,
	ModelNode_get_Children_mFF4D2C5A42BBD89B1E3FFA5312344365395DF3EB,
	ModelNode_get_Parent_m6375F585D6E3A650A448BCF4E40167A9D1E04643,
	ModelNode_ParentOnLevel_mE0AC7A43B1C162A5B9ED126ADA1F895F319CF73D,
	ModelNode_get_Level_mB73F870EBAD9B9F50292216385C4938DAE2320FF,
	ModelNode_get_IsRoot_mB45E23692275319F6DC534B44809DAE3BFE9CE4C,
	ModelNode_get_IsLeaf_mD1B1F9292FF2E62ECE9BAEF2436E67F2AD625EFC,
	ModelNode_AddChild_m0F0BED29D463C6A59373ACEDC619319273AF102B,
	ModelNode_RemoveChild_mCF0ADAFE70DA9934DF97E19CCC1A04B507DCC5D0,
	ModelNode_IsChildOf_m5D1D3683A4B828EFF16CD31E779373F196CB0997,
	ModelNode_IsParentOf_m03FEABC9964416E3CAB3A5E2502503F7FCAC64B6,
	ModelNode_get_Subnodes_mBC9105B2B42E92CAD2897C2D1D3068821E9FB278,
	ModelNode_get_SubnodesBottomup_m9A1F0DA324C0150DC3FCE1664740EB6D73DEA391,
	ModelNode_get_NumSubnodes_m50A45F64DDEF377B7FA4AF07A3BAB99E2E8D2A27,
	ModelNode_ClosestCommonParent_m09E4C1428A6AF6922246CB769EBDE383CC570DC4,
	ModelNode_get_State_mC1C9413A48FDA86ECCBC8A2AF5A103843D7FD39B,
	ModelNode_get_DefaultState_m827080CE676044C40AB01C282690EB0516EDE5A6,
	ModelNode_SetNodeState_mD4DC82E1A58ECE727FFD5F1228B3A34E550430E2,
	ModelNode_Awake_m622DEB4141D514F84A183A95FDBC82D5676433AD,
	ModelNode_SetInitialSpecs_mD8BCFFCE4990E6E5F71E901C828A9388D70B2A39,
	ModelNode_OnNetworkSpawn_m1A8B2804FE5E69ECD2C62711EA4B331E1857B9D0,
	ModelNode_get_Renderers_m519AA84AE424E7ED6688A286ED59893440B501E2,
	ModelNode_TransformBounds_mBCDAB363B70345EA7E0CD68CCF8C05CCA1AB8738,
	ModelNode_GetCornerPoint_m5E498DC619DCA7157E7C6C7D0ADD55C1343BC23C,
	ModelNode_get_bounds_m5A3DDA8062EF12793DFA99B2052C312FD78259C9,
	ModelNode_get_localBounds_m4A603833BD10571A5C391AE1568D49BAF6BB87D1,
	ModelNode_get_InitialLocalBounds_m5CF1929A367704C09AA1AF8F2552617649248F82,
	ModelNode_get_localTransform_m9ED61F496983D6ABB10230D19DE5CAE25D4111A1,
	ModelNode_SetLocalTransformServerRpc_m8280C54BE756A66308788467FC3723A41303669A,
	ModelNode_get_NodeInfo_m70D1078BAEE6D47DD22744DC810CEBE23B92A3C9,
	ModelNode_set_NodeInfo_m81DE0A18978CE1B8BB4F46BEE9258FBF59FD1618,
	ModelNode_get_Tooltip_mC031631900DD81F1A11827CD6846D406E99920F9,
	ModelNode_set_Tooltip_m3EB042924ACF8E5D15611E9E9D2FCB0E207A5B47,
	ModelNode_get_TooltipShowing_m35DEBE42E033A621D9B644930AAD2DA9424B9CBA,
	ModelNode_set_TooltipShowing_m273E29DDF760D5CBC8A42CEFA9063B682A75DF11,
	ModelNode_SetTooltipShowingServerRpc_m7DB0F90A9FB4E5F6E1353BB3E300D44C7F28D66B,
	ModelNode_OnTooltipShowingValueChanged_m6554A55ECFE4E18D6C9A4C73F9E1F9E0D1F88160,
	ModelNode_set_OutlineColorIdx_m461F6500C1C851873DC52C3B5EB9D700D8D12B6A,
	ModelNode_get_IsOutlined_m37903FAF397A681452FE5B1A8CD0000D46225E22,
	ModelNode_set_IsOutlined_m680F05F4D5B9DCAA885EE11A2A646E42499B3DB6,
	ModelNode_get_IsHidden_m263943BFBB527ADF3E980547FB808EF0C1A98B0B,
	ModelNode_set_IsHidden_m3CDA03271AD0076F4E2AB902229C92400147FF28,
	ModelNode_get_IsFaded_mA36C7603644B6C5A05EFC15C527ED9B28AA7B876,
	ModelNode_set_IsFaded_m83B24E8F5BA91598F8E3A6F3D2DC82180AC18F83,
	ModelNode_ToggleHidden_m10CAC96C7A3B74D8B5D94DF45CAD591819CDB16B,
	ModelNode_ToggleFaded_m75A152D903070B860946DE320C3FDB46E6E766E5,
	ModelNode_ToggleOutline_mB8C04193FFB57477652C11796CFEABDE26BEECFA,
	ModelNode_get_VisibilityState_m7D27E7C01612736BB7A687278A65C8FBC27F371D,
	ModelNode__ctor_m9CE5A58CB12C479FABEB3869EE0119601A273099,
	ModelNode___initializeVariables_mA1E6C11F0A6784CF6A12D58DFA299C89FA253FCB,
	ModelNode___initializeRpcs_mCE73A7F729592933EE91B3D52F8A955D1B21AD26,
	ModelNode___rpc_handler_3333735372_m7C6FDE13053755624A6AAA07713277516B77D270,
	ModelNode___rpc_handler_997827599_m7820F524D7175FC14650EE565FEBFFE22F609EDD,
	ModelNode___getTypeName_mD5CA22908D097ECCABA1C8E76FF0D20B18BC4BD8,
	U3Cget_SubnodesU3Ed__24__ctor_mA63749C9CF5E4B56717C07E77FC8FD31BFA84571,
	U3Cget_SubnodesU3Ed__24_System_IDisposable_Dispose_m6DC1096CEDF337C5783590C8B29874822986ADEF,
	U3Cget_SubnodesU3Ed__24_MoveNext_mF36A99FB12DDA56EE69F4D277948AA0DAE938E47,
	U3Cget_SubnodesU3Ed__24_U3CU3Em__Finally1_m6722EE0D419CF9210C62C1ADC663691064AFD7A1,
	U3Cget_SubnodesU3Ed__24_U3CU3Em__Finally2_mDC7C3469A97794A3C512A70ABBA4F5BF8021E14F,
	U3Cget_SubnodesU3Ed__24_System_Collections_Generic_IEnumeratorU3CModelNodeU3E_get_Current_mCB96B0EBBBB59A75CFA4D500BDFC5D68D6D728F5,
	U3Cget_SubnodesU3Ed__24_System_Collections_IEnumerator_Reset_m483BD4D760A5C900CB26B7AF905B77CFE5B26833,
	U3Cget_SubnodesU3Ed__24_System_Collections_IEnumerator_get_Current_mE1997EA3980F0C305F745ADAE6DC5B1281802546,
	U3Cget_SubnodesU3Ed__24_System_Collections_Generic_IEnumerableU3CModelNodeU3E_GetEnumerator_m3FCC2E3D2A7CC0B153F56E19AF20F915E79DE29F,
	U3Cget_SubnodesU3Ed__24_System_Collections_IEnumerable_GetEnumerator_m253817175AA1C700F952FDCEED6F686288A404E6,
	U3Cget_SubnodesBottomupU3Ed__26__ctor_m727C6A2A5CF2B5AD3D9B4834B31D8DDD7F3B6E49,
	U3Cget_SubnodesBottomupU3Ed__26_System_IDisposable_Dispose_mDF8BF8C2FB65EC5830A93673BDD8E8C190055C53,
	U3Cget_SubnodesBottomupU3Ed__26_MoveNext_m785C4B5947E18C88FEFFBF97F5EB8D1A1C497A08,
	U3Cget_SubnodesBottomupU3Ed__26_U3CU3Em__Finally1_m3BDEDBACA984CB3B9E6D60351AD5FB17417EADD9,
	U3Cget_SubnodesBottomupU3Ed__26_U3CU3Em__Finally2_mBBE08B0D50749631112EDDE3E99CC0DF5E5E3852,
	U3Cget_SubnodesBottomupU3Ed__26_System_Collections_Generic_IEnumeratorU3CModelNodeU3E_get_Current_m3818FE367D5CB97FA29C12AE08A2866F5F52C495,
	U3Cget_SubnodesBottomupU3Ed__26_System_Collections_IEnumerator_Reset_m18458D953AA2CF944ACA16E46B5F1106C55972FA,
	U3Cget_SubnodesBottomupU3Ed__26_System_Collections_IEnumerator_get_Current_m538B9916ED559986519EB58251C4F3909BD8F1BF,
	U3Cget_SubnodesBottomupU3Ed__26_System_Collections_Generic_IEnumerableU3CModelNodeU3E_GetEnumerator_m3FFEEA6D968A4EEB4AB14B86F228490B14556CE3,
	U3Cget_SubnodesBottomupU3Ed__26_System_Collections_IEnumerable_GetEnumerator_mB43F370B4F868C9F430D4DDF4CA94C7EF9DE1492,
	NodeController_add_NodeSelected_mA08E3710533051D10915CA576D3AE131918D17F1,
	NodeController_remove_NodeSelected_m1E90F92DFF04ED7ED5540F837E76BAD16475883B,
	NodeController_add_NodeDeselected_m500A44F24EDA7A98D6C0E0ABEA6AFF02078E4BC5,
	NodeController_remove_NodeDeselected_m5A817FCDA7F320F39C613839324D07330D7ED96E,
	NodeController_add_NodeHovered_m85D7AC8CE8DF8071A0E8046F96A01FE5040B791D,
	NodeController_remove_NodeHovered_m9278E79CD40A99125AD3905C7A1B957A858C5C96,
	NodeController_add_NodeUnhovered_m73BCA4DC35EAB72ADD877A300239FF214D87189C,
	NodeController_remove_NodeUnhovered_mA83BD650DA4303B0AA9BFD6E647619374ACEBF0F,
	NodeController_add_NodeAdded_m241980B76E127FCD34D04DDBA7566AC5C57B9088,
	NodeController_remove_NodeAdded_m78F38218C1B64DD67154E6738BE6D217BE8E01E2,
	NodeController_add_NodeRemoved_m4EFD8D34C2BD49CDCD03D1FCA53D4989F14FF922,
	NodeController_remove_NodeRemoved_m74D8172EFF48E13CFB8ECF57331815F12B3DB8BF,
	NodeController_add_BoundsSizeChanged_m995851F32772B7BA388CD2403AE084BE7DD68F42,
	NodeController_remove_BoundsSizeChanged_m73225EC52DB6BA434A9802565F18C51908D7E44B,
	NodeController_Awake_mC45B0157156A7AE2B2CC7A5C86DC7EF8BDBD4E33,
	NodeController_OnNetworkSpawn_mF65AE39C799BF8973B7DD41EC9EEC3CFC8F3C253,
	NodeController_get_RootNode_mB805CBA9B34507469D63D818104AF76A85E42E10,
	NodeController_set_RootNode_mD6593246594D07711554495307B21EE521F00C5B,
	NodeController_get_RootNodes_m5B08E7CABD23D8C54E9058ED739018D614CF7FF3,
	NodeController_Node_m3E1EDFA9EB2FD2583B747D33ABD0DE07D06A2BED,
	NodeController_get_Nodes_mDADBD21674E262201993C37E1EBFF9CF69D7BDFE,
	NodeController_get_NodesBottomup_mCA5BFA748B56C012E9552B005B4B9857B145BD0A,
	NodeController_get_Layers_mC1631C607B7D0E0A8216A455EE944FF3FAF2160E,
	NodeController_get_NumNodes_m7FC53FA04309411EB987F946F0D7F9C2EE79437F,
	NodeController_get_SelectedNodeIdx_m4B46B501CA8E204D9A36C2A2E59C428991706F3E,
	NodeController_set_SelectedNodeIdx_m0E2E7E6138D35E0E81B44353930A745198FDF33A,
	NodeController_SetSelectedNodeIdxServerRpc_m8C6E19D314318CAE1D68249A76DB8105130E71DB,
	NodeController_OnSelectedNodeIdxValueChanged_m4545349C537E01EA643A24D7E73B71676D1B318B,
	NodeController_get_SelectedNode_mB2B0F14A51FC2D04BDE3EDDF80F5494630416808,
	NodeController_get_HoveredNodeIdx_mF7EDAC68588BD8B07AF64388429A8CCA084D33A5,
	NodeController_set_HoveredNodeIdx_m7BA16E87AC59E446DE4CB871A3719D141A9D0F87,
	NodeController_SetHoveredNodeIdxServerRpc_mC5ACF746B5D29DAC5C3BF66129DECA5ECCAD670B,
	NodeController_OnHoveredNodeIdxValueChanged_mBA5EDA7B95E744EA40762269005D522686E499CF,
	NodeController_get_HoveredNode_m3C1FE9FDEDB5428D96EDDAF20DCC1A5997DEEA69,
	NodeController_get_GroupedSelection_m3292DEC97B73AF4E92FFDF9255473FCFC70A811B,
	NodeController_set_GroupedSelection_m634B94BDDAB406143EC43962878C577059377CCC,
	NodeController_SetGroupedSelectionServerRpc_m97B2456286A23A7084C89742E8AC3847E05019D6,
	NodeController_OnLayerHoverEnter_mD7E03939F495899957FCB49B7671156CAA464C34,
	NodeController_OnLayerHoverExit_m1B107DD8BB9DCE01730A90F7168E618404F47623,
	NodeController_OnLayerSelected_m76720BF754BE87418464E39779FFB9DA0FFCA1DA,
	NodeController_RemoveNode_m7E3A48134C24E33B20AC492CCA82ACD0A0180D3E,
	NodeController_SetNodeStates_m3D100F6966860DC9E3E1C2D426CC95A0315A198A,
	NodeController_get_NodeStates_m5BDD13DB9A972D00EF67282C28C5701249B7B7BD,
	NodeController_get_DefaultNodeStates_m8C585818030BDE0752D08482B200E08E3EA20517,
	NodeController__ctor_mE0C6C1BA739ECFC7EBCA77EF1DCA73C947408019,
	NodeController_U3Cset_RootNodeU3Eb__30_1_m1C617741785B4D11B7990A1F1A2408F47618AD54,
	NodeController___initializeVariables_mC9AB7DFABB75478ACEF4CBC3C6BE3EF6995BA10E,
	NodeController___initializeRpcs_mEE95381CF92043DF17795DE532DDCFAF5B0A6FE0,
	NodeController___rpc_handler_2277630493_m0B268225D5208B13A409F40EC71BDE268E3C7B7B,
	NodeController___rpc_handler_1356188875_m61A5ED577DB765399839C4E6D003D1772B46437F,
	NodeController___rpc_handler_3206384415_mFA33823112BC02759E40A70E60245C3A9C637267,
	NodeController___getTypeName_mAF170B46C0C63434518F9D29B3AF13C88D106C35,
	U3CU3Ec__DisplayClass30_0__ctor_mCF91A0A339674BBD148825210685E65D2D2A1C05,
	U3CU3Ec__DisplayClass30_0_U3Cset_RootNodeU3Eb__0_m6F09DA9A01504A2CCEC07E7876B0F66AF103BD40,
	U3CU3Ec__DisplayClass30_0_U3Cset_RootNodeU3Eb__2_mC8FA07C9BA74F6AD8878B0F6A6119DFCF30C485B,
	U3CU3Ec__cctor_m990D5477F1FA651CBCBA6DCFE9A6936573108B79,
	U3CU3Ec__ctor_m4ABE993B1910C2C8CBF0F8C385EFE68FAE723666,
	U3CU3Ec_U3Cget_RootNodesU3Eb__32_0_mAD2FA24D3BB2027F3DAE99FFB2EA7734B9C11E68,
	U3CU3Ec_U3Cget_LayersU3Eb__39_0_mB4D275C66A58E03257F1056827804B5450C7FCE6,
	NodePositionController_add_BoundsSizeChanged_m03C17170D4B1455B45C91E07A167EB0761FFEB0D,
	NodePositionController_remove_BoundsSizeChanged_m8E83F65C49A207C4F9BF6342E97680E0D7320EEE,
	NodePositionController_FitInBounds_m0D7C349AF1F762E035A81515290A3891C2602C70,
	NodePositionController_SetDefaultTransforms_m7E2E5A2FA4A00C4992C33F82B3D4A807388AE0EC,
	NodePositionController_SetExplodeTransforms_mAB76875CAECF2F628601101C34CF4843EA634B78,
	NodePositionController_TransformBounds_mDCB5E4EA1D4F2B1661DCD72241E70AE58C281E32,
	NodePositionController_GetCornerPoint_mCF2B22C53D94C2FC2768EF193C8BE55D92D8BB4B,
	NodePositionController__ctor_m4697DAAD0F0493AB677E4C7351D881FB8C9F8F0F,
	NodeVisibilityStateController_add_NodeVisibilityChanged_mA074BF5C6CBE7629FBD6EBE5E8E137BB26FF1EAA,
	NodeVisibilityStateController_remove_NodeVisibilityChanged_mCDD3A8A6B0EA986F6E855D7305F4EBF9A808817A,
	NodeVisibilityStateController_get_ShowTooltipSelected_mAE456EF0C858114E11409B543FA1B4C844E42062,
	NodeVisibilityStateController_set_ShowTooltipSelected_m138ED3BE3DE60E9E8CE55F3E94AE01D700E22264,
	NodeVisibilityStateController_get_ShowTooltipHovered_mD927A2C292699F487C2EC7073370962D04DA09AE,
	NodeVisibilityStateController_set_ShowTooltipHovered_m83C663CF46F0CACF191FDE6C49E45C4383ACA5CB,
	NodeVisibilityStateController_get_OutlineSelected_m11CB38F4D357E29FADFE31BB5190DFF0E06B30D1,
	NodeVisibilityStateController_set_OutlineSelected_mF4EDF4DC583E636BECFC743C427FA10898EDCEDA,
	NodeVisibilityStateController_get_OutlineHovered_m2EF237C7FD26493AD7C597CAD20079F84482755F,
	NodeVisibilityStateController_set_OutlineHovered_m80418AB986A5FFDF1BDC796FA25E5AD9B420F485,
	NodeVisibilityStateController_get_NodeController_m5F6BF11594011B0223A4A5582FDF10CC07381974,
	NodeVisibilityStateController_set_NodeController_m707D189E4188CDAD27A0764E176117420331855A,
	NodeVisibilityStateController_Start_m838050143B8A595D40A6CA58D968D49973D2B4F6,
	NodeVisibilityStateController_OnDestroy_mE5274783EEB67274622B9D81F81BA9E286846282,
	NodeVisibilityStateController_RegisterEvents_mE85066357DC5B2669E6163BFB0BB84EAFCF9457B,
	NodeVisibilityStateController_UnregisterEvents_m840B4CDD375FB97475B164E350123BEBF5A04124,
	NodeVisibilityStateController_OnNodeAdded_mE7EE6EDCA3E582FD58D21B8BADC09C7D402514DA,
	NodeVisibilityStateController_OnNodeSelected_m3C8FB1F1A2D0D824795829C87262F4A07A21E004,
	NodeVisibilityStateController_OnNodeHovered_mA036937B0DB96B9AC44C0C4683B568D49CE6CB29,
	NodeVisibilityStateController_OnNodeDeselected_mC8178693F6EF69B00B58D31803A4A8B6C29F3BC5,
	NodeVisibilityStateController_OnNodeUnhovered_mE99384D59F5A6800928C0D8EFCFDE7EE5061A8CB,
	NodeVisibilityStateController_IsHidden_m73779E6CB3783A62AC87C8E826F8284A5C0DE83B,
	NodeVisibilityStateController_IsFaded_m731A50A14843285595ECCA03D23FCDED06FD36A4,
	NodeVisibilityStateController_OtherLayersHidden_m0BF9FCA6F53C1D1B206017C7DCC15CD04114BD57,
	NodeVisibilityStateController_OtherLayersFaded_m4ABDA720E5D77FC9E2C9C028452C1CD540F3B261,
	NodeVisibilityStateController_ToggleHideNode_mC0234DDDF211650A69EB0D954DAD7C63E3384D22,
	NodeVisibilityStateController_ToggleFadeNode_mCE0D8612D9B9743F35E1201A92EC1C1FD5E6824C,
	NodeVisibilityStateController_HideNode_m9E34FDB0E8DAA7F707E4AADC1EDEB6F39A61EFA3,
	NodeVisibilityStateController_FadeNode_m02F4813518E5BDD2831B99E27B2D9DCACC02CAA8,
	NodeVisibilityStateController_ShowNode_mD0BDCF8F9DFAF57B1C4F65F8E27741D634EC4F7F,
	NodeVisibilityStateController_UnfadeNode_mBB0D92EBDC016838992E271D79794CA38C437E0E,
	NodeVisibilityStateController_ShowAll_m4DD4CDB7BDF9EE1564C5C7FEA2B9B9C99301CA91,
	NodeVisibilityStateController_UnfadeAll_mB0246734BF6F8A8637E0EF3E870434EC53E52D94,
	NodeVisibilityStateController_HideAll_m3B2D063B876AE108473A081A7C418E674C2AA572,
	NodeVisibilityStateController_FadeAll_mAC156C8AB2EBEA2C092146EDAE06DB16447037DF,
	NodeVisibilityStateController_HideOtherLayers_m9841BFC2F3C3EC6F6E567A0BB2179DE8580B737F,
	NodeVisibilityStateController_FadeOtherLayers_m7E4374B453D67C78675E694713526C89154325B2,
	NodeVisibilityStateController__ctor_m4B30C3FAF9D2265B3F16A1D21A392E628CBF775D,
	NodeVisibilityStateController_U3CRegisterEventsU3Eb__30_0_m41902C51F2737F191B54DDBD9E3503B48931DEC7,
	NodeVisibilityStateController_U3CRegisterEventsU3Eb__30_1_m8A586A5ED9797BEAD33FA2D1611B5842D56D8C88,
	NodeVisibilityStateController_U3COnNodeAddedU3Eb__32_0_m88962164BB7B01403C078AF6C4D25AC6B15BC160,
	NodeVisibilityStateController_U3COnNodeAddedU3Eb__32_1_m01293661F5FEEEF471689DCAA6CFFA5268421120,
	AnimationMenu_Awake_m373435FBADE18D6ABD6139E83DE4E7D03A4112C7,
	AnimationMenu_OnIsPlayingValueChanged_mF5682A9DB34F4B40C943449DBDC60FBFF75617F3,
	AnimationMenu_OnClickPausePlay_m33451FC1F961DCC0130B64CA098BB5554A6CA806,
	AnimationMenu_OnClickStop_mD9C322C1E63FC1948F82D9A0D3495085C04419C6,
	AnimationMenu_OnAnimationSliderUpdated_m33D790A5F8F4371E52D6ACC23A50A116D71F2B64,
	AnimationMenu_Update_mCBA8F3C08102823EEE46F9CE49342F3E138D156E,
	AnimationMenu__ctor_m5E0935D7684BC162E036C07477E9A6B59BF20E26,
	CourseMenu_get_SelectedCheckpointGraphName_mE7485583238B4F692A814A945CECBD60CF325678,
	CourseMenu_set_SelectedCheckpointGraphName_mEEA997763934F5D26D01F8E3DFA9186E5EE16005,
	CourseMenu_OnClickStartCourse_mF311853DC459CBC6D73D6EDF61288A514F78D90A,
	CourseMenu_LoadCourses_m1AEBF04DA67008A842EBDC5F341510C87200484B,
	CourseMenu__ctor_m6F5B4C5378588CF9ED02253D7DAF049233F924F8,
	U3CU3Ec__DisplayClass10_0__ctor_m4640DC48A59D9989F12CFBE56CD69CF9A42F9D1A,
	U3CU3Ec__DisplayClass10_0_U3CLoadCoursesU3Eb__0_mA8CCA323A5A1EF0E055665F74B83A261F4275F98,
	U3CU3Ec__DisplayClass10_1__ctor_m90A5B3BE280DF6189578333933A4B4C22CB62AF2,
	U3CU3Ec__DisplayClass10_1_U3CLoadCoursesU3Eb__2_mE70B340315BACFA5FEBB52374F2CFE1B5D30BB9A,
	U3CU3Ec__cctor_m0182A1B2CCB01155E87C96A35CA98673DC8153B7,
	U3CU3Ec__ctor_m1687C2A533A3290CDD2758DDCA821B4820A96878,
	U3CU3Ec_U3CLoadCoursesU3Eb__10_1_m730CBCAC9EEB066487EA614BDBEFD8A0C8FCD043,
	LayerMenu_SetUIDirty_m43DCFCC06B1A76DD94FF4EF173D646722508F43B,
	LayerMenu_buttonIDToNodeID_m0976D668098B61EDBD3580DCDC71C90EE48C7527,
	LayerMenu_nodeIDToButtonID_mF3E146159082136968EE91126B7B2E98D3CC1042,
	LayerMenu_get_StateController_m7ED0F7AB7AD056A484AA3FA45A65FCBAFCA72F73,
	LayerMenu_set_StateController_mE014352D09519195EE20AFE038D211A156B91C54,
	LayerMenu_get_NumButtons_m7DB608DD8C8C24FB45A03F1F191968ED7EE71BE1,
	LayerMenu_get_NodeController_m5B7B309401E64621101E7165005EE2D2E9898EE9,
	LayerMenu_set_NodeController_m82F460BD02258F5B419E413832334885A42C5534,
	LayerMenu_OnNodeAdded_mBBED3CA04190677C53F021EE69B786A1F44219EC,
	LayerMenu_OnNodeSelected_mCE901AA3BE3D782EEFEE3580F68A1CADA3E959E1,
	LayerMenu_OnClickHide_m3195379BF76B6CBD5191AC08415BC58EE5DAB80A,
	LayerMenu_OnClickHideOthers_m1F9E79A9A5474A4C3E147480F257CF3DA061D9CF,
	LayerMenu_OnClickFade_m1A1B1D71EED4C87B6198B6589A88D42423DBFB35,
	LayerMenu_OnClickFadeOthers_m59179E112E76BBC85C88E2E61A8C4D8E4A8241E2,
	LayerMenu_OnClickShowAll_m10D7EE07003526374ACB0EF66F147E17CA9A4094,
	LayerMenu_OnClickUnfadeAll_m7B864D11FBA4B14C55F3CC8A1165843DB2E0F695,
	LayerMenu_UpdateUI_mDC61D8BD72752616CBD5D4D6044099F14F732BAB,
	LayerMenu_OnDestroy_m6626FAD1F5DDC7C9A479CD65878ADA19D31506F4,
	LayerMenu_Update_m23D4C1374DDFC3BB24B00B7B44ED674EC2BCFC90,
	LayerMenu__ctor_m8EFE84B60F0D8CA708744946391AE9B96C8B04A7,
	LayerMenu_U3Cset_NodeControllerU3Eb__27_0_m8043AEEF4542C6EED423333E930AB386E99C6DAC,
	U3CU3Ec__DisplayClass27_0__ctor_m5AB9B23E58BCB793749BB4F3245A1A6D44F93E56,
	U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__2_m42A4BF08F230F812CEB7FC07E12C4703D0CB5539,
	U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__3_mD9DBB46D148B07E0A3871768156EA24548990520,
	U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__4_mC83FF2057180B5940C2B2CCCDC7B16F8503C82E0,
	U3CU3Ec__DisplayClass27_0_U3Cset_NodeControllerU3Eb__5_m58CF3E24336795B72A235062EE7E4F792A75D418,
	U3CU3Ec__cctor_mA0E2DFBCCE6779162AFEC87C974263DF8A68243E,
	U3CU3Ec__ctor_m353A9DC42FD5DE0CECA17F31F0D0174AD341A3B4,
	U3CU3Ec_U3Cset_NodeControllerU3Eb__27_1_mE05442CFBB1709F29A2F2E93C64D20EC8C9D6881,
	NodeButton_get_IsLeaf_m16C4F324748A06489E8E7E02D94CCEADB8167FB8,
	NodeButton_set_IsLeaf_m6A2F97455FBB594E859055AE02EC8447217303C7,
	NodeButton_get_IsExpanded_mF15ADC3482C85837D4ACA05E82CC47A980322C96,
	NodeButton_set_IsExpanded_m1C655AC358C19687882D7F50094F5ECB1324A020,
	NodeButton_get_Level_mBFC40B9DD56F137235972460B734507A6F09C00C,
	NodeButton_set_Level_m6084C3DEC2F7B7CDF64582C475724C68123D9438,
	NodeButton_get_IsVisible_mDA207A8F7EAEBEDD554E69A3BB384DB88614E67B,
	NodeButton_set_IsVisible_mACB89EB05B4C5284D892589FEA78F47B7C752C39,
	NodeButton_get_IsOpaque_mB3532E400EB01E0D7B0B45E696969BA9601F955C,
	NodeButton_set_IsOpaque_m81AA456CD023250F73A19000BB729AE55EA969CF,
	NodeButton_SetNodeInfo_mD418A0E9BFAE93B35EBB672004A9CECD291FCBC8,
	NodeButton__ctor_mDDB4B3B0F75CF064A2A55F74134341182439A294,
	SettingsMenu_Awake_mCDFFB87F0B63DED3D973531C7E3CC95E3CF1495C,
	SettingsMenu_OnClippingPlaneIsActiveValueChanged_m0399DAEA914559A296B5968218AE9C727D6294D0,
	SettingsMenu_OnClippingPlaneTextureIdxValueChanged_m943863E91E31614F39E2A86E912AF695E3710824,
	SettingsMenu_OnAnimationSpeedChanged_m57651F442265423B2FF90AFEE0B505D0810BF0B5,
	SettingsMenu_OnRotationSpeedChanged_mE952487B07F18581AD0F256C88E852B997BA8353,
	SettingsMenu_OnAutorotateModelValueChanged_mAACB6BC2AAFFFB2AADF5C439DCC0B3C9D49D3986,
	SettingsMenu_OnChangeAnimationSpeed_m7108D368ACBAF429619006FB9C238757314954B0,
	SettingsMenu_OnToggleAnimationMenu_m2415C6016E565D91E8125C54F64D4988C71B7191,
	SettingsMenu_OnToggleModelRotation_mBC7D2C4655CC94B522BC35D77661CF68DA0C4938,
	SettingsMenu_OnChangeRotationSpeed_m396C6F5636031E3A50D9387FFDF10A46E708BCAC,
	SettingsMenu_OnToggleClippingPlane_m387B9183E340E3873F14AB207BE41A333A840B27,
	SettingsMenu_OnSelectVolumetricTexture_m7168349B722232B4D0CDC7B534E95D9F9C384ED3,
	SettingsMenu_OnCheckSelectionMode_m1A03EB05D72745AB71D74367FDABC1A7BCD944F7,
	SettingsMenu_OnToggleShowAnnotations_mEC91B55D15AF6F9D66D1F0EECAAA0AE5A6566766,
	SettingsMenu_OnToggleReadDescriptions_m296014715B40541EB4FB63A96AF6C04F240DFE9E,
	SettingsMenu_OnToggleGroupedSelection_mF6738EB77B987B360D6EDECAE3E46D071EE26289,
	SettingsMenu_OnToggleGazeInteractions_mC78F6A12373D2C311F6A63F9FDD05CC9BF018A0B,
	SettingsMenu_OnCheckHighlightSelectedLayers_mD7C8539D38271B67EE8017587F154C78023C8BE0,
	SettingsMenu_OnUncheckHighlightSelectedLayers_m62778E32C3146F55CCEEC32850CFDEF687E9C326,
	SettingsMenu_OnCheckHighlightHoveredLayers_mAAFA70F12FE459B3702A41AF0DFDEB9CFFD8CE4F,
	SettingsMenu_OnUncheckHighlightHoveredLayers_m58E3690888BED5A481895D36BF4E7EA92BA754D0,
	SettingsMenu_OnCheckTooltipSelectedLayers_mE0B8EE32ECB351C9A7C25DFAB5F1C111D578A758,
	SettingsMenu_OnUncheckTooltipSelectedLayers_m40E398B8CA5A898BA2B41A075071E503C3146987,
	SettingsMenu_OnCheckTooltipHoveredLayers_m73C979AA247FC11208ADD9531E2ECB6D4DB2EDA1,
	SettingsMenu_OnUncheckTooltipHoveredLayers_mFFA1EE00CC7C6D4B821F4E7C8EC1C928FA44D2FD,
	SettingsMenu_OnToggleColorLayers_mFE4A5D8A444D62F9471CF133207724FED0C36331,
	SettingsMenu_SetModel_mAA2DB9619E5F19AA630F479B26FF9EF88DFA52D1,
	SettingsMenu__ctor_mAB642D09DCFB585F2998C81DE33DD08C5C420E1C,
	ShortcutMenu_OnClickCloseModel_m6ACE1740413E5A4F6EA45CBF55EBE4D72B80F712,
	ShortcutMenu_OnClickShowMenu_m449328845141DE1F9E362A1717E96D3ED7337243,
	ShortcutMenu_OnClickShowBoundingBox_m2C0A4AC4BE481244D50B74D58FAD357F34E584B4,
	ShortcutMenu_OnToggleMovementType_m1E83C4A30FEAB75A3C3E0C82CD7EFE1F53963999,
	ShortcutMenu_OnClickExplodeView_m493B8F877F200C2BB3808468BE71DA34647890AF,
	ShortcutMenu_OnClickResetView_m90E725C215BBB9702DA7B40C08B8326811D4CAD6,
	ShortcutMenu__ctor_m10EF98CBBFA8429E2F8A0C06774EA245104E980B,
	UIController_get_CurrentTab_m1BC9AFE655D91A01C370BC5F3A0A007775E98D7D,
	UIController_set_CurrentTab_m672F6C81839419233E8C80FBA99D095AF8E2FB32,
	UIController_get_SideMenuActive_mF0683DC3E7FCBD0276A3CA2092AA02E3C6530064,
	UIController_set_SideMenuActive_m5C462EEF0783B8BC4126BA4BBC4E9EE1AAA0042F,
	UIController_get_AnimationMenuActive_mFDE1F70C52451C3717BE83F25F9A732E4D3A9ADF,
	UIController_set_AnimationMenuActive_m3850C11E8C14AF0A3BA923D80C27C94778F7A709,
	UIController_get_TabsActive_m322AE6983C1B9341A8B85EF7C58BC28E029B0964,
	UIController_set_TabsActive_m781D217EAD65283A949292F06743E8A5F94A1429,
	UIController_ToggleSideMenu_m5BFA7DF1438848343829E30D6E0FBFBDB4F0C9BC,
	UIController_ToggleAnimationMenu_mF7138D086387EE5324CE75AC635DC7E4BDAC2681,
	UIController_get_state_m03F931F9E53911B99798BA40C35CC73FE6DD5CC9,
	UIController_SetUIState_mBFDE8CDE7EEF1D9F2DFA9A297F787AC33F33895B,
	UIController_SetUIStateServerRpc_m03F6569E7BE9B8F913D6DD216F91320B8C20C739,
	UIController__ctor_mFF218DBC8CCEFE36AAC295D2376501658CD8B7A2,
	UIController___initializeVariables_m1510A4F84E5851F32AFD8D6D125F5104F7432F69,
	UIController___initializeRpcs_m83A8CEA1408B47ED405E2A449C04C721419F110A,
	UIController___rpc_handler_4105482430_m4094C44647DE97A52211ED34C03B0E3B51A27045,
	UIController___getTypeName_m9DAF56EBE6947A15BBFAD9B3A1F191413707A154,
	ModelInstanceSpawner_SpawnModel_mD086D4B7B704881D4664061564930491F533FF29,
	ModelInstanceSpawner_SpawnModelServerRpc_mA59C14969E9F1BCF5792E3475B7AB3B52ECAD0BD,
	ModelInstanceSpawner_Start_m8649BE53E8E63548C1339746570DC79EBF9AAD8D,
	ModelInstanceSpawner__ctor_mB6015FF84127E16F2A5AEA57B1C95523A5705EE5,
	ModelInstanceSpawner___initializeVariables_m26A221CD816989645FC9A18BDBC4BBF937466E78,
	ModelInstanceSpawner___initializeRpcs_m9A58BEAF0A054C7E87CA7ADB9244606D9A4235AB,
	ModelInstanceSpawner___rpc_handler_2237502063_mD08A176DE377C74494F7B9DA5C61E6869F617A03,
	ModelInstanceSpawner___getTypeName_mDB29FBF854C6F2DC5A9C53085BAC5B4539C750EC,
	U3CU3Ec__DisplayClass9_0__ctor_mE745783EA9BECFAF435DFE55E0E750EC8AEE2798,
	U3CU3Ec__DisplayClass9_0_U3CSpawnModelU3Eb__0_m84AA7B89C78C39FA7119565FAF9D99F6560CB979,
	U3CU3Ec__DisplayClass9_0_U3CSpawnModelU3Eb__1_mB628926E8C1BC5EB6726204252213AED392BAAF0,
	ModelButton_SetModel_mA7F07EB8DBC3EF06250370EABF155362C3A78259,
	ModelButton__ctor_mB0A326B1309322468724DB8C37E2C4A66F7264B2,
	ModelSelectionView_Start_mBED178692CCD393B727BCBA5B114900E99884355,
	ModelSelectionView_SetModels_mC83E43B1CE9FD3C6FFD93A22338B0FF53EB19548,
	ModelSelectionView_InitializeSelectionButtons_mAD430A8509190FFB72BB7B4057946C2DA1AFD6CD,
	ModelSelectionView_SelectModel_m058885C6E9ACDF8DFE54D7765DFC34C88AC573B7,
	ModelSelectionView_get_SelectedModelName_m4F1022505A8BC00CC6E93E75CD48054006EE3F94,
	ModelSelectionView_set_SelectedModelName_m4159DD2D7CEAC20D12C01AF455F953E9B4858FD5,
	ModelSelectionView_InitializeSpawnButtons_mA344BD21858A2A0067E89BEFCB570CC22FB431F7,
	ModelSelectionView_SpawnSelectedModel_m435A5B71DC4D77241F45AB4538FF24D910F82840,
	ModelSelectionView__ctor_m866FF33FAEE00D8DC0DCC296CBEB5BE523B57822,
	U3CU3Ec__DisplayClass14_0__ctor_m6F6C57530FF060B871E435A008D6085431DC4922,
	U3CU3Ec__DisplayClass14_0_U3CInitializeSelectionButtonsU3Eb__0_m8C912A84AD9FB4AE2B6D81FC9C7A738D94F6D42A,
	FaceCamera_Start_m36C189451F380B7FA03494B1466276854CF49FBD,
	FaceCamera_Update_m295E828F20DC5F8CADC64934C989C94F27592CA3,
	FaceCamera__ctor_mC5D44A53DFAE40B1103AEBB2A513D3CE453AE25A,
	MoveWithScale_Update_mA4F9AE47ABBED7BAED9889BAA34D0B03D7741D1F,
	MoveWithScale__ctor_mD1FB2A319A8EE4CA0A04D2DEA01F406290F42266,
	Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB,
	Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7,
	Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AddressableGUID_GetHashCode_m1B75548F336B04CBCC0F79D343C8B7793A5428CD,
	AddressableGUID_ToString_m4FEFA14381A279352C222CAC78765D64877B0F57,
	AddressableGUIDEqualityComparer_GetHashCode_m0ADF96C8AE96E81949584C4957A5841F99CFE400,
	AddressableGUIDEqualityComparer_Equals_m0EBF00D28B970E0FDAAC4288F762C5289AF85668,
	AddressableGUIDEqualityComparer__ctor_mDFD280AEECF2CE714179798711BA35ED48744DDC,
	ConnectionPayload__ctor_mB9FE78986306A8A447B39B2F3E9FE0DF402DCD36,
	DisconnectionPayload__ctor_mA0E6EEC2B19E7AD14A949ADCA0613617CB148880,
	DynamicPrefabLoadingUtilities_get_HashOfDynamicPrefabGUIDs_m19BE6FB0EC2F55CAC5D5526EE38D145311BF173A,
	DynamicPrefabLoadingUtilities_set_HashOfDynamicPrefabGUIDs_m70F46E1894B38B2695D9AC801F96BB4124F9606C,
	DynamicPrefabLoadingUtilities_HasClientLoadedPrefab_m5FDF779AD901113110E7DEBB5FDAD32F7F79799A,
	DynamicPrefabLoadingUtilities_IsPrefabLoadedOnAllClients_m0547BB30966EF228D54C95698266D4F02084BE52,
	DynamicPrefabLoadingUtilities_TryGetLoadedGameObjectFromGuid_m590430CCE0D7B8B27796D289AA9712B19F8C6A59,
	DynamicPrefabLoadingUtilities_get_LoadedDynamicPrefabResourceHandles_mE3FE39EBF4C9C60C2B9C37553CC8A4BDB04A496C,
	DynamicPrefabLoadingUtilities_get_LoadedPrefabCount_m7C1CC63B963E14E571472E008EA8A14371408A85,
	DynamicPrefabLoadingUtilities__cctor_m26546A581E4ADF74C06B13411561260B42E7988D,
	DynamicPrefabLoadingUtilities_Init_m8E8CC5A0009C96664F9F456AB9DD56BBC3F57384,
	DynamicPrefabLoadingUtilities_LoadDynamicPrefab_m552BB90861F6C915C1B67491430E3E0446C372BE,
	DynamicPrefabLoadingUtilities_LoadDynamicPrefabs_mE8922623F2A8555D7DEA57A5CFC505506CF19954,
	DynamicPrefabLoadingUtilities_RecordThatClientHasLoadedAPrefab_m467D99718CCC83E2E98A18D9FE0C40CD4323DCE3,
	DynamicPrefabLoadingUtilities_UnloadAndReleaseAllDynamicPrefabs_m9174EAABC95326B2FE7A4357524E0DD64FA41DEF,
	DynamicPrefabLoadingUtilities_GenerateRequestPayload_mE03D3133A759A5E90DF8B72299E7D644A7580E88,
	DynamicPrefabLoadingUtilities_RefreshLoadedPrefabGuids_m47C3E0A258A7E251AB0B1CD66CEC128E2F8F33A3,
	DynamicPrefabLoadingUtilities_CalculateDynamicPrefabArrayHash_mDD33053B593190159B45A369A256C36C7303A397,
	U3CLoadDynamicPrefabU3Ed__18_MoveNext_mDAA5B95B0DEC4B3CD160BE6FA7A41995C186005A,
	U3CLoadDynamicPrefabU3Ed__18_SetStateMachine_m7201FDC8677C4768C3AA33252F3DD9916A159DE3,
	U3CLoadDynamicPrefabsU3Ed__19_MoveNext_mBAF81017A61D5A5310906EC39EFA0762615B4647,
	U3CLoadDynamicPrefabsU3Ed__19_SetStateMachine_mDECD003D0E34E262C5F471404EF89DBAFB80948F,
	U3CU3Ec__cctor_m35FAF4C9C8712BC1CBC9A022AEDD2B1334C207A1,
	U3CU3Ec__ctor_m29428986C77FD6386DCAA646F72B95C9D6880EB7,
	U3CU3Ec_U3CCalculateDynamicPrefabArrayHashU3Eb__24_0_m26647CE7AA489E827A481FCE704ABF3639AE0C04,
	DynamicPrefabSpawner_Start_mB0B47EDF54532290D0E2D09C9D9FD48076F48AF1,
	DynamicPrefabSpawner_get_Instance_m36AC7CE906776D734C683CA154EB5C32360FE103,
	DynamicPrefabSpawner_OnDestroy_m4735259261C8F3A219E988E8BF319A7E940A74EC,
	DynamicPrefabSpawner_TrySpawnDynamicPrefabSynchronously_mE47945473775AA77493F3A392B18B4AE838292BC,
	DynamicPrefabSpawner_LoadAddressableClientRpc_m9DFEE617D31EFFA3E90BE63ED719099D2632C1CB,
	DynamicPrefabSpawner_AcknowledgeSuccessfulPrefabLoadServerRpc_m61B640057F09727F81B1E1D24B83D6684886AFA9,
	DynamicPrefabSpawner__ctor_m4E4A4B82143EB0E9D5B7568AA3D3FCCD2F73C012,
	DynamicPrefabSpawner_U3CTrySpawnDynamicPrefabSynchronouslyU3Eg__SpawnU7C9_0_m0C693D961713E5CD866FA519E31A4700152EFD8F,
	DynamicPrefabSpawner_U3CLoadAddressableClientRpcU3Eg__LoadU7C10_0_m4503A9D24CFF35AE281924EE95D874290E8B0447,
	DynamicPrefabSpawner___initializeVariables_m8CB06D5038D080C2241CD7FBED43A8AF31147BF4,
	DynamicPrefabSpawner___initializeRpcs_mD964C13E99FFDCD98C2D4C0A07CA8F9742870E6C,
	DynamicPrefabSpawner___rpc_handler_1610375934_mAD3467A88B31FE10394C88E064259741AAB037FF,
	DynamicPrefabSpawner___rpc_handler_3270584422_mC653BCDC5394C010A1CCAB0E35A6204B1F6BE11E,
	DynamicPrefabSpawner___getTypeName_m486A7A4C8B4036C42484F515C3FF55A5EBC4E9D5,
	U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_MoveNext_m5A60F480F8432D153F33112324D84A881C8012F7,
	U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_SetStateMachine_m6646A5927C420EDF0F8AA7F7D6D686905FBA611F,
	U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_MoveNext_m84534720935749153F37C828F1959A67C50394B5,
	U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_SetStateMachine_mC4A20EEAFB77E59E681CD112D7AC68B391272CC3,
	DynamicPrefabSpawnerOld_Start_m568FDF57F95A186A18352347624BF6C3E8A0C967,
	DynamicPrefabSpawnerOld_OnDestroy_mABF481ACFFE29EC9A01CD289FD0FF4281B4EB505,
	DynamicPrefabSpawnerOld_SpawnDynamicPrefab_m4D6A71687B53D57F4D2DA764255A5ADBC4A8CF90,
	DynamicPrefabSpawnerOld_ShowHiddenObjectsToClient_m80EE8FDB7AA2D7733DDD5D5CA0279B167076FA65,
	DynamicPrefabSpawnerOld_LoadAddressableClientRpc_mCD37E86E519B61F6E1D012520BD30BEFDFD8CF36,
	DynamicPrefabSpawnerOld_AcknowledgeSuccessfulPrefabLoadServerRpc_m232CD356B4D12D8F28470964F3117C93E5F2F8D7,
	DynamicPrefabSpawnerOld__ctor_m5FE5990ED3402C5E2906978FA6600110238E29BA,
	DynamicPrefabSpawnerOld_U3CLoadAddressableClientRpcU3Eg__LoadU7C6_0_m44946B3C3C2C8158514203593B6F596353F706C2,
	DynamicPrefabSpawnerOld___initializeVariables_mCAAB45ED7CB411BCF42021D2E554CA305645A1E5,
	DynamicPrefabSpawnerOld___initializeRpcs_m6E2F9006E195C79183192DD9A06F12C67FA955F4,
	DynamicPrefabSpawnerOld___rpc_handler_3300809267_mB15D4F9708662014008E5DCCDD163EF4E45C9969,
	DynamicPrefabSpawnerOld___rpc_handler_1977834903_mACF6F6FBCC5559F2919EB2BA76A8CB11FDFE3CE0,
	DynamicPrefabSpawnerOld___getTypeName_m3497A2B0044806A6F3D36192D65D6267C2AED4D5,
	U3CU3Ec__DisplayClass4_0__ctor_mCFDFE43FC78830C0EA7537108540338DB6C29920,
	U3CU3Ec__DisplayClass4_0_U3CSpawnDynamicPrefabU3Eg__SpawnU7C0_mCDECFD2395BA8F993F17E29CCBD96C57D6F31345,
	U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_MoveNext_mB062C756B59D76F04BB4EF0B3C4A3704712512EC,
	U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_SetStateMachine_mC1A34E7246CFDA1FD65C6B857FF0E8110B126386,
	U3CU3Ec__DisplayClass4_1__ctor_mA2AA687212B817970414D709DBC42FED5C085BA8,
	U3CU3Ec__DisplayClass4_1_U3CSpawnDynamicPrefabU3Eb__1_mF264D4648102270E4ACC1D024338A1623AF9AB9F,
	U3CSpawnDynamicPrefabU3Ed__4_MoveNext_mB88B52A3253E26DCA53797F7312041743C7A6A05,
	U3CSpawnDynamicPrefabU3Ed__4_SetStateMachine_m287475ABB1EE5F9AAF6CB4194AD5FD71B141C724,
	U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_MoveNext_m848CA185D81D33BB8E707503D26BEDBF3F4BFEF8,
	U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_SetStateMachine_m46CEE3262BCDF0776CD304BCF73B1715360897D2,
	OwnershipController_get_ObjectManipulator_m61005773A3C82488D60462D0CB4F989249A5864E,
	OwnershipController_set_ObjectManipulator_mAF273D77C60A6605F6CD10481D7BA31DA4284A8B,
	OwnershipController_get_BoundsControl_mB274C53130F3D3FD2A5A67E5A8EDE57C8AAF6444,
	OwnershipController_set_BoundsControl_m0E115ED5171C4002B146E7BB9DE6ECCFE1730D0F,
	OwnershipController_get_objectManipulatorTarget_m5A2C83F0CC1026FFFDBEEE88578263750AA2CB16,
	OwnershipController_get_boundsControlTarget_mA9C68245BB52B763E487601E7C54A43AA718ED34,
	OwnershipController_Start_mE4F9D271F6CF8C6ACB8D640042F9930E01A64F02,
	OwnershipController_OnEnable_m6FCEAF67FBC662AA4647CB907AAF44979C05C2CD,
	OwnershipController_OnDisable_m8F9D4F2BC664F698AB52135DB6F96B68542AF7B7,
	OwnershipController_OnDestroy_m70E8FD87D3822C72E670754DCC8955952BE8D748,
	OwnershipController_OnApplicationQuit_m02F6445DF1E934173E68CA252D86A9613E3BD3B9,
	OwnershipController_RegisterObjectManipulatorEvents_m8DA55A37B445A18CA0F6ED5670DE62B6BCB553B5,
	OwnershipController_DeregisterObjectManipulatorEvents_m480E784B4E75A0238AC24AC5088EE6BB3BFC95C6,
	OwnershipController_RegisterBoundsControlEvents_m16725AEEE5F82B9EFCA3FB862A493A6C18655DB7,
	OwnershipController_DeregisterBoundsControlEvents_m2D6D25DDE283329D1A48A91264C931770F149097,
	OwnershipController_OnLastSelectExited_m31762788BA6C3D32D17EFB254E631682BB1DBDA9,
	OwnershipController_OnFirstSelectEntered_mEEAA386A701B1EDDE8D7E6023FF35062AAE95992,
	OwnershipController_RequestOwnershipServerRpc_m66484EB34B34BED2D164D96925931D1B306C11A5,
	OwnershipController_ReturnOwnershipServerRpc_mE6F54C3B7C2F2DBF1CCC5B388749F42C6EC2B9BA,
	OwnershipController__ctor_mDBDE5993C25E1C267E2EADC540B1452ECA748CE5,
	OwnershipController___initializeVariables_m2CA9521F217BFD208AAF7C08EE78419EF4B4A75D,
	OwnershipController___initializeRpcs_m5A9703E3EB4C724D0EA9DB10311C69B3C1672E21,
	OwnershipController___rpc_handler_977887190_m75BDF3C3283617B0543D428A8AB6833F70F6E774,
	OwnershipController___rpc_handler_3349833540_m6CEA80AE24F5B2F1AC6A223524C4FEAFEC4E1E67,
	OwnershipController___getTypeName_m3CA781E07A91B6250E23557630123FF059C711D5,
	ActiveSessionManager_get_activeRemoteLobby_m3B30BCE0B63AB9B49CF326979A519470C43A7DD4,
	ActiveSessionManager_get_activeLocalLobby_m497D720DBCCE2EA11BEBDE6C4306A0BC05D4FC54,
	ActiveSessionManager_get_SessionActive_m5D1646B17E67801AEBD403A83799C730C6367DAA,
	ActiveSessionManager_get_IsHost_mC7B96E2CECE4A0D9CCA9DCFC89BABBC6A95E0E0F,
	ActiveSessionManager_get_IsRemote_m7AFD530EE86FFC411C06DC6F7A3EE1F0E7F2B40B,
	ActiveSessionManager_get_activeLobbyInfo_m53926F408AB7C309367B216A9B7AF24C41B9A098,
	ActiveSessionManager_Start_m1BB3409145C55D4A61622F60A47B67218724AD1C,
	ActiveSessionManager_StartSession_m24C41E3E45BF550F6D95A398115BDCD6ED93E4A9,
	ActiveSessionManager_StartLocalSession_m08D4649D25A7D4617F33B57DE419BCB148DF0A2D,
	ActiveSessionManager_StartRemoteSession_m6A92962E515186677BE7BA8404BD100806AA1C6C,
	ActiveSessionManager_JoinLocalSession_mB2C6537CE273BD38ADC36A91F329B1B7B8059FFB,
	ActiveSessionManager_JoinRemoteSession_m759E806864C402974AE1BF6C5B9A698E5BBFBB2D,
	ActiveSessionManager_ExitSession_m36B16BF66C83C4DA7454D3A2F28376C021B55D94,
	ActiveSessionManager_TransferHost_mB2FF473C23D0C790F06E11E5EB857D2566AB9CA5,
	ActiveSessionManager_OnLocalSessionUpdated_m56997287A2B4F93C17B0D937A1D85ED2368713D2,
	ActiveSessionManager_OnRemoteSessionUpdated_mD66FC6DC917406087E31B7A5A8BD3310E32FB651,
	ActiveSessionManager_KickPlayer_m7B67744E70BF11CA7B7D9AA741AB7F391E1462F5,
	ActiveSessionManager_ShutdownNetworkManager_mE293CEF418420305AF0E931F10AF556DCF688B6C,
	ActiveSessionManager_get_anchorPosition_mAF9CB4EAE75213EB77672CE495DEDD0B2ED7950D,
	ActiveSessionManager_LocateAnchor_mDED0100CD00C439D517EE77D44F4DFC46CBE0C94,
	ActiveSessionManager_AnchorLocated_m9E8C44E914A28106160CE0E958EA3E02A7A95D47,
	ActiveSessionManager_PlaceLocalAnchor_mD92CA05EFE6C1CF9CFE9084DA402A814E6DE46C0,
	ActiveSessionManager_PlaceRemoteAnchor_m23F7008E0273138807F67E58150E2E20A1986BDB,
	ActiveSessionManager_CreateRelay_mD80CC991E27E6E33A2A9CCFD062E52AA95CEE80F,
	ActiveSessionManager_JoinRelay_mE20D85053EB6DC7565622FDE0CA8ECA987355FA3,
	ActiveSessionManager__ctor_mAB9D922A4DA294AE7A3AD6F7972B5A4812AE5F29,
	ActiveSessionManager_U3CExitSessionU3Eb__33_0_m54B1F2C38833E6398B00139513BCF7C806D07543,
	U3CU3Ec__cctor_m6AB018065D4872C7AC56381A4B27BD82618095C0,
	U3CU3Ec__ctor_mA585605B0EE44C1169C1169EAA497D95AA3D76F5,
	U3CU3Ec_U3CStartU3Eb__27_0_mC88780AAC375893E8A0731E3D0AEFF2BF51DABAD,
	U3CU3Ec_U3CStartU3Eb__27_1_mCD2924BE4C88F194F38E25A15D779CC183729BCF,
	U3CStartLocalSessionU3Ed__29_MoveNext_mC11450688E2D0A5ECEF84F28F37829DA0017B62B,
	U3CStartLocalSessionU3Ed__29_SetStateMachine_mBC67BDED374E292E47757E00BB3F26D1A870B2BD,
	U3CStartRemoteSessionU3Ed__30_MoveNext_mB8D0CBAC544812F856C95679905AD6F1AA41FA97,
	U3CStartRemoteSessionU3Ed__30_SetStateMachine_mB4894FD3F5D0E09A81BE6DC98C304351981276C7,
	U3CJoinLocalSessionU3Ed__31_MoveNext_m0DE55FB91C1D2392FFD73CD6C4E4DF93234C3C1E,
	U3CJoinLocalSessionU3Ed__31_SetStateMachine_m663F267133CBC463162F19434B82D50EDFD16E59,
	U3CJoinRemoteSessionU3Ed__32_MoveNext_mB0BF725FBA739BA3411520ECB2EEC0D04487D2E0,
	U3CJoinRemoteSessionU3Ed__32_SetStateMachine_mA2A614F5864911C4EC4866301D52DB8BF0CA9F5C,
	U3CExitSessionU3Ed__33_MoveNext_m97083D94FFDB66ABE557DA3F100E8B592B25493D,
	U3CExitSessionU3Ed__33_SetStateMachine_m2F34597B460DEEA622D677CE0C816C2A63589AE3,
	U3CTransferHostU3Ed__34_MoveNext_m0EF821494E6C1BDE589B6762779195861AB9DAA4,
	U3CTransferHostU3Ed__34_SetStateMachine_mCCD291FA1D801B61D8A57C3E1C076A041BA21AD7,
	U3CShutdownNetworkManagerU3Ed__38_MoveNext_m3CFF850A83E2614256D3D33EE12BFD67B9DABA7D,
	U3CShutdownNetworkManagerU3Ed__38_SetStateMachine_m56E3983D843FD13B901ED58E2C2E52689DF3B836,
	U3CU3Ec__DisplayClass42_0__ctor_mFD5B2A1C7CFE02BE8FC8ED10CB9F13B13B1FA074,
	U3CU3Ec__DisplayClass42_0_U3CAnchorLocatedU3Eb__0_mDB02FCD59786E1BCB853C4D57958FDD99606B9F4,
	U3CU3Ec__DisplayClass43_0__ctor_mF009B9C6CCE3A3E787BAFBAC8E9962CC2A26087B,
	U3CU3Ec__DisplayClass43_0_U3CPlaceLocalAnchorU3Eb__0_m176386270C3E4ADEAF792DCEB01A0527EDD55B6C,
	U3CPlaceLocalAnchorU3Ed__43_MoveNext_mE4857E80B20205CE4CADC54540E85D7B17FB5FBE,
	U3CPlaceLocalAnchorU3Ed__43_SetStateMachine_mA8ECAE99CD4D1459DC3423061E32F1BD4482A10E,
	U3CU3Ec__DisplayClass44_0__ctor_m8351ACBD950C7844C7C3E8D73B863CF944EAC29A,
	U3CU3Ec__DisplayClass44_0_U3CPlaceRemoteAnchorU3Eb__0_mBA9F18A7B28E91ED699C1450CA073502B5DCE4DC,
	U3CPlaceRemoteAnchorU3Ed__44_MoveNext_m1E6AAB357B1316C8EE163F5657501D8D16A03862,
	U3CPlaceRemoteAnchorU3Ed__44_SetStateMachine_m1CC20A722C021B1ACE4A330CA4A77EC73B5F4071,
	U3CCreateRelayU3Ed__45_MoveNext_mB5970CAAA03FD31E5EAD2512DDCC334A8601B66D,
	U3CCreateRelayU3Ed__45_SetStateMachine_m258A3525393C6ED7F1FC4541E3B98018658517D4,
	U3CJoinRelayU3Ed__46_MoveNext_m89BC4291D60E5009914A2969424C0919BA6A5AB4,
	U3CJoinRelayU3Ed__46_SetStateMachine_m40C03F5C232047EBC7397C2CE181CF6C8F794458,
	LobbyConfiguration__ctor_m43F7688305E45E3137EAA40FD6B19DAD7EE61C09,
	LobbyConfiguration__ctor_m69F0D77EFA68BF87DED2CC0E2DBEA4BEDA49B868,
	LobbyConfiguration__ctor_mD3B41416AA2EA4065F11E6A6E03EB41596BAFC6B,
	LobbyInfo_get_currentParticipantCount_m7B6EB91D24BD2B4802AAC578CE8A46E2904A7793,
	LobbyInfo__ctor_mAE6536178812C5A7943257B815CB480371F3EE21,
	LobbyInfo__ctor_m54D85F5829123B20FCE81D8D56BB4297202C1C8D,
	LobbyDiscovery_get_RemoteLobbies_mB27D8B3A5B6E53DD14E88DF3F1F1C9F9A1C50FBD,
	LobbyDiscovery_Update_mEABEBFCB69353C571A3F71161A2267E0F467EB9F,
	LobbyDiscovery_HandleRefreshLobbyList_m8DD3230A0892090D4661C14DAC998B671A643732,
	LobbyDiscovery_RefreshLobbyList_m7C56BCA481FA256B69D6DD79F4BCB9C5972DE7BE,
	LobbyDiscovery_StartSearching_mE6FE20548623554A0965B92DC18D87FA1366C7CA,
	LobbyDiscovery_StopSearching_m81CA8162A26DD29E5D9367C16A400EDE7D6C1CCD,
	LobbyDiscovery__ctor_m1B9AB1120F9E1610853BBD941F40E0897F6E542C,
	U3CRefreshLobbyListU3Ed__9_MoveNext_m25444895F8ECC7ABEF7D86B5EE6C0714CDDDA59D,
	U3CRefreshLobbyListU3Ed__9_SetStateMachine_mACA4060E704D5DF0652FF515F3C6076290027169,
	U3CStartSearchingU3Ed__10_MoveNext_m1BC4133C328BB76EBDA027DB0286198AC8BB3865,
	U3CStartSearchingU3Ed__10_SetStateMachine_mFD2AF4BC5CC765B4DD70B09BA164991BF0E4813E,
	U3CStopSearchingU3Ed__11_MoveNext_m94A73BD34E875E523A3106B3A35850EF49931757,
	U3CStopSearchingU3Ed__11_SetStateMachine_m2BB6F3CBE7BB647554A07CBA9B7471EA9186C8CA,
	LobbyListManager_get_LocalLobbies_mB9469211838751A138915982DD58FC39922EE8C2,
	LobbyListManager_get_RemoteLobbies_m2323FFB98BA8EA23AE709C56F5347FCEBF137FAE,
	LobbyListManager_add_LocalLobbyAdded_m02469213AA76BFAB5240A75AD378EC36BB610130,
	LobbyListManager_remove_LocalLobbyAdded_mAB666CD7E510A1BA6059BC7785AE0EB546DC061D,
	LobbyListManager_add_LocalLobbyRemoved_m4CE8BC60A048B7F48D8BEAA2EA6589DEE05AEEA1,
	LobbyListManager_remove_LocalLobbyRemoved_m03FB878E18924F0B0D24B9DC92E29AD9696B2306,
	LobbyListManager_add_LocalLobbyUpdated_m44EB684401AD4F80ABC7596F9DB0C09C16BAF9D3,
	LobbyListManager_remove_LocalLobbyUpdated_m05FF38C0E2827EF8999A143D24A1360B6B3CE24D,
	LobbyListManager_add_RemoteLobbyAdded_m5B247CEBDCA8176A5962C5CDC8369FDD11305038,
	LobbyListManager_remove_RemoteLobbyAdded_m773CE5894EBA6F4E6F8B4505A165787A45D0FDF3,
	LobbyListManager_add_RemoteLobbyRemoved_mA5AEC824110F9C2946BFE5921488D7DD3FE402F7,
	LobbyListManager_remove_RemoteLobbyRemoved_m57522F54BE792AC1B76FC12982E0C8DA4C7F579D,
	LobbyListManager_add_RemoteLobbyUpdated_mBC15B68780E5849CB6AED1DE1191258EDEB20CE5,
	LobbyListManager_remove_RemoteLobbyUpdated_m872BF35DAFC3D4CA54411435EF0401DBB52A82CA,
	LobbyListManager_Start_m1200238E56A483492F46770B1ABF15447A6AD9A4,
	LobbyListManager_StartLocalLobbySearch_m9EEA20B6E9378326DE92AA4A0B6A1C2F74B4796C,
	LobbyListManager_StartRemoteLobbySearch_mBB781225B8640925B72525FEA65C09E5E91F894A,
	LobbyListManager_OnEnable_m59C1CA96EABE366CCF7FC26175C7DB1C5D588AA6,
	LobbyListManager_OnDisable_m70942B3C6435A2AFBE4C641112A0CB067D4BAAD0,
	LobbyListManager_RemoveAllLobbies_m86EB4FB306368D74E4FD35FE43A4F82BA7D1A83A,
	LobbyListManager_UpdateRemoteLobbies_m6F2B726A97635EDD04B08A3457C880D0386D0826,
	LobbyListManager_AddOrUpdateLocalLobby_m5B4155CDE6EE9329F2BDC0568E50C14F0FE27D4D,
	LobbyListManager_RemoveLocalLobby_m9C85FB21E0713E0E821FAAEDCB3B6C0722DFBA95,
	LobbyListManager__ctor_m04CA6AF124044DC0FD9DCFCFC5B09CE568EB6C5B,
	LobbyManager_get_Instance_m67B14E9BC9AE2C192CEB05F7CCACF44A351D0A4C,
	LobbyManager_set_Instance_m3423E6546EF52A57B2E855196DE88DDA85224C16,
	LobbyManager_Awake_m55E6DF51682162CFF126F53F17428BDA50BB1734,
	LobbyManager_Update_m2EDDF5EAAD4D4C87A19CF970492F05044168A99C,
	LobbyManager_HandleLobbyHeartbeat_mEB88C36CAFC16C7E034C642187C545035957CA3A,
	LobbyManager_HandleLobbyPolling_m451863141B4D930ABACEF5BBF82DCF79ACC27B36,
	LobbyManager_IsPlayerInLobby_mD65F8792B7455B02864616AC569BBAF235BCB192,
	LobbyManager_get_JoinedLobby_m50AB58743CC6D185A02B84741257F6FA918129CC,
	LobbyManager_IsLobbyHost_m5CF768BAB88F769DBCBBAC5069610F725B949617,
	LobbyManager_GetPlayer_m00825770FA1DCA8AA39FB605F1F734181F7E75E6,
	LobbyManager_UpdatePlayerName_mBF1471DF3B589358B0DCD5821CA880B46F2C00FD,
	LobbyManager_CreateLobby_m31BCF6482AB561BA784A2F6239B7176D539E1199,
	LobbyManager_JoinLobbyByCode_m8E911B83D6ECFD3980BCB064AE8E2576B30E0775,
	LobbyManager_JoinLobby_mC60BCB3F5109631B879A092BCD68AC60AE1E3184,
	LobbyManager_LeaveLobby_m5EB7FB963A346DD0F49EB123D9D0A52DE46A1AFF,
	LobbyManager_KickPlayer_m87D27B7251D72C637C550750C58D895F15BDE2DD,
	LobbyManager_DestroyLobby_m76A0DB6357794347ED2448587E1669AC8C368519,
	LobbyManager_TransferLobbyHost_mA521E76A63B6F0EF92A5B64746511F04789EE897,
	LobbyManager__ctor_m1A4D2FDD6CF3611918EF889457B0D4ABA7384C4B,
	U3CHandleLobbyHeartbeatU3Ed__16_MoveNext_mABB2068123612A856345C26D9F29469C08FB7650,
	U3CHandleLobbyHeartbeatU3Ed__16_SetStateMachine_mE54FAB3A73B11C01A066A9E101317C15B252344D,
	U3CHandleLobbyPollingU3Ed__17_MoveNext_mF41C7C2B380BEADF3FFC3FDB0F29F927AEC162AE,
	U3CHandleLobbyPollingU3Ed__17_SetStateMachine_m85B92A652AAA0C42BC81732DE2208C91AB2837BE,
	U3CUpdatePlayerNameU3Ed__23_MoveNext_mB6A64351A0BECD3340DCF230846172D90C0B87DA,
	U3CUpdatePlayerNameU3Ed__23_SetStateMachine_m46E12BFD3917F99E43B9CE9627E19EB3B33D0FFC,
	U3CCreateLobbyU3Ed__24_MoveNext_mEBAEC77D233648DC14816A0220460FBC34FA4E4F,
	U3CCreateLobbyU3Ed__24_SetStateMachine_m028997C9F1E7FBA40ADCDE13D7EA17355CEADD6D,
	U3CJoinLobbyByCodeU3Ed__25_MoveNext_m6F9BAE91C9FE08A92E3AD62D2FD2AB27583B3372,
	U3CJoinLobbyByCodeU3Ed__25_SetStateMachine_mB5FD19586A74F53A9ED43337FA2256DCE8412ACC,
	U3CJoinLobbyU3Ed__26_MoveNext_m9F9C11809B250A10242DEE4F2A028B96DE79410E,
	U3CJoinLobbyU3Ed__26_SetStateMachine_m9DE0AD576552F531E92DCB603535B298396E931D,
	U3CLeaveLobbyU3Ed__27_MoveNext_m8A73C2A8A0C45CD7156A42D1CAE243AB5D79AD78,
	U3CLeaveLobbyU3Ed__27_SetStateMachine_mF80F0204D2765208BC35273C8D4E2A17E6A7D353,
	U3CKickPlayerU3Ed__28_MoveNext_m9872BE9C0490DE8C8A946F27CC0FEBA09C94E50F,
	U3CKickPlayerU3Ed__28_SetStateMachine_m61E87687F064F4D623EEE7A77240E9DF349D6503,
	U3CDestroyLobbyU3Ed__29_MoveNext_m34FDFD43B2A080EC6EE88636C9D733CB5C81FAB2,
	U3CDestroyLobbyU3Ed__29_SetStateMachine_m3D8DB8CDACA3D15D72B442DD617A13DFE56738BF,
	U3CTransferLobbyHostU3Ed__30_MoveNext_m4FC6D9D4AA9383060CA5DDB317E3E1E686DA8877,
	U3CTransferLobbyHostU3Ed__30_SetStateMachine_m9B00D73A248FE24B300127054BC90CC69CBE10CE,
	LocalLobby_get_participantCount_m11F6F03A1A434513FA044957AFF3775D5D428A77,
	LocalLobby__ctor_mD89412120EC9014E50088BA840D416C464727CA8,
	LocalLobby__ctor_mFC023660723363DA3BD932F28D6C75EEB95A5566,
	LocalLobby__ctor_m276E0B5D348ACC467B55CE8BB2679BAEABA080D4,
	LocalLobby_get_endpoint_m7F7D1ECE978ED88E4764CB934E95EFAACD0919EC,
	LocalLobby_AddPlayer_m83E6EF9DAE22890A0A135DC6D9F2A798B7455586,
	LocalLobbyDiscovery_get_IsAdvertising_mDCC931CCC6798A7D7FF0B95956B3FFF264795218,
	LocalLobbyDiscovery_get_IsSearching_mA43F672E4891435A585150B34714E64720020CDA,
	LocalLobbyDiscovery_add_ServerFoundCallback_mA73150E8CE95383A28D624C75221066E0FDD00D3,
	LocalLobbyDiscovery_remove_ServerFoundCallback_m703B00475FDEDD8102F0398B5E4FEF33B25B4523,
	LocalLobbyDiscovery_OnDisable_mF2534D5AE8D8D4013E99F6E4CF5841B9FF457089,
	LocalLobbyDiscovery_OnDestroy_m25E85F205AE69B322CC2BCD3C0C2730E770F1B99,
	LocalLobbyDiscovery_OnApplicationQuit_m602708B602B670542F00A28184124C8D9E6C8B6C,
	LocalLobbyDiscovery_StartAdvertisingServer_m6BF503D244E3F5EAAB6E41FE65045374E8E2AA90,
	LocalLobbyDiscovery_StopAdvertisingServer_m226D3BFD60384FA595D70710D8D368B36CF9BCE5,
	LocalLobbyDiscovery_AdvertiseServerAsync_mDFC23F6187F0CF9BD770C1DF878E6C1BCECC9A49,
	LocalLobbyDiscovery_StartSearchingForServers_m80F7A7B8F2D57C3A2D65A3C5025E838246908D90,
	LocalLobbyDiscovery_StopSearchingForServers_m52591539302BC3C18E5639F5F7EFEBDCC0C76CB9,
	LocalLobbyDiscovery_SearchForServersAsync_mAD893E6C6065C283446364384D716E90F30E43F3,
	LocalLobbyDiscovery__ctor_mE64DA509F0ABE56C4F444EBE10042BB44ED77D11,
	LocalLobbyDiscovery_U3CStartAdvertisingServerU3Eb__20_0_mB7676E2EBA8ED3E003DC5CC06CF9C94A8C883AE0,
	U3CAdvertiseServerAsyncU3Ed__22_MoveNext_mA4953319F3B567A79ECB0EBB397A59A28D2D7A12,
	U3CAdvertiseServerAsyncU3Ed__22_SetStateMachine_m90936D6BD07428A4D6C7B052D5E26E806DA0EE2A,
	U3CU3Ec__DisplayClass25_0__ctor_mABE9AE499F607DD1A5B2EC81389A3B3BA1F0CC3A,
	U3CU3Ec__DisplayClass25_0_U3CSearchForServersAsyncU3Eb__0_mBCE40A67CCC918E29D47E4B8DC8C4D57220C5F3B,
	U3CSearchForServersAsyncU3Ed__25_MoveNext_m285A8B5B876066DAF7FAD3F800DFEB229B8BFAED,
	U3CSearchForServersAsyncU3Ed__25_SetStateMachine_m515B2595011C1B82C34D358DF3C3D201E5B31F7A,
	LocalLobbyManager_get_Instance_m190F052099D7FCDDB3ACFE52094B13D127A443B5,
	LocalLobbyManager_set_Instance_mE4B277B7D220EB55DA6389822718C1C1ABBBDC58,
	LocalLobbyManager_get_Lobbies_mF07D8DD7B2C15A2EF73C3F97586E867F400929CB,
	LocalLobbyManager_get_ServerPort_m3EAC3B1D7CF263C45EAEC883B6710A2188D1B5BB,
	LocalLobbyManager_get_ClientPort_m2D5F2E0908C84C6F3A97C38E73701C4D935808AE,
	LocalLobbyManager_get_IsHost_mC0C324086AA62F97BA712A283A1FF57692FA998B,
	LocalLobbyManager_get_JoinedLobby_mF424C48D9E5DED94CF2461722D034BF745D3F5A8,
	LocalLobbyManager_GetPlayer_m65FD3348B312F658A1F70E61D73544750A2AB5A6,
	LocalLobbyManager_Awake_m84C8039A1C6073AD11E7F66F964D84F7D688DB73,
	LocalLobbyManager_OnDisable_mEAC7039DF1FE20E68193852277E5A6CF38FCBB0C,
	LocalLobbyManager_OnDestroy_m752E0C352650267909E3DFC781E57A785BB0FA3B,
	LocalLobbyManager_OnApplicationQuit_m37E896E4D5CCC9713B461E012FC8C0B41715F42F,
	LocalLobbyManager_Update_m146CF0A9575815578D92B4A87986DF4A37B450F2,
	LocalLobbyManager_HandleAdvertisedLobbyHeartbeats_mCA695C1DE4DE3B926DA17BB3A80052606333E902,
	LocalLobbyManager_HandleJoinedLobbyHeartbeats_m5421A0422BEDC4AD427DE29BF476350230CD15F2,
	LocalLobbyManager_StartClient_m8F18A224218CFB2C59AF382CD34E9EB165E13356,
	LocalLobbyManager_StopClient_m7BAFE744411202A92F00392C9261FC0610841BA4,
	LocalLobbyManager_StartServer_m52FC8461CF8497F94675BFFC80C5AE4217A8795D,
	LocalLobbyManager_StopServer_m38888F44BADE905D4797FF98B17CFF7FC401D5BB,
	LocalLobbyManager_StartListeningClient_m87BECF9579313309365DE8BA0D45FEBB1118F1C1,
	LocalLobbyManager_StartListeningClientAsync_mFF36434DC24B293B709133134895F1EEC5DF0719,
	LocalLobbyManager_StopListeningClient_mF8BEEB5F6913C479A5E29A0D9982EB206E410065,
	LocalLobbyManager_StartListeningServerAsync_mE02DF11F5A4C3F65476F1115BAAC7B6DFCB49E7E,
	LocalLobbyManager_StopListeningServer_m13F814367D4D7E0D7C9982ECCC6ECE0BF7130915,
	LocalLobbyManager_StartSearchingClient_m49BC52E6FE2B55D0759F53A4A228B5F1FB98356D,
	LocalLobbyManager_StartSearchingClientAsync_m0A4D577970CB997FB774DA7BE11CC63617BB8E01,
	LocalLobbyManager_StopSearchingClient_m7D3BC20837947A43283E95403422384F34541D52,
	LocalLobbyManager_SendClientMessageAsync_m4B49769F5A791B4DE4B56CA69D69422DB1C19333,
	LocalLobbyManager_SendServerMessageAsync_mA92B5A2E7DA50D6F32AC122CD8BBDE6F07802852,
	LocalLobbyManager_get_clientIsSearching_mBABA1E8E6C3EA277137AB7DD8377BEB6D3F88A97,
	LocalLobbyManager_get_clientIsListening_m03C27DCE09D0ACE42784DDC9F41514FABD4D45A6,
	LocalLobbyManager_get_serverIsListening_mA23F6BA5BBE14F66E6D6E823C056021A0B837BF0,
	LocalLobbyManager_SenderIsHost_mB593252651ECDFFF9B9C0977513039E53DE2C296,
	LocalLobbyManager_SenderIsMember_mE4198D7029842521D5349F1516A99288C7A68D5D,
	LocalLobbyManager_HandleMessage_m1FD6E508519CD7F99153ABD83FF41CB057CCADC7,
	LocalLobbyManager_HandleClientHeartbeat_mFA6CA39C7231E6E0A90AECEFE12D042FA4375734,
	LocalLobbyManager_HandleServerHeartbeat_m956B3706AF80ECFB7F928829BD4079DB391A0C4F,
	LocalLobbyManager_HandleLobbyAdvertisement_mE4FA0C9A63183B284628D9E549405C6D3CA76479,
	LocalLobbyManager_HandleLobbySearchRequest_m95C75CC8173261E63EE0F86C42BF664C41F04923,
	LocalLobbyManager_HandleLobbyUpdate_m0F5B994FB7D0181EB51F84B1ECC38E863099B27F,
	LocalLobbyManager_HandleLobbyDestroyed_mC87CAF350596BE9CB0A20AC6F68BC390BE147CBF,
	LocalLobbyManager_HandleLobbyJoinRequest_m353901E5BB150435C35DFADA02148BFA0FCEE0A9,
	LocalLobbyManager_HandleLobbyJoinResponse_mFEEB1498E07CEA861F5F523B8DA95B7A6A61A084,
	LocalLobbyManager_HandleLobbyLeaveRequest_m423180F0F928AF541E4409E6925B84F2BD19DC47,
	LocalLobbyManager_HandlePlayerUpdate_m7AF0062AE676D06075B87C1610EC55D4A48F2512,
	LocalLobbyManager_HandlePlayerKick_m56B0F460E36FEAE3041CCF95670645D1F30C0DCF,
	LocalLobbyManager_SendLobbyUpdate_m03579034EAF81A4E4F2E0BF8B04A13982C7CCEE9,
	LocalLobbyManager_SendPlayerUpdate_mCE5D548107675E3CDD79150EF28E3570BE23CE42,
	LocalLobbyManager_SendLobbySearchRequest_m08AF7DA76746217296DA4FC86EC272E16367EFE1,
	LocalLobbyManager_SendClientHeartbeat_m0B7B6F5833BE8C80AD08CBFF6336F58462FCC8D1,
	LocalLobbyManager_CreateLobby_mD8B9D38B75B3D95AFD4ADFC9C915B75F134846EA,
	LocalLobbyManager_JoinLobbyByCode_m7EF0F8DEE0504C6E245B7F777E9E371774F4B43D,
	LocalLobbyManager_JoinLobby_m06EEDFD02588DD484EAB75CE0B029AA25B115DE8,
	LocalLobbyManager_LeaveLobby_mBC0F1D7B7A203D7B7B5C5FC733895E5C412032C8,
	LocalLobbyManager_DestroyLobby_mBC8DA820B5E2520F16E98FBF767062B1246A1B6A,
	LocalLobbyManager_KickPlayer_m9A5808C3B4EE193651EAAEEFBD6B1D77E3FDC036,
	LocalLobbyManager_TransferLobbyHost_m8B7DDED2BB097E8D2591156563BEB7558430A328,
	LocalLobbyManager_AddLobby_m43A114B909A9EBAF16B402AE9B822C4E9F5EE149,
	LocalLobbyManager_RemoveLobby_m190DD326CB7284FB12FC9EE0AD1AEF1FA7C4618F,
	LocalLobbyManager_RemoveAllLobbies_mB014E12211527AB6C6E2C5F51E12862385BDA9E1,
	LocalLobbyManager__ctor_m9C8D56DFFF3472043C4E9FA775C647056F47FD66,
	LocalLobbyManager_U3CCreateLobbyU3Eb__84_0_mD53222765B7AE95A190C4D991A8AFDF23B33C701,
	U3CU3Ec__DisplayClass51_0__ctor_m7A1F6DC32DA14E53D0F6E96D8F85C641DD92C444,
	U3CU3Ec__DisplayClass51_0_U3CStartListeningClientAsyncU3Eb__0_mD9D1915D6A03BB622CE7EB5574BE4C0A0E7D224F,
	U3CStartListeningClientAsyncU3Ed__51_MoveNext_mC296F54F2EEFC5AFD81551A534F467ABA06159FF,
	U3CStartListeningClientAsyncU3Ed__51_SetStateMachine_mB595BD4394C387BD7DF0E7208720317C2364E296,
	U3CU3Ec__DisplayClass53_0__ctor_m828EA0457644B9115AF70CA360EC9F9692A7CB3F,
	U3CU3Ec__DisplayClass53_0_U3CStartListeningServerAsyncU3Eb__0_mFDB289338481C9555AD600ACDD739EEE62D34DA7,
	U3CStartListeningServerAsyncU3Ed__53_MoveNext_m343428C0B7362B8E517B9BBAE531CCBC0F2594D0,
	U3CStartListeningServerAsyncU3Ed__53_SetStateMachine_m7F74BCAA5437DF0508E0F095B85F561E9409B75B,
	U3CStartSearchingClientAsyncU3Ed__56_MoveNext_m2D1E6DBE748978539DE360BBB9CA49A37A98C2F9,
	U3CStartSearchingClientAsyncU3Ed__56_SetStateMachine_m67B5E70F7BC655728EDC778350C8176F8BBBC90D,
	U3CSendClientMessageAsyncU3Ed__58_MoveNext_mD66C90618809CF9A70C25959978F5F5E644F3541,
	U3CSendClientMessageAsyncU3Ed__58_SetStateMachine_m6D290276DF0382BD9589625ECA249F2B4348BDAC,
	U3CSendServerMessageAsyncU3Ed__59_MoveNext_m4E42D1CB25FB4F3A0BF6C129E30F630177E67506,
	U3CSendServerMessageAsyncU3Ed__59_SetStateMachine_mA67FCC5226AA7F338976A0BD63E36F10F1FA0F07,
	U3CHandleLobbySearchRequestU3Ed__72_MoveNext_mD43858816A0807484305BF003FACD2D0D32D652E,
	U3CHandleLobbySearchRequestU3Ed__72_SetStateMachine_mD9705204E6BF8C12370E1E44F027CADA0FA79D1F,
	U3CSendLobbyUpdateU3Ed__80_MoveNext_mF7243DBCDB8E10D5DC0DAA519D04A491BA1C0E85,
	U3CSendLobbyUpdateU3Ed__80_SetStateMachine_m398D67B94FA145E40C4795B2EC7A2C13AB5705C1,
	U3CSendPlayerUpdateU3Ed__81_MoveNext_m68B140503BF008CB9C3F5EC3B6777C7014501462,
	U3CSendPlayerUpdateU3Ed__81_SetStateMachine_m0544B8DD9FE6B4FC9C9606CE0F825F435692D87B,
	U3CSendLobbySearchRequestU3Ed__82_MoveNext_m759975C2302973B31ED94F8205E0E2BAE33B7A4D,
	U3CSendLobbySearchRequestU3Ed__82_SetStateMachine_m43B1C2551233FF1177772227F7B0C93ED08A9B83,
	U3CSendClientHeartbeatU3Ed__83_MoveNext_m3E15302EBB44E9F9913E273D79A363ED4B327B3D,
	U3CSendClientHeartbeatU3Ed__83_SetStateMachine_mE3EF863032125870B8B3E4F5996713D2E6359DCD,
	U3CCreateLobbyU3Ed__84_MoveNext_m218701A5A677B7C500C48A0C272794D051B11917,
	U3CCreateLobbyU3Ed__84_SetStateMachine_m6CBBAB0CE132420589F3F5629A380256A1539CDB,
	U3CJoinLobbyByCodeU3Ed__85_MoveNext_mD72A3D5FD08210B803129F88CC3D8C3B45402B48,
	U3CJoinLobbyByCodeU3Ed__85_SetStateMachine_mDA60AE5B340745B217794FF36D61CB7372CE914D,
	U3CJoinLobbyU3Ed__86_MoveNext_m123B4D24C1ED48F8FA612BE2900BD65C330B28AB,
	U3CJoinLobbyU3Ed__86_SetStateMachine_m5B34EFB95E2B8B3467845E4442ECED18733DA047,
	U3CLeaveLobbyU3Ed__87_MoveNext_m04DA6B1D686AD5A7080C27709914228BE95C81A2,
	U3CLeaveLobbyU3Ed__87_SetStateMachine_m1816662CBEACB2A24E946D7CAE3DD1E21EE453D2,
	U3CDestroyLobbyU3Ed__88_MoveNext_m50A01D84130D48C3E13AEC498339E1D45F836CCE,
	U3CDestroyLobbyU3Ed__88_SetStateMachine_m70B496D1E400BEDF360BAD4875BCA85C3B021D96,
	U3CKickPlayerU3Ed__89_MoveNext_mEFF5F9A5414632CAB14C0BD10E301B72E7E8AD04,
	U3CKickPlayerU3Ed__89_SetStateMachine_m873D5625E53C2781F3BD2BC364A6F98F18052AC6,
	U3CTransferLobbyHostU3Ed__90_MoveNext_m2DE99B1A92DD04B6FD9A94111D42BB73CAB042AA,
	U3CTransferLobbyHostU3Ed__90_SetStateMachine_mCCC607DCC0303A0585D3AFC30782A8D52E578EB6,
	U3CU3CCreateLobbyU3Eb__84_0U3Ed_MoveNext_m3091BB36BAD5C9AE3FF475D2D0CF4D801DD2B775,
	U3CU3CCreateLobbyU3Eb__84_0U3Ed_SetStateMachine_mBB98E6072F3F29C271793965A4409115595D468D,
	LocalMessage__ctor_mDDB7FC57ADF1AB8A559D516687D4B073A25F1F99,
	PlayerJoinResponseData__ctor_m36F664E88EAA01C9E369725A837F179A2F366E05,
	LocalPlayer_get_endpoint_m6A80FDDF3C5019DB66AEE83813EA3C1A852387CB,
	LocalPlayer__ctor_mC91FED2C8D3E57084CC8AC278585167E16ABF06F,
	LocalPlayer__ctor_m30D7A32DAF1540AAF2AF366FC4F03F3A1A6C3018,
	PlayerInfo__ctor_mC32222D17D4407F9824227F42DF0F6967FEBB479,
	PlayerInfo__ctor_m4F95D1F2CEDE4C7D26379CC5D17D40DD7BC98192,
	PlayerManager_get_Instance_m6A9319DF9F35EE81B0BB6F69F9CAEBEAF168418B,
	PlayerManager_set_Instance_mEB9E08C3E990502415BF920F221F354EF18704B5,
	PlayerManager_Awake_m4A997ABB547A7B4C3BA447681C1C8718B7B4A5B6,
	PlayerManager_Start_mD4281A9FA4F173B0A41018EA2F11F09281E574BE,
	PlayerManager_GetLocalPlayer_mBB2B10841D02A5CE908B86F49C563ED4D9C010EB,
	PlayerManager_GetRemotePlayer_mB6F11D45130C226041AAB81C7F3DDEF51FF45D19,
	PlayerManager__ctor_mEDEFFACDDE66D7077DE8CDED6560CA06218B5A0E,
	ActiveSessionUI_Start_mE6462F168A10FAFA4E63BE78B503960A413A4327,
	ActiveSessionUI_OnEnable_mF433BE0945DE83D7521F3672E6AFE08F7860A3FE,
	ActiveSessionUI_OnDestroy_mA0D39A7C7C36DAE7B710F4D25E3B90F7ADC89670,
	ActiveSessionUI_OnDisable_m3AF7EBFA6F41B9717EB4E3055340F01631A4714F,
	ActiveSessionUI_UpdateLobbyInfo_m76550D683A283D642CF446311E2B4BB8723C8B1B,
	ActiveSessionUI_ResetUI_mCC39C9DF1D031EA1222CBF0F76F725800C8F7D02,
	ActiveSessionUI_get_SelectedParticipantID_m0430BBFB877DA74B504E1ACE450291EE7190ADF7,
	ActiveSessionUI_set_SelectedParticipantID_m24CDC917FED740497354B0BED020B5D7276A4621,
	ActiveSessionUI_OnClickExitSession_m9A1D4250EA17364E70F3461B89B925379F1C27D8,
	ActiveSessionUI_OnClickKickPlayer_m96F7BAC67C5C0D549BE4C4A967CD79A779DFDD01,
	ActiveSessionUI_AddOrUpdatePlayerButton_m6BB11CB092B3C4DC865AEA190F88AF12A86F657E,
	ActiveSessionUI_OnParticipantLeft_m616CBFE1710D268F904A06A2805B584623130FA0,
	ActiveSessionUI_RemoveParticipantButton_m3BECBB580F9E0C0988981E9A2844E72C8B17BF1C,
	ActiveSessionUI_AddParticipantButton_mE97AB85378DB2D3C14FFE2A7811347B510730586,
	ActiveSessionUI__ctor_m1584C27E4C4A20058A44AA69D39E3DD8A2BF5A52,
	U3CU3Ec__DisplayClass23_0__ctor_mFCA759B62B0DEA71E04EAC186F1D14358B9D9038,
	U3CU3Ec__DisplayClass23_0_U3CAddParticipantButtonU3Eb__0_m699118BA78C6742C4440DADF2957CF4DC70A0A57,
	LobbyButton_SetLobbyData_m245977958FF3FB6390F441DBB5EBB587F830432B,
	LobbyButton_SetLobbyData_mFC7860B206533FF6C938F0CCD4851C6A8AF5B09C,
	LobbyButton__ctor_mC18F9FDEEC010FAAAB1CE25FE3F20E247A0FD643,
	NewSessionDialog_get_EnteredLobbyConfiguration_m59643D81CE7CCB59774237D346CCD95D08C3268B,
	NewSessionDialog__ctor_m3ACAF28D80FB3C6B1299700F30812FC109F3B415,
	SessionListUI_get_ShowLocalLobbies_m2128AE29A06C0E897AA372D6865DD54020D7BD50,
	SessionListUI_set_ShowLocalLobbies_m79372827EBB17435514599110C52A74BFE889FDE,
	SessionListUI_Start_mF09416D52FAE4759105A96D3E3C0A743557AED0E,
	SessionListUI_StartLocalLobbySearch_mB376D8C12981E7E5C98344B047C0465088ACB688,
	SessionListUI_StartRemoteLobbySearch_m0F5C7E7EBC8B77FB3D351C8B44CE25BCD83EF8C6,
	SessionListUI_OnEnable_mA3A2019E62414A0B99A35A535A70C11812DEABEA,
	SessionListUI_OnDisable_m5B572A52E95F5EDEEB90D9083865986F6396BEC4,
	SessionListUI_get_SelectedRemoteSessionID_m0DF28CEEEDF0228D7EDFEF416AA23FA85961978D,
	SessionListUI_set_SelectedRemoteSessionID_m6879640755F3922F5E55F4D0902BD6A119035C09,
	SessionListUI_get_SelectedLocalSessionID_m7BBA9817A9C43BDFBC2E4F85F07022A4155951A1,
	SessionListUI_set_SelectedLocalSessionID_mF597DEAB4761450AD3D47A25BDB680FD66FE8ECA,
	SessionListUI_DestroyAllButtons_mB6A7FA185FDAACA34DA1C86F85F7A5624065DA95,
	SessionListUI_UpdateLocalLobbyButtons_m6A9938D206D60398E39836E3FC6572212942D4FD,
	SessionListUI_UpdateRemoteLobbyButtons_mDA1E9FF10DBD8240F3CC83BAA0003BD6C117DFDC,
	SessionListUI_AddLocalLobbyButton_m009BCDD990E8656CEEA93E08182147FF44D5BF77,
	SessionListUI_RemoveLocalLobbyButton_m30AC0336D31CC3D4FD8C3F90A9547120ECD6A52B,
	SessionListUI_UpdateLocalLobbyButton_m70B5F86CA070681054B6F5EA7EB050A18B1A5DE4,
	SessionListUI_AddRemoteLobbyButton_mC6BB74429E0AB3C7D0544AD65BC3659B3571C89E,
	SessionListUI_RemoveRemoteLobbyButton_m633F425006915F6D61F5F3285A94A11923128A57,
	SessionListUI_UpdateRemoteLobbyButton_m898D52DBA2FBF7E1CD365507B624201578E5DEC8,
	SessionListUI_OnClickNewSession_m5C2A3D572D1AAF3243258647461FDEE3EAF8E83E,
	SessionListUI_OnClickJoinSession_m0F284BE8D2E5CC169E3F6B26237917881C46D66F,
	SessionListUI_OnConnectionTypeToggleSelected_m67D9FD93083A937FD05D6A7E3C74411E641CCDB5,
	SessionListUI__ctor_mD79DC3E5227DCF16DCD57DBC82EF09D55527A8B4,
	U3CU3Ec__DisplayClass33_0__ctor_mE9B8265E221839835DCFDC99232FB6442978E9EE,
	U3CU3Ec__DisplayClass33_0_U3CAddLocalLobbyButtonU3Eb__0_m60D92BDFE96FCA8DAD070CE5A3C659327921EEE6,
	U3CU3Ec__DisplayClass33_0_U3CAddLocalLobbyButtonU3Eb__1_m2FBD6B8C70E7610F1CE4924B6EF000403E1DEC1A,
	U3CU3Ec__DisplayClass36_0__ctor_mE6FF1237D40FE2308ACF1AEAACBB9F5DCA4EA3EA,
	U3CU3Ec__DisplayClass36_0_U3CAddRemoteLobbyButtonU3Eb__0_mDDAD61AAD2F07235681EC7ED0D589E90737E7CAE,
	U3CU3Ec__DisplayClass36_0_U3CAddRemoteLobbyButtonU3Eb__1_m518798B06A807A23EED2AA7DAD78E4093671FE1D,
	U3CU3Ec__DisplayClass39_0__ctor_m44407C6125D25B6160597703796E724A82BC3C94,
	U3CU3Ec__DisplayClass39_0_U3COnClickNewSessionU3Eb__0_m6F71A4B0C372D5EA9297D52136B3B56EB915C092,
	SessionParticipantButton_SetPlayerInfo_mD03ACC7EDBB1581A5D3D24104AACB005E6462B84,
	SessionParticipantButton__ctor_m65D6F449A777EF49924C3D62DB5A94AD04B762ED,
	SharedExperienceUI_Start_mAB4F9A79E691A53D0F02898DA9D3B1D432384797,
	SharedExperienceUI_OnDestroy_m92A32985109D4D7AA7C6BAD81E0CEE262682BB31,
	SharedExperienceUI_OnSessionStarted_mC46A57DE0119F71D742B33674251DFE9C6CD2797,
	SharedExperienceUI_OnSessionEnded_m4CA579A6643C4B53A8E91311C2D8E8929671614A,
	SharedExperienceUI__ctor_mD7EE1FBDCC79B71182B6F616006F172B29E92D14,
	UnityMainThreadDispatcher_Update_mA4581B031CF68811CC6957B72109257C5FB953BA,
	UnityMainThreadDispatcher_Enqueue_m3DC7FEF4D9722834BC491A5C13EF1249BF757480,
	UnityMainThreadDispatcher_Enqueue_m41C8F287DAD216F87A4E3CC547B1D4E4E9760C14,
	UnityMainThreadDispatcher_EnqueueAsync_m0BCC28431763AA0CBF211AA2A9170195F3454788,
	UnityMainThreadDispatcher_ActionWrapper_mA9140D8E54337437EAA9E9E3EC19EEDBD0503EF7,
	UnityMainThreadDispatcher_Exists_mF21EC0F90BAE0E070BAAA14B7691E8711D469C62,
	UnityMainThreadDispatcher_Instance_m0B17E9E95172FC70FF269E3E5EAEC2761BCB7E2A,
	UnityMainThreadDispatcher_Awake_m518E1A8505DD06DAC84D9A11FE8C69E5501307AB,
	UnityMainThreadDispatcher_OnDestroy_mA5CE8D1450187C2F82327C120CC24BDC97B1830E,
	UnityMainThreadDispatcher__ctor_mEE26CFC31BB55E3BBC0BFBF761C2CD9A9CD3E5A1,
	UnityMainThreadDispatcher__cctor_mBFF1612070ABC7A4DC5E10504BC6267387A1C1BF,
	U3CU3Ec__DisplayClass2_0__ctor_m8A970E6582E1302186F63B9E37705398F45BA973,
	U3CU3Ec__DisplayClass2_0_U3CEnqueueU3Eb__0_m9DCF63AE3D8BF2E9C9874C84E2D644E0193132CC,
	U3CU3Ec__DisplayClass4_0__ctor_m70225B853DFA571BD40B2C66E4C5891332F22581,
	U3CU3Ec__DisplayClass4_0_U3CEnqueueAsyncU3Eg__WrappedActionU7C0_mF22148EFA0776594EC9B22FE8399FC0AC64D4F2C,
	U3CActionWrapperU3Ed__5__ctor_m9BDFC74813CC5245F648664F8DF0E750728D2DAB,
	U3CActionWrapperU3Ed__5_System_IDisposable_Dispose_m6DFCB0B4116846E2C9857DAC58A7BE5E74D58FDB,
	U3CActionWrapperU3Ed__5_MoveNext_m539BDACB125A526C06DE11B32E9E3A694E95BECA,
	U3CActionWrapperU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7AE1A8ED6FA260A9DF0C9CC5C3E9E162DAD64BA,
	U3CActionWrapperU3Ed__5_System_Collections_IEnumerator_Reset_m2E3C55B7DF0FAE95862C7A9C369E177D02706EFD,
	U3CActionWrapperU3Ed__5_System_Collections_IEnumerator_get_Current_m6D440EABF1C1B20808A448DEB30CD4826B28723B,
	ButtonWithText_get_Text_m7E91C654C173DE6B8770A20F0C39C6CE79A9876E,
	ButtonWithText_set_Text_m6F27B96CECCF830CD0454D17BF4E1B3B4AD6CEBE,
	ButtonWithText__ctor_m0D19225559805A6B111EE422B4B691829605C8B5,
	CustomDialog_get_OnDismissed_m5C2702D3E859A2AC7C1B071F576EF94A4B8C60F1,
	CustomDialog_set_OnDismissed_mE7C544C03E66B0A2443EB0E0994B808ACC667BA4,
	CustomDialog_Reset_m1745F36743F9B6C9FAAEEED870E15989E08A009C,
	CustomDialog_Awake_mFEA2048DE0B8BB82AE22D0B0CFC891CDD566C539,
	CustomDialog_Show_mFB673DA2C340ACAE9EF64E2206672E6619225837,
	CustomDialog_Dismiss_mB29B1DF1A5A90742388ADE57D29D0E0DB12F3DA6,
	CustomDialog_DestroyAfterAnimation_mA0AD22A995BED4B7A752C9D94D1A2E43C2BB2E39,
	CustomDialog__ctor_mB02A1EDF9A17AAA841172E537C364940FB247566,
	CustomDialog_U3CAwakeU3Eb__12_0_m8345EAB962AAE8EAD455204F379874645914F8B2,
	CustomDialog_U3CAwakeU3Eb__12_1_m973EBA0B955FBF92A194E18AC2662209A7BB9B76,
	CustomDialog_U3CAwakeU3Eb__12_2_mEBA8782B31F697D2A49110D20210E7415275059F,
	U3CDestroyAfterAnimationU3Ed__15__ctor_mD2283751092670C0A22A7A99E4C70DB1EE3DF3C5,
	U3CDestroyAfterAnimationU3Ed__15_System_IDisposable_Dispose_m5306FA8F42A83E8565D9C4F47120DC28C1481340,
	U3CDestroyAfterAnimationU3Ed__15_MoveNext_m3477B972A5F6A01686D81F5D9E7A34598A7A3772,
	U3CDestroyAfterAnimationU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDA7F4DE4A0C7618858A100F4B79C7302116021C9,
	U3CDestroyAfterAnimationU3Ed__15_System_Collections_IEnumerator_Reset_m85686D2DC4AC9CF2BF932958CBC73970125F8DC9,
	U3CDestroyAfterAnimationU3Ed__15_System_Collections_IEnumerator_get_Current_m83722BCB8A601C1F0F0204AA53A578920BD7F9FE,
	CustomDialogManager_get_DialogPrefab_m80014EA8741E28FC9B505885162B7BDDD0372C54,
	CustomDialogManager_set_DialogPrefab_m2962E9F071493B9191B3A26008B37EC6ED0D1AB9,
	CustomDialogManager_SpawnDialog_m4977B6F4B1338355E95E6944915B2BEC22124F0F,
	CustomDialogManager_OnDialogDismissed_mCAEAF70EED3619951B6FA3EDEE3047992EBFEAF8,
	CustomDialogManager__ctor_m1F41AC17A377D4CE5CF7F751427D41A22129325C,
	LoadingIndicator_get_LoadingText_m6B67F46055FC6EC65B00AC2FA27830D0F20CC46A,
	LoadingIndicator_set_LoadingText_m06A7FB0939FBA07E0935F8769DA1104338D37ECA,
	LoadingIndicator_Update_mDC96632C85E29109FBD8B0EE34D8587AF1D521F9,
	LoadingIndicator__ctor_mDE8A60DBC8694C3DDCAF03D0E62CBEF9CA956748,
	Tooltip_Start_m4AB8523D7CBBB7E82F57487B92149DA47D3F6C82,
	Tooltip_get_Caption_m915D134C9A5D83C22CDFCF25F4BA069563396C6F,
	Tooltip_set_Caption_m259D64C96BEC060427008FB6E07605E783856D48,
	Tooltip_get_TargetObject_m8B7E7C39C41D59AFCE338D66973136A0ABED5D47,
	Tooltip_set_TargetObject_mB251443FF03E012D232960ED25312789CE58E0E0,
	Tooltip_get_TargetCollider_mC0118A76F610FB266A4B470F80B9ABD07EBB6D6D,
	Tooltip_OnEnable_m7EC9D5AE25908D9F63DC334C9DEFE47924733739,
	Tooltip_Update_m7369D6BF08179D8BAC3C69E974A006668D91A3E2,
	Tooltip__ctor_m839D8DEE46301BEAA0E8102131A4E7A597860837,
	NULL,
	NULL,
	NULL,
	VirtualizedScrollRectDropdown_get_Scroll_m0EDA6499312232E62ED0FEBD6869DCD097121BE2,
	VirtualizedScrollRectDropdown_set_Scroll_m28B7F227B3BB1BF9B355CBB62A9EE5B899796E72,
	VirtualizedScrollRectDropdown_get_NumNodes_m89452939479227C2054143D101FE08DE0D893894,
	VirtualizedScrollRectDropdown_get_NumExpandedNodes_m9AB3E4E24C7B4D5E318718796978EB0040B890F1,
	VirtualizedScrollRectDropdown_get_PartiallyVisibleCount_m15CAFBC9EF70778D1011AA4ADF094ECE6182D101,
	VirtualizedScrollRectDropdown_get_TotallyVisibleCount_m9915D15AC7EB613C74069E8D4FE20732ED71A51B,
	VirtualizedScrollRectDropdown_get_ScreenItemCountF_m879F9A52A6889130719C531A2E5E865A12237A3A,
	VirtualizedScrollRectDropdown_get_MaxScroll_mFE4F3BF50B2BA8E7F9E817DB40AE7EFCB9536FA6,
	VirtualizedScrollRectDropdown_get_OnAssignedNode_mE666EAE3FB5884D758864BB2443652520A6D4373,
	VirtualizedScrollRectDropdown_set_OnAssignedNode_m4D91EA386A72AC501969C29EB9AC0B25D7A79C4C,
	VirtualizedScrollRectDropdown_get_OnUnassignedNode_m0042C5648AA43102AD358268C6C1B581258306AA,
	VirtualizedScrollRectDropdown_set_OnUnassignedNode_m179D28701D75137F915B4911F275DC560C343D2A,
	VirtualizedScrollRectDropdown_listPositionToDropdownNodeID_mB8FEA785BE60DEFB38F4F34692093300F353CBC7,
	VirtualizedScrollRectDropdown_dropdownNodeIDToListPosition_m7C5E76D59E05E7B717732FA843400703A9038F27,
	VirtualizedScrollRectDropdown_GetNode_m83E48D3AF78FD30D1D7B45DEE976A100687B422D,
	VirtualizedScrollRectDropdown_ItemLocation_m8CABE1610825C3F0955B6D706F5FF9F61921818D,
	VirtualizedScrollRectDropdown_PosToScroll_m074A78D61B4CA81498507E1A5C0DDB6FA0056637,
	VirtualizedScrollRectDropdown_ScrollToPos_m5CCF23AA723C1A9803C1DF9D6708BA35E1A2D7DF,
	VirtualizedScrollRectDropdown_OnValidate_mCEB90873B16527133A0C0686251789E86F78C821,
	VirtualizedScrollRectDropdown_Start_mC5536132E5EBC4A4A9C233EC5CBA8D0AD693EFC4,
	VirtualizedScrollRectDropdown_EndOfFrameInitialize_mFC6F555256EF7D188CD9A7BD94BB2B66E7FC5326,
	VirtualizedScrollRectDropdown_BakeCachedValues_m898462DB0D1C59FDE3F792DDF8AC12CD4686196E,
	VirtualizedScrollRectDropdown_Initialize_m648D209FE743ADEFA7874A7CC1AECCFEAA450216,
	VirtualizedScrollRectDropdown_InitializePool_mC9CDDEDFA34D953D83A5CB6A214C5276EC92D3D4,
	VirtualizedScrollRectDropdown_UpdateScrollView_m9A3C90BF2D592FC4F9CE01B58BE85B6623C27F64,
	VirtualizedScrollRectDropdown_UpdateScroll_mEAF1C48560E1EA0054D3D63E5B6B7C0109AB3E24,
	VirtualizedScrollRectDropdown_UpdateVisibleNodes_mDF25364E95139478E0DD1E179AC58D87661AB55D,
	VirtualizedScrollRectDropdown_MakeInvisible_m343F25C882433EB29D5E8F983D7B69D0B3CE0042,
	VirtualizedScrollRectDropdown_MakeVisible_m67D6C0B88C75D3908E693B4D9137A12E9234D510,
	VirtualizedScrollRectDropdown_ReassignNode_m4B700DEBFB811E975862543610F39C7FE474802F,
	VirtualizedScrollRectDropdown_UpdateNumNodes_m1F2E759D190739A1B5EAE1DBF3EE05E5BD448C8F,
	VirtualizedScrollRectDropdown_TryGetVisible_mB3EE7699315018871D95148B86ECEA91F2EDEC4F,
	VirtualizedScrollRectDropdown_TryGetVisibleFromID_m52548A461055F9BBC7EC17BDF59D8494432C5153,
	VirtualizedScrollRectDropdown_AddNode_m6191B32D49D86B283C32ED1A7A00C5841E11286D,
	VirtualizedScrollRectDropdown_RemoveNode_mD61094D8B0E8E23CE6F39F245CDBABA2CF7E1325,
	VirtualizedScrollRectDropdown_SetNodeExpanded_mDDEF7271F8811D9E055A75F6D2BBA7F993F2F0F2,
	VirtualizedScrollRectDropdown_ToggleNodeExpanded_m9394EDFE0B1BDE4F463586768CB368C376187EEC,
	VirtualizedScrollRectDropdown__ctor_m10F7279FBB2C58F96FCAC67E2BF21C84C4A950A8,
	VirtualizedScrollRectDropdown_U3CInitializeU3Eb__56_0_m3796DA571D11323CA1E8A21108A1EE77C163E4B4,
	DropdownNode_get_isRoot_m200E3BF7F28A8A6BB3FD29F9746FA82A378E60D3,
	DropdownNode_get_isLeaf_m017C045AB5B419DAE15B656E0A75569E0717A900,
	DropdownNode_get_Level_m6BEF8585A0BD48702D851FA2D19EBF45CF1C4CA7,
	DropdownNode_TraverseTree_m62513DD47C7378252253C074307FD4B9A6324805,
	DropdownNode_TraverseExpandedTree_m1B9AF0357A68DF72DC0275BE3968A2959BA1D361,
	DropdownNode_Size_m9BCA339F8FE44107C189300B80E032DB3B78C127,
	DropdownNode_ExpandedSize_m8B4AA83C833A37843C7BA7A5F2F8DD5639172DD1,
	DropdownNode__ctor_mAE23DCD81168AC6CA9EDF1AF64E3D8915A3CFAC8,
	U3CTraverseTreeU3Ed__10__ctor_mF67E4A40887D006B397E7316DB1E9D11E5FAE3C9,
	U3CTraverseTreeU3Ed__10_System_IDisposable_Dispose_mF39A701F2613F5BA2BCFDFC7CFED9D7D20171501,
	U3CTraverseTreeU3Ed__10_MoveNext_m3D8D90118261EAB1E14D72938765183589A50307,
	U3CTraverseTreeU3Ed__10_U3CU3Em__Finally1_m71734ACE7138881A27D9CA09B9FEA88E51BCE9A1,
	U3CTraverseTreeU3Ed__10_U3CU3Em__Finally2_m89C00DF0B65E208EAC012304034056E903637216,
	U3CTraverseTreeU3Ed__10_System_Collections_Generic_IEnumeratorU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_get_Current_m8F34545F2F847C395EE7F3284502C09B78190529,
	U3CTraverseTreeU3Ed__10_System_Collections_IEnumerator_Reset_mD68F98166DE34F035088C9B2269D467C73B27D07,
	U3CTraverseTreeU3Ed__10_System_Collections_IEnumerator_get_Current_m8E192FB0D0B9C8BFD267AB977EE73CD9ADF6CAA3,
	U3CTraverseTreeU3Ed__10_System_Collections_Generic_IEnumerableU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_GetEnumerator_m0A128FC02F2E97F9568F54AEE20C52A14E3619D7,
	U3CTraverseTreeU3Ed__10_System_Collections_IEnumerable_GetEnumerator_mA419D41D79432CA8560C858616CF45CCE9246ECE,
	U3CTraverseExpandedTreeU3Ed__11__ctor_m26B71E23ABBCD4EC693F495B9BE031005FFF6494,
	U3CTraverseExpandedTreeU3Ed__11_System_IDisposable_Dispose_m58213CE5125123D080C4A8FB591A02176DEA769F,
	U3CTraverseExpandedTreeU3Ed__11_MoveNext_m64873EDDC334029D8B4D65CB00DD5CF899DE34F9,
	U3CTraverseExpandedTreeU3Ed__11_U3CU3Em__Finally1_m065CA0B97F9478817448FFBE05881A3F99CB95F9,
	U3CTraverseExpandedTreeU3Ed__11_U3CU3Em__Finally2_mB6F15E0734678D2608A1F8B43174AB603BBE8A8B,
	U3CTraverseExpandedTreeU3Ed__11_System_Collections_Generic_IEnumeratorU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_get_Current_mA1D521CBA0E97D8EDB12ECE0E3A3BF4EA8D48BD8,
	U3CTraverseExpandedTreeU3Ed__11_System_Collections_IEnumerator_Reset_m56B0F7F7C55D5FEFDC753588F18488093A3533B5,
	U3CTraverseExpandedTreeU3Ed__11_System_Collections_IEnumerator_get_Current_mB59796F1B37FF3497144D84A6A2F8E8E001B16D7,
	U3CTraverseExpandedTreeU3Ed__11_System_Collections_Generic_IEnumerableU3CVirtualizedScrollRectDropdown_DropdownNodeU3E_GetEnumerator_m2DD7F35FD932DE1F5931B2CC0BFBC0AE6F8EA465,
	U3CTraverseExpandedTreeU3Ed__11_System_Collections_IEnumerable_GetEnumerator_m1A403747CD73673217F6B52F158CE1310B32ACB7,
	U3CEndOfFrameInitializeU3Ed__54__ctor_mAEFB1DB6CFC7EC0849EA8BB97EDDA8D84BDBB345,
	U3CEndOfFrameInitializeU3Ed__54_System_IDisposable_Dispose_m0E11B05B209B989EAB93E0BE63D1121FE3321E33,
	U3CEndOfFrameInitializeU3Ed__54_MoveNext_m533EAD14FEC8BA916B74C26EC49D938E983A0505,
	U3CEndOfFrameInitializeU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12FCF0379F696C59EB3A887D0C7CDF90A161F210,
	U3CEndOfFrameInitializeU3Ed__54_System_Collections_IEnumerator_Reset_mB97056C82284E24994CBDDA6CD987122D14EFB36,
	U3CEndOfFrameInitializeU3Ed__54_System_Collections_IEnumerator_get_Current_m839FE56810966248491B02954A224C93BFDCCA7C,
	Window_get_CurrentViewIdx_m80116C0F124D5ADAC8F2433D92DA9239582FF688,
	Window_set_CurrentViewIdx_mE2ECFA62E2A5B8686BFF43D6B60B2AD2C1F6534E,
	Window_Close_m23280372D65090C0FFD3AF2498E9E0900F80607D,
	Window_Open_mD928FAA24D4F13587362E04022968C000D9754A3,
	Window_BringIntoFocus_m99E44EECB3E0A338AF1E18192E051732FA387F21,
	Window_Start_mE735AD36D179FD96AE86617476E0456043EEA6F3,
	Window_Update_m6D6FEAE3CF208B12341F6BB2281823AAD35B9851,
	Window__ctor_mFB056C087DFE57E41DEB633825899473B59E9B0E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkHelper_GetLocalIPAddress_mEB955EA2B25CCD3AB946232A4C7EA759B5CEF9D6,
	NetworkHelper_InitializeRemoteServices_mADDBB8490D8C03F86F96FEEAD5F9763FFB8552DD,
	NetworkHelper__ctor_mB61C7D76612961EE5B815A16643BBFD88797E215,
	U3CInitializeRemoteServicesU3Ed__2_MoveNext_mA4D2A43BC7AAB416CD8F4B72BDED1F724DA30C81,
	U3CInitializeRemoteServicesU3Ed__2_SetStateMachine_mE82F2F99EBC77877DEF695C1F0BD36DA2C56C76E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ClientNetworkTransform_OnIsServerAuthoritative_m0270CDAF368412EFF55D0CDD8D633127DDDF1753,
	ClientNetworkTransform__ctor_m5DFB7B5CCA0BCCB07B3F58E7C2BCD2E648043AAC,
	ClientNetworkTransform___initializeVariables_mF55990F97595B98244B63CB75F908420B8A0C9E0,
	ClientNetworkTransform___getTypeName_mD015C2C216565C5492A4A10B9C77978255ED1FD7,
	Billboard_LateUpdate_m66475B437AE6267C7A5BB47DF60B27A7DECF8819,
	Billboard__ctor_m6FE92E731006932969545C0B8F83A974AFAFE62E,
	MaterialMatrix_BuildMatrix_m7A72C99F4F1BA20FA27476E421EA9DD15B32458F,
	MaterialMatrix__ctor_m81B93D4D4BA51773CAC69423675EBB8596A24415,
	NetworkVariableSerializationHelper_InitializeSerialization_mA4F17C9D8F4934C552263003358AF8CC0E55E6E3,
};
extern void LocalTransform__ctor_m5B614313B67478392333D4BF0AC97F9FF5E16B3D_AdjustorThunk (void);
extern void LocalTransform__ctor_m6236FFC8C68D10344A687DED13BAA193C967A635_AdjustorThunk (void);
extern void U3CStartU3Ed__1_MoveNext_mD2A371AD44B08D643FCC1CBE31AEC1E2FF13A2EE_AdjustorThunk (void);
extern void U3CStartU3Ed__1_SetStateMachine_mD189FAB04CE51EC7DC2E00C4547993B95ACD637E_AdjustorThunk (void);
extern void U3CSpawnNodeAsyncU3Ed__6_MoveNext_mECD34BB602497E62BD79E97238DE2A82B177253D_AdjustorThunk (void);
extern void U3CSpawnNodeAsyncU3Ed__6_SetStateMachine_mBE32AC90DEA5CE29BD86EDC48EC557CEB4207597_AdjustorThunk (void);
extern void U3CInitializeInstantiatedLayersAsyncU3Ed__8_MoveNext_m391D984F9F94FA0589D9B4A8839FBAB9056F9698_AdjustorThunk (void);
extern void U3CInitializeInstantiatedLayersAsyncU3Ed__8_SetStateMachine_m3C625409AFF0F2D49092A283948F50AFEE5C54FF_AdjustorThunk (void);
extern void U3CSetModelServerAsyncU3Ed__28_MoveNext_m6DF37858A28B30105DACAF6ECCBA557114CAE733_AdjustorThunk (void);
extern void U3CSetModelServerAsyncU3Ed__28_SetStateMachine_m8D4E88707BDB0867A619D67317DF84000D230704_AdjustorThunk (void);
extern void U3CSetModelClientAsyncU3Ed__30_MoveNext_m3970AA534DC9529520D7A7828A315D58AB120556_AdjustorThunk (void);
extern void U3CSetModelClientAsyncU3Ed__30_SetStateMachine_m08B8D8353C6879666DFC6EB7A1D66D32759517A7_AdjustorThunk (void);
extern void AddressableGUID_GetHashCode_m1B75548F336B04CBCC0F79D343C8B7793A5428CD_AdjustorThunk (void);
extern void AddressableGUID_ToString_m4FEFA14381A279352C222CAC78765D64877B0F57_AdjustorThunk (void);
extern void U3CLoadDynamicPrefabU3Ed__18_MoveNext_mDAA5B95B0DEC4B3CD160BE6FA7A41995C186005A_AdjustorThunk (void);
extern void U3CLoadDynamicPrefabU3Ed__18_SetStateMachine_m7201FDC8677C4768C3AA33252F3DD9916A159DE3_AdjustorThunk (void);
extern void U3CLoadDynamicPrefabsU3Ed__19_MoveNext_mBAF81017A61D5A5310906EC39EFA0762615B4647_AdjustorThunk (void);
extern void U3CLoadDynamicPrefabsU3Ed__19_SetStateMachine_mDECD003D0E34E262C5F471404EF89DBAFB80948F_AdjustorThunk (void);
extern void U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_MoveNext_m5A60F480F8432D153F33112324D84A881C8012F7_AdjustorThunk (void);
extern void U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_SetStateMachine_m6646A5927C420EDF0F8AA7F7D6D686905FBA611F_AdjustorThunk (void);
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_MoveNext_m84534720935749153F37C828F1959A67C50394B5_AdjustorThunk (void);
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_SetStateMachine_mC4A20EEAFB77E59E681CD112D7AC68B391272CC3_AdjustorThunk (void);
extern void U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_MoveNext_mB062C756B59D76F04BB4EF0B3C4A3704712512EC_AdjustorThunk (void);
extern void U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_SetStateMachine_mC1A34E7246CFDA1FD65C6B857FF0E8110B126386_AdjustorThunk (void);
extern void U3CSpawnDynamicPrefabU3Ed__4_MoveNext_mB88B52A3253E26DCA53797F7312041743C7A6A05_AdjustorThunk (void);
extern void U3CSpawnDynamicPrefabU3Ed__4_SetStateMachine_m287475ABB1EE5F9AAF6CB4194AD5FD71B141C724_AdjustorThunk (void);
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_MoveNext_m848CA185D81D33BB8E707503D26BEDBF3F4BFEF8_AdjustorThunk (void);
extern void U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_SetStateMachine_m46CEE3262BCDF0776CD304BCF73B1715360897D2_AdjustorThunk (void);
extern void U3CStartLocalSessionU3Ed__29_MoveNext_mC11450688E2D0A5ECEF84F28F37829DA0017B62B_AdjustorThunk (void);
extern void U3CStartLocalSessionU3Ed__29_SetStateMachine_mBC67BDED374E292E47757E00BB3F26D1A870B2BD_AdjustorThunk (void);
extern void U3CStartRemoteSessionU3Ed__30_MoveNext_mB8D0CBAC544812F856C95679905AD6F1AA41FA97_AdjustorThunk (void);
extern void U3CStartRemoteSessionU3Ed__30_SetStateMachine_mB4894FD3F5D0E09A81BE6DC98C304351981276C7_AdjustorThunk (void);
extern void U3CJoinLocalSessionU3Ed__31_MoveNext_m0DE55FB91C1D2392FFD73CD6C4E4DF93234C3C1E_AdjustorThunk (void);
extern void U3CJoinLocalSessionU3Ed__31_SetStateMachine_m663F267133CBC463162F19434B82D50EDFD16E59_AdjustorThunk (void);
extern void U3CJoinRemoteSessionU3Ed__32_MoveNext_mB0BF725FBA739BA3411520ECB2EEC0D04487D2E0_AdjustorThunk (void);
extern void U3CJoinRemoteSessionU3Ed__32_SetStateMachine_mA2A614F5864911C4EC4866301D52DB8BF0CA9F5C_AdjustorThunk (void);
extern void U3CExitSessionU3Ed__33_MoveNext_m97083D94FFDB66ABE557DA3F100E8B592B25493D_AdjustorThunk (void);
extern void U3CExitSessionU3Ed__33_SetStateMachine_m2F34597B460DEEA622D677CE0C816C2A63589AE3_AdjustorThunk (void);
extern void U3CTransferHostU3Ed__34_MoveNext_m0EF821494E6C1BDE589B6762779195861AB9DAA4_AdjustorThunk (void);
extern void U3CTransferHostU3Ed__34_SetStateMachine_mCCD291FA1D801B61D8A57C3E1C076A041BA21AD7_AdjustorThunk (void);
extern void U3CShutdownNetworkManagerU3Ed__38_MoveNext_m3CFF850A83E2614256D3D33EE12BFD67B9DABA7D_AdjustorThunk (void);
extern void U3CShutdownNetworkManagerU3Ed__38_SetStateMachine_m56E3983D843FD13B901ED58E2C2E52689DF3B836_AdjustorThunk (void);
extern void U3CPlaceLocalAnchorU3Ed__43_MoveNext_mE4857E80B20205CE4CADC54540E85D7B17FB5FBE_AdjustorThunk (void);
extern void U3CPlaceLocalAnchorU3Ed__43_SetStateMachine_mA8ECAE99CD4D1459DC3423061E32F1BD4482A10E_AdjustorThunk (void);
extern void U3CPlaceRemoteAnchorU3Ed__44_MoveNext_m1E6AAB357B1316C8EE163F5657501D8D16A03862_AdjustorThunk (void);
extern void U3CPlaceRemoteAnchorU3Ed__44_SetStateMachine_m1CC20A722C021B1ACE4A330CA4A77EC73B5F4071_AdjustorThunk (void);
extern void U3CCreateRelayU3Ed__45_MoveNext_mB5970CAAA03FD31E5EAD2512DDCC334A8601B66D_AdjustorThunk (void);
extern void U3CCreateRelayU3Ed__45_SetStateMachine_m258A3525393C6ED7F1FC4541E3B98018658517D4_AdjustorThunk (void);
extern void U3CJoinRelayU3Ed__46_MoveNext_m89BC4291D60E5009914A2969424C0919BA6A5AB4_AdjustorThunk (void);
extern void U3CJoinRelayU3Ed__46_SetStateMachine_m40C03F5C232047EBC7397C2CE181CF6C8F794458_AdjustorThunk (void);
extern void U3CRefreshLobbyListU3Ed__9_MoveNext_m25444895F8ECC7ABEF7D86B5EE6C0714CDDDA59D_AdjustorThunk (void);
extern void U3CRefreshLobbyListU3Ed__9_SetStateMachine_mACA4060E704D5DF0652FF515F3C6076290027169_AdjustorThunk (void);
extern void U3CStartSearchingU3Ed__10_MoveNext_m1BC4133C328BB76EBDA027DB0286198AC8BB3865_AdjustorThunk (void);
extern void U3CStartSearchingU3Ed__10_SetStateMachine_mFD2AF4BC5CC765B4DD70B09BA164991BF0E4813E_AdjustorThunk (void);
extern void U3CStopSearchingU3Ed__11_MoveNext_m94A73BD34E875E523A3106B3A35850EF49931757_AdjustorThunk (void);
extern void U3CStopSearchingU3Ed__11_SetStateMachine_m2BB6F3CBE7BB647554A07CBA9B7471EA9186C8CA_AdjustorThunk (void);
extern void U3CHandleLobbyHeartbeatU3Ed__16_MoveNext_mABB2068123612A856345C26D9F29469C08FB7650_AdjustorThunk (void);
extern void U3CHandleLobbyHeartbeatU3Ed__16_SetStateMachine_mE54FAB3A73B11C01A066A9E101317C15B252344D_AdjustorThunk (void);
extern void U3CHandleLobbyPollingU3Ed__17_MoveNext_mF41C7C2B380BEADF3FFC3FDB0F29F927AEC162AE_AdjustorThunk (void);
extern void U3CHandleLobbyPollingU3Ed__17_SetStateMachine_m85B92A652AAA0C42BC81732DE2208C91AB2837BE_AdjustorThunk (void);
extern void U3CUpdatePlayerNameU3Ed__23_MoveNext_mB6A64351A0BECD3340DCF230846172D90C0B87DA_AdjustorThunk (void);
extern void U3CUpdatePlayerNameU3Ed__23_SetStateMachine_m46E12BFD3917F99E43B9CE9627E19EB3B33D0FFC_AdjustorThunk (void);
extern void U3CCreateLobbyU3Ed__24_MoveNext_mEBAEC77D233648DC14816A0220460FBC34FA4E4F_AdjustorThunk (void);
extern void U3CCreateLobbyU3Ed__24_SetStateMachine_m028997C9F1E7FBA40ADCDE13D7EA17355CEADD6D_AdjustorThunk (void);
extern void U3CJoinLobbyByCodeU3Ed__25_MoveNext_m6F9BAE91C9FE08A92E3AD62D2FD2AB27583B3372_AdjustorThunk (void);
extern void U3CJoinLobbyByCodeU3Ed__25_SetStateMachine_mB5FD19586A74F53A9ED43337FA2256DCE8412ACC_AdjustorThunk (void);
extern void U3CJoinLobbyU3Ed__26_MoveNext_m9F9C11809B250A10242DEE4F2A028B96DE79410E_AdjustorThunk (void);
extern void U3CJoinLobbyU3Ed__26_SetStateMachine_m9DE0AD576552F531E92DCB603535B298396E931D_AdjustorThunk (void);
extern void U3CLeaveLobbyU3Ed__27_MoveNext_m8A73C2A8A0C45CD7156A42D1CAE243AB5D79AD78_AdjustorThunk (void);
extern void U3CLeaveLobbyU3Ed__27_SetStateMachine_mF80F0204D2765208BC35273C8D4E2A17E6A7D353_AdjustorThunk (void);
extern void U3CKickPlayerU3Ed__28_MoveNext_m9872BE9C0490DE8C8A946F27CC0FEBA09C94E50F_AdjustorThunk (void);
extern void U3CKickPlayerU3Ed__28_SetStateMachine_m61E87687F064F4D623EEE7A77240E9DF349D6503_AdjustorThunk (void);
extern void U3CDestroyLobbyU3Ed__29_MoveNext_m34FDFD43B2A080EC6EE88636C9D733CB5C81FAB2_AdjustorThunk (void);
extern void U3CDestroyLobbyU3Ed__29_SetStateMachine_m3D8DB8CDACA3D15D72B442DD617A13DFE56738BF_AdjustorThunk (void);
extern void U3CTransferLobbyHostU3Ed__30_MoveNext_m4FC6D9D4AA9383060CA5DDB317E3E1E686DA8877_AdjustorThunk (void);
extern void U3CTransferLobbyHostU3Ed__30_SetStateMachine_m9B00D73A248FE24B300127054BC90CC69CBE10CE_AdjustorThunk (void);
extern void U3CAdvertiseServerAsyncU3Ed__22_MoveNext_mA4953319F3B567A79ECB0EBB397A59A28D2D7A12_AdjustorThunk (void);
extern void U3CAdvertiseServerAsyncU3Ed__22_SetStateMachine_m90936D6BD07428A4D6C7B052D5E26E806DA0EE2A_AdjustorThunk (void);
extern void U3CSearchForServersAsyncU3Ed__25_MoveNext_m285A8B5B876066DAF7FAD3F800DFEB229B8BFAED_AdjustorThunk (void);
extern void U3CSearchForServersAsyncU3Ed__25_SetStateMachine_m515B2595011C1B82C34D358DF3C3D201E5B31F7A_AdjustorThunk (void);
extern void U3CStartListeningClientAsyncU3Ed__51_MoveNext_mC296F54F2EEFC5AFD81551A534F467ABA06159FF_AdjustorThunk (void);
extern void U3CStartListeningClientAsyncU3Ed__51_SetStateMachine_mB595BD4394C387BD7DF0E7208720317C2364E296_AdjustorThunk (void);
extern void U3CStartListeningServerAsyncU3Ed__53_MoveNext_m343428C0B7362B8E517B9BBAE531CCBC0F2594D0_AdjustorThunk (void);
extern void U3CStartListeningServerAsyncU3Ed__53_SetStateMachine_m7F74BCAA5437DF0508E0F095B85F561E9409B75B_AdjustorThunk (void);
extern void U3CStartSearchingClientAsyncU3Ed__56_MoveNext_m2D1E6DBE748978539DE360BBB9CA49A37A98C2F9_AdjustorThunk (void);
extern void U3CStartSearchingClientAsyncU3Ed__56_SetStateMachine_m67B5E70F7BC655728EDC778350C8176F8BBBC90D_AdjustorThunk (void);
extern void U3CSendClientMessageAsyncU3Ed__58_MoveNext_mD66C90618809CF9A70C25959978F5F5E644F3541_AdjustorThunk (void);
extern void U3CSendClientMessageAsyncU3Ed__58_SetStateMachine_m6D290276DF0382BD9589625ECA249F2B4348BDAC_AdjustorThunk (void);
extern void U3CSendServerMessageAsyncU3Ed__59_MoveNext_m4E42D1CB25FB4F3A0BF6C129E30F630177E67506_AdjustorThunk (void);
extern void U3CSendServerMessageAsyncU3Ed__59_SetStateMachine_mA67FCC5226AA7F338976A0BD63E36F10F1FA0F07_AdjustorThunk (void);
extern void U3CHandleLobbySearchRequestU3Ed__72_MoveNext_mD43858816A0807484305BF003FACD2D0D32D652E_AdjustorThunk (void);
extern void U3CHandleLobbySearchRequestU3Ed__72_SetStateMachine_mD9705204E6BF8C12370E1E44F027CADA0FA79D1F_AdjustorThunk (void);
extern void U3CSendLobbyUpdateU3Ed__80_MoveNext_mF7243DBCDB8E10D5DC0DAA519D04A491BA1C0E85_AdjustorThunk (void);
extern void U3CSendLobbyUpdateU3Ed__80_SetStateMachine_m398D67B94FA145E40C4795B2EC7A2C13AB5705C1_AdjustorThunk (void);
extern void U3CSendPlayerUpdateU3Ed__81_MoveNext_m68B140503BF008CB9C3F5EC3B6777C7014501462_AdjustorThunk (void);
extern void U3CSendPlayerUpdateU3Ed__81_SetStateMachine_m0544B8DD9FE6B4FC9C9606CE0F825F435692D87B_AdjustorThunk (void);
extern void U3CSendLobbySearchRequestU3Ed__82_MoveNext_m759975C2302973B31ED94F8205E0E2BAE33B7A4D_AdjustorThunk (void);
extern void U3CSendLobbySearchRequestU3Ed__82_SetStateMachine_m43B1C2551233FF1177772227F7B0C93ED08A9B83_AdjustorThunk (void);
extern void U3CSendClientHeartbeatU3Ed__83_MoveNext_m3E15302EBB44E9F9913E273D79A363ED4B327B3D_AdjustorThunk (void);
extern void U3CSendClientHeartbeatU3Ed__83_SetStateMachine_mE3EF863032125870B8B3E4F5996713D2E6359DCD_AdjustorThunk (void);
extern void U3CCreateLobbyU3Ed__84_MoveNext_m218701A5A677B7C500C48A0C272794D051B11917_AdjustorThunk (void);
extern void U3CCreateLobbyU3Ed__84_SetStateMachine_m6CBBAB0CE132420589F3F5629A380256A1539CDB_AdjustorThunk (void);
extern void U3CJoinLobbyByCodeU3Ed__85_MoveNext_mD72A3D5FD08210B803129F88CC3D8C3B45402B48_AdjustorThunk (void);
extern void U3CJoinLobbyByCodeU3Ed__85_SetStateMachine_mDA60AE5B340745B217794FF36D61CB7372CE914D_AdjustorThunk (void);
extern void U3CJoinLobbyU3Ed__86_MoveNext_m123B4D24C1ED48F8FA612BE2900BD65C330B28AB_AdjustorThunk (void);
extern void U3CJoinLobbyU3Ed__86_SetStateMachine_m5B34EFB95E2B8B3467845E4442ECED18733DA047_AdjustorThunk (void);
extern void U3CLeaveLobbyU3Ed__87_MoveNext_m04DA6B1D686AD5A7080C27709914228BE95C81A2_AdjustorThunk (void);
extern void U3CLeaveLobbyU3Ed__87_SetStateMachine_m1816662CBEACB2A24E946D7CAE3DD1E21EE453D2_AdjustorThunk (void);
extern void U3CDestroyLobbyU3Ed__88_MoveNext_m50A01D84130D48C3E13AEC498339E1D45F836CCE_AdjustorThunk (void);
extern void U3CDestroyLobbyU3Ed__88_SetStateMachine_m70B496D1E400BEDF360BAD4875BCA85C3B021D96_AdjustorThunk (void);
extern void U3CKickPlayerU3Ed__89_MoveNext_mEFF5F9A5414632CAB14C0BD10E301B72E7E8AD04_AdjustorThunk (void);
extern void U3CKickPlayerU3Ed__89_SetStateMachine_m873D5625E53C2781F3BD2BC364A6F98F18052AC6_AdjustorThunk (void);
extern void U3CTransferLobbyHostU3Ed__90_MoveNext_m2DE99B1A92DD04B6FD9A94111D42BB73CAB042AA_AdjustorThunk (void);
extern void U3CTransferLobbyHostU3Ed__90_SetStateMachine_mCCC607DCC0303A0585D3AFC30782A8D52E578EB6_AdjustorThunk (void);
extern void U3CU3CCreateLobbyU3Eb__84_0U3Ed_MoveNext_m3091BB36BAD5C9AE3FF475D2D0CF4D801DD2B775_AdjustorThunk (void);
extern void U3CU3CCreateLobbyU3Eb__84_0U3Ed_SetStateMachine_mBB98E6072F3F29C271793965A4409115595D468D_AdjustorThunk (void);
extern void U3CInitializeRemoteServicesU3Ed__2_MoveNext_mA4D2A43BC7AAB416CD8F4B72BDED1F724DA30C81_AdjustorThunk (void);
extern void U3CInitializeRemoteServicesU3Ed__2_SetStateMachine_mE82F2F99EBC77877DEF695C1F0BD36DA2C56C76E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[118] = 
{
	{ 0x0600001C, LocalTransform__ctor_m5B614313B67478392333D4BF0AC97F9FF5E16B3D_AdjustorThunk },
	{ 0x0600001D, LocalTransform__ctor_m6236FFC8C68D10344A687DED13BAA193C967A635_AdjustorThunk },
	{ 0x0600004F, U3CStartU3Ed__1_MoveNext_mD2A371AD44B08D643FCC1CBE31AEC1E2FF13A2EE_AdjustorThunk },
	{ 0x06000050, U3CStartU3Ed__1_SetStateMachine_mD189FAB04CE51EC7DC2E00C4547993B95ACD637E_AdjustorThunk },
	{ 0x06000120, U3CSpawnNodeAsyncU3Ed__6_MoveNext_mECD34BB602497E62BD79E97238DE2A82B177253D_AdjustorThunk },
	{ 0x06000121, U3CSpawnNodeAsyncU3Ed__6_SetStateMachine_mBE32AC90DEA5CE29BD86EDC48EC557CEB4207597_AdjustorThunk },
	{ 0x06000125, U3CInitializeInstantiatedLayersAsyncU3Ed__8_MoveNext_m391D984F9F94FA0589D9B4A8839FBAB9056F9698_AdjustorThunk },
	{ 0x06000126, U3CInitializeInstantiatedLayersAsyncU3Ed__8_SetStateMachine_m3C625409AFF0F2D49092A283948F50AFEE5C54FF_AdjustorThunk },
	{ 0x0600017D, U3CSetModelServerAsyncU3Ed__28_MoveNext_m6DF37858A28B30105DACAF6ECCBA557114CAE733_AdjustorThunk },
	{ 0x0600017E, U3CSetModelServerAsyncU3Ed__28_SetStateMachine_m8D4E88707BDB0867A619D67317DF84000D230704_AdjustorThunk },
	{ 0x0600017F, U3CSetModelClientAsyncU3Ed__30_MoveNext_m3970AA534DC9529520D7A7828A315D58AB120556_AdjustorThunk },
	{ 0x06000180, U3CSetModelClientAsyncU3Ed__30_SetStateMachine_m08B8D8353C6879666DFC6EB7A1D66D32759517A7_AdjustorThunk },
	{ 0x060002D3, AddressableGUID_GetHashCode_m1B75548F336B04CBCC0F79D343C8B7793A5428CD_AdjustorThunk },
	{ 0x060002D4, AddressableGUID_ToString_m4FEFA14381A279352C222CAC78765D64877B0F57_AdjustorThunk },
	{ 0x060002EA, U3CLoadDynamicPrefabU3Ed__18_MoveNext_mDAA5B95B0DEC4B3CD160BE6FA7A41995C186005A_AdjustorThunk },
	{ 0x060002EB, U3CLoadDynamicPrefabU3Ed__18_SetStateMachine_m7201FDC8677C4768C3AA33252F3DD9916A159DE3_AdjustorThunk },
	{ 0x060002EC, U3CLoadDynamicPrefabsU3Ed__19_MoveNext_mBAF81017A61D5A5310906EC39EFA0762615B4647_AdjustorThunk },
	{ 0x060002ED, U3CLoadDynamicPrefabsU3Ed__19_SetStateMachine_mDECD003D0E34E262C5F471404EF89DBAFB80948F_AdjustorThunk },
	{ 0x060002FF, U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_MoveNext_m5A60F480F8432D153F33112324D84A881C8012F7_AdjustorThunk },
	{ 0x06000300, U3CTrySpawnDynamicPrefabSynchronouslyU3Ed__9_SetStateMachine_m6646A5927C420EDF0F8AA7F7D6D686905FBA611F_AdjustorThunk },
	{ 0x06000301, U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_MoveNext_m84534720935749153F37C828F1959A67C50394B5_AdjustorThunk },
	{ 0x06000302, U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C10_0U3Ed_SetStateMachine_mC4A20EEAFB77E59E681CD112D7AC68B391272CC3_AdjustorThunk },
	{ 0x06000312, U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_MoveNext_mB062C756B59D76F04BB4EF0B3C4A3704712512EC_AdjustorThunk },
	{ 0x06000313, U3CU3CSpawnDynamicPrefabU3Eg__SpawnU7C0U3Ed_SetStateMachine_mC1A34E7246CFDA1FD65C6B857FF0E8110B126386_AdjustorThunk },
	{ 0x06000316, U3CSpawnDynamicPrefabU3Ed__4_MoveNext_mB88B52A3253E26DCA53797F7312041743C7A6A05_AdjustorThunk },
	{ 0x06000317, U3CSpawnDynamicPrefabU3Ed__4_SetStateMachine_m287475ABB1EE5F9AAF6CB4194AD5FD71B141C724_AdjustorThunk },
	{ 0x06000318, U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_MoveNext_m848CA185D81D33BB8E707503D26BEDBF3F4BFEF8_AdjustorThunk },
	{ 0x06000319, U3CU3CLoadAddressableClientRpcU3Eg__LoadU7C6_0U3Ed_SetStateMachine_m46CEE3262BCDF0776CD304BCF73B1715360897D2_AdjustorThunk },
	{ 0x06000352, U3CStartLocalSessionU3Ed__29_MoveNext_mC11450688E2D0A5ECEF84F28F37829DA0017B62B_AdjustorThunk },
	{ 0x06000353, U3CStartLocalSessionU3Ed__29_SetStateMachine_mBC67BDED374E292E47757E00BB3F26D1A870B2BD_AdjustorThunk },
	{ 0x06000354, U3CStartRemoteSessionU3Ed__30_MoveNext_mB8D0CBAC544812F856C95679905AD6F1AA41FA97_AdjustorThunk },
	{ 0x06000355, U3CStartRemoteSessionU3Ed__30_SetStateMachine_mB4894FD3F5D0E09A81BE6DC98C304351981276C7_AdjustorThunk },
	{ 0x06000356, U3CJoinLocalSessionU3Ed__31_MoveNext_m0DE55FB91C1D2392FFD73CD6C4E4DF93234C3C1E_AdjustorThunk },
	{ 0x06000357, U3CJoinLocalSessionU3Ed__31_SetStateMachine_m663F267133CBC463162F19434B82D50EDFD16E59_AdjustorThunk },
	{ 0x06000358, U3CJoinRemoteSessionU3Ed__32_MoveNext_mB0BF725FBA739BA3411520ECB2EEC0D04487D2E0_AdjustorThunk },
	{ 0x06000359, U3CJoinRemoteSessionU3Ed__32_SetStateMachine_mA2A614F5864911C4EC4866301D52DB8BF0CA9F5C_AdjustorThunk },
	{ 0x0600035A, U3CExitSessionU3Ed__33_MoveNext_m97083D94FFDB66ABE557DA3F100E8B592B25493D_AdjustorThunk },
	{ 0x0600035B, U3CExitSessionU3Ed__33_SetStateMachine_m2F34597B460DEEA622D677CE0C816C2A63589AE3_AdjustorThunk },
	{ 0x0600035C, U3CTransferHostU3Ed__34_MoveNext_m0EF821494E6C1BDE589B6762779195861AB9DAA4_AdjustorThunk },
	{ 0x0600035D, U3CTransferHostU3Ed__34_SetStateMachine_mCCD291FA1D801B61D8A57C3E1C076A041BA21AD7_AdjustorThunk },
	{ 0x0600035E, U3CShutdownNetworkManagerU3Ed__38_MoveNext_m3CFF850A83E2614256D3D33EE12BFD67B9DABA7D_AdjustorThunk },
	{ 0x0600035F, U3CShutdownNetworkManagerU3Ed__38_SetStateMachine_m56E3983D843FD13B901ED58E2C2E52689DF3B836_AdjustorThunk },
	{ 0x06000364, U3CPlaceLocalAnchorU3Ed__43_MoveNext_mE4857E80B20205CE4CADC54540E85D7B17FB5FBE_AdjustorThunk },
	{ 0x06000365, U3CPlaceLocalAnchorU3Ed__43_SetStateMachine_mA8ECAE99CD4D1459DC3423061E32F1BD4482A10E_AdjustorThunk },
	{ 0x06000368, U3CPlaceRemoteAnchorU3Ed__44_MoveNext_m1E6AAB357B1316C8EE163F5657501D8D16A03862_AdjustorThunk },
	{ 0x06000369, U3CPlaceRemoteAnchorU3Ed__44_SetStateMachine_m1CC20A722C021B1ACE4A330CA4A77EC73B5F4071_AdjustorThunk },
	{ 0x0600036A, U3CCreateRelayU3Ed__45_MoveNext_mB5970CAAA03FD31E5EAD2512DDCC334A8601B66D_AdjustorThunk },
	{ 0x0600036B, U3CCreateRelayU3Ed__45_SetStateMachine_m258A3525393C6ED7F1FC4541E3B98018658517D4_AdjustorThunk },
	{ 0x0600036C, U3CJoinRelayU3Ed__46_MoveNext_m89BC4291D60E5009914A2969424C0919BA6A5AB4_AdjustorThunk },
	{ 0x0600036D, U3CJoinRelayU3Ed__46_SetStateMachine_m40C03F5C232047EBC7397C2CE181CF6C8F794458_AdjustorThunk },
	{ 0x0600037B, U3CRefreshLobbyListU3Ed__9_MoveNext_m25444895F8ECC7ABEF7D86B5EE6C0714CDDDA59D_AdjustorThunk },
	{ 0x0600037C, U3CRefreshLobbyListU3Ed__9_SetStateMachine_mACA4060E704D5DF0652FF515F3C6076290027169_AdjustorThunk },
	{ 0x0600037D, U3CStartSearchingU3Ed__10_MoveNext_m1BC4133C328BB76EBDA027DB0286198AC8BB3865_AdjustorThunk },
	{ 0x0600037E, U3CStartSearchingU3Ed__10_SetStateMachine_mFD2AF4BC5CC765B4DD70B09BA164991BF0E4813E_AdjustorThunk },
	{ 0x0600037F, U3CStopSearchingU3Ed__11_MoveNext_m94A73BD34E875E523A3106B3A35850EF49931757_AdjustorThunk },
	{ 0x06000380, U3CStopSearchingU3Ed__11_SetStateMachine_m2BB6F3CBE7BB647554A07CBA9B7471EA9186C8CA_AdjustorThunk },
	{ 0x060003AC, U3CHandleLobbyHeartbeatU3Ed__16_MoveNext_mABB2068123612A856345C26D9F29469C08FB7650_AdjustorThunk },
	{ 0x060003AD, U3CHandleLobbyHeartbeatU3Ed__16_SetStateMachine_mE54FAB3A73B11C01A066A9E101317C15B252344D_AdjustorThunk },
	{ 0x060003AE, U3CHandleLobbyPollingU3Ed__17_MoveNext_mF41C7C2B380BEADF3FFC3FDB0F29F927AEC162AE_AdjustorThunk },
	{ 0x060003AF, U3CHandleLobbyPollingU3Ed__17_SetStateMachine_m85B92A652AAA0C42BC81732DE2208C91AB2837BE_AdjustorThunk },
	{ 0x060003B0, U3CUpdatePlayerNameU3Ed__23_MoveNext_mB6A64351A0BECD3340DCF230846172D90C0B87DA_AdjustorThunk },
	{ 0x060003B1, U3CUpdatePlayerNameU3Ed__23_SetStateMachine_m46E12BFD3917F99E43B9CE9627E19EB3B33D0FFC_AdjustorThunk },
	{ 0x060003B2, U3CCreateLobbyU3Ed__24_MoveNext_mEBAEC77D233648DC14816A0220460FBC34FA4E4F_AdjustorThunk },
	{ 0x060003B3, U3CCreateLobbyU3Ed__24_SetStateMachine_m028997C9F1E7FBA40ADCDE13D7EA17355CEADD6D_AdjustorThunk },
	{ 0x060003B4, U3CJoinLobbyByCodeU3Ed__25_MoveNext_m6F9BAE91C9FE08A92E3AD62D2FD2AB27583B3372_AdjustorThunk },
	{ 0x060003B5, U3CJoinLobbyByCodeU3Ed__25_SetStateMachine_mB5FD19586A74F53A9ED43337FA2256DCE8412ACC_AdjustorThunk },
	{ 0x060003B6, U3CJoinLobbyU3Ed__26_MoveNext_m9F9C11809B250A10242DEE4F2A028B96DE79410E_AdjustorThunk },
	{ 0x060003B7, U3CJoinLobbyU3Ed__26_SetStateMachine_m9DE0AD576552F531E92DCB603535B298396E931D_AdjustorThunk },
	{ 0x060003B8, U3CLeaveLobbyU3Ed__27_MoveNext_m8A73C2A8A0C45CD7156A42D1CAE243AB5D79AD78_AdjustorThunk },
	{ 0x060003B9, U3CLeaveLobbyU3Ed__27_SetStateMachine_mF80F0204D2765208BC35273C8D4E2A17E6A7D353_AdjustorThunk },
	{ 0x060003BA, U3CKickPlayerU3Ed__28_MoveNext_m9872BE9C0490DE8C8A946F27CC0FEBA09C94E50F_AdjustorThunk },
	{ 0x060003BB, U3CKickPlayerU3Ed__28_SetStateMachine_m61E87687F064F4D623EEE7A77240E9DF349D6503_AdjustorThunk },
	{ 0x060003BC, U3CDestroyLobbyU3Ed__29_MoveNext_m34FDFD43B2A080EC6EE88636C9D733CB5C81FAB2_AdjustorThunk },
	{ 0x060003BD, U3CDestroyLobbyU3Ed__29_SetStateMachine_m3D8DB8CDACA3D15D72B442DD617A13DFE56738BF_AdjustorThunk },
	{ 0x060003BE, U3CTransferLobbyHostU3Ed__30_MoveNext_m4FC6D9D4AA9383060CA5DDB317E3E1E686DA8877_AdjustorThunk },
	{ 0x060003BF, U3CTransferLobbyHostU3Ed__30_SetStateMachine_m9B00D73A248FE24B300127054BC90CC69CBE10CE_AdjustorThunk },
	{ 0x060003D5, U3CAdvertiseServerAsyncU3Ed__22_MoveNext_mA4953319F3B567A79ECB0EBB397A59A28D2D7A12_AdjustorThunk },
	{ 0x060003D6, U3CAdvertiseServerAsyncU3Ed__22_SetStateMachine_m90936D6BD07428A4D6C7B052D5E26E806DA0EE2A_AdjustorThunk },
	{ 0x060003D9, U3CSearchForServersAsyncU3Ed__25_MoveNext_m285A8B5B876066DAF7FAD3F800DFEB229B8BFAED_AdjustorThunk },
	{ 0x060003DA, U3CSearchForServersAsyncU3Ed__25_SetStateMachine_m515B2595011C1B82C34D358DF3C3D201E5B31F7A_AdjustorThunk },
	{ 0x0600041B, U3CStartListeningClientAsyncU3Ed__51_MoveNext_mC296F54F2EEFC5AFD81551A534F467ABA06159FF_AdjustorThunk },
	{ 0x0600041C, U3CStartListeningClientAsyncU3Ed__51_SetStateMachine_mB595BD4394C387BD7DF0E7208720317C2364E296_AdjustorThunk },
	{ 0x0600041F, U3CStartListeningServerAsyncU3Ed__53_MoveNext_m343428C0B7362B8E517B9BBAE531CCBC0F2594D0_AdjustorThunk },
	{ 0x06000420, U3CStartListeningServerAsyncU3Ed__53_SetStateMachine_m7F74BCAA5437DF0508E0F095B85F561E9409B75B_AdjustorThunk },
	{ 0x06000421, U3CStartSearchingClientAsyncU3Ed__56_MoveNext_m2D1E6DBE748978539DE360BBB9CA49A37A98C2F9_AdjustorThunk },
	{ 0x06000422, U3CStartSearchingClientAsyncU3Ed__56_SetStateMachine_m67B5E70F7BC655728EDC778350C8176F8BBBC90D_AdjustorThunk },
	{ 0x06000423, U3CSendClientMessageAsyncU3Ed__58_MoveNext_mD66C90618809CF9A70C25959978F5F5E644F3541_AdjustorThunk },
	{ 0x06000424, U3CSendClientMessageAsyncU3Ed__58_SetStateMachine_m6D290276DF0382BD9589625ECA249F2B4348BDAC_AdjustorThunk },
	{ 0x06000425, U3CSendServerMessageAsyncU3Ed__59_MoveNext_m4E42D1CB25FB4F3A0BF6C129E30F630177E67506_AdjustorThunk },
	{ 0x06000426, U3CSendServerMessageAsyncU3Ed__59_SetStateMachine_mA67FCC5226AA7F338976A0BD63E36F10F1FA0F07_AdjustorThunk },
	{ 0x06000427, U3CHandleLobbySearchRequestU3Ed__72_MoveNext_mD43858816A0807484305BF003FACD2D0D32D652E_AdjustorThunk },
	{ 0x06000428, U3CHandleLobbySearchRequestU3Ed__72_SetStateMachine_mD9705204E6BF8C12370E1E44F027CADA0FA79D1F_AdjustorThunk },
	{ 0x06000429, U3CSendLobbyUpdateU3Ed__80_MoveNext_mF7243DBCDB8E10D5DC0DAA519D04A491BA1C0E85_AdjustorThunk },
	{ 0x0600042A, U3CSendLobbyUpdateU3Ed__80_SetStateMachine_m398D67B94FA145E40C4795B2EC7A2C13AB5705C1_AdjustorThunk },
	{ 0x0600042B, U3CSendPlayerUpdateU3Ed__81_MoveNext_m68B140503BF008CB9C3F5EC3B6777C7014501462_AdjustorThunk },
	{ 0x0600042C, U3CSendPlayerUpdateU3Ed__81_SetStateMachine_m0544B8DD9FE6B4FC9C9606CE0F825F435692D87B_AdjustorThunk },
	{ 0x0600042D, U3CSendLobbySearchRequestU3Ed__82_MoveNext_m759975C2302973B31ED94F8205E0E2BAE33B7A4D_AdjustorThunk },
	{ 0x0600042E, U3CSendLobbySearchRequestU3Ed__82_SetStateMachine_m43B1C2551233FF1177772227F7B0C93ED08A9B83_AdjustorThunk },
	{ 0x0600042F, U3CSendClientHeartbeatU3Ed__83_MoveNext_m3E15302EBB44E9F9913E273D79A363ED4B327B3D_AdjustorThunk },
	{ 0x06000430, U3CSendClientHeartbeatU3Ed__83_SetStateMachine_mE3EF863032125870B8B3E4F5996713D2E6359DCD_AdjustorThunk },
	{ 0x06000431, U3CCreateLobbyU3Ed__84_MoveNext_m218701A5A677B7C500C48A0C272794D051B11917_AdjustorThunk },
	{ 0x06000432, U3CCreateLobbyU3Ed__84_SetStateMachine_m6CBBAB0CE132420589F3F5629A380256A1539CDB_AdjustorThunk },
	{ 0x06000433, U3CJoinLobbyByCodeU3Ed__85_MoveNext_mD72A3D5FD08210B803129F88CC3D8C3B45402B48_AdjustorThunk },
	{ 0x06000434, U3CJoinLobbyByCodeU3Ed__85_SetStateMachine_mDA60AE5B340745B217794FF36D61CB7372CE914D_AdjustorThunk },
	{ 0x06000435, U3CJoinLobbyU3Ed__86_MoveNext_m123B4D24C1ED48F8FA612BE2900BD65C330B28AB_AdjustorThunk },
	{ 0x06000436, U3CJoinLobbyU3Ed__86_SetStateMachine_m5B34EFB95E2B8B3467845E4442ECED18733DA047_AdjustorThunk },
	{ 0x06000437, U3CLeaveLobbyU3Ed__87_MoveNext_m04DA6B1D686AD5A7080C27709914228BE95C81A2_AdjustorThunk },
	{ 0x06000438, U3CLeaveLobbyU3Ed__87_SetStateMachine_m1816662CBEACB2A24E946D7CAE3DD1E21EE453D2_AdjustorThunk },
	{ 0x06000439, U3CDestroyLobbyU3Ed__88_MoveNext_m50A01D84130D48C3E13AEC498339E1D45F836CCE_AdjustorThunk },
	{ 0x0600043A, U3CDestroyLobbyU3Ed__88_SetStateMachine_m70B496D1E400BEDF360BAD4875BCA85C3B021D96_AdjustorThunk },
	{ 0x0600043B, U3CKickPlayerU3Ed__89_MoveNext_mEFF5F9A5414632CAB14C0BD10E301B72E7E8AD04_AdjustorThunk },
	{ 0x0600043C, U3CKickPlayerU3Ed__89_SetStateMachine_m873D5625E53C2781F3BD2BC364A6F98F18052AC6_AdjustorThunk },
	{ 0x0600043D, U3CTransferLobbyHostU3Ed__90_MoveNext_m2DE99B1A92DD04B6FD9A94111D42BB73CAB042AA_AdjustorThunk },
	{ 0x0600043E, U3CTransferLobbyHostU3Ed__90_SetStateMachine_mCCC607DCC0303A0585D3AFC30782A8D52E578EB6_AdjustorThunk },
	{ 0x0600043F, U3CU3CCreateLobbyU3Eb__84_0U3Ed_MoveNext_m3091BB36BAD5C9AE3FF475D2D0CF4D801DD2B775_AdjustorThunk },
	{ 0x06000440, U3CU3CCreateLobbyU3Eb__84_0U3Ed_SetStateMachine_mBB98E6072F3F29C271793965A4409115595D468D_AdjustorThunk },
	{ 0x06000526, U3CInitializeRemoteServicesU3Ed__2_MoveNext_mA4D2A43BC7AAB416CD8F4B72BDED1F724DA30C81_AdjustorThunk },
	{ 0x06000527, U3CInitializeRemoteServicesU3Ed__2_SetStateMachine_mE82F2F99EBC77877DEF695C1F0BD36DA2C56C76E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1378] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	4729,
	9731,
	12024,
	12124,
	11777,
	9563,
	12124,
	12124,
	12124,
	11881,
	12024,
	9810,
	12124,
	0,
	0,
	0,
	0,
	19887,
	11881,
	9731,
	12124,
	3067,
	9731,
	0,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	0,
	12124,
	11777,
	9563,
	11881,
	12124,
	11777,
	11944,
	9731,
	9731,
	12124,
	11881,
	2944,
	12124,
	8639,
	12124,
	12124,
	12124,
	12124,
	12124,
	2969,
	12124,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	19887,
	12124,
	12124,
	12124,
	12124,
	11944,
	12124,
	12124,
	12124,
	9672,
	12124,
	11777,
	11944,
	12124,
	11944,
	19832,
	12124,
	12124,
	12124,
	8647,
	8647,
	11944,
	11944,
	8647,
	12124,
	12124,
	12124,
	9731,
	9731,
	12124,
	7116,
	19832,
	12124,
	11777,
	9563,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	11881,
	11881,
	11881,
	8179,
	4119,
	8179,
	12124,
	11944,
	5574,
	12124,
	5194,
	9731,
	12124,
	12124,
	12124,
	9672,
	9731,
	9731,
	9731,
	9731,
	9731,
	12124,
	12124,
	0,
	0,
	12124,
	9731,
	9731,
	12124,
	5149,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9672,
	1916,
	12124,
	12124,
	12124,
	12124,
	19887,
	12124,
	8820,
	9731,
	12124,
	11777,
	9563,
	9563,
	4728,
	9731,
	12124,
	12124,
	12124,
	12024,
	9810,
	11777,
	9563,
	11777,
	9563,
	9731,
	9731,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	9672,
	12124,
	9731,
	9731,
	12124,
	9731,
	9731,
	11944,
	5582,
	5582,
	909,
	11777,
	9563,
	9563,
	4728,
	11881,
	9672,
	9672,
	5149,
	11881,
	9672,
	9672,
	12124,
	12124,
	12124,
	15625,
	15625,
	15625,
	15625,
	15625,
	11944,
	11944,
	11944,
	9731,
	12124,
	12124,
	12124,
	12124,
	11777,
	9563,
	9672,
	9672,
	12124,
	12124,
	12124,
	11944,
	11944,
	9731,
	11944,
	9731,
	11944,
	12124,
	11944,
	11944,
	5582,
	5582,
	11777,
	9563,
	9563,
	4728,
	9672,
	5149,
	9672,
	11777,
	9563,
	9563,
	4728,
	11777,
	9563,
	9563,
	4728,
	12124,
	12124,
	12124,
	15625,
	15625,
	15625,
	15625,
	11944,
	4460,
	5582,
	4460,
	12124,
	12124,
	11944,
	12124,
	9731,
	19887,
	12124,
	4452,
	12124,
	9731,
	9731,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	5582,
	5582,
	11944,
	11881,
	9672,
	9672,
	5149,
	12024,
	9810,
	9810,
	5657,
	9810,
	12024,
	9810,
	11777,
	9563,
	9563,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	15625,
	15625,
	15625,
	15625,
	15625,
	11944,
	12124,
	12124,
	12124,
	9893,
	9731,
	9731,
	9731,
	5582,
	5582,
	4730,
	4730,
	9726,
	9726,
	12124,
	11944,
	2966,
	1967,
	5640,
	5770,
	5770,
	11944,
	12124,
	12124,
	9810,
	12024,
	9810,
	9563,
	11777,
	9563,
	1823,
	866,
	12124,
	12124,
	12124,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	15625,
	11944,
	12124,
	9731,
	12124,
	9731,
	11881,
	9672,
	11944,
	11944,
	8639,
	11881,
	11777,
	11777,
	9731,
	7116,
	7116,
	7116,
	11944,
	11944,
	11881,
	8647,
	11944,
	11944,
	5582,
	12124,
	12124,
	12124,
	11944,
	3256,
	8968,
	11775,
	11775,
	11775,
	11907,
	909,
	11944,
	9731,
	11944,
	9731,
	11777,
	9563,
	9563,
	4728,
	9672,
	11777,
	9563,
	11777,
	9563,
	11777,
	9563,
	12124,
	12124,
	12124,
	11881,
	12124,
	12124,
	12124,
	15625,
	15625,
	11944,
	9672,
	12124,
	11777,
	12124,
	12124,
	11944,
	12124,
	11944,
	11944,
	11944,
	9672,
	12124,
	11777,
	12124,
	12124,
	11944,
	12124,
	11944,
	11944,
	11944,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	12124,
	12124,
	11944,
	9731,
	11944,
	8639,
	11944,
	11944,
	11944,
	11881,
	11881,
	9672,
	9672,
	5149,
	11944,
	11881,
	9672,
	9672,
	5149,
	11944,
	11777,
	9563,
	9563,
	9731,
	12124,
	9731,
	9672,
	5582,
	11944,
	11944,
	12124,
	9810,
	12124,
	12124,
	15625,
	15625,
	15625,
	11944,
	12124,
	9810,
	9810,
	19887,
	12124,
	7116,
	7116,
	9731,
	9731,
	12124,
	12124,
	12124,
	3256,
	8968,
	12124,
	9731,
	9731,
	11777,
	9563,
	11777,
	9563,
	11777,
	9563,
	11777,
	9563,
	11944,
	9731,
	12124,
	12124,
	12124,
	12124,
	9731,
	9672,
	9672,
	9672,
	9672,
	7060,
	7060,
	7060,
	7060,
	9672,
	9672,
	9672,
	9672,
	9672,
	9672,
	12124,
	12124,
	12124,
	12124,
	5088,
	5088,
	12124,
	4728,
	4728,
	4728,
	4728,
	12124,
	4728,
	12124,
	12124,
	9731,
	12124,
	12124,
	11944,
	9731,
	12124,
	9731,
	12124,
	12124,
	5574,
	12124,
	12124,
	19887,
	12124,
	5574,
	12124,
	8128,
	8128,
	11944,
	9731,
	11881,
	11944,
	9731,
	9731,
	9672,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	5574,
	12124,
	12124,
	12124,
	12124,
	12124,
	19887,
	12124,
	5574,
	11777,
	9563,
	11777,
	9563,
	11881,
	9672,
	11777,
	9563,
	11777,
	9563,
	9731,
	12124,
	12124,
	4728,
	5149,
	5657,
	5657,
	4728,
	9731,
	12124,
	9563,
	9731,
	12124,
	9672,
	9672,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9672,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	11881,
	9672,
	11777,
	9563,
	11777,
	9563,
	11944,
	9731,
	12124,
	12124,
	11944,
	9731,
	9731,
	12124,
	12124,
	12124,
	15625,
	11944,
	9731,
	5600,
	12124,
	12124,
	12124,
	12124,
	15625,
	11944,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	9731,
	9731,
	9731,
	11944,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	11881,
	11944,
	8046,
	3452,
	12124,
	12124,
	12124,
	19815,
	18913,
	16145,
	18097,
	15992,
	19832,
	19815,
	19887,
	18920,
	16468,
	18503,
	16958,
	19887,
	19832,
	19887,
	19887,
	12124,
	9731,
	12124,
	9731,
	19887,
	12124,
	4004,
	12124,
	19832,
	12124,
	2505,
	4718,
	5245,
	12124,
	16467,
	9538,
	12124,
	12124,
	15625,
	15625,
	11944,
	12124,
	9731,
	12124,
	9731,
	12124,
	12124,
	4460,
	5300,
	4718,
	5245,
	12124,
	9538,
	12124,
	12124,
	15625,
	15625,
	11944,
	12124,
	8617,
	12124,
	9731,
	12124,
	7263,
	12124,
	9731,
	12124,
	9731,
	11944,
	9731,
	11944,
	9731,
	11944,
	11944,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	9731,
	5539,
	9726,
	12124,
	12124,
	12124,
	15625,
	15625,
	11944,
	11944,
	11944,
	11777,
	11777,
	11777,
	11944,
	12124,
	9731,
	9731,
	9731,
	9731,
	9731,
	12124,
	9731,
	9731,
	9731,
	9731,
	11944,
	12113,
	9731,
	5582,
	11944,
	11944,
	8639,
	8647,
	12124,
	11944,
	19887,
	12124,
	5582,
	5582,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	9731,
	11881,
	9731,
	9731,
	11944,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	11944,
	11944,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	9731,
	9731,
	12124,
	19832,
	18920,
	12124,
	12124,
	12124,
	12124,
	11777,
	11944,
	11777,
	11944,
	9731,
	1664,
	8647,
	8647,
	12124,
	9731,
	12124,
	9731,
	12124,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	11881,
	9731,
	12124,
	9731,
	11944,
	9731,
	11777,
	11777,
	9731,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	12124,
	9731,
	19832,
	18920,
	11944,
	12100,
	12100,
	11777,
	11944,
	11944,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	11944,
	12124,
	11944,
	12124,
	12124,
	11944,
	12124,
	4460,
	4460,
	11777,
	11777,
	11777,
	7116,
	7116,
	5582,
	5582,
	12124,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	12124,
	12124,
	12124,
	11944,
	11944,
	8647,
	4460,
	8647,
	12124,
	12124,
	9731,
	9731,
	9731,
	8647,
	12124,
	12124,
	11944,
	12124,
	12124,
	12124,
	9731,
	12124,
	12124,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9731,
	12124,
	9563,
	11944,
	9731,
	12124,
	9731,
	9731,
	19832,
	18920,
	12124,
	12124,
	11944,
	11944,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	11944,
	9731,
	12124,
	12124,
	9731,
	9731,
	9731,
	9731,
	12124,
	12124,
	12124,
	9731,
	9731,
	12124,
	11944,
	12124,
	11777,
	9563,
	12124,
	12124,
	12124,
	12124,
	12124,
	11944,
	9731,
	11944,
	9731,
	12124,
	12124,
	12124,
	9731,
	9731,
	9731,
	9731,
	9731,
	9731,
	12124,
	12124,
	9672,
	12124,
	12124,
	9810,
	9810,
	12124,
	9810,
	9810,
	12124,
	12124,
	9731,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	9731,
	9731,
	8647,
	8647,
	19780,
	19832,
	12124,
	12124,
	12124,
	19887,
	12124,
	12124,
	12124,
	12124,
	9672,
	12124,
	11777,
	11944,
	12124,
	11944,
	11944,
	9731,
	12124,
	11944,
	9731,
	12124,
	12124,
	12124,
	12124,
	11944,
	12124,
	12124,
	12124,
	12124,
	9672,
	12124,
	11777,
	11944,
	12124,
	11944,
	11944,
	9731,
	4440,
	12124,
	12124,
	11944,
	9731,
	12124,
	12124,
	12124,
	11944,
	9731,
	11944,
	9731,
	11944,
	12124,
	12124,
	12124,
	0,
	0,
	0,
	12024,
	9810,
	11881,
	11881,
	11881,
	11881,
	12024,
	12024,
	11944,
	9731,
	11944,
	9731,
	8128,
	8128,
	8639,
	8968,
	8823,
	8823,
	12124,
	12124,
	11944,
	12124,
	12124,
	12124,
	9810,
	9810,
	12124,
	9672,
	9672,
	5149,
	12124,
	3522,
	3522,
	8128,
	9672,
	5088,
	9672,
	12124,
	9890,
	11777,
	11777,
	11881,
	11944,
	11944,
	11881,
	11881,
	12124,
	9672,
	12124,
	11777,
	12124,
	12124,
	11944,
	12124,
	11944,
	11944,
	11944,
	9672,
	12124,
	11777,
	12124,
	12124,
	11944,
	12124,
	11944,
	11944,
	11944,
	9672,
	12124,
	11777,
	11944,
	12124,
	11944,
	11881,
	9672,
	12124,
	12124,
	12124,
	12124,
	12124,
	12124,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	19832,
	19832,
	12124,
	12124,
	9731,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	11777,
	12124,
	12124,
	11944,
	12124,
	12124,
	12124,
	12124,
	19887,
};
static const Il2CppTokenRangePair s_rgctxIndices[28] = 
{
	{ 0x02000002, { 0, 15 } },
	{ 0x02000068, { 54, 8 } },
	{ 0x020000E0, { 77, 35 } },
	{ 0x020000E3, { 112, 13 } },
	{ 0x020000E4, { 125, 20 } },
	{ 0x0600001E, { 15, 2 } },
	{ 0x0600001F, { 17, 2 } },
	{ 0x06000021, { 19, 2 } },
	{ 0x06000023, { 21, 3 } },
	{ 0x06000025, { 24, 4 } },
	{ 0x06000027, { 28, 3 } },
	{ 0x06000029, { 31, 8 } },
	{ 0x0600002B, { 39, 1 } },
	{ 0x0600002D, { 40, 2 } },
	{ 0x0600002F, { 42, 2 } },
	{ 0x06000031, { 44, 1 } },
	{ 0x06000033, { 45, 2 } },
	{ 0x06000035, { 47, 1 } },
	{ 0x06000037, { 48, 6 } },
	{ 0x060002D2, { 62, 1 } },
	{ 0x0600051B, { 63, 1 } },
	{ 0x0600051C, { 64, 1 } },
	{ 0x0600051D, { 65, 2 } },
	{ 0x0600051E, { 67, 2 } },
	{ 0x0600051F, { 69, 2 } },
	{ 0x06000520, { 71, 2 } },
	{ 0x06000521, { 73, 2 } },
	{ 0x06000522, { 75, 2 } },
};
extern const uint32_t g_rgctx_U3CU3Ef__AnonymousType0_2_t83A5F781C9210E341719DBF8BE50CCDC4C364236;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mA22CFD723B5172F96926FC8B4F33A5B78F428F05;
extern const uint32_t g_rgctx_EqualityComparer_1_t84BB38BBDD3554572FF3616F8BBC4DDDFE783BEF;
extern const uint32_t g_rgctx_EqualityComparer_1_t84BB38BBDD3554572FF3616F8BBC4DDDFE783BEF;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m1F93983048E853CDAC463DCEF3B2A1583B7C3C00;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m39467E01E9CA7F6C7CB0FC36E58A9C9429B9868B;
extern const uint32_t g_rgctx_EqualityComparer_1_t345BFD7CFEECFE031BD9E7D0EC934A028FD070D5;
extern const uint32_t g_rgctx_EqualityComparer_1_t345BFD7CFEECFE031BD9E7D0EC934A028FD070D5;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m11BF028A965552C61F50212FA9254C2D30874C90;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_m0B38695EE9B916EFCEF0F96A8051DC69567C3BCB;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mFF642C55AB376D72E4B9BF0CA4BF92F241CD6931;
extern const uint32_t g_rgctx_U3CNetworkReferenceU3Ej__TPar_tE693FE05683043149CFAB8B393026B7051CDDFA5;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CNetworkReferenceU3Ej__TPar_tE693FE05683043149CFAB8B393026B7051CDDFA5_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CNodeInfoU3Ej__TPar_tAF2D7033D5E845C5CF0C9D12E6C51FA93D2821DB;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CNodeInfoU3Ej__TPar_tAF2D7033D5E845C5CF0C9D12E6C51FA93D2821DB_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_m5DAD8788F7D14EA0824AD1876BAA8A34F2842494;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_m01877E1FDF72A3CF42578C05507B05303221852F;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_m76D0F56F095821C0CB4A3142219BA63FACBB6496;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mEB1578A6DC01C4F6FC5C9CD001B3761932AD74C3;
extern const uint32_t g_rgctx_NodeState_NetworkSerialize_TisT_tEC0A9F2F24B6022E820A169D97F080ECF7196350_m3E6BA83CA68B3D485EE514441A508DD749579DB6;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mCF2CBE781DAF08FCC5E61DBBDFAFCF3A8373E214;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m2229C4091C14F4917BB72FDC6150AACF88BD39B3;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m88423417E97BB7F497998E260C747AD685B0948F;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisAnimationMode_t18EA6E57BAA8C2A59CCBA671E7F05CFAFFB55753_mF8DCB1B4F09E32D1BFDEA03D75D0E72D69DB2230;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mB5074D9626C8CD444944813DFAB6BBCCB546A853;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_mAA8BE168BBD42780AEAF4D5A09F45C179435EBAE;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisClippingPlaneType_t9C15AB83AC25FCAE15DB31BB3F56D50D700CB660_m03FFCC3F380204A0E747B4114842230150A2B76F;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_m566429A8E6332ABD833B0175BA97BE064A977DB8;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m1DDC35721350C7CBD850CD523B0085D02E5A431E;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_mDD92EECB77F006595392BC3F7DD6CCCAE44F2B60;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mE35CFBBC2581B0D8749E41B2ABEB82611DD564AB;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_mEF810BD9D5071B2F4D658BC4FE87E4444AF80B92;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m22950432E60C98C0794FDE48A582703971265CBE;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m8777038FDF8B578C589375E328C36DD2ABB66449;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m0678A3E28E4AF2789660DAC8DAB6DFD3CFB9AC5F;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisNodeState_t7803840D8CE424224C3F831EDA4275E2ED58CC82_m6EFE26387AAE95A52DC1A6670746E53E14E7BBF2;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisUIState_tA9D35C5AE3B41595E5126C66BA9D5EEFAA73A8C2_mB545CF6FB7C54DBE68E614C17B9A7C58295A02BB;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisAnimationState_t42C71CFDC8BD616159251A185D30B44040667685_m87C6BE864FDB27BADD5E779817083D1425563B7C;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisClippingPlaneState_t9964E2D27F0131C1C669B9EE9969F3453FE8E871_m0CFE2551F09E348E9D4AF402EFE2156D2190EBD7;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m02F71C8515AF6FEF4F9E2C1F10EE939531922363;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mE1DF8D4A0166052E0C1DE13E9B23587D18E3380A;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransformTransition_tD1A538B53A3CB64214498AD0D174FFC0BCD2D14E_mFE7BAFDB9D2787B618B31FD97E2E2208F17B220C;
extern const uint32_t g_rgctx_NodeStateTransition_NetworkSerialize_TisT_tA2926E8273F25E18CEE13D3076C481B633BEF96F_mED5B3C9C61F98E8DFF3E61C073BA2E2465568752;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m379CAC552C897CCB43D03176555624C3C0F047BF;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mF12C9D383035BFB483631EE56D35292DEAE51BF2;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m6E26B680680495F80D93A20158F91D3C8D71AEEF;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransformTransition_tD1A538B53A3CB64214498AD0D174FFC0BCD2D14E_mE2CEBE4763BE874A759E1798F609B74091B42481;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m057EF379422A0F40FDF52CE2A2CB189730024A79;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m413236BB2C898040BF68C1DC5A3D59D484F57263;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransformTransition_tD1A538B53A3CB64214498AD0D174FFC0BCD2D14E_m8769829AC080CB5F34204732B98364AC947A3978;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisNodeStateTransition_tAF3211019488ADB5ACF40CB9BBC511167ECADA37_m95749DF3FD40CAAFCE531E4E808F3B5554F78EB3;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisAnimationStateTransition_t39A0F157A5F57C398BF0393C31CFCDDB5462D81E_m77AE273843C7B3BBF9E14A5043B9E64DD07E2EFA;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisClippingPlaneStateTransition_tCF26C7B578610D2B01EF9D9D4E3C3A6268BBBB3E_m390AC2EE1E206AC149DF4BD2044DE14AAA876E0B;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisUIStateTransition_t0EAC8DE32F79733B96D8BB129767C4256B6AC92C_m3CFF7AFE77D19331EDCF017003B99D60CF2A9198;
extern const uint32_t g_rgctx_SN_1_get_HasValue_mEE59F5483343CD2531805F83B8B26560ACF281F7;
extern const uint32_t g_rgctx_SN_1_get_Value_m242FE25A41E4819C4C4FE5A51AAFE0E538CD7E0D;
extern const uint32_t g_rgctx_SN_1_t881CFFE1D5F5B414AE8CE58E9AAF7F2FFDB389E2;
extern const uint32_t g_rgctx_SN_1__ctor_m8239F21642D6C0EA3E2C2D2B0D47A0968C4589A6;
extern const uint32_t g_rgctx_Nullable_1_get_HasValue_m7742A4EF320B8BB2B13DBBCDE54CE919F0FA55BD;
extern const uint32_t g_rgctx_Nullable_1_get_Value_m9FCE5202F893AEA926B2E8D2D91B49D63AA92208;
extern const uint32_t g_rgctx_Nullable_1_tDCC4D83F3F43B228BA0C64B5F5BA1A1CE96C619E;
extern const uint32_t g_rgctx_Nullable_1__ctor_m436933044D5EC4296E6C6E101D81818EFA1C87EF;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisFixedString128Bytes_tEBC488E0CC30C6D842951A4E6F09AC58677F1952_m5891C29032ED5A9FDEFA14BA16DBF6314AB13375;
extern const uint32_t g_rgctx_T_t36A9F40131D5D0C2F25AE6ACB57606C07A090206;
extern const uint32_t g_rgctx_JsonConvert_DeserializeObject_TisT_tE35B7D6468F2AA89097D5F53311461F60E43BF39_mAFDAA4B258C69EC6380F331E8BBD1D62BAAED82B;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m9B7C9235E10FEBA4540DD3A5659EBE7BC8A27516;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_m8FEF23822877ACE3D368C6A335D410A3721C3710;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m7F9673E882A3C1DC645B9DFDCB020F68D24A1C5C;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_m3C83D49966CFFF72828B108CFEDE3C7C68472AF1;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m86D716CEC489F2681E8B64AC7CF93931AC91E3BA;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_m229A222149C133370BEB0E0A9B3D03E7E07B689E;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mDC59B01510F6C04FB02674791573091181280427;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_m6866B5AF746CE1B0029C06562FF6A79B0851B0C4;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mCB7055283C85AD6A25E0D5541C649672B679FA5B;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_mF141BC109932CE005CAFC6303BB19FF156FB0C74;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m45F04F12466ADCEFD924B8F871597ADC38B31E19;
extern const uint32_t g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_m221E6A919411AE8D5F42EF58547D4BDB06786743;
extern const uint32_t g_rgctx_List_1_tFDDD816F3D87842A1F853F3179B6A2BA806A18C7;
extern const uint32_t g_rgctx_List_1__ctor_m15C3796A1E56A01093AFC392D9A27C6CF0DDD3C9;
extern const uint32_t g_rgctx_TreeNode_1__ctor_m26E195CD8648582E93964D69AD586F264321690B;
extern const uint32_t g_rgctx_TreeNode_1_t91FCEFA3A5A46CB1E3A8751545C1E64138C396BF;
extern const uint32_t g_rgctx_TreeNode_1_get_Level_m3ABCF2BE164DA353677934C02538BD4B7A28B537;
extern const uint32_t g_rgctx_List_1_get_Count_m0BD666A0CE98E029F712432A39ADF67289E916B5;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m895E903B46A1FC834342B6D175016A9E083A5620;
extern const uint32_t g_rgctx_Enumerator_get_Current_m513CA3101B9F483790C09379B5CD2ED587EDC1C0;
extern const uint32_t g_rgctx_TreeNode_1_get_Size_m6764C8567AC9A504A4E15F2E2C85ED1D29D287DC;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m334BE38E692826143481DD79764343AE308A5BD8;
extern const uint32_t g_rgctx_Enumerator_tD5B12D31BC3D6C05D3BA758A07AD9970FCD780EE;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tD5B12D31BC3D6C05D3BA758A07AD9970FCD780EE_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_List_1_get_Item_m5510E922691B975312A3FF4859F4C28FFE385E3E;
extern const uint32_t g_rgctx_List_1_Clear_m032B4F482A69E1B4B1896DC17A1596D53C0EDDED;
extern const uint32_t g_rgctx_TreeNode_1__ctor_mE2A6DEEC18CFFD2A9973C1BA270471736EA18E0B;
extern const uint32_t g_rgctx_List_1_Add_m331C3D0B2D951B2A8AE4D4C3B24212208BC1B49D;
extern const uint32_t g_rgctx_TreeNode_1_FindInChildren_m176619DADA951720FCB7B5E9C8A0E4EA630ADBCD;
extern const uint32_t g_rgctx_TreeNode_1_get_Count_mCC953E379E65D054707F5C539845C93BDC653532;
extern const uint32_t g_rgctx_TreeNode_1_get_Data_m3C223CC660C17B4C52FB08E6CB36B2FF663098C2;
extern const uint32_t g_rgctx_T_tF8590C56E585C4D13A029356AD136FC3CAE5E78B;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tF8590C56E585C4D13A029356AD136FC3CAE5E78B_Object_Equals_m07105C4585D3FE204F2A80D58523D001DC43F63B;
extern const uint32_t g_rgctx_List_1_Remove_m1883F1A7C780AD6BF8D2D5CF19A7D1D083210C14;
extern const uint32_t g_rgctx_U3CGetEnumeratorU3Ed__32_t7ECE412EE0AC4877BFBEC1352BDF65F5D641A4F4;
extern const uint32_t g_rgctx_U3CGetEnumeratorU3Ed__32__ctor_m4BE035C0287E00D2255B3D926E524D1C5E322A65;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33_t8CBE52B6A9B5A60DAF6B68EC048F166DE868BFB0;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33__ctor_m26C1BC0CCF91017E439AEC4795CF268DE2D129E8;
extern const uint32_t g_rgctx_TreeNode_1_GetEnumerator_m9E330536DBC7769C92E1E0E6A068BF60568DA7DD;
extern const uint32_t g_rgctx_IEnumerator_1_tA105693C27CF4C6DF68D8D879B6A3412669A7BB9;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_mC576567804E15EB99C6BE99B2564B4966A1764A7;
extern const uint32_t g_rgctx_TraversalDataDelegate_t104C910423D025767D0C8053E7B715066778FF28;
extern const uint32_t g_rgctx_TraversalDataDelegate_Invoke_m2AC45D6F664F6BFED6C3B33E1211DBA71DCB79CA;
extern const uint32_t g_rgctx_TreeNode_1_Traverse_m34352D453B7FAC79313A354986CCC6BB15ADA7E9;
extern const uint32_t g_rgctx_TraversalNodeDelegate_t637DC6D6A1449CB04371675F46732D0701FB37CA;
extern const uint32_t g_rgctx_TraversalNodeDelegate_Invoke_mE673F5A6CD213EDACC550814EA24B42028FDB3A8;
extern const uint32_t g_rgctx_TreeNode_1_Traverse_m2FADE06920B4447F2DEF5C80D347E3AADCDF5ED4;
extern const uint32_t g_rgctx_U3CGetEnumeratorU3Ed__32_U3CU3Em__Finally2_m77CC3B147F12D4FE94C638550083125C600614FF;
extern const uint32_t g_rgctx_U3CGetEnumeratorU3Ed__32_U3CU3Em__Finally1_m9116EBC95DEFC51AD4A53A600B82DCF0783A5D3F;
extern const uint32_t g_rgctx_List_1_tA357EA2AFBCCB361CE2FAC0F5076B7F9366628F7;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m7E0BD390834F0527B47CA0C5B228CB1A85BFD285;
extern const uint32_t g_rgctx_Enumerator_get_Current_m527412339A721D3C7006B6075D31F9ABE8F924EE;
extern const uint32_t g_rgctx_TreeNode_1_t0B8C7736F6ACA60212498154C84279143CC880BE;
extern const uint32_t g_rgctx_TreeNode_1_GetEnumerator_m133871C0868548350B898B025374C83699C3E476;
extern const uint32_t g_rgctx_IEnumerator_1_t7D376B67D58328A675398D0009A7641FE5CE3C62;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m302E094469A24EACEB162BB1F2819CC3832569AB;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mC9039957A9624180ACF5BD31A5023446874AB4DD;
extern const uint32_t g_rgctx_U3CGetEnumeratorU3Ed__32_System_IDisposable_Dispose_m7108D9BB162EFF60C44F851119DDAEE31E3C03AA;
extern const uint32_t g_rgctx_Enumerator_tCDC62EC481BC3C56469802E787E935D97D867BCF;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tCDC62EC481BC3C56469802E787E935D97D867BCF_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33_U3CU3Em__Finally2_mA76295FA2F2055EB024FA27D7CFEC171EAEABEBD;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33_U3CU3Em__Finally1_mDAECC4687E7F4607D6227BF762D43BF2B6444EF4;
extern const uint32_t g_rgctx_TreeNode_1_get_Data_mDA451468010575C2EC70CE5194FF6C80CFCF5C6D;
extern const uint32_t g_rgctx_List_1_tDD654ACFB3444F326B7BA84ED2ED020FCCB6A798;
extern const uint32_t g_rgctx_List_1_GetEnumerator_mD79F44A819A8622A31568D2CF7AC4F1C4644B500;
extern const uint32_t g_rgctx_Enumerator_get_Current_m2FF9BEADB4E2515E8BED258CA7A9FF1655135649;
extern const uint32_t g_rgctx_TreeNode_1_tBDCE55C02393B596D403E664ED1C2D58A7FE82D5;
extern const uint32_t g_rgctx_TreeNode_1_NodesData_m4000446FD1DD83D76DD28C738B8385B2B6B65232;
extern const uint32_t g_rgctx_IEnumerable_1_t062ADA925510CA9C1602CCCD47B8B90EF6ECF2F4;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_mE17ED66F5A2EE4DA5FF9F93C7DED698445E41CC3;
extern const uint32_t g_rgctx_IEnumerator_1_t8969EE1E3D0D19A5A47599F160A95F83CDBBA3D8;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m2581AD220FB3262AE0754BB98A87C33AA3ECBB9F;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m7E10210401567F1D8A25E208D1D07E016AA7327C;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33_System_IDisposable_Dispose_mAE8B86104E1FEC34929A7CD46288AA6DE826998D;
extern const uint32_t g_rgctx_Enumerator_t847782C7DF6B494C2324734274CD6A3FE5834B69;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t847782C7DF6B494C2324734274CD6A3FE5834B69_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_T_t19254A602029730CCBF2F86AE0D6DC3F7791E288;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33_t5CC8E4FF0D413685768DE4A2089533D3C8C1EC70;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33__ctor_m8330FBEA1726F358FAC0076F8BA0AC07E4C55CA3;
extern const uint32_t g_rgctx_U3CNodesDataU3Ed__33_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mA646692813B0190860232886CCE97B07FF0FC02B;
static const Il2CppRGCTXDefinition s_rgctxValues[145] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ef__AnonymousType0_2_t83A5F781C9210E341719DBF8BE50CCDC4C364236 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mA22CFD723B5172F96926FC8B4F33A5B78F428F05 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t84BB38BBDD3554572FF3616F8BBC4DDDFE783BEF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t84BB38BBDD3554572FF3616F8BBC4DDDFE783BEF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m1F93983048E853CDAC463DCEF3B2A1583B7C3C00 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m39467E01E9CA7F6C7CB0FC36E58A9C9429B9868B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t345BFD7CFEECFE031BD9E7D0EC934A028FD070D5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t345BFD7CFEECFE031BD9E7D0EC934A028FD070D5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m11BF028A965552C61F50212FA9254C2D30874C90 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_m0B38695EE9B916EFCEF0F96A8051DC69567C3BCB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mFF642C55AB376D72E4B9BF0CA4BF92F241CD6931 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CNetworkReferenceU3Ej__TPar_tE693FE05683043149CFAB8B393026B7051CDDFA5 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CNetworkReferenceU3Ej__TPar_tE693FE05683043149CFAB8B393026B7051CDDFA5_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CNodeInfoU3Ej__TPar_tAF2D7033D5E845C5CF0C9D12E6C51FA93D2821DB },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CNodeInfoU3Ej__TPar_tAF2D7033D5E845C5CF0C9D12E6C51FA93D2821DB_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_m5DAD8788F7D14EA0824AD1876BAA8A34F2842494 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_m01877E1FDF72A3CF42578C05507B05303221852F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_m76D0F56F095821C0CB4A3142219BA63FACBB6496 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mEB1578A6DC01C4F6FC5C9CD001B3761932AD74C3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NodeState_NetworkSerialize_TisT_tEC0A9F2F24B6022E820A169D97F080ECF7196350_m3E6BA83CA68B3D485EE514441A508DD749579DB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mCF2CBE781DAF08FCC5E61DBBDFAFCF3A8373E214 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m2229C4091C14F4917BB72FDC6150AACF88BD39B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m88423417E97BB7F497998E260C747AD685B0948F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisAnimationMode_t18EA6E57BAA8C2A59CCBA671E7F05CFAFFB55753_mF8DCB1B4F09E32D1BFDEA03D75D0E72D69DB2230 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mB5074D9626C8CD444944813DFAB6BBCCB546A853 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_mAA8BE168BBD42780AEAF4D5A09F45C179435EBAE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisClippingPlaneType_t9C15AB83AC25FCAE15DB31BB3F56D50D700CB660_m03FFCC3F380204A0E747B4114842230150A2B76F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_m566429A8E6332ABD833B0175BA97BE064A977DB8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m1DDC35721350C7CBD850CD523B0085D02E5A431E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_mDD92EECB77F006595392BC3F7DD6CCCAE44F2B60 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mE35CFBBC2581B0D8749E41B2ABEB82611DD564AB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_mEF810BD9D5071B2F4D658BC4FE87E4444AF80B92 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m22950432E60C98C0794FDE48A582703971265CBE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m8777038FDF8B578C589375E328C36DD2ABB66449 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m0678A3E28E4AF2789660DAC8DAB6DFD3CFB9AC5F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisNodeState_t7803840D8CE424224C3F831EDA4275E2ED58CC82_m6EFE26387AAE95A52DC1A6670746E53E14E7BBF2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisUIState_tA9D35C5AE3B41595E5126C66BA9D5EEFAA73A8C2_mB545CF6FB7C54DBE68E614C17B9A7C58295A02BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisAnimationState_t42C71CFDC8BD616159251A185D30B44040667685_m87C6BE864FDB27BADD5E779817083D1425563B7C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisClippingPlaneState_t9964E2D27F0131C1C669B9EE9969F3453FE8E871_m0CFE2551F09E348E9D4AF402EFE2156D2190EBD7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m02F71C8515AF6FEF4F9E2C1F10EE939531922363 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mE1DF8D4A0166052E0C1DE13E9B23587D18E3380A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransformTransition_tD1A538B53A3CB64214498AD0D174FFC0BCD2D14E_mFE7BAFDB9D2787B618B31FD97E2E2208F17B220C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NodeStateTransition_NetworkSerialize_TisT_tA2926E8273F25E18CEE13D3076C481B633BEF96F_mED5B3C9C61F98E8DFF3E61C073BA2E2465568752 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m379CAC552C897CCB43D03176555624C3C0F047BF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mF12C9D383035BFB483631EE56D35292DEAE51BF2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m6E26B680680495F80D93A20158F91D3C8D71AEEF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransformTransition_tD1A538B53A3CB64214498AD0D174FFC0BCD2D14E_mE2CEBE4763BE874A759E1798F609B74091B42481 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m057EF379422A0F40FDF52CE2A2CB189730024A79 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m413236BB2C898040BF68C1DC5A3D59D484F57263 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransformTransition_tD1A538B53A3CB64214498AD0D174FFC0BCD2D14E_m8769829AC080CB5F34204732B98364AC947A3978 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisNodeStateTransition_tAF3211019488ADB5ACF40CB9BBC511167ECADA37_m95749DF3FD40CAAFCE531E4E808F3B5554F78EB3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisAnimationStateTransition_t39A0F157A5F57C398BF0393C31CFCDDB5462D81E_m77AE273843C7B3BBF9E14A5043B9E64DD07E2EFA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisClippingPlaneStateTransition_tCF26C7B578610D2B01EF9D9D4E3C3A6268BBBB3E_m390AC2EE1E206AC149DF4BD2044DE14AAA876E0B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisUIStateTransition_t0EAC8DE32F79733B96D8BB129767C4256B6AC92C_m3CFF7AFE77D19331EDCF017003B99D60CF2A9198 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SN_1_get_HasValue_mEE59F5483343CD2531805F83B8B26560ACF281F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SN_1_get_Value_m242FE25A41E4819C4C4FE5A51AAFE0E538CD7E0D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SN_1_t881CFFE1D5F5B414AE8CE58E9AAF7F2FFDB389E2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SN_1__ctor_m8239F21642D6C0EA3E2C2D2B0D47A0968C4589A6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1_get_HasValue_m7742A4EF320B8BB2B13DBBCDE54CE919F0FA55BD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1_get_Value_m9FCE5202F893AEA926B2E8D2D91B49D63AA92208 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Nullable_1_tDCC4D83F3F43B228BA0C64B5F5BA1A1CE96C619E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1__ctor_m436933044D5EC4296E6C6E101D81818EFA1C87EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisFixedString128Bytes_tEBC488E0CC30C6D842951A4E6F09AC58677F1952_m5891C29032ED5A9FDEFA14BA16DBF6314AB13375 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t36A9F40131D5D0C2F25AE6ACB57606C07A090206 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConvert_DeserializeObject_TisT_tE35B7D6468F2AA89097D5F53311461F60E43BF39_mAFDAA4B258C69EC6380F331E8BBD1D62BAAED82B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m9B7C9235E10FEBA4540DD3A5659EBE7BC8A27516 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_m8FEF23822877ACE3D368C6A335D410A3721C3710 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m7F9673E882A3C1DC645B9DFDCB020F68D24A1C5C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_m3C83D49966CFFF72828B108CFEDE3C7C68472AF1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m86D716CEC489F2681E8B64AC7CF93931AC91E3BA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_m229A222149C133370BEB0E0A9B3D03E7E07B689E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mDC59B01510F6C04FB02674791573091181280427 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_m6866B5AF746CE1B0029C06562FF6A79B0851B0C4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_mCB7055283C85AD6A25E0D5541C649672B679FA5B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_mF141BC109932CE005CAFC6303BB19FF156FB0C74 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m45F04F12466ADCEFD924B8F871597ADC38B31E19 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BufferSerializer_1_SerializeValue_TisLocalTransform_tF01D494AD7A350FBFDCED119781B4A130C4BD47E_m221E6A919411AE8D5F42EF58547D4BDB06786743 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tFDDD816F3D87842A1F853F3179B6A2BA806A18C7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m15C3796A1E56A01093AFC392D9A27C6CF0DDD3C9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1__ctor_m26E195CD8648582E93964D69AD586F264321690B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeNode_1_t91FCEFA3A5A46CB1E3A8751545C1E64138C396BF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_get_Level_m3ABCF2BE164DA353677934C02538BD4B7A28B537 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m0BD666A0CE98E029F712432A39ADF67289E916B5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_m895E903B46A1FC834342B6D175016A9E083A5620 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m513CA3101B9F483790C09379B5CD2ED587EDC1C0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_get_Size_m6764C8567AC9A504A4E15F2E2C85ED1D29D287DC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m334BE38E692826143481DD79764343AE308A5BD8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tD5B12D31BC3D6C05D3BA758A07AD9970FCD780EE },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tD5B12D31BC3D6C05D3BA758A07AD9970FCD780EE_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m5510E922691B975312A3FF4859F4C28FFE385E3E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Clear_m032B4F482A69E1B4B1896DC17A1596D53C0EDDED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1__ctor_mE2A6DEEC18CFFD2A9973C1BA270471736EA18E0B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m331C3D0B2D951B2A8AE4D4C3B24212208BC1B49D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_FindInChildren_m176619DADA951720FCB7B5E9C8A0E4EA630ADBCD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_get_Count_mCC953E379E65D054707F5C539845C93BDC653532 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_get_Data_m3C223CC660C17B4C52FB08E6CB36B2FF663098C2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tF8590C56E585C4D13A029356AD136FC3CAE5E78B },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tF8590C56E585C4D13A029356AD136FC3CAE5E78B_Object_Equals_m07105C4585D3FE204F2A80D58523D001DC43F63B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Remove_m1883F1A7C780AD6BF8D2D5CF19A7D1D083210C14 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CGetEnumeratorU3Ed__32_t7ECE412EE0AC4877BFBEC1352BDF65F5D641A4F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CGetEnumeratorU3Ed__32__ctor_m4BE035C0287E00D2255B3D926E524D1C5E322A65 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CNodesDataU3Ed__33_t8CBE52B6A9B5A60DAF6B68EC048F166DE868BFB0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CNodesDataU3Ed__33__ctor_m26C1BC0CCF91017E439AEC4795CF268DE2D129E8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_GetEnumerator_m9E330536DBC7769C92E1E0E6A068BF60568DA7DD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tA105693C27CF4C6DF68D8D879B6A3412669A7BB9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_mC576567804E15EB99C6BE99B2564B4966A1764A7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TraversalDataDelegate_t104C910423D025767D0C8053E7B715066778FF28 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TraversalDataDelegate_Invoke_m2AC45D6F664F6BFED6C3B33E1211DBA71DCB79CA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_Traverse_m34352D453B7FAC79313A354986CCC6BB15ADA7E9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TraversalNodeDelegate_t637DC6D6A1449CB04371675F46732D0701FB37CA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TraversalNodeDelegate_Invoke_mE673F5A6CD213EDACC550814EA24B42028FDB3A8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_Traverse_m2FADE06920B4447F2DEF5C80D347E3AADCDF5ED4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CGetEnumeratorU3Ed__32_U3CU3Em__Finally2_m77CC3B147F12D4FE94C638550083125C600614FF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CGetEnumeratorU3Ed__32_U3CU3Em__Finally1_m9116EBC95DEFC51AD4A53A600B82DCF0783A5D3F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tA357EA2AFBCCB361CE2FAC0F5076B7F9366628F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_m7E0BD390834F0527B47CA0C5B228CB1A85BFD285 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m527412339A721D3C7006B6075D31F9ABE8F924EE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeNode_1_t0B8C7736F6ACA60212498154C84279143CC880BE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_GetEnumerator_m133871C0868548350B898B025374C83699C3E476 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t7D376B67D58328A675398D0009A7641FE5CE3C62 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m302E094469A24EACEB162BB1F2819CC3832569AB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_mC9039957A9624180ACF5BD31A5023446874AB4DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CGetEnumeratorU3Ed__32_System_IDisposable_Dispose_m7108D9BB162EFF60C44F851119DDAEE31E3C03AA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tCDC62EC481BC3C56469802E787E935D97D867BCF },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tCDC62EC481BC3C56469802E787E935D97D867BCF_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CNodesDataU3Ed__33_U3CU3Em__Finally2_mA76295FA2F2055EB024FA27D7CFEC171EAEABEBD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CNodesDataU3Ed__33_U3CU3Em__Finally1_mDAECC4687E7F4607D6227BF762D43BF2B6444EF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_get_Data_mDA451468010575C2EC70CE5194FF6C80CFCF5C6D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tDD654ACFB3444F326B7BA84ED2ED020FCCB6A798 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_mD79F44A819A8622A31568D2CF7AC4F1C4644B500 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m2FF9BEADB4E2515E8BED258CA7A9FF1655135649 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeNode_1_tBDCE55C02393B596D403E664ED1C2D58A7FE82D5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeNode_1_NodesData_m4000446FD1DD83D76DD28C738B8385B2B6B65232 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t062ADA925510CA9C1602CCCD47B8B90EF6ECF2F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_mE17ED66F5A2EE4DA5FF9F93C7DED698445E41CC3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t8969EE1E3D0D19A5A47599F160A95F83CDBBA3D8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m2581AD220FB3262AE0754BB98A87C33AA3ECBB9F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m7E10210401567F1D8A25E208D1D07E016AA7327C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CNodesDataU3Ed__33_System_IDisposable_Dispose_mAE8B86104E1FEC34929A7CD46288AA6DE826998D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t847782C7DF6B494C2324734274CD6A3FE5834B69 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t847782C7DF6B494C2324734274CD6A3FE5834B69_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t19254A602029730CCBF2F86AE0D6DC3F7791E288 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CNodesDataU3Ed__33_t5CC8E4FF0D413685768DE4A2089533D3C8C1EC70 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CNodesDataU3Ed__33__ctor_m8330FBEA1726F358FAC0076F8BA0AC07E4C55CA3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CNodesDataU3Ed__33_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mA646692813B0190860232886CCE97B07FF0FC02B },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1378,
	s_methodPointers,
	118,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	28,
	s_rgctxIndices,
	145,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
